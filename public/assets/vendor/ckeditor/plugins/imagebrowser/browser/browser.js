var CkEditorImageBrowser = {};
var CkEditorImageBrowserPage = 1;

CkEditorImageBrowser.images = [];
CkEditorImageBrowser.images_custom_source = [];
CkEditorImageBrowser.ckFunctionNum = null;

CkEditorImageBrowser.$imagesContainer = null;
CkEditorImageBrowser.$images_customContainer = null;

CkEditorImageBrowser.init = function () {
	CkEditorImageBrowser.$imagesContainer = $('#js-images-container');
	CkEditorImageBrowser.$images_customContainer = $('#custom_source_media');

	var baseHref = CkEditorImageBrowser.getQueryStringParam("baseHref");
	if (baseHref) {
		var h = (document.head || document.getElementsByTagName("head")[0]),
			el = h.getElementsByTagName("link")[0];
		el.href = location.href.replace(/\/[^\/]*$/,"/browser.css");
		(h.getElementsByTagName("base")[0]).href = baseHref;
	}

	CkEditorImageBrowser.ckFunctionNum = CkEditorImageBrowser.getQueryStringParam('CKEditorFuncNum');

	CkEditorImageBrowser.initEventHandlers();

	CkEditorImageBrowser.initLoadData(
		CkEditorImageBrowser.getQueryStringParam('listUrl'),
		CkEditorImageBrowser.getQueryStringParam('custom_source')
	);
};

CkEditorImageBrowser.initLoadData = function(url, custom_source) {
	CkEditorImageBrowser.loadMediaData(url, function(){
		CkEditorImageBrowser.renderImages('media');
	});
	if ('undefined' !== typeof custom_source) {
		if (custom_source = JSON.parse(custom_source)) {
			CkEditorImageBrowser.loadCustomSourceData(url, custom_source, function(){
				CkEditorImageBrowser.renderImages('custom_source');
			});
		}
	}
};

CkEditorImageBrowser.loadMediaData = function (url, onLoaded) {
	CkEditorImageBrowser.images = [];

	var pageId = CkEditorImageBrowserPage > 1 ? '?page=' + CkEditorImageBrowserPage : '';

	$.getJSON(url + pageId, function (currPage) {
		$.each(currPage.media, function (_idx, item) {
			if (typeof(item.thumb) === 'undefined') {
				item.thumb = item.image;
			}
			CkEditorImageBrowser.addImage('media', item.image, item.thumb, item.json);
		});
		var $pagination_container = $('#pagination-container');
		$pagination_container.html(currPage.pagination);
		$pagination_container.find('a').attr('href', '#').on('click', function(e){
			e.preventDefault();
			var $parent_li = $(this).closest('li[data-page]');
			if ('undefined' !== $parent_li && $parent_li.length === 1) {
				CkEditorImageBrowserPage = parseInt($parent_li.data('page'));
				CkEditorImageBrowser.loadMediaData(
					CkEditorImageBrowser.getQueryStringParam('listUrl'),
					function () {
						CkEditorImageBrowser.renderImages('media');
					}
				);
			}
		});

		onLoaded();
	}).error(function(jqXHR, textStatus, errorThrown) {
		var errorMessage;
		if (jqXHR.status < 200 || jqXHR.status >= 400) {
			errorMessage = 'HTTP Status: ' + jqXHR.status + '/' + jqXHR.statusText + ': <strong style="color: red;">' + url + '</strong>';
		} else if (textStatus === 'parsererror') {
			errorMessage = textStatus + ': invalid JSON: <strong style="color: red;">' + url + '</strong>: ' + errorThrown.message;
		} else {
			errorMessage = textStatus + ' / ' + jqXHR.statusText + ' / ' + errorThrown.message;
		}
		CkEditorImageBrowser.$imagesContainer.html(errorMessage);
    });
};

CkEditorImageBrowser.loadCustomSourceData = function (url, custom_source, onLoaded) {
	CkEditorImageBrowser.images_custom_source = [];

	if ('undefined' !== typeof custom_source.name && 'undefined' !== typeof custom_source.ids && $.trim(custom_source.ids)) {
		$('#custom_source_tab').text(custom_source.name);
		$.getJSON(url + '/' + custom_source.ids, function (media_items) {
			$.each(media_items.media, function (_idx, item) {
				if (typeof(item.thumb) === 'undefined') {
					item.thumb = item.image;
				}
				CkEditorImageBrowser.addImage('custom_source', item.image, item.thumb, item.json);
			});
			onLoaded();
		}).error(function(jqXHR, textStatus, errorThrown) {
			var errorMessage;
			if (jqXHR.status < 200 || jqXHR.status >= 400) {
				errorMessage = 'HTTP Status: ' + jqXHR.status + '/' + jqXHR.statusText + ' : <strong style="color: red;">' + url + '</strong>';
			} else if (textStatus === 'parsererror') {
				errorMessage = textStatus + ': invalid JSON: <strong style="color: red;">' + url + '</strong>: ' + errorThrown.message;
			} else {
				errorMessage = textStatus + ' / ' + jqXHR.statusText + ' / ' + errorThrown.message;
			}
			CkEditorImageBrowser.$images_customContainer.html(errorMessage);
		});
	}
};

CkEditorImageBrowser.addImage = function (type, imageUrl, thumbUrl, imageJson) {
	var obj = {
		'imageUrl' : imageUrl,
		'thumbUrl' : thumbUrl,
		'imageJson': imageJson
	};
	if (type == 'media') {
		CkEditorImageBrowser.images.push(obj);
	} else if (type == 'custom_source') {
		CkEditorImageBrowser.images_custom_source.push(obj);
	}
};

CkEditorImageBrowser.renderImages = function(type) {
	var images = null,
		$images_container = null,
		templateHtml = $('#js-template-image').html();

	if (type == 'media') {
		images = CkEditorImageBrowser.images;
		$images_container = CkEditorImageBrowser.$imagesContainer;
	} else if (type == 'custom_source') {
		images = CkEditorImageBrowser.images_custom_source;
		$images_container = CkEditorImageBrowser.$images_customContainer;
	}

	if (images) {
		$images_container.html('');

		$.each(images, function (_idx, imageData) {
			var html = templateHtml;
			html = html.replace('%imageUrl%', imageData.imageUrl);
			html = html.replace('%thumbUrl%', imageData.thumbUrl);
			html = html.replace('%imageInfo%', JSON.stringify(imageData.imageJson));
			var $item = $($.parseHTML(html));
			$images_container.append($item);
		});
	}
};

CkEditorImageBrowser.initEventHandlers = function () {
	$(document).on('click', '.js-image-link', function(e) {
		e.preventDefault();
		var $info_col = $('#info_col');
		if (!$info_col.is(':visible')) {
			$('#browser_col').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12').addClass('col-lg-10 col-md-9 col-sm-9 col-xs-8');
			$info_col.fadeIn();
		}
		var json = $(this).data('info');
		var $image_info = $('#image_info');
		$image_info.find('h5').text(json.info.filename_orig);
		$image_info.find('img').attr('src', json.info.src);
		var $image_crops = $image_info.find('.image_crops');
		$image_crops.html('');
		for (var i = 0, l = json.crops.length; i < l; i++) {
			var image_crop = json.crops[i];
			$image_crops
				.append(
					$('<span/>')
						.addClass('btn btn-primary btn-xs')
						.text(image_crop.dimensions)
						.data('src', image_crop.src)
						.click(function(){
							window.opener.CKEDITOR.tools.callFunction(CkEditorImageBrowser.ckFunctionNum, $(this).data('src'));
							window.close();
						})
				)
				.append(
					document.createTextNode(' ')
				);
		}
	});
};

CkEditorImageBrowser.getQueryStringParam = function (name) {
	var regex = new RegExp('[?&]' + name + '=([^&]*)'),
		result = window.location.search.match(regex);

	return (result && result.length > 1 ? decodeURIComponent(result[1]) : null);
};
