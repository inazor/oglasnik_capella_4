﻿/**
 * List all available image crop styles for selected image.
 */

'use strict';

(function() {
    CKEDITOR.plugins.add('imagecrops', {
        requires: 'dialog,image',
        init: function(editor) {
            CKEDITOR.on('dialogDefinition', function(ev) {
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                var currDialog = dialogDefinition.dialog;

                if (dialogName == 'image') {
                    dialogDefinition.addContents({
                        accessKey: 'C',
                        id: 'crops',
                        label: 'Available styles',
                        title: 'Available styles',
                        elements: [
                            {
                                type : 'html',
                                html : '<div><strong>Available image styles</strong><div id="availableImageCrops"></div></div>'
                            }
                        ]
                    });

                    currDialog.on('selectPage', function(clickedObj){
                        var currPageTab = clickedObj.data.page;
                        var $available_crops = $('#availableImageCrops');
                        if (currPageTab == 'crops') {
                            var imageUrl = currDialog.getValueOf('info', 'txtUrl');
                            $available_crops.html('');
                            if ('undefined' !== typeof imageUrl && $.trim(imageUrl)) {
                                $.getJSON('/admin/media/styles?url=' + imageUrl, function(mediaCrops) {
                                    $.each(mediaCrops, function(i, item) {
                                        var divClass = '';
                                        if (imageUrl === item.src) {
                                            divClass = 'current';
                                        }
                                        $available_crops.append(
                                            $('<div/>')
                                                .addClass('imageCrop')
                                                .append(
                                                    $('<div/>')
                                                        .addClass(divClass)
                                                        .append(
                                                            $('<div/>')
                                                                .addClass('dimensions')
                                                                .append(
                                                                    $('<strong/>')
                                                                    .text(item.width + ' x ' + item.height)
                                                                )
                                                        )
                                                        .append(
                                                            $('<div/>')
                                                                .addClass('image')
                                                                .append(
                                                                    $('<img/>')
                                                                        .attr('src', item.src)
                                                                )
                                                        )
                                                        .append(
                                                            $('<div/>')
                                                                .addClass('clearfix')
                                                                .html('<!--IE-->')
                                                        )
                                                        .click(function(){
                                                            currDialog.setValueOf('info', 'txtUrl', $(this).find('img').attr('src'))
                                                            $available_crops.find('div.current').removeClass('current');
                                                            $(this).addClass('current');
                                                        })
                                                )
                                        );
                                    });
                                });
                            } else {
                                $available_crops.html('No available styles!');
                            }
                        }
                    });
                }
            });
        }
    });
})();
