$(document).ready(function(){
    if (jQuery().masonry && $('.masonry-articles .masonry-item').length) {
        $('.masonry-articles').masonry({
            itemSelector: '.masonry-item',
        });
        $('.masonry-articles').imagesLoaded().progress(function(){
            $('.masonry-articles').masonry('layout');
        });
    }
});
