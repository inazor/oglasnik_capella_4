jQuery(document).ready(function(){
    jQuery('#birth_date').dateDropdowns({
        daySuffixes: false,
        minAge: parseInt(jQuery('#birth_date').data('minage')),
        defaultValue: jQuery('#birth_date').val(),
        dropdownClass: 'form-control date-select',
        wrapperClass: 'date-dropdowns',
        submitFieldName: 'birth_date'
    });
    // bind country dropdown change
    jQuery('#country_id').change(function(){
        jQuery('#county_id').find('option').remove();
        var county_div = jQuery('#county_id').closest('.form-group');
        var selected_country_id = jQuery(this).val();
        var selected_country_calling_code = jQuery(this).find('option:selected').data('calling-code');
        if ('' !== selected_country_id) {
            jQuery.get(
                '/ajax/counties/' + selected_country_id,
                function (data) {
                    if (data.length) {
                        jQuery.each(data, function(idx, data){
                            jQuery('#county_id').append(jQuery('<option/>').attr('value', data.id).text(data.name));
                        });
                        if (!county_div.is(':visible')) {
                            county_div.fadeIn();
                        }
                    } else {
                        if (county_div.is(':visible')) {
                            county_div.fadeOut();
                        }
                    }
                    jQuery('#county_id').selectpicker('refresh');
                },
                'json'
            );
        } else {
            county_div.fadeOut();
            jQuery('.foreign-input .input-group-addon[data-country-code]')
                .attr('data-country-code', '+')
                .data('country-code', '+')
                .text('+');
        }
    });
    jQuery('#frm_user_details select').selectpicker({
        style: 'btn btn-default',
        size: 10,
        noneSelectedText: '',
        liveSearch: true,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        dropupAuto: false
    });

    jQuery('input.phone-number-handler').phoneNumberHandler();
});
