var googleMapObject = null;

jQuery(document).ready(function() {
    $('#frm_modal_contact').ajaxifyModalForm();
    $('#shop_classifieds').filterizeForm();
    $('.classifieds-categories-listing-sidebar').sidebardCategoriesDrillDown();

    if ($('#modal-inspect-user-map').length) {
        $('#modal-inspect-user-map').on('show.bs.modal', function() {
            if (!googleMapObject) {
                googleMapObject = $('#googleMapContainer').initGoogleMap({
                    'add_marker': true
                });
            }
        });
    }

    $('.shop-details').tabbifyClassifiedsDetails({
        instanceName : 'tabby',
        //mobileBreakPoint: 1338,
        mobileBreakPoint: 1278,
        selectors : {
            header : '.convert-to-tab-xs',
            body : '.listings-main-col',
            bodyP : '> .excerpt'
        }
    });
});
