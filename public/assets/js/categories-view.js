function run_paid_carousel($container) {
    var $box_titles = $container.find('.ad-box-title');
    $box_titles.append($('<div class="slick-pager"></div>'));

    var markup_mobile = modify_carousel_markup($container);

    var markup = '';
    var pages = [];
    var page = [];
    var perPage = 3;
    var $paidAds = $container.find('.ad-box:not(.ad-box-title)');
    var paidAdsCount = $paidAds.length;

    $paidAds.each(function(i, elem){
        page.push(elem);
        if (page.length == perPage || i == (paidAdsCount - 1)) {
            pages.push(page);
            page = [];
        }
    });

    $(pages).each(function(i, page) {
        markup += '<div class="category-fullrow-layout category-fullrow-layout-page">';
        $(page).each(function(j, ad) {
            markup += ad.outerHTML;
        });
        markup += '</div>';
    });

    if (paidAdsCount) {
        markup = $box_titles.get(0).outerHTML + '<div class="paid-ads-carousel">' + markup + '</div>';

        $container.html(markup);

        // Shove mobile markup after the given container
        $container.after('<div id="paid-ads-carousel-mobile">' + $box_titles.get(0).outerHTML + '<div class="paid-ads-carousel-mobile">' + markup_mobile + '</div></div>');
    }

    function modifyLazyLoadingMarkup($container) {
        $container.find('.image-wrapper').each(function(i,el){
            var $el = jQuery(el);
            var src = $el.data('src');
            if (src) {
                $el.css({'background-image': 'url(' + src + ')'});
                $el.removeAttr('data-src');
                $el.removeClass('lazy');
            }
        });
        return $container;
    }

    // Build desktop slick
    var $slick_pager = $container.find('.ad-box-title > .slick-pager');
    var slick_options = {
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        slide: '.category-fullrow-layout-page',
        slidesToShow: 1,
        slidesToScroll: 1,
        appendArrows: $slick_pager,
        appendDots: $slick_pager
    };
    var $slides_container = $container.find('.paid-ads-carousel');
    $slides_container = modifyLazyLoadingMarkup($slides_container);
    $slides_container.slick(slick_options);

    // Build slick for mobiles
    var $container_mobile = $('#paid-ads-carousel-mobile');
    var $mobile_slides_container = $container_mobile.find('.paid-ads-carousel-mobile');
    var $slick_mobile_pager = $container_mobile.find('.ad-box-title > .slick-pager');
    var slick_options_mobile = {
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        slide: '.ad-box',
        slidesToShow: 1,
        slidesToScroll: 1,
        appendArrows: $slick_mobile_pager,
        appendDots: $slick_mobile_pager
    };
    $mobile_slides_container = modifyLazyLoadingMarkup($mobile_slides_container);
    $mobile_slides_container.slick(slick_options_mobile);
}

var modify_carousel_markup = function($container) {
    // Server should already have basic mobile markup returned, so grab that
    var markup_mobile = $container.get(0).outerHTML;
    markup_mobile = markup_mobile.replace('id="paid-ads-carousel"', 'id="paid-ads-carousel-copy"');
    // Remove box-title div from mobile copy
    var $markup_mobile = $(markup_mobile);
    $markup_mobile.find('.ad-box-title').remove();
    // Unwrap the ads from the wrapper div
    var $markup_mobile_ads = $markup_mobile.find('.ad-box');
    markup_mobile = '';
    $markup_mobile_ads.each(function(i, ad){
        markup_mobile += ad.outerHTML;
    });

    return markup_mobile;
};

var upsell_clicks = function() {
    jQuery('.up-sell-box').on('click', function(evt) {
        var $el = jQuery(this);
        var href = $el.data('link');
        if (href) {
            window.location = href;
        }
        evt.preventDefault();
        evt.stopPropagation();
    });
};

jQuery(document).ready(function() {
    jQuery('#category_filters').filterizeForm();
    var $carousel_container = jQuery('#paid-ads-carousel');
    if ($carousel_container.length > 0) {
        run_paid_carousel($carousel_container);
    }
    upsell_clicks();
});
