$(document).ready(function(){
    /*
    TODO: Add checking for mobile devices and old browsers that might have problems with this approach and let them
    upload images the old-fashioned way...
     */
    var $jsUploadForm = $('#frm_avatar_coverbg_uploads');
    if ($jsUploadForm.length) {
        $('a[data-field="avatar"], a[data-field="coverbg"]').click(function(e){
            e.preventDefault();
            $jsUploadForm.attr('action', $(this).attr('href'));
            var $uploadBtn = $jsUploadForm.find('#avatarCoverBgField');
            $uploadBtn.attr('name', $(this).data('field'));
            $uploadBtn.change(function(){ $jsUploadForm.submit(); });
            $uploadBtn.click();
        });
    }
});
