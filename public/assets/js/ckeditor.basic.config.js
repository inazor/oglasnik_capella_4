/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.language = 'hr';
    config.height = 300;
    config.autoGrow_minHeight = 300;
    config.autoGrow_onStartup = true;
    config.autoGrow_bottomSpace = 50;
    config.dataIndentationChars = '    ';
    config.entities = false;
    config.startupOutlineBlocks = true;
    config.contentsLanguage = 'hr';
    config.format_tags = 'p;h2;h3;h4';
    config.pasteFilter = 'plain-text';
    config.forcePasteAsPlainText = true;
    config.pasteFromWordPromptCleanup = true;
    config.toolbar = [
        { name: 'formatting', groups: [ 'styles', 'basicstyles' ], items: [ 'Format', '-', 'Bold', 'Italic', 'Underline' ] },
        { name: 'paragraph', groups: [ 'list', 'align' ], items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
        { name: 'links', items: [ 'Link', 'Unlink' ] },
    ];
    config.toolbarGroups = [
        { name: 'formatting', groups: [ 'styles', 'basicstyles' ] },
        { name: 'paragraph', groups: [ 'list', 'align' ] },
        { name: 'links' }
    ];
    config.allowedContent = {
        'p h2 h3 h4': { styles: 'text-align' },
        'a' : { attributes: '!href' },
        'ul ol li strong em u': true
    };
};
