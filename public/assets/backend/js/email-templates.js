$(document).ready(function(){
    var editor = ace.edit('tpleditor');
    editor.setTheme('ace/theme/monokai');
    editor.getSession().setMode('ace/mode/twig');
    editor.getSession().setUseWrapMode(true);

    var $window = $(window);
    var $template_dropdown = $('#template_id');
    var $form = $('#frm-tpleditor');
    var $form_tplchooser = $('#frm-tplchooser');

    var editor_to_hidden = function() {
        $('#code').val(editor.getSession().getValue());
    };

    var confirm_changes = function(){
        if ($form.hasChanges()) {
            return confirm('Unsaved changes detected, are you sure you wish to load a different template?');
        } else {
            return true;
        }
    };

    // Propagate editor updates to our hidden field
    editor.on('change', function(){
        editor_to_hidden();
    });

    // Monitor the form with the hidden field for changes
    $form.changeWatcher();

    // Alert on navigating away if there are any unsaved changes (unless a submit happens)
    $window.on('beforeunload', function(e){
        if ($form.hasChanges()) {
            return 'Unsaved changes detected, continue anyway?';
        }
    });

    // On editor form submit
    $form.on('submit', function(){
        editor_to_hidden();
        $window.off('beforeunload');
        return true;
    });

    // Keeping track of currently selected dropdown value so that
    // we can undo it if the user cancels the confirm()
    $template_dropdown.data('current', $template_dropdown.val());

    // Submit the template dropdown loader form after a successful change
    $template_dropdown.on('change', function(){
        var $this = $(this);
        var ok = confirm_changes();
        if (!ok) {
            // "undo" the change event
            $template_dropdown.selectpicker('val', $this.data('current'));
        } else {
            // Submit the form
            $this.data('current', $this.val());
            $window.off('beforeunload');
            $form_tplchooser.trigger('submit');
        }
    });

    // Build the selectpicker
    $template_dropdown.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        selectedTextFormat: 'count>3',
        title: 'Select email template',
        noneSelectedText: 'Select email template',
        noneResultsText: 'No email template match',
        mobile: OGL.is_mobile_browser
    });
});
