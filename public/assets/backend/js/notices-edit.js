$(document).ready(function(){
    var $received_at = $('#received_at');

    if ($.trim($received_at.val())) {
        $received_at.val(moment($received_at.val()).format('DD.MM.YYYY HH:mm'));
    }
    $received_at.datetimepicker();
    $('textarea#message').ckeditor({
        customConfig: '/assets/backend/js/ckeditor_basic_config.js'
    });
});
