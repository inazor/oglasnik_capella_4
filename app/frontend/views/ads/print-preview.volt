<!DOCTYPE html>
<html class="no-js" lang="hr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{ getTitle() }}
    <script>
        document.documentElement.className = document.documentElement.className.replace('no-js', 'js');
    </script>
    <style type="text/css">
        * { margin:0; padding:0; }
    </style>
    {{ this.assets.outputCss() }}
{#
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/base.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('assets/css/print.css') }}">
#}
{% if curr_env == 'production' -%}
    <!-- GoogleAnalytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-2093500-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- Hotjar Tracking Code for http://www.oglasnik.hr -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:287193,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
{%- endif %}
</head>
<body>
        <div class="container">
            <a href="{{ this.url.get(null) }}"><img src="{{ this.url.get('/assets/img/logo-header.svg') }}" width="120" height="30" alt="oglasnik"></a>
            <div class="row">
                <div class="col-xs-10">
                    <div class="title-single">
                        <h1>{{ ad.title|striptags|truncate(80) }}</h1>
                    </div>
                    <div class="row details">
                        <div class="col-xs-4">
                            Šifra oglasa: <span class="blue">{{ ad.id }}</span><br>
                            {% set ad_price = ad.getPrices() -%}
                            {% if ad_price %}
                                {% if ad_price['main'] > 0 -%}
                                    <span class="price-oglas-details">cijena: {{ ad_price['main'] }}</span>
                                    {% if ad_price['other'] %}<br>približno: {{ ad_price['other'] }}{% endif %}
                                {% endif %}
                            {%- endif %}
                        </div>
                        <div class="col-xs-4">
                            {% set adPhoneNumbers = ad.getFormattedPublicPhoneNumbers() %}
                            {% if adPhoneNumbers|length %}
                                <img src="{{ url('assets/img/icn_telephone.png') }}" alt="Telefon">
                                {% for adPhoneNumber in adPhoneNumbers %}{% if adPhoneNumber['link'] is defined %}<a href="tel:{{ adPhoneNumber['link'] }}">{{ adPhoneNumber['text'] }}</a>{% else %}{{ adPhoneNumber['text'] }}{% endif %}{{ !loop.last ? ', ' : '' }}{% endfor %}
                            {% endif %}

                            {# TODO: implement location based on given address #}
                            {% set adLocationData = ad.getLocationData() %}
                            {% if adLocationData %}
                            <p class="location"><img src="{{ url('assets/img/icn_map.png') }}" alt="Lokacija"> {{ adLocationData['text'] ~ (adLocationData['gps']|default(null) ? ' <small><a href="#" class="scrollToMap">(Pogledaj na karti)</a></small>' : '')}}</p>
                            {% endif %}
                        </div>
                        <div class="col-xs-4">
                            {% if ad_user is defined and ad_user %}
                                {% set ad_user_shop = ad_user.shop %}
                                {% set avatar = ad_user.getAvatar().getSrc() %}
                                <div>
                                    <span class="border-radius overflow-hidden"><img class="avatar-header" src="{{ avatar }}" width="30" height="30" /></span>
                                    {% set users_username = (ad_user.type == 1 ? ad_user.username : ad_user.company_name) %}
                                    {% if ad_user_shop %}{% set users_username = ad_user_shop.title %}{% endif %}
                                    {{ users_username }} <small><a href="{{ ad_user.ads_page_url }}">(Prikaži sve oglase)</a></small>
                                </div>
                            {% endif %}
                        </div>
                    </div>
                </div>
                <div class="col-xs-2">
                    <img src="//chart.googleapis.com/chart?cht=qr&chs=120x120&chl={{ url('oglas/' ~ ad.id) }}" alt="QR code" />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <hr>
                    <h3>Opširnije o oglasu</h3>
                    <div class="ad-details">
                        <p class="color-deep-dark-blue-2">{{ ad.description|stripsometags|nl2br }}</p>
                    </div>
                    <hr>

                    {% for ad_section_parameters in ad_render['html']['other']|default([]) %}
                        {{ ad_section_parameters }}
                    {% endfor %}

                    {% if ad_render['map'] is defined -%}
                        <!-- hide this whole row in case map is not visible -->
                        <div class="row margin-top-sm margin-bottom-sm">
                            <div class="col-xs-12">
                                <div
                                        id="google_map_container"
                                        data-zoom="{{ ad_render['map']['zoom']|default('8') }}"
                                        data-lat="{{ ad_render['map']['lat']|default('0') }}"
                                        data-lng="{{ ad_render['map']['lng']|default('0') }}"
                                        style="height:420px;"
                                ><!--IE--></div>
                            </div>
                        </div>
                        <hr>
                    {% endif -%}

                    <div class="hidden-print">
                        <hr>
                        <span class="view-real-page btn btn-primary" data-url="{{ ad.get_frontend_view_link() }}">Pogledaj stranicu oglasa</span>
                    </div>
                </div>
                <div class="col-xs-6 side-col">
                    {% if ad_render['ad_media']|length > 0 -%}
                        <div class="gallery" id="galerija">
                            <div class="big image-big">
                                {% set first_media = ad_render['ad_media'][0] %}
                                {% set first_media_src = first_media.get_url('GalleryBig') %}
                                <img src="{{ first_media_src }}" alt="">
                            </div>
                            <ul class="thumbs">
                                {% for ad_media in ad_render['ad_media'] -%}
                                    {%- set ad_media_thumbnail = ad_media.get_url('GalleryThumb') -%}
                                    <li><img src="{{ ad_media_thumbnail }}" alt=""></li>
                                {% endfor -%}
                            </ul>
                        </div>
                    {% endif -%}
                </div>
            </div>
        </div>
        <script type="text/javascript" src=" {{ url('assets/vendor/jquery/jquery-1.11.2.min.js') }}"></script>
        <script>
            jQuery(document).ready(function() {
                $('header .logo').click(function(){
                    if (window.opener && window.opener !== window) {
                        window.close();
                    } else {
                        location.href = '{{ url('') }}';
                    }
                });
                $('span.view-real-page').click(function(){
                    if (window.opener && window.opener !== window) {
                        window.close();
                    } else {
                        location.href = $('span.view-real-page').data('url');
                    }
                });
                $('.convert-to-tab-xs').removeClass('convert-to-tab-xs');
                if (window.print) {
                    window.print();
                }
            });
        </script>
    </body>
</html>
