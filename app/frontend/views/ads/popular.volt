<section>
    <div class="container">
        <h2 class="section-title">{{ 'Popularni oglasi'|category_name_markup }}</h2>
        {% if ads is defined and ads and ads is iterable %}

            {% if banners is defined %}
                {% set banners = '<div class="text-center margin-bottom-sm banner banner-728 banner-leaderboard hidden-xs"><iframe src="' ~ ad_iframe_url ~ '" width="728" height="90" frameborder="0" scrolling="no"></iframe></div>' %}
                {% set banners = banners ~ '<div class="text-center margin-bottom-sm banner banner-rectangle visible-xs hidden-sm hidden-md hidden-lg"><iframe src="' ~ ad_iframe_url_mobile ~ '" width="300" height="250" frameborder="0" scrolling="no"></iframe></div>' %}
            {% endif %}

            {% set initially_shown_blocks_cnt = 3 %}
            <div class="row grid-layout">
            {% for i, ads_group in ads %}
                {% set block_idx = i + 1 %}
                {% set block_class = '' %}
                {% if block_idx > initially_shown_blocks_cnt %}
                    {% set block_class = ' class="hidden"' %}
                {% endif %}
                <div id="latest-ads-block-{{ block_idx }}"{{ block_class }}>
                    {% set marginBottomLg = 'margin-bottom-sm' %}
                    {% set currentIndex = 1 %}
                    {% for ad in ads_group %}
                        {% set isOdd = (currentIndex % 2 != 0) %}
                        {% include 'chunks/ads-grid-single.volt' %}
                        {% if currentIndex % 4 == 0 %}
                            <div class="clearfix"></div>
                        {% elseif currentIndex % 2 == 0 %}
                            <div class="clearfix hidden-md hidden-lg"></div>
                        {% endif %}
                        {% set currentIndex = currentIndex + 1 %}
                    {% endfor %}
                    {#{% if block_idx <= initially_shown_blocks_cnt %}#}
                    {{ banners }}
                    {#{% endif %}#}
                </div>
            {% endfor %}
            </div>

            <button data-start="{{ initially_shown_blocks_cnt }}" id="load-more-latest" class="center btn-default"><span class="loading fa fa-fw fa-spinner fa-spin hidden"></span> Prikaži još oglasa</button>
        {% endif %}
    </div>
</section>
