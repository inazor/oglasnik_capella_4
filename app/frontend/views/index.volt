{% include 'chunks/head.volt' %}
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGSS62" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- FacebookSDK/start -->
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '{{ this.config.social.fb_app_id }}',
                xfbml      : true,
                version    : 'v2.6'
            });
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/hr_HR/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- FacebookSDK/end -->

    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    {% if banners and banner_background is defined %}
        <div class="banner banner-background hidden-xs hidden-sm">{{ banner_background }}</div>
    {% endif %}

    <div id="page-mm-container" class="page-mm-container">

        {% if banners and banner_billboard is defined %}
        <div class="banner-container banner-container-billboard text-center hidden-xs hidden-md">
            {{ banner_billboard }}
        </div>
        {% endif %}

        <nav id="mobile-nav" class="mobile-navigation">{% include('chunks/mmenu') %}</nav>

        <div id="top"></div>
        <div class="site-header hidden-xs">
            <div class="container">
                <a class="homer hidden-sm" href="{{ this.url.get(null) }}"></a>
                <a href="javascript:void(0);" class="mobile-nav-trigger hidden-md hidden-lg"><span class="fa fa-navicon icon"></span><span class="mobile-nav-trigger-text">Menu</span>{% if userUnreadMsgCount > 0 %}<span class="count-pos count-text"> {{ userUnreadMsgCount }}</span>{% endif %}</a>
                <ul class="nav hidden">
                    <li class="yamm">
                        <div class="dropdown yamm-fw">
                            <a href="{{ this.url.get('categories/all') }}" class="dropdown-toggle" data-toggle="dropdown">Kategorije</a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="yamm-content homepage-detailed-category-list">
                                        <div class="row">
                                            <div class="col-md-12 blue-links">
                                                Izdvojene kategorije <a href="{{ this.url.get('categories/all') }}" class="margin-left-10px">Otvori sve kategorije</a>
                                                <hr>
                                            </div>
                                            {% if megamenu_sections_markup is defined %}
                                                {{ megamenu_sections_markup }}
                                            {% endif %}
                                            <div class="col-md-12">
                                                <hr>
                                                {#<img src="{{ url('assets/img/icn_map.png') }}" alt="Lokacija" class="margin-right-10px"><a href="{{ this.url.get('search/map') }}">Pretraži nekretnine pomoću karte <span class="fa fa-chevron-right fa-fw"></span></a>#}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Oglasi</a>
                            <ul class="dropdown-menu">
                                {#<li><a href="{{ this.url.get('search/map') }}">Pretraži nekretnine na karti</a></li>#}
                                <li><a href="{{ this.url.get('novi-oglasi') }}">Novi oglasi</a></li>
                                <li><a href="{{ this.url.get('popularni-oglasi') }}">Popularni oglasi</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>

                {% if layout_features['search_bar'] %}
                    {% set main_search_category_id   = main_search_category|default('') %}
                    {% set main_search_category_name = searchCategories[main_search_category_id]|default(null) ? searchCategories[main_search_category_id].name : 'Sve kategorije' %}
                <form action="{{ this.url.get('search') }}" class="searchform js-allow-double-submit{% if not layout_features['mobile_search_bar_visible_initially'] %} hidden-xs{% endif %}" id="main_search_form" autocomplete="off">
                    <div class="search-wrapper">
                        <div class="input-group">
                            <input name="q" id="main_search_term" type="text" class="form-control search-term default-form-control" arial-label="..." placeholder="Upišite pojam ili šifru oglasa" value="{{ main_search_term|default('')|escape_attr }}">
                            <input name="category_id" class="search-category-input" id="main_search_category" type="hidden" value="{{ main_search_category|default('') }}">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdown-button-name">{{ main_search_category_name }}</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu category-chooser">
                                    <li><a href="#"{{ '' == main_search_category_id ? 'class="active"' : '' }} data-category-id="">Sve kategorije</a></li>
                                    {% for searchCategory in categories %}
                                    <li><a href="#"{{ searchCategory.id == main_search_category_id ? 'class="active"' : '' }} data-category-id="{{ searchCategory.id }}" data-category-href="{{ this.url.get(searchCategory.url) }}">{{ searchCategory.name }}</a></li>
                                    {% endfor %}
                                </ul>
                            </div>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default btn-search"><span class="fa fa-search"></span></button>
                            </span>
                        </div>
                    </div>
                </form>
                {% endif %}

                <div class="pull-right header-right">
                    {% if auth.logged_in() %}
                    <div class="user-menu-wrapper dropdown hidden-xs hidden-sm">
                        {% set user = auth.get_user() %}
                        {% set avatar = user.getAvatar().setClass('avatar-header img-circle').setWidth(39).setHeight(39) %}
                        <button type="button" class="btn btn-default dropdown-toggle user-cp background-transparent" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="username">{{ user.username }}</span>
                            {{ avatar }}{% if userUnreadMsgCount > 0 %}<span class="count-pos count-text"> {{ userUnreadMsgCount }}</span>{% endif %}
                        </button>
                        <ul class="dropdown-menu">
                            {% if auth.logged_in(['admin','supersupport','support','finance','moderator','content','sales']) %}
                            <li><a href="{{ this.url.get('admin') }}">Admin</a></li>
							<li><a href="{{ this.url.get('suva') }}">SUVA</a></li>
                            {% endif %}
                            <li><a href="{{ this.url.get('moj-kutak/podaci') }}">Postavke profila</a></li>
                            <li><a href="{{ this.url.get('moj-kutak/svi-oglasi') }}">Moji oglasi</a></li>
                            <li><a href="{{ this.url.get('moj-kutak/spremljeni-oglasi') }}">Spremljeni oglasi</a></li>
                            <li><a href="{{ this.url.get('moj-kutak/poruke') }}">Poruke{% if userUnreadMsgCount > 0 %}<span class="count-text"> {{ userUnreadMsgCount }}</span>{% endif %}</a></li>
                            <li><a href="{{ this.url.get('info/pomoc') }}">Pomoć</a></li>
                            <li><a href="{{ this.url.get('user/signout') }}" class="color-secondary-red">Odjava</a></li>
                        </ul>
                    </div>
                    {% else %}
                    <p class="login-register-buttons hidden-xs hidden-sm">
                        <a href="{{ this.url.get('user/signin') }}">Prijava</a> |
                        <a class="active" href="{{ this.url.get('user/signup') }}">Registracija</a>
                    </p>
                    {% endif %}
                    <a href="{{ this.url.get('predaja-oglasa') ~ (submit_ad_category ? '?category_id=' ~ submit_ad_category : '') }}" class="btn light-blue predaj-oglas">Predaj oglas</a>
                </div>
            </div>
            <div class="subHeader">
                <div class="container">
                    {% if subHeaderLinks_contents|default(null) %}
                    <div class="pull-left"><ul class="links">{{ subHeaderLinks_contents }}</ul></div>
                    {% endif %}
                    <div class="pull-right">
                        <ul class="links social">
                            <li><a href="https://www.facebook.com/plavioglasnik/" target="_blank"><span class="fa fa-facebook fa-fw"></span></a></li>
                            <li><a href="https://www.instagram.com/oglasnik.hr/" target="_blank"><span class="fa fa-instagram fa-fw"></span></a></li>
                            <li><a href="https://twitter.com/oglasnik" target="_blank"><span class="fa fa-twitter fa-fw"></span></a></li>
                            <li><a href="https://plus.google.com/+oglasnik" target="_blank"><span class="fa fa-google-plus fa-fw"></span></a></li>
                            <li><a href="https://www.linkedin.com/company/oglasnik-d.o.o." target="_blank"><span class="fa fa-linkedin fa-fw"></span></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"><!--IE--></div>
                </div>
            </div>
        </div>

        <div class="header-mobile hidden-sm hidden-md hidden-lg">
            <div class="top-row">
                <a href="javascript:void(0);" class="mobile-nav-trigger"><span class="fa fa-navicon icon"></span>{% if userUnreadMsgCount > 0 %}<span class="count-pos count-text"> {{ userUnreadMsgCount }}</span>{% endif %}</a>
                <a class="homer" href="{{ this.url.get(null) }}"></a>
{#
                <a class="login" href="{{ this.url.get('user/signin') }}">Prijava</a>
#}
                <span class="after-homer">
                    <a href="{{ this.url.get('predaja-oglasa') ~ (submit_ad_category ? '?category_id=' ~ submit_ad_category : '') }}" class="visible-md visible-lg hidden-xs btn light-blue predaj-oglas">Predaj oglas</a>
                    {% if layout_features['search_bar'] and layout_features['mobile_search_bar_toggler'] %}
                    <a href="#search-xs" class="visible-xs-inline-block js-search-toggle{% if layout_features['mobile_search_bar_visible_initially'] %} is-active{% endif %}"><span class="fa fa-search"></span></a>
                    {% endif %}
                </span>
            </div>
            {% if layout_features['search_bar'] %}
            <div class="search-wrapper">
                <form action="{{ this.url.get('search') }}" class="searchform js-allow-double-submit{% if not layout_features['mobile_search_bar_visible_initially'] %} hidden-xs{% endif %}" id="search-xs" autocomplete="off">
                    <div class="input-group input-group-sm">
                        <input name="q" id="search-xs-term" type="text" class="form-control search-term" placeholder="Pojam ili šifra oglasa" value="{{ main_search_term|default('')|escape_attr }}">
                        <span class="input-group-addon category-addon">
                            <select name="category_id" id="mobile-search-category">
                                <option value=""{{ '' == main_search_category_id ? ' selected' : '' }}>Sve kategorije</option>
                                {% for searchCategory in categories %}
                                <option value="{{ searchCategory.id }}"{{ searchCategory.id == main_search_category_id ? ' selected' : '' }}>{{ searchCategory.name }}</option>
                                {% endfor %}
                            </select>
                        </span>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><span class="fa fa-search icon"></span></button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="visible-xs hidden-md hidden-lg">
                <a href="{{ this.url.get('predaja-oglasa') ~ (submit_ad_category ? '?category_id=' ~ submit_ad_category : '') }}" class="btn btn-sm light-blue predaj-oglas predaj-oglas-large">Predaj oglas</a>
            </div>
            {% endif %}
        </div>

        {% if not layout_features['full_page_width'] %}
        <div class="container no-padding-xs">
        {% endif %}

            {% if banners and banner_mobile_top is defined %}
            <div class="text-center banner banner-mobile-top margin-top-sm visible-xs">{{ banner_mobile_top }}</div>
            {% endif %}

            <div class="site-wrapper position-relative pad-xs-only-lr">
                {% if layout_features['full_page_width'] %}
                    <div class="container">
                {% endif %}
                {{ flashSession.output() }}
                {% if layout_features['full_page_width'] %}
                    </div>
                {% endif %}

                {{ content() }}
            </div>
        {% if not layout_features['full_page_width'] %}
        </div> <!-- end .container -->
        {% endif %}

        {% include 'chunks/footer.volt' %}

    </body>
</html>
