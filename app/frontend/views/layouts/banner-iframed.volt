<!DOCTYPE html>
<html class="no-js" lang="hr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
    <style type="text/css">
        html,body { margin:0; padding:0; }
    </style>
</head>
<body class="iframed-ad">
    {{ content() }}
<script type="text/javascript" charset="utf-8" src="//ad.adverticum.net/g3.js"></script>
</body></html>
