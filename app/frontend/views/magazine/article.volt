{% set main_pic_thumb = null %}
{% if not (article_media is empty) and article_media.valid() %}
{% set main_pic = article_media[0] %}
{% set main_pic_thumb = main_pic.get_thumb('CMS-800x450') %}
{% endif %}
<div class="margin-top-md">
    <div class="row">
        <div class="main-col col-lg-9 article">
            {% if main_pic_thumb %}
            <img width="100%" src="{{ main_pic_thumb.getSrc() }}" />
            {% endif %}
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    {% include 'chunks/breadcrumbs.volt' %}

                    <h1 class="margin-top-0 no-bottom-margin">{{ article.title }}</h1>
                    <p class="article-date no-top-margin">{{ article.publish_date|day_date }}</p>
                    {% if article.excerpt %}<div class="excerpt">{{ article.excerpt }}</div>{% endif %}
                    {{ article.content }}

                    <div class="social-buttons margin-top-lg">
                        <a href="#" class="social-btn twitter" alt="Twitter"></a>
                        <a href="#" class="social-btn facebook" alt="Facebook"></a>
                        <a href="#" class="social-btn googleplus" alt="Google+"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="side-col col-lg-3">
            {% if latest_articles is defined and latest_articles and latest_articles is iterable %}
            {{ partial('chunks/cms-popular-articles', ['title':'Najčitanije vijesti', 'articles':latest_articles]) }}
            {% endif %}

            {% if banners and banner_home_300x250 is defined %}
            <div class="banner sidebar-rectangle-holder banner-rect-articles hidden-sm hidden-xs">{{ banner_home_300x250 }}</div>
            {% endif %}
        </div>
    </div>
</div>

{% if simmilar_articles is defined and simmilar_articles and simmilar_articles is iterable %}
<div class="container margin-top-lg">
    <div class="position-relative">
        <h2 class="section-title">Povezane <b>vijesti</b></h2>
    </div>
    {{ partial('chunks/cms-articles', ['articles':simmilar_articles]) }}
</div>
{% endif %}
