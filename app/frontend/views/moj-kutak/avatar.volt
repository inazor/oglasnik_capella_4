{# Avatar upload #}

{{ form(NULL, 'id': 'frm_user_uploads', 'method': 'post', 'enctype': 'multipart/form-data', 'autocomplete': 'off') }}
    {{ hiddenField('_csrftoken') }}
    <div class="border-radius border-dark margin-bottom-sm">
        <div class="box-header">
            <h2 class="margin-0">Uređivanje avatara</h2>
        </div>
        <div class="box-body">
            <p>Maksimalna veličina avatar slike je <strong>200Kb</strong>, podržani formati su: <strong>JPG, PNG, GIF</strong>.</p>
            <p>Finalne dimenzije avatar slike: <strong>200 x 200 piksela</strong>.</p>
            <div class="form-group{{ not(errors is empty) ? ' has-error' : '' }}">
                <input type="file" id="avatar" name="avatar">
                {% if not (errors is empty) %}
                    {%- for err in errors -%}
                        <p class="help-block">{{ err }}</p>
                    {%- endfor -%}
                {% endif %}
            </div>
            <div>
                <button class="btn btn-primary" type="submit">Pošalji <span class="fa fa-upload fa-fw"></span></button>
            </div>
        </div>
    </div>
{{ endForm() }}
