{{ form(NULL, 'id':'frm_shop', 'method':'post', 'autocomplete':'off') }}
{% if shop is defined and shop %}
    {{ hiddenField(['action', 'value':'edit']) }}
    <div>
        <ul id="moj-kutak-trgovina-tabs" class="nav nav-tabs margin-top-0" role="tablist">
            <li role="presentation" class="active"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">Trgovina</a></li>
            <li role="presentation"><a href="#izlozi" aria-controls="izlozi" role="tab" data-toggle="tab">Izlozi</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="about">
                <div class="border-radius border-dark border-top-none margin-bottom-sm">
                    <div class="box-header">
                        <div class="pull-right">
                            <button class="width-full" type="submit" name="save">Spremi izmjene</button>
                        </div>
                        Status: {{ shop_expiry_data }}
                        <div class="clearfix"><!--IE--></div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-md-8">
                                        {% set field = 'title' %}
                                        <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                            <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                                                Naziv trgovine
                                                <abbr title="Obavezno polje">*</abbr>
                                            </label>
                                            {{ textField([field, 'class':'form-control', 'placeholder':'Moja trgovina d.o.o.', 'value':(_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) ]) }}
                                            {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                            {% endif %}
                                        </div>
                                        {% set field = 'subtitle' %}
                                        <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                            <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                                                Podnaslov trgovine
                                            </label>
                                            {{ textField([field, 'class':'form-control', 'placeholder':'Tehnička oprema', 'value':(_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) ]) }}
                                            {% if errors is defined and errors.filter(field) %}
                                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                            {% endif %}
                                        </div>
                                        {% set field = 'slug' %}
                                        <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                            <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                                                Slug
                                                <abbr title="Obavezno polje">*</abbr>
                                            </label>
                                            <div class="input-group">
                                                <span class="input-group-addon">{{ url('trgovina/') }}</span>
                                                {{ textField([field, 'class':'form-control', 'placeholder':'moja-trgovina-doo', 'value':(_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) ]) }}
                                            </div>
                                            {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                            {% else %}
                                            <p class="help-block">Minimalno 3, maksimalno 64 znaka, bez razmaka (dozvoljen samo minus '-')</p>
                                            {% endif %}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        {% set field = 'default_sorting_order' %}
                                        <div class="form-group">
                                            <label for="{{ field }}" class="control-label">Zadani način sortiranja oglasa</label>
                                            <div class="icon_dropdown">{{ default_sorting_order_dropdown }}</div>
                                        </div>
                                        {% set field = 'email' %}
                                        <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                            <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                                                Email adresa
                                            </label>
                                            {{ textField([field, 'class':'form-control', 'placeholder':'info@mojatrgovina.hr', 'value':(_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) ]) }}
                                            {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                            {% else %}
                                            <p class="help-block text-small">(ako je različita od {{ user.email }})</p>
                                            {% endif %}
                                        </div>
                                        {% set field = 'web' %}
                                        <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                            <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                                                Web adresa
                                            </label>
                                            {{ textField([field, 'class':'form-control', 'placeholder':'http://www.mojatrgovina.hr', 'value':(_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) ]) }}
                                            {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                {% set field = 'about' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                                        O trgovini
                                        <abbr title="Obavezno polje">*</abbr>
                                    </label>
                                    {{ textArea([field, 'class':'form-control', 'rows':5, 'cols':40, 'value':(_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) ]) }}
                                    {% if errors is defined and errors.filter(field) %}
                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                    {% endif %}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="izlozi">
                <div class="border-radius border-dark border-top-none margin-bottom-sm">
                    <div class="box-header text-right">
                        <div class="pull-right">
                            <a href="{{ url('moj-kutak/trgovina/izlog/novi') }}" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> Novi izlog</a>
                        </div>
                        <div class="clearfix"><!--IE--></div>
                    </div>
                    <div class="box-body">
                        {% if featured_shops is defined and featured_shops is iterable %}
                        {% for featured_shop in featured_shops %}
                            <div class="row{{ !loop.last ? ' margin-bottom-2em' : '' }}" data-window-id="{{ featured_shop.id }}">
                                <div class="col-md-6">
                                    {% set shop_preview = featured_shop.buildShoppingWindowMarkup(shop, true) %}
                                    <div class="featured-shops-box featured-shops-box-preview">
                                        {{ shop_preview }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="well well-sm">
                                        <div class="status">
                                            {% set status_array = featured_shop.getStatusArray() %}
                                            <strong>Status izloga:</strong> <span class="{{ status_array['class'] }}">{{ status_array['text'] }}</span>
                                        </div>

                                        {% if (featured_shop.created_at > 0) %}
                                        <div class="created">
                                            <strong>Stvoren:</strong> <span>{{ date('d.m.Y. H:i', featured_shop.created_at) }}</span>
                                        </div>
                                        {% endif %}

                                        {% set timeframe = featured_shop.getTimeframe('d.m.Y') %}
                                        {% if timeframe %}
                                            <div class="timeframe">
                                                <strong>Trajanje:</strong> <span>{{ timeframe }}</span>
                                            </div>
                                        {% endif %}

                                        {% set cat_data = featured_shop.getCategoriesDisplayData() %}
                                        {% if not(cat_data is empty) %}
                                            <div class="categories">
                                                <strong>Kategorije u kojima se prikazuje:</strong>
                                                {{ cat_data['markup'] }}
                                            </div>
                                        {% endif %}

                                        {{ featured_shop.getActionLinksMarkup(status_array) }}
                                    </div>
                                </div>
                            </div>
                        {% endfor %}
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
{% else %}
    {{ hiddenField(['action', 'value':'create']) }}
{% endif %}
{{ endForm() }}

<div class="modal fade" id="delete-shopping-window-confirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Zatvori"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span class="text-danger">Upozorenje</span></h4>
            </div>
            <div class="modal-body">
                <p>Jeste li sigurni da želite <span class="text-danger">nepovratno obrisati odabrani izlog</span>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ne, odustani</button>
                <a id="confirm-delete-link" href="#" class="btn btn-primary btn-danger">Da, obriši izlog</a>
            </div>
        </div>
    </div>
</div>
