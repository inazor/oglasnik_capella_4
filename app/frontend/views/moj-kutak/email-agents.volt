{# MyAccount Email Agents #}
{% include 'chunks/moj-kutak-profile-header.volt' %}

<div class="col-lg-3 col-md-3">
    {{ partial('moj-kutak/menu') }}
</div>
<div class="col-lg-9 col-md-9">
    <h2>Email agenti</h2>

    {{ flashSession.output() }}

    {% if email_agents %}
    <table class="table table-condensed table-hover" id="emailAgents">
        {% for email_agent in email_agents %}
        <tr{{email_agent['active'] == 0 ? ' class="danger"' : ''}} data-email-agent-id="{{ email_agent['id'] }}">
            <td>
                <a href="{{ email_agent['view_url'] }}"><span class="fa fa-search fa-fw"></span>{{ email_agent['name'] }}</a><br/>
                <small class="text-muted">Kategorija: <strong>{{ email_agent['category'] }}</strong></small>
            </td>
            <td class="text-right" width="60"><a href="{{ url('moj-kutak/brisi-email-agenta/' ~ email_agent['id']) }}" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Briši</a></td>
        </tr>
        {% endfor %}
    </table>
    {% else %}
    <div class="alert alert-info margin-top-2em" role="alert">
        <h2>Nemate podešen niti jedan Email agent</h2>
        <p>Tekst koji objašnjava kako se stvaraju novi Email agenti.</p>
        <p>Treba otići u kategoriju koju za koju se želi primati agent, i treba popuniti filtere koji ga zanimaju, kliknuti na pretraži i kad bude zadovoljan s rezultatima treba kliknuti na gumb '<strong>Spremi kao EmailAgent</strong>' na dnu filter forme.</p>
    </div>
    {% endif %}
</div>

{% include 'chunks/moj-kutak-profile-footer.volt' %}
