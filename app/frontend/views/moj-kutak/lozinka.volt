{# MyAccount pwdchange #}

{% if show_form is defined and show_form %}
{{ form(NULL, 'id': 'frm_pwdchange', 'method': 'post', 'autocomplete': 'off') }}
    {{ hiddenField('_csrftoken') }}
    <div class="border-radius border-dark margin-bottom-sm">
        <div class="box-header">
            <h2 class="margin-0">Promjena lozinke</h2>
            <p>Za promjenu lozinke upišite svoju trenutnu lozinku i svoju novu željenu lozinku.</p>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    {% set field = 'pwd' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                            Trenutna lozinka
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ passwordField([ field, 'placeholder' : 'Trenutna lozinka', 'class': 'form-control' ]) }}
                        {% if errors is defined and errors.filter(field) %}
                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {% set field = 'pwd_new' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                            Nova željena lozinka
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ passwordField([ field, 'placeholder' : 'Nova željena lozinka', 'class': 'form-control' ]) }}
                        <p class="help-block">Minimalno 6 znakova</p>
                        {% if errors is defined and errors.filter(field) %}
                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
                <div class="col-md-6">
                    {% set field = 'pwd_new_repeat' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                            Ponovljena nova lozinka
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ passwordField([ field, 'placeholder' : 'Ponovljena nova lozinka', 'class': 'form-control' ]) }}
                        <p class="help-block">Minimalno 6 znakova</p>
                        {% if errors is defined and errors.filter(field) %}
                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center">
        <button class="width-full" type="submit">Promijeni lozinku</button>
    </div>
{{ endForm() }}
{% endif %}
