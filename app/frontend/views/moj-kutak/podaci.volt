{# MyAccount details #}

{{ form(NULL, 'id': 'frm_user_details', 'method': 'post', 'autocomplete': 'off') }}
    {{ hiddenField('_csrftoken') }}
    <div class="border-radius border-dark margin-bottom-sm">
        <div class="box-header">
            <h2 class="margin-0">Osobni podaci</h2>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="control-label">Korisničko ime</label>
                        <div class="form-control" disabled="disabled">{{ user.username }}</div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label class="control-label">E-mail adresa</label>
                        <div class="form-control" disabled="disabled">{{ user.email }}</div>
                    </div>
                </div>
            </div>
            {% set company_user = user.type == constant("Baseapp\Models\Users::TYPE_COMPANY") %}
            {% if company_user %}
            <div class="row">
                <div class="col-lg-12">
                    {% set field = 'company_name' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label required">
                            Naziv poslovnog subjekta
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ textField([field, 'class': 'form-control', 'placeholder': 'Naziv poslovnog subjekta', 'value': (_POST[field] is defined ? _POST[field] : user.readAttribute(field)) ]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    {% set field = 'oib' %}
                    {% if user.readAttribute(field) %}
                    <div class="form-group">
                        <label for="{{ field }}" class="control-label required">OIB<abbr title="Obavezno polje">*</abbr></label>
                        <input type="hidden" name="oib" value="{{ user.readAttribute(field) }}" />
                        <div class="form-control" disabled="disabled">{{ user.readAttribute(field) }}</div>
                    </div>
                    {% else %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label required">
                            OIB
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ textField([field, 'class': 'form-control', 'placeholder': 'OIB (11 znamenki)', 'data-mask':'00000000000', 'maxlength':11, 'value': (_POST[field] is defined ? _POST[field] : user.readAttribute(field)) ]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                    {% endif %}
                </div>
                <div class="col-lg-6 col-md-6">
                    {% set field = 'birth_date' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label required">
                            Datum osnivanja
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {% set field_value = (_POST[field] is defined ? _POST[field] : (user.readAttribute(field) ? date('Y-m-d', strtotime(user.readAttribute(field))) : '')) %}
                        <input type="hidden" id="{{ field }}" data-minage="0" value="{{ field_value }}">
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    {% set field = 'first_name' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label required">
                            Ime odgovorne osobe
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ textField([field, 'class': 'form-control', 'placeholder': 'Ime', 'value': (_POST[field] is defined ? _POST[field] : user.readAttribute(field)) ]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    {% set field = 'last_name' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label required">
                            Prezime odgovorne osobe
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ textField([field, 'class': 'form-control', 'placeholder': 'Prezime', 'value': (_POST[field] is defined ? _POST[field] : user.readAttribute(field)) ]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
            </div>
            {% else %}
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    {% set field = 'first_name' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">
                            Ime
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ textField([field, 'class': 'form-control', 'placeholder': 'Ime', 'value': (_POST[field] is defined ? _POST[field] : user.readAttribute(field)) ]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    {% set field = 'last_name' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">
                            Prezime
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        {{ textField([field, 'class': 'form-control', 'placeholder': 'Prezime', 'value': (_POST[field] is defined ? _POST[field] : user.readAttribute(field)) ]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    {% set field = 'oib' %}
                    {% if user.readAttribute(field) %}
                    <div class="form-group">
                        <label for="{{ field }}" class="control-label required">OIB <abbr title="Obavezno polje">*</abbr></label>
                        <input type="hidden" name="oib" value="{{ user.readAttribute(field) }}" />
                        <div class="form-control" disabled="disabled">{{ user.readAttribute(field) }}</div>
                    </div>
                    {% else %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label required">OIB <abbr title="Obavezno polje">*</abbr></label>
                        {{ textField([field, 'class': 'form-control', 'placeholder': 'OIB (11 znamenki)', 'data-mask':'00000000000', 'maxlength':11, 'value': (_POST[field] is defined ? _POST[field] : user.readAttribute(field)) ]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                    {% endif %}
                </div>
                <div class="col-lg-6 col-md-6">
                    {% set field = 'birth_date' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label class="control-label">
                            Datum rođenja
                        </label>
                        {% set field_value = (_POST[field] is defined ? _POST[field] : (user.readAttribute(field) ? date('Y-m-d', strtotime(user.readAttribute(field))) : '')) %}
                        <input type="hidden" id="{{ field }}" data-minage="13" value="{{ field_value }}">
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
            </div>
            {% endif %}
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="country_id" class="control-label">Država</label>
                        <div class="icon_dropdown">{{ country_dropdown }}</div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group"{{ not user.county_id ? ' style="display:none"' : '' }}>
                        <label for="county_id" class="control-label">Županija</label>
                        <div class="icon_dropdown">{{ county_dropdown }}</div>
                    </div>
                </div>
                {% set fields = ['zip_code': 'Poštanski broj', 'city': 'Grad', 'address': 'Adresa'] %}
                {% for field, label in fields %}
                <div class="col-md-6 col-lg-6">
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">{{ label }}</label>
                        {{ textField([field, 'class': 'form-control', 'placeholder': label, 'value': (_POST[field] is defined ? _POST[field] : user.readAttribute(field)) ]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
                {% endfor %}
            </div>
            <div class="row">
                {% set fields = ['phone1' : 'Telefon #1', 'phone2': 'Telefon #2'] %}
                {% for field, label in fields %}
                <div class="col-md-6 col-lg-6">
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">
                            {{ label }}
                            {% if (field == 'phone1' and company_user) %}
                            <abbr class="required-company-field" title="Obavezno polje">*</abbr>
                            {% endif %}
                        </label>
                        {{ hiddenField([field, 'value':(_POST[field] is defined ? _POST[field] : user.readAttribute(field)), 'class':'phone-number-handler'])}}
                        {% set chkfield = field ~ '_public' %}
                        <div class="form-group checkbox checkbox-primary">
                            {{ checkField(_POST[chkfield] is defined and _POST[chkfield] == 'on' ? [ chkfield, 'value':'on', 'checked':'checked' ] : (user.readAttribute(chkfield) ? [chkfield, 'value': 'on', 'checked': 'checked'] : [chkfield, 'value': 'on'])) }}
                            <label for="{{ chkfield }}">
                                Želim prikazati broj telefona u oglasima koje objavim
                            </label>
                        </div>
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
                {% endfor %}
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="checkbox checkbox-primary">
                        {% set field = 'newsletter' %}
                        {{ checkField(_POST[field] is defined and _POST[field] == 'on' ? [field, 'value': 'on', 'checked': 'checked'] : (user.readAttribute(field) ? [field, 'value': 'on', 'checked': 'checked'] : [field, 'value': 'on'])) }}
                        <label for="{{ field }}">Želim primati obavijesti o novostima putem e-maila</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center">
        <button class="width-full" type="submit">Spremi izmjene</button>
    </div>
{{ endForm() }}
