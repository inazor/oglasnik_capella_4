{# MyAccount ads edit View #}
{% include 'chunks/moj-kutak-profile-header.volt' %}

<div class="side-col col-md-3 margin-top-lg hidden-xs">
    <h2 class="margin-top-lg">Moji oglasi</h2>
    <ul class="user-cp-menu">
        <li{{ (current_action == 'sviOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/svi-oglasi', 'Svi oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total'] }})</span></li>
        <li{{ (current_action == 'aktivniOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/aktivni-oglasi', 'Aktivni oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_active'] }})</span></li>
        <li{{ (current_action == 'istekliOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/istekli-oglasi', 'Istekli oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_expired'] }})</span></li>
        <li{{ (current_action == 'deaktiviraniOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/deaktivirani-oglasi', 'Deaktivirani oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_deactivated'] }})</span></li>
        <li{{ (current_action == 'neobjavljeniOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/neobjavljeni-oglasi', 'Neobjavljeni oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_unpublished'] }})</span></li>
        <li{{ (current_action == 'prodaniOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/prodani-oglasi', 'Prodani oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_sold'] }})</span></li>
        <li class="separator"><!--IE--></li>
        <li{{ (current_action == 'podijeliOglase' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/podijeli-oglase', 'Podijeli oglase']) }}</li>
    </ul>
</div>

<div class="col-md-9 margin-top-lg">
    <h2 class="margin-top-0">Uređivanje oglasa</h2>

    {{ flashSession.output() }}

    {{ form(NULL, 'id': 'frm_ads', 'method': 'post', 'autocomplete': 'off') }}
        {{ hiddenField(['next', 'value': next_url]) }}
        {{ hiddenField('_csrftoken') }}
        {{ hiddenField(['category_id', 'value': category.id]) }}
        {{ hiddenField(['user_id', 'value': ad.user_id]) }}

        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div id="ads_category_parameters">{{ rendered_parameters }}</div>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Kontakt telefon</h3></div>
                    <div class="panel-body">
                        <div class="row">
                            {% set users_country_iso_code = 'HR' %}
                            {% if auth.logged_in() %}
                                {% set logged_in_user = auth.get_user() %}
                                {% set users_country_iso_code = logged_in_user.getCountryISOCode() %}
                            {% endif %}

                            {% set ad_phone1 = (_POST['phone1'] is defined ? _POST['phone1'] : (ad.phone1 is defined ? ad.getPhoneField('phone1', users_country_iso_code) : '')) %}
                            {% set ad_phone2 = (_POST['phone2'] is defined ? _POST['phone2'] : (ad.phone2 is defined ? ad.getPhoneField('phone2', users_country_iso_code) : '')) %}

                            <div class="col-sm-6">
                                {% set field = 'phone1' %}
                                <div class="form-group">
                                    <label class="control-label" for="{{ field }}">Primarni broj</label>
                                    {{ hiddenField([field, 'value':ad_phone1, 'class':'phone-number-handler', 'data-type':phoneNumberType])}}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                {% set field = 'phone2' %}
                                <div class="form-group">
                                    <label class="control-label" for="{{ field }}">Dodatni broj</label>
                                    {{ hiddenField([field, 'value':ad_phone2, 'class':'phone-number-handler', 'data-type':phoneNumberType])}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-right">
            <hr/>
            <button class="btn btn-plain btn-md-fontsize btn-grey pull-left" type="submit" name="save" value="preview">Pregledaj prije predaje</button>

            <button class="btn btn-default" type="submit" name="cancel" value="cancel">Odustani</button>
            <button class="btn btn-primary btn-full-width-on-xs" type="submit" name="save" value="save">Spremi promjene</button>
        </div>
    {{ endForm() }}
</div>

{% include 'chunks/moj-kutak-profile-footer.volt' %}

