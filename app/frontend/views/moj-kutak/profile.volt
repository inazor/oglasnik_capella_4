{# MyAccount profile layout (child layouts are included dynamically) #}

{% include 'chunks/moj-kutak-profile-header.volt' %}

<div class="side-col col-md-3 margin-top-lg hidden-xs">
    <h2 class="margin-top-lg">Postavke profila</h2>
    <ul class="user-cp-menu">
        <li{{ (current_action == 'podaci' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/podaci', 'Osobni podaci']) }}</li>
        <li{{ (current_action == 'lozinka' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/lozinka', 'Promjena lozinke']) }}</li>
        <li{{ (current_action == 'trgovina' or current_action == 'trgovinaIzlog' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/trgovina', 'Trgovina / Izlozi']) }}</li>
        <li class="separator"><!--IE--></li>
        <li{{ (current_action == 'avatar' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/avatar', 'Promjena avatara']) }}</li>
        <li{{ (current_action == 'coverbg' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/coverbg', 'Promjena cover slike']) }}</li>
    </ul>
</div>

<div class="col-md-9 margin-top-lg">
    {{ flashSession.output() }}
    {% include('moj-kutak/' ~ current_action) %}
    {% if trgovina_upsell is defined and trgovina_upsell %}
        {% include('trgovina/index') %}
    {% endif %}
</div>

{% include 'chunks/moj-kutak-profile-footer.volt' %}
