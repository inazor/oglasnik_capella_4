<h1>Skok na vrh</h1>
<p>Kupovinom opcije ‘Skok na vrh’ Vaš će se oglas jednokratno pozicionirati na vrhu liste u svojoj kategoriji.</p>
{{ flashSession.output() }}
<h2>Plaćanje narudžbe:</h2>

<div class="billing-info">
    {{ order_table_markup }}

    <h2>Odaberite način plaćanja</h2>
    <div id="payment-method-chooser">
        <div class="payment-method payment-method-cc">
            <div class="radio radio-primary radio-medium">
                <input class="radio-light-blue" data-target="#details-cc" type="radio" name="payment_method" id="payment_method_cc" value="cc">
                <label for="cc"><span class="text-muted">Plaćanje karticom - Aktivacija <strong>ODMAH</strong></span></label>
            </div>
        </div>
        <div class="payment-method payment-method-offline">
            <div class="radio radio-primary radio-medium">
                <input class="radio-light-blue" data-target="#details-offline-{{ order_id }}" type="radio" name="payment_method" id="payment_method_offline" value="offline">
                <label for="offline"><span class="text-muted">Plaćanje uplatnicom ili internet bankarstvom</span></label>
            </div>
        </div>
    </div>

    <hr>

    {% include 'chunks/cc-payment-form.volt' %}

    {{ partial('chunks/hub-3a', ['order_id': order_id, 'payment_data_table_markup': payment_data_table_markup, 'offline_button_markup': offline_button_markup]) }}

</div>
