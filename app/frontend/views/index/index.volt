{# Homepage view #}
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "{{ slsb_url }}",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "{{ slsb_url }}search?q={search_term}",
        "query-input": "required name=search_term"
    }
}
</script>

{# if aktualno_contents %}
<section class="margin-top-md">
    <div class="container popular">{{ aktualno_contents }}</div>
</section>
{% endif #}

{% if homepage_sections %}
<section class="margin-top-md">
    <div class="container pad-xs-5-only-lr">
        <div class="homepage-main{{ full_with_homepage_sections|default(false) ? ' full-width' : '' }}">
            <section class="homepage-detailed-category-list">{% include('chunks/homepage-sections') %}</section>
        </div>
        {% if full_with_homepage_sections|default(false) == false %}
        <div class="homepage-300px text-center">
            {% if banners %}
                {% if banner_home_300x250_s1 is defined %}
                <div class="margin-bottom-md">{{ banner_home_300x250_s1 }}</div>
                {% endif %}
                {% if banner_home_300x250_s2 is defined %}
                <div class="margin-bottom-md">{{ banner_home_300x250_s2 }}</div>
                {% endif %}
                {% if banner_home_300x250_s3 is defined %}
                <div class="margin-bottom-md">{{ banner_home_300x250_s3 }}</div>
                {% endif %}
                {% if banner_home_300x250_s4 is defined %}
                <div class="margin-bottom-md">{{ banner_home_300x250_s4 }}</div>
                {% endif %}
                {% if banner_home_300x250_s5 is defined %}
                <div class="margin-bottom-md">{{ banner_home_300x250_s5 }}</div>
                {% endif %}
            {% endif %}
        </div>
        {% endif %}
    </div>
</section>
{% endif %}

<section class="margin-bottom-md">
    {% if banners and banner_home_mobile is defined %}
    <div class="text-center banner banner-mobile-homepage visible-xs">{{ banner_home_mobile }}</div>
    {% endif %}

    {% if banners and banner_megabanner is defined %}
    <div class="container"><div class="text-center banner margin-bottom-md hidden-sm hidden-xs">{{ banner_megabanner }}</div></div>
    {% endif %}

    {% if banners and banner_728x90 is defined %}
    <div class="banner w728 center banner-728x90 hidden-xs">{{ banner_728x90 }}</div>
    {% endif %}
</section>

{% if homepageFeaturedShops is defined and homepageFeaturedShops %}
<section class="margin-bottom-md overflow-hidden">
    <div class="container">
        <h2 class="section-title text-center homepage-section-title">{{ 'Izdvojene trgovine'|category_name_markup }}</h2>
        {{ homepageFeaturedShops }}
    </div>
</section>
{% endif %}

{% if posaoHrClassifieds is defined and posaoHrClassifieds and posaoHrClassifieds is iterable %}
{{ partial('chunks/posao-hr', ['posaoHr':posaoHr, 'classifieds':posaoHrClassifieds] )}}
{% endif %}

{% if homepage_stats_boxes is defined and homepage_stats_boxes %}
<section class="background-blue random-info-container hidden-xs">
    <div class="container">
        <div class="row">
            {% for box_cnt, box_data in homepage_stats_boxes  %}
                <div class="col-md-3 col-sm-6 text-center">
                    <img class="margin-bottom-md" src="{{ url('assets/img/' ~ box_data['icon']) }}" alt=""><br>
                    <span class="fancy-number">{{ box_data['num'] }}</span><br>
                    <span class="semitransparent">{{ box_data['text']|escape }}</span>
                </div>
            {% endfor %}
        </div>
    </div>
</section>
{% endif %}

{% if latest_magazine_articles is defined and latest_magazine_articles and latest_magazine_articles is iterable %}
<section class="margin-top-sm">
    <div class="container">
        <h2 class="section-title text-center homepage-section-title">Oglasnik <b>novosti</b></h2>
        {{ partial('chunks/cms-articles', ['articles':latest_magazine_articles, 'rowClass':'masonry-articles', 'itemClass':'masonry-item']) }}
        <a href="{{ url('novosti') }}" class="btn btn-primary center">Pogledaj više</a>
    </div>
</section>
{% endif %}
