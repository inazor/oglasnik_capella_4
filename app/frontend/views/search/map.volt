{{ form('search/map', 'id':'searchMapForm', 'class':'searchOnMap filtersView', 'method':'get', 'data-md5':mapMD5) }}
    <div id="searchOnMapContent">
        <div class="listing-section border-right">
            <div class="scroll-y search-by-location-filter" data-view-type="filters">
                <div class="row">
                    <div class="col-md-6 location-text-filter">
                        <label class="default-typeface text-small control-label" for="location_text">Traži u blizini lokacije</label>
                        <input type="hidden" id="location_bounds" name="location_bounds" value="{{ locationBounds|default('') }}" />
                        <input type="hidden" id="location_zoom" name="location_zoom" value="{{ locationZoom|default('') }}" />
                        <input type="hidden" id="location_center" name="location_center" value="{{ locationCenter|default('') }}" />
                        <input type="text" id="location_text" name="location_text" value="{{ locationText|default('') }}" class="form-control" placeholder="Pretraži grad ili ulicu..." />
                    </div>
                    <div class="col-md-6 price-filter">
                        <div>
                            <label class="default-typeface text-small">Cijena</label>
                            <div class="text-right currency-switch pull-right">
                                <input type="hidden" name="ad_price_code" id="ad_price_currency" value="{{ adPriceCurrency|default('EUR') }}" />
                                <span class="currency-item" data-currency="EUR">eur</span>
                                <span class="currency-item" data-currency="HRK">kn</span>
                            </div>
                            <div class="clearfix"><!--IE--></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 price-from-box">
                                <div class="input-group">
                                    <span class="input-group-addon">Od</span>
                                    <input type="text" id="parameter_price_from" name="ad_price_from" value="{{ adPriceFrom|default('') }}" class="form-control text-right" data-type="number" data-default="0">
                                    <span class="input-group-addon price-currency-label">EUR</span>
                                </div>
                            </div>
                            <div class="col-sm-6 price-to-box">
                                <div class="input-group">
                                    <span class="input-group-addon">Do</span>
                                    <input type="text" id="parameter_price_to" name="ad_price_to" value="{{ adPriceTo|default('') }}" class="form-control text-right" data-type="number" data-default="0">
                                    <span class="input-group-addon price-currency-label">EUR</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-bottom-md">
                    <div class="col-md-3 col-sm-6 margin-top-10px">
                        <label class="default-typeface text-small" for="parent_category_id">Vrsta nekretnine</label>
                        {{ parent_category_id_dropdown }}
                    </div>
                    <div class="col-md-3 col-sm-6 margin-top-10px">
                        <label class="default-typeface text-small" for="category_id">Vrsta transakcije</label>
                        {{ category_id_dropdown }}
                    </div>
                    <div class="col-lg-3 col-sm-6 margin-top-10px">
                        <label class="default-typeface text-small" for="sort">Poredaj oglase</label>
                        {{ sort_dropdown }}
                    </div>
                    <div class="col-md-3 col-sm-6 margin-top-10px">
                        <label class="default-typeface text-small hidden-xs">&nbsp;</label>
                        <div class="form-group checkbox checkbox-primary">
                            <input type="checkbox" id="parameter_uploadable" name="ad_params_uploadable" value="1" data-type="checkbox" data-default="false">
                            <label for="parameter_uploadable" title="Prikaži samo oglase sa slikom"><span class="hidden-lg">Prikaži samo oglase sa slikom</span><span class="visible-lg-inline">Oglasi sa slikom</span></label>
                        </div>
                    </div>
                </div>
                <div class="row darker-row" id="categoryFiltersDiv">
                    <div class="title">Više filtera</div>
                    <div class="desktop-buttons">
                        <div class="col-xs-6 text-left">
                            <input type="hidden" name="more-filters" id="more-filters-input" value="{{ moreFiltersBtn|default(0) }}" data-default="0"/>
                            <span class="btn{{ (moreFiltersBtn|default(0) == 1) ? ' active' : '' }} more-filters-btn" id="more-filters-btn"{{ (moreFiltersBtn|default(0) == 0) ? '  style="display:none"' : '' }}>{{ (moreFiltersBtn|default(0) == 1) ? 'Manje filtera &nbsp; <span class="fa fa-angle-up"></span>' : 'Više filtera &nbsp; <span class="fa fa-angle-down"></span>' }}</span>
                        </div>
                        <div class="col-xs-6 text-right">
                            <button class="btn light-blue" tyle="submit" id="submitBtn">Pretraži</button>
                            <span class="ajaxLoading" style="display:none"><span class="fa fa-refresh fa-fw fa-spin"></span>Učitavanje...</span>
                        </div>
                        <div class="clearfix"><!--IE--></div>
                    </div>
                    {% if moreFilters is defined and moreFilters is iterable %}
                    <div id="more-filters-div" class="col-xs-12"{{ (moreFiltersBtn|default(0) == 0) ? ' style="display:none"' : '' }}>
                        {% for categoryFilters in moreFilters %}{{ categoryFilters }}{% endfor %}
                    </div>
                    {% endif %}
                </div>
                <div class="results" id="listingContainer">
                    {% include 'chunks/ads-map.volt' %}
                </div>
            </div>
        </div>
        <div class="map-section">
            <div id="googleMapContainer" data-view-type="map"><!--IE--></div>
            <div class="autoSuggest"></div>
        </div>
    </div>
    <div id="searchOnMapMobileFooter">
        <div class="view-buttons">
            <span class="mobile-button view-button filters" data-view-type="filters">Filteri</span>
        </div>
        <div class="mobile-map">
            <span class="refreshBtn">
                <span class="ajaxLoading" style="display:none"><span class="fa fa-refresh fa-fw fa-spin"></span></span>
            </span>
        </div>
        <div class="mobile-filtering">
            <span class="mobile-button filters more-filters-btn{{ (moreFiltersBtn|default(0) == 1) ? ' active' : '' }}" id="moreFiltersMobileBtn"{{ (moreFiltersBtn|default(0) == 0) ? '  style="display:none"' : '' }}>{{ (moreFiltersBtn|default(0) == 1) ? 'Manje filtera &nbsp; <span class="fa fa-angle-up"></span>' : 'Više filtera &nbsp; <span class="fa fa-angle-down"></span>' }}</span>
            <span class="mobile-button searchBtn" id="mobileSubmitBtn">Pretraži</span>
        </div>
    </div>
{{ endForm() }}
