{# User sign in #}
<div class="modal-header">
    <a href="/" class="close"></a>
    <h2 class="modal-title section-title" id="modal-label">Prijava <b>korisnika</b></h2>
    <p class="text-center margin-top-sm">Nemate profil na Oglasnik.hr? <a href="{{ this.url.get('user/signup') }}">Registrirajte se</a></p>
</div>

<div class="modal-body">
    {{ flashSession.output() }}
    {{ form(NULL, 'id':'frm_login', 'method':'post') }}
        {{ hiddenField('_csrftoken') }}
        {% set field = 'username' %}
        <div class="form-group">
            {{ textField([field, 'placeholder':'Korisničko ime / email*', 'tabindex':1, 'class':'form-control icon-field username']) }}
            {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
        </div>
        {% set field = 'password' %}
        <div class="form-group">
            {{ passwordField([field, 'placeholder':'Lozinka*', 'tabindex':2, 'class':'form-control icon-field password']) }}
            {% if errors is defined and errors.filter(field) %}<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>{% endif %}
        </div>
        {% if errors is defined %}
        <div class="warning margin-top-sm margin-bottom-sm text-danger text-small">
            <b>Greška!</b>
            {% for error in errors %}{{ error }}{% endfor %}
        </div>
        {% endif %}
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group checkbox checkbox-primary margin-top-0 margin-bottom-0">
                    {% set field = 'remember' %}
                    {{ checkField(_POST[field] is defined and _POST[field] == 'on' ? [field, 'value':'on', 'checked':'checked', 'tabindex':3] : [field, 'value':'on', 'tabindex':3]) }}
                    <label for="{{ field }}">Zapamti me</label>
                </div>
            </div>
            <div class="col-sm-6 text-right-sm">
                <a href="{{ this.url.get('user/pwdforgot') }}">Zaboravili ste lozinku?</a>
            </div>
        </div>

        <div class="margin-top-lg">
            <button type="submit" tabindex="4" class="width-full btn-standard modal-btn-send light-blue">Prijavi se</button>
        </div>

        <div class="row margin-top-sm">
            <div class="col-xs-6">
                <a href="{{ this.url.get('user/oauth/facebook') }}" class="width-full btn btn-standard facebook">Facebook<span class="hidden-xs"> prijava</span></a>
            </div>
            <div class="col-xs-6">
                <a href="{{ this.url.get('user/oauth/google') }}" class="width-full btn btn-standard google-plus">Google+<span class="hidden-xs"> prijava</span></a>
            </div>
        </div>

        <p><small><br/>
            Ako imate problema s prijavom na stranicu ili aktivacijom
            oglasa, slobodno se obratite našoj korisničkoj podršci na
            <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a> ili na broj telefona 01/6102-885 (od
            ponedjeljka do petka od 08 do 17 sati).<br/>
            <br/>
            Hvala na razumijevanju!<br/>
            Vaš Oglasnik.hr</small>
        </p>
    {{ endForm() }}
</div>
