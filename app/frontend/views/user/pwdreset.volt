{# User pwdreset #}
<section>
    <div class="underline page-title">
        <h1 class="underline">{{ title }}</h1>
        <span>Nemaš korisnički račun? <a href="{{ this.url.get('user/signup') }}">Registriraj se &raquo;</a></span>
    </div>
    {{ flashSession.output() }}
    {% if show_form %}
    <p>Upišite svoju novu željenu lozinku u oba polja.</p>
    {{ form(NULL, 'id' : 'frm_pwdreset', 'method' : 'post') }}
        {{ hiddenField('token') }}
        {{ hiddenField('_csrftoken') }}
        <div class="row">
            <div class="col-md-6 col-lg-6">
                {% set field = 'password' %}
                <div class="form-group">
                    <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        Nova željena lozinka
                        <abbr title="Obavezno polje">*</abbr>
                    </label>
                    {{ passwordField([ field, 'class' : 'form-control', 'placeholder' : 'Lozinka', 'value' : (_POST[field] is defined ? _POST[field] : '') ]) }}
                    {% if errors is defined and errors.filter(field) %}
                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                    {% endif %}
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                {% set field = 'repeatPassword' %}
                <div class="form-group">
                    <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        Ponovljena nova željena lozinka
                        <abbr title="Obavezno polje">*</abbr>
                    </label>
                    {{ passwordField([ field, 'class' : 'form-control', 'placeholder' : 'Lozinka', 'value' : (_POST[field] is defined ? _POST[field] : '') ]) }}
                    {% if errors is defined and errors.filter(field) %}
                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                    {% endif %}
                </div>
            </div>
            <div class="col-md-12 col-lg-12">
                <hr />
                <button class="btn btn-primary" type="submit">Pošalji <span class="fa fa-caret-right fa-fw"></span></button>
            </div>
        </div>
    {{ endForm() }}
    {% endif %}
</section>
