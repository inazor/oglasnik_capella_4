<div class="post-box{{ ad['online_product_id'] is defined ? ' ' ~ ad['online_product_id']|slugify ~ '-oglas' : '' }}" data-share-url="{{ ad['frontend_url'] }}">
    <div class="img-wrapper">
        {%- set thumb = ad['thumb_pic'] -%}
        <a href="{{ ad['frontend_url'] }}"><img width="{{ thumb.width }}" height="{{ thumb.height }}" src="{{ thumb.src }}" alt="{{ ad['title']|escape_attr|striptags }}"></a>
    </div>
    <div class="post-info">
        <h3><a href="{{ ad['frontend_url'] }}">{{ ad['title']|striptags }}</a></h3>
        {% set desc = ad['description_tpl']|default(ad['description'])|striptags|truncate(150)|nl2br %}
        <p>{{ desc }}</p>
    </div>
    <div class="toolbar">
        <span class="publish-date">{{ ad['sort_date'] }}</span>
        <span class="price">
        {% if ad['price']['main'] > 0 %}
        {% if ad['price']['other'] is defined %}{{ ad['price']['other'] }} <abbr title="približno">~</abbr> {% endif %}
        &nbsp;<b class="blue">{{ ad['price']['main'] }}</b></span><br/>
        {% endif %}
        <a href="#" class="share-facebook" title="Podjeli na facebooku"><span class="fa fa-facebook-square"></span></a>
        <a class="toggle-favorite disabled" data-id="{{ ad['id'] }}"><span class="fa fa-star"></span></a>
        <div class="clearfix"></div>
    </div>
</div>
