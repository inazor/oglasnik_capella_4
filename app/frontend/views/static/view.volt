{# CMS static page frontend view #}

{% if not (article_media is empty) and article_media.valid() %}
{% set main_pic = article_media[0] %}
{% set main_pic_thumb = main_pic.get_thumb('Clanak-big-710x300') %}
{% else %}
{% set main_pic_thumb = 'http://placehold.it/1920x520' %}
{% endif %}
<div class="page-header margin-top-lg" style="background-image:url('{{ main_pic_thumb }}');"></div>
<div class="container margin-top-md">
    <div class="main-col col-lg-8 col-lg-offset-1 article">
        <!--h1 class="hidden-sm hidden-md hidden-lg margin-top-0">O nama</h1-->
        <h1 class="margin-top-0">{{ article.title }}</h1>
        {% if article.excerpt %}<div class="excerpt">{{ article.excerpt }}</div>{% endif %}
        <div>{{ article.content }}</div>
    </div>
</div> <!-- end .container -->
