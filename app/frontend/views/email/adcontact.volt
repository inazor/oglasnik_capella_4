{# Ad contact form email tpl #}
<p>Pozdrav,<br />primili ste poruku vezanu uz Vaš oglas <b><a href="{{ ad_link }}">{{ ad_title }}</a></b> predanom na Oglasnik.hr</p>

<p>
    Ime:<br/><strong>{{ name }}</strong><br />

{#
    Email:<br/><strong>{{ email }}</strong><br />
#}
    Poruka:<br/>
    <strong>{{ message|nl2br }}</strong>
</p>

{{ remark is defined and remark ? '<p>' ~  remark ~ '</p>' : '' }}
