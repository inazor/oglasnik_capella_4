{# pwdreset email tpl #}
<p>Pozdrav <b>{{ username }}</b>,</p>

<p>Za pokretanje postupka poništenja lozinke kliknite na sljedeći link:</p>

<p><a href="{{ url }}">{{ url }}</a></p>

<p>Verifikacijski kôd za promjenu lozinke je: <b>{{ token }}</b></p>

<p>Ako klik na link rezultira greškom, kopirajte i zalijepite link u novi tab/prozor svog preglednika.<br><br>
Ako ste ovu poruku primili greškom, vjerojatno je neki drugi korisnik zabunom<br>
unio Vašu e-mail adresu u pokušaju da poništi svoju lozinku.<br><br>
Ako niste Vi pokrenuli zahtjev, zanemarite ovu poruku i primite naše isprike.<br><br>
Ovo je informativna poruka. Odgovori na nju se ne prate i na njih se ne odgovara.</p>
