<div id="details-cc" class="payment-method-details">
    <p>Klikom na &quot;Plaćanje karticom&quot; šaljemo Vas na stranice WSPay sustava za plaćanje karticama.</p>
    <p><img src="{{ url('assets/img/credit-cards.jpg') }}" usemap="#stickers"></p>
    <map name="stickers">
        <area alt="" title="" href="http://www.wspay.info/" shape="rect" coords="1,1,98,47" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.americanexpress.hr/" shape="rect" coords="103,0,153,46" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.diners.com.hr/" shape="rect" coords="157,0,216,48" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.visa.com.hr/" shape="rect" coords="216,1,294,44" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.mastercard.com/hr/" shape="rect" coords="296,0,372,46" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.maestrocard.com/hr/" shape="rect" coords="374,0,450,46" style="outline:none;" target="_blank">
        <area alt="" title="" href="https://www.discover.com/" shape="rect" coords="450,0,526,46" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.diners.com.hr/" shape="rect" coords="525,0,616,45" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.americanexpress.hr/webtrgovina/Jamstvo_sigurne_online_kupnje.html" shape="rect" coords="620,0,724,44" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.pbzcard.hr/media/53821/mcsc_hr.html" shape="rect" coords="739,0,823,50" style="outline:none;" target="_blank">
        <area alt="" title="" href="http://www.pbzcard.hr/media/53827/vbv_hr.html" shape="rect" coords="831,1,913,60" style="outline:none;" target="_blank">
    </map>
    <hr>
    <div class="form-container">
        {{ rendered_form }}
    </div>
</div>
