{% set homepage_section_icon = null %}
{% set homepage_section_json = homepage_section['json'] %}
{% if homepage_section_json.icon|default(null) %}
    {% set homepage_section_icon = url.get('assets/img/categories/' ~ homepage_section_json.icon) %}
{% endif %}
{% set bg_image = null %}
{% set bg_image_thumb = null %}
{% if homepage_section['backgroundPic']|default(null) %}
    {% set bg_image = homepage_section['backgroundPic'] %}
    {% set bg_image_thumb = bg_image.getThumb(['width':500, 'height':300]) %}
{% endif %}
{% set boxClass = full_with_homepage_sections|default(false) ? 'col-md-3 ' : '' %}

<div class="{{ boxClass }}col-sm-4 col-xs-6">
    <div class="section-box">
        {% if bg_image_thumb %}<span class="background" style="background-image:url('{{ bg_image_thumb.src }}')"></span>{% endif %}
        <a href="{{ homepage_section['frontend_url'] }}"{{ homepage_section['frontend_url_target']|default(null) ? ' target="' ~ homepage_section['frontend_url_target'] ~ '"' : '' }}>
            <span class="info">
                {% if homepage_section_icon %}<span class="icon"><img src="{{ homepage_section_icon }}"></span>{% endif %}
                <h2>{{ homepage_section['name'] }}</h2>
            </span>
        </a>
    </div>
</div>
