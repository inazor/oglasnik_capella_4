<div class="row grid-layout">
{% set currentIndex = 1 %}
{% set marginBottomLg = 'margin-bottom-sm' %}
{% for ad in ads %}
    {% if separateAdsWithHeaders|default(false) %}
        {% if !lastListingHeaderText %}
            {% if 'Ostali oglasi' != ad['listing_heading_text'] %}
                {% set lastListingHeaderText = ad['listing_heading_text'] %}
                {% set lastListingHeaderClass = ad['listing_heading_class'] %}
                {% set showListingHeaderText = true %}
                {% set currentIndex = 1 %}
            {% else %}
                {% set showListingHeaderText = false %}
            {% endif %}
        {% else %}
            {% if lastListingHeaderText != ad['listing_heading_text'] %}
                {% set lastListingHeaderText = ad['listing_heading_text'] %}
                {% set lastListingHeaderClass = ad['listing_heading_class'] %}
                {% set showListingHeaderText = true %}
                {% set currentIndex = 1 %}
            {% else %}
                {% set showListingHeaderText = false %}
            {% endif %}
        {% endif %}

        {% if showListingHeaderText %}
        <div class="col-xs-12">
            <div class="ad-box ad-box-wide ad-box-title no-checkbox category-listing {{ marginBottomLg }} no-hover no-border border-radius-xs{{ lastListingHeaderClass ? lastListingHeaderClass : '' }}">
                <span class="margin-0 default-typeface">{{ lastListingHeaderText }}</span>
            </div>
        </div>
        {% endif %}
    {% endif %}

    {% set isOdd = (currentIndex % 2 != 0) %}
    {% include 'chunks/ads-grid-single.volt' %}

    {% if narrowGridMode is defined and narrowGridMode %}
        {# add clear after every 2 and 3 items but hide the clears depending on screen width #}
        {% if currentIndex % 2 == 0 %}
            <div class="clearfix hidden-md hidden-lg"></div>
        {% endif %}
        {% if currentIndex % 3 == 0 %}
            <div class="clearfix hidden-xs hidden-sm hidden-md"></div>
        {% endif %}
    {% else %}
        {# wider / full width grid #}
        {% if currentIndex % 4 == 0 %}
            <div class="clearfix"></div>
        {% elseif currentIndex % 2 == 0 %}
            <div class="clearfix hidden-md hidden-lg"></div>
        {% endif %}
    {% endif %}
    {% set currentIndex = currentIndex + 1 %}
{% endfor %}
</div>
