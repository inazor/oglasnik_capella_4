{# oib collection/interstitial form #}
<div class="pad-xs-5-only-lr">

{% include('chunks/ads-submission-title-bit') %}
{% include('chunks/ads-submission-missing-reason') %}

{{ form(NULL, 'id' : 'frm_fullname', 'method', 'post', 'autocomplete' : 'off') }}
{{ hiddenField('_csrftoken') }}
<div class="row">
    {% set field = 'first_name' %}
    <div class="col-md-6 form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
        <label for="{{ field }}" class="control-label">
            Ime
            <abbr title="Obavezno polje">*</abbr>
        </label>
        {{ textField([field, 'class': 'form-control', 'placeholder': 'Ime']) }}
        {% if errors is defined and errors.filter(field) %}
            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
        {% endif %}
    </div>
    {% set field = 'last_name' %}
    <div class="col-md-6 form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
        <label for="{{ field }}" class="control-label">
            Prezime
            <abbr title="Obavezno polje">*</abbr>
        </label>
        {{ textField([field, 'class': 'form-control', 'placeholder': 'Prezime']) }}
        {% if errors is defined and errors.filter(field) %}
            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
        {% endif %}
    </div>
    <div class="col-xs-12">
        <br>
        <button class="btn btn-primary" type="submit" name="save">Pošalji</button>
    </div>
</div>
{{ endForm() }}

</div>
