{% if breadcrumbs is defined and breadcrumbs is iterable %}
<div class="breadcrumbs">
    {% for breadcrumb in breadcrumbs %}
        {% set crumb_url = '' %}
        {% set crumb_title = '' %}
        {% if breadcrumb is type('array') %}
            {% set crumb_url = breadcrumb['frontend_url'] %}
            {% set crumb_title = breadcrumb['name'] %}
        {% else %}
            {% set crumb_url = breadcrumb.get_url() %}
            {% set crumb_title = breadcrumb.name %}
        {% endif %}
        {% if not loop.first %}
            {% if not loop.last %}
                <span><a href="{{ crumb_url }}">{{ crumb_title }}</a></span> <span class="fa fa-angle-right"></span>
            {% else %}
                <span class="active"><a href="{{ crumb_url }}">{{ crumb_title }}</a></span>
            {% endif %}
        {% else %}
            <span><a href="{{ url() }}">Oglasnik</a></span> <span class="fa fa-angle-right"></span>
        {% endif %}
    {% endfor %}
</div>
{% endif %}
