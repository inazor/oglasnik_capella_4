<ul>
    <li><a href="{{ this.url.get('') }}">Početna</a></li>
    <li>
        <a href="{{ this.url.get('categories/all') }}">Kategorije</a>
        <ul>
        {% for category in categories %}
            {% set categoryJson = category.json %}
            {% set categoryIcon = categoryJson.icon|default(null) %}
            {% if categoryIcon %}
                {% set categoryIcon = '<span class="icon" style="background-image:url(' ~ url('assets/img/categories/' ~ categoryIcon) ~ ')" alt="' ~ category.name ~ '"></span>' %}
            {% else %}
                {% set categoryIcon = '' %}
            {% endif %}
            <li><a href="{{ this.url.get(category.url) }}">{{ categoryIcon ~ category.name }}</a></li>
        {% endfor %}
        </ul>
    </li>
    <li>
        <span>Oglasi</span>
        <ul>
            {#<li><a href="{{ this.url.get('search/map') }}">Pretraži nekretnine na karti</a></li>#}
            <li><a href="{{ this.url.get('novi-oglasi') }}">Novi oglasi</a></li>
            <li><a href="{{ this.url.get('popularni-oglasi') }}">Popularni oglasi</a></li>
        </ul>
    </li>
    <li>
        <span>Ostalo</span>
        <ul>
            <li><a href="{{ this.url.get('info/cjenik') }}">Cjenik</a></li>
            <li><a href="{{ this.url.get('info/o-nama') }}">O nama</a></li>
            <li><a href="{{ this.url.get('info/kontakt-i-podrska') }}">Podrška</a></li>
            <li><a href="{{ this.url.get('info/pomoc') }}">Pomoć</a></li>
            <li><a href="{{ this.url.get('novosti') }}">Vijesti</a></li>
        </ul>
    </li>
    {% if auth.logged_in() %}
    <li>
        <span class="text-primary">Korisnički profil{% if userUnreadMsgCount > 0 %}<span class="count-text"> {{ userUnreadMsgCount }}</span>{% endif %}</span>
        <ul>
            {% if auth.logged_in(['admin','supersupport','support','finance','moderator','content','sales']) %}
            <li><a href="{{ this.url.get('admin') }}">Admin</a></li>
            {% endif %}
            <li><a href="{{ this.url.get('moj-kutak/podaci') }}">Postavke profila</a></li>
            <li><a href="{{ this.url.get('moj-kutak/svi-oglasi') }}">Moji oglasi</a></li>
            <li><a href="{{ this.url.get('moj-kutak/spremljeni-oglasi') }}">Spremljeni oglasi</a></li>
            <li><a href="{{ this.url.get('moj-kutak/poruke') }}">Poruke{% if userUnreadMsgCount > 0 %}<span class="count-text"> {{ userUnreadMsgCount }}</span>{% endif %}</a></li>
            <li><a href="{{ this.url.get('info/pomoc') }}">Pomoć</a></li>
            <li><a href="{{ this.url.get('user/signout') }}" class="color-secondary-red">Odjava</a></li>
        </ul>
    </li>
    {% else %}
    <li><a href="{{ this.url.get('user/signin') }}" class="text-primary">Prijava</a></li>
    <li><a href="{{ this.url.get('user/signup') }}" class="text-primary">Registracija</a></li>
    {% endif %}
</ul>
