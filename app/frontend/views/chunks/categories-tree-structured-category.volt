{% if category.level <= maxLevelDepth and not(category.name|lower|trim in forbiddenCategoryNames) and category.active %}
<li>
    {% set categoryJson = category.json %}
    {% set categoryIcon = '' %}
    {# set blank category icon for main categories #}
    {% if category.level == 2 %}
        {% set categoryIcon = '<span class="category-icon"></span>' %}
        {% if categoryJson.icon is defined and categoryJson.icon %}
            {% set categoryIcon = '<span class="category-icon" style="background-image:url(/assets/img/categories/' ~ categoryJson.icon ~ ')"></span>' %}
        {% endif %}
    {% endif %}
    <a href="{{ url(category.url) }}">{{ categoryIcon ~ category.name }}</a>{{ category.count|default(0) ? ' <small>' ~ category.count ~ '</small>' : ''}}
</li>
{% set categoryChildren = category.children %}
{% if categoryChildren|length %}
{{ partial('chunks/categories-tree-structured-children', ['children':categoryChildren, 'maxLevelDepth':maxLevelDepth, 'forbiddenCategoryNames':forbiddenCategoryNames])}}
{% endif %}

{% endif %}
