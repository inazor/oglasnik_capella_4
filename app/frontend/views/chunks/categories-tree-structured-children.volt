<li>
    <ul class="subcategory">
        {% for child in children %}{{ partial('chunks/categories-tree-structured-category', ['category': child, 'maxLevelDepth':maxLevelDepth, 'forbiddenCategoryNames':forbiddenCategoryNames]) }}{% endfor %}
    </ul>
</li>
