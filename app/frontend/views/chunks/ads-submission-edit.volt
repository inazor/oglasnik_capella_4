{# Frontend Ad Submit View #}
<div class="pad-xs-5-only-lr">

{% include('chunks/ads-submission-title-bit') %}

{{ steps_wizard_markup }}

<h2>Detalji oglasa</h2>
<p class="chosen-ad-category">Kategorija: <span class="color-deep-dark-blue-2">{{ category.getPath() }}</span></p>

{{ form(NULL, 'id':'frm_ads_submit', 'method':'post', 'autocomplete':'off') }}
    {{ hiddenField('_csrftoken') }}
    {{ hiddenField(['category_id', 'value': category.id]) }}
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div id="ads_category_parameters">{{ rendered_parameters }}</div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Kontakt telefon</h3></div>
                <div class="panel-body">
                    <div class="row">
                        {% set users_phone_nrs = null %}
                        {% set users_country_iso_code = 'HR' %}
                        {% if auth.logged_in() %}
                            {% set logged_in_user = auth.get_user() %}
                            {% set users_country_iso_code = logged_in_user.getCountryISOCode() %}
                            {% set users_phone_nrs = logged_in_user.get_public_phone_numbers() %}
                            {% set ad_phone1 = users_phone_nrs[0]|default('') %}
                            {% set ad_phone2 = users_phone_nrs[1]|default('') %}
                        {% endif %}

                        {% set ad_phone1 = (_POST['phone1'] is defined ? _POST['phone1'] : (ad.phone1 is defined ? ad.getPhoneField('phone1', users_country_iso_code) : (users_phone_nrs ? users_phone_nrs[0] : ''))) %}
                        {% set ad_phone2 = (_POST['phone2'] is defined ? _POST['phone2'] : (ad.phone2 is defined ? ad.getPhoneField('phone2', users_country_iso_code) : (users_phone_nrs ? users_phone_nrs[1] : ''))) %}

                        <div class="col-lg-3 col-md-3">
                            {% set field = 'phone1' %}
                            <div class="form-group">
                                <label class="control-label" for="{{ field }}">Primarni broj</label>
                                {{ hiddenField([field, 'value':ad_phone1, 'class':'phone-number-handler', 'data-type':phoneNumberType])}}
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            {% set field = 'phone2' %}
                            <div class="form-group">
                                <label class="control-label" for="{{ field }}">Dodatni broj</label>
                                {{ hiddenField([field, 'value':ad_phone2, 'class':'phone-number-handler', 'data-type':phoneNumberType])}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{#
    {% if not auth.logged_in() %}
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            {% if errors is defined and errors.filter('user_id') %}
                            <div class="has-error">
                                <p class="help-block">{{ current(errors.filter('user_id')).getMessage() }}</p>
                            </div>
                            {% endif %}

                            <div role="tabpanel">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#existing_user" aria-controls="existing_user" role="tab" data-toggle="tab">Postojeći korisnik</a></li>
                                    <li role="presentation"><a href="#new_user" aria-controls="new_user" role="tab" data-toggle="tab">Novi korisnik</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="existing_user">
                                        <div class="row">
                                            <div class="col-md-8 col-lg-8">
                                                {% set field = 'username' %}
                                                <div class="form-group">
                                                    <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                                                        Korisničko ime / Email
                                                        <abbr title="Obavezno polje">*</abbr>
                                                    </label>
                                                    {{ textField([ field, 'placeholder' : 'Korisničko ime / Email', 'class': 'form-control' ]) }}
                                                    {% if errors is defined and errors.filter(field) %}
                                                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                                    {% endif %}
                                                </div>
                                                {% set field = 'password' %}
                                                <div class="form-group">
                                                    <label for="{{ field }}" class="control-label{{ errors is defined and errors.filter(field) ? ' has-error' : (_POST[field] is defined ? ' has-success' : '') }}">
                                                        Lozinka
                                                        <abbr title="Obavezno polje">*</abbr>
                                                    </label>
                                                    {{ passwordField([ field, 'placeholder' : 'Lozinka', 'class': 'form-control' ]) }}
                                                    {% if errors is defined and errors.filter(field) %}
                                                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                                    {% endif %}
                                                    <a class="pull-right text-small" href="{{ this.url.get('user/pwdforgot') }}">Zaboravili ste lozinku?</a>
                                                </div>
                                                <div class="checkbox checkbox-primary">
                                                    {% set field = 'remember' %}
                                                    {{ checkField(_POST[field] is defined and _POST[field] == 'on' ? [ field, 'value': 'on', 'checked': 'checked' ] : [ field, 'value': 'on' ]) }}
                                                    <label for="{{ field }}">Zapamti me</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <a class="btn btn-default btn-facebook btn-block" href="{{ this.url.get('user/oauth/facebook') }}"><span class="fa fa-facebook fa-fw"></span> Facebook prijava</a>
                                                <a class="btn btn-default btn-google-plus btn-block" href="{{ this.url.get('user/oauth/google') }}"><span class="fa fa-google-plus fa-fw"></span> Google+ prijava</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="new_user">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    {% endif %}
#}

    <div class="text-right margin-bottom-sm">
        <hr>
        <button class="btn btn-plain btn-md-fontsize margin-bottom-1em-xs-only btn-grey pull-left" type="submit" name="next" value="preview">Pregledaj prije predaje</button>
        {% if stepType == 'new' %}
        <button class="btn btn-primary btn-full-width-on-xs" type="submit" name="next" value="product_chooser">Nastavi predaju</button>
        {% elseif stepType == 'republish' %}
        <button class="btn btn-default" type="submit" name="cancel" value="cancel">Odustani</button>
        <button class="btn btn-primary btn-full-width-on-xs" type="submit" name="next" value="product_chooser">Ponovi oglas</button>
        {% endif %}
    </div>
{{ endForm() }}

</div>
