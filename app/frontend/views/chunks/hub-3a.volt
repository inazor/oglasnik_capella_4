<div id="details-offline-{{ order_id }}" class="hub-3a-details" data-order-id="{{ order_id }}">
    <p>
        Aktivacija PO PRIMITKU UPLATE na naš bankovni račun, od 1 do 3 RADNA DANA PO IZVRŠENOJ UPLATI.<br>
        Plaćanje možete izvršiti općom uplatnicom u svim poštanskim uredima ili internet bankarstvom bilo koje banke.
    </p>
    {% if not(payment_data_table_markup is empty) %}
        {{ payment_data_table_markup }}
    {% endif %}
    <p>Potvrdu uplate pošaljite faksom na broj <a href="tel:+38516102898"><strong>01/6102-898</strong></a> ili na e-mail: <a href="mailto:podrska@oglasnik.hr"><strong>podrska@oglasnik.hr</strong></a></p>
    {% if not(bank_slip_markup is empty) %}
        {{ bank_slip_markup }}
    {% endif %}
    {% if not(offline_button_markup is empty) %}
        <div class="text-right">
            {{ offline_button_markup }}
        </div>
    {% endif %}
</div>
