<!DOCTYPE html>
<html class="no-js" lang="hr">
<head>
    <meta charset="utf-8">
    {{ getTitle() }}
    <meta name="description" content="{{ site_desc }}">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
    <meta property="fb:app_id" content="{{ this.config.social.fb_app_id }}">
    <meta property="fb:pages" content="53962986681">
    <meta property="og:site_name" content="Oglasnik.hr"/>
    <meta name="google-site-verification" content="gHg3AUv8yKIUTPwdorSp_NV7cEBpT0Pb65FtV_kYneA">
    <link rel="icon" type="image/x-icon" href="{{ this.url.getStatic('favicon.ico') }}">

    {%- if not (og_meta is empty) %}
        {{ og_meta }}
    {% endif %}

    <script>
        document.documentElement.className = document.documentElement.className.replace('no-js', 'js');
    </script>
    {{ this.assets.outputCss() }}
    {# was told these dfp tags are to go sitewide in the head, for something called yieldlove? #}
    {%- if banners -%}
    <script type="text/javascript">
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function() {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
            var node =document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>
    {% endif %}
    {%- if curr_env == 'production' %}
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NGSS62');</script>
    <!-- End Google Tag Manager -->
    <!-- GoogleAnalytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-2093500-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- Hotjar Tracking Code for http://www.oglasnik.hr -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:287193,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
        {% if fb_pixel is defined and fb_pixel %}
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                    document,'script','https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '838932269508780');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=838932269508780&ev=PageView&noscript=1"/></noscript>
        <!-- End Facebook Pixel Code -->
        {% endif %}
    {%- endif %}
    {% if head_scripts is defined and count(head_scripts) %}
        {% for script in head_scripts %}
        <script type="text/javascript">{{ script }}</script>
        {% endfor %}
    {%- endif -%}

    {{- this.assets.outputJs('head') -}}

    {% if banners %}
        <script type="text/javascript" charset="utf-8" src="//ad.adverticum.net/g3.js"></script>
        <script type="text/javascript">
            var goAdZonesRequested = {{ banner_zone_ids }};
            if (window.goAdverticum3) {
                window.goAdverticum3.loadZones({ zones: goAdZonesRequested });
            }
        </script>
    {% endif %}
</head>
