<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Zatvori"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel"><span class="text-danger">Upozorenje</span></h4>
            </div>
            {{ form(NULL, 'id': 'frm_deleteModal', 'method': 'post', 'autocomplete': 'off') }}
                {{ hiddenField(['next', 'value':next_url]) }}
                <div class="modal-body">
                    <p>{{ deleteModalWarning|default('Da li ste sigurni da želite nastaviti s izvršavanjem ove akcije?<br /><span class="text-danger">Brisanje je <strong>nepovratna akcija</strong>!</span>') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ne, odustani</button>
                    <button type="submit" class="btn btn-danger">Da, obriši</button>
                </div>
            {{ endForm() }}
        </div>
    </div>
</div>
