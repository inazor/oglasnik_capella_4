<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Models\CmsCategories as Categories;
use Baseapp\Models\CmsArticles as Articles;

/**
 * Static Controller
 *
 */
class StaticController extends IndexController
{

    /**
     * Contact Action
     */
    public function contactAction()
    {
        $this->tag->setTitle('Contact');

        if ($this->request->isPost() === true) {
            $validation = new \Baseapp\Extension\Validation();

            $validation->add('fullName', new \Phalcon\Validation\Validator\PresenceOf());
            $validation->add('content', new \Phalcon\Validation\Validator\PresenceOf());
            $validation->add('content', new \Phalcon\Validation\Validator\StringLength(array(
                'max' => 5000,
                'min' => 10,
            )));
            $validation->add('email', new \Phalcon\Validation\Validator\PresenceOf());
            $validation->add('email', new \Phalcon\Validation\Validator\Email());
            $validation->add('repeatEmail', new \Phalcon\Validation\Validator\Confirmation(array(
                'with' => 'email',
            )));

            $validation->setLabels(array('fullName' => 'Full name', 'content' => 'Content', 'email' => 'Email', 'repeatEmail' => 'Repeat email'));
            $messages = $validation->validate($_POST);

            if (count($messages)) {
                $this->view->setVar('errors', $validation->getMessages());
                $this->flashSession->warning($this->tag->linkTo(array('#', 'class' => 'close', 'title' => 'Close', '×')) . '<strong>Warning!</strong> Please correct the errors.');
            } else {
                $this->flashSession->notice($this->tag->linkTo(array('#', 'class' => 'close', 'title' => 'Close', '×')) . '<strong>Success!</strong> Message was sent');

                $email = new \Baseapp\Library\Email();
                $email->prepare('Contact', $this->config->app->admin, 'contact', array(
                    'fullName' => $this->request->getPost('fullName'),
                    'email' => $this->request->getPost('email'),
                    'content' => $this->request->getPost('content'),
                ));
                $email->addReplyTo($this->request->getPost('email'));
                $email->Send();

                unset($_POST);
            }
        }
    }

    /**
     * Buy me some chocolate Action
     */
    public function buyAction()
    {
        $this->tag->setTitle('Buy chocolate');
        if ($this->request->isPost() == TRUE && $this->request->hasPost('quantity')) {
            $this->session->set('checkout', array(
                'price' => 1,
                'quantity' => $this->request->getPost('quantity'),
            ));

            $this->redirect_to('payment/checkout');
        }
    }

    /**
     * SEO helper action which forwards to viewAction with the appropriate params
     *
     * @return mixed
     */
    public function categorySlugAction()
    {
        // Removing first forward slash
        $full_url = ltrim($this->request->getURI(), '/');

        // Get everything before the question mark, effectively stripping the query portion of the url
        $full_slug = strtok($full_url, '?');

        // Forwarding to category action which knows how to deal with these things
        return $this->dispatcher->forward(
            array(
                'action' => 'category',
                'params' => array($full_slug)
            )
        );
    }

    public function categoryAction($categorySlug = null)
    {
        $do_404 = false;

        if (!$categorySlug) {
            $do_404 = true;
        }

        /* @var $category Categories */
        $find_params = array(
            'conditions' => 'url = :url: AND active = 1',
            'bind' => array(
                'url' => $categorySlug
            )
        );

        $category       = Categories::findFirst($find_params);
        $virtualRoot    = $category->getVirtualRootByTemplate();
        $templateName   = $virtualRoot ? $virtualRoot->getTemplateName() : $category->getTemplateName();
        $templatePath   = $virtualRoot ? $virtualRoot->getTemplatePath() : $category->getTemplatePath();
        $isFullPageWith = $virtualRoot ? $virtualRoot->isFullPageWith() : $category->isFullPageWith();

        if (Categories::TEMPLATE_PRICE_LIST === $templateName) {
            if ($category->id === $virtualRoot->id) {
                if ($directChildren = $category->getDirectChildren()) {
                    $articlesCategory = $directChildren[0];
                }
            } else {
                $articlesCategory = $category;
            }

            // load first article in this category
            if ($categoryArticles = $articlesCategory->getCategoryArticles()) {
                $firstArticle = $categoryArticles[0];
                return $this->dispatcher->forward(array(
                    'action'       => 'articleSlug',
                    'params'       => array($firstArticle->slug, 'categorySlug' => $firstArticle->getCategoryURI())
                ));
            }
        }

        if (!$category) {
            $do_404 = true;
        }

        if ($do_404) {
            return $this->trigger_404();
        }

        $this->load_zendesk_widget_script = $category->Settings->zendesk === 1;

        // Set page title
        $page_title = !empty($category->meta_title) ? $category->meta_title : $category->name;
        $this->tag->setTitle($page_title);

        $this->view->pick('static' . DIRECTORY_SEPARATOR . $templatePath);

        $this->view->setVar('virtualRoot', $virtualRoot);
        $this->view->setVar('category', $category);

        if ('' !== trim(strip_tags($category->excerpt))) {
            $content          = new \stdClass();
            $content->title   = $category->name;
            $content->excerpt = $category->excerpt;
            $this->view->setVar('content', $content);
        }

        $this->view->setVar('currentCategorySlug', $categorySlug);
        $this->setLayoutFeature('full_page_width', $isFullPageWith);
    }

    public function articleSlugAction($article_slug = null)
    {
        if (!$this->dispatcher->wasForwarded()) {
            $categorySlug = isset($this->router->getParams()['categorySlug']) ? $this->router->getParams()['categorySlug'] : null;
        } else {
            $categorySlug = isset($this->dispatcher->getParams()['categorySlug']) ? $this->dispatcher->getParams()['categorySlug'] : null;
        }

        if (!$categorySlug) {
            return $this->trigger_404();
        }

        $category     = $categorySlug ? Categories::findFirst(array(
            'conditions' => 'url = :url: AND active = 1',
            'bind' => array(
                'url' => $categorySlug
            )
        )) : null;

        if (!$article_slug || !$category) {
            return $this->trigger_404();
        }

        /* @var $article Articles */
        $find_params = array(
            'conditions' => 'category_id = :category_id: AND slug = :slug: AND active = 1',
            'bind' => array(
                'category_id' => $category->id,
                'slug'        => $article_slug
            )
        );

        if (!$article = Articles::findFirst($find_params)) {
            return $this->trigger_404();
        }

        // Set page title
        $page_title = !empty($article->meta_title) ? $article->meta_title : $article->title;
        $this->tag->setTitle($page_title);

        $virtualRoot    = $category->getVirtualRootByTemplate();
        $template       = $virtualRoot ? $virtualRoot->getTemplateName() : $category->getTemplateName();
        $templatePath   = $virtualRoot ? $virtualRoot->getTemplatePath() : $category->getTemplatePath();
        $isFullPageWith = $virtualRoot ? $virtualRoot->isFullPageWith() : $category->isFullPageWith();

        if (method_exists($this, 'setLayoutFeature') && $article->full_page_image == 1) {
            $templatePath   .= '-full-page-image';
            $isFullPageWith  = true;
        }

        if (Categories::TEMPLATE_DEFAULT === $template) {
            $this->assets->addCss('assets/vendor/slick/slick.css');
            $this->assets->addJs('assets/vendor/slick/slick.min.js');
            $this->assets->addCss('assets/vendor/gallery/css/blueimp-gallery.min.css');
            $this->assets->addJs('assets/vendor/gallery/js/blueimp-gallery.min.js');
            $this->load_zendesk_widget_script = $article->zendesk === 1;
        } else {
            $this->view->setVar('virtualRoot', $virtualRoot);
            $this->view->setVar('category', $category);
            $this->view->setVar('currentCategorySlug', $categorySlug);
            $this->view->setVar('currentArticleSlug', $article_slug);
            $this->load_zendesk_widget_script = $category->Settings->zendesk === 1;
        }
        $this->view->setVar('content_media', $article->get_media());

        $this->view->pick('static' . DIRECTORY_SEPARATOR . $templatePath);
        $this->view->setVar('content', $article);
        $this->setLayoutFeature('full_page_width', $isFullPageWith);
    }

}
