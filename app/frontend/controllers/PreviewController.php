<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Models\CmsCategories;
use Baseapp\Models\Media;
use Baseapp\Extension\Tag;


/**
 * Preview Controller
 *
 */
class PreviewController extends IndexController
{

    /**
     * Magazine article preview Action
     */
    public function magazineAction()
    {
        if ($this->request->isPost()) {
            $article = new \stdClass();
            $article->title = $this->request->getPost('title', null, 'No title yet');
            $article->excerpt = $this->request->getPost('excerpt', null);
            $article->content = $this->request->getPost('content', null, '');
            $article->publish_date = $this->request->getPost('publish_date', null);
            $article_media_ids = $this->request->getPost('article_media_gallery', null);
            $article_media = null;
            if ($article_media_ids) {
                $article_media_items = (new Media())->get_multiple_ids($article_media_ids);
                if ($article_media_items) {
                    $article_media = $article_media_items;
                }
            }
            $this->view->setVar('article_media', $article_media);

            $this->tag->setTitle($this->request->getPost('meta_title', null, $this->request->getPost('title', null, 'No title yet')));
            $this->view->pick('magazine/article');
            $this->view->setVar('article', $article);

            // setup category breadcrumbs
            if ($category_id = $this->request->getPost('category_id', 'int', null)) {
                if ($category = CmsCategories::findFirst($category_id)) {
                    $category_breadcrumbs = $category->getNodePath();
                    if ($category_breadcrumbs) {
                        $article_breadcrumbs = array();
                        $currBc = 0;
                        foreach ($category_breadcrumbs as $bc) {
                            $article_breadcrumbs[] = Tag::linkTo('magazine/' . $bc->url, (0 === $currBc ? 'Magazin' : $bc->name));
                            $currBc++;
                        }

                        if (count($article_breadcrumbs)) {
                            $article_breadcrumbs = implode('<span class="fa fa-angle-right fa-fw"></span>', $article_breadcrumbs);
                        }
                        $this->view->setVar('article_breadcrumbs', $article_breadcrumbs);
                    }
                }
            }
            $this->assets->addCss('assets/vendor/social-likes/social-likes_flat.css');
            $this->assets->addJs('assets/vendor/social-likes/social-likes.min.js');
            $this->assets->addCss('assets/vendor/slick/slick.css');
            $this->assets->addJs('assets/vendor/slick/slick.min.js');
            $this->assets->addCss('assets/vendor/gallery/css/blueimp-gallery.min.css');
            $this->assets->addJs('assets/vendor/gallery/js/jquery.blueimp-gallery.min.js');
            $this->assets->addJs('assets/js/magazine-article.js');
        }
    }

}
