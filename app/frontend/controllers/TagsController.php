<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Models\Tags;
use Baseapp\Library\Tool;

/**
 * Tags Controller
 *
 */
class TagsController extends IndexController
{
    /**
     * View Action
     */
    public function viewAction($entity_slug = null)
    {
        $do_404 = false;

        if (!$entity_slug) {
            $do_404 = true;
        }

        /* @var $entity Tags */
        $entity = Tags::findFirstBySlug($entity_slug);

        if (!$entity) {
            $do_404 = true;
        }

        if ($do_404) {
            return $this->trigger_404();
        }

        $page = 1;
        if ($this->request->hasQuery('page')) {
            $page = $this->request->getQuery('page', 'int', 1);
            if ($page <= 0) {
                $page = 1;
            }
        }

        $builder = $this->modelsManager->createBuilder();
        $builder->columns(array('article.*'));
        $builder->addFrom('Baseapp\Models\CmsArticles', 'article');
        $builder->innerJoin('Baseapp\Models\CmsArticlesTags', 'article.id = at.article_id', 'at');
        $builder->where('at.tag_id = :tag_id:', array('tag_id' => $entity->id));
        $builder->groupBy(array('article.id'));
//        $builder->orderBy($filter_sorting_order);

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            'builder' => $builder,
            'limit' => $this->config->settingsFrontend->pagination_items_per_page,
            'page' => $page
        ));
        $current_page = $paginator->getPaginate();

        $pagination_links = Tool::pagination(
            $current_page,
            'tag/' . $entity->slug,
            'pagination',
            $this->config->settingsFrontend->pagination_count_out,
            $this->config->settingsFrontend->pagination_count_in
        );

        $this->tag->setTitle(mb_strtoupper($entity->slug) . ' - Članci označeni tagom ' . mb_strtoupper($entity->slug) . ' | Oglasnik.hr');
        $this->view->pick('novosti/tags');
        $this->view->setVar('entity', $entity);
        $this->view->setVar('articles', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
    }

}
