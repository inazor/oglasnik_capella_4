<?php

namespace Baseapp\Frontend\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Models\AdsHomepage;
use Baseapp\Models\CmsArticlesMagazine as Article;
use Baseapp\Models\CmsCategories as Categories;
use Baseapp\Models\Ads as Ads;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;

/**
 * Magazine Controller
 */
class MagazineController extends IndexController
{
    /**
     * Index Action
     */
    public function indexAction()
    {
        $categories = array();

        $magazine_root_category = Categories::findFirst(array(
            'conditions' => 'type = :magazine_type: AND parent_id = 1',
            'bind' => array(
                'magazine_type' => Categories::TYPE_MAGAZINE
            )
        ));

        if ($magazine_root_category) {
            $category_objs = Categories::find(array(
                'conditions' => 'active = 1 AND lft >= :magazine_lft: AND rght <= :magazine_rght:',
                'bind'       => array(
                    'magazine_lft'  => $magazine_root_category->lft,
                    'magazine_rght' => $magazine_root_category->rght
                )
            ));
            if ($category_objs) {
                foreach ($category_objs as $category) {
                    $cat_builder = $this->modelsManager->createBuilder();
                    $cat_builder->columns(array('article.*'));
                    $cat_builder->addFrom('Baseapp\Models\CmsArticles', 'article');
                    $cat_builder->where('article.category_id = :category_id:',array('category_id' => $category->id));
                    if (!$this->canPreview()) {
                        $cat_builder->andWhere('article.active = 1 AND article.publish_date <= :publish_date:', array('publish_date' => $this->today_date_sql));
                    }
                    $cat_builder->groupBy(array('article.id'));
                    $cat_builder->limit(4);
                    $cat_builder->orderBy('article.publish_date DESC');
                    $cat_articles = $cat_builder->getQuery()->execute();
                    $cat_articles = Article::getEnrichedFrontendBasicResultsArray($cat_articles);

                    if ($cat_articles && count($cat_articles)) {
                        $categories[] = array(
                            'category' => $category,
                            'articles' => $cat_articles
                        );
                    }
                }
            }

            $this->tag->setTitle('Magazin | Oglasnik.hr');

            $this->assets->addJs('assets/vendor/imagesloaded.pkgd.min.js');
            $this->assets->addJs('assets/vendor/masonry.pkgd.min.js');
            $this->assets->addJs('assets/js/magazine.js');
            $this->view->setVar('articles_categories', $categories);
            $this->view->setVar('most_read', null);

/*  TODO: do we need this?
            $featured_ads = AdsHomepage::getAds();
            $this->view->setVar('homepage_featured_ads_markup', Ads::buildHomepageFeaturedAdsMarkup($featured_ads));
*/
        }
    }

    /**
     * Category Action
     */
    public function categoryAction($entity_url = null)
    {
        $do_404 = false;

        if (!$entity_url) {
            $do_404 = true;
        }

        /* @var $entity Categories */
        $entity = Categories::findFirstByUrl($entity_url);

        if (!$entity) {
            $do_404 = true;
        }

        if ($do_404) {
            return $this->trigger_404();
        }

        $page = 1;
        if ($this->request->hasQuery('page')) {
            $page = $this->request->getQuery('page', 'int', 1);
            if ($page <= 0) {
                $page = 1;
            }
        }

        $builder = $this->modelsManager->createBuilder();
        $builder->columns(array('article.*'));
        $builder->addFrom('Baseapp\Models\CmsArticles', 'article');
        $builder->where('article.active = 1 AND article.category_id = :category_id:', array('category_id' => $entity->id));
        if (!$this->canPreview()) {
            $builder->andWhere('article.publish_date <= :publish_date:', array('publish_date' => $this->today_date_sql));
        }
        $builder->groupBy(array('article.id'));
        $builder->orderBy('article.publish_date DESC');

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            'builder' => $builder,
            'limit'   => $this->config->settingsFrontend->pagination_items_per_page,
            'page'    => $page
        ));
        $current_page = $paginator->getPaginate();
        $current_page->items = Article::getEnrichedFrontendBasicResultsArray($current_page->items);

        $pagination_links = Tool::pagination(
            $current_page,
            'novosti/' . $entity->url,
            'pagination',
            $this->config->settingsFrontend->pagination_count_out,
            $this->config->settingsFrontend->pagination_count_in
        );

        if ($entity_settings = $entity->getSettings()) {
            $entity->set_meta_title($entity_settings->meta_title);
            $entity->set_meta_description($entity_settings->meta_description);
        }

        $this->tag->setTitle($entity->getSEOMeta('title') . ' - Magazin | Oglasnik.hr');
        $this->site_desc = $entity->getSEOMeta('description');
        $this->view->setVar('category', $entity);
        $this->view->setVar('breadcrumbs', $entity->getBreadcrumbs());
        $this->view->setVar('articles', $current_page->items);
        $this->view->setVar('pagination_links', $pagination_links);

        if (count($current_page->items)) {
            $this->assets->addJs('assets/vendor/imagesloaded.pkgd.min.js');
            $this->assets->addJs('assets/vendor/masonry.pkgd.min.js');
        }
        $this->assets->addJs('assets/js/magazine-category.js');
    }

    /**
     * Article Action
     */
    public function articleAction($entity_slug = null)
    {
        $do_404 = false;

        if (!$entity_slug) {
            $do_404 = true;
        }

        /* @var $entity Article */
        $find_params = array(
            'conditions' => 'slug = :slug: AND active = 1 AND publish_date <= :publish_date:',
            'bind' => array(
                'slug'         => $entity_slug,
                'publish_date' => $this->today_date_sql
            )
        );

        // Overriding default restrictive find conditions for those that can preview unpublished/inactive stuff
        if ($this->canPreview()) {
            $find_params['conditions'] = 'slug = :slug:';
            unset($find_params['bind']['publish_date']);
        }

        $entity = Article::findFirst($find_params);

        if (!$entity) {
            $do_404 = true;
        }

        if ($do_404) {
            return $this->trigger_404();
        }

        $title = isset($entity->meta_title) && !empty($entity->meta_title) ? $entity->meta_title : $entity->title;
        $this->tag->setTitle($title . ' - ' . $entity->getCategory()->name . ' - Magazin | Oglasnik.hr');
        $this->site_desc = $entity->getSEOMeta('description');
        $entity_media = $entity->get_media();

        $this->view->setVar('breadcrumbs', $entity->getBreadcrumbs());
        $this->view->setVar('article', $entity);
        $this->view->setVar('article_media', $entity_media);
        $this->view->setVar('latest_articles', Article::getLatest(4, 'CMS-70x70'));
        $this->view->setVar('simmilar_articles', $entity->getSimmilar(4));

        if (count($entity_media) > 1) {
            $this->assets->addCss('assets/vendor/gallery/css/blueimp-gallery.min.css');
            $this->assets->addJs('assets/vendor/gallery/js/blueimp-gallery.min.js');
        }
        $this->assets->addJs('assets/js/magazine-article.js');

        $articleViewFile = 'article';
        if (method_exists($this, 'setLayoutFeature') && $entity->full_page_image == 1) {
            $articleViewFile .= '-full-page-image';
            $this->setLayoutFeature('full_page_width', true);
        }

        $this->view->pick('magazine/' . $articleViewFile);
    }

    /**
     * Returns true if the current user should be able to see unpublished/inactive stuff
     *
     * @return mixed
     */
    protected function canPreview()
    {
        // TODO/FIXME: add a separate 'preview' role, or check for 'moderator' here or something...
        return $this->auth->logged_in('admin');
    }
}
