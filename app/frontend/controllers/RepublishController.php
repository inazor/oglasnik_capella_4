<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Bootstrap;
use Baseapp\Library\Products\GroupedProductsChooser;
use Baseapp\Library\Products\OrderableEnabledProductsSelectorManual;
use Baseapp\Library\Utils;
use Baseapp\Library\WspayHelper;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Models\Ads;
use Baseapp\Models\Categories;
use Baseapp\Models\Orders;
use Baseapp\Traits\BarcodeControllerMethods;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Dispatcher;
use Phalcon\Escaper;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Baseapp\Library\Validations\AdsFrontend as AdsValidationsFrontend;

class RepublishController extends IndexController
{
    use ParametrizatorHelpers;
    use BarcodeControllerMethods;

    protected $current_user;
    protected $cancel_url = null;

    const SESSION_TOKEN_NAME = 'tokens-republish';

    /**
     * @var GroupedProductsChooser|null
     */
    protected $products_chooser = null;

    // Load zendesk for the entire controller as requested
    protected $load_zendesk_widget_script = true;

    /**
     * Overriding beforeExecuteRoute in order to provide some global view variables and secure access.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::beforeExecuteRoute($dispatcher);

        // This controller should not be accessible for anon users
        if (!$this->auth->logged_in()) {
            $this->disable_view();
            $this->redirect_signin();
            return false;
        }
        $this->current_user = $this->auth->get_user();

        if (method_exists($this, 'setLayoutFeature')) {
            // turn off the search bar
            $this->setLayoutFeature('search_bar', false);
        }

        $this->view->setVar('current_action', $dispatcher->getActionName());
        $this->view->setVar('current_action_method', $dispatcher->getActiveMethod());
    }

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        // turn off all banner zones for this controller
        $this->view->setVar('banners', false);
    }

    protected function checkEntity(Ads $entity)
    {
        if (!$entity) {
            return false;
        }

        // Check that the current user is the owner of the Ad
        $owner = $entity->getUser();
        if ($owner && $owner->id != $this->current_user->id) {
            return false;
        }

        if (!$entity->canBeRepublished()) {
            return false;
        }

        return true;
    }

    protected function modelMessagesTransformer(Model $model)
    {
        // TODO: extend Phalcon\Validation\Message\Group to encapsulate this via __toString() or something...
        $msgs = array();
        foreach ($model->getMessages() as $msg) {
            $msgs[] = $msg->getMessage();
        }
        $msg_string = implode(', ', $msgs);

        return $msg_string;
    }

    /**
     * Creates a hexadecimal representation of a random token. Resembles
     * an SHA1 hash, but it's not. And it's better (or so they say).
     *
     * @link http://stackoverflow.com/a/14869745
     *
     * The resulting string will be twice as long as the random bytes we generate;
     * Each byte encoded to hex is 2 characters. 20 bytes will be 40 characters of hex.
     * Using 20 bytes, we have 256^20 or 1,461,501,637,330,902,918,203,684,832,716,283,019,655,932,542,976 unique
     * output values. This is identical to SHA1's 160-bit (20-byte) possible outputs.
     *
     * @return string
     */
    protected function createRandomToken()
    {
        $token = bin2hex(openssl_random_pseudo_bytes(20));

        return $token;
    }

    /**
     * Helper to encapsulate and reduce code duplication when checking required session token data
     * at various points in the ad republish stages
     *
     * @param array|null $data Session token data
     * @param array $keys List of key names in $data which have to exist and be non-empty
     *
     * @return bool False if any data is missing, true if everything's ok
     */
    protected function hasRequiredTokenData(array $data = null, $keys = array())
    {
        $errors = false;

        if (empty($data)) {
            $errors = true;
        }

        if (!is_array($keys) && is_string($keys)) {
            $keys = (array) $keys;
        }

        if (!$errors) {
            foreach ($keys as $key) {
                if (!isset($data[$key]) || empty($data[$key])) {
                    $errors = true;
                    break;
                }
            }
        }

        return !$errors;
    }

    /**
     * @param string $token Ad submission token
     * @param array $data Data to store (or merge if the token already exists)
     */
    protected function storeSessionTokenData($token, $data = array())
    {
        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());

        if (!isset($tokens[$token])) {
            $tokens[$token] = $data;
        } else {
            $tokens[$token] = array_merge($tokens[$token], $data);
        }

        $this->session->set(self::SESSION_TOKEN_NAME, $tokens);
    }

    /**
     * @param $token string Ad submission token
     * @param null $key Optional, specific key of data stored under the specified $token bucket. Returns null if key does not exist
     *
     * @return array|mixed|null
     */
    protected function getSessionTokenData($token, $key = null)
    {
        $data = null;

        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());

        if (isset($tokens[$token])) {
            $data = $tokens[$token];
            // Allows fetching a single key (if specified) from the store if it exists. Returns null if it doesn't.
            if (null !== $key) {
                if (isset($data[$key])) {
                    $data = $data[$key];
                } else {
                    $data = null;
                }
            }
        }

        return $data;
    }

    /**
     * @param $token
     *
     * @return bool
     */
    protected function deleteSessionTokenData($token)
    {
        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());
        if (isset($tokens[$token])) {
            unset($tokens[$token]);
            $this->session->set(self::SESSION_TOKEN_NAME, $tokens);
            return true;
        }

        return false;
    }

    /**
     * @param string|null $token
     *
     * @return int
     */
    protected function getStepForToken($token = null)
    {
        $step = 1;

        $stored_step = $this->getSessionTokenData($token, 'step');
        if (null !== $stored_step) {
            $step = $stored_step;
        }

        return $step;
    }

    /**
     * @param int $default
     *
     * @return bool True if all checks pass, false otherwise
     */
    protected function isRequestedStepAndTokenValid($default = 1)
    {
        $error = false;

        // Check for GET `step` parameter, falling back to possibly provided $default
        $specified_step = (int) $this->request->get('step', 'int', $default);
        if (!$specified_step || $specified_step < 0) {
            $error = true;
        }

        if (!$error && $specified_step > 1) {
            // First make sure we're within allowed upper bounds
            $steps       = $this->getWizardSteps();
            $steps_total = count($steps);
            if ($specified_step > $steps_total) {
                $error = true;
            }

            // Now make sure we have a token to work with
            $token = null;
            if (!$error) {
                $token = $this->request->get('token', 'string', null);
            }

            // Make sure session-stored step of the token is ok compared
            // to the one specified in the GET parameters
            if (!$error && $token) {
                // Requested step has to be lower or equal to what's stored for it on the server-side
                $stored_step = $this->getStepForToken($token);
                if (!($specified_step <= $stored_step)) {
                    $error = true;
                }

                // Also make sure the requested step actually exists among our defined steps
                if (!$error) {
                    if (!isset($steps[$specified_step])) {
                        $error = true;
                    }
                }
            }
        }

        return !$error;
    }

    /**
     * Builds and returns the "steps wizard" markup.
     *
     * @param int $active The step considered 'active' or 'current'
     * @param null|string $uri Link/href passed as 3rd parameter to `buildStepHref()`
     *
     * @return string
     */
    protected function buildStepsMarkup($active = 1, $uri = null)
    {
        $steps = $this->getWizardSteps();

        // Check if we're given a token via GET
        $token = $this->request->get('token', 'string', null);

        $markup = '<div class="row wizard-steps">';

        foreach ($steps as $k => $step_data) {
            $is_active    = ($k == $active);
            $is_done      = ($k < $active);

            $classnames = array();
            if ($is_active) {
                $classnames[] = 'active';
            }
            if ($is_done) {
                $classnames[] = 'done';
            }

            $step_markup = '<span class="wizard-step">' . $step_data['title'] . '</span>';
            if (!$is_active) {
                // Current step not active, but not everything should be clickable, depending on where
                // we are in the process

                // TODO: determine if something else needs to be clickable (and when, if so)
                $do_link = false;

                // If we have a token, check how far we've come according to the
                // token info from the session and do not build links for stuff we haven't
                // already reached
                if ($token) {
                    $token_step = $this->getStepForToken($token);
                    if ($k <= $token_step) {
                        $do_link = true;
                    }
                }

                if ($do_link) {
                    $step_link   = $this->buildStepHref($k, $token, $uri);
                    $step_markup = sprintf('<a href="%s">%s</a>', $step_link, $step_markup);
                }
            }

            $classnames_string = '';
            if (!empty($classnames)) {
                $classnames_string = ' ' . implode(' ', $classnames);
            }

            $markup .= '<div class="col-sm-3 col-xs-6' . $classnames_string . '">';
            $markup .= $step_markup;
            $markup .= '</div>';
        }

        $markup .= '</div>';

        return $markup;
    }

    /**
     * @param int $step
     *
     * @return int
     */
    protected function getCurrentStep($step = 1)
    {
        $checks_ok = $this->isRequestedStepAndTokenValid($step);

        if ($checks_ok) {
            $step = $this->request->get('step', 'int', $step);
        }

        $steps = $this->getWizardSteps();
        if (!isset($steps[$step])) {
            $step = 1;
        }

        return (int) $step;
    }

    protected function buildStepHref($step = 1, $token = null, $uri = 'republish')
    {
        $query = $this->request->getQuery();
        unset($query['_url']);

        if ($step >= 1) {
            $query['step'] = $step;
        }

        if (null !== $token) {
            $query['token'] = $token;
        }

        // having ?step=1 is not needed
        if (1 === $step) {
            unset($query['step']);
        }

        $href = $this->url->get($uri, $query);
        return $href;
    }

    /**
     * @return array
     */
    protected function getWizardSteps()
    {
        static $steps = array(
            1 => array(
                'view'  => 'chunks/ads-submission-edit',
                'title' => 'Detalji oglasa',
            ),
            2 => array(
                'view'  => 'chunks/ads-submission-products-chooser',
                'title' => 'Odabir vrste oglasa'
            ),
            3 => array(
                'view'  => 'chunks/ads-submission-payment',
                'title' => 'Plaćanje',
            )
        );

        return $steps;
    }

    /**
     * @param string|null $token
     * @param array|null $token_data
     * @param string|null $message
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    protected function redirectToLastKnownStepWithMessage($token, $token_data, $message = null)
    {
        if (null !== $message) {
            $this->flashSession->error($message);
        }

        if (null !== $token_data && is_array($token_data) && !empty($token_data)) {
            $last_reached_step = $token_data['step'];
        } else {
            $last_reached_step = 1;
        }

        return $this->redirect_to($this->buildStepHref($last_reached_step, $token));
    }

    protected function setupProductsChooser(Ads $ad)
    {
        $post_data = $this->request->getPost();

        $ad_products = $ad->getProducts();

        $selection = new OrderableEnabledProductsSelectorManual($ad->getCategory(), $ad->User, $post_data, $ad_products);
        $selection->process();
        $chooser = new GroupedProductsChooser($selection);

        $chosen_products = $chooser->getChosenProducts('grouped');
        $billing_info = array();
        foreach ($chosen_products as $group => $group_products) {
            foreach ($group_products as &$product) {
                $billing_info[$group][] = $product->getShoppingCartInfo();
            }
        }

        $this->assets->addJs('assets/js/classified-submit-choose-ad-type.js');

        // Fake the offline group checkbox if an offline product is preselected
        $post_data = $this->request->getPost();
        if (!empty($ad_products['offline'])) {
            $post_data['offline-products'] = true;
        }

        $this->view->setVar('products_chooser_markup', $chooser->getMarkup($post_data));
        $this->view->setVar('products_cost_total', $chooser->getChosenProductsTotalCostDisplay());
        $this->view->setVar('billing_info', $billing_info);
    }

    /**
     * @param $category_id
     *
     * @return Categories|false
     */
    protected function getCategoryIfExists($category_id)
    {
        $category = false;
        if ($category_id) {
            $category = Categories::findFirst($category_id);
        }

        return $category;
    }

    public function addParametrizationAssets()
    {
        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addCss('assets/vendor/datepicker3.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');
        $this->assets->addJs('assets/vendor/jquery.autoNumeric.js');
        $this->assets->addCss('assets/vendor/fancyBox/source/jquery.fancybox.css');
        $this->assets->addJs('assets/vendor/fancyBox/source/jquery.fancybox.pack.js');
    }

    protected function getAdPreview($ad, $rendered_markup, $extras = array())
    {
        $page_title = sprintf('Pregled oglasa prije predaje');
        $this->tag->setTitle($page_title . ' - ' . $ad->title);

        // Load custom assets if needed
        if (isset($rendered_markup['assets'])) {
            $this->setup_assets($rendered_markup['assets']);
        }
        // Load regular assets for the ad view
        $this->assets->addJs('assets/js/classified-view.js');

        $entity_user = $ad->getUserDetails();
        // Check if the ad itself has custom phone numbers on it... if so,
        // then we have to remove phone numbers from $entity_user and replace them
        // with the ones found in the ad
        if ((isset($ad->phone1) && trim($ad->phone1)) || (isset($ad->phone2) && trim($ad->phone2))) {
            $entity_user->phone1 = $ad->phone1;
            $entity_user->phone2 = $ad->phone2;
        }
        $this->view->setVar('ad_user', $entity_user);

        // Quick hack to allow passing additional view data/vars from the action
        if (!empty($extras)) {
            foreach ($extras as $k => $v) {
                $this->view->setVar($k, $v);
            }
        }

        $this->view->setVar('ad', $ad);
        $this->view->setVar('ad_render', $rendered_markup);
        $this->view->pick('chunks/ads-submission-preview');
    }


    public function indexAction($entity_id = null)
    {
        if ($entity_id === null) {
            $this->flashSession->error('Neispravni parametri ponavljanja oglasa');
            return $this->redirect_to('moj-kutak');
        }

        /**
         * @var Ads $ad
         */
        $ad = Ads::findFirst($entity_id);
        $ok = $this->checkEntity($ad);

        if (!$ok) {
            $this->flashSession->error('Neispravni parametri ponavljanja oglasa');
            return $this->redirect_to('moj-kutak');
        }

        $this->view->setVar('ad', $ad);
        $this->view->setVar('stepType', 'republish');

        // Perform sanity checks for requested step and token vars, and if something is not quite right
        // redirect back to the beginning
        $sanity_checks_ok = $this->isRequestedStepAndTokenValid();
        if (!$sanity_checks_ok) {
            // Bail back to the beginning if anything is weird
            return $this->redirect_to('republish/' . $entity_id);
        }

        // Prep some variables
        $steps             = $this->getWizardSteps();
        $current_step      = $this->getCurrentStep();
        $current_step_data = $steps[$current_step];

        // Build wizard view markup
        $this->view->setVar('steps_wizard_markup', $this->buildStepsMarkup($current_step, 'republish/' . $entity_id));

        // Set appropriate page title
        $page_title = sprintf('Ponavljanje oglasa - Korak %s - %s', $current_step, $current_step_data['title']);
        $this->tag->setTitle($page_title);

        $this->assets->addJs('assets/js/ie-11-nav-cache-bust.js');

        // Grab/set common vars
        $token        = $this->request->get('token', 'string', null);
        $token_data   = $this->getSessionTokenData($token);
        $preview_data = $this->request->has('preview') && isset($token_data['preview']) ? $token_data['preview'] : null;

        if (isset($token_data['cancel_url']) && !empty($token_data['cancel_url'])) {
            $this->cancel_url = trim($token_data['cancel_url']);
        }
        if (!$this->cancel_url) {
            $this->cancel_url = trim($this->get_redir_path_from_current_request('moj-kutak'));
        }

        switch ($current_step) {
            // Ad edit form
            case 1:
            default:
                if ($preview_data) {
                    $result = $this->getAdPreview(
                        $ad,
                        $preview_data['rendered_markup'],
                        $preview_data['extra_view_data']
                    );
                } else {
                    $result = $this->processStep1($ad, $token, $token_data);
                }
                break;
            // Products chooser, potentially creating an order upon submit (if not a free ad)
            case 2:
                $result = $this->processStep2($ad, $token, $token_data);
                break;
            // Payment/Order recap
            case 3:
                $result = $this->processStep3($ad, $token, $token_data);
                break;
        }

        // If any of the step $result above is already a ResponseInterface instance, return that
        if ($result instanceof ResponseInterface) {
            return $result;
        }

        if (!$preview_data) {
            // Pick the "current" step's view
            $this->view->pick($current_step_data['view']);
            $this->view->setVar('stepType', 'republish');
        }
    }

    /**
     * Handles showing/saving the parametrized Ad form
     *
     * @param string $token
     * @param array|null $token_data
     *
     * @return mixed|ResponseInterface
     */
    protected function processStep1($ad, $token = null, $token_data = array())
    {
        if (!$ad || !isset($ad->id) || empty($ad->id)) {
            // This should not happen under normal circumstances
            return $this->trigger_404();
        }

        // Check that the specified category actually exists
        $category_id = isset($ad->category_id) && !empty($ad->category_id) ? $ad->category_id : null;
        $category    = $this->getCategoryIfExists($category_id);
        if (!$category) {
            // This should not happen under normal circumstances (since
            // we shouldn't even be storing token data for a non-existing category)
            return $this->trigger_404();
        }
        $this->view->setVar('category', $category);

        // We could have an order created earlier on step 2 and then have gone back to change it completely
        $order = null;
        if (isset($token_data['order_id']) && !empty($token_data['order_id'])) {
            $order = Orders::findFirst($token_data['order_id']);
        }

        if (null === $token) {
            $token = $this->createRandomToken();
        }

        // Load the ad form page, allowing submit/edit

        // TODO: parametrizator will need a rewrite, I feel it's doing a lot of things that could be simplified
        $parametrizator = new Parametrizator();
        $parametrizator->setModule('frontend');
        $parametrizator->setCategory($ad->getCategory());
        $parametrizator->setAd($ad);

        $show_ad_form = true;
        if ($this->request->isPost()) {
            if ($this->request->getPost('next') == 'preview') {
                // For now, previewing requires all validation to pass in order
                // to generate ad's preview. This is wrong, but there's no other
                // way for now, unless core is rewritten :/
                $validation = new AdsValidationsFrontend();
                $validation = $validation->get();
                $parametrizator->setValidation($validation);
                $parametrizator->setRequest($this->request);
                $messages = $parametrizator->prepare_and_validate();

                if (count($messages)) {
                    $this->view->setVar('errors', $messages);
                    $this->flashSession->error('<strong>Greška!</strong> Ispravite sva crvena polja.');
                    $parametrizator->setErrors($messages);
                } else {
                    $parametrizator->parametrize();
                    $next_step = 1;
                    $back_markup = '<form action="' . $this->buildStepHref($next_step, $token, 'republish/' . $ad->id) . '" method="post"><input type="hidden" name="post_field" value="' . base64_encode(json_encode($this->request->getPost())) . '"/><button type="submit" name="save" value="back-from-preview" class="btn btn-primary"><span class="fa fa-fw fa-arrow-circle-left"></span> Povratak na uređivanje oglasa</button></form>';

                    $extra_view_data = array(
                        'back_markup' => $back_markup
                    );
                    $show_ad_form = false;
                    $preview_data = array(
                        'ad'              => $parametrizator->getAd(),
                        'extra_view_data' => $extra_view_data
                    );
                    $rendered_markup = $preview_data['ad']->getRenderedAd();
                    $rendered_media = isset($rendered_markup['ad_media']) ? $rendered_markup['ad_media'] : null;
                    $rendered_markup['ad_media'] = null;

                    if ($rendered_media) {
                        foreach ($rendered_media as $media) {
                            $rendered_markup['ad_media'][] = $media;
                        }
                    }

                    $preview_data['rendered_markup'] = $rendered_markup;
                    $this->storeSessionTokenData($token, array('ad_id' => $ad->id, 'step' => $next_step, 'preview' => $preview_data, 'cancel_url' => $this->cancel_url));
                    return $this->redirect_to($this->buildStepHref($next_step, $token, 'republish/' . $ad->id . '?preview'));
                }
            } elseif ($this->request->getPost('save') == 'back-from-preview') {
                $parametrizator->setData(json_decode(base64_decode($this->request->getPost('post_field')), true));
            } elseif ($this->request->getPost('cancel') == 'cancel') {
                if ($order) {
                    $order->save(array('status' => Orders::STATUS_CANCELLED));
                }
                $this->deleteSessionTokenData($token);
                $show_ad_form = false;
                return $this->redirect_to($this->cancel_url);
            } elseif ($this->request->hasPost('next')) {
                $saved = $ad->frontend_save_changes($this->request);

                if ($saved instanceof Ads) {
                    $next_step = 2;
                    $this->storeSessionTokenData($token, array('ad_id' => $saved->id, 'step' => $next_step, 'cancel_url' => $this->cancel_url));
                    return $this->redirect_to($this->buildStepHref($next_step, $token, 'republish/' . $saved->id));
                } else {
                    $this->view->setVar('errors', $saved);
                    $this->flashSession->error('<strong>Greška!</strong> Ispravite sva crvena polja.');
                    $parametrizator->setRequest($this->request);
                    $parametrizator->setErrors($saved);
                }

            }
        } else {
            $parametrizator->setData($ad->getData());
        }

        if ($show_ad_form) {
            $this->view->setVar('ad', $ad);
            $this->view->setVar('curr_url', $this->buildStepHref(1, $token, 'republish/' . $ad->id));
            $this->addParametrizationAssets();
            $currUser = $this->auth->get_user();
            $phoneNumberType = $currUser && $currUser->country_id ? ($currUser->country_id == 1 ? 'domestic' : 'foreign') : 'auto';
            $this->view->setVar('phoneNumberType', $phoneNumberType);
            $this->assets->addJs('assets/js/classified-submit-classified-edit.js');
            $this->setup_rendered_parameters($parametrizator->render_input());
            $this->storeSessionTokenData($token, array('ad_id' => $ad->id, 'step' => 1, 'cancel_url' => $this->cancel_url));
        }
    }

    /**
     * Handles the Products chooser, creating/updating the potential Order upon submit
     *
     * @param Ads $ad
     * @param string|null $token
     * @param array|null $token_data
     *
     * @return void|ResponseInterface
     */
    protected function processStep2(Ads $ad, $token = null, $token_data = array())
    {
        // We could have an order created earlier on step 3 and then have gone back to change it completely
        $order = null;
        if (isset($token_data['order_id']) && !empty($token_data['order_id'])) {
            $order = Orders::findFirst($token_data['order_id']);
        }

        if (null === $token) {
            $token = $this->createRandomToken();
        }

        $this->setupProductsChooser($ad);

        $show_offline_phone_warning = false;
        if (!$ad->phone1 && !$ad->phone2) {
            $show_offline_phone_warning = true;
        }
        $this->view->setVar('show_offline_phone_warning', $show_offline_phone_warning);

        if ($this->request->hasPost('next')) {
            $result = $this->processStep2Submit($ad, $order, $token, 'offline');
            if ($result instanceof ResponseInterface) {
                return $result;
            } elseif ($result instanceof Validation\Message\Group) {
                $this->view->setVar('errors', $result);
            }
        } elseif ($this->request->getPost('cancel') == 'cancel') {
            if ($order) {
                $order->save(array('status' => Orders::STATUS_CANCELLED));
            }
            $this->deleteSessionTokenData($token);
            return $this->redirect_to($this->cancel_url);
        }
    }

    /**
     * Handles saving chosen products and creating an Order if needed and redirecting to step 3.
     * Also deals with saving/publishing a free Ad and redirecting to "/moj-kutak"
     *
     * @param Ads $ad
     * @param Orders|null $order
     * @param string|null $token
     * @param string $payment_method Optional, defaults to 'offline'
     *
     * @return ResponseInterface|Validation\Message\Group
     */
    protected function processStep2Submit(Ads $ad, Orders $order = null, $token = null, $payment_method = 'offline')
    {
        $product_chooser_results = $ad->getAdRepublishChosenProductsAndPaymentState($this->request);
        if (Ads::PAYMENT_STATE_NEW !== $product_chooser_results['payment_state']) {
            // If chosen products require any kind of payment, create an order, store it
            // in session token data storage and forward to the payment page/controller (which is a redirect
            // to step 3 currently, but it will probably change in the future)

            if (Ads::PAYMENT_STATE_ORDERED === $product_chooser_results['payment_state']) {
                // Creates a new or updates an existing order (depending on value of $order)
                $order = Orders::saveForAd(
                    $ad,
                    $order,
                    $product_chooser_results['products']['all'],
                    array(
                        'payment_method' => $payment_method,
                        /**
                         * This makes sure that in a complicated case involving:
                         *
                         * - 1. create an order
                         * - 2. go back, change it to free stuff (which marks the order as STATUS_CANCELLED)
                         * - 3. go back again somehow skipping finishing-up ad-submission with free stuff (which
                         *      should not happen, but, things break, yes?) and changing the products to something
                         *      paid-for (again, somehow)
                         *
                         * we don't end up with an Order with a STATUS_CANCELED state which should really not
                         * be marked as cancelled (well, it was, once, but it was updated again and it now again
                         * requires fulfillment due to having paid-for stuff assigned to this ad).
                         *
                         * So we make sure here that order status always gets set appropriately.
                         *
                         * We could in theory add another STATUS_ flag for this special case if we're
                         * interested in how many people actually do this kind of thing at all...
                         */
                        'status'         => Orders::STATUS_NEW
                    )
                );

                // If successful, go to step 3, otherwise stay and show errors
                if ($order instanceof Orders) {
                    $ad->latest_payment_state = Ads::PAYMENT_STATE_ORDERED;
                    $ad->update();

                    $next_step = 3;
                    $this->storeSessionTokenData(
                        $token,
                        array(
                            'ad_id'      => $ad->id,
                            'step'       => $next_step,
                            'order_id'   => $order->id,
                            'cancel_url' => $this->cancel_url
                        )
                    );

                    $url = $this->buildStepHref($next_step, $token, 'republish/' . $ad->id);
                    return $this->redirect_to($url);
                } else {
                    $this->flashSession->error('<strong>Greška!</strong> Došlo je do greške prilikom spremanja narudžbe.');

                    $errors         = new Validation\Message\Group();
                    $errors->appendMessage(new Validation\Message($this->modelMessagesTransformer($order)));

                    return $errors;
                }
            } else {

                // There's a special case here in which the user could've chosen
                // some paid products earlier, but then went back and changed it to
                // just free products... In which case our Order becomes useless.
                // So gonna mark it as canceled here for now.
                if ($order) {
                    $order->save(array('status' => Orders::STATUS_CANCELLED));
                }

                // No payments required of any kind, set success and clear current token's
                // stored session data and redirect the user somewhere?
                $this->deleteSessionTokenData($token);
                $ad->latest_payment_state = Ads::PAYMENT_STATE_FREE;
                $ad->published_at = Utils::getRequestTime();
                $ad->sort_date    = Utils::getRequestTime();

                $online_product        = $product_chooser_results['products']['online'];
                $ad->online_product_id = $online_product->getId();
                $ad->online_product    = serialize($online_product);
                $ad->modifyWithProduct($product_chooser_results['products']['online']);

                $offline_product       = null;
                if (isset($product_chooser_results['products']['offline'])) {
                    $offline_product = $product_chooser_results['products']['offline'];
                    $ad->offline_product_id = $offline_product->getId();
                    $ad->offline_product    = serialize($offline_product);
                } else {
                    $ad->offline_product_id = null;
                    $ad->offline_product    = null;
                }

                $ad->verify_and_publish_ad('frontend');
                // clear info that ad is sold!
                $ad->sold = 0;
                $ad->save();
                $this->saveAudit('Ad with free product(s) republished.');

                // TODO: There are probably various different messages we need to show based on
                // different moderation statuses and moderation requirements, no?
                if ('ok' !== $ad->moderation) {
                    // Moderation different than 'ok'
                    $this->flashSession->success('<strong>Super!</strong> Vaš oglas je uspješno produljen!');
                    return $this->redirect_to('moj-kutak/svi-oglasi');
                } else {
                    // Moderation 'ok' -> we're golden...
                    $this->flashSession->success('<strong>Super!</strong> Vaš oglas je uspješno produljen!');
                    $link = $ad->get_frontend_view_link();
                    return $this->redirect_to($link);
                }
            }
        } else {
            $this->flashSession->error('<strong>Greška!</strong> Greška prilikom odabira proizvoda.');
        }
    }

    /**
     * Handles payment/order recap + payment method chooser + cc payment provider handling + finish up when done
     *
     * @param Ads $ad
     * @param null $token
     * @param array|null $token_data
     *
     * @return ResponseInterface
     */
    protected function processStep3(Ads $ad, $token = null, $token_data = array())
    {
        if (!$this->hasRequiredTokenData($token_data, array('order_id'))) {
            return $this->redirectToLastKnownStepWithMessage($token, $token_data);
        }

        $order_id = $token_data['order_id'];
        $order    = Orders::findFirst($order_id);

        if (!$order) {
            $message = '<strong>Ooops!</strong> Nije moguće pronaći podatke o vašoj narudžbi!';
            return $this->redirectToLastKnownStepWithMessage($token, $token_data, $message);
        }

        /**
         * zyt: 23.09.2016.
         * The Order we're currently working with could've been cancelled either via /moj-kutak/otkazi-narudzbu or
         * by some other means in between the time it was created and now that we landed on this url again...
         * In case we're here and it's a cancelled order, handle all the things that are done
         * on processStep2Submit in order to avoid duplicating code (and so that we don't have
         * ads with latest_payment_state=cancelled when we're about to re-new the now-cancelled
         * order that still appears to be existing in the session since step3 was never fully
         * submitted).
         */
        if ($order->status == Orders::STATUS_CANCELLED) {
            $result = $this->processStep2Submit($ad, $order, $token);
            if ($result instanceof ResponseInterface) {
                return $result;
            }/* elseif ($result instanceof Validation\Message\Group) {
                $this->view->setVar('errors', $result);
            }*/
        }

        if ($this->request->hasPost('next')) {
            // We're done here, the order should be fulfilled offline
            $this->deleteSessionTokenData($token);
            $this->flashSession->success('<strong>Super!</strong> Vaša narudžba je zaprimljena i čeka aktivaciju!');
            return $this->redirect_to('moj-kutak');
        }

        // Build payment thing
        $url_args = array(
            'step'  => 3,
            'token' => $token
        );
        $payment_data = array(
            'ShoppingCartID' => $order->getPbo(),
            'TotalAmount'    => $order->getTotal(),
            'ReturnURL'      => $this->url->get('republish/' . $ad->id, $url_args + ['return' => 1]),
            'CancelURL'      => $this->url->get('republish/' . $ad->id, $url_args + ['cancel' => 1]),
            'ReturnErrorURL' => $this->url->get('republish/' . $ad->id, $url_args + ['error' => 1])
        );
        $helper = new WspayHelper($payment_data, $this->current_user);

        // Example error request we get from WSPay:
        if ($this->request->getQuery('error')) {
            // republishr/$ad_id?step=3&token=83b6fef8f17b772c417f4d8587631b87aafb1032&error=1&CustomerFirstname=sda
            // &CustomerSurname=asd&CustomerAddress=neka+tamo&CustomerCountry=Hrvatska&CustomerZIP=da+nebi
            // &CustomerCity=ma+je&CustomerEmail=asd@asdf.com&CustomerPhone=123456&ShoppingCartID=900000164
            // &Lang=HR&DateTime=20150621022730&Amount=350&ECI=&PaymentType=VISA&PaymentPlan=0000
            // &ShopPostedPaymentPlan=0000&Success=0&ApprovalCode=&ErrorMessage=ODBIJENO
            $escaper = new Escaper();
            $cart_id = $this->request->getQuery('ShoppingCartID', 'string');
            $remote_message = $escaper->escapeHtml($this->request->getQuery('ErrorMessage', 'string'));
            $data = json_encode($this->request->getQuery());

            $log_message = 'WSPay returned an error for Order #' . $escaper->escapeHtml($cart_id) . ': ' . $remote_message;
            $log_message .= "\n" . $data;
            $this->saveAudit($log_message);

            $this->flashSession->error('<strong>Greška</strong> WSPay vratio: ' . $remote_message . '. Molimo pokušajte ponovo ili odaberite drugi način plaćanja.');
        } elseif ($this->request->getQuery('cancel')) {
            // TODO: Do we even have to bother with anything in case of cancel? Why?
        } elseif ($this->request->getQuery('return') && $this->request->getQuery('Success')) {
            // Verifying received data from WSpay
            $query_vars = $this->request->getQuery();
            $valid_signature = $helper->verifyReturnedSignature($query_vars);
            if (true === $valid_signature) {
                // Great success!
                $save_data = array(
                    'payment_method' => strtolower($this->request->getQuery('PaymentType', 'string')),
                    'approval_code'  => $this->request->getQuery('ApprovalCode')
                );
                $finalized = $order->finalizePurchase($save_data);
                if ($finalized) {
                    $this->deleteSessionTokenData($token);
                    $this->flashSession->success('<strong>Odlično!</strong> Vaša narudžba je uspješno obrađena!');
                    return $this->redirect_to('moj-kutak');
                } else {
                    $this->flashSession->error('<strong>Greška!</strong> Izvršenje plaćene narudžbe nije uspjelo. Molimo kontaktirajte korisničku podršku!');
                }
            }
        }

        // Display payment method choices
        $this->view->setVar('order_total_formatted', Utils::format_money($order->getTotal(), true));
        $this->view->setVar('order_pbo', $order->getPbo());
        $this->view->setVar('order_table_markup', $order->getPaymentRecapTableMarkup());
        $this->view->setVar('rendered_form',  $helper->getFormMarkup());

        // $barcode_src = $this->url->get('republish/barcode', array('token' => $token));
        $barcode_src = $order->get2DBarcodeInlineSvg();
        $this->view->setVar('order_id', $order->id);
        $this->view->setVar('payment_data_table_markup', Orders::buildPaymentDataTableMarkup($order, $barcode_src));
        $this->view->setVar('offline_button_markup', Orders::buildOfflineConfirmButtonMarkup('republish', 'Predaj oglas'));

        $this->assets->addJs('assets/js/payment-method-toggle.js');
    }
}
