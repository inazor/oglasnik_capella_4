<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Traits\ErrorControllerTrait;

class ErrorController extends IndexController
{
    use ErrorControllerTrait;
}
