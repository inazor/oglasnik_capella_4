<?php

namespace Baseapp\Models;

use Baseapp\Extension\Image;
use Baseapp\Library\Images\Thumb;
use Baseapp\Library\Utils;
use Phalcon\Di;

class UsersProfileImages extends BaseModelBlamable
{
    protected $id;
    public $user_id;

    public $avatar;
    public $avatar_details;

    public $profile_bg;
    public $profile_bg_details;

    static $base_dir;
    static $base_uri;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'      => 'User',
                'reusable'   => true,
            )
        );
    }

    public function getSource()
    {
        return 'users_profile_images';
    }

    public function onConstruct()
    {
        $config = $this->getDI()->get('config');

        self::$base_dir = $config->app->uploads_dir;
        self::$base_uri = $config->app->uploads_uri;
    }

    public static function getAvatarUploadsUri()
    {
        self::checkConfig();

        return self::$base_uri . '/avatars';
    }

    public static function getAvatarUploadsDir()
    {
        self::checkConfig();

        return self::$base_dir . '/avatars';
    }

    public static function getBgsUploadsUri()
    {
        self::checkConfig();

        return self::$base_uri . '/bgs';
    }

    public static function getBgsUploadsDir()
    {
        self::checkConfig();

        return self::$base_dir . '/bgs';
    }

    public static function checkConfig()
    {
        static $config = null;

        if (null === $config) {
            $config = Di::getDefault()->getShared('config');

            if (empty(self::$base_dir)) {
                self::$base_dir = $config->app->uploads_dir;
            }
            if (empty(self::$base_uri)) {
                self::$base_uri = $config->app->uploads_uri;
            }
        }
    }

    public function deleteAvatar()
    {
        $done = false;

        if (!empty($this->avatar)) {
            $details = json_decode($this->avatar_details, true);
            if (!empty($details['path'])) {
                $deleted = unlink($details['path']);
                if (!$deleted) {
                    if (isset($details['path_root'])) {
                        $deleted = unlink(ROOT_PATH . $details['path_root']);
                    }
                }
                $this->avatar = null;
                $this->avatar_details = null;
                $done = true;
            }
        }

        if ($done) {
            $done = $this->save();
        }

        return $done;
    }

    public function deleteProfileBg()
    {
        $done = false;

        if (!empty($this->profile_bg)) {
            $details = json_decode($this->profile_bg_details, true);
            if (!empty($details['path'])) {
                $deleted = unlink($details['path']);
                if (!$deleted) {
                    if (isset($details['path_root'])) {
                        $deleted = unlink(ROOT_PATH . $details['path_root']);
                    }
                }
                $this->profile_bg = null;
                $this->profile_bg_details = null;
                $done = true;
            }
        }

        if ($done) {
            $done = $this->save();
        }

        return $done;
    }

    public function assignNewProfileBg($filename, array $details = array())
    {
        $this->deleteProfileBg();

        if (!isset($details['path'])) {
            throw new \Exception('Missing cover bg path details');
        }

        $created = $this->resizeProfileBg(ROOT_PATH . $details['path_root']);
        // $created = true; // use whatever is uploaded for now directly...
        if ($created) {
            $this->profile_bg = $filename;
        }

        if (!empty($details)) {
            $this->profile_bg_details = json_encode($details);
        }

        return $this->save();
    }

    public function assignNewAvatar($filename, array $details = array())
    {
        $this->deleteAvatar();

        if (!isset($details['path'])) {
            throw new \Exception('Missing avatar path details');
        }

        $created = $this->resizeAvatar(ROOT_PATH . $details['path_root']);
        if ($created) {
            $this->avatar = $filename;
        }

        if (!empty($details)) {
            $this->avatar_details = json_encode($details);
        }

        return $this->save();
    }

    public function resizeAvatar($path)
    {
        $style = $this->getAvatarStyle();
        $image = new Image($path, $style->width, $style->height);

        // Auto rotate as soon as we open it, so anything done further should be fine
        $im     = $image->getInternalImInstance();
        $format = strtolower($im->getImageFormat());
        if ('jpeg' === $format) {
            $image->auto_rotate();
        }
        unset($im, $format);

        // Store original dimensions
        $orig_w = $image->getWidth();
        $orig_h = $image->getHeight();

        // TODO: can handle CMYK crap if it becomes an issue in the future...
        // $image->handle_cmyk();

        // If there's a stored crop definition (or the style specifies a crop)
        $do_crop       = false;
        if ($style->crop) {
            $do_crop                 = true;
            $existing_crop           = array();
            $existing_crop['width']  = $style->width;
            $existing_crop['height'] = $style->height;
        }

        // Do the cropping if needed
        if ($do_crop) {
            if (isset($existing_crop['x']) && isset($existing_crop['y'])) {
                // Exact coordinates exist
                $image->crop($existing_crop['width'], $existing_crop['height'], $existing_crop['x'], $existing_crop['y']);
            } else {
                // No previous specific crop record, try smart fitting without any upsizing for now
                $image->fit($existing_crop['width'], $existing_crop['height'], function($constraint) {
                    $constraint->upsize();
                });
            }

            // Enforce cropped dimensions in case the result is smaller than what the style dictates
            // NB: Only 'hard-crop' styles enforce this (for now at least), the other possible use-case
            // might come from a stored precise crop definition without the style itself having the
            // crop flag - that's why this if is here...
            if ($style->crop) {
                $actual_w      = $image->getWidth();
                $actual_h      = $image->getHeight();
                $target_width  = ($actual_w < $style->width) ? $style->width : null;
                $target_height = ($actual_h < $style->height) ? $style->height : null;
                if ($target_width || $target_height) {
                    $image->resize_canvas($target_width, $target_height);
                }
            }
        }

        // Now resize the (even potentially cropped) image if needed, hopefully avoiding upscaling in all cases
        $image->resize_down($style->width, $style->height);

        return $image->save($path);
    }

    public function resizeProfileBg($path)
    {
        $style = $this->getProfileBgStyle();
        $image = new Image($path, $style->width, $style->height);

        // Auto rotate as soon as we open it, so anything done further should be fine
        $im     = $image->getInternalImInstance();
        $format = strtolower($im->getImageFormat());
        if ('jpeg' === $format) {
            $image->auto_rotate();
        }
        unset($im, $format);

        // Store original dimensions
        $orig_w = $image->getWidth();
        $orig_h = $image->getHeight();

        // TODO: can handle CMYK crap if it becomes an issue in the future...
        // $image->handle_cmyk();

        // If there's a stored crop definition (or the style specifies a crop)
        $do_crop       = false;
        if ($style->crop) {
            $do_crop                 = true;
            $existing_crop           = array();
            $existing_crop['width']  = $style->width;
            $existing_crop['height'] = $style->height;
        }

        // Do the cropping if needed
        if ($do_crop) {
            if (isset($existing_crop['x']) && isset($existing_crop['y'])) {
                // Exact coordinates exist
                $image->crop($existing_crop['width'], $existing_crop['height'], $existing_crop['x'], $existing_crop['y']);
            } else {
                // No previous crop record found, resize to target dimensions and even allow upscaling
                // Click-bait headline: "The results may surprise you!"
                $image->fit($existing_crop['width'], $existing_crop['height']);
                $image->resize($existing_crop['width'], $existing_crop['height'], null, false);
            }

            // Enforce cropped dimensions in case the result is smaller than what the style dictates
            // NB: Only 'hard-crop' styles enforce this (for now at least), the other possible use-case
            // might come from a stored precise crop definition without the style itself having the
            // crop flag - that's why this if is here...
            if ($style->crop) {
                $actual_w      = $image->getWidth();
                $actual_h      = $image->getHeight();
                $target_width  = ($actual_w < $style->width) ? $style->width : null;
                $target_height = ($actual_h < $style->height) ? $style->height : null;
                if ($target_width || $target_height) {
                    $image->resize_canvas($target_width, $target_height);
                }
            }
        }

        // Now resize the (even potentially cropped) image if needed, hopefully avoiding upscaling in all cases
        $image->resize_down($style->width, $style->height);

        return $image->save($path);
    }

    /**
     * @return Thumb
     */
    public function getAvatarImage()
    {
        $thumb = null;

        if (!empty($this->avatar)) {
            $details = json_decode($this->avatar_details, true);
            $avatar_path = $details['path'];
            if (!file_exists($avatar_path)) {
                $avatar_path = isset($details['path_root']) ? ROOT_PATH . $details['path_root'] : null;
            }
            if ($avatar_path && file_exists($avatar_path)) {
                $info = getimagesize($avatar_path);
                $src = $this->buildAvatarUrl($this->avatar);
                $thumb = new Thumb();
                $thumb->setSrc($src);
                $thumb->setWidth($info[0]);
                $thumb->setHeight($info[1]);
            }
        }

        if (!$thumb) {
            $thumb = self::getDefaultAvatarThumb();
        }

        return $thumb;
    }

    public function buildImageUrl($filename, $base_dir = null, $base_uri = null)
    {
        $basename = basename($filename);

        $partitioned_filepath = Utils::get_fqpn(
            $basename,
            Utils::fqpn_options(array(
                'basedir' => $base_dir,
            ))
        );

        $url_part = str_replace($base_dir, '', $partitioned_filepath);
        // $url_parts starts with a slash due to Utils::fqpn_options()
        $url = $base_uri . $url_part;

        return $url;
    }

    public function buildAvatarUrl($filename)
    {
        return $this->buildImageUrl($filename, self::getAvatarUploadsDir(), self::getAvatarUploadsUri());
    }

    public function buildProfileBgUrl($filename)
    {
        return $this->buildImageUrl($filename, self::getBgsUploadsDir(), self::getBgsUploadsUri());
    }

    /**
     * @return Thumb
     */
    public function getProfileBgImage()
    {
        $thumb = null;

        if (!empty($this->profile_bg)) {
            $details = json_decode($this->profile_bg_details, true);
            $profile_bg_path = $details['path'];
            if (!file_exists($profile_bg_path)) {
                $profile_bg_path = isset($details['path_root']) ? ROOT_PATH . $details['path_root'] : null;
            }
            if ($profile_bg_path && file_exists($profile_bg_path)) {
                $info = getimagesize($profile_bg_path);
                $src = $this->buildProfileBgUrl($this->profile_bg);
                $thumb = new Thumb();
                $thumb->setSrc($src);
                $thumb->setWidth($info[0]);
                $thumb->setHeight($info[1]);
            }
        }

        if (!$thumb) {
            $thumb = Media::getNoImageThumb($this->getProfileBgStyle());
        }

        return $thumb;
    }

    public function getProfileBgStyle()
    {
        static $style = null;

        if (null === $style) {
            $style = ImageStyles::fromArray(array(
                'slug'      => 'coverbg',
                'width'     => 1500,
                'height'    => 400,
                'crop'      => true,
                'watermark' => false
            ));
        }

        return $style;
    }

    public function getAvatarStyle()
    {
        static $style = null;

        if (null === $style) {
            $style = ImageStyles::fromArray(array(
                'slug'      => 'avatar',
                'width'     => 200,
                'height'    => 200,
                'crop'      => true,
                'watermark' => false
            ));
        }

        return $style;
    }

    public static function findByUserId($user_id)
    {
        return self::findFirst(array('user_id = :user_id:', 'bind' => array('user_id' => $user_id)));
    }

    public static function getDefaultAvatarThumb()
    {
        $instance = new self();
        $thumb = Media::getNoImageThumb($instance->getAvatarStyle(), null, 'assets/img/avatar-default-optim.svg');

        return $thumb;
    }
}
