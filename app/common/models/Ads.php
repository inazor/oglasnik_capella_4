<?php

namespace Baseapp\Models;

use Baseapp\Library\Debug;
use Baseapp\Library\Products\BackendProductsChooser;
use Baseapp\Library\Products\EnabledProductsSelector;
use Baseapp\Library\Products\GroupedProductsChooser;
use Baseapp\Library\Products\Offline;
use Baseapp\Library\Products\Online;
use Baseapp\Library\Products\OrderableEnabledProductsSelectorManual;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Products\EnabledProductsSelectorManual;
use Baseapp\Library\Products\ProductsSelector;

use Baseapp\Library\Utils;
use Baseapp\Library\Moderation;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Library\Avus\ExportToAvus;
use Baseapp\Models\Categories;
use Baseapp\Models\AdsParameters;
use Baseapp\Models\AdsAdditionalData;
use Baseapp\Models\AdsOfflineExportHistory;
use Baseapp\Models\Currency;
use Baseapp\Models\Dictionaries as Dictionary;
use Baseapp\Models\Locations as Location;
use Baseapp\Models\Users;
use Baseapp\Models\UsersShops;
use Baseapp\Models\UsersFavoriteAds;
use Baseapp\Models\UsersMessages;
use Baseapp\Models\Media;
use Baseapp\Library\Email;
use Baseapp\Library\Validations\AdsBackend as AdsValidationsBackend;
use Baseapp\Library\Validations\AdsFrontend as AdsValidationsFrontend;
use Baseapp\Library\DateUtils;
use Baseapp\Bootstrap;
use Baseapp\Traits\MediaCommonMethods;
use Baseapp\Traits\InfractionReportsHelpers;
use Phalcon\Di;
use Phalcon\Escaper;
use Phalcon\Http\RequestInterface;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\Model\Query\Builder;
use \Phalcon\Mvc\Model\Relation as PhRelation;
use Phalcon\Mvc\Model\Resultset;


/**
 * Ads Model
 */
class Ads extends BaseModelBlamable
{
    use MediaCommonMethods;
    use InfractionReportsHelpers;

    const PAYMENT_STATE_NEW            = 1; // Every new ad gets this status as we still don't know about the payment
    const PAYMENT_STATE_FREE           = 2; // Free ad (even those that have an Order but with cost == 0)
    const PAYMENT_STATE_ORDERED        = 3; // Every ad waiting to be paid
    const PAYMENT_STATE_PAID           = 4; // Paid ad
    const PAYMENT_STATE_CANCELED_ORDER = 5; // Ad who's order was canceled

    private $_prepared_data = array();
    private $_prepared_data_json = array();

    private $_media_data = array();

    public $sort_date = 0;
    public $product_sort = 0;
    public $is_spam = 0;
    public $active = 0;
    public $sold = 0;
    public $soft_delete = 0;

    public $online_product_id = null;
    public $online_product = null;
    public $offline_product_id = null;
    public $offline_product = null;

    private $_unsavedMedia = null;

    protected $reportReasons = array(
        'Prijevaran oglas',
        'Oglas vi�e nije aktualan',
        'Kontakt telefon nije ispravan',
        'Nema povratne informacije od osobe koja je predala oglas',
        'Dupli oglas',
        'Pogre�na rubrika'
    );

    /**
     * Ads initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('category_id', __NAMESPACE__ . '\Categories', 'id', array(
            'alias'      => 'Category',
            'reusable'   => true,
            'foreignKey' => array(
                'action' => PhRelation::ACTION_CASCADE
            )
        ));
        $this->belongsTo('currency_id', __NAMESPACE__ . '\Currency', 'id', array(
            'alias'      => 'Currency',
            'reusable'   => true,
            'foreignKey' => array(
                'action' => PhRelation::ACTION_CASCADE
            )
        ));
        $this->hasMany('id', __NAMESPACE__ . '\AdsParameters', 'ad_id', array(
            'alias'      => 'AdParameters',
            'reusable'   => true,
            'foreignKey' => array(
                'action' => PhRelation::ACTION_CASCADE
            )
        ));
        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', array(
            'alias'      => 'User',
            'reusable'   => true,
            'foreignKey' => array(
                'action' => PhRelation::ACTION_CASCADE
            )
        ));
        $this->hasManyToMany(
            'id', __NAMESPACE__ . '\AdsParameters', 'ad_id',
            'parameter_id', __NAMESPACE__ . '\Parameters', 'id'
        );
        $this->hasMany(
            'id', __NAMESPACE__ . '\AdsMedia', 'ad_id',
            array(
                'alias'    => 'Media',
                'reusable' => true
            )
        );
        $this->hasOne(
            'id', __NAMESPACE__ . '\AdsSearchTerms', 'ad_id',
            array(
                'alias'    => 'SearchTerm',
                'reusable' => true
            )
        );
        $this->hasOne(
            'id', __NAMESPACE__ . '\AdsAdditionalData', 'ad_id',
            array(
                'alias'      => 'AdditionalData',
                'reusable'   => true
            )
        );
        $this->hasManyToMany(
            'id', __NAMESPACE__ . '\UsersFavoriteAds', 'ad_id',
            'user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'    => 'FavoriteByUsers',
                'reusable' => true
            )
        );
    }

	
	//IN:
	public function beforeSave(){
		// $this->xRec = isset($this->id) && $this->id > 0 ? $this::findFirst($this->id)->toArray() : null;
	}
	public function afterSave(){
		if ( \Baseapp\Library\libSuva::getContext('n_source_path') != 'suva' ){
			return \Baseapp\Library\libSuva::Ads_afterSave ($this);

		}
	}


	
    public function beforeCreate()
    {
        $this->created_at = Utils::getRequestTime();
    }

    // public function afterCreate()
    // {
        // error_log("common/models/ads/ After create ".print_r($this->toArray(), true));
    // }
    // public function afterUpdate()
    // {
        // error_log("common/models/ads/ After Update ".print_r($this->toArray(), true));
    // }

	
	
    public function beforeUpdate()
    {
        $this->modified_at = Utils::getRequestTime();
    }

    public function getPhoneField($field = 'phone1', $country_iso_code = 'HR')
    {
        if (isset($this->$field)) {
            if ($this->$field) {
                $parsed = Utils::parsePhoneNumber($this->$field, $country_iso_code);
                if ($parsed['valid']) {
                    $this->$field = $parsed['international'];
                } else {
                    $this->$field = null;
                }
            }

            return $this->$field;
        }

        return null;
    }

    public function getPrice()
    {
        return self::buildPrice($this->price);
    }

    public function assignPrice($price = '')
    {
        $this->price = Utils::kn2lp($price);
    }

    public static function buildPrice($price)
    {
        return Utils::lp2kn($price, 0);
    }

    // Overriding toArray() in order to set 'price' correctly from minor currency units
    public function toArray($columns = null)
    {
        $data = parent::toArray($columns);

        if (isset($data['price'])) {
            $data['price'] = $this->getPrice();
        }

        return $data;
    }

    /**
     * @return null|\Baseapp\Models\Categories
     */
    public function getCategory()
    {
        $category = null;

        if ($this->category_id) {
            $category = Categories::findFirst($this->category_id);
        }

        return $category;
    }

    /**
     * @return null|Users
     */
    public function getUser()
    {
        $user = null;

        if (isset($this->user_id) && $this->user_id) {
            $user = Users::findFirst($this->user_id);
        }

        return $user;
    }

    /**
     * Helper method to check if current ad can continue the process of submission
     *
     * @param int|null $user_id User id to check (only owners should be able to continue the ad submission)
     *
     * @return bool
     */
    public function canContinueSubmission($user_id = null)
    {
        $allow = false;

        // User id is the actual owner
        if ($user_id && ($this->user_id == $user_id)) {
            $allow = true;
        }

        // Soft-deleted ads cannot continue no matter what
        if ($allow && $this->isSoftDeleted()) {
            $allow = false;
        }

        return $allow;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->expires_at < Utils::getRequestTime();
    }

    public function getDaysUntilExpiry()
    {
        $days = null;

        if (!$this->isExpired()) {
            $expires_at = new \DateTime('@' . $this->expires_at);
            $now        = new \DateTime('now');
            $diff       = $expires_at->diff($now);
            $days       = $diff->d;
        }

        return $days;
    }

    public function markExpired()
    {
        $sql = "UPDATE `" . $this->getSource() . "` SET `active` = 0, `manual_deactivation` = 0, `latest_payment_state` = :latest_payment_state WHERE (`active` = 1 OR (`active` = 0 AND `manual_deactivation` = 0)) AND `is_spam` = 0 AND `expires_at` < :current_timestamp";

        $conn = $this->getWriteConnection();
        $conn->execute(
            $sql,
            array(
                ':latest_payment_state' => self::PAYMENT_STATE_NEW,
                ':current_timestamp'    => Utils::getRequestTime()
            )
        );

        return $conn->affectedRows();
    }

    public function canBeRepublished()
    {
        $can_be_republished = true;

        // currently active ads cannot be republished
        if ((int)$this->active === 1) {
            $can_be_republished = false;
        }

        // currently unexpired ads cannot be republished (also, they should not be flagged as 'sold')
        if ($this->sold === 0 && !$this->isExpired()) {
            $can_be_republished = false;
        }

        // ads that have moderation status NOK cannot be republished
        if (Moderation::MODERATION_STATUS_NOK === $this->moderation) {
            $can_be_republished = false;
        }

        return $can_be_republished;
    }

    public function isSoftDeleted($soft_deleted = null)
    {
        if (null !== $soft_deleted) {
            $this->disable_notnull_validations();
            $this->soft_delete = (bool) $soft_deleted;
            if ($this->soft_delete) {
                $this->active = 0;
            }
            $this->update();
            $this->enable_notnull_validations();
        }

        return (bool) $this->soft_delete;
    }

    /**
     * Ad soft delete method (frontend user will not see this ad, while backend users will)
     */
    public function soft_delete()
    {
        $result = $this->isSoftDeleted(true);

        // WARNING: This will become a serious problem if/when wallet/basket payments arrive, if
        // the Orders and OrdersItems architecture stays the same, because it means an order can
        // end up being cancelled, but it can have line items that are not related to this ad at all...

        // When an ad is soft-deleted, mark any new/pending Orders it might have had as cancelled
        Orders::cancelAllPendingOrdersForAd($this->id);

        // Delete it from the AdsHomepage list
        AdsHomepage::deleteByAdId($this->id);

        return $result;
    }

    public function setUnsavedMedia($media = null)
    {
        if (empty($media)) {
            $media = null;
        }
        $this->_unsavedMedia = $media;
    }

    public function getUnsavedMedia()
    {
        return $this->_unsavedMedia;
    }

    /**
     * @return array
     */
    public static function getAllPaymentStates()
    {
        return array(
            (string)self::PAYMENT_STATE_NEW            => 'New',
            (string)self::PAYMENT_STATE_FREE           => 'Free',
            (string)self::PAYMENT_STATE_ORDERED        => 'Ordered',
            (string)self::PAYMENT_STATE_PAID           => 'Paid',
            (string)self::PAYMENT_STATE_CANCELED_ORDER => 'Order canceled'
        );
    }

    public static function getPaymentStateString($payment_state)
    {
        $payment_states = self::getAllPaymentStates();
        if (isset($payment_states[(string)$payment_state])) {
            return $payment_states[(string)$payment_state];
        }

        return 'Unknown';
    }

    // export ad to avus
    protected function export_to_avus()
    {
        if (isset($this->offline_product) && !empty($this->offline_product)) {
            if (Moderation::MODERATION_STATUS_OK === $this->moderation) {
                $export_to_avus = new ExportToAvus($this);
                $appearance_details = $export_to_avus->process();
                if ($appearance_details['exported_id'] && $appearance_details['appearance_date']) {
                    // TODO: do some update somewhere?
                }
            }
        }
    }

    // Returns info whether ad is scheduled for offline export
    public function is_offline_exportable()
    {
        $scheduled = false;

        if (isset($this->offline_product) && !empty($this->offline_product)) {
            $offline_export_record = AdsOfflineExportHistory::findFirst(array(
                'conditions' => 'ad_id = :ad_id: AND appearance_date = :appearance_date:',
                'bind'       => array(
                    'ad_id'           => $this->id,
                    'appearance_date' => ExportToAvus::getNextAppearanceDate()  // next issue
                )
            ));

            if ($offline_export_record) {
                $scheduled = true;
            }
        }

        return $scheduled;
    }

    // test if this ad can be exported to avus
    // we need to see if this ad has at least one phone number (or even user
    // has at least one public phone number)
    public function can_be_exported_to_avus()
    {
        $can_be_exported_to_avus = true;

        if ($this->offline_product_id) {
            $can_be_exported_to_avus = false;

            $ad_user = $this->getUserDetails();
            if ($ad_user && !empty($ad_user->phone1)) {
                // phone numbers should follow this pattern AREACODE/PHONENUMBER
                if (Utils::convert_phone_number_to_avus_format($ad_user->phone1)) {
                    $can_be_exported_to_avus = true;
                }
                if (!$can_be_exported_to_avus && Utils::convert_phone_number_to_avus_format($ad_user->phone2)) {
                    $can_be_exported_to_avus = true;
                }
            } elseif ($ad_user = $this->getUser()) {
                $ad_user = $ad_user->toArray();

                if ($ad_user['phone1_public'] && $ad_user['phone1']) {
                    $can_be_exported_to_avus = Utils::convert_phone_number_to_avus_format($ad_user['phone1']) ? true : false;
                }
                if (!$can_be_exported_to_avus && $ad_user['phone2_public'] && $ad_user['phone2']) {
                    $can_be_exported_to_avus = Utils::convert_phone_number_to_avus_format($ad_user['phone2']) ? true : false;
                }
            }
        }

        return $can_be_exported_to_avus;
    }


    /**
     * Helper method to publish ads based on the settings they currently have
     *
     * TODO: Shouldn't this all be handled in beforeUpdate() and beforeCreate() events?
     * We should be able to check the current controller from the di/dispatcher probably within
     * those events/methods to determine frontend/backend/cli usage and rules...
     *
     * @param string $module 'backend'|'frontend'
     *
     * @return bool
     */
    public function verify_and_publish_ad($module)
    {
        // We assume that ads added from backend module can be published
        // immediately (we have to check other things, like is it paid or not),
        // but we'll deal with them later.. All ads added from frontend are
        // assumed not to be published automatically...
        $can_be_published = ('backend' === $module ? true : false);

        // set default ad expire time period
        $default_ad_expire_time = 30;

        // get current timestamp
        $current_timestamp = Utils::getRequestTime();

        $cat_settings = null;
        if (isset($this->id) && $this->id) {
            $cat_settings = $this->getCategory()->Settings;
        } elseif (isset($this->category_id) && $this->category_id) {
            $cat_settings = CategoriesSettings::findFirstByCategoryId($this->category_id);
        }
        if ($cat_settings) {
            $cat_ad_expire_time = !empty($cat_settings->default_ad_expire_time) ? $cat_settings->default_ad_expire_time : null;
            // set ad expiry based on category settings (if they exist)
            if ($cat_ad_expire_time) {
                $default_ad_expire_time = $cat_ad_expire_time;
            }

            // in case the ad is in category that requires post-moderation, and
            // the request came from frontend module, the ad could be published
            // immediately
            $cat_moderation_type = !empty($cat_settings->moderation_type) ? $cat_settings->moderation_type : 'post';
            if ('post' === $cat_moderation_type && 'frontend' === $module) {
                $can_be_published = true;
            }
        }

        // check moderation status... for now, we won't allow publishing of ads that have
        // moderation = 'nok', as it is assumed that it was reviewed and rejected
        if (Moderation::MODERATION_STATUS_NOK === $this->moderation) {
            $can_be_published = false;
        } elseif (/*'moderation' == $module && */Moderation::MODERATION_STATUS_OK === $this->moderation) {
            $can_be_published = true;
        }

        // if user manually deactivated the ad, then no matter what, it cannot
        // be published!
        if ($this->manual_deactivation) {
            $can_be_published = false;
        }

        // in case this method is called on an ad that has an expire date set,
        // and its' expire date is in past, then this ad cannot be published...
        $ads_expire_date = (int) !empty($this->expires_at) ? $this->expires_at : 0;
        if ($ads_expire_date && $ads_expire_date < $current_timestamp) {
            $can_be_published = false;
        }

        // soft deleted ads cannot be published
        if ($this->isSoftDeleted()) {
            $can_be_published = false;
        }

        if ($can_be_published) {
            // as we are about to mark this as ad published, we need to check if
            // this is the first time the ad is published or not (so we can
            // write some timestamps to the db). There is another case when we
            // should write (update) some timestamps in the db. It's the case
            // when the ad is being re-published (after it has expired and
            // moderated with 'ok').

            $is_first_publish = false;
            if (!isset($this->first_published_at) || $this->first_published_at <= 0) {
                $is_first_publish = true;
            }

            if ($is_first_publish) {
                $this->first_published_at = $current_timestamp;
                $this->published_at       = $current_timestamp;
                $this->sort_date          = $current_timestamp;

                if (!isset($this->expires_at) || empty($this->expires_at)) {
                    $ads_online_product = $this->getProduct('online');
                    if ($ads_online_product) {
                        $default_ad_expire_time = $ads_online_product->getDuration();
                    }

                    $this->expires_at         = $current_timestamp + (24 * 3600 * $default_ad_expire_time);
                }
            }

            // mark ad as published (active = 1)
            $this->active = 1;
			
        } else {
            // mark ad as unpublished (active = 0)
            $this->active = 0;
        }
    }

    /**
     * Creates a new ad
     *
     * @param string $module
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    protected function add_new($module, $request)
    {
        $category_id = (int) $request->getPost('category_id');

        // load the category we'll be working on
        $category = Categories::findFirst($category_id);

        if (null !== $category && null !== $category->Fieldsets) {
            if ('frontend' === $module) {
                $validation = new AdsValidationsFrontend();
            } elseif ('backend' === $module) {
                $validation = new AdsValidationsBackend();
            }
            $validation = $validation->get();

            $parametrizator = new Parametrizator();
            $parametrizator->setModule($module);
            $parametrizator->setMode('create');
            $parametrizator->setValidation($validation);
            $parametrizator->setRequest($request);
            $parametrizator->setCategory($category);
            $parametrizator->setAd($this);

            $messages = $parametrizator->prepare_and_validate();

            if (count($messages)) {
                return $messages;
            } else {
                $parametrizator->parametrize();

                if ('backend' === $module) {
                    $this->verify_and_publish_ad($module);
                    $this->handleProductsChooserSaving($request, $module);
                } else {
                    // mark every new ad with 'latest_payment_state = new' as we
                    // don't know will it be free of paid...
                    $this->latest_payment_state = self::PAYMENT_STATE_NEW;
                }

                if (true === $this->create()) {
                    $this->activate_products();
                    return $this;
                } else {
                    Bootstrap::log($this->getMessages());
                    return $this->getMessages();
                }
            }
        }
    }

    /**
     * Helper method to activate any of the products that ad can have
     */
    public function activate_products()
    {
        // if needed, export the ad to avus (checking is done in export_to_avus method)!
        if (!$this->hasOutstandingOrdersPreventingOfflinePublication() && Moderation::MODERATION_STATUS_OK === $this->moderation) {
            $this->export_to_avus();
        }

    }

    /**
     * Save an existing ad
     *
     * @param string $module
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    protected function save_changes($module, $request)
    {
        $category_id = (int) $request->getPost('category_id');

        // load the category we'll be working on
        $category = Categories::findFirst($category_id);
		//old: (null !== $category) && (null !== ($category->Fieldsets))
        if (isset($category) && isset($category->Fieldsets)) {
            $this->Category = $category;

            if ('frontend' === $module) {
                $validation = new AdsValidationsFrontend();
            } elseif ('backend' === $module) {
                $validation = new AdsValidationsBackend();
            }
            $validation = $validation->get();

            if ('backend' === $module && $request->getPost('moderation') == 'nok') {
                $validation->add('moderation_reason_nok', new \Phalcon\Validation\Validator\PresenceOf(array(
                    'message' => 'Moderation message should be entered for NOK status'
                )));
            }

            $parametrizator = new Parametrizator();
            $parametrizator->setModule($module);
            $parametrizator->setMode('edit');
            $parametrizator->setValidation($validation);
            $parametrizator->setRequest($request);
            $parametrizator->setCategory($category);
            $parametrizator->setAd($this);

            $messages = $parametrizator->prepare_and_validate();

            if (count($messages)) {
                return $messages;
            } else {
                $parametrizator->parametrize();
                $moderation_status_changed = false;

                // every ad saved from frontend should go through moderation
                // process if it's content (parameter values) has been changed

                if ('frontend' === $module && $this->hasAdContentChanged()) {
                    $this->moderation = Moderation::MODERATION_STATUS_WAITING;
                } elseif ('backend' === $module) {
                    $moderation = new Moderation($this);
                    $moderation_status_changed = $moderation->setModerationStatusAndReason($request);
                }

                if ('backend' === $module) {
                    if (Moderation::MODERATION_STATUS_WAITING !== $this->moderation) {
                        $this->verify_and_publish_ad($module);
                    }

                    // only certain roles have ability to attach/update ad's product
                    if ($this->getDI()->getShared('auth')->logged_in(array('admin', 'supersupport'))) {
                        $this->handleProductsChooserSaving($request, $module);
                    }
                }

                if (true === $this->update()) {
                    if ('backend' === $module) {
                        $this->activate_products();
                    }
                    return $this;
                } else {
                    Bootstrap::log($this->getMessages());
                    return $this->getMessages();
                }
            }
        }
    }

    /**
     * Creates a new ad from the frontend module
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function frontend_add_new($request)
    {
        return $this->add_new('frontend', $request);
    }

    /**
     * Saves data for an existing ad from frontend
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function frontend_save_changes($request)
    {
        return $this->save_changes('frontend', $request);
    }

    /**
     * Creates a new ad from the backend module
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_add_new($request)
    {
        return $this->add_new('backend', $request);
    }

    /**
     * Saves data for an existing ad from backend
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_save_changes($request)
    {
        return $this->save_changes('backend', $request);
    }

    /**
     * Check if current ad's content (parameter values) has been changed before actual saving
     *
     * @return boolean
     */
    public function hasAdContentChanged()
    {
        if (!isset($this->id) || empty($this->id)) {
            return true;
        }

        $original_ad = self::findFirst($this->id);
        if ($original_ad->json_data !== $this->json_data) {
            return true;
        }

        return false;
    }

    /**
     * Save existing ad's moderation status (and reason)
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_moderation($request, $resolveUnresolvedInfractionReports = false)
    {
        $return = null;
        $try_sending_reason = false;

        if (Moderation::MODERATION_STATUS_NOK === $request->getPost('moderation')) {
            $validation = new AdsValidationsBackend();
            $validation = $validation->get();
            if ($request->getPost('moderation') == 'nok') {
                $validation->add('moderation_reason_nok', new \Phalcon\Validation\Validator\PresenceOf(array(
                    'message' => 'Moderation message should be entered for NOK status'
                )));
            }
            $messages = $validation->validate($request->getPost());

            if (count($messages)) {
                return $validation->getMessages();
            }
        }
        $moderation = new Moderation($this);

        if (!$moderation->setModerationStatusAndReason($request)) {
            $return = $this;
            $try_sending_reason = true;
        } else {
            $this->verify_and_publish_ad('moderation');

            if (true === $this->save()) {
                $this->activate_products();
                $try_sending_reason = true;
                $return = $this;

                // should we clear unresolved infraction reports for this ad?
                if ($resolveUnresolvedInfractionReports) {
                    $unresolvedInfractionReports = $this->getUnresolvedInfractionReports();
                    $unresolvedInfractionReportsIDs = $this->getIDsFromInfractionReports($unresolvedInfractionReports);

                    InfractionReports::resolveID($unresolvedInfractionReportsIDs);
                }
            } else {
                $return = $this->getMessages();
                Bootstrap::log($return);
            }
        }

        // if admin checked 'Send reason email' checkbox, then try to send the email
        if ($try_sending_reason && $request->hasPost('send_reason_email')) {
            $moderation->sendModerationMessage($this->moderation, $this->moderation_reason);
        }

        return $return;
    }

//  Common ad methods ----------------------------------------------------------

    /**
     * @return bool
     */
    public function is_active() {
        return $this->active && !$this->is_spam;
    }

    /**
     * Is ad spam?
     * @return boolean
     */
    public function is_spam() {
        return $this->is_spam;
    }

    /**
     * Get publicly visible phone numbers for current ad. If there are no phone numbers linked with this ad, 
     * we'll try to get all publicly visible phone numbers from user's profile
     * 
     * @return array
     */
    public function getPublicPhoneNumbers()
    {
        $phoneNumbers = array();

        if (!empty($this->phone1)) {
            $phoneNumbers[] = $this->phone1;
        }
        if (!empty($this->phone2)) {
            $phoneNumbers[] = $this->phone2;
        }

        if (empty($phoneNumbers) && $user = $this->getUser()) {
            if (!empty($user->phone1) && $user->phone1_public) {
                $phoneNumbers[] = trim($user->phone1);
            }
            if (!empty($user->phone2) && $user->phone2_public) {
                $phoneNumbers[] = trim($user->phone2);
            }
        }
        $user = null;
        unset($user);

        return $phoneNumbers;
    }

    public function getFormattedPublicPhoneNumbers()
    {
        $formattedPhoneNumbers = array();
        $phoneNumbers = $this->getPublicPhoneNumbers();
        foreach ($phoneNumbers as $phoneNumber) {
            $parsedPhoneNumber = Utils::parsePhoneNumber($phoneNumber);
            if ($parsedPhoneNumber) {
                if ($parsedPhoneNumber['valid']) {
                    $formattedPhoneNumbers[] = array(
                        'link' => $parsedPhoneNumber['international'],
                        'text' => $parsedPhoneNumber['formattedNationalNumber']
                    );
                } else {
                    $formattedPhoneNumbers[] = array(
                        'text' => $parsedPhoneNumber['formattedNationalNumber']
                    );
                }
            }
        }

        return $formattedPhoneNumbers;
    }

    /**
     * Get ad's user details
     */
    public function getUserDetails()
    {
        $ad_user = $this->getUser();

        $user = null;

        $builder = self::query();
        $builder->columns(array(
            'user.type',
            'user.email',
            'user.username',
            'user.phone1',
            'user.phone1_public',
            'user.phone2',
            'user.phone2_public',
            'user.company_name',
            'shop.id as shop_id',
            'loc_country.name as country_name',
            'loc_county.name as county_name'
        ))
        ->innerJoin('Baseapp\Models\Users', 'user.id = Baseapp\Models\Ads.user_id', 'user')
        ->leftJoin('Baseapp\Models\UsersShops', 'user.id = shop.user_id', 'shop')
        ->leftJoin('Baseapp\Models\Locations', 'user.country_id = loc_country.id', 'loc_country')
        ->leftJoin('Baseapp\Models\Locations', 'user.county_id = loc_county.id', 'loc_county')
        ->where(
            'Baseapp\Models\Ads.id = :ad_id:',
            array(
                'ad_id' => $this->id
            )
        )
        ->limit(1);
        $results = $builder->execute();
        if ($results && count($results) >= 1) {
            $user = $ad_user;
            $user->ads_page_url = $ad_user->get_ads_page();
            $user->shop = null;
            if ($results[0]->shop_id) {
                $shop = UsersShops::findFirst($results[0]->shop_id);
                if ($shop->isActive()) {
                    $user->shop = $shop;
                }
            }

            $user->phoneNumbers = $this->getPublicPhoneNumbers();
        }

        return $user;
    }

    /**
     * Returns products belonging to the current Ad.
     *
     * If there are some, it's an array with a product in each named group:
     * ['online' => null|string|ProductsInterface $product, 'offline' => null|string|ProductsInterface $product)]
     *
     * Returns null if Ad has no products at all.
     *
     * @return array|null|ProductsInterface[]
     */
    public function getProducts()
    {
        $ret = null;

        $online = $this->getProduct('online');
        if (!empty($online)) {
            $ret           = array();
            $ret['online'] = $online;
        }

        $offline = $this->getProduct('offline');
        if (!empty($offline)) {
            if (!is_array($ret)) {
                $ret = array();
            }
            $ret['offline'] = $offline;
        }

        return $ret;
    }

    /**
     * Returns the currently assigned product for the specified product group (if there is one).
     *
     * @param string $group Which product group to get. Defaults to 'online'.
     * @param bool $unserialized Whether the data/product should be unserialized or not. Defaults to true.
     *
     * @return null|string|ProductsInterface
     */
    public function getProduct($group = 'online', $unserialized = true)
    {
        switch ($group) {
            case 'offline':
                $product = $this->offline_product;
                break;
            case 'online':
            default:
                $product = $this->online_product;
                break;
        }

        if ($unserialized && !empty($product)) {
            $product = unserialize($product);
        }

        return $product;
    }

    /**
     * Sets product-specific properties (which is actually different ad types and should be named accordingly
     * at some point in the future). Expiry time and sort order for now.
     *
     * @param ProductsInterface $product
     */
    public function modifyWithProduct(ProductsInterface $product)
    {
        // Bail if given an offline product for now since the things done below should not be
        // touched in any way when given an offline product.
        if ($product->isOfflineType()) {
            return;
        }

        // If the product has a duration, set the ad's expires_at date to now + $duration days
        $product_duration_days = $product->getDuration();
        if (null !== $product_duration_days && $product_duration_days > 0) {
            $now = Utils::getRequestTime();
            $day_seconds = 24 * 3600;
            $this->expires_at = ($now + ($day_seconds * $product_duration_days));
        }

        // Set the sort order according to the product's sort index
        $this->product_sort = $product->getSortIdx();
    }

    public static function isPushable(array $ad, Users $user = null)
    {
        $data = array(
            'active'         => $ad['active'],
            'moderation'     => $ad['moderation'],
            'online_product' => $ad['online_product'],
            'is_owner'       => false
        );

        if (null !== $user) {
            $data['is_owner'] = ($ad['user_id'] == $user->id);
        }

        return self::isPushUpDoable($data);
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public static function isPushUpDoable(array $data)
    {
        $doable = true;

        // Generally, don't even bother doing any successive checks if a certain
        // check already failed... That's the reason for all those repeated if guards below.

        // If some extra keys are present in $data, make sure they evaluate to true
        if ($doable) {
            // Owner check is done early so we don't even bother with any further checks for anon listings etc.
            if (isset($data['is_owner'])) {
                $doable = $data['is_owner'];
            }
        }

        // Ad must be active
        if ($doable) {
            if (!$data['active']) {
                $doable = false;
            }
        }

        // Moderation status must be ok
        if ($doable) {
            if (Moderation::MODERATION_STATUS_OK !== $data['moderation']) {
                $doable = false;
            }
        }

        // Making sure online product is there, it "works", and has pushup ability
        if ($doable) {
            $product = false;
            if (!empty($data['online_product'])) {
                $product = @unserialize($data['online_product']);
                if ($product) {
                    if (!$product->hasPushUp()) {
                        $product = false;
                    }
                }
            }
            // If we didn't satisfy product rules above, it's not doable
            if (!$product) {
                $doable = false;
            }
        }

        return $doable;
    }

    /**
     * Apply pushUp logic to an Ad and update latest_payment_state accordingly (if/when $paid = true)
     *
     * @param bool $paid Defaults to false. When true, sets latest_payment_state to self::PAYMENT_STATE_PAID
     *
     * @return bool
     */
    public function pushUp($paid = false)
    {
        $new_data = array(
            'sort_date' => Utils::getRequestTime()
        );

        // When a paid pushUp is applied, make sure to set the latest_payment_state accordingly
        if ($paid) {
            $new_data['latest_payment_state'] = self::PAYMENT_STATE_PAID;
        }

		//IN: Dodavanje informacije o pushup proizvodu
		
		
		
        return $this->save($new_data);
    }

    public function modifyWithProductUponPayment(ProductsInterface $product)
    {
        /**
         * PLEASE, DO NOT CHANGE STUFF HERE WITHOUT FAMILIARIZING YOURSELF WITH WHAT'S GOING ON AND WHY!
         *
         * If the $product comes from the upgrade purchase/process, it can have
         * a currently selected option value of 'Do isteka'. What happens if we don't change that?
         *
         * The originally ordered option is overwritten when this product is now serialized and saved to the ad.
         * But, further below we're calling `modifyWithProduct()` and that method currently works just fine with
         * such products ('Do isteka'), and deals with them normally -- this is because the duration of 'Do isteka'
         * is treated as 0 and so it doesn't over-extend the ad's expiry date.
         *
         * So, we need the serialized product to not contain the special 'Do isteka' option (and not have
         * it set as selected either).
         * But, we also need to work with the originally passed in $product instance too, so that we can properly
         * apply/modify the ad with it (due to other potential product rules etc.).
         */
        $selected_option = $product->getSelectedOption();
        if ('Do isteka' === $selected_option) {
            $product_to_store = clone $product;
            $product_to_store->restoreBackedUpSelectedOption();
        } else {
            $product_to_store = $product;
        }

        $product_serialized = serialize($product_to_store);
        $product_id         = $product->getId();

        $data = array();
        if ($product->isOnlineType()) {
            // Skip modifying the saved product for Pushup special case (shouldn't happen at all any more, since
            // the changes to Orders::finalizePurchase() were done to avoid calling this method for Pushups completely)
            if (!($product instanceof Online\Pushup)) {
                $data = array(
                    'online_product_id' => $product_id,
                    'online_product'    => $product_serialized
                );
            }
        } elseif ($product->isOfflineType()) {
            $data = array(
                'offline_product_id' => $product_id,
                'offline_product'    => $product_serialized
            );

            // Get current (or create a new export history record)
            $ads_offline_export_history = AdsOfflineExportHistory::getByAppearanceDateOrCreateNew(
                $this->id,
                ExportToAvus::getNextAppearanceDate()
            );
            if ($ads_offline_export_history->processed == 0) {
                $ads_offline_export_history->product = $product_serialized;
                $ads_offline_export_history->save();
            }
        }

        /**
         * zyt: 06.10.2016.
         * Always refreshing sort_date is problematic in cases when user is doing/paying an Upgrade,
         * because due to pricing differences, he can get a cheaper PushUp by just ordering/extending an IstaknutiOnline
         * in categories where PushUp costs more than Istaknuti etc... Or so it would apppear based on
         * discussions here: https://trello.com/c/yJTejG8v/302
         * Due to the above, we've decided to prevent extending "current" product in UpgradeController, you can
         * just order a completely new/more-expensive-one...
         */
        // Always refresh the sort_date when a payment is being handled
        // https://trello.com/c/h1noiaMy/287
        $this->sort_date = Utils::getRequestTime();

        /**
         * if we're working with expired ads, we have to set their dates to currentTimestamp...
         *
         * TODO: Can we get a false positive here
         */
        if ($this->isExpired()) {
            $this->published_at = Utils::getRequestTime();
        }

        /**
         * Applying basic $product rules exactly as stored in the OrderItem that got us here.
         * That $product can be slightly different than the product that we're actually serializing and saving into the ad.
         * This is due to the ads upgrade thingy being an afterthought and not being planned properly, so we're kind of
         * stuck with this for now. Please, do read the huge comment at the beginning of this method if
         * you're about to do something here.
         */
        $this->modifyWithProduct($product);

        $this->verify_and_publish_ad('frontend');
        // clear info that ad is sold!
        $this->sold = 0;

        // mark the ad as paid
        $this->latest_payment_state = self::PAYMENT_STATE_PAID;

        // Save the product-related-fields changes (some stuff set in modifyWithProduct()!)
        $result = $this->save($data);
        if (!$result) {
            // Lets at least log something if saving here fails since its rather important...
            $msg = 'Failed saving the Ads entity in Ads::modifyWithProductUponPurchase() for product: ';
            $msg .= $product->getId() . ' (' . $product->getType() . ')';
            Bootstrap::log($msg);
        } else {
            // Export to AVUS if needed after a successful save
            if ($product->isOfflineType()) {
                $this->export_to_avus();
            }
        }

        // Apply some extra logic when needed
        $this->processProductExtras($product);
    }

    /**
     * @param ProductsInterface $product
     */
    public function processProductExtras(ProductsInterface $product)
    {
        // Apply some extra logic depending on $product type
        switch (true) {
            /**
             * zyt: 22.09.2016.
             * This pushUp case shouldn't be happening anyway due to how/where
             * things are handled when processing an Order and/or which products
             * are offered in the backend. So I'm commenting it out for now.
             */
            /*
            case $product instanceof Online\Pushup:
                $this->pushUp();
                break;
            */
            case $product instanceof Online\IstaknutiOnline:
            case $product instanceof Online\Premium:
                $this->applySelectedProductExtrasRules($product);
                break;
        }
    }

    public function applySelectedProductExtrasRules(ProductsInterface $product)
    {
        $selected_extras = $product->getSelectedExtras();
        if ($selected_extras) {
            foreach ($selected_extras as $feature => $duration) {
                // TODO: apply specific extra logic depending on $feature and $duration
                switch ($feature) {
                    case 'Objava na naslovnici':
                        // Populating the list of ads that need to be shown on the homepage
                        $saved = AdsHomepage::insertOrUpdate($this, $product, $duration);
                        break;
                }
            }
        }
    }

    /**
     * @param RequestInterface $request
     * @param string $module
     * @param Categories|null $category
     *
     * @return BackendProductsChooser
     */
    public function getProductsChooser($request, $module = 'backend', $category = null)
    {
        $post_data = $request->getPost();

        if (null === $category) {
            $category = $this->getCategory();
        }

        // Handles offline-products checkbox when it's needed
        $offline_product = $this->getProduct('offline');
        if ($offline_product) {
            $post_data['offline-products'] = true;
        }

        // Different modules, different product selectors because not everyone can order everything,
        // but anything can be created from the backend... Or so they said.
        if ('backend' === $module) {
            $selection = new EnabledProductsSelectorManual($category, $this->getUser(), $post_data, $this->getProducts());
        } else {
            $selection = new OrderableEnabledProductsSelectorManual($category, $this->getUser(), $post_data, $this->getProducts());
        }

        $selection->process();

        $chooser = new BackendProductsChooser($selection);

        return $chooser;
    }

    /**
     * @param \Phalcon\Http\RequestInterface $request
     * @param string $module Optional, defaults to 'backend'
     */
    public function handleProductsChooserSaving($request, $module = 'backend')
    {
        $chooser = $this->getProductsChooser($request, $module);
        $chosen_products = $chooser->getChosenProducts('grouped');

        $paid_ad = false;

        // Process online products
        $product = null;
        if (isset($chosen_products['online']) && is_array($chosen_products['online'])) {
            /* @var $product ProductsInterface */
            $product = $chosen_products['online'][0];
            $product_serialized = serialize($product);

            // Check if significant changes have been made compared to the old product
            // and modify the ad based on the product if needed
            $changed = false;
            if (!$this->online_product) {
                // No old product to check, just save new basically
                $changed = true;
            } else {
                $old_product = unserialize($this->online_product);
                if (!$old_product) {
                    // Info about old product is not available, and we're saving.. so it's changed for sure!
                    $changed = true;

                    // Log some data about failed serialize data
                    if (false === $old_product) {
                        $ad_id = !empty($this->id) ? $this->id : null;
                        $log_message = 'Online product unserialize() failed for ad: ' . $ad_id;
                        $log_message .= "\nData:" . var_export($this->online_product, true);
                        Bootstrap::log($log_message);
                    }
                } else {
                    if ($old_product->getId() !== $product->getId()) {
                        // Completely new product id, save the new one
                        $changed = true;
                    } else {
                        // Same products, but perhaps the selected option changed (in which case we want to modify the ad again)
                        $old_product_selected_option = $old_product->getSelectedOption();
                        $product_selected_option     = $product->getSelectedOption();
                        if ($old_product_selected_option !== $product_selected_option) {
                            $changed = true;
                        }

                        // Check if extras have changed
                        $old_product_extras = $old_product->getSelectedExtras();
                        $product_extras     = $product->getSelectedExtras();
                        if ($old_product_extras !== $product_extras) {
                            $changed = true;
                        }
                    }
                }
            }

            // Store the new one and modify the ad accordingly since it appears the product has
            // changed (or there wasn't any to begin with)
            if ($changed) {
                $this->online_product_id = $product->getId();
                $this->online_product    = $product_serialized;

                $this->modifyWithProduct($product);

                if ($product->getCost() > 0) {
                    $paid_ad = true;
                }
            }
        }

        if (('backend' === $module || $request->hasPost('offline-products'))) {
            // Get current (or create a new export history record)
            $ads_offline_export_history = AdsOfflineExportHistory::getByAppearanceDateOrCreateNew(
                isset($this->id) && $this->id ? $this->id : null,
                ExportToAvus::getNextAppearanceDate()
            );


            // Process offline products (simpler, at least for now)
            if (isset($chosen_products['offline']) && is_array($chosen_products['offline'])) {
                /* @var $offline_product ProductsInterface */
                $offline_product = $chosen_products['offline'][0];
                $offline_product_serialized = serialize($offline_product);

                // Check if significant changes have been made compared to the old product
                // and modify the ad based on the product if needed
                $changed = false;
                if (!$this->offline_product) {
                    // No old product to check, just save new basically
                    $changed = true;
                } else {
                    $old_offline_product = unserialize($this->offline_product);
                    if (!$old_offline_product) {
                        // Info about old product is not available, and we're saving.. so it's changed for sure!
                        $changed = true;

                        // Log some data about failed serialize data
                        if (false === $old_offline_product) {
                            $ad_id = !empty($this->id) ? $this->id : null;
                            $log_message = 'Offline product unserialize() failed for ad: ' . $ad_id;
                            $log_message .= "\nData:" . var_export($this->offline_product, true);
                            Bootstrap::log($log_message);
                        }
                    } else {
                        if ($old_offline_product->getId() !== $offline_product->getId()) {
                            // Completely new product id, save the new one
                            $changed = true;
                        } else {
                            // Same products, but perhaps the selected option changed (in which case we want to modify the ad again)
                            $old_offline_product_selected_option = $old_offline_product->getSelectedOption();
                            $offline_product_selected_option     = $offline_product->getSelectedOption();
                            if ($old_offline_product_selected_option !== $offline_product_selected_option) {
                                $changed = true;
                            }
                        }
                    }
                }
                // Store the new one and modify the ad accordingly since it appears the product has
                // changed (or there wasn't any to begin with)
                if ($changed) {
                    $this->offline_product_id = $offline_product->getId();
                    $this->offline_product    = $offline_product_serialized;

                    // update the product in the record ONLY if it's not processed!
                    if ($ads_offline_export_history->processed == 0) {
                        $ads_offline_export_history->product = $this->offline_product;
                        $ads_offline_export_history->save();
                    }

                    if ($offline_product->getCost() > 0) {
                        $paid_ad = true;
                    }
                }
            } else {
                $this->offline_product_id = null;
                $this->offline_product    = null;
                if (!$ads_offline_export_history->processed && $ads_offline_export_history->exported_id) {
                    // TODO: delete the record in ExportToAvus model?
                    $delete_result = ExportToAvus::deleteExportedRecord($ads_offline_export_history->exported_id);
                    if ($delete_result) {
                        $ads_offline_export_history->delete();
                    }
                }
            }
        } else {
            $this->offline_product_id = null;
            $this->offline_product = null;
        }

        // Making sure existing "paid ads" are treated as such, otherwise
        // they get reset to PAYMENT_STATE_FREE below (when an admin saves an
        // existing ad under certain conditions).
        // This is partially caused by the $paid_ad variable only being set if/when the ad's
        // product has changed above.
        if (!$paid_ad && isset($this->id) && !empty($this->id)) {
            $paid_ad = ($this->latest_payment_state === self::PAYMENT_STATE_PAID);
        }

        // for backend calls to this method, we check if the ad has a product
        // that should be paid. As payment doesn't work in backend as it works on
        // frontend, then we have to immediately change the status of an ad to
        // free or paid - BUT, ONLY IN CASE IT DOESN'T HAVE AN ORDER!!!
        if ('backend' === $module && !$this->hasOutstandingOrdersPreventingAnyPublication()) {
            if ($paid_ad) {
                $this->latest_payment_state = self::PAYMENT_STATE_PAID;
            } else {
                $this->latest_payment_state = self::PAYMENT_STATE_FREE;
            }
        }

        // If/when an ad is saved in the backend and changes have been made, we have
        // to apply product-specific extras rules too
        if ('backend' === $module && isset($changed) && $changed) {
            // A completely new product could've been chosen, or extras on an existing one could've
            // been changed. We're deleting any existing AdsHomepage records here in those cases,
            // and if the new product has those features, the records will be inserted again
            if (isset($this->id) && !empty($this->id)) {
                AdsHomepage::deleteByAdId($this->id);
            }
            // Apply new extras rules if there are any
            if (null !== $product && $product->hasExtras()) {
                $this->processProductExtras($product);
            }
        }
    }

/******************************************************************************/
    /**
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return array
     */
    public function getAdRepublishChosenProductsAndPaymentState($request)
    {
        $chooser = $this->getProductsChooser($request, 'frontend');
        $chosen_products = $chooser->getChosenProducts('grouped');

        $products = array();
        $payment_state = self::PAYMENT_STATE_NEW;

        // Process online products
        if (isset($chosen_products['online']) && is_array($chosen_products['online'])) {
            $payment_state = self::PAYMENT_STATE_FREE;

            /* @var $product ProductsInterface */
            $product            = $chosen_products['online'][0];
            $products['all'][]  = $product;
            $products['online'] = $product;

            if ($product->getCost() > 0) {
                $payment_state = self::PAYMENT_STATE_ORDERED;
            }
        }

        // Process offline products (simpler, at least for now)
        if (isset($chosen_products['offline']) && is_array($chosen_products['offline'])) {
            if (self::PAYMENT_STATE_NEW === $payment_state) {
                $payment_state = self::PAYMENT_STATE_FREE;
            }
            /* @var $offline_product ProductsInterface */
            $offline_product     = $chosen_products['offline'][0];
            $products['all'][]   = $offline_product;
            $products['offline'] = $offline_product;

            if ($offline_product->getCost() > 0) {
                $payment_state = self::PAYMENT_STATE_ORDERED;
            }
        }

        return array(
            'payment_state' => $payment_state,
            'products'      => $products
        );
    }
/******************************************************************************/

    public function getRenderedAd($data = null)
    {
        $cloned = clone $this;
        $parametrizator = new Parametrizator();
        $parametrizator->setCategory($cloned->getCategory());
        $parametrizator->setModule('frontend');
        $parametrizator->setAd($cloned);
        if ($data) {
            $parametrizator->setData($cloned->getData());
        }
        return $parametrizator->getRenderedAd();
    }

    /**
     * Generate an array with all ad's parameters
     * @return array
     */
    public function getData()
    {
        $data = array(
            'ad_title'               => trim($this->title),
            'ad_description'         => trim($this->description),
            'ad_description_offline' => trim($this->description_offline),
            'ad_price'               => $this->getPrice(),
            'ad_price_currency_id'   => intval($this->currency_id),
            'ad_location_1'          => intval($this->country_id),
            'ad_location_2'          => intval($this->county_id),
            'ad_location_3'          => intval($this->city_id),
            'ad_location_4'          => intval($this->municipality_id)
        );
        if ($this->lat || $this->lng) {
            $ad_location_5 = array(
                (floatval($this->lat) ? $this->lat : 0),
                (floatval($this->lng) ? $this->lng : 0)
            );
            $data['ad_location_5'] = implode(',', $ad_location_5);
        }

        if ($this->AdParameters && count($this->AdParameters)) {
            foreach ($this->AdParameters as $AdParameter) {
                $key = 'ad_params_' . $AdParameter->parameter_id . ($AdParameter->level ? '_' . $AdParameter->level : '');
                // Certain param types need some special handling for various reasons...
                if ($AdParameter->Parameter->type_id == 'CHECKBOXGROUP') {
                    $data[$key][] = $AdParameter->value;
                } elseif ($AdParameter->Parameter->type_id == 'DATEINTERVAL') {
                    $value = trim($AdParameter->value);
                    $value_array = explode(',', $value);
                    if (2 === count($value_array)) {
                        $data[$key]['from'] = $value_array[0];
                        $data[$key . '_from'] = $value_array[0];
                        $data[$key]['to'] = $value_array[1];
                        $data[$key . '_to'] = $value_array[1];
                    }
                } elseif ($AdParameter->Parameter->type_id == 'SPINID' || $AdParameter->Parameter->type_id == 'SPINIDGW') {
                    // Makes `ad_params_spinid` and `ad_params_spinidgw` available as "named" params
                    $data['ad_params_' . strtolower($AdParameter->Parameter->type_id)] = $AdParameter->value;
                } else {
                    // All others...
                    $data[$key] = $AdParameter->value;
                }
            }
        }

        if ($this->Media->valid()) {
            $medias = $this->getMedia(array('order' => 'sort_idx ASC'));
            foreach ($medias as $media) {
                $data['ad_media_gallery'][] = $media->media_id;
            }
        }

        return $data;
    }

    /**
     * Get ad's prices (main and exchange-rate-converted price)
     *
     * @return array Array with two members: ['main' => (string) main price, 'other' => (string) other price (or null)]
     */
    public function getPrices()
    {
        $currencies = Currency::all();

        $ad_currency_id = $this->currency_id;
        $ad_currency    = $currencies[$ad_currency_id];

        $prefix        = $ad_currency['prefix'] ? $ad_currency['prefix'] . ' ' : '';
        $suffix        = $ad_currency['suffix'] ? ' ' . $ad_currency['suffix'] : '';
        $ad_main_price = sprintf('%s%s%s', $prefix, Utils::format_money($this->getPrice()), $suffix);

        // If the currency_id is not the same as the default one, display the other price
        $ad_other_price = null;

        $default_currency_id = $this->getDI()->getShared('config')->payment->default_currency_id;
        if ($ad_currency_id != $default_currency_id) {
            // Ad's currency differs from the default, so it becomes the 'other' price
            $ad_other_price = $ad_main_price;

            if ('HRK' === $ad_currency['short_name']) {
                $other_currency = $currencies['EUR'];
            }
            if ('EUR' === $ad_currency['short_name']) {
                $other_currency = $currencies['HRK'];
            }

            $cijena_2 = Utils::lp2kn($this->price * $other_currency['exchange_rate'] / $ad_currency['exchange_rate'], 0);

            $prefix        = $other_currency['prefix'] ? $other_currency['prefix'] . ' ' : '';
            $suffix        = $other_currency['suffix'] ? ' ' . $other_currency['suffix'] : '';
            $ad_main_price = sprintf('%s%s%s', $prefix, Utils::format_money($cijena_2), $suffix);
        }

        $ad_price = array(
            'main'  => $ad_main_price,
            'other' => $ad_other_price
        );

        return $this->price ? $ad_price : null;
    }

    public static function buildPrices($currency_id, $price)
    {
        static $currencies = null;
        static $default_currency_id = null;

        if ($price) {
            // Since this method gets called at least 20 times in certain loops...
            if (null === $currencies) {
                $currencies          = Currency::all();
                $default_currency_id = Di::getDefault()->getShared('config')->payment->default_currency_id;
            }

            $currency   = $currencies[$currency_id];
            $prefix     = $currency['prefix'] ? $currency['prefix'] . ' ' : '';
            $suffix     = $currency['suffix'] ? ' ' . $currency['suffix'] : '';
            $main_price = sprintf('%s%s%s', $prefix, Utils::format_money(self::buildPrice($price)), $suffix);

            // If the currency_id is not the same as the default one, display the other price
            $other_price = null;
            if ($currency_id != $default_currency_id) {
                // Ad's currency differs from the default, so it becomes the 'other' price
                $other_price = $main_price;

                if ('HRK' === $currency['short_name']) {
                    $other_currency = $currencies['EUR'];
                }
                if ('EUR' === $currency['short_name']) {
                    $other_currency = $currencies['HRK'];
                }

                $price_tmp = Utils::lp2kn($price * $other_currency['exchange_rate'] / $currency['exchange_rate'], 0);

                $prefix     = $other_currency['prefix'] ? $other_currency['prefix'] . ' ' : '';
                $suffix     = $other_currency['suffix'] ? ' ' . $other_currency['suffix'] : '';
                $main_price = sprintf('%s%s%s', $prefix, Utils::format_money($price_tmp), $suffix);
            }

            $prices = array(
                'main'  => $main_price,
                'other' => $other_price
            );

            return $prices;
        }

        return null;
    }

    public function getTitle()
    {
        return strip_tags($this->title);
    }

    /**
     * Prepares and sends a contact email message.
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return bool
     * @throws \phpmailerException depending on Email class config
     */
    public function send_contact_email($request)
    {
        $messageData = array(
            'user_id'      => $this->user_id,
            'entity'       => 'ads',
            'entity_id'    => $this->id,
            'sender_name'  => strip_tags($request->getPost('name')),
            'sender_email' => strip_tags($request->getPost('email')),
            'message'      => strip_tags($request->getPost('message'))
        );

        return UsersMessages::send($messageData, 'adcontact', $this);
    }

    /**
     * Spam/Not Spam ad, depending on the previous state
     *
     * @return bool Whether the update was successful or not
     */
    public function spamToggle()
    {
        $this->disable_notnull_validations();
        $this->is_spam = (int) !$this->is_spam;
        if ($this->is_spam) {
            $this->moderation = 'nok';
            $this->moderation_msg = 'Spam';
        } else {

        }
        $result = $this->update();
        $this->enable_notnull_validations();
        return $result;
    }

    public function sellUserAdsById($user_id, $ad_id)
    {
        $result = false;

        if ($ads = $this->getUserAdsById($user_id, $ad_id)) {
            if ($result = $ads->update(array('active' => 0, 'manual_deactivation' => 0, 'sold' => 1))) {
                AdsHomepage::setActiveForAdId($ad_id, 0);
            }
        }

        return $result;
    }

    public function activateUserAdsById($user_id, $ad_id)
    {
        $result = false;

        if ($ads = $this->getUserAdsById($user_id, $ad_id)) {
            if ($result = $ads->update(array('active' => 1, 'manual_deactivation' => 0))) {
                AdsHomepage::setActiveForAdId($ad_id, 1);
            }
        }

        return $result;
    }

    public function deactivateUserAdsById($user_id, $ad_id)
    {
        $result = false;

        if ($ads = $this->getUserAdsById($user_id, $ad_id)) {
            if ($result = $ads->update(array('active' => 0, 'manual_deactivation' => 1))) {
                AdsHomepage::setActiveForAdId($ad_id, 0);
            }
        }

        return $result;
    }

    /**
     * Active/Inactive ad, depending on the previous state
     *
     * @return bool Whether the update was successful or not
     */
    public function activeToggle()
    {
        $this->disable_notnull_validations();
        $this->active = (int) !$this->active;
        $result = $this->update();
        $this->enable_notnull_validations();

        // Copy the new active state over to the homepage ads bucket
        AdsHomepage::setActiveForAdId($this->id, $this->active);

        return $result;
    }

    private static function getExcludedCategoriesAndUsers()
    {
        $result = array(
            'categories' => array(),
            'users'      => array()
        );

        $restricted_categories = CategoriesSettings::getAgeRestrictedCategoryIdsList();
        if (!empty($restricted_categories)) {
            $result['categories'] = array_keys($restricted_categories);
        }

        // Additional category ids excluded via new admin page
        $excluded_category_ids_admin = MiscSettings::getLatestAdsCategoryIdExclusionList();
        if (!empty($excluded_category_ids_admin)) {
            $result['categories'] = array_merge($result['categories'], $excluded_category_ids_admin);
        }

        // Excluding user ids specified via the new admin page too
        $result['users'] = MiscSettings::getLatestAdsUserIdExclusionList();

        return $result;
    }

    /**
     * Get latest ads
     *
     * @param int $count Number of "latest" ads to return
     *
     * @return mixed|\Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function getLatest($count = 4)
    {
/*
        $results = self::find(array(
            'conditions' => 'is_spam=0 AND active=1',
            'order'      => 'created_at DESC',
            'limit'      => $count
        ));
*/
        $excluded = self::getExcludedCategoriesAndUsers();

        // Attempting to show only latest ads that have an image... Will not work for long
        // TODO/FIXME: what about moderation status? What about other shit like that?
        // TODO/FIXME: performance of this is terrible - ORDER BY published_at, really? What about product_sort, sort_date? Or we'll need to index published_at field too...
        $builder = new Builder();
        $builder->columns(array(
            'Baseapp\Models\Ads.*'
        ));
        $builder->from('Baseapp\Models\Ads');
        $builder->innerJoin('Baseapp\Models\AdsMedia', 'Baseapp\Models\Ads.id = Baseapp\Models\AdsMedia.ad_id AND Baseapp\Models\AdsMedia.sort_idx = 1');
        $builder->andWhere('Baseapp\Models\Ads.is_spam = 0 AND Baseapp\Models\Ads.active = 1');
        $builder->andWhere('Baseapp\Models\Ads.moderation = "ok"');
        if (!empty($excluded['categories'])) {
            $builder->andWhere('Baseapp\Models\Ads.category_id NOT IN (' . implode(',', $excluded['categories']) . ')');
        }
        if (!empty($excluded['users'])) {
            $builder->andWhere('Baseapp\Models\Ads.user_id NOT IN (' . implode(',', $excluded['users']) . ')');
        }
        $builder->groupBy('Baseapp\Models\Ads.id');
        $builder->orderBy('Baseapp\Models\Ads.published_at DESC');
        $builder->limit($count);

        $results = $builder->getQuery()->execute();

        return $results;
    }

    /**
     * Get fresh/new/recent ads
     *
     * @param int $count Number of "latest" ads to return
     *
     * @return mixed|\Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function getFresh($count = 24)
    {
        $excluded = self::getExcludedCategoriesAndUsers();

        // TODO/FIXME: performance is still terrible, but at least we're not sorting by published_at for now...
        $builder = new Builder();
        $builder->columns(array(
            'Baseapp\Models\Ads.*'
        ));
        $builder->from('Baseapp\Models\Ads');
        // Don't care about images for now?
        // $builder->innerJoin('Baseapp\Models\AdsMedia', 'Baseapp\Models\Ads.id = Baseapp\Models\AdsMedia.ad_id AND Baseapp\Models\AdsMedia.sort_idx = 1');
        $builder->andWhere('Baseapp\Models\Ads.is_spam = 0 AND Baseapp\Models\Ads.active = 1');
        $builder->andWhere('Baseapp\Models\Ads.moderation = "ok"');
        if (!empty($excluded['categories'])) {
            $builder->andWhere('Baseapp\Models\Ads.category_id NOT IN (' . implode(',', $excluded['categories']) . ')');
        }
        if (!empty($excluded['users'])) {
            $builder->andWhere('Baseapp\Models\Ads.user_id NOT IN (' . implode(',', $excluded['users']) . ')');
        }
        $builder->groupBy('Baseapp\Models\Ads.id');
        $builder->orderBy('Baseapp\Models\Ads.id DESC');
        $builder->limit($count);

        $results = $builder->getQuery()->execute();

        return $results;
    }

    /**
     * Get today's best performing (most viewed) ads
     *
     * @param int $count Number of "best performing" ads to return
     *
     * @return array|null;
     */
    public static function getTodaysBestPerforming($count = 4)
    {
        $excluded = self::getExcludedCategoriesAndUsers();

        // Attempting to show only best performing (with most views today) ads that have an image... Will not work for long
        // TODO/FIXME: what about moderation status? What about other shit like that?
        // TODO/FIXME: performance of this is terrible - ORDER BY published_at, really? What about product_sort, sort_date? Or we'll need to index published_at field too...

        $builder = new Builder();
        $period = date('Ymd', Utils::getRequestTime());
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->innerJoin('Baseapp\Models\AdsMedia', 'ad.id = adm.ad_id AND adm.sort_idx = 1', 'adm');
        $builder->innerJoin('Baseapp\Models\AdsViews', 'ad.id = adv.ad_id AND adv.type = 0 AND adv.period = "' . $period . '"', 'adv');
        $builder->where('ad.active = 1 AND ad.is_spam = 0 AND ad.moderation = "ok"');
        if (!empty($excluded['categories'])) {
            $builder->andWhere('ad.category_id NOT IN (' . implode(',', $excluded['categories']) . ')');
        }
        if (!empty($excluded['users'])) {
            $builder->andWhere('ad.user_id NOT IN (' . implode(',', $excluded['users']) . ')');
        }
        $builder->groupBy('ad.id');
        $builder->orderBy('adv.count DESC, ad.published_at DESC');
        $builder->limit($count);

        $results = $builder->getQuery()->execute();

        if (count($results)) {
            return self::getEnrichedFrontendBasicResultsArray($results);
        }

        return null;
    }

    /**
     * Attempting to return ads with most views in recent days (that have an image). Will not work for long.
     *
     * @param $limit
     *
     * @return array|null
     */
    public static function getRecentlyPopular($limit)
    {
        $excluded = self::getExcludedCategoriesAndUsers();
        $builder  = new Builder();

        $today_string = date('Ymd', Utils::getRequestTime());
        $yesterday_string = date('Ymd', strtotime('yesterday'));
        $periods_in = ' IN ("' . $today_string . '", "' . $yesterday_string . '")';

        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->innerJoin('Baseapp\Models\AdsMedia', 'ad.id = adm.ad_id AND adm.sort_idx = 1', 'adm');
        $builder->innerJoin('Baseapp\Models\AdsViews', 'ad.id = adv.ad_id AND adv.type = 0 AND adv.period ' . $periods_in, 'adv');
        $builder->where('ad.active = 1 AND ad.is_spam = 0 AND ad.moderation = "ok"');

        if (!empty($excluded['categories'])) {
            $builder->andWhere('ad.category_id NOT IN (' . implode(',', $excluded['categories']) . ')');
        }
        if (!empty($excluded['users'])) {
            $builder->andWhere('ad.user_id NOT IN (' . implode(',', $excluded['users']) . ')');
        }

        $builder->groupBy('ad.id');
        $builder->orderBy('adv.count DESC, ad.published_at DESC');
        $builder->limit($limit);

        $results = $builder->getQuery()->execute();

        if (count($results)) {
            return self::getEnrichedFrontendBasicResultsArray($results);
        }

        return null;
    }

    public static function buildLatestCachedSidebarMarkup()
    {
        $results = self::getLatest(3);

        $escaper = new Escaper();

        $markup = '';
        $cnt = 0;
        $total = count($results);

        if ($total > 0) {
            $markup .= '<h2 class="underline"><span class="underline">Najnoviji oglasi</span></h2>';
        }

        foreach ($results as $result) {
            $cnt++;
            $first_last = '';
            if (1 === $cnt) {
                $first_last = ' first';
            }
            if ($cnt === $total) {
                $first_last = ' last';
            }

            $title       = $result->getTitle();
            $description = $result->description_tpl;
            if (empty($description)) {
                $description = $result->description;
            }

            $description = strip_tags($description);

            $link  = $result->get_frontend_view_link();
            $thumb = $result->get_thumb('Oglas-120x90');
            $thumb->setAlt('');

            $markup .= '<div class="post-box post-box-small' . $first_last . '">';
            $markup .= '<a href="' . $link . '">' . $thumb->getTag() . '</a>';
            $markup .= '<div class="info">';
            $markup .= '<h3><a href="' . $link .'">' . $escaper->escapeHtml($title) . '</a></h3>';
            $markup .= '<p>' . $escaper->escapeHtml($description) . '</p>';
            $markup .= '</div>';
            $markup .= '</div>';
        }

        if (!empty($markup)) {
            self::storeLatestCachedSidebarMarkup($markup);
        }

        return $markup;
    }

    /**
     * @param $data
     * @param $lifetime
     *
     * @return bool
     */
    public static function storeLatestCachedSidebarMarkup($data, $lifetime = null)
    {
        $ret = null;
        $cache = self::getMemcache();
        if ($cache) {
            try {
                $ret = $cache->save('latest-ads-sidebar', $data, $lifetime);
            } catch (\Phalcon\Cache\Exception $e) {
                $ret = false;
                Bootstrap::log($e);
            }
        }

        return $ret;
    }

    /**
     * @return mixed|null
     */
    public static function getLatestCachedSidebarMarkup()
    {
        $cache_key = 'latest-ads-sidebar';
        $result    = null;
        $cache     = self::getMemcache();
        if ($cache) {
            try {
                $result = $cache->get($cache_key);
            } catch (\Phalcon\Cache\Exception $e) {
                Bootstrap::log($e);
            }
        }

        return $result;
    }

    /**
     * Returns either the complete HTML markup for the ad's frontend view
     * or just the URL (which is then used as the "href").
     *
     * Used primarily to keep the link format/definition in a single place since
     * we've started providing better "Ad updated/created" success messages
     * in the backend too. Having a link within the message makes it
     * a lot easier to see the frontend version in a new tab/window).
     *
     * @param string $type Optional. Defaults to 'href' (returning just the URL).
     *                     If specified as 'html' returns the complete <a href...> markup.
     *
     * @return string
     */
    public function get_frontend_view_link($type = 'href')
    {
        $link = null;

        if (isset($this->id)) {
            //  server.domain.name/[category_slug]/[ad-title]-oglas-[ad-id]

            $category_slug = $this->getCategory()->url;
            $ad_title_slug = Utils::slugify($this->title);

            $url  = '/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $this->id;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get($category_slug . '/' . $ad_title_slug . '-oglas-' . $this->id);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><i class="fa fa-fw fa-eye"></i>%s</a>';
                $link   = sprintf($markup, $url, 'View ad');
            }
        }

        return $link;
    }

    public static function buildFrontendViewLink(\stdClass $row, $type = 'href')
    {
        $link = null;

        if (isset($row->id)) {
            //  server.domain.name/[category_slug]/[ad-title]-oglas-[ad-id]

            $di = Di::getDefault();
            $tree = $di->get(Categories::MEMCACHED_KEY);
            $category_slug = $tree[$row->category_id]->url;
            $ad_title_slug = Utils::slugify($row->title);

            $url  = '/' . $category_slug . '/' . $ad_title_slug . '-oglas-' . $row->id;

            // Use the 'url' resolver service if present, making it more flexible
            if ($di->has('url')) {
                $url = $di->get('url')->get($category_slug . '/' . $ad_title_slug . '-oglas-' . $row->id);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><span class="fa fa-fw fa-eye"></span>%s</a>';
                $link   = sprintf($markup, $url, 'View ad');
            }
        }

        return $link;
    }

    /**
     * Helper method to get ad status so we can better present what's happening
     * with the ad in different stages (paid, not paid, waiting for moderation, ...)
     * @return int Integer representation of an ad status
     */
    public function get_status()
    {
        $status = 0;
        if ($this->id) {
            // first thing to check is, if this ad has completed the process of
            // submission. We know this by checking if it has online_product set

            if (!empty($this->online_product) && !empty($this->online_product_id)) {
                if ($this->active) {
                    $status = 1;

                    $unpaid_orders = Orders::findUnpaidOrdersForAd($this->id);

                    if ('waiting' === $this->moderation) {
                        $status = 14;
                    } elseif (count($unpaid_orders)) {
                        $status = 15;
                    }
                } else {
                    $hasOutstandingOrdersPreventingAnyPublication = $this->hasOutstandingOrdersPreventingAnyPublication();

                    if ($this->sold) {
                        $status = 7;
                        if (!$this->canBeRepublished()) {
                            $status = 71;
                        }

                        /**
                         * TODO/FIXME: This is a quick fix for this case as ad still has old products attached to it
                         * and it might me that they are free and don't require payment. We have to see for this ad's
                         * unpaid orders in order to decide whether we're waiting for payment or not.
                         *
                         * But, the problem is that one ad can have multiple order.. and if at least one unpaid order is
                         * found, this will change ad's status, which might be wrong. For now, admins have to be carefull
                         * in deciding which order is cancelled, which is created, which is completed...
                         */
                        $unpaid_orders = Orders::findUnpaidOrdersForAd($this->id);
                        if (count($unpaid_orders)) {
                            $status = 70;
                        }
                    } elseif ($this->is_spam) {
                        $status = 6;
                    } elseif ($this->manual_deactivation) {
                        $status = 3;
                    } elseif ($this->isExpired()) {
                        $status = 2;
                        if (!$this->canBeRepublished()) {
                            $status = 21;
                        }

                        /**
                         * TODO/FIXME: This is a quick fix for this case as ad still has old products attached to it
                         * and it might me that they are free and don't require payment. We have to see for this ad's
                         * unpaid orders in order to decide whether we're waiting for payment or not.
                         *
                         * But, the problem is that one ad can have multiple order.. and if at least one unpaid order is
                         * found, this will change ad's status, which might be wrong. For now, admins have to be carefull
                         * in deciding which order is cancelled, which is created, which is completed...
                         */
                        $unpaid_orders = Orders::findUnpaidOrdersForAd($this->id);
                        if (count($unpaid_orders)) {
                            $status = 20;
                        }
                    } elseif ($this->latest_payment_state == Ads::PAYMENT_STATE_CANCELED_ORDER) {
                        $status = 0;
                    } elseif ('waiting' === $this->moderation) {
                        $status = 4;

                        if ($hasOutstandingOrdersPreventingAnyPublication) {
                            $status = 41;
                        }
                    } elseif ('nok' === $this->moderation) {
                        $status = 5;
                    } elseif ('ok' === $this->moderation && $hasOutstandingOrdersPreventingAnyPublication) {
                        $status = 41;
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Helper method to get all active ads
     *
     * @param int $user_id Filter ads only for this user
     * @param bool $return_builder
     *
     * @return Ads[]|\Phalcon\Mvc\Model\Query\Builder
     */
    public function getAllAds($user_id = null, $return_builder = false)
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        if (null !== $user_id) {
            $builder->where(
                'ad.soft_delete = 0 AND ad.user_id = :user_id:',
                array(
                    'user_id' => intval($user_id)
                )
            );
        }
        $builder->orderBy('ad.id DESC');

        if ($return_builder) {
            return $builder;
        }
        $ads = $builder->getQuery()->execute();

        return $ads;
    }

    /**
     * Get user's ads by id(s)
     * 
     * @param  int $user_id
     * @param  int|string|array $ad_id
     * 
     * @return null|\Baseapp\Models\Ads|\Baseapp\Models\Ads[]
     */
    public function getUserAdsById($user_id, $ad_id)
    {
        $ad_ids = array();

        if (is_array($ad_id)) {
            $ad_ids = $ad_id;
        } elseif (strpos($ad_id, ',') !== false) {
            $ad_ids = explode(',', $ad_id);
        } elseif ((int) $ad_id) {
            $ad_ids[] = (int) $ad_id;
        }

        if (count($ad_ids)) {
            if (count($ad_ids) == 1) {
                return self::findFirst(array(
                    'conditions' => 'id = :ad_id: AND soft_delete = 0 AND user_id = :user_id:',
                    'bind'       => array(
                        'ad_id'   => $ad_ids[0],
                        'user_id' => (int) $user_id
                    )
                ));
            } else {
                return self::find(array(
                    'conditions' => 'id IN ({ad_id:array}) AND soft_delete = 0 AND user_id = :user_id:',
                    'bind'       => array(
                        'ad_id'   => $ad_ids,
                        'user_id' => (int) $user_id
                    )
                ));
            }
        }

        return null;
    }

    /**
     * Helper method to get all favorited ads for a user
     *
     * @param int $user_id Filter ads only for this user
     * @param bool $return_builder
     *
     * @return Ads[]|\Phalcon\Mvc\Model\Query\Builder
     */
    public function getFavoriteAds($user_id, $return_builder = false)
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->columns(array(
            'ad.*',
            'ufa.note AS favorited_ad_note'
        ));
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->innerJoin('Baseapp\Models\UsersFavoriteAds', 'ad.id = ufa.ad_id', 'ufa');
        $builder->where(
            'ad.soft_delete = 0 AND ufa.user_id = :user_id:',
            array(
                'user_id' => intval($user_id)
            )
        );
        $builder->orderBy('ad.id DESC');

        if ($return_builder) {
            return $builder;
        }
        $ads = $builder->getQuery()->execute();

        return $ads;
    }

    /**
     * Helper method to get all active ads
     *
     * @param int $user_id Filter ads only for this user
     * @param bool $return_builder
     *
     * @return Ads[]|\Phalcon\Mvc\Model\Query\Builder
     */
    public function getActiveAds($user_id = null, $return_builder = false, $sort = null)
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->where('ad.active = 1');
        if (null !== $user_id) {
            $builder->andWhere(
                'ad.soft_delete = 0 AND ad.user_id = :user_id:',
                array(
                    'user_id' => intval($user_id)
                )
            );
        }
        if ($sort) {
            $builder->orderBy($sort);
        } else {
            $builder->orderBy('ad.id DESC');
        }

        if ($return_builder) {
            return $builder;
        }
        $ads = $builder->getQuery()->execute();

        return $ads;
    }

    /**
     * Helper method to get all expired ads
     *
     * @param int $user_id Filter ads only for this user
     * @param bool $return_builder
     *
     * @return Ads[]|\Phalcon\Mvc\Model\Query\Builder
     */
    public function getExpiredAds($user_id = null, $return_builder = false)
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->where('ad.active = 0');
        $builder->andWhere(
            'ad.expires_at < :current_time:',
            array(
                'current_time' => Utils::getRequestTime()
            )
        );
/*
        $builder->andWhere(
            'moderation = :moderation_status:',
            array(
                'moderation_status' => 'ok'
            )
        );
*/
        if (null !== $user_id) {
            $builder->andWhere(
                'ad.soft_delete = 0 AND ad.user_id = :user_id:',
                array(
                    'user_id' => intval($user_id)
                )
            );
        }
        $builder->orderBy('ad.id DESC');

        if ($return_builder) {
            return $builder;
        }
        $ads = $builder->getQuery()->execute();

        return $ads;
    }

    /**
     * Helper method to get all manualy deactivated ads
     *
     * @param int $user_id Filter ads only for this user
     * @param bool $return_builder
     *
     * @return Ads[]|\Phalcon\Mvc\Model\Query\Builder
     */
    public function getDeactivatedAds($user_id = null, $return_builder = false)
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->where('ad.manual_deactivation = 1');
        if (null !== $user_id) {
            $builder->andWhere(
                'ad.soft_delete = 0 AND ad.user_id = :user_id:',
                array(
                    'user_id' => intval($user_id)
                )
            );
        }
        $builder->orderBy('ad.id DESC');

        if ($return_builder) {
            return $builder;
        }
        $ads = $builder->getQuery()->execute();

        return $ads;
    }

    /**
     * Helper method to get all inactive ads
     *
     * @param int $user_id Filter ads only for this user
     * @param bool $return_builder
     *
     * @return Ads[]|\Phalcon\Mvc\Model\Query\Builder
     */
    public function getInactiveAds($user_id = null, $return_builder = false)
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->inWhere('moderation', ['waiting', 'nok', 'ok']);
        $builder->andWhere(
            'ad.soft_delete = 0 AND ad.active = 0 AND ad.manual_deactivation = 0 AND ad.sold = 0 AND (ad.expires_at IS NULL OR ad.expires_at >= :current_time:)',
            array(
                'current_time' => Utils::getRequestTime()
            )
        );
        if (null !== $user_id) {
            $builder->andWhere(
                'ad.user_id = :user_id:',
                array(
                    'user_id' => intval($user_id)
                )
            );
        }
        $builder->orderBy('ad.id DESC');

        if ($return_builder) {
            return $builder;
        }
        $ads = $builder->getQuery()->execute();

        return $ads;
    }

    /**
     * Helper method to get all sold ads
     *
     * @param int $user_id Filter ads only for this user
     * @param bool $return_builder
     *
     * @return Ads[]|\Phalcon\Mvc\Model\Query\Builder
     */
    public function getSoldAds($user_id = null, $return_builder = false)
    {
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->where('ad.active = 0 AND ad.sold = 1');
        if (null !== $user_id) {
            $builder->andWhere(
                'ad.soft_delete = 0 AND ad.user_id = :user_id:',
                array(
                    'user_id' => intval($user_id)
                )
            );
        }
        $builder->orderBy('ad.id DESC');

        if ($return_builder) {
            return $builder;
        }
        $ads = $builder->getQuery()->execute();

        return $ads;
    }

    public function getAllRelatedOrders($limit = null)
    {
        return Orders::findOrdersForAd($this, $limit);
    }

    /**
     * Iterates over the Ad's Products calling $callback on each one and returns
     * true if the result of $callback invocation (which is being passed the $product as the first argument) returns true.
     *
     * @param callable $callback
     *
     * @return bool
     */
    protected function hasProductsMatching(callable $callback)
    {
        $found = false;

        $products = $this->getProducts();
        if ($products) {
            foreach ($products as $product) {
                if ($callback($product)) {
                    $found = true;
                    break;
                }
            }
        }

        return $found;
    }

    /**
     * Returns true if the Ad has any Online products assigned/chosen that require payment before publication
     *
     * @return bool
     */
    public function hasOnlineProductsRequiringPaymentBeforePublication()
    {
        $callback = function(ProductsInterface $product) {
            return ($product instanceof Online && $product->isPaymentRequiredBeforePublication());
        };

        return $this->hasProductsMatching($callback);
    }

    /**
     * Returns true if the Ad has any Offline products assigned/chosen that require payment before publication
     *
     * @return bool
     */
    public function hasOfflineProductsRequiringPaymentBeforePublication()
    {
        $callback = function(ProductsInterface $product) {
            return ($product instanceof Offline && $product->isPaymentRequiredBeforePublication());
        };

        return $this->hasProductsMatching($callback);
    }

    /**
     * Returns true if the Ad has any products assigned/chosen that require payment before publication
     * (regardless of product subtype)
     *
     * @return bool
     */
    public function hasAnyProductsRequiringPaymentBeforePublication()
    {
        $callback = function(ProductsInterface $product) {
            return $product->isPaymentRequiredBeforePublication();
        };

        return $this->hasProductsMatching($callback);
    }


    /**
     * Checks for any unpaid Orders for Online products assigned to this Ad
     * (that require payment before publication)
     *
     * @return bool
     */
    public function hasOutstandingOrdersPreventingOnlinePublication()
    {
        $has_products_preventing_publication = $this->hasOnlineProductsRequiringPaymentBeforePublication();

        // TODO: is this really the way it's supposed to be? Where can/will this fail?

        // If any of the Online product(s) currently assigned
        // has matching unpaid orders, we should be blocked from publication
        // If there aren't any, we can publish?

        $found_outstanding_orders = false;
        if ($has_products_preventing_publication) {
            $orders = Orders::findUnpaidOrdersForAd(clone $this);
            //var_dump($orders);
            //exit;
            if (!empty($orders)) {
                $found_outstanding_orders = true;
            }
        }

        return $found_outstanding_orders;
    }

    /**
     * Checks for any unpaid Orders for Offline products assigned to this Ad
     * (that require payment before exporting to Avus)
     *
     * @return bool
     */
    public function hasOutstandingOrdersPreventingOfflinePublication()
    {
        $has_products_preventing_publication = $this->hasOfflineProductsRequiringPaymentBeforePublication();

        $found_outstanding_orders = false;
        if ($has_products_preventing_publication) {
            $orders = Orders::findUnpaidOrdersForAd(clone $this);
            if (!empty($orders)) {
                $found_outstanding_orders = true;
            }
        }

        return $found_outstanding_orders;
    }

    public function hasOutstandingOrdersPreventingAnyPublication()
    {
        $has_online_products_preventing_publication = $this->hasOnlineProductsRequiringPaymentBeforePublication();
        $has_offline_products_preventing_publication = $this->hasOfflineProductsRequiringPaymentBeforePublication();

        $found_outstanding_orders = false;
        if ($has_online_products_preventing_publication || $has_offline_products_preventing_publication) {
            $orders = Orders::findUnpaidOrdersForAd(clone $this);
            if (!empty($orders)) {
                $found_outstanding_orders = true;
            }
        }

        return $found_outstanding_orders;
    }

    protected static function get_possible_listing_options(Ads $ad, $ad_status = null, Users $user = null)
    {
        $links = array();

        if (null === $ad_status) {
            $ad_status = $ad->get_status();
        }

		//NIKOLA: provjera je li ad iz backenda
		$jeFrontend = $ad->n_source == 'frontend'? true : false;
		
        switch ($ad_status) {
            case 0:
                // continue with submission
                if ($jeFrontend) $links[] = '<button data-href="/predaja-oglasa?ad_id=' . $ad->id . '">Nastavi s predajom</button>';
                if ($jeFrontend) $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obri�i</button>';

                break;
            case 1:
            case 14:
            case 15:
                // active
                if ($jeFrontend) $links[] = '<button data-href="/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                if ($jeFrontend) $links[] = '<button data-href="/moj-kutak/deaktiviraj-oglas/' . $ad->id . '" data-type="deactivate">Deaktiviraj</button>';
                if ($jeFrontend) $links[] = '<button data-href="/moj-kutak/prodani-oglas/' . $ad->id . '" data-type="sold">Prodano</button>';

                $ads_online_product = $ad->getProduct('online');
                if ($ad_status == 1 && $ads_online_product) {
                    if ($ads_online_product->isUpgradable()) {
                        if ($jeFrontend) $links[] = '<button data-href="/upgrade/' . $ad->id . '">Izdvoji ovaj oglas</button>';
                    }
                }

                // ads waiting to be paid
                if ($unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order) {
                        if ($jeFrontend) $links[] = '<button class="payment-details-btn" data-order-id="' . $latest_order->id . '">Podaci za pla�anje<span class="fa fa-search-plus fa-fw"></span></button>';
                    }
                }

                // See if we should show the pushup button
                $pushup_data = $ad->toArray();
                $pushup_data['is_owner'] = ($user->id == $ad->user_id);
                if (Ads::isPushUpDoable($pushup_data)) {
                    // Making sure the ad doesn't have any unpaid pushup orders already
                    $unpaid_resultset = Orders::findUnpaidPushUpOrders($user, array($ad->id));
                    if (empty($unpaid_resultset)) {
                        if ($jeFrontend) $links[] = '<button class="push-up-btn" data-href="/push-up/' . $ad->id . '">Skok na vrh</button>';
                    }
                }

                if ($jeFrontend)  $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obri�i</button>';
                $links[] = '<a href="' . $ad->get_frontend_view_link() . '" class="btn btn-default">Pogledaj oglas</a>';

                break;
            case 2:
            case 20:
            case 21:
                // expired
                if ($ad_status != 20 && $ad->canBeRepublished()) {
                    if ($jeFrontend) $links[] = '<button data-href="/republish/' . $ad->id . '">Ponovi</button>';
                } else {
                    if ($jeFrontend) $links[] = '<button data-href="/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                }
                if ($ad_status == 20 && $unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order && !$latest_order->isExpired()) {
                        if ($jeFrontend) $links[] = '<button class="payment-details-btn" data-order-id="' . $latest_order->id . '">Podaci za pla�anje<span class="fa fa-search-plus fa-fw"></span></button>';
                    }
                }

                if ($jeFrontend)  $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obri�i</button>';
                $links[] = '<a href="' . $ad->get_frontend_view_link() . '" class="btn btn-default">Pogledaj oglas</a>';

                break;
            case 3:
                // deactivated
                if ($jeFrontend) $links[] = '<button data-href="/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                if ($jeFrontend) $links[] = '<button data-href="/moj-kutak/aktiviraj-oglas/' . $ad->id . '" data-type="activate">Aktiviraj</button>';
                if ($jeFrontend)  $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obri�i</button>';
                $links[] = '<a href="' . $ad->get_frontend_view_link() . '" class="btn btn-default">Pogledaj oglas</a>';

                break;

            case 5:
                if ($jeFrontend)  $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obri�i</button>';
                break;

            case 4:
            case 41:
            case 6:
                // inactive
                if ($jeFrontend) $links[] = '<button data-href="/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                if ($jeFrontend)  $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obri�i</button>';
                $links[] = '<a href="' . $ad->get_frontend_view_link() . '" class="btn btn-default">Pogledaj oglas</a>';

            case 41:
                // ads waiting to be paid
                if ($unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order && !$latest_order->isExpired()) {
                        if ($jeFrontend) $links[] = '<button class="payment-details-btn" data-order-id="' . $latest_order->id . '">Podaci za pla�anje<span class="fa fa-search-plus fa-fw"></span></button>';
                    }
                }
                break;

            case 7:
            case 70:
            case 71:
                // sold
                if ($ad_status != 70 && $ad->canBeRepublished()) {
                    if ($jeFrontend) $links[] = '<button data-href="/republish/' . $ad->id . '">Ponovi</button>';
                } else {
                    if ($jeFrontend) $links[] = '<button data-href="/moj-kutak/uredi-oglas/' . $ad->id . '">Uredi</button>';
                }
                if ($ad_status == 70 && $unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order && !$latest_order->isExpired()) {
                        if ($jeFrontend) $links[] = '<button class="payment-details-btn" data-order-id="' . $latest_order->id . '">Podaci za pla�anje<span class="fa fa-search-plus fa-fw"></span></button>';
                    }
                }

                if ($jeFrontend)  $links[] = '<button class="delete-btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-formurl="/moj-kutak/obrisi-oglas/' . $ad->id . '" data-type="delete">Obri�i</button>';
                $links[] = '<a href="' . $ad->get_frontend_view_link() . '" class="btn btn-default">Pogledaj oglas</a>';
                break;
        }

        return $links;
    }

    /**
     * Enrich $results with Media files given in $media_ids array
     *
     * @param array $results             Array of Ads we'll be working on
     * @param array $media_ids           Array of media id's
     * @param array $thumb_style_details Thumbnail settings array
     *
     * @return array           Enriched $results array
     */
    protected static function enrichResultsWithMedia($results, $media_ids, $thumb_style_details)
    {
        if (!empty($media_ids)) {
            if ($ad_medias = (new Media())->get_multiple_ids($media_ids)) {
                foreach ($results as $k => $ad) {
                    foreach ($ad_medias as $media) {
                        if ($media->id == $ad['media_id']) {
                            $ad['thumb_pic'] = $media->get_thumb($thumb_style_details);
                        }
                    }
                    $results[$k] = $ad;
                }
            }
        }

        return $results;
    }

    protected static function enrichResultsWithUsers($results, $user_ids)
    {
        if (!empty($user_ids)) {
            if ($ad_users = (new Users())->get_multiple_ids($user_ids)) {
                foreach ($results as $k => $ad) {
                    foreach ($ad_users as $row) {
                        $user = $row;
                        $shop = null;
                        if (isset($row->user)) {
                            $user = $row->user;
                            $shop = isset($row->shop) ? $row->shop : null;
                        }
                        if ($user->id == $ad['user_id']) {
                            $ad['user'] = $user;
                            if ($shop && $shop->title && $shop->isActive()) {
                                $ad['shop'] = $shop;
                            }
                        }
                    }
                    $results[$k] = $ad;
                }
            }
        }

        return $results;
    }

    /**
     * Transforms the Resultset into a regular array containing all the needed info already formatted
     * so it could just be echo-ed in the view. Also, resulting array contains main ad images.
     *
     * @param Resultset $rows Resultset we'll be working on
     * @param string $module Module where the ads will be displayed
     * @param Users|null $user Optional. Currently logged in user. For frontend purposes.
     * @return array
     */
    public static function buildResultsArray(Resultset &$rows, $module = 'backend', Users $user = null)
    {
        $results = array();

        // Bail if the Resultset ain't valid
        if (!$rows->valid()) {
            return $results;
        }

        $ids       = array();
        $media_ids = array();
        $user_ids  = array();
        $results   = array();

        $thumb_style_details = ('backend' === $module ? 'Oglas-120x90' : 'GridView');
        $no_pic = (new Media())->getNoImageThumb($thumb_style_details);

        // Init the category tree for results decoration (only once in case of repeated calls)
        static $cat_tree = null;
        if (null === $cat_tree) {
            $cat_tree = Di::getDefault()->get(Categories::MEMCACHED_KEY);
        }

        foreach ($rows as $k => $row) {
            /* @var Ads $ad */
            if ('backend' === $module) {
                $ad = $row->ad;
                $user_ids[] = $ad->user_id;
                $result = $ad->toArray();
                $result['user_remark'] = $row->user_remark;
                if ($row->user_shop_title) {
                    $result['user_icon'] = 'shopping-cart';
                    $result['user_username'] = $row->user_shop_title . ' (' . $row->user_username . ')';
                } else {
                    $result['user_icon'] = $row->user_type === Users::TYPE_COMPANY ? 'globe' : 'user';
                    $result['user_username'] = $row->user_username;
                }

                // format datetimes
                $result['created_at']   = $ad->created_at ? date('d.m.Y. H:i:s', $ad->created_at) : '';
                $result['published_at'] = $ad->published_at ? date('d.m.Y. H:i:s', $ad->published_at) : '';
                $result['modified_at']  = $ad->modified_at ? date('d.m.Y. H:i:s', $ad->modified_at) : '';

                $result['sort_date_formatted'] = $ad->sort_date ? date('d.m.Y. H:i:s', $ad->sort_date) : null;

                // Fill ad's status information
                $ad_status_class = 'active';
                $ad_status_text  = 'Active';

                if (!isset($ad->online_product) || empty($ad->online_product)) {
                    $ad_status_class = 'inactive';
                    $ad_status_text  = 'Draft';
                } else {
                    if ($ad->moderation == Moderation::MODERATION_STATUS_REPORTED) {
                        $ad_status_text  = 'Reported';
                    } elseif ($ad->is_spam == 1) {
                        $ad_status_class = 'spam';
                        $ad_status_text  = 'Spam';
                    } elseif ($ad->active == 0) {
                        $ad_status_class = 'inactive';
                        $ad_status_text  = 'Inactive';
                        if ($ad->isSoftDeleted()) {
                            $ad_status_class = 'deleted';
                            $ad_status_text  = 'Deleted';
                        }
                    }
                }

                $result['status_class'] = $ad_status_class;
                $result['status_text']  = $ad_status_text;

                // Fill moderation information
                $moderation_icon  = 'square-o';
                $moderation_msg   = 'Awaiting moderation';
                $moderation_class = 'text-warning';
                if ($ad->moderation === Moderation::MODERATION_STATUS_OK) {
                    $moderation_icon  = 'check-square-o';
                    $moderation_msg   = 'Moderation OK';
                    $moderation_class = 'text-success';
                } elseif ($ad->moderation === Moderation::MODERATION_STATUS_NOK) {
                    $moderation_icon  = 'minus-square-o';
                    $moderation_msg   = 'Moderation NOK';
                    $moderation_class = 'text-danger';
                } elseif (!isset($ad->online_product) || empty($ad->online_product)) {
                    $moderation_icon  = 'exclamation-triangle';
                    $moderation_msg   = 'Cannot be moderated';
                    $moderation_class = 'text-danger';
                }
                $result['moderation_icon']  = $moderation_icon;
                $result['moderation_msg']   = $moderation_msg;
                $result['moderation_class'] = $moderation_class;
                // has ad ever been reported?
                $result['reported']         = $ad->moderation == 'reported';

            } else {
                if (isset($row->ad)) {
                    $ad = $row->ad;
                } else {
                    $ad = $row;
                }
                $user_ids[] = $ad->user_id;
                $result = $ad->toArray();
                $result['price'] = $ad->price ? $ad->getPrices() : null;

                $result['online_product'] = '';
                if (!empty($ad->online_product)) {
                    $online_product = unserialize($ad->online_product);
                    if ($online_product instanceof ProductsInterface) {
                        $result['online_product'] = $online_product->getBillingTitleWithSelectedOption();
                    }
                }

                $result['offline_product'] = '';
                if (!empty($ad->offline_product) && $ad->is_offline_exportable()) {
                    $offline_product = unserialize($ad->offline_product);
                    if ($offline_product instanceof ProductsInterface) {
                        $result['offline_product'] = $offline_product->getBillingTitleWithSelectedOption();
                    }
                }

                // format datetimes
                $format                      = 'H:i d.m.Y.';
                $result['created_at']        = $ad->created_at ? date($format, $ad->created_at) : '';
                $result['published_at']      = $ad->published_at ? date($format, $ad->published_at) : '';
                $result['modified_at']       = $ad->modified_at ? date($format, $ad->modified_at) : '';
                $result['expires_at']        = $ad->expires_at ? date($format, $ad->expires_at) : '';
                $result['favorited_ad']      = isset($row->favorited_ad) ? $row->favorited_ad : null;
                $result['favorited_ad_note'] = isset($row->favorited_ad_note) ? $row->favorited_ad_note : null;

                $ad_status_class = 'muted';
                $ad_status_text = '';
                $show_link = true;
                $ad_status = $ad->get_status();
                $result['status'] = $ad_status;

                if ($ad_status == 0) {
                    $ad_status_class = 'warning';
                    $ad_status_text = 'U postupku predaje';
                    // TODO/FIXME: newly submitted ads whose Order has been cancelled by the user now also
                    // end up having status=0 -- this might become a new specific status code?
                } elseif ($ad_status == 1) {
                    $ad_status_class = 'success';
                    $ad_status_text = 'Aktivan';
                } elseif ($ad_status == 14) {
                    $ad_status_class = 'success';
                    $ad_status_text = 'Aktivan';    // (�eka moderaciju)
                } elseif ($ad_status == 15) {
                    $ad_status_class = 'success';
                    $ad_status_text = 'Aktivan <span class="text-danger">(�eka uplatu izdvajanja)</span>';
                } elseif ($ad_status == 2) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Istekao';
                } elseif ($ad_status == 20) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'U postupku obnove (�eka uplatu)';
                } elseif ($ad_status == 21) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Istekao (nije mogu�e produljiti)';
                } elseif ($ad_status == 3) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Deaktiviran';
                } elseif ($ad_status == 4) {
                    $ad_status_class = 'danger';
                    $ad_status_text = '�eka moderaciju';
                    $show_link = false;
                } elseif ($ad_status == 41) {
                    $ad_status_class = 'danger';
                    $ad_status_text = '�eka uplatu';
                    $show_link = false;
                } elseif ($ad_status == 5) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Odbijen na moderaciji';
                    $show_link = false;
                } elseif ($ad_status == 6) {
                    $ad_status_class = 'danger';
                    $ad_status_text = 'Odbijen zbog prijave';
                    $show_link = false;
                }

                $result['status_class'] = $ad_status_class;
                $result['status_text']  = $ad_status_text;
                $result['show_link']    = $show_link;

                // process ad options (buttons under the ad)
                $result['links'] = self::get_possible_listing_options($ad, $ad_status, $user);

                // show payment details for ads waiting to be paid.
                if ($unpaid_ad_orders = Orders::findUnpaidOrdersForAd($ad)) {
                    $latest_order = $unpaid_ad_orders[count($unpaid_ad_orders) - 1];
                    if ($latest_order && !$latest_order->isExpired()) {
                        $result['order_id'] = $latest_order->id;
                        $result['barcode_img_src'] = $latest_order->get2DBarcodeInlineSvg();
                        // $result['bank_slip_markup'] = Orders::buildBankSlipMarkup($latest_order, $result['barcode_img_src']);
                        $result['payment_data_table_markup'] = Orders::buildPaymentDataTableMarkup($latest_order, $result['barcode_img_src']);
                        //$result['order_total_formatted'] = Utils::format_money($latest_order->getTotal(), true);
                        //$result['order_pbo'] = $latest_order->getPbo();
                        // $result['barcode_img_src'] = $latest_order->get2DBarcodeInlineImage();
                    }
                }
            }
            // if json_data exist, convert it to json object
            $result['json_data'] = trim($ad->json_data) ? json_decode(trim($ad->json_data)) : null;
            $result['frontend_url'] = $ad->get_frontend_view_link();

            // Decorate the results with extra info about the category it belongs to
            $result['category_node'] = null;
            $result['category_path'] = null;
            $cat = isset($cat_tree[$result['category_id']]) ? $cat_tree[$result['category_id']] : null;
            if ($cat) {
                $result['category_node'] = $cat;
                $result['category_path'] = $cat->path['text'];
            }

            // get short location for current row (if available)
            $rowLocationData = self::getLocationDataUsingOneQuery($ad, $shortLocation = true);
            $result['location'] = null;
            if ($rowLocationData && isset($rowLocationData['text'])) {
                $result['location'] = $rowLocationData['text'];
            }

            // Grab ad ids for later
            $ids[] = $ad->id;
            // Grab media ids also
            if ($result['json_data'] && isset($result['json_data']->ad_media_gallery) && count($result['json_data']->ad_media_gallery)) {
                $result['media_id'] = $result['json_data']->ad_media_gallery[0];
                $media_ids[]        = $result['json_data']->ad_media_gallery[0];
            } else {
                $result['media_id'] = null;
            }
            $result['thumb_pic'] = $no_pic;

            $results[$k] = $result;
        }

        $results = self::enrichResultsWithMedia($results, $media_ids, $thumb_style_details);

        if ('frontend' === $module) {
            $results = self::enrichResultsWithUsers($results, array_unique($user_ids));
        }

        return $results;
    }

    /**
     * Enrich given Resultset with minimal needed info about the ads that should
     * be shown on category/section listings.
     *
     * @param Resultset $rows Resultset we'll be working on
     * @param boolean $combineSpecialProducts Whether special products should be combined in a listing or not
     *
     * @return array
     */
    public static function getEnrichedFrontendBasicResultsArray(Resultset &$rows, $combineSpecialProducts = false)
    {
        $results = array();

        // Bail if the Resultset ain't valid
        if (!$rows->valid()) {
            return $results;
        }

        $media_ids = array();
        $user_ids  = array();
        $results   = array();

        $thumb_style_details = 'GridView';
        $no_pic = (new Media())->getNoImageThumb($thumb_style_details);

        foreach ($rows as $k => $row) {
            if (isset($row->ad)) {
                $ad = $row->ad;
            } else {
                $ad = $row;
            }
            $user_ids[] = $ad->user_id;

            $result = $ad->toArray();
            if ($combineSpecialProducts) {
                switch($row->online_product_id) {
                    case 'platinum':
                    case 'pinky':
                    case 'premium':
                    case 'premium-bez':
                        $result['listing_heading_text'] = 'Izdvojeni oglasi';
                        $result['listing_heading_class'] = ' izdvojeno-' . $row->online_product_id;
                        break;
                    default:
                        $result['listing_heading_text'] = 'Ostali oglasi';
                        $result['listing_heading_class'] = null;
                }
            } else {
                switch($row->online_product_id) {
                    case 'platinum':
                        $result['listing_heading_text'] = 'Platinum oglasi';
                        $result['listing_heading_class'] = ' izdvojeno-' . $row->online_product_id;
                        break;
                    case 'pinky':
                        $result['listing_heading_text'] = 'Pinky oglasi';
                        $result['listing_heading_class'] = ' izdvojeno-' . $row->online_product_id;
                        break;
                    case 'premium':
                    case 'premium-bez':
                        $result['listing_heading_text'] = 'Premium oglasi';
                        $result['listing_heading_class'] = ' izdvojeno-' . $row->online_product_id;
                        break;
                    default:
                        $result['listing_heading_text'] = 'Ostali oglasi';
                        $result['listing_heading_class'] = null;
                }
            }

            $result['price']        = $ad->getPrices();
            $result['sort_date']    = date('d.m.Y.', $result['sort_date']);
            $result['favorited_ad'] = isset($row->favorited_ad) ? $row->favorited_ad : null;
            $result['frontend_url'] = $ad->get_frontend_view_link();

            // get short location for current row (if available)
            $rowLocationData = self::getLocationDataUsingOneQuery($row, $shortLocation = true);
            $result['location'] = null;
            if ($rowLocationData && isset($rowLocationData['text'])) {
                $result['location'] = $rowLocationData['text'];
            }

            // Grab media ids
            $result['json'] = json_decode($result['json_data']);
            if ($result['json'] && isset($result['json']->ad_media_gallery) && count($result['json']->ad_media_gallery)) {
                $result['media_id'] = $result['json']->ad_media_gallery[0];
                $media_ids[]        = $result['media_id'];
            } else {
                $result['media_id'] = null;
            }

            // Grab and set `spinID` key if any of "spinids" exist
            if ($result['json']) {
                if (isset($result['json']->ad_params_spinid)) {
                    $result['spinID'] = true;
                } elseif (isset($result['json']->ad_params_spinidgw)) {
                    $result['spinID'] = true;
                }
            }

            $result['thumb_pic'] = $no_pic;

            $results[$k] = $result;
        }

        $results = self::enrichResultsWithMedia($results, $media_ids, $thumb_style_details);
        $results = self::enrichResultsWithUsers($results, array_unique($user_ids));

        return $results;
    }

    /**
     * @param $ads
     *
     * @return null|string
     * @throws \Exception
     */
/*  TODO: do we need this?
    public static function buildHomepageFeaturedAdsMarkup($ads)
    {
        $markup  = null;
        $escaper = new Escaper();
        $total   = count($ads);

        if ($total) {

            $markup = <<<HTML
<section class="promoted-ads">
    <div class="underline margin-bottom">
        <h1 class="underline">Oglasi dana</h1>
    </div>
    <div class="promoted-ads-carousel">
HTML;

            $slides = array_chunk($ads, 4);

            foreach ($slides as $k => $slide_ads) {
                $markup .= '<div class="ads-page">';

                foreach ($slide_ads as $ad) {
                    $href  = $ad['frontend_url'];
                    $title = $escaper->escapeHtml($ad['title']);
                    $title_attr = $escaper->escapeHtmlAttr($title);

                    $thumb = $ad['thumb_pic'];
                    $thumb->setAlt($title_attr);

                    // $crumbs = $ad->getCategory()->getBreadcrumbs();
                    $crumbs = false;

                    $markup .= '<div class="post-box">';
                    $markup .= '<a href="' . $href . '">' . $thumb->getTag() . '</a>';

                    if (!empty($crumbs)) {
                        $cnt = 0;
                        $total = count($crumbs);
                        $markup .= '<p class="categories">';
                        foreach ($crumbs as $crumb) {
                            $cnt++;
                            if ($cnt > 1) {
                                $markup .= '<a href="' . $crumb->get_url() . '">' . $crumb->name . '</a>';
                                if ($cnt < $total) {
                                    $markup .= ' - ';
                                }
                            }
                        }
                        $markup .= '</p>';
                    }

                    $markup .= '<h3><a href="' . $href . '">' . $title . '</a></h3>';

                    $description = $ad['description_tpl'];
                    if (empty($description)) {
                        $description = $ad['description'];
                    }
                    $description = strip_tags($description);
                    $description = Utils::str_truncate($description, 150);
                    $description = nl2br($description);
                    $markup .= '<p>' . $description . '</p>';

                    $markup .= '</div>';
                }

                $markup .= '</div>';
            }

            $markup .= '</div>';
            $markup .= '</section>';
        }

        return $markup;
    }
*/

    /**
     * Enrich given array of plain PDO results with extra data required to show the results
     * on categories/section listings.
     *
     * @param array $rows Array of results to hydrate
     *
     * @return array
     */
    public static function hydrateAdsResultsArray(array &$rows)
    {
        $results = array();

        // Bail if we don't have anything to work with
        if (empty($rows)) {
            return $results;
        }

        $media_ids = array();
        $user_ids  = array();
        $results   = array();

        $thumb_style_details = 'GridView';
        $no_pic = (new Media())->getNoImageThumb($thumb_style_details);

        $logged_in_user = Di::getDefault()->getShared('auth')->get_user();
        $logged_in_user_id = null;
        if ($logged_in_user) {
            $logged_in_user_id = $logged_in_user->id;
        }

        foreach ($rows as $k => $row) {
            /** @var $row \stdClass */
            $result = array();

            // TODO: Profile this for performance bottlenecks
            if (is_object($row)) {
                $result = get_object_vars($row);
            }
            $user_ids[] = $row->user_id;
            $result['price']        = self::buildPrices($row->currency_id, $row->price);
            $result['sort_date']    = date('d.m.Y.', $row->sort_date);
            $result['frontend_url'] = self::buildFrontendViewLink($row);

            switch ($row->online_product_id) {
                case 'platinum':
                    $result['listing_heading_text'] = 'Platinum oglasi';
                    $result['listing_heading_class'] = ' izdvojeno-' . $row->online_product_id;
                    break;
                case 'pinky':
                    $result['listing_heading_text'] = 'Pinky oglasi';
                    $result['listing_heading_class'] = ' izdvojeno-' . $row->online_product_id;
                    break;
                case 'premium':
                case 'premium-bez':
                    $result['listing_heading_text'] = 'Premium oglasi';
                    $result['listing_heading_class'] = ' izdvojeno-' . $row->online_product_id;
                    break;
                default:
                    $result['listing_heading_text'] = 'Ostali oglasi';
                    $result['listing_heading_class'] = null;
            }

            // Grab media ids
            $result['json'] = json_decode($row->json_data);
            if ($result['json'] && isset($result['json']->ad_media_gallery) && count($result['json']->ad_media_gallery)) {
                $result['media_id'] = $result['json']->ad_media_gallery[0];
                $media_ids[]        = $result['media_id'];
            } else {
                $result['media_id'] = null;
            }

            // Grab and set `spinID` key if any of "spinids" exist
            if ($result['json']) {
                if (isset($result['json']->ad_params_spinid)) {
                    $result['spinID'] = true;
                } elseif (isset($result['json']->ad_params_spinidgw)) {
                    $result['spinID'] = true;
                }
            }

            $result['thumb_pic'] = $no_pic;

            $result['isPushable'] = false;
            /*
            if ($row->user_id == $logged_in_user_id && $row->moderation == Moderation::MODERATION_STATUS_OK) {
                $onlineProduct = unserialize($row->online_product);
                if ($onlineProduct) {
                    $result['isPushable'] = $onlineProduct->hasPushUp();
                }
            }
            */

            $results[$k] = $result;
        }

        $results = self::enrichResultsWithMedia($results, $media_ids, $thumb_style_details);
        $results = self::enrichResultsWithUsers($results, array_unique($user_ids));

        // Process results and set isPushable for each, but without querying a lot
        $results = self::enrichResultsWithIsPushable($results, $logged_in_user);

        return $results;
    }

    public static function fetchViewCountsForAdsList($list)
    {
        $vcm = Di::getDefault()->get('viewCountsManager');

        $row_ids = Utils::array_pluck($list, 'id');
        // $row_ids = Utils::array_pluck($list, 'id');
        $counts = $vcm->getTotalCountMany($row_ids);

        return $counts;
    }

    public static function enrichResultsWithViewCounts(&$results)
    {
        $view_counts = self::fetchViewCountsForAdsList($results);

        foreach ($results as $k => $ad) {
            $ad['view_count'] = 0;
            foreach ($view_counts as $count) {
                if ($count['ad_id'] == $ad['id']) {
                    $ad['view_count'] = $count['views'];
                }
            }
            $results[$k] = $ad;
        }

        return $results;
    }

    /**
     * @param array $list
     * @param \Baseapp\Models\Users|null $user
     *
     * @return array|mixed|Resultset\Complex
     */
    public static function fetchUnpaidPushUpOrdersForAdsList($list, Users $user = null)
    {
        $results = array();

        // Don't bother querying for anon listings, we must have a valid user on frontend listings
        if (null !== $user) {
            $row_ids = Utils::array_pluck($list, 'id');
            $results = Orders::findUnpaidPushUpOrders($user, $row_ids);
        }

        return $results;
    }

    /**
     * @param array $results
     * @param \Baseapp\Models\Users|null $user
     *
     * @return array
     */
    public static function enrichResultsWithIsPushable(&$results, Users $user = null)
    {
        $unpaid_orders_resultset = self::fetchUnpaidPushUpOrdersForAdsList($results, $user);

        foreach ($results as $k => $ad) {

            $ad['isPushable'] = self::isPushable($ad, $user);

            foreach ($unpaid_orders_resultset as $unpaid_resultset) {
                // If we get an ad_id in the resultset of unpaid orders for the current user,
                // that ad should not be considered pushable until the order is paid or cancelled etc.
                // echo Debug::dump($unpaid_resultset);exit;
                if ($unpaid_resultset->oi->ad_id == $ad['id']) {
                    $ad['isPushable'] = false;
                }
            }

            $results[$k] = $ad;
        }

        return $results;
    }


    public function save_remark($remark = null)
    {
        $ads_additional_data = AdsAdditionalData::findFirst(array(
            'conditions' => 'ad_id = :id:',
            'bind'       => array(
                'id' => $this->id
            )
        ));

        if (!$ads_additional_data) {
            $ads_additional_data = new AdsAdditionalData();
            $ads_additional_data->ad_id = $this->id;
            $ads_additional_data->remark = $remark;
            return $ads_additional_data->create();
        } else {
            $ads_additional_data->remark = $remark;
            return $ads_additional_data->update();
        }
    }

    /**
     * @return string
     */
    public function getShortLocationText()
    {
        $text = '';

        $data = static::getLocationDataUsingOneQuery($this, true);
        if (!empty($data) && isset($data['text'])) {
            $text = $data['text'];
        }

        return $text;
    }

    /**
     * @return array|null
     */
    public function getLocationData()
    {
        return static::getLocationDataUsingOneQuery($this);
    }

    /**
     * @param Ads|\stdClass $ad
     * @param bool $shortLocation
     *
     * @return array|null
     */
    public static function getLocationDataUsingOneQuery($ad, $shortLocation = false)
    {
        $data = null;

        // Grab GPS coords from the ad itself
        $coords  = null;
        if ($ad->lat || $ad->lng) {
            $data = array();
            $gps = array(
                (floatval($ad->lat) ? $ad->lat : 0),
                (floatval($ad->lng) ? $ad->lng : 0)
            );
            $coords = implode(',', $gps);
            $data['gps'] = $coords;
        }

        // Grab location ids in order to query for location names in specific order
        $location_ids = array();
        // Add country info only if not "default"
        if ($ad->country_id !== 1 && $ad->country_id) {
            $location_ids[] = $ad->country_id;
        }
        // County, if available
        if ($ad->county_id) {
            $location_ids[] = $ad->county_id;
        }
        // City info, if we have it
        if ($ad->city_id) {
            $location_ids[] = $ad->city_id;
        }
        // Municipality if wanted & available
        if (!$shortLocation && $ad->municipality_id) {
            $location_ids[] = $ad->municipality_id;
        }

        // Fire a single query against the Location model
        $locations = Location::findMultiple($location_ids);
        if (count($locations)) {
            $location_names = array();
            // Going through locations in the order they were specified
            foreach ($location_ids as $location_id) {
                foreach ($locations as $location) {
                    if ($location->id == $location_id) {
                        $location_names[] = $location->name;
                    }
                }
            }
            if (!is_array($data)) {
                $data = array();
            }
            $data['text'] = implode(', ', array_reverse(array_unique($location_names)));
        }

        return $data;
    }

    public function getNearByOnMap($bounds)
    {
        $allowedFields = array(
            'id'           => 1,
            'title'        => 1,
            'description'  => 1,
            'price'        => 1,
            'lat'          => 1,
            'lng'          => 1,
            'frontend_url' => 1,
            'thumb'        => 1
        );

        $ads = array();

        $builder = new Builder();
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->where(
            'ad.active = 1 AND ad.is_spam = 0 AND ad.category_id = :category_id: AND NOT(ad.id = :ad_id:)',
//             'ad.active = 1 AND ad.is_spam = 0 AND NOT(ad.id = :ad_id:)',
            array(
                'ad_id'       => $this->id,
                'category_id' => $this->category_id
            )
        );
        $builder->andWhere(
            'ad.lng IS NOT NULL AND ad.lat IS NOT NULL AND ad.lng <= :east: AND ad.lng >= :west: AND ad.lat <= :north: AND ad.lat >= :south:',
            array(
                'east'  => $bounds->east,
                'west'  => $bounds->west,
                'north' => $bounds->north,
                'south' => $bounds->south
            )
        );
        $builder->groupBy(array('ad.id'));
/*
        $parsed = $builder->getQuery()->parse();
        $dialect = $this->getDI()->get('db')->getDialect();
        $sql = $dialect->select($parsed);

        var_dump($sql);exit;
*/
        $results = $builder->getQuery()->execute();

        if ($results) {
            $rows = self::getEnrichedFrontendBasicResultsArray($results);
            foreach ($rows as $row) {
                $thumb = $row['thumb_pic'];
                $row['thumb'] = $thumb->getSrcOnlyTag();
                unset($row['thumb_pic']);
                unset($thumb);
                $ads[] = array_intersect_key($row, $allowedFields);
            }
        }

        return $ads;
    }
//SELECT [ad].* FROM [Baseapp\Models\Ads] AS [ad] WHERE (ad.active = 1 AND ad.is_spam = 0 AND ad.category_id = :category_id: AND NOT(ad.id = :ad_id:)) AND (ad.lat IS NOT NULL AND ad.lng IS NOT NULL AND ad.lat <= :east: AND ad.lat >= :west: AND ad.lng <= :north: AND ad.lng >= :south:) GROUP BY ad.id

    /**
     * Helper method to get multiple ads records at once
     *
     * @param array|string|null $ids Comma-separated list of ids or an array of ad ids
     *
     * @return null|Ads[]
     */
    public function get_multiple_ids($ids = null)
    {
        $ads = null;

        if ($ids) {
            if (!is_array($ids)) {
                if (strpos($ids, ',') !== false) {
                    $ids = explode(',', $ids);
                } elseif (is_numeric($ids)) {
                    $ids = array((int) $ids);
                }
            }

            if (count($ids)) {
                $queryBuilder = new \Phalcon\Mvc\Model\Query\Builder();
                $ads = $queryBuilder
                   ->addFrom('Baseapp\Models\Ads', 'ad')
                   ->inWhere('ad.id', $ids)
                   ->orderBy('FIELD(ad.id, ' . implode(',', $ids) . ')')
                   ->getQuery()
                   ->execute();
            }
        }

        return $ads;
    }

}
