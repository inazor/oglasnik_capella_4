<?php

namespace Baseapp\Models;
use Baseapp\Library\Debug;

/**
 * UsersFavoriteAds Model
 */
class UsersFavoriteAds extends BaseModelBlamable
{
    /**
     * UsersFavoriteAds initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias' => 'User',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );
        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias' => 'Ad',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );
    }

    /**
     * Returns back the same list of $ids and a boolean along with each $id indicating
     * if the $ad_id is in the list of user's favorites or not
     *
     * @param array $ad_ids
     * @param $user_id
     *
     * @return array
     */
    public static function checkIdsForUser(array $ad_ids, $user_id)
    {
        $json_key = 'favs';

        // By default assume false for all of them
        $list = array();
        foreach ($ad_ids as $ad_id) {
            $list[$json_key][$ad_id] = false;
        }

        // Now actually query them and see if they're really there
        $results = self::query()->inWhere('ad_id', $ad_ids)->andWhere('user_id = :user_id:', array('user_id' => $user_id))->execute();

        $total = count($results);
        if ($total > 0) {
            foreach ($results as $result) {
                $list[$json_key][$result->ad_id] = true;
            }
        }

        return $list;
    }

}
