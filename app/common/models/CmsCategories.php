<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Phalcon\Mvc\Model\Resultset;
use Baseapp\Library\Validations\CmsCategories as CmsCategoriesValidations;
use Baseapp\Library\Behavior\NestedSet as NestedSetBehavior;
use Baseapp\Traits\FormModelControllerTrait;
use Baseapp\Traits\NestedSetActions;
use Baseapp\Traits\MemcachedMethods;
use Baseapp\Models\CmsCategoriesSettings as CmsCategorySettings;

/**
 * Cms categories Model
 */
class CmsCategories extends BaseModelBlamable
{
    use FormModelControllerTrait;
    use NestedSetActions;
    use MemcachedMethods;

    const MEMCACHED_KEY       = 'cmsCategoryTree';

    const TYPE_MAGAZINE       = 'magazine';
    const TYPE_INFO           = 'info';

    const TEMPLATE_INHERIT    = 'inherit';
    const TEMPLATE_DEFAULT    = 'default';
    const TEMPLATE_PRICE_LIST = 'price-list';

    /**
     * Categories initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->addBehavior(new NestedSetBehavior(array(
            'hasManyRoots'    => false,
            'rootAttribute'   => 'root_id',
            'rightAttribute'  => 'rght',
            'parentAttribute' => 'parent_id',
        )));

        /**
         * Every category can have many articles
         */
        $this->hasMany('id', __NAMESPACE__ . '\CmsArticles', 'category_id',
            array(
                'alias'    => 'Articles',
                'reusable' => true
            )
        );

        /**
         * Every category has one CmsCategoriesSettings
         */
        $this->hasOne('id', __NAMESPACE__ . '\CmsCategoriesSettings', 'category_id',
            array(
                'alias'    => 'Settings',
                'reusable' => true
            )
        );
    }

    public static function getTypes()
    {
        return array(
            self::TYPE_MAGAZINE => 'Magazine',
            self::TYPE_INFO     => 'Info'
        );
    }

    public static function getTemplates($withInherit = false)
    {
        $templates = array();

        if ($withInherit) {
            $templates[self::TEMPLATE_INHERIT] = 'Inherit template';
        }

        $templates[self::TEMPLATE_DEFAULT]    = 'Default';
        $templates[self::TEMPLATE_PRICE_LIST] = 'Price list';

        return $templates;
    }

    protected function getCachableData()
    {
        return $this->getTree();
    }

    public function getTemplateName()
    {
        return isset($this->template) && $this->template ? $this->template : self::TEMPLATE_DEFAULT;
    }

    public function getTemplatePath()
    {
        return 'templates' . DIRECTORY_SEPARATOR . ($this->template ? $this->template : self::TEMPLATE_DEFAULT) . DIRECTORY_SEPARATOR . 'index';
    }

    public function isFullPageWith()
    {
        switch($this->template) {
            case self::TEMPLATE_PRICE_LIST:
                $isFullPageWith = true;
                break;

            default:
                $isFullPageWith = false;
        }

        return $isFullPageWith;
    }

    public function getVirtualRootByTemplate()
    {
        $template = $this->template;

        // if current node has a template set, then consider it as a root
        if (null !== $template) {
            return $this;
        }

        $cmsTree = $this->getDI()->get(self::MEMCACHED_KEY);
        unset($cmsTree[0]);
        unset($cmsTree[1]);
        $lastParentID = $this->parent_id;
        while (null === $template) {
            $parentCategory = isset($cmsTree[$lastParentID]) ? $cmsTree[$lastParentID] : null;
            if (null === $parentCategory) {
                break;
            }
            if (isset($parentCategory->template) && trim($parentCategory->template)) {
                $template = $parentCategory->template;
            } else {
                $lastParentID = $parentCategory->parent_id;
            }
        }
        unset($cmsTree);

        if ($template) {
            return self::findFirst($lastParentID);
        }

        return null;
    }

    public function getDirectChildren()
    {
        $children = array();
        $childrenIDs = array();

        $cmsTree = $this->getDI()->get(self::MEMCACHED_KEY);
        unset($cmsTree[0]);
        unset($cmsTree[1]);
        if (isset($cmsTree[$this->id]) && isset($cmsTree[$this->id]->children)) {
            foreach ($cmsTree[$this->id]->children as $child) {
                if ($child->active === 1) {
                    $childrenIDs[] = $child->id;
                }
            }
        }
        unset($cmsTree);

        if (count($childrenIDs)) {
            $children = self::find(array(
                'id IN ({childrenIDs:array})',
                'bind' => array(
                    'childrenIDs' => $childrenIDs
                ),
                'order' => 'lft ASC'
            ));
        }
        unset($childrenIDs);

        return $children;
    }

    public function getCategoryArticles()
    {
        $articles = array();

        $tmp = $this->getArticles(array('conditions' => 'active = 1', 'order' => 'sort_order ASC'));
        if ($tmp && count($tmp)) {
            $articles = $tmp;
        }
        unset($tmp);

        return $articles;
    }

    /**
     * Helper method to populate models data with POST values
     * @param \Phalcon\Http\RequestInterface $request
     */
    protected function populate_model_with_POST($request)
    {
        $this->parent_id = (int) $request->getPost('parent_id');
        $this->name      = (string) $request->getPost('name');
        $this->url       = (string) $request->getPost('url');
        $this->excerpt   = trim(strip_tags($request->getPost('excerpt'))) ? trim(strip_tags($request->getPost('excerpt'))) : null;
        $this->type      = (string) $request->getPost('type');
        $this->template  = trim($request->getPost('template')) !== self::TEMPLATE_INHERIT ? (string) trim($request->getPost('template')) : null;
        $this->active    = $request->hasPost('active') ? 1 : 0;

        $category_settings = $this->Settings;
        if (!$category_settings) {
            $category_settings = new CmsCategorySettings();
        }
        $category_settings->meta_title       = trim(strip_tags($request->getPost('meta_title'))) ? trim(strip_tags($request->getPost('meta_title'))) : null;
        $category_settings->meta_description = trim(strip_tags($request->getPost('meta_description'))) ? trim(strip_tags($request->getPost('meta_description'))) : null;
        $category_settings->meta_keywords    = trim(strip_tags($request->getPost('meta_keywords'))) ? trim(strip_tags($request->getPost('meta_keywords'))) : null;
        $category_settings->zendesk          = $request->hasPost('zendesk') ? 1 : 0;;

        $this->Settings = $category_settings;
    }

    /**
     * Create a Category method
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param \Baseapp\Models\CmsCategories $parent
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request, $parent)
    {
        $this->populate_model_with_POST($request);

        $validation = new CmsCategoriesValidations();
        $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
            'model' => '\Baseapp\Models\CmsCategories',
            'extra_conditions' => array(
                'lft > :parent_left: AND rght < :parent_right: AND level = :level:',
                array(
                    'parent_left' => $parent->lft,
                    'parent_right' => $parent->rght,
                    'level' => $parent->level + 1
                )
            ),
            'message' => 'Category name should be unique within its parent',
        )));

        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->appendTo($parent)) {
                $this->deleteMemcachedData();
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save Category data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param \Baseapp\Models\CmsCategories $parent
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request, $parent)
    {
        $validation = new CmsCategoriesValidations();
        if ($this->name != $request->getPost('name')) {
            $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
                'model' => '\Baseapp\Models\CmsCategories',
                'extra_conditions' => array(
                    'lft > :parent_left: AND rght < :parent_right: AND level = :level:',
                    array(
                        'parent_left' => $parent->lft,
                        'parent_right' => $parent->rght,
                        'level' => $parent->level + 1
                    )
                ),
                'message' => 'Category name should be unique within its parent',
            )));
        }
        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('url');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }
        $validation->set_unique($require_unique);

        $messages = $validation->validate($request->getPost());

        $this->populate_model_with_POST($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            // if item was moved to another parent then $parent->id and $this->parent()->id differ!
            if ((int) $this->parent()->id != (int) $parent->id) {
                $save_result = $this->moveAsLast($parent);
            } else {
                $save_result = $this->saveNode();
            }
            if (true === $save_result) {
                $this->deleteMemcachedData();
                return $this;
            } else {
                Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Active/Inactive category, depending on the previous state
     *
     * @return bool Whether the update was successful or not
     */
    public function activeToggle()
    {
        $this->disable_notnull_validations();
        $this->active = (int) !$this->active;
        $result = $this->saveNode();
        $this->enable_notnull_validations();
        return $result;
    }

    /**
     * Get all roots from the tree
     *
     * As this is actually a single root tree, this method returns direct descendants of the real 'root' node
     *
     * @return \Baseapp\Models\Categories
     */
    public static function getRoots() {
        if ($available_categories = self::findFirst('lft = 1')) {
            return $available_categories->children();
        }

        return null;
    }

    /**
     * Get current category's path
     * @param  string  $separator Separator string between subcategories
     * @param  boolean $root_node Include root node or not
     * @return string
     */
    public function getPath($separator = ' › ', $root_node = false) {
        /**
         * Checking if the first argument is being used as boolean, meaning
         * we're supposed to treat $separator as default value.
         */
        if (is_bool($separator)) {
            $root_node = $separator;
            $separator = ' › ';
        } elseif ('' == trim($separator)) {
            $separator = ' › ';
        }

        $category_path = array();
        if ($category_ancestors = $this->getNodePath()) {
            foreach ($category_ancestors as $category_ancestor) {
                $category_path[] = $category_ancestor->name;
            }
        }

        if (false == $root_node && isset($category_path[0])) {
            unset($category_path[0]);
        }

        $category_path = implode($separator, $category_path);

        return $category_path;
    }

    /**
     * Returns either the complete HTML markup for the category's frontend view
     * or just the URL (which is then used as the "href").
     *
     * Used primarily to keep the link format/definition in a single place since
     * we've started providing better "Article updated/created" success messages
     * in the backend too. Having a link within the message makes it
     * a lot easier to see the frontend version in a new tab/window).
     *
     * @param string $type Optional. Defaults to 'href' (returning just the URL).
     *                     If specified as 'html' returns the complete <a href...> markup.
     *
     * @return string
     */
    public function get_frontend_view_link($type = 'href')
    {
        $link = null;

        if (isset($this->url)) {
            $url  = '/novosti/' . $this->url;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get('novosti/' . $this->url);
            }

            $link = $url;

            if ('html' === $type) {
                $markup = '<a href="%s" target="_blank"><i class="fa fa-fw fa-eye"></i>%s</a>';
                $link   = sprintf($markup, $url, 'View article');
            }
        }

        return $link;
    }

    public function getBreadcrumbs()
    {
        $breadcrumbs = null;

        if ($categoryBreadcrumbs = $this->getNodePath()) {
            $breadcrumbs = array();
            $currBc = 0;

            foreach ($categoryBreadcrumbs as $bc) {
                if ($bc->parent_id !== 1) {
                    $breadcrumbs[] = array(
                        'name'         => (0 === $currBc ? 'Novosti' : $bc->name),
                        'frontend_url' => $this->getDI()->get('url')->get('novosti/' . $bc->url)
                    );
                    $currBc++;
                }
            }
            if (empty($breadcrumbs)) {
                $breadcrumbs = null;
            }
        }

        return $breadcrumbs;
    }

}
