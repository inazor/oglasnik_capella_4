<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Baseapp\Library\Utils;
use Phalcon\Mvc\Model\ResultsetInterface;

class UsersShopsFeaturedHomepage extends BaseModelBlamable
{
    const DEFAULT_DURATION_IN_DAYS = 14;

    public $id;
    public $users_shops_featured_id;
    public $expires_at;
    public $active;
    public $created_at;
    public $modified_at;

    /**
     * UsersShopsFeaturedHomepage initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'users_shops_featured_id', __NAMESPACE__ . '\UsersShopsFeatured', 'id',
            array(
                'alias'      => 'FeaturedShop',
                'reusable'   => true,
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE,
                )
            )
        );
    }

    public function getSource()
    {
        return 'users_shops_featured_homepage';
    }

    public function beforeCreate()
    {
        $this->created_at = Utils::getRequestTime();
    }

    public function beforeUpdate()
    {
        $this->modified_at = Utils::getRequestTime();
    }

    public function markExpired()
    {
        $sql = "UPDATE `" . $this->getSource() . "` SET `active` = 0 WHERE `active` = 1 AND `expires_at` < :current_timestamp";

        $conn = $this->getWriteConnection();
        $conn->execute($sql, array(':current_timestamp' => Utils::getRequestTime()));

        return $conn->affectedRows();
    }

    public static function insertOrUpdate(UsersShopsFeatured $featuredShop)
    {
        $now = Utils::getRequestTime();

        $record = self::findFirst('users_shops_featured_id = ' . $featuredShop->id);
        if (!$record) {
            $record = new self();
            $record->users_shops_featured_id = $featuredShop->id;
        }

        $record->active     = 1;
        $expires_at         = $now + (86400 * self::DEFAULT_DURATION_IN_DAYS); // now + x days worth of seconds
        $record->expires_at = $expires_at;

        $saved = $record->save();

        if (!$saved) {
            Bootstrap::log($record->getMessages());
        }

        return $saved;
    }

    public static function deleteByUsersShopsFeaturedId($id)
    {
        if ($users_shops_featured_homepage = self::findFirst('users_shops_featured_id = ' . $id)) {
            return $users_shops_featured_homepage->delete();
        }

        return false;
    }

    public static function setActiveForUsersShopsFeaturedId($id, $value)
    {
        $ids = array();

        if (is_array($id)) {
            $ids = $id;
        } elseif (strpos($id, ',') !== false) {
            $ids = explode(',', $id);
        } elseif ((int) $id) {
            $ids[] = (int) $id;
        }

        if (count($ids)) {
            if (count($ids) == 1) {
                $users_shops_featured_homepage = self::findFirst(array(
                    'conditions' => 'users_shops_featured_id = :id:',
                    'bind'       => array(
                        'id' => $ids[0]
                    )
                ));
            } else {
                $users_shops_featured_homepage = self::find(array(
                    'conditions' => 'users_shops_featured_id IN ({id:array})',
                    'bind'       => array(
                        'id' => $ids
                    )
                ));
            }

            $value = (int) $value;

            if ($users_shops_featured_homepage) {
                return $users_shops_featured_homepage->update(array('active' => $value));
            }
        }

        return false;
    }

    public static function findActiveAndNonExpired()
    {
        $now = Utils::getRequestTime();

        return self::find('active = 1 AND expires_at > ' . $now);
    }

    /**
     * @param ResultsetInterface $resultset
     *
     * @return array
     */
    public static function getUsersShopsFeaturesIdsArray(ResultsetInterface $resultset)
    {
        $ids = array();

        foreach ($resultset as $result) {
            $ids[] = $result->users_shops_featured_id;
        }

        return $ids;
    }

    /**
     * @return array|mixed|null
     */
    public static function getUsersShopsFeaturedIds()
    {
        $resultset = self::findActiveAndNonExpired();
        $ids       = self::getUsersShopsFeaturesIdsArray($resultset);

        if (!empty($ids)) {
            return $ids;
        }

        return null;
    }

    public static function getRandomlyOrderedUsersShopsFeaturedIds()
    {
        $users_shops_featured = self::getUsersShopsFeaturedIds();

        if (!empty($users_shops_featured) && is_array($users_shops_featured)) {
            shuffle($users_shops_featured);
        }

        return $users_shops_featured;
    }

}
