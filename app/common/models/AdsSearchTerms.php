<?php

namespace Baseapp\Models;

/**
 * AdsSearchTerms Model
 */
class AdsSearchTerms extends BaseModelBlamable
{
    /**
     * AdsSearchTerms initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias'      => 'Ad',
                'reusable'   => true,
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE,
                )
            )
        );
    }

    public function getSource()
    {
        return 'ads_search_terms';
    }

    public static function getByPrimaryKeyOrCreateNew($ad_id = null)
    {
        $ads_search_term = null;

        if ($ad_id) {
            $ads_search_term = self::findFirst(
                array(
                    'conditions' => 'ad_id = :ad_id:',
                    'bind'       => array(
                        'ad_id' => $ad_id,
                    )
                )
            );
        }

        if (!$ads_search_term) {
            $ads_search_term = new self();
            $ads_search_term->ad_id = $ad_id;
            $ads_search_term->search_data = null;
        }

        return $ads_search_term;
    }
}
