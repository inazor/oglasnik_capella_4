<?php

namespace Baseapp\Models;

//use Baseapp\Library\Validations\Sections as SectionsValidations;
//use Baseapp\Traits\FormModelControllerTrait;
use Phalcon\Mvc\Model\Relation;

/**
 * Sections Model
 */
class TransactionTypes extends BaseModel
{

    /**
     * Sections initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('id', __NAMESPACE__ . '\Categories', 'transaction_type_id', array(
            'alias' => 'Categories',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE
            )
        ));

    }

}
