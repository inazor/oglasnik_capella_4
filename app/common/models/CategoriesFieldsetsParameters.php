<?php

namespace Baseapp\Models;

/**
 * CategoriesFieldsetsParameters Model
 */
class CategoriesFieldsetsParameters extends BaseModelBlamable
{
    /**
     * CategoriesFieldsetsParameters initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'category_fieldset_id', __NAMESPACE__ . '\CategoriesFieldsets', 'id',
            array(
                'alias' => 'Fieldset',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );

        $this->belongsTo(
            'parameter_id', __NAMESPACE__ . '\Parameters', 'id',
            array(
                'alias' => 'Parameter',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );

        $this->belongsTo(
            'currency_id', __NAMESPACE__ . '\Currency', 'id',
            array(
                'alias' => 'Currency',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );
    }

    /**
     * Get current parameter's JSON encoded attributes
     * @return json
     */
    public function getJSONattributes()
    {
        $parameter_attributes = null;

        $dbParameter = Parameters::getCached($this->parameter_id);

        if (trim($this->levels)) {
            $parameter_attributes = array(
                'id' => (int) $this->parameter_id,
                'type_id' => $dbParameter->type_id,
                'price' => floatval($this->price) ? (float) $this->price : null,
                'price_currency_id' => intval($this->price_currency_id) ? (int) $this->price_currency_id : null,
                'levels' => trim($this->levels) ? json_decode($this->levels, true) : null
            );
        } elseif (trim($this->data)) {
            $parameter_data = json_decode($this->data);

            if ($parameter_data) {
                if ('UPLOADABLE' == $dbParameter->type_id) {
                    $parameter_attributes = array(
                        'id'                    => (int) $this->parameter_id,
                        'type_id'               => $dbParameter->type_id,
                        'price'                 => floatval($this->price) ? (float) $this->price : null,
                        'price_currency_id'     => intval($this->price_currency_id) ? (int) $this->price_currency_id : null,
                        'label_text'            => trim($parameter_data->label_text) ? (string) $parameter_data->label_text : null,
                        'button_text'           => trim($parameter_data->button_text) ? (string) $parameter_data->button_text : null,
                        'max_items'             => trim($parameter_data->max_items) ? (int) $parameter_data->max_items : null,
                        'help_text'             => trim($parameter_data->help_text) ? (string) $parameter_data->help_text : null,
                        'is_searchable'         => isset($parameter_data->is_searchable) ? (int) $parameter_data->is_searchable : 0,
                        'is_searchable_options' => isset($parameter_data->is_searchable_options) ? $parameter_data->is_searchable_options : null,
                        'is_required'           => (int) $parameter_data->is_required
                    );
                } elseif ('PICTURE' == $dbParameter->type_id) {
                    $parameter_attributes = array(
                        'id' => (int) $this->parameter_id,
                        'type_id' => $dbParameter->type_id,
                        'price' => floatval($this->price) ? (float) $this->price : null,
                        'price_currency_id' => intval($this->price_currency_id) ? (int) $this->price_currency_id : null,
                        'label_text' => trim($parameter_data->label_text) ? (string) $parameter_data->label_text : null,
                        'button_text' => trim($parameter_data->button_text) ? (string) $parameter_data->button_text : null,
                        'max_pictures' => trim($parameter_data->max_pictures) ? (int) $parameter_data->max_pictures : null,
                        'help_text' => trim($parameter_data->help_text) ? (string) $parameter_data->help_text : null,
                        'is_searchable' => isset($parameter_data->is_searchable) ? (int) $parameter_data->is_searchable : 0,
                        'is_searchable_options' => isset($parameter_data->is_searchable_options) ? $parameter_data->is_searchable_options : null,
                        'is_required' => (int) $parameter_data->is_required
                    );
                } elseif ('MONTHYEAR' == $dbParameter->type_id || 'YEAR' == $dbParameter->type_id) {
                    $parameter_attributes = array(
                        'id' => (int) $this->parameter_id,
                        'type_id' => $dbParameter->type_id,
                        'price' => floatval($this->price) ? (float) $this->price : null,
                        'price_currency_id' => intval($this->price_currency_id) ? (int) $this->price_currency_id : null,
                        'label_text' => trim($parameter_data->label_text) ? (string) $parameter_data->label_text : null,
                        'placeholder_text' => trim($parameter_data->placeholder_text) ? (string) $parameter_data->placeholder_text : null,
                        'help_text' => trim($parameter_data->help_text) ? (string) $parameter_data->help_text : null,
                        'interval_begin_value' => trim($parameter_data->interval_begin_value) ? (int) $parameter_data->interval_begin_value : 0,
                        'interval_begin_period' => trim($parameter_data->interval_begin_period) ? (string) $parameter_data->interval_begin_period : 'Year',
                        'interval_end_value' => trim($parameter_data->interval_end_value) ? (int) $parameter_data->interval_end_value : 0,
                        'interval_end_period' => trim($parameter_data->interval_end_period) ? (string) $parameter_data->interval_end_period : 'Year',
                        'is_reverse' => (int) $parameter_data->is_reverse,
                        'default_value' => trim($parameter_data->default_value) ? (is_numeric($parameter_data->default_value) ? (int) $parameter_data->default_value : (string) $parameter_data->default_value) : null,
                        'is_searchable' => isset($parameter_data->is_searchable) ? (int) $parameter_data->is_searchable : 0,
                        'is_searchable_options' => isset($parameter_data->is_searchable_options) ? $parameter_data->is_searchable_options : null,
                        'is_required' => (int) $parameter_data->is_required,
                        'is_hidden' => (int) $parameter_data->is_hidden
                    );
                } else {
                    if ('CURRENCY' == $dbParameter->type_id) {
                        if ($parameter_data->currency_id) {
                            $currency_id = (int) $parameter_data->currency_id;
                        } else {
                            $currency_id = 'user';
                        }
                    } else {
                        $currency_id = null;
                    }

                    $parameter_attributes = array(
                        'id' => (int) $this->parameter_id,
                        'type_id' => $dbParameter->type_id,
                        'price' => floatval($this->price) ? (float) $this->price : null,
                        'price_currency_id' => intval($this->price_currency_id) ? (int) $this->price_currency_id : null,
                        'label_text' => trim($parameter_data->label_text) ? (string) $parameter_data->label_text : null,
                        'placeholder_text' => trim($parameter_data->placeholder_text) ? (string) $parameter_data->placeholder_text : null,
                        'default_value' => trim($parameter_data->default_value) ? (is_numeric($parameter_data->default_value) ? (int) $parameter_data->default_value : (string) $parameter_data->default_value) : null,
                        'other_text' => trim($parameter_data->other_text) ? (string) $parameter_data->other_text : null,
                        'other_label_text' => trim($parameter_data->other_label_text) ? (string) $parameter_data->other_label_text : null,
                        'other_placeholder_text' => trim($parameter_data->other_placeholder_text) ? (string) $parameter_data->other_placeholder_text : null,
                        'help_text' => trim($parameter_data->help_text) ? (string) $parameter_data->help_text : null,
                        'currency_id' => $currency_id,
                        'prefix' => trim($parameter_data->prefix) ? (string) $parameter_data->prefix : null,
                        'suffix' => trim($parameter_data->suffix) ? (string) $parameter_data->suffix : null,
                        'is_searchable' => isset($parameter_data->is_searchable) ? (int) $parameter_data->is_searchable : 0,
                        'is_searchable_options' => isset($parameter_data->is_searchable_options) ? $parameter_data->is_searchable_options : null,
                        'is_required' => (int) $parameter_data->is_required,
                        'is_hidden' => (int) $parameter_data->is_hidden
                    );

                    if ('NUMBER' == $dbParameter->type_id) {
                        if (isset($parameter_data->number_decimals)) {
                            $parameter_attributes['number_decimals'] = intval($parameter_data->number_decimals);
                        }
                        if (isset($parameter_data->number_min_value)) {
                            $parameter_attributes['number_min_value'] = intval($parameter_data->number_min_value);
                        }
                        if (isset($parameter_data->number_max_value)) {
                            $parameter_attributes['number_max_value'] = intval($parameter_data->number_max_value);
                        }
                    }

                    if ('TITLE' == $dbParameter->type_id) {
                        $parameter_attributes['max_length'] = isset($parameter_data->max_length) ? intval($parameter_data->max_length) : 80;
                    }

                }
            }
        }

        if ($parameter_attributes) {
            return json_encode($parameter_attributes);
        }

        return null;
    }

}
