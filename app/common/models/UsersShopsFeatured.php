<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Baseapp\Library\Utils;
use Baseapp\Library\Validations\UsersShopsFeaturedFrontend as FeaturedShopsValidationsFrontend;
use Baseapp\Library\Validations\UsersShopsFeaturedBackend as FeaturedShopsValidationsBackend;
use Phalcon\Escaper;
use Phalcon\Cache\Exception as PhCacheException;

/**
 * UsersShopsFeatured Model
 */
class UsersShopsFeatured extends BaseModelBlamable
{
    const MEMCACHED_KEY = 'users_shops_featured';

    const TYPE_ADS      = 1;
    const TYPE_INTRO    = 2;

    protected $fields_types_map = array(
        'int'     => array(
            'shop_id',
            'type',
        ),
        'boolint' => array(
            'active',
            'deleted'
        ),
        'dateint' => array(
            'published_at',
            'expires_at'
        ),
        'string'  => array(
            'categories',
            'ads',
            'intro'
        )
    );

    public $type = self::TYPE_ADS; // default type
    public $created_at  = null;
    public $modified_at = null;
    public $active      = 1;
    public $deleted     = 0;

    /**
     * UsersShopsFeatured initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'shop_id', __NAMESPACE__ . '\UsersShops', 'id',
            array(
                'alias' => 'Shop',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );

        $this->hasOne(
            'id', __NAMESPACE__ . '\UsersShopsFeaturedHomepage', 'users_shops_featured_id',
            array(
                'alias'    => 'HomepageFeatured',
                'reusable' => true
            )
        );
    }

    public function beforeCreate()
    {
        $this->created_at = Utils::getRequestTime();
    }

    public function beforeUpdate()
    {
        $this->modified_at = Utils::getRequestTime();
    }

    public function get_categories_from_request($request)
    {
        // fill categories
        $categories_array = array();
        if ($main_category_id = $request->getPost('main_category_id', 'int', null)) {
            $categories_array[] = (int) $main_category_id;
        }
        if ($additional_category_ids = $request->getPost('additional_category_ids')) {
            if (is_array($additional_category_ids) && count($additional_category_ids) <= 2) {
                foreach ($additional_category_ids as $cat_id) {
                    $cat_id = (int) $cat_id;
                    if ($cat_id) {
                        $categories_array[] = $cat_id;
                    }
                }
            }
        }

        return count($categories_array) ? implode(',', $categories_array) : null;
    }

    /**
     * Add specific validations based on chosen fetured shop's type
     */
    protected function add_featured_shop_type_validations($request, &$validation)
    {
        if ($request->getPost('type') == self::TYPE_ADS) {
            $validation->validate_if_set('ads');
        } else {
            $validation->validate_if_set('intro');
        }
    }

    /**
     * Creates a new shopping window
     *
     * @param string $module
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    protected function add_new($module, $request)
    {
        $this->categories = $this->get_categories_from_request($request);
        $_POST['categories'] = $this->categories;

        if ('frontend' === $module) {
            $validation = new FeaturedShopsValidationsFrontend();
        } elseif ('backend' === $module) {
            $validation = new FeaturedShopsValidationsBackend();
        }
        // $this->add_featured_shop_type_validations($request, $validation);
        $messages = $validation->validate($request->getPost());

        $this->initialize_model_with_post($request);

        // WARNING: windows created from the frontend are active by default regardless of what happens earlier
        if ('frontend' === $module) {
            $this->active = 1;
        }

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save an existing shop
     *
     * @param string $module
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    protected function save_changes($module, $request)
    {
        if ('frontend' === $module) {
            $validation = new FeaturedShopsValidationsFrontend();
        } elseif ('backend' === $module) {
            $this->categories = $this->get_categories_from_request($request);
            $validation = new FeaturedShopsValidationsBackend();
        }
        $_POST['categories'] = $this->categories;

        if ('frontend' === $module) {
            $_POST['active']  = $this->active;
        }

        // $this->add_featured_shop_type_validations($request, $validation);
        $messages = $validation->validate($request->getPost());

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->save()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Creates a new shop from the frontend module
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function frontend_add_new($request)
    {
        return $this->add_new('frontend', $request);
    }

    /**
     * Saves data for an existing shop from frontend
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function frontend_save_changes($request)
    {
        return $this->save_changes('frontend', $request);
    }

    /**
     * Creates a new shop from the backend module
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_add_new($request)
    {
        return $this->add_new('backend', $request);
    }

    /**
     * Saves data for an existing shop from backend
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function backend_save_changes($request)
    {
        return $this->save_changes('backend', $request);
    }

    public function is_featured_on_homepage()
    {
        return $this->HomepageFeatured;
    }

    /**
     * Returns markup for action links for each shopping window according to it's current status
     *
     * @param array $status_array The stuff returned from `getStatusArray()`
     *
     * @return null|string
     */
    public function getActionLinksMarkup($status_array = null)
    {
        if (null === $status_array) {
            $status_array = $this->getStatusArray();
        }

        // If awaiting payment and has outstanding orders, make the link to payment
        $order_id = null;
        if (1 == $status_array['code']) {
            $order = $this->getFirstOutstandingOrder();
            if ($order) {
                $order_id = $order->id;
            }
        }

        $id = $this->id;

        $all_links = array(
            'edit' => array(
                'href'  => 'moj-kutak/trgovina/izlog/uredi/' . $id,
                'title' => 'Uredi',
                'attr'  => ''
            ),
            'deactivate' => array(
                'href'  => 'izlozi/deaktiviraj/' . $id,
                'title' => 'Deaktiviraj',
                'attr'  => ''
            ),
            'activate' => array(
                'href' => 'izlozi/aktiviraj/' . $id,
                'title' => 'Aktiviraj',
                'attr'  => ''
            ),
            'payment' => array(
                // 'href'  => 'izlozi/payment/' . $order_id,
                'href'  => 'izlozi/payment/' . $order_id,
                'title' => 'Plati',
                'attr'  => ''
            ),
            'delete' => array(
                'href'  => 'izlozi/obrisi/' . $id,
                'title' => 'Obriši',
                'attr'  => 'class="delete-btn delete-window" data-toggle="modal" data-target="#delete-shopping-window-confirm"'
            )
        );

        // Remove payment link if we haven't found an order (for now at least)
        if (null === $order_id) {
            unset($all_links['payment']);
        }

        $wanted_links_keys = isset($status_array['links']) ? $status_array['links'] : array();

        // shove 'edit' and 'delete' links in there if they're not explicitly set already
        if (!empty($wanted_links_keys)) {
            if (!in_array('edit', $wanted_links_keys)) {
                array_unshift($wanted_links_keys, 'edit');
            }
            if (!in_array('delete', $wanted_links_keys)) {
                array_push($wanted_links_keys, 'delete');
            }
        }

        $markup = null;
        if (!empty($wanted_links_keys)) {
            $markup  = '<div class="actions margin-top-sm width-full">' . PHP_EOL;
            $markup .= '    <hr/>' . PHP_EOL;
            foreach ($wanted_links_keys as $link_key) {
                if (!empty($all_links[$link_key])) {
                    $link = $all_links[$link_key];
                    $href = $this->getDI()->get('url')->get($link['href']);

                    if ('delete' === $link_key) {
                        $markup .= '    <button data-formurl="' . $href . '"' . ($link['attr'] ? ' ' . $link['attr'] : '') . '>' . $link['title'] . '</button>' . PHP_EOL;
                    } else {
                        $markup .= '    <button data-href="' . $href . '"' . ($link['attr'] ? ' ' . $link['attr'] : '') . '>' . $link['title'] . '</button>' . PHP_EOL;
                    }
                }
            }
            $markup .= '</div>' . PHP_EOL;
        }

        return $markup;
    }

    /**
     * @return array
     */
    public function getStatusArray()
    {
        // 1. ceka placanje - nema oba datuma (i unpaid order?)
        // - gumbi su: uredi, 'plati', obrisi

        // 2. istekao
        // - ako je expires_at u proslosti, gumbi: uredi, obrisi

        // 3. aktivan
        // - aktivan je ako ima oba datuma i ako su datumi "valid"
        // - gumbi su uredi, deaktiviraj, obrisi

        // 4. deaktiviran
        // - ima oba datuma i datumi su valid, a active = 0
        // gumbi: uredi, aktiviraj, obrisi

        $has_both_dates = $this->hasBothDatesSet();
        $published      = $this->isPublished();
        $expired        = $this->hasExpired();

        if (!$has_both_dates) {
            $status_text  = 'Čeka plaćanje';
            $status_class = 'text-danger';
            $status_code  = 1;
            $links        = array('edit', 'payment');
        } else {
            if ($published && !$expired) {
                $status_text  = 'Aktivan';
                $status_class = 'text-success';
                $status_code  = 3;
                $links        = array('edit', 'deactivate');

                if (!$this->active) {
                    $status_text  = 'Deaktiviran';
                    $status_class = 'text-info';
                    $status_code  = 4;
                    $links        = array('edit', 'activate');
                }
            }

            if ($expired) {
                $status_text  = 'Istekao';
                $status_class = 'text-info';
                $status_code  = 2;
                $links        = array('edit');
            }
        }

        $status = array(
            'text'    => $status_text,
            'class'   => $status_class,
            'links'   => $links,
            'code'    => $status_code
        );

        return $status;
    }

    /**
     * @return bool
     */
    public function hasBothDatesSet()
    {
        return ($this->published_at && $this->expires_at);
    }

    /**
     * @return bool
     */
    public function isActiveViaDates()
    {
        return ($this->isPublished() && !$this->hasExpired());
    }

    public function hasExpired()
    {
        $expired = false;

        if ($this->expires_at) {
            $now     = Utils::getRequestTime();
            $expired = ($now > $this->expires_at);
        }

        return $expired;
    }

    public function isPublished()
    {
        $published = false;

        if ($this->published_at) {
            $now = Utils::getRequestTime();
            $published = ($now >= $this->published_at);
        }

        return $published;
    }

    public function getTimeframe($format = 'd.m.Y H:i')
    {
        $timeframe = '';

        if ($this->published_at && $this->expires_at) {
            $timeframe = date($format, $this->published_at) . ' - ' . date($format, $this->expires_at);
        }

        return $timeframe;
    }

    public function getCategoriesDisplayData()
    {
        $data = null;

        $categories = $this->getCategoriesArray();
        if (!empty($categories)) {
            // Only go grab the tree once for repeated calls
            static $cat_tree = null;
            if (null === $cat_tree) {
                $cat_tree = $this->getDI()->get(Categories::MEMCACHED_KEY);
            }

            $data = array(
                'ids' => $categories
            );
            $data['markup'] = '<div class="fsb categories"><ul>';
            foreach ($categories as $cat_id) {
                $cat = isset($cat_tree[$cat_id]) ? $cat_tree[$cat_id] : null;
                if ($cat) {
                    $data['markup'] .= '<li>' . $cat->path['text'] . '</li>';
                }
            }
            $data['markup'] .= '</ul></div>';
        }

        return $data;
    }

    public function getCategoriesArray()
    {
        $categories_array = array();

        if (isset($this->categories) && trim($this->categories)) {
            $categories_array = explode(',', $this->categories);
        }

        return $categories_array;
    }

    /**
     * Get main category id
     *
     * @return null|int
     */
    public function getMainCategory()
    {
        $main_category_id = null;

        $categories_array = $this->getCategoriesArray();
        if (count($categories_array)) {
            $main_category_id = (int) $categories_array[0];
        }

        return $main_category_id;
    }

    /**
     * Get additional category ids
     *
     * @return null|array
     */
    public function getAdditionalCategories()
    {
        $additional_category_ids = null;

        $categories_array = $this->getCategoriesArray();
        if (count($categories_array) > 1) {
            $additional_category_ids = array(
                (int) $categories_array[1]
            );
            if (isset($categories_array[2])) {
                $additional_category_ids[] = (int) $categories_array[2];
            }
        }

        return $additional_category_ids;
    }


    /**
     * Get Featured Shop's featured ads
     *
     * @return null|\Baseapp\Models\Ads[]
     */
    public function getAds($only_active_ads = true)
    {
        $ads = null;

        if (!empty($this->ads)) {
            // Wound up with some broken shit saved in the DB like `,824295`, which results in
            // syntax error exceptions, so making somewhat sure to avoid those here...
            $ads_list_string = implode(',', $this->makeAdIdsListSafe($this->ads));

            $builder = new \Phalcon\Mvc\Model\Query\Builder();
            $builder->addFrom('Baseapp\Models\Ads', 'ad');
            if ($only_active_ads) {
                $builder->where('ad.active = 1');
            }
            $builder->inWhere('ad.id', explode(',', $ads_list_string));
            $builder->orderBy('FIELD(ad.id, ' . $ads_list_string . ')');

            $ads = $builder->getQuery()->execute();
        }

        return $ads;
    }

    /**
     * @param UsersShops|null $shop
     *
     * @return null|string
     */
    public function buildShoppingWindowMarkup(UsersShops $shop = null, $skip_bootstrap_columns = false, $show_editor_actions = false)
    {
        $escaper = new Escaper();

        // If not provided in advance, get Shop details via relation/alias (extra query behind the scenes)
        if (null === $shop) {
            $shop = $this->Shop;
        }

        if (!$shop) {
            return null;
        }

        $shop_name = $shop->getName();
        $shop_url = $shop->getUrl();

        $markup = '';
        if (!$skip_bootstrap_columns) {
            $markup .= '<div class="shop col-sm-4 col-xs-12">' . PHP_EOL;
        }
        $markup .= '    <div class="ad-box-rectangle-category-listing bg-white border-radius">' . PHP_EOL;
        $markup .= '        <div class="box-header padding-top-sm clearfix">' . PHP_EOL;
        $markup .= '            <div class="col-xs-12">' . PHP_EOL;

        $shop_owner = $shop->getOwner();
        if ($shop_owner) {
            $thumb = $shop_owner->getAvatar();
        } else {
            $thumb = UsersProfileImages::getDefaultAvatarThumb();
        }
        $shop_name_attr = strip_tags($escaper->escapeHtmlAttr($shop_name));
        $thumb->setAlt($shop_name_attr);

        $markup .= '                <div class="fl border-radius overflow-hidden margin-right-sm">' . PHP_EOL;
        $markup .= '                    <a href="' . $shop_url . '"><img src="' . $thumb->getSrc() . '" alt="" width="60" height="60"></a>' . PHP_EOL;
        $markup .= '                </div>' . PHP_EOL;
        $markup .= '                <h2 class="margin-0"><a href="' . $shop_url . '">' . $shop_name . '</a></h2>' . PHP_EOL;
        if ($shop->subtitle) {
            $markup .= '                <span>' . $shop->subtitle . '</span>' . PHP_EOL;
        }
        $markup .= '            </div>' . PHP_EOL;
        $markup .= '        </div>' . PHP_EOL;

        $has_ads                 = false;
        $shop_about_hidden_class = '';
        $shop_ads_hidden_class   = ' hidden';

        $ads = $this->getAds();
        if ($ads && !empty($ads)) {
            $has_ads                 = true;
            $shop_about_hidden_class = ' hidden';
            $shop_ads_hidden_class   = '';
        } else {
            Bootstrap::log('No ads in featured shop windows - should show info text (shop_id: ' . $shop->id . ' | izlog_id: ' . $this->id . ')');
        }

        $markup .= '        <div class="box-body padding-bottom-sm clearfix">' . PHP_EOL;
        $markup .= '            <div class="box-classifieds' . $shop_ads_hidden_class . '">' . PHP_EOL;

        if ($has_ads) {
            foreach ($ads as $ad) {
                $ad_link = $ad->get_frontend_view_link();
                $ad_thumb = $ad->get_thumb('GridView', false, $ad->category_id);
                $ad_title = $ad->getTitle();
                $ad_title_attr = strip_tags($escaper->escapeHtmlAttr($ad_title));
                $ad_thumb->setAlt($ad_title_attr);

                $markup .= '                <div class="col-xs-6 classified-details" data-classified-id="' . $ad->id . '">' . PHP_EOL;
                $markup .= '                    <div class="border-radius overflow-hidden">' . PHP_EOL;
                if ($show_editor_actions) {
                    $markup .= '                        <div class="editor-actions">' . PHP_EOL;
                    $markup .= '                            <span class="action-btn remove-from-shopping-window fa fa-close" data-classified-id="' . $ad->id . '"></span>' . PHP_EOL;
                    $markup .= '                            <a class="action-btn preview-classifieds fa fa-globe" href="' . $ad_link . '" target="_blank"></a>' . PHP_EOL;
                    $markup .= '                        </div>' . PHP_EOL;
                    $markup .= '                        <img class="' . $ad_thumb->getClass() . '" src="' . $ad_thumb->getSrc() . '"' . ($show_editor_actions ? '' : ' width="165" height="165"') . ' alt="' . $ad_title_attr . '">' . PHP_EOL;
                } else {
                    $markup .= '                        <a href="' . $ad_link . '"><img class="' . $ad_thumb->getClass() . '" src="' . $ad_thumb->getSrc() . '" width="165" height="165" alt="' . $ad_title_attr . '"></a>' . PHP_EOL;
                }
                $markup .= '                    </div>' . PHP_EOL;
                $markup .= '                </div>' . PHP_EOL;
            }
        }

        $markup .= '            </div>' . PHP_EOL;

        $shop_about = $shop->getAbout();
        $shop_about = Utils::str_truncate_html($shop_about, 150);
        $markup .= '            <div class="col-xs-12 shop-about' . $shop_about_hidden_class . '">' . $shop_about . '</div>';

        $markup .= '            <div class="col-xs-12 margin-top-sm shop-bottom-wrap">' . PHP_EOL;
        $markup .= '                <div class="meta">' . PHP_EOL;
        if ($shopEmail = $shop->getEmail()) {
            $markup .= '                    <span class="fa fa-envelope fa-fw"></span> <a href="mailto:' . $shopEmail . '" target="_blank" rel="nofollow">' . $shopEmail . '</a><br>' . PHP_EOL;
        }
        if ($shopWeb = $shop->getWeb()) {
            $markup .= '                    <span class="fa fa-globe fa-fw"></span> <a href="' . $shopWeb . '" target="_blank" rel="nofollow">' . $shopWeb . '</a>' . PHP_EOL;
        }
        $markup .= '                </div>' . PHP_EOL;
        $markup .= '                <a class="black bold display-block margin-top-sm" href="' . $shop_url . '">Pogledaj sve oglase ovog oglašivača<span class="fa fa-angle-right fa-fw"></span></a>' . PHP_EOL;
        $markup .= '            </div>' . PHP_EOL;
        $markup .= '        </div>' . PHP_EOL;
        $markup .= '    </div>' . PHP_EOL;
        if (!$skip_bootstrap_columns) {
            $markup .= '</div>' . PHP_EOL;
        }

        return $markup;
    }

    /**
     * Helper method to generate an array of shopping windows on a given resultset
     *
     * @param  \Phalcon\Mvc\Model\Resultset $results
     * @return null|array
     */
    private static function getShoppingWindowsMarkupArrayFromResults($results)
    {
        $shoppingWindows = null;

        if ($results->valid()) {
            $shoppingWindows = array();

            // Generating each shopping window's markup
            foreach ($results as $result) {
                /* @var UsersShops $usersShop */
                $usersShop = $result->shop;
                /* @var UsersShopsFeatured $usersShopFeatured */
                $usersShopFeatured = $result->izlog;

                // add generated markup for current featured shop (shopping window) to our array
                $shoppingWindows[] = $usersShopFeatured->buildShoppingWindowMarkup($usersShop);
            }

            if (empty($shoppingWindows)) {
                $shoppingWindows = null;
            }
        }

        return $shoppingWindows;
    }

    private static function isValidCachedArray($cachedArray)
    {
        if (!$cachedArray || empty($cachedArray)) {
            return false;
        }

        if (!isset($cachedArray['markup']) || !isset($cachedArray['hash']) || empty($cachedArray['markup'])) {
            return false;
        }

        if (md5(serialize($cachedArray['markup'])) !== $cachedArray['hash']) {
            return false;
        }

        return true;
    }

    /**
     * Helper method to generate markup of shopping windows on a given resultset
     *
     * @param  \Phalcon\Mvc\Model\Resultset $results
     * @return null|string(html)
     */
    private static function buildShoppingWindowsMarkupFromResults($results, $withTagline = true)
    {
        $markup = null;

        if ($markupArray = static::getShoppingWindowsMarkupArrayFromResults($results)) {
            $markup = static::buildShoppingWindowsMarkupFromArray($markupArray, $withTagline);
        }

        return $markup;
    }

    /**
     * Helper method to generate markup for shopping windows on a given array of shopping windows' markups
     *
     * @param  array    $markupArray  Array of shopping windows' markups
     * @param  boolean  $withTagline  Whether to display tagLine (Izdvojene trgovine) or not
     * @return null|string(html)
     */
    private static function buildShoppingWindowsMarkupFromArray($markupArray, $withTagline = true)
    {
        $markup = null;

        if ($markupArray && !empty($markupArray)) {
            $markup = '<div class="featured-shops">' . PHP_EOL;
            if ($withTagline) {
                $markup .= '    <h2 class="section-title hidden-xs"><span class="tagline">Izdvojene trgovine</span></h2>' . PHP_EOL;
            }
            $markup .= '    <div class="row no-gutters-xs featured-shops-windows">' . PHP_EOL;

            // Randomize the order of shopping windows somewhat before merging them as strings
            shuffle($markupArray);
            $markup .= implode('', $markupArray);

            $markup .= '    </div>' . PHP_EOL;

            if (count($markupArray) > 3) {
                $markup .= '    <div class="row no-gutters-xs featured-shops-windows-pager">' . PHP_EOL;
                $markup .= '        <div class="col-sm-4 col-sm-offset-4 hidden-xs">' . PHP_EOL;
                $markup .= '            <nav>' . PHP_EOL;
                $markup .= '                <ul class="pager clearfix">' . PHP_EOL;
                $markup .= '                    <li class="prev"><a href="#" data-dir="prev">&lt;</a></li>' . PHP_EOL;
                $markup .= '                    <li class="number"></li>' . PHP_EOL;
                $markup .= '                    <li class="next"><a href="#" data-dir="next">&gt;</a></li>' . PHP_EOL;
                $markup .= '                </ul>' . PHP_EOL;
                $markup .= '            </nav>' . PHP_EOL;
                $markup .= '        </div>' . PHP_EOL;
                $markup .= '    </div>' . PHP_EOL;
            }

            $markup .= '</div>' . PHP_EOL;
        }

        return $markup;
    }

    /**
     * Helper method to build shopping windows markup for specific category
     *
     * @param  Categories  $category       Category we want shopping windows from
     * @param  boolean     $cacheResults   Should we try to get/set cached results?
     * @param  null|int    $cacheLifeTime  How long should we cache results
     *
     * @return null|string(html)
     */
    public static function buildCategoryShoppingWindowsMarkup(Categories $category, $cacheResults = false, $cacheLifeTime = null)
    {
        $markupArray = null;

        if (isset($category->id) && $category->id) {
            $cachedArray = null;
            $memcache    = null;
            $cacheKey    = static::MEMCACHED_KEY . '_category_' . $category->id;

            // Try getting already built shopping windows array from cache, if that fails, generate a new one
            if ($cacheResults && null !== $cacheLifeTime && $memcache = static::getMemcache()) {
                try {
                    $cachedArray = $memcache->get($cacheKey, $cacheLifeTime);
                } catch (PhCacheException $e) {
                    $memcache = null;
                    Bootstrap::log($e);
                }

                if (static::isValidCachedArray($cachedArray)) {
                    $markupArray = $cachedArray['markup'];
                }
            }

            if (!$markupArray) {
                $results     = UsersShops::getFeatured($category->id);
                $markupArray = static::getShoppingWindowsMarkupArrayFromResults($results);

                if ($markupArray && !empty($markupArray)) {
                    $cachedArray = array(
                        'markup' => $markupArray,
                        'hash'   => md5(serialize($markupArray))
                    );

                    if ($cacheResults && null !== $cacheLifeTime && $memcache) {
                        $memcache->save($cacheKey, $cachedArray, $cacheLifeTime);
                    }
                } else {
                    $markupArray = null;
                }
            }
        }

        return static::buildShoppingWindowsMarkupFromArray($markupArray);
    }

    /**
     * Helper method to build shopping windows markup for multiple category id's
     *
     * @param  array       $categoryIDs    Array of category id's we want shopping windows for
     * @param  boolean     $cacheResults   Should we try to get/set cached results?
     * @param  null|int    $cacheLifeTime  How long should we cache results
     *
     * @return null|string(html)
     */
    public static function buildSectionShoppingWindowsMarkup($categoryIDs = array(), $cacheResults = false, $cacheLifeTime = null)
    {
        $markupArray = null;

        if (!empty($categoryIDs)) {
            $cachedArray = null;
            $memcache    = null;
            $cacheKey    = static::MEMCACHED_KEY . '_categories_' . md5(implode(',', $categoryIDs));

            // Try getting already built shopping windows array from cache, if that fails, generate a new one
            if ($cacheResults && null !== $cacheLifeTime && $memcache = static::getMemcache()) {
                try {
                    $cachedArray = $memcache->get($cacheKey, $cacheLifeTime);
                } catch (PhCacheException $e) {
                    $memcache = null;
                    Bootstrap::log($e);
                }

                if (static::isValidCachedArray($cachedArray)) {
                    $markupArray = $cachedArray['markup'];
                }
            }

            if (!$markupArray) {
                $results     = UsersShops::queryShoppingWindowsForCategories($categoryIDs);
                $markupArray = static::getShoppingWindowsMarkupArrayFromResults($results);

                if ($markupArray && !empty($markupArray)) {
                    $cachedArray = array(
                        'markup' => $markupArray,
                        'hash'   => md5(serialize($markupArray))
                    );

                    if ($cacheResults && null !== $cacheLifeTime && $memcache) {
                        $memcache->save($cacheKey, $cachedArray, $cacheLifeTime);
                    }
                } else {
                    $markupArray = null;
                }
            }
        }

        return static::buildShoppingWindowsMarkupFromArray($markupArray);
    }

    /**
     * Helper method to build shopping windows markup for multiple shopping windows id's
     *
     * @param  array       $ids            Array of shopping windows' id's we want markup for
     * @param  boolean     $cacheResults   Should we try to get/set cached results?
     * @param  null|int    $cacheLifeTime  How long should we cache results
     *
     * @return null|string(html)
     */
    public static function buildShoppingWindowsMarkupByIDs($ids = array(), $cacheResults = false, $cacheLifeTime = null)
    {
        $markupArray = null;

        if (!empty($ids)) {
            $cachedArray = null;
            $memcache    = null;
            $cacheKey    = static::MEMCACHED_KEY . '_ids_' . md5(implode(',', $ids));

            // Try getting already built shopping windows array from cache, if that fails, generate a new one
            if ($cacheResults && null !== $cacheLifeTime && $memcache = static::getMemcache()) {
                try {
                    $cachedArray = $memcache->get($cacheKey, $cacheLifeTime);
                } catch (PhCacheException $e) {
                    $memcache = null;
                    Bootstrap::log($e);
                }

                if (static::isValidCachedArray($cachedArray)) {
                    $markupArray = $cachedArray['markup'];
                }
            }

            if (!$markupArray) {
                $results     = UsersShops::getShoppingWindowsByIDs($ids);
                $markupArray = static::getShoppingWindowsMarkupArrayFromResults($results);

                if ($markupArray && !empty($markupArray)) {
                    $cachedArray = array(
                        'markup' => $markupArray,
                        'hash'   => md5(serialize($markupArray))
                    );

                    if ($cacheResults && null !== $cacheLifeTime && $memcache) {
                        $memcache->save($cacheKey, $cachedArray, $cacheLifeTime);
                    }
                } else {
                    $markupArray = null;
                }
            }
        }

        return static::buildShoppingWindowsMarkupFromArray($markupArray, false);
    }

    /**
     * Returns the first found unpaid Order that contains our Izlog product (if there is one, false otherwise).
     *
     * @return false|Orders
     */
    public function getFirstOutstandingOrder()
    {
        // First get a list of all new orders belonging to this user.
        // Then see if any of those have order items with this specific product, and if so,
        // if that product's extra order data matches this shopping window/shop id combination.
        $shop      = $this->Shop;
        $shop_id   = $shop->id;
        $user_id   = $shop->getOwner()->id;
        $window_id = $this->id;

        $results = Orders::findUnpaidOrdersWithIzlogProductForUser($user_id);
        $found = $order = false;
        foreach ($results as $row) {
            $order   = $row->o;
            $item    = $row->oi;
            $product = $item->getProduct();
            $ordered_details = $product->getOrderData();
            if ($ordered_details) {
                if (isset($ordered_details['shop_id']) && isset($ordered_details['shopping_window_id'])) {
                    $found = ($ordered_details['shop_id'] == $shop_id && $ordered_details['shopping_window_id'] == $window_id);
                    if ($found) {
                        break;
                    }
                }
            }
        }

        // Reset the return value back to false if nothing was found
        if (!$found) {
            $order = false;
        }

        return $order;
    }

    /**
     * @param string|array $list
     *
     * @return array
     */
    public function makeAdIdsListSafe($list)
    {
        if (is_string($list)) {
            $list = array_map('intval', explode(',', $list));
        }

        $list = array_map(function($el){
            return trim($el);
        }, $list);

        $list = array_unique(array_filter($list));

        return $list;
    }
}
