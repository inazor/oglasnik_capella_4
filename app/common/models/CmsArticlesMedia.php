<?php

namespace Baseapp\Models;

class CmsArticlesMedia extends BaseModelBlamable
{
    public $id;
    public $article_id;
    public $media_id;
    public $sort_idx;

    public function initialize()
    {
        $this->belongsTo(
            'media_id', __NAMESPACE__ . '\Media', 'id',
            array(
                'alias'      => 'Media',
                'reusable'   => true
            )
        );

        $this->belongsTo(
            'article_id', __NAMESPACE__ . '\CmsArticles', 'id',
            array(
                'alias'      => 'Article',
                'reusable'   => true
            )
        );

        parent::initialize();
    }

    public function getSource()
    {
        return 'cms_articles_media';
    }

    public function getMediaForMultipleArticleIds($articleIDs = null)
    {
        $mediaItems = null;

        if ($articleIDs) {
            if (!is_array($articleIDs)) {
                if (strpos($articleIDs, ',') !== false) {
                    $articleIDs = explode(',', $articleIDs);
                } elseif (is_numeric($articleIDs)) {
                    $articleIDs = array((int) $articleIDs);
                }
            }

            if (count($articleIDs)) {
                $builder = new \Phalcon\Mvc\Model\Query\Builder();
                $builder->columns(array('media.*', 'cam.article_id'));
                $builder->addFrom('Baseapp\Models\Media', 'media');
                $builder->innerJoin('Baseapp\Models\CmsArticlesMedia', 'media.id = cam.media_id AND cam.sort_idx = 1', 'cam');
                $builder->inWhere('cam.article_id', $articleIDs);
                $builder->orderBy('cam.article_id');

                $mediaItems = $builder->getQuery()->execute();
            }
        }

        return $mediaItems;
    }

}
