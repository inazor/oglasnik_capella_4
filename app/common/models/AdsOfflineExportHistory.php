<?php

namespace Baseapp\Models;

use Baseapp\Library\Avus\AvusExportList;

class AdsOfflineExportHistory extends BaseModel
{
    public $id;
    public $ad_id;
    public $appearance_date;
    public $product;
    public $exported_id = null;
    public $processed = 0;

/*
    public function initialize()
    {
        parent::initialize();
    }
*/

    public function getSource()
    {
        return 'ads_offline_export_history';
    }

    public static function getByAppearanceDateOrCreateNew($ad_id, $appearance_date)
    {
        $ads_offline_export_history = null;

        $ads_offline_export_history = self::findFirst(array(
            'conditions' => 'ad_id = :ad_id: AND appearance_date = :appearance_date:',
            'bind'       => array(
                'ad_id'           => $ad_id,
                'appearance_date' => $appearance_date
            )
        ));

        if (!$ads_offline_export_history) {
            $ads_offline_export_history                  = new self();
            $ads_offline_export_history->ad_id           = $ad_id;
            $ads_offline_export_history->appearance_date = $appearance_date;
        }

        return $ads_offline_export_history;
    }

    public function getUnprocessedRecords()
    {
        return self::find('exported_id IS NOT NULL AND processed = 0');
    }

    public function markProcessedRecords()
    {
        $marked_as_processed_count       = 0;
        $unprocessed_record_exported_ids = array();

        if ($unprocessed_records = $this->getUnprocessedRecords()) {
            foreach ($unprocessed_records as $record) {
                $unprocessed_record_exported_ids[] = $record->exported_id;
            }
        }

        if (!count($unprocessed_record_exported_ids)) {
            return 0;
        }

        $avusExportList = new AvusExportList();
        $exported_rows  = $avusExportList->getExportedIds($unprocessed_record_exported_ids);

        if (!$exported_rows) {
            return 0;
        }

        $processed_records = array();
        foreach ($exported_rows as $row) {
            if ($row['exported'] == 'Y') {
                $processed_records[] = $row['id'];
            }
        }

        if (count($processed_records)) {
            $sql    = "UPDATE `" . $this->getSource() . "` SET `processed` = 1 WHERE `exported_id` IN (" . implode(",", $processed_records) . ") AND `processed` = 0";
            $conn   = $this->getWriteConnection();
            $result = $conn->execute($sql);
            $marked_as_processed_count = $conn->affectedRows();
        }

        return $marked_as_processed_count;
    }

}
