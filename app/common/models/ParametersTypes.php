<?php

namespace Baseapp\Models;

/**
 * ParametersTypes represents different kinds of Parameters
 */
class ParametersTypes extends BaseModel
{
    // Base attributes

    public $id;                 // varchar(32)  - UPPERCASE name of parameters type (PK)
    public $class;              // varchar(64)  - classes to use on menus
    public $accept_dictionary;  // tinyint(1)   - if parameter is driven by dictionary
    public $can_other;          // tinyint(1)   - if parameter can have 'Other' so we can display additional text field

    /**
     * ParametersTypes initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasMany('id', __NAMESPACE__ . '\Parameters', 'type_id', array(
            'alias' => 'Parameters'
        ));
    }

}
