<?php

namespace Baseapp\Extension;

use Phalcon\Mvc\Model\Query;

class PrettyExceptions extends \Phalcon\Debug
{
    public function __construct()
    {
        $this->setUri('/assets/debug/1.2.0/');
    }

    /**
     * Handles uncaught exceptions
     *
     * @param \Exception exception
     * @return boolean
     */
    public function onUncaughtException(\Exception $exception)
    {
        /**
         * Cancel the output buffer if active
         */
        /*
        $obLevel = ob_get_level();
        if ($obLevel > 0) {
            ob_end_clean();
        }*/

        /**
         * Avoid that multiple exceptions being showed
         */
        if (self::$_isActive) {
            echo $exception->getMessage();
            return;
        }

        /**
         * Globally block the debug component to avoid other exceptions must be shown
         */
        self::$_isActive = true;

        $className = get_class($exception);

        /**
         * Escape the exception's message avoiding possible XSS injections?
         */
        $escapedMessage = $this->_escapeString($exception->getMessage());

        /**
         * CSS static sources to style the error presentation
         * Use the exception info as document"s title
         */
        $html = '<!DOCTYPE html><html><head><meta charset="utf-8">';
        $html .= '<title>' . $className . ': ' . $escapedMessage . '</title>';
        $html .= $this->getCssSources() . '</head><body>';

        /**
         * Get the version link
         */
        $html .= $this->getVersion();

        /**
         * Main exception info
         */
        $html .= '<div align="center"><div class="error-main">';
        $html .= '<h1>' . $className . ': ' . $escapedMessage . '</h1>';
        $html .= '<span class="error-file">' . $exception->getFile() . ' (' . $exception->getLine() . ')</span>';
        $html .= '</div>';

        $showBackTrace = $this->_showBackTrace;

        /**
         * Check if the developer wants to show the backtrace or not
         */
        if ($showBackTrace) {

            $dataVars = $this->_data;

            /**
             * Create the tabs in the page
             */
            $html .= '<div class="error-info"><div id="tabs"><ul>';
            $html .= '<li><a href="#error-tabs-1">Backtrace</a></li>';
            $html .= '<li><a href="#error-tabs-2">Request</a></li>';
            $html .= '<li><a href="#error-tabs-3">Server</a></li>';
            $html .= '<li><a href="#error-tabs-4">Included Files</a></li>';
            $html .= '<li><a href="#error-tabs-5">Memory</a></li>';
            if (is_array($dataVars)) {
                $html .= '<li><a href="#error-tabs-6">Variables</a></li>';
            }
            $html .= '</ul>';

            /**
             * Print backtrace
             */
            $html .= '<div id="error-tabs-1"><table cellspacing="0" align="center" width="100%">';
            /*
            $trace = $exception->getTrace();
            foreach ($trace as $n => $traceItem) {
                // Every line in the trace is rendered using "showTraceItem", but if we
                // call it, it keeps throwing Phalcon's internal "Wrong number of parameters" exception...
                $html .= $this->showTraceItem($n, $traceItem);
            }
            */
            $html .= '<tr><td><pre>' . $exception->getTraceAsString() . '</pre></td>';
            $html .= '</table></div>';

            /**
             * Print _REQUEST superglobal
             */
            $html .= '<div id="error-tabs-2"><table cellspacing="0" align="center" class="superglobal-detail">';
            $html .= '<tr><th>Key</th><th>Value</th></tr>';
            foreach ($_REQUEST as $keyRequest => $value) {
                $html .= '<tr><td class="key">' . $keyRequest . '</td><td class="value">' . $this->_getVarDump($value). '</td></tr>';
            }
            $html .= '</table></div>';

            /**
             * Print _SERVER superglobal
             */
            $html .= '<div id="error-tabs-3"><table cellspacing="0" align="center" class="superglobal-detail">';
            $html .= '<tr><th>Key</th><th>Value</th></tr>';
            foreach ($_SERVER as $keyServer => $value) {
                // $html .= '<tr><td class="key">' . $keyServer . '</td><td>' . Utils::soft_break($value) . '</td></tr>';;
                $html .= '<tr><td class="key">' . $keyServer . '</td><td class="value">' . $this->_getVarDump($value) . '</td></tr>';
            }
            $html .= '</table></div>';

            /**
             * Show included files
             */
            $html .= '<div id="error-tabs-4"><table cellspacing="0" align="center" class="superglobal-detail">';
            $html .= '<tr><th>#</th><th>Path</th></tr>';
            foreach (get_included_files() as $keyFile => $value) {
                $html .= '<tr><td class="key">' . $keyFile . '</th><td class="value">' . $value . '</td></tr>';
            }
            $html .= '</table></div>';

            /**
             * Memory usage
             */
            $html .= '<div id="error-tabs-5"><table cellspacing="0" align="center" class="superglobal-detail">';
            $html .= '<tr><th colspan="2">Memory</th></tr><tr><td class="key">Usage</td><td class="value">' . memory_get_usage(true) . '</td></tr>';
            $html .= '</table></div>';

            /**
             * Print extra variables passed to the component
             */
            if (is_array($dataVars)) {
                $html .= '<div id="error-tabs-6"><table cellspacing="0" align="center" class="superglobal-detail">';
                $html .= '<tr><th>Key</th><th>Value</th></tr>';
                foreach ($dataVars as $keyVar => $dataVar) {
                    $html .= '<tr><td class="key">' . $keyVar . '</td><td class="value">' . $this->_getVarDump($dataVar[0]) . '</td></tr>';
                }
                $html .= '</table></div>';
            }

            $html .= '</div>';
        }

        /**
         * Get Javascript sources
         */
        $html .=  $this->getJsSources() . '</div></body></html>';

        /**
         * Print the HTML, @TODO, add an option to store the html
         */
        // echo $html;

        /**
         * Unlock the exception renderer
         */
        self::$_isActive = false;

        return $html;
    }
}
