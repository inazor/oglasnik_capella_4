<?php

namespace Baseapp\Extension\Validator;

use Baseapp\Traits\ValidatorTrait;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class BirthDate extends Validator implements ValidatorInterface
{
    use ValidatorTrait;

    protected function validateSingle($value)
    {
        if ($value && !$this->validateBirthDate($value)) {
            $this->validator->appendMessage(new Message($this->getMessage(), $this->attribute, 'Date'));
            return false;
        }

        return true;
    }

    private function validateBirthDate($value)
    {
        if (!$this->isSetOption('format')) {
            throw new Exception('`format` option must be set!');
        }

        $date = \DateTime::createFromFormat($this->getOption('format'), $value);
        if (!$date) {
            return false;
        }

        // Check that the birth year is not larger than current year
        $birth_year = $date->format('Y');

        // Check that year is within allowed min/max
        $year_now = date('Y');
        // @link https://en.wikipedia.org/wiki/Oldest_people#Ten_verified_oldest_people_living
        $birth_year_min = $this->getOption('minYear') ? $this->getOption('minYear') : 1899;
        $birth_year_max = $this->getOption('maxYear') ? $this->getOption('maxYear') : $year_now;
        if ($birth_year > $birth_year_max || $birth_year < $birth_year_min) {
            return false;
        }

        return true;
    }

    private function getMessage()
    {
        $message = $this->getOption('message');
        if (!$message) {
            $message = 'Birth date is not valid (format: ' . $this->getOption('format') . ').';
        }

        return $message;
    }
}
