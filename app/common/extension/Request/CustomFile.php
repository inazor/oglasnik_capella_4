<?php

namespace Baseapp\Extension\Request;

use \Phalcon\Http\Request\File;

/**
 * Extends \Phalcon\Http\Request\File to support additional stuff we'd like to have
 * or do with File object
 */
class CustomFile extends File
{

    // overwrite moveTo method in a way not to use default 'move_uploaded_file' method
    public function moveTo($destination)
    {
        $copy_result = false;

        if (file_exists($this->_tmp)) {
            $copy_result = copy($this->_tmp, $destination);
            if ($copy_result) {
                @unlink($this->_tmp);
            }
        }

        return $copy_result;
    }

}
