<?php

namespace Baseapp\Traits;

/**
 * ErrorControllerTrait encapsulates commonalities between frontend/backend
 * error controllers since Phalcon can't do cross-module dispatching and we
 * cannot forward requests between frontend/backend modules (and we kind of
 * need to do that if we're to have custom error views in a single place).
 *
 * This way at least the controller actions are defined in a single place,
 * and we only have to duplicate views for now. And that might be mitigated
 * in the close future too.
 */
trait ErrorControllerTrait
{
    public function indexAction()
    {
        return $this->redirect_home();
    }

    public function show404Action()
    {
        $this->tag->setTitle('Greška 404 - Ups! Stranica koju tražiš ne postoji');
        $this->response->setStatusCode(404, 'Not Found');

        // Force the view file since some controllers call pick() too early which
        // overrides Phalcon's "last-controller/last-action" default view picking...
        $this->view->pick('error/show404');
    }

    public function show400Action()
    {
        $this->tag->setTitle('Greška 400 - Bad Request');
        $this->response->setStatusCode(400, 'Bad Request');

        // Force the view file since some controllers call pick() too early which
        // overrides Phalcon's "last-controller/last-action" default view picking...
        $this->view->pick('error/show400');
    }

    public function show500Action()
    {
        // TODO/FIXME:
        // Disabling search on error 500 for now, because it loads assets twice for some reason,
        // but we need to fix the underlying issue ASAP
        if (method_exists($this, 'setLayoutFeature')) {
            $this->setLayoutFeature('search_bar', false);
        }

        $this->tag->setTitle('Greška 500 - Internal Server Error');
        $this->response->setStatusCode(500, 'Internal Server Error');

        // Force the view file since some controllers call pick() too early which
        // overrides Phalcon's "last-controller/last-action" default view picking...
        $this->view->pick('error/show500');
    }

    public function show503Action()
    {
        if (method_exists($this, 'setLayoutFeature')) {
            $this->setLayoutFeature('search_bar', false);
        }

        $this->tag->setTitle('Greška 503 - Usluga privremeno nedostupna');
        $this->response->setStatusCode(503, 'Service Not Available');

        // Force the view file since some controllers call pick() too early which
        // overrides Phalcon's "last-controller/last-action" default view picking...
        $this->view->pick('error/show503');
    }
}
