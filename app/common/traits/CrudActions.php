<?php

namespace Baseapp\Traits;

use Baseapp\Extension\Tag;
use Baseapp\Models\Ads;
use Baseapp\Models\AdsHomepage;
use Baseapp\Models\Categories;
use Baseapp\Models\CmsCategories;
use Baseapp\Models\Dictionaries;
use Baseapp\Models\Orders;

/**
 * Trait CrudActions abstracts the most common and simplest CRUD controller actions
 */
trait CrudActions
{

    // TODO: editAction(), indexAction(), createAction() maybe...

    public function deleteAction($entity_id = null)
    {
        if (!$this->auth->logged_in('admin')) {
            $this->flashSession->error('Deleting resources from DB is something that only admins can do!');
            return $this->redirect_back();
        } else {
            $this->view->pick('chunks/delete');

            // Check if multiple resources are to be handled
            if (false !== strpos($entity_id, ',')) {
                $entity_ids = explode(',', $entity_id);
            } else {
                $entity_id = (int) $entity_id;
                if (!$entity_id) {
                    $this->flashSession->error('Missing required ID parameter for delete action');

                    return $this->redirect_back();
                }
                $entity_ids = array($entity_id);
            }

            /* @var $model \Baseapp\Models\BaseModel */
            $model = $this->crud_model_class;

            $ids = implode(',', array_map(function($id){
                return (int) trim($id);
            }, $entity_ids));

            /* @var $entities \Baseapp\Models\BaseModel[]|\Phalcon\Mvc\Model\Resultset */
            $condition = 'id IN (' . $ids . ')';
            $entities = $model::find($condition);

            if (!$entities->valid()) {
                $this->flashSession->error(sprintf('Entities not found [Model: %s, ID(s): %s]', $this->crud_model_class, $ids));
                return $this->redirect_back();
            }

            // Check if any entity in the collection is marked as non-deletable
            foreach ($entities as $entity) {
                if (isset($entity->is_deletable) && !$entity->is_deletable) {
                    $this->flashSession->error(sprintf('Entity is protected and deletion was skipped [Model: %s, ID: %s]', $this->crud_model_class, $entity->ident()));
                }
            }

            // Do actual deletion if needed
            if ($this->request->isPost()) {
                foreach ($entities as $entity) {
                    $ident = $entity->ident();
                    $ident_id = $entity->id;
                    if ($entity instanceof Dictionaries || $entity instanceof Categories || $entity instanceof CmsCategories) {
                        $result = $entity->deleteNode();
                    } else {
                        $result = $entity->delete();
                    }
                    if ($result) {
                        // Ad-specific related deletions/cancellations
                        if ($entity instanceof Ads) {
                            // When deleting an ad, delete the potential AdsHomepage record too
                            AdsHomepage::deleteByAdId($ident_id);
                            // Cancel all pending orders also..
                            Orders::cancelAllPendingOrdersForAd($ident_id);
                        } elseif ($entity instanceof Dictionaries || $entity instanceof Categories || $entity instanceof CmsCategories) {
                            $entity->deleteMemcachedData();
                            if ($entity instanceof Dictionaries) {
                                $entity->refreshMemcachedDictionaryTree();
                            }
                        }
                        $this->flashSession->success('Resource <strong>' . $ident . '</strong> has been deleted.');
                    } else {
                        $this->flashSession->error('Failed deleting <strong>' . $ident . '</strong>!');
                    }
                }
                return $this->redirect_to($this->get_next_url());
            }

            // Setup view vars
            $this->tag->setTitle('Delete Confirmation');
            Tag::setDefault('entity_id', $entity_id);
            $this->view->setVar('entity_id', $entity_id);
            $this->view->setVar('entities', $entities);
            $this->view->setVar('model', $model);
            $this->view->setVar('is_soft_delete', false);

            // Check if entity has relations and display a warning
            $fk_relations = $this->get_model_relations();
            if (!empty($fk_relations)) {
                $fk_warning = array();
                $fk_warning['message'] = '<strong>Warning:</strong> Model has relationships defined!';
                $fk_warning['message'] .= ' Deleting such entities could cause unintended consequences!';
                $fk_warning['relations'] = $fk_relations;
                $this->view->setVar('fk_warning', $fk_warning);
            }
        }
    }

    private function get_model_relations()
    {
        $model = $this->crud_model_class;
        $mm = $this->modelsManager;
        $mm->load($model);
        // $this->modelsManager->initialize(new $model());
        $relations = $mm->getRelations($model);
        $relationships = array();
        if (!empty($relations)) {
            foreach ($relations as $relation) {
                $ref_model = $relation->getReferencedModel();
                $relationship = array();
                $relationship['ref_model'] = $ref_model;
                $relationship['fields'] = var_export($relation->getFields(), true);
                $relationship['ref_fields'] = var_export($relation->getReferencedFields(), true);
                $relationship['config'] = var_export($relation->getForeignKey(), true);
                $relationships[] = $relationship;
            }
        }
        return $relationships;
    }

    /*
    protected function check_entity($entity_id)
    {
        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for delete action');
            return $this->redirect_back();
        }

        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }
    }
    */

}
