<?php

namespace Baseapp\Traits;

use Baseapp\Library\Utils;
use Baseapp\Models\Ads;
use Baseapp\Models\Categories;
use Baseapp\Models\CategoriesSettings;
use Baseapp\Models\Users;

trait RandomSpecialProductsHelpers
{
    use AgeRestrictionHelpers;

    protected function getRandomAdsWithSpecialProduct($product_id, $category_ids = array(), $combineSpecialProducts = false)
    {
        $amount  = 3;

        $builder = $this->modelsManager->createBuilder();
        $columns = array('ad.*');
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
//        $builder->innerJoin('Baseapp\Models\Categories', 'ad.category_id = category.id AND ad.active = 1 AND ad.is_spam = 0', 'category');

        $whereArray  = array(
            'ad.active = 1 AND ad.is_spam = 0 AND ad.online_product_id = :product_id:'
        );
        $whereParams = array(
            'product_id' => (string) $product_id
        );

        // Match ads within any of the specified category ids (if specified)
        if (!is_array($category_ids) && !empty($category_ids)) {
            $category_ids = (array) $category_ids;
        }
        if (!empty($category_ids)) {
            // References the join alias
            $whereArray[] = 'ad.category_id IN (' . implode(', ', array_map('intval', $category_ids)) . ')';
        }

        $builder->columns($columns);
        $builder->where(implode(' AND ', $whereArray), $whereParams);
        $builder->orderBy('RAND()');
        $builder->limit($amount);

        $resultset = $builder->getQuery()->execute();
        $ads       = Ads::getEnrichedFrontendBasicResultsArray($resultset, $combineSpecialProducts);

        return $ads;
    }

    /**
     * @param string $search_term
     * @param string $product_id
     * @param array $category_ids Array of category ids to restrict the search to
     *
     * @return array
     */
    public function getRandomAdsWithSpecialProductMatchingSearch($search_term, $product_id, $category_ids = array())
    {
        $amount = 3;

        $search_term = Utils::remove_accents($search_term);
        $search_term = Utils::strip_boolean_operator_characters($search_term);

        // $search_term_parsed = Utils::build_boolean_mode_operators($search_term, '+', '*');
        $search_term_parsed = Utils::build_boolean_mode_operators($search_term, '+');
        $search_term_parsed = Utils::quote_unquoted_words_with_dots($search_term_parsed);

        $builder = $this->modelsManager->createBuilder();
        $columns = array('ad.*');

        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->innerJoin('Baseapp\Models\AdsSearchTerms', 'ad.id = ast.ad_id', 'ast');

        $where_array  = array(
            // '(FULLTEXT_MATCH_BMODE(ast.search_data, :search_term_parsed:) OR (LOWER(ad.title) LIKE CONCAT(\'%\', :search_like1:, \'%\') OR LOWER(ad.description) LIKE CONCAT(\'%\', :search_like2:, \'%\') OR ast.search_data LIKE CONCAT(\'%\', :search_like3:, \'%\')))',
            'FULLTEXT_MATCH_BMODE(ast.search_data_unaccented, :search_term_parsed:)',
        );

        $search_term_parsed_lc = trim(mb_strtolower($search_term_parsed, 'UTF-8'));
        $search_term_lc        = trim(mb_strtolower($search_term, 'UTF-8'));

        $where_params = array(
            'search_term_parsed' => $search_term_parsed_lc,
            //'search_like1'       => $search_term_lc,
            //'search_like2'       => $search_term_lc,
            //'search_like3'       => $search_term_lc
        );
        $where_types  = array(
            'search_term_parsed' => \PDO::PARAM_STR,
            //'search_like1'       => \PDO::PARAM_STR,
            //'search_like2'       => \PDO::PARAM_STR,
            //'search_like3'       => \PDO::PARAM_STR
        );

        // Only include ads with the specified product_id
        $where_array[] = 'ad.online_product_id = "' . $product_id . '"';

        // Match ads only within the specified category ids (if specified and if they're not excluded due to age restrictions)
        if (!is_array($category_ids) && !empty($category_ids)) {
            $category_ids = array();
            $category_ids[] = (int) $category_ids;
        }
        $category_ids = array_map('intval', $category_ids);

        // Check if we're supposed to exclude age restricted categories (and which)
        /* @var Users $user */
        $user = $this->auth->get_user();
        $restriction_data = $this->getSearchAgeRestrictionData($user);
        $exclude_restricted      = $restriction_data['exclude'];
        $restricted_category_ids = $restriction_data['category_ids'];

        // If the search is scoped to one more categories, include only those results
        if (!empty($category_ids)) {
            // Make sure that what we're scoped to is actually allowed (aka not age restricted)
            if ($exclude_restricted && !empty($restricted_category_ids)) {
                $category_ids  = array_diff($restricted_category_ids, $category_ids);
            }
            // If the result is now empty, we've gotten some specific categories to search for, but they
            // all appear to be restricted, so we can't really return any results in that case
            if (empty($category_ids)) {
                return array();
            }
        }

        // Join on categories if we have any and include any potential subcategories into the results too
        if (!empty($category_ids)) {
            $builder->innerJoin('Baseapp\Models\Categories', 'ad.category_id = category.id', 'category');
            $cat_where_array = array();
            $cat_where_params = array();
            $tree = $this->getDI()->get(Categories::MEMCACHED_KEY);
            foreach ($category_ids as $cat_id) {
                $category = $tree[$cat_id];
                $cat_where_array[] = '(category.lft >= :category_' . $cat_id . '_left: AND category.rght <= :category_' . $cat_id . '_rght:)';
                $where_params['category_' . $cat_id . '_left'] = $category->lft;
                $where_params['category_' . $cat_id . '_rght'] = $category->rght;
            }
            if (count($cat_where_array)) {
                $where_array[] = '(' . implode(' OR ', $cat_where_array) . ')';
                $where_params = array_merge($where_params, $cat_where_params);
            }
        }

        $where_array[] = 'ad.active = 1';
        $where_array[] = 'ad.is_spam = 0';

        // Exclude any potential results from any restricted categories
        if ($exclude_restricted && !empty($restricted_category_ids)) {
            $where_array[] = 'ad.category_id NOT IN (' . implode(',', $restricted_category_ids) . ')';
        }

        if (count($where_types)) {
            $builder->where(implode(' AND ', $where_array), $where_params, $where_types);
        } else {
            $builder->where(implode(' AND ', $where_array), $where_params);
        }

        $builder->columns($columns);
        $builder->orderBy('RAND()');
        $builder->groupBy(array('ad.id'));

        $builder->limit($amount);

        $resultset = $builder->getQuery()->execute();
        $ads       = Ads::getEnrichedFrontendBasicResultsArray($resultset);

        return $ads;
    }

    /**
     * Returns the markup built using `categories/ad-chunk.volt` template for 3 random ads
     * that have the special $product_id ('pinky' or 'platinum' for now) and optionally belong
     * only to the specified list of $category_ids.
     *
     * @param $product_id
     * @param array $category_ids
     *
     * @return null|string
     */
    protected function generateSpecialProductsMarkup($product_id, $category_ids = array())
    {
        // Should get 3 random ads that have the special products (Pinky/Platinum) and belong
        // enabled within $cat_settings (along with other conditions required to display the ads)
        // and generate their markup and return it
        $markup = null;

        $ads = $this->getRandomAdsWithSpecialProduct($product_id, $category_ids);

        if (!empty($ads)) {
            $markup = $this->renderSpecialProductMarkup($ads);
        }

        return $markup;
    }

    /**
     * @param array $ad
     *
     * @return mixed
     */
    protected function renderSpecialProductMarkup($ads)
    {
        // Parsing using simpleView in order to not duplicate code in case of view listing changes
        $markup = $this->simpleView->render(
            'chunks/category-listing-ads',
            array(
                'ads'   => $ads,
            )
        );

        return $markup;
    }

    /**
     * @param array $category_ids
     *
     * @return array
     */
    protected function getMapOfSpecialProductIdsAndCategories(array $category_ids)
    {
        // Keeps the list of ALL categories that have special products enabled in them (and which ones)
        $categories_with_specials = CategoriesSettings::getListOfAllCategoriesWithEnabledSpecialProducts();

        // If any of the categories have enabled special products, we have to
        // show those randomized ads that have those products and that belong to those categories.
        // That's why we re-arrange the list into a map of 'product_id' => array($category_id1, $category_id2) etc.
        // And then we can easily loop over them elsewhere and call getSpecialProductsMarkup() for each one
        $product_id_categories_map = array();

        foreach ($category_ids as $category_id) {
            if (isset($categories_with_specials[$category_id])) {
                foreach ($categories_with_specials[$category_id] as $product_id) {
                    $product_id_categories_map[$product_id][] = $category_id;
                }
            }
        }

        $product_id_categories_map = $this->rearrangeMapKeysIfNeeded($product_id_categories_map);

        return $product_id_categories_map;
    }

    /**
     * Re-orders the product_id_categories_map keys to match the order specified by Oglasnik in case
     * it isn't already in that order, it currently depends entirely on which products are found first when
     * iterating over the list of entire category settings list
     *
     * @param $map_array
     *
     * @return array
     */
    private function rearrangeMapKeysIfNeeded($map_array)
    {
        $wanted_order = array('platinum', 'pinky');

        // Get first key name
        reset($map_array);
        $first_key = key($map_array);

        // If first key is not the one we want it to be, rearrange
        if ($first_key !== $wanted_order[0]) {
            $new_data = array();
            foreach ($wanted_order as $key_name) {
                if (isset($map_array[$key_name])) {
                    $new_data[$key_name] = $map_array[$key_name];
                }
            }

            return $new_data;
        } else {
            return $map_array;
        }
    }

    /**
     * @param array $categoryIDs
     */
    protected function getSpecialProductsArray(array $categoryIDs, $combineSpecialProducts = false)
    {
        // Transform the passed list of category ids into a map of product ids and their categoryIDs
        $productsMap    = $this->getMapOfSpecialProductIdsAndCategories($categoryIDs);
        $specialProducts = null;

        if (!empty($productsMap)) {
            $specialProducts = array();
            // Loop over products and generate a block of specials for each one
            foreach ($productsMap as $productID => $categories) {
                $specialProducts = array_merge($specialProducts, $this->getRandomAdsWithSpecialProduct($productID, $categoryIDs, $combineSpecialProducts));
            }
        }

        return $specialProducts;
    }

    /**
     * @param array $categoryIDs
     */
    protected function getSpecialProductsArrayBySearchTerm($searchTerm, array $categoryIDs, $combineSpecialProducts = false)
    {
        $specialProducts = array();

        foreach (array('platinum', 'pinky') as $productID) {
            $ads = $this->getRandomAdsWithSpecialProductMatchingSearch($searchTerm, $productID, $categoryIDs);
            if (!empty($ads)) {
                $specialProducts = array_merge($specialProducts, $ads);
            }
        }

        return count($specialProducts) ? $specialProducts : null;
    }

    /**
     * @param array $category_ids
     */
    protected function processSpecialProductsMarkup(array $category_ids)
    {
        // Transform the passed list of category ids into a map of product ids and their category_ids
        $products_map    = $this->getMapOfSpecialProductIdsAndCategories($category_ids);
        $markup_specials = '';
        if (!empty($products_map)) {
            // Loop over products and generate a block of specials for each one
            foreach ($products_map as $product_id => $categories) {
                $markup_specials .= $this->generateSpecialProductsMarkup($product_id, $categories);
            }
        }

        return $markup_specials;
    }

    /**
     * @param $search_term
     * @param array $category_ids
     */
    protected function processSpecialProductsMarkupWithSearch($search_term, array $category_ids = array())
    {
        $markup = '';

        // FIXME/TODO: This is here just so that the model initializes and loads the cache properly
        $foo = new CategoriesSettings();

        foreach (array('platinum', 'pinky') as $product_id) {
            $ads = $this->getRandomAdsWithSpecialProductMatchingSearch($search_term, $product_id, $category_ids);
            if (!empty($ads)) {
                foreach ($ads as $ad) {
                    $markup .= $this->renderSpecialProductMarkup($ad);
                }
            }
        }

        $this->view->setVar('special_products_markup', $markup);
    }
}
