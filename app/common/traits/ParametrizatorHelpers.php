<?php

namespace Baseapp\Traits;

trait ParametrizatorHelpers
{

    protected function getParameterSubclass($type_id, $module = 'backend')
    {
        switch ($type_id) {
            case 'DEPENDABLE_DROPDOWN':
                $parameter = new \Baseapp\Library\Parameters\DependableDropdownParameter($module);
                break;

            case 'LOCATION':
                $parameter = new \Baseapp\Library\Parameters\LocationParameter($module);
                break;

            case 'TITLE':
                $parameter = new \Baseapp\Library\Parameters\TitleParameter($module);
                break;

            case 'DESCRIPTION':
                $parameter = new \Baseapp\Library\Parameters\DescriptionParameter($module);
                break;

            case 'CURRENCY':
                $parameter = new \Baseapp\Library\Parameters\CurrencyParameter($module);
                break;

            case 'UPLOADABLE':
                $parameter = new \Baseapp\Library\Parameters\UploadableParameter($module);
                break;

            case 'DATEINTERVAL':
                $parameter = new \Baseapp\Library\Parameters\DateIntervalParameter($module);
                break;

            case 'DROPDOWN':
                $parameter = new \Baseapp\Library\Parameters\DropdownParameter($module);
                break;

            case 'DATE':
                $parameter = new \Baseapp\Library\Parameters\DateParameter($module);
                break;

            case 'YEAR':
                $parameter = new \Baseapp\Library\Parameters\YearParameter($module);
                break;

            case 'MONTHYEAR':
                $parameter = new \Baseapp\Library\Parameters\MonthYearParameter($module);
                break;

            case 'NUMBER':
                $parameter = new \Baseapp\Library\Parameters\NumberParameter($module);
                break;

            case 'NUMBERINTERVAL':
                $parameter = new \Baseapp\Library\Parameters\NumberIntervalParameter($module);
                break;

            case 'RADIO':
                $parameter = new \Baseapp\Library\Parameters\RadioParameter($module);
                break;

            case 'CHECKBOX':
                $parameter = new \Baseapp\Library\Parameters\CheckboxParameter($module);
                break;

            case 'CHECKBOXGROUP':
                $parameter = new \Baseapp\Library\Parameters\CheckboxGroupParameter($module);
                break;

            case 'TEXT':
                $parameter = new \Baseapp\Library\Parameters\TextParameter($module);
                break;

            case 'TEXTAREA':
                $parameter = new \Baseapp\Library\Parameters\TextareaParameter($module);
                break;

            case 'URL':
                $parameter = new \Baseapp\Library\Parameters\UrlParameter($module);
                break;

            case 'URL_BUTTON':
                $parameter = new \Baseapp\Library\Parameters\UrlButtonParameter($module);
                break;

            case 'SPINID':
                $parameter = new \Baseapp\Library\Parameters\SpinIdParameter($module);
                break;

            case 'SPINIDGW':
                $parameter = new \Baseapp\Library\Parameters\SpinIdGwParameter($this->module);
                break;

            default:
                $parameter = null;
        }

        return $parameter;
    }

    /**
     * Helper method to setup rendered parameters markup/js/css
     *
     * @param array $data
     */
    protected function setup_rendered_parameters($data = null)
    {
        if ($data && is_array($data)) {
            // setup html markup
            if (isset($data['html'])) {
                $this->view->setVar('rendered_parameters', $data['html']);
            }

            // setup markup's specific scripts
            if (isset($data['js']) && trim($data['js'])) {
                $this->scripts['category_specific_js'] = $data['js'];

                if (isset($data['assets'])) {
                    $this->setup_assets($data['assets']);
                }
            }
        }
    }

    /**
     * Helper method to setup assets (if they are set)
     *
     * @param array $assets
     */
    protected function setup_assets($assets = null)
    {
        if ($assets && is_array($assets)) {
            if (isset($assets)) {
                if (isset($assets['js']) && count($assets['js'])) {
                    foreach ($assets['js'] as $asset_js) {
                        // Existing code defines specific assets as plain strings
                        if (is_string($asset_js)) {
                            $this->assets->addJs($asset_js);
                        }
                        /**
                         * But, when using an array, we treat the first array element
                         * as the asset url, and the second element as a list (another array) of
                         * key=>val attributes that are passed to AssetManager::addJs()
                         * and this is mainly used for external, non-filterable resources, at least
                         * for now... Had to add this to support custom data attribute for 360 spin script
                         * which requires data-gw="true" attribute, otherwise it fails to init.
                         * See common/library/Parameters/Renderer/View/SpinIdParameter.php for the array definition
                         */
                        if (is_array($asset_js)) {
                            $this->assets->addJs($asset_js[0], false, false, $asset_js[1]);
                        }
                    }
                }
                if (isset($assets['css']) && count($assets['css'])) {
                    foreach ($assets['css'] as $asset_css) {
                        $this->assets->addCss($asset_css);
                    }
                }
            }
        }
    }

}
