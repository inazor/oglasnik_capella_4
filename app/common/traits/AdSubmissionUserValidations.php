<?php

namespace Baseapp\Traits;

use Baseapp\Extension\Tag;
use Baseapp\Library\Validations\UsersAdSubmission;

trait AdSubmissionUserValidations
{

    protected $requiredUserData = array('first_name', 'last_name', 'oib');

    /**
     * Returns true if we should require the OIB before allowing/doing any other action/view.
     *
     * @return bool
     */
    protected function oibNeeded()
    {
        // Logged in users without `oib` need to have it filled in
        $user = $this->auth->get_user();
        if ($user) {
            /** @var Users $user */
            if (!$user->has_oib()) {
                return true;
            }
        }

        // Anons are allowed to proceed for now, since they'll become authenticated later anyway
        return false;
    }

    /**
     * Returns true if we should require the First/Last name before allowing/doing any other action/view.
     *
     * @return bool
     */
    protected function fullNameNeeded()
    {
        // Logged in users without `oib` need to have it filled in
        $user = $this->auth->get_user();
        if ($user) {
            /** @var Users $user */
            if (!$user->has_fullName()) {
                return true;
            }
        }

        // Anons are allowed to proceed for now, since they'll become authenticated later anyway
        return false;
    }

    /**
     * Checks and forwards to needed form/action if needed (user misses some of the required data in its profile.
     *
     * @return bool|void
     */
    protected function missingUserDataCheckRequest()
    {
        $missingUserData = true;
        $fullNameNeeded  = $this->fullNameNeeded();
        $oibNeeded       = $this->oibNeeded();
        $params          = array(
            'controller' => 'predaja-oglasa',
            'action'     => null
        );

        if (true === $fullNameNeeded && true === $oibNeeded) {
            $params['action'] = 'userDetails';
        } elseif ($oibNeeded) {
            $params['action'] = 'oib';
        } elseif ($fullNameNeeded) {
            $params['action'] = 'fullName';
        } else {
            $missingUserData = false;
        }

        if ($missingUserData) {
            // TODO: this might be dangerous!
            $this->dispatcher->forward($params);
        }

        return $missingUserData;
    }

    /**
     * If this action is executed (forwarded to) it means the User has
     * no OIB, and we shouldn't let him submit an Ad until we get the OIB
     */
    public function oibAction()
    {
        // Only allowing access to this action via forwarding for now
        if (!$this->dispatcher->wasForwarded()) {
            return $this->trigger_404();
        }

        // We return this, meaning we should allow the user to move on
        $finished = false;
        $user     = $this->auth->get_user();

        $this->tag->setTitle('Predaja oglasa - popunite OIB');

        if ($this->request->isPost()) {
            $this->check_csrf();

            // Do some validations
            $validation = new UsersAdSubmission();
            $validation->enable('oib');
            $validation = $validation->get();

            $messages = $validation->validate($this->request->getPost());
            if (count($messages)) {
                // Errors occurred, show them
                $this->flashSession->error('<strong>Ooops!</strong> Molimo ispravite uočene greške.');
                $this->view->setVar('errors', $messages);
            } else {
                // Validations passed, store the new oib and continue where we left off...
                try {
                    $result = $user->update(array('oib' => $this->request->getPost('oib')));
                    if (true !== $result) {
                        $this->flashSession->error('<strong>Ooops!</strong> Spremanje OIB-a nije uspjelo!');
                    } else {
                        // Update the session-stored User data since we've just changed it
                        $finished = true;
                        $this->auth->refresh_user();
                        $this->flashSession->success('<strong>OIB uspješno spremljen!</strong> Nastavite s predajom oglasa.');
                        return $this->dispatcher->forward(array(
                            'action' => 'index'
                        ));
                    }
                } catch (\Exception $e) {
                    Bootstrap::log($e);
                    $this->flashSession->error('<strong>Ooops!</strong> Dogodio se Exception prilikom spremanja OIB-a!');
                }
            }
        } else  {
            /**
             * TODO: we might want to display the current User's OIB here just in case (for those
             * cases where we detect an invalid OIB stored for whatever reason)
             */
            Tag::setDefault('oib', '');
        }

        $this->view->pick('chunks/ads-submission-missing-oib');
        $this->view->setVar('stepType', 'new');

        // return $finished;
    }

    /**
     * If this action is executed (forwarded to) it means the User has
     * no First/Last name, and we shouldn't let him submit an Ad until we get user's full name
     */
    public function fullNameAction()
    {
        // Only allowing access to this action via forwarding for now
        if (!$this->dispatcher->wasForwarded()) {
            return $this->trigger_404();
        }

        // We return this, meaning we should allow the user to move on
        $finished = false;
        $user     = $this->auth->get_user();

        $this->tag->setTitle('Predaja oglasa - popunite ime i prezime');

        if ($this->request->isPost()) {
            $this->check_csrf();

            // Do some validations
            $validation = new UsersAdSubmission();
            $validation->enable('fullName');
            $validation = $validation->get();

            $messages = $validation->validate($this->request->getPost());
            if (count($messages)) {
                // Errors occurred, show them
                $this->flashSession->error('<strong>Ooops!</strong> Molimo ispravite uočene greške.');
                $this->view->setVar('errors', $messages);
            } else {
                // Validations passed, store user's full name and continue where we left off...
                try {
                    $result = $user->update(array(
                        'first_name' => $this->request->getPost('first_name'),
                        'last_name'  => $this->request->getPost('last_name')
                    ));
                    if (true !== $result) {
                        $this->flashSession->error('<strong>Ooops!</strong> Spremanje imena i prezimena nije uspjelo!');
                    } else {
                        // Update the session-stored User data since we've just changed it
                        $finished = true;
                        $this->auth->refresh_user();
                        $this->flashSession->success('<strong>Ime i prezime uspješno spremljeni!</strong> Nastavite s predajom oglasa.');
                        return $this->dispatcher->forward(array(
                            'action' => 'index'
                        ));
                    }
                } catch (\Exception $e) {
                    Bootstrap::log($e);
                    $this->flashSession->error('<strong>Ooops!</strong> Dogodio se Exception prilikom spremanja imena i prezimena!');
                }
            }
        } else  {
            Tag::setDefault('first_name', $user->first_name);
            Tag::setDefault('last_name', $user->last_name);
        }

        $this->view->pick('chunks/ads-submission-missing-fullname');
        $this->view->setVar('stepType', 'new');

        // return $finished;
    }

    /**
     * If this action is executed (forwarded to) it means the User is missing multiple
     * required fields in its profile, and we shouldn't let him submit an Ad until we get all that's required
     */
    public function userDetailsAction()
    {
        // Only allowing access to this action via forwarding for now
        if (!$this->dispatcher->wasForwarded()) {
            return $this->trigger_404();
        }

        // We return this, meaning we should allow the user to move on
        $finished = false;
        $user     = $this->auth->get_user();

        $this->tag->setTitle('Predaja oglasa - popunite sve tražene podatke');

        if ($this->request->isPost()) {
            $this->check_csrf();

            // Do some validations
            $validation = new UsersAdSubmission();
            $validation->enableAll();
            $validation = $validation->get();

            $messages = $validation->validate($this->request->getPost());
            if (count($messages)) {
                // Errors occurred, show them
                $this->flashSession->error('<strong>Ooops!</strong> Molimo ispravite uočene greške.');
                $this->view->setVar('errors', $messages);
            } else {
                // Validations passed, store user's missing data and continue where we left off...
                try {
                    $userData = array();
                    foreach ($this->requiredUserData as $field) {
                        if ($fieldValue = $this->request->getPost($field, null)) {
                            $userData[$field] = $fieldValue;
                        }
                    }

                    $result = $user->update($userData);
                    if (true !== $result) {
                        $this->flashSession->error('<strong>Ooops!</strong> Spremanje traženih podataka nije uspjelo!');
                    } else {
                        // Update the session-stored User data since we've just changed it
                        $finished = true;
                        $this->auth->refresh_user();
                        $this->flashSession->success('<strong>Traženi podaci uspješno spremljeni!</strong> Nastavite s predajom oglasa.');
                        return $this->dispatcher->forward(array(
                            'action' => 'index'
                        ));
                    }
                } catch (\Exception $e) {
                    Bootstrap::log($e);
                    $this->flashSession->error('<strong>Ooops!</strong> Dogodio se Exception prilikom spremanja traženih podataka!');
                }
            }
        } else  {
            foreach ($this->requiredUserData as $field) {
                Tag::setDefault($field, $user->{$field});
            }
        }

        $this->view->pick('chunks/ads-submission-missing-data');
        $this->view->setVar('stepType', 'new');

        // return $finished;
    }

}
