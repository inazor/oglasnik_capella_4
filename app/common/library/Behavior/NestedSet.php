<?php

namespace Baseapp\Library\Behavior;

use Baseapp\Bootstrap;
use Baseapp\Library\Utils;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Mvc\Model\Exception;
use Phalcon\Mvc\ModelInterface;

//use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

/**
 * A modified/improved version of https://github.com/braska/incubator/tree/master/Library/Phalcon/Mvc/Model/Behavior
 * which is really an attempt at porting https://github.com/yiiext/nested-set-behavior to Phalcon
 *
 * It's our attempt at providing a generic, robust and performant NestedSet behavior.
 *
 * The rough idea is to use a combination of adjacency/nested-set/materialized-path solutions and
 * create a simple hierarchy/tree API on top of it all. The final goal is speeding up certain
 * "costly" tree operations (i.e., moving nodes in a large tree, in a naive/traditional nested-set
 * implementation).
 * 
 * We've since diverged from a basic fork and have bolted things such as $this->parentAttribute and more...
 *
 * @package Baseapp\Library\Behavior
 */
class NestedSet extends Behavior implements BehaviorInterface
{
    private $debug_print = false;

    private $owner;
    private $hasManyRoots = false;
    private $rootAttribute = 'root';
    private $leftAttribute = 'lft';
    private $rightAttribute = 'rgt';
    private $levelAttribute = 'level';
    private $parentAttribute = 'parent';
    private $pathAttribute = 'path';
    private $pathSeparator = ' › ';
    private $primaryKey = 'id';
    private $ignoreEvent = false;
    private $useTransactions = true;

    private $deleted = false;

    public function __construct($options = null)
    {
        if (isset($options['hasManyRoots'])) {
            $this->hasManyRoots = (bool) $options['hasManyRoots'];
        }

        if (isset($options['rootAttribute'])) {
            $this->rootAttribute = $options['rootAttribute'];
        }

        if (isset($options['leftAttribute'])) {
            $this->leftAttribute = $options['leftAttribute'];
        }

        if (isset($options['rightAttribute'])) {
            $this->rightAttribute = $options['rightAttribute'];
        }

        if (isset($options['parentAttribute'])) {
            $this->parentAttribute = $options['parentAttribute'];
        }

        if (isset($options['pathAttribute'])) {
            $this->pathAttribute = $options['pathAttribute'];
        }

        if (isset($options['levelAttribute'])) {
            $this->levelAttribute = $options['levelAttribute'];
        }

        if (isset($options['primaryKey'])) {
            $this->primaryKey = $options['primaryKey'];
        }

        if (isset($options['useTransactions'])) {
            $this->useTransactions = $options['useTransactions'];
        }
    }

    public function setIgnoreEvent($value)
    {
        $this->ignoreEvent = (bool) $value;
    }

    public function getIgnoreEvent()
    {
        return $this->ignoreEvent;
    }

    private function debug_print($msg)
    {
        if ($this->debug_print) {
            echo "\n" . $msg;
        }
    }

    /**
     * @param string $eventType
     * @param ModelInterface $model
     *
     * @throws Exception
     */
    public function notify($eventType, ModelInterface $model)
    {
        switch ($eventType) {
            case 'beforeCreate':
            case 'beforeDelete':
            case 'beforeUpdate':
                if (!$this->ignoreEvent) {
                    $changed_fields = $model->getChangedFields();
                    if (!empty($changed_fields)) {
                        throw new Exception(
                            sprintf(
                                'You should not use `%s::%s` when `%s` is attached. Use the methods of behavior.',
                                get_class($model),
                                $eventType,
                                __CLASS__
                            )
                        );
                    }
                }
                break;
        }
    }

    /**
     * Calls a method when it's missing in the model
     *
     * @param ModelInterface $model
     * @param string $method
     * @param null $arguments
     * @return mixed|null|string
     * @throws Exception
     */
    public function missingMethod(ModelInterface $model, $method, $arguments = null)
    {
        if (!method_exists($this, $method)) {
            return null;
        }

        // $this->getDbHandler($model);
        $this->setOwner($model);

        return call_user_func_array(array($this, $method), $arguments);
    }

    /**
     * @return ModelInterface
     */
    public function getOwner()
    {
        if (!$this->owner instanceof ModelInterface) {
            trigger_error("Owner isn't a valid ModelInterface instance.", E_USER_WARNING);
        }

        return $this->owner;
    }

    public function setOwner(ModelInterface $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return bool
     */
    public function hasValidOwner()
    {
        $owner = $this->getOwner();

        return isset($owner->{$this->primaryKey});
    }

/*
    public function refreshOwner()
    {
        if (!$this->hasValidOwner() || static::$actions_performed === 0) {
            return;
        }

        // TODO/FIXME: calling $owner->refresh() often returns completely wrong
        // results for some reason...

        $owner = $this->getOwner();
        $owner->refresh();
    }
*/

    public function getIsNewRecord()
    {
        return $this->getOwner()->getDirtyState() === Model::DIRTY_STATE_TRANSIENT;
    }

    /**
     * Returns the current node's deleted flag
     * @return boolean Whether the node is deleted
     */
    public function getIsDeletedRecord()
    {
        return $this->deleted;
    }

    /**
     * Sets the current node's deleted flag
     * @param boolean $value Whether the node should be marked as deleted
     */
    public function setIsDeletedRecord($value)
    {
        $this->deleted = $value;
    }

    /**
     * Determines if node is a leaf
     * @return boolean Whether the node is leaf
     */
    public function isLeaf()
    {
        $owner = $this->getOwner();

        return (1 === $owner->{$this->rightAttribute} - $owner->{$this->leftAttribute});
    }

    /**
     * Returns true if the node is a root node
     * @return boolean
     */
    public function isRoot()
    {
        return (1 === $this->getOwner()->{$this->leftAttribute});
    }

    /**
     * Returns true if the current node is a descendant of $subj
     *
     * @param ModelInterface $subj the subject node
     * @return boolean Whether the node is descendant of $subj
     */
    public function isDescendantOf($subj)
    {
        $owner = $this->getOwner();
        $result = ($owner->{$this->leftAttribute} > $subj->{$this->leftAttribute})
            && ($owner->{$this->rightAttribute} < $subj->{$this->rightAttribute});

        if ($this->hasManyRoots) {
            $result = $result && ($owner->{$this->rootAttribute} === $subj->{$this->rootAttribute});
        }

        return $result;
    }

    /**
     * Returns true if the current node is an ancestor of $subj
     *
     * @param ModelInterface $subj
     * @return bool
     */
    public function isAncestorOf($subj)
    {
        $owner = $this->getOwner();
        $result = ($owner->{$this->leftAttribute} < $subj->{$this->leftAttribute})
            && ($owner->{$this->rightAttribute} > $subj->{$this->rightAttribute});

        if ($this->hasManyRoots) {
            $result = $result && ($owner->{$this->rootAttribute} === $subj->{$this->rootAttribute});
        }

        return $result;
    }

/*
    public function isDescendant($ancestor)
    {
        if (!$this->hasValidOwner()) {
            throw new Exception('Model does not exist');
        }

        $owner = $this->getOwner();
        $path = $owner->getPath();
        $ancestor_path = $ancestor->getPath();

        return strpos($path, $ancestor_path . $ancestor->{$this->primaryKey} . $this->pathSeparator) !== false and $ancestor_path !== $path;
    }

    public function isAncestor($descendant)
    {
        if (!$this->hasValidOwner()) {
            throw new Exception('Model does not exist');
        }
        $owner = $this->getOwner();
        $path = $owner->getPath();
        $descendant_path = $descendant->getPath();

        return strpos($descendant_path, $path . $owner->{$this->primaryKey} . $this->pathSeparator) !== false and $descendant_path !== $path;
    }
*/

    /**
     * Named scope. Gets descendants of a node (with an optional depth)
     * @param int $depth
     * @return ResultsetInterface
     */
    public function descendants($depth = null)
    {
        $owner = $this->getOwner();

        $query = $owner::query()
            ->where($this->leftAttribute . ' > ' . $owner->{$this->leftAttribute})
            ->andWhere($this->rightAttribute . ' < ' . $owner->{$this->rightAttribute})
            ->orderBy($this->leftAttribute);

        if (null !== $depth) {
            $query = $query->andWhere($this->levelAttribute . ' <= ' . ($owner->{$this->levelAttribute} + $depth));
        }

        if ($this->hasManyRoots) {
            $query = $query->andWhere($this->rootAttribute . ' = ' . $owner->{$this->rootAttribute});
        }

        return $query->execute();
    }

    /**
     * This is basically the descendants() method, but written in a different way
     * in order to have control over results hydration and the SQL conditions
     * required to support including the owning node in the results.
     *
     * @param ModelInterface $owner
     * @param bool $include_self
     * @param int|null $depth
     *
     * @return \Phalcon\Mvc\Model\Resultset | false
     */
    protected function queryOwnerTree($owner, $include_self = true, $depth = null)
    {
        $params['hydration'] = Model\Resultset::HYDRATE_OBJECTS;

        $lft_cmp = '>';
        $rgt_cmp = '<';
        if ($include_self) {
            $lft_cmp = '>=';
            $rgt_cmp = '<=';
        }

        $params['order'] = $this->leftAttribute;
        $params['conditions'] = $this->leftAttribute . ' ' . $lft_cmp . ' ' . $owner->{$this->leftAttribute};
        $params['conditions'] .= ' AND ' . $this->rightAttribute . ' ' . $rgt_cmp . ' ' . $owner->{$this->rightAttribute};

        if (null !== $depth) {
            $depth = (int) $depth;
            $params['conditions'] .= ' AND ' . $this->levelAttribute . ' <= ' . ($owner->{$this->levelAttribute} + $depth);
        }

        if ($this->hasManyRoots) {
            $params['conditions'] .= ' AND ' . $this->rootAttribute . ' = ' . $owner->{$this->rootAttribute};
        }

        $nodes = $owner::find($params);
        return $nodes;
    }

    /**
     * Returns the resultset containing all the nodes in the tree starting
     * from the root. If multi root mode is on, the results are ordered
     * by root_id, lft instead of just lft.
     *
     * @return \Phalcon\Mvc\Model\Resultset | false
     */
    protected function queryNodesAll()
    {
        $owner = $this->getOwner();
        $params['hydration'] = Model\Resultset::HYDRATE_OBJECTS;
        $params['order'] = $this->hasManyRoots
            ? $this->rootAttribute . ', ' . $this->leftAttribute
            : $this->leftAttribute;
        $nodes = $owner::find($params);
        return $nodes;
    }

    /**
     * Returns the resultset containing all the nodes belonging to the
     * tree specified by that tree's $root_id
     *
     * @param int $root_id
     *
     * @return \Phalcon\Mvc\Model\Resultset | false
     */
    protected function queryNodesWithRoot($root_id)
    {
        $owner = $this->getOwner();
        $params['hydration'] = Model\Resultset::HYDRATE_OBJECTS;
        $params['conditions'] = $this->rootAttribute . ' = ' . (int) $root_id;
        $params['order'] = $this->leftAttribute;
        $nodes = $owner::find($params);
        return $nodes;
    }

    /**
     * Returns the resultset matching the node's ancestors (path to node).
     *
     * @param null|ModelInterface $owner
     * @param bool $with_self
     * @param int|null $depth
     *
     * @return \Phalcon\Mvc\Model\Resultset | false
     */
    protected function queryNodeAncestors($owner = null, $with_self = true, $depth = null)
    {
        if (!($owner instanceof ModelInterface)) {
            if ($this->hasValidOwner()) {
                $owner = $this->getOwner();
            }
        }

        if (null !== $depth) {
            $depth = (int) $depth;
        }

        $params = array();
        $params['hydration'] = Model\Resultset::HYDRATE_OBJECTS;

        $lft_cmp = '<';
        $rgt_cmp = '>';
        if ($with_self) {
            $lft_cmp = '<=';
            $rgt_cmp = '>=';
        }

        $params['order'] = $this->levelAttribute;

        $params['conditions'] = $this->leftAttribute . ' ' . $lft_cmp . ' ' . $owner->{$this->leftAttribute};
        $params['conditions'] .= ' AND ' . $this->rightAttribute . ' ' . $rgt_cmp . ' ' . $owner->{$this->rightAttribute};

        if (null !== $depth) {
            $params['conditions'] .= ' AND ' . $this->levelAttribute . ' >= ' . ($owner->{$this->levelAttribute} - $depth);
        }

        // Constrain the query to a single root in multi-root mode
        if ($this->hasManyRoots) {
            $params['conditions'] .= ' AND ' . $this->rootAttribute . ' = ' . $owner->{$this->rootAttribute};
        }

        $nodes = $owner::find($params);
        return $nodes;
    }

    /**
     * @param null|ModelInterface $owner
     * @param bool $self_included
     * @param null $depth
     *
     * @return array
     */
    public function getNodePath($owner = null, $self_included = true, $depth = null)
    {
        // Shift args if first param is a boolean
        if (is_bool($owner)) {
            $depth = $self_included;
            $self_included = $owner;
            $owner = null;
        }

        $nodes = $this->queryNodeAncestors($owner, $self_included, $depth);
        // $tree = $this->build_tree_array($nodes);

        $path = array();
        foreach ($nodes as $node) {
            $path[$node->{$this->primaryKey}] = $node;
        }

        return $path;
    }

/*
    public function getPath()
    {
        $owner = $this->getOwner();

        return $owner->{$this->pathAttribute};
    }
*/
/*
    public function getPath($owner = null, $self_included = true, $depth = null)
    {
        return $this->getNodePath($owner, $self_included, $depth);
    }
*/

    /**
     * Returns the tree array representing the tree structure of the $owner node.
     * Includes the node itself by default in the resulting tree.
     *
     * If no owner is specified, it assumes the "current" owner/node in the sense of
     * the object that this behavior is currently attached to.
     *
     * @param ModelInterface | null $owner
     * @param bool $self_included
     * @param int $depth
     *
     * @return array|null
     */
    protected function getOwnerTree($owner = null, $self_included = true, $depth = null)
    {
        if (!($owner instanceof ModelInterface)) {
            if ($this->hasValidOwner()) {
                $owner = $this->getOwner();
            }
        }

        $nodes = $this->queryOwnerTree($owner, $self_included, $depth);
        $tree = $this->build_tree_array($nodes);

        return $tree;
    }

    /**
     * Returns the tree array representing the tree structure.
     *
     * If $root_id is specified, it can return only that specific tree's structure in
     * case of a multi root tree. If no $root id is provided and a multi root tree is used,
     * it will return all the available trees.
     *
     * If no $root_id is specified, we check if there's a valid owner (existing node),
     * and return that node's tree (along with the node itself if $self_included is true).
     *
     * The idea being that whenever you need a tree, you just call this one getTree() method,
     * and it automagically gives you back what you'd expect regardless of the underlying
     * tree implementation.
     *
     * @param null $root_id
     * @param bool $self_included
     *
     * @return array|null
     */
    public function getTree($root_id = null, $self_included = true)
    {
        /**
         * Checking if the first argument is being used as boolean, meaning
         * we're supposed to treat $root_id as though it's null.
         * This is purely an API "convenience", allowing getTree(true/false) calls on
         * a Model::findFirst() result and similar...
         */
        if (is_bool($root_id)) {
            $self_included = $root_id;
            $root_id = null;
        }

        // If no root_id is specified, check if we're in "scope" usage
        if (null === $root_id) {
            if ($this->hasValidOwner()) {
                // If a valid owner exist, return the owner's tree
                $nodes = $this->queryOwnerTree($this->getOwner(), $self_included);
            } else {
                // Return all the available trees (if there are many) or just a single entire tree
                $nodes = $this->queryNodesAll();
            }
        } else {
            $nodes = $this->queryNodesWithRoot($root_id);
        }

        return $this->build_tree_array($nodes);
    }

    /**
     * Returns all trees available in the model (in case there are many).
     * In single root mode just that one (whole) tree is returned.
     *
     * @return array|null
     */
    public function getTrees()
    {
        return $this->build_tree_array($this->queryNodesAll());
    }

    /**
     * The entire tree is represented as a flat array containing all the nodes AND as a hierarchy,
     * where each node's children are available.
     *
     * The first array element is always present, and is representing the "root".
     * That first element's 'children' property contains an array representing the
     * hierarchy of the returned tree.
     *
     * This method creates that array from the passed Resultset and returns it.
     *
     * @param Model\Resultset $nodes
     *
     * @return array|null
     */
    protected function build_tree_array(\Phalcon\Mvc\Model\Resultset $nodes)
    {
        $tree = null;

        if ($nodes) {

            // Creating a single fake root node which acts as a helper/container holding
            // children of all first level items
            $root = new \stdClass();
            $root->{$this->primaryKey} = 0;
            $root->{$this->levelAttribute} = 0;
            $root->{$this->parentAttribute} = null;
            $root->is_root = true;
            $root->is_leaf = false;
            $root->top_parent_id = null;
            $root->children = array();

            $tree = array($root);
            $top_parent_id_for_categories = array();

            $get_top_parent_id = 
                function($id, $parent_id) use (&$top_parent_id_for_categories) {
                    $top_parent_id = null;

                    if ($parent_id) {
                        if (1 === (int) $parent_id) {
                            $top_parent_id = (int) $id;
                        } elseif (isset($top_parent_id_for_categories[$parent_id])) {
                            $top_parent_id = $top_parent_id_for_categories[$parent_id];
                        }

                        $top_parent_id_for_categories[$id] = $top_parent_id;
                    }

                    return $top_parent_id;
                };

            // Build the initial array structure for each node of the tree keyed by primary key values
            foreach ($nodes as $row) {
                $id = (int) $row->{$this->primaryKey};
                $tree[$id] = $row;
                // TODO: $tree[$id]->parents = array(); and build that out too? do we need it?
                $tree[$id]->children = array();
                // This way we have some flags on each node regardless of hasManyRoot
                // and we can check them inside getSelectablesFromTreeArray() and any
                // other places we might need that info...
                $tree[$id]->is_root = (1 === (int) $row->{$this->leftAttribute});
                $tree[$id]->is_leaf = (1 === (int) ($row->{$this->rightAttribute} - $row->{$this->leftAttribute}));

                $top_parent_id = isset($row->{$this->parentAttribute}) ? $get_top_parent_id($id, (int) $row->{$this->parentAttribute}) : null;
                $tree[$id]->top_parent_id = $top_parent_id;

                $json = null;
                if (isset($tree[$id])) {
                    if (isset($tree[$id]->json) && $tree[$id]->json) {
                        $json = json_decode($tree[$id]->json);
                    }
                }
                $tree[$id]->json = $json;

                // TODO: what other "flags" might we need here that are (relatively) easy to calculate?
            }
            unset($row, $id);

            // Now iterate over the initial tree array and populate children data
            // based on each node's parent_id field (if it exists)
            foreach ($tree as $id => $row) {
                if (isset($row->{$this->parentAttribute})) {
                    // We have a parent field attribute (holding the id of this node's parent node)
                    // so we can populate that parent node's children array with the current node
                    $parent_id = (int) $row->{$this->parentAttribute};
                    if ($parent_id >= 0) {
                        /**
                         * NOTE:
                         * It's possible to have queried for nodes without roots, but those roots are actually
                         * pointed to by the parent_id attribute. And we don't get those nodes in the first
                         * loop above at all (and/or from the DB query really). So in those cases, we'll only
                         * setup those children that we can.
                         * But that means that the tree (children) is not really completely built.
                         * And that could be dangerous if someone decides to build a new tree
                         * using such "incomplete" children data. You have been warned.
                         */
                        if (isset($tree[$parent_id])) {
                            $tree[$parent_id]->children[$id] = $row;
                        } else {
                            // We don't have the node pointed to by the parent_id at all. We could fetch it, but why?
                            // The tree was explicitly requested without roots/selfs in that case?
                            // TODO: If we decide to start to "partially" fill only the children array,
                            // further checks will have to be made in to_hierarchy() to account
                            // for these "partial" tree array elements which only hold children really...
                            // But they could come in handy to produce the entire tree structure in $tree[0]->children?
                            // $tree[$parent_id]->children[$id] = $row;
                        }
                    }
                }
            }
            unset($row, $id);

            // Iterate again and build path data for each node (based on parent id, without new queries)
            $path_options = array(
                'with_self' => true,
                'as_string' => false
            );
            foreach ($tree as $id => &$node) {
                $path                = $this->getNodePathFromTreeArray($tree, $node, $path_options);
                $path_text_with_root = implode($this->pathSeparator, array_values($path));

                // Removing the root element
                $path_copy = $path;
                array_shift($path_copy);
                $path_text_without_root = implode($this->pathSeparator, array_values($path_copy));

                // look for fancy name in node's json field first
                if (isset($node->json->fancy_name)) {
                    $fancy_name = $node->json->fancy_name;
                } else {
                    // Build out fancy category display name which must include the parent's name
                    // if it exists and if the current node has a transaction_type_id set
                    $fancy_name = '';

                    // Name thyself, demon!
                    if (isset($node->name)) {
                        $fancy_name = $node->name;
                    }

                    // Skip over the absolute root node which doesn't have this data
                    if (isset($node->transaction_type_id)) {
                        // Fetch parent node and prefix its name if needed
                        if ($node->transaction_type_id) {
                            $parent_id = $node->{$this->parentAttribute};
                            if ($parent_id > 1) {
                                $parent_node = isset($tree[$parent_id]) ? $tree[$parent_id] : null;
                                if ($parent_node) {
                                    $fancy_name = $parent_node->name . ' ' . $fancy_name;
                                }
                            }
                        }
                    }
                }

                $node->path = array(
                    'data' => $path,
                    'text' => $path_text_without_root,
                    'text_with_root' => $path_text_with_root,
                    'fancy_name' => $fancy_name
                );

                // Overwrite the modified node
                $tree[$id]        = $node;
            }

            // Populating tree[0]->children array with the entire tree hierarchy
            $hierarchy = $this->to_hierarchy($tree);
            // Copy that hierarchy into the absolute root element's children array
            $tree[0]->children = $hierarchy[0]->children;
            unset($hierarchy);
        }

        return $tree;
    }

    /**
     * Builds a hierarchy from a node list using iteration (not recursion).
     * Each item/node will have a 'children' property set (an array containing the item/node's child nodes).
     *
     * @param $collection
     *
     * @return array
     */
    public function to_hierarchy($collection)
    {
        // Trees mapped
        $trees = array();

        if (count($collection) > 0) {
            // Node Stack. Used to help building the hierarchy
            $stack = array();

            foreach ($collection as $node) {
                // Skip "fake" root node
                if ($node->{$this->primaryKey} <= 0) {
                    continue;
                }

                $item = $node;
                $item->children = array();

                // Number of stack items
                $l = count($stack);

                // Are we dealing with different levels?
                while ($l > 0 && $stack[$l - 1]->{$this->levelAttribute} >= $item->{$this->levelAttribute}) {
                    array_pop($stack);
                    $l--;
                }

                // Stack is empty (we are inspecting the root)
                if (0 === $l) {
                    // Assigning the root node
                    $i = count($trees);
                    $trees[$i] = $item;
                    $stack[] = & $trees[$i];
                } else {
                    // Add node to parent
                    $prev_l = $l - 1;
                    /*
                    $i = count($stack[$prev_l]->children);
                    $stack[$prev_l]->children[$i] = $item;
                    $stack[] = & $stack[$prev_l]->children[$i];
                    */
                    // Keying the children array based on node id
                    $id = $item->{$this->primaryKey};
                    $stack[$prev_l]->children[$id] = $item;
                    $stack[] = & $stack[$prev_l]->children[$id];
                }
            }
        }

        return $trees;
    }
/*
    public function compute_parent_ids_sql()
    {
        $owner = $this->getOwner();
        $source = $owner->getSource();
        $sql = <<<SQL
SELECT
  A.id, IF(B.id IS NULL, 1, B.id) AS parent_id
FROM
  $source AS A
  LEFT OUTER JOIN $source AS B
    ON B.lft = (SELECT MAX(C.lft) FROM $source AS C WHERE A.lft > C.lft AND A.lft < C.rght);
SQL;
        $db = $owner->getDI()->get('db');
        $result = $db->query($sql);
        $result->setFetchMode(\Phalcon\Db::FETCH_OBJ);

        var_dump($result->fetchAll());
        exit;
    }
*/

    /**
     * Returns a single-dimensional array filled with a 'tree like' structure so it can
     * be used in select elements or printed out
     *
     * @param string $prefix Prefix to prepend to all nodes based on the level they belong to
     * @param mixed $depth Depth we want to display
     * @param bool $includeRoot
     * @return array One-dimensional array with a 'tree like' structure
     */
    public function getSelectValues($prefix = '--', $depth = null, $includeRoot = true)
    {
        $values = array();
        $prefix_removes = 2;
        $owner = $this->getOwner();
        $descendants = $owner->descendants($depth);

        if ($includeRoot) {
            $values[$owner->{$this->primaryKey}] = $owner->name;
            $prefix_removes = 1;
        }

        foreach ($descendants as $descendant) {
            $display = trim(str_repeat($prefix, $descendant->{$this->levelAttribute} - $prefix_removes) . ' ' . $descendant->name);
            $values[$descendant->{$this->primaryKey}] = $display;
        }

        return $values;
    }

    /**
     * A wrapper around getSelectablesFromTreeArray() massaging array arguments provided
     * to it and ensuring a tree array is built if needed (before calling the wrapped method).
     *
     * It's envisioned as a convenience method similar to getTree() in the sense of having
     * unified usage and being universally callable (and returning the "expected" results
     * regardless of the underlying tree structure/implementation).
     *
     * @param array $options    Options array, defaults:
     *                          ['source' => null, 'prefix' => '--', 'with_root' => true, 'depth' => null]
     *
     * @return array|null
     */
    public function getSelectables($options = array())
    {
        $selectables = null;
        $tree_array = null;

        $defaults = array(
            'source' => null,
            'prefix' => '--',
            'with_root' => true,
            'depth' => null,
            'breadcrumbs' => false
        );
        $options = array_merge($defaults, $options);

        // Make sure we have our tree array built according to the $source
        if (empty($options['source'])) {
            if ($this->hasValidOwner()) {
                $options['source'] = $this->getOwner();
            }
        }

        // Create/query for a tree array if source is a model/node instance
        if ($options['source'] instanceof ModelInterface) {
            // Source is an existing node instance, get that node's tree structure
            $options['source'] = $this->getOwnerTree($options['source'], $options['with_root'], $options['depth']);
            // Since the tree has already been depth limited, reset $options['depth']
            // back to default
            $options['depth'] = $defaults['depth'];
        }

        if (is_array($options['source'])) {
            $selectables = $this->getSelectablesFromTreeArray(
                $options['source'],
                $options['prefix'],
                $options['with_root'],
                $options['depth'],
                $options['breadcrumbs']
            );
        }

        return $selectables;
    }

    public function getSubtree($tree_array, $isChild = false, $levelCorrection = 0)
    {
        $subtree = array();

        if (!$isChild) {
            $levelCorrection = $tree_array->level - 1;
        }

        if (is_array($tree_array)) {
            foreach($tree_array as $treeID => $treeNode) {
                $subTreeArray = $this->getSubtree($treeNode, true, $levelCorrection);
                foreach ($subTreeArray as $treeID => $tree) {
                    $subtree[$treeID] = $tree;
                }
            }
        } else {
            $subtree[$tree_array->id] = $tree_array;
            if (!$isChild) {
                $subtree[$tree_array->id]->is_root = true;
            }
            $subtree[$tree_array->id]->level = $subtree[$tree_array->id]->level - $levelCorrection;
            if (isset($tree_array->children) && count($tree_array->children)) {
                $subTreeArray = $this->getSubtree($tree_array->children, true, $levelCorrection);
                foreach ($subTreeArray as $treeID => $tree) {
                    $subtree[$treeID] = $tree;
                }
            }
        }

        return $subtree;
    }

    /**
     * Returns a single-dimensional array of tree nodes with their names and primary key values.
     * The array is keyed by the node's primary key values.
     * The value is the node's name prefixed with the optional prefix (repeated by the node's level/depth).
     *
     * Useful for generating HTML dropdowns (<select>s containing these <option>s).
     *
     * @param array $tree_array The array created by build_tree_array()
     * @param string $prefix Prefix that's prepended to the node's name (repeated n times, where n is the node's level)
     * @param bool $with_root Should root nodes be included in the output or not
     * @param int|null $depth
     * @param null|bool|array $breadcrumbs Breadcrumbs options (if desired, or a hash of options to customize them,
     *                                     forwarded to getNodePathFromTreeArray()'s $options parameter)
     *
     * @return array
     */
    public function getSelectablesFromTreeArray($tree_array, $prefix = '--', $with_root = true, $depth = null, $breadcrumbs = false, $skip_disabled = false)
    {
        $selectables = array();

        if (null !== $depth) {
            $depth = (int) $depth;
        }

        // Process potential breadcrumbs options
        $breadcrumbs_default_options = array(
            'with_self' => false,
            'as_string' => true
        );
        $breadcrumbs_options = $breadcrumbs_default_options;
        if ($breadcrumbs) {
            if (is_array($breadcrumbs)) {
                $breadcrumbs_options = array_merge($breadcrumbs_default_options, $breadcrumbs);
                $breadcrumbs = true;
            }
        }

        if (count($tree_array) > 0) {

            // Filter out the fake root node always + actual root nodes if requested
            $tree_array = Utils::array_where($tree_array, function ($key, $value) use ($with_root, $depth) {
                $keep = false; // drop by default
                if ($key > 0) {
                    $keep = true; // keep all non-fake-root nodes by default
                    if (!$with_root && $value->is_root) {
                        // root nodes explicitly requested to not be included
                        $keep = false;
                    }

                    // Filter based on depth/level
                    if ($keep && $depth) {
                        $level = $value->{$this->levelAttribute};
                        if ($level > $depth) {
                            $keep = false;
                        }
                    }

                }
                return $keep;
            });

            foreach ($tree_array as $id => $node) {
                if ($skip_disabled && $node->active == 0) {
                    continue;
                }

                $node_level = (int) $node->{$this->levelAttribute};
                $root_prefix_correction = 0;
                if (!$with_root) {
                    $root_prefix_correction = 1;
                }
                $display = trim(str_repeat($prefix, ($node_level - 1 - $root_prefix_correction)) . ' ' . $node->name);
                $pk = $node->{$this->primaryKey};
                $selectables[$pk] = $display;
                // If breadcrumbs are desired, populate an array which our modified Tag::select() handles
                if ($breadcrumbs) {
                    $data = array();
                    $data['text'] = $display;

                    $node_path = $this->getNodePathFromTreeArray(
                        $tree_array,
                        $node,
                        $breadcrumbs_options
                    );

                    $attribs = array();
                    if (!empty($node_path)) {
                        $attribs = array(
                            'data-subtext' => $node_path,
                            'data-level' => $node->level
                        );
                    }

                    if (!empty($attribs)) {
                        $data['attributes'] = $attribs;
                    }

                    $selectables[$pk] = $data;
                }
            }
            unset($id, $node);
        }

        return $selectables;
    }

    public function getNodePathFromTreeArray(&$tree_array, $node, $options = array())
    {
        $defaults = array(
            'with_self' => false,
            'as_string' => false,
        );
        $options = array_merge($defaults, $options);

        // TODO: do we treat root nodes differently? why? why not?
        /*
        if ($node->is_root) {
        }

        if ($options['skip_root_nodes']) {
            if ($node->is_root) {
                return '';
            }
        }
        */

        $path = array();

        $tree_node = $tree_array[$node->{$this->primaryKey}];
        $parent_id = $tree_node->{$this->parentAttribute};

        if ($options['with_self']) {
            // Root/force-inserted stdClass nodes don't have this property
            if (isset($tree_node->name)) {
                $path[] = $tree_node->name;
            }
        }

        while ($parent_id > 0) {
            if (isset($tree_array[$parent_id])) {
                $tree_node = $tree_array[$parent_id];
                $path[]    = $tree_node->name;
                $parent_id = $tree_node->{$this->parentAttribute};
            } else {
                // Bail if no node with such a parent exists?
                $parent_id = 0;
            }
        }

        $path = array_reverse($path);

        if ($options['as_string']) {
            $path = implode($this->pathSeparator, array_values($path));
        }

        return $path;
    }

    /**
     * Named scope. Returns the node's immediate children (direct descendants only)
     * @return ResultsetInterface
     */
    public function children()
    {
        return $this->descendants(1);
    }

    /**
     * Named scope. Returns node's ancestors
     * @param int $depth
     * @return ResultsetInterface
     */
    public function ancestors($depth = null)
    {
        $owner = $this->getOwner();

        $query = $owner::query()
            ->where($this->leftAttribute . ' < ' . $owner->{$this->leftAttribute})
            ->andWhere($this->rightAttribute . ' > ' . $owner->{$this->rightAttribute})
            ->orderBy($this->leftAttribute);

        if (null !== $depth) {
            $query = $query->andWhere($this->levelAttribute . ' >= ' . ($owner->{$this->levelAttribute} - $depth));
        }

        if ($this->hasManyRoots) {
            $query = $query->andWhere($this->rootAttribute . ' = ' . $owner->{$this->rootAttribute});
        }

        return $query->execute();
    }

    /**
     * Named scope. Returns root node(s).
     * Allows for basic searching if you provide extra (PH/S)QL conditions as the first parameter.
     * If using bound parameters within the passed conditions you must provide the key/value pairs in $binds.
     *
     * @param null|string $condition Optional. Extra (PH/S)QL conditions. Defaults to `$this->leftAttribute = 1`
     * @param array $binds Optional. Bind parameters (if used within $condition, ignored otherwise)
     * @param array $extras Optional. array('order' => 'id DESC') for example. Merged with existing find parameters.
     *
     * @return Model\ResultsetInterface
     */
    public function roots($condition = null, $binds = array(), $extras = array())
    {
        $owner = $this->getOwner();

        $find_params = array();
        $find_params['conditions'] = $this->leftAttribute . ' = 1';

        // If a custom $condition is specified, add it to our default
        if (null !== $condition) {
            if (!empty($condition)) {
                $find_params['conditions'] .= ' AND ' . $condition;
                if (!empty($binds)) {
                    $find_params['bind'] = $binds;
                }
            }
        }

        if (!empty($extras)) {
            $find_params = array_merge($find_params, $extras);
        }

        return $owner::find($find_params);
    }

    /**
     * Named scope. Returns node's root
     *
     * @return ModelInterface
     * @throws Exception
     */
    public function root()
    {
        if (!$this->hasManyRoots) {
            throw new Exception('Many roots mode is off.');
        }

        $owner = $this->getOwner();
        return $owner::findFirst($owner->{$this->rootAttribute});
    }

    /**
     * Returns the node's root node if a valid existing node is present.
     *
     * Without an existing node it returns:
     *     - In single root mode it returns the root node of the entire tree
     *     - In multi root mode either a list of all root nodes or a specific root node if specified by $root_id
     *
     * @param null $root_id
     *
     * @return Model\ResultsetInterface|ModelInterface
     */
    public function getRoot($root_id = null)
    {
        $model = $this->getOwner();
        if ($this->hasValidOwner()) {
            // We have an owner, return that owner's root
            return $model::findFirst($model->{$this->rootAttribute});
        } else {
            // No owner, return the best we can depending on mode
            if ($this->hasManyRoots) {
                // Multi root mode
                if (null === $root_id) {
                    // No root specified, return them all
                    return $model::find($this->leftAttribute . ' = 1');
                } else {
                    // Return a specific root from a multi root tree
                    return $model::findFirst((int) $root_id);
                }
            } else {
                // Single root mode, just return the absolute root node
                return $model::findFirst($this->leftAttribute . ' = 1');
            }
        }
    }

    /**
     * Named scope. Returns node's parent
     *
     * @return ModelInterface
     */
    public function parent()
    {
        $owner = $this->getOwner();

        $query = $owner::query()
            ->where($this->leftAttribute . ' < ' . $owner->{$this->leftAttribute})
            ->andWhere($this->rightAttribute . ' > ' . $owner->{$this->rightAttribute})
            ->orderBy($this->rightAttribute)
            ->limit(1);

        if ($this->hasManyRoots) {
            $query = $query->andWhere($this->rootAttribute . ' = ' . $owner->{$this->rootAttribute});
        }

        return $query->execute()->getFirst();
    }

    /**
     * Named scope. Returns node's previous sibling
     * @return ModelInterface
     */
    public function prev()
    {
        $owner = $this->getOwner();
        $query = $owner::query()->where($this->rightAttribute . ' = ' . ($owner->{$this->leftAttribute} - 1));

        if ($this->hasManyRoots) {
            $query = $query->andWhere($this->rootAttribute . ' = ' . $owner->{$this->rootAttribute});
        }

        return $query->execute()->getFirst();
    }

    /**
     * Named scope. Returns node's next sibling
     * @return ModelInterface
     */
    public function next()
    {
        $owner = $this->getOwner();
        $query = $owner::query()->where($this->leftAttribute . ' = ' . ($owner->{$this->rightAttribute} + 1));

        if ($this->hasManyRoots) {
            $query = $query->andWhere($this->rootAttribute . ' = ' . $owner->{$this->rootAttribute});
        }

        return $query->execute()->getFirst();
    }

    /**
     * Prepends node to target as first child
     *
     * @param ModelInterface $target The target node
     * @param array $attributes List of attributes
     * @return boolean Whether the prepend succeeded
     */
    public function prependTo($target, $attributes = null)
    {
        return $this->addNode($target, $target->{$this->leftAttribute} + 1, 1, $attributes);
    }

    /**
     * Prepends target to node as first child
     *
     * @param ModelInterface $target The target node
     * @param array $attributes List of attributes
     *
     * @return boolean Whether the prepend succeeded
     */
    public function prepend($target, $attributes = null)
    {
        return $target->prependTo($this->getOwner(), $attributes);
    }

    /**
     * Appends node to target as last child
     *
     * @param ModelInterface $target The target node
     * @param array $attributes List of attributes
     *
     * @return boolean Whether the appending succeeded
     */
    public function appendTo($target, $attributes = null)
    {
        return $this->addNode($target, $target->{$this->rightAttribute}, 1, $attributes);
    }

    /**
     * Appends target to node as last child
     *
     * @param ModelInterface $target The target node
     * @param array $attributes List of attributes.
     *
     * @return boolean Whether the appending succeeded
     */
    public function append($target, $attributes = null)
    {
        return $target->appendTo($this->getOwner(), $attributes);
    }

    /**
     * Inserts node as previous sibling of target
     *
     * @param ModelInterface $target The target node
     * @param array $attributes List of attributes
     *
     * @return boolean Whether the insert succeeded
     */
    public function insertBefore($target, $attributes = null)
    {
        return $this->addNode($target, $target->{$this->leftAttribute}, 0, $attributes);
    }

    /**
     * Inserts node as next sibling of target
     *
     * @param ModelInterface $target The target node
     * @param array $attributes List of attributes
     *
     * @return boolean Whether the insert succeeded
     */
    public function insertAfter($target, $attributes = null)
    {
        return $this->addNode($target, $target->{$this->rightAttribute} + 1, 0, $attributes);
    }

    /**
     * Move node as previous sibling of target
     *
     * @param ModelInterface $target The target node
     * @return boolean whether the move succeeded
     */
    public function moveBefore($target)
    {
        return $this->moveNode($target, $target->{$this->leftAttribute}, 0);
    }

    /**
     * Move node as next sibling of target
     *
     * @param ModelInterface $target The target node
     * @return boolean Whether the move succeeded
     */
    public function moveAfter($target)
    {
        return $this->moveNode($target, $target->{$this->rightAttribute} + 1, 0);
    }

    /**
     * Move node as first child of target
     *
     * @param ModelInterface $target The target node
     * @return boolean Whether the move succeeded
     */
    public function moveAsFirst($target)
    {
        return $this->moveNode($target, $target->{$this->leftAttribute} + 1, 1);
    }

    /**
     * Move node as last child of target
     *
     * @param ModelInterface $target The target node
     * @return boolean Whether the move succeeded
     */
    public function moveAsLast($target)
    {
        return $this->moveNode($target, $target->{$this->rightAttribute}, 1);
    }

    /**
     * Move node as new root
     *
     * @return boolean Whether the move succeeded
     * @throws Exception
     */
    public function moveAsRoot()
    {
        $owner = $this->getOwner();

        if (!$this->hasManyRoots) {
            throw new Exception('Many roots mode is off.');
        }

        if ($this->getIsNewRecord()) {
            throw new Exception('The node should not be new record.');
        }

        if ($this->getIsDeletedRecord()) {
            throw new Exception('The node should not be deleted.');
        }

        if ($owner->isRoot()) {
            throw new Exception('The node is already a root node.');
        }

        // $db = $owner->getDI()->get('db');
        $db = $owner->getWriteConnection();
        if ($this->useTransactions) {
            $db->begin();
        }

        $left = $owner->{$this->leftAttribute};
        $right = $owner->{$this->rightAttribute};
        $levelDelta = 1 - $owner->{$this->levelAttribute};
        $delta = 1 - $left;

        $condition = $this->leftAttribute . ' >= ' . $left . ' AND ';
        $condition .= $this->rightAttribute . ' <= ' . $right . ' AND ';
        $condition .= $this->rootAttribute . ' = ' . $owner->{$this->rootAttribute};

        $sql = 'UPDATE `' . $owner->getSource() . '` SET ';
        $sql .= '`' . $this->leftAttribute . '` = (`' . $this->leftAttribute . '`' . sprintf('%+d', $delta) . ')';
        $sql .= ', `' . $this->rightAttribute . '` = (`' . $this->rightAttribute . '`' . sprintf('%+d', $delta) . ')';
        $sql .= ', `' . $this->levelAttribute . '` = (`' . $this->levelAttribute . '`' . sprintf('%+d', $levelDelta) . '),';
        $sql .= ', `' . $this->rootAttribute . '` = ' . $owner->{$this->primaryKey};
        $sql .= ', `' . $this->parentAttribute . '` = NULL';
        $sql .= ' WHERE ' . $condition;

        $db->execute($sql);
/*
        $this->ignoreEvent = true;
        foreach ($owner::find(array('conditions' => $condition, 'for_update' => true)) as $i) {
            $arr = array(
                $this->leftAttribute => $i->{$this->leftAttribute} + $delta,
                $this->rightAttribute => $i->{$this->rightAttribute} + $delta,
                $this->levelAttribute => $i->{$this->levelAttribute} + $levelDelta,
                $this->rootAttribute => $owner->{$this->primaryKey}
            );
            if (false === $i->update($arr)) {
                if ($this->useTransactions) {
                    $owner->getDI()->getDb()->rollback();
                }
                $this->ignoreEvent = false;
                return false;
            }
        }
        $this->ignoreEvent = false;
*/

        $this->shiftLeftRight($right + 1, $left - $right - 1);

        if ($this->useTransactions) {
            $db->commit();
        }

        return true;
    }

    /**
     * Creates a new root node in multiple-root mode or updates an existing node.
     *
     * @param array $attributes List of attributes
     * @param array $whiteList Whether to perform validation
     * @return boolean Whether the save succeeded
     */
    public function saveNode($attributes = null, $whiteList = null)
    {
        $owner = $this->getOwner();

        $this->ignoreEvent = true;

        if (!$this->hasValidOwner()) {
            $result = $this->makeRoot($attributes, $whiteList);
        } else {
            $result = $owner->update($attributes, $whiteList);
        }

        $this->ignoreEvent = false;

        return $result;
    }

    /**
     * @param array $items
     * @param array $existing_tree
     *
     * @return bool
     * @throws \Exception
     */
    public function updateTreeOrder($items, $existing_tree = null)
    {
        if (empty($items)) {
            throw new \Exception('$items is required in order to reorder the tree.');
        }

        $owner = $this->getOwner();

        if (!$existing_tree) {
            $root = $owner->getRoot();
            $existing_tree = $root->getTree();
        }

        $queries = array();
        foreach ($items as $item) {
            $current_node_id = (int) $item['item_id'];
            if ($current_node_id > 1) {
                $existing_node = $existing_tree[$current_node_id];

                $changes = array();
                $check_map = array(
                    // $item key => $node key
                    'left' => $this->leftAttribute,
                    'right' => $this->rightAttribute,
                    'parent_id' => $this->parentAttribute,
                    'depth' => $this->levelAttribute
                );
                foreach ($check_map as $item_key => $node_key) {
                    if ($existing_node->$node_key != $item[$item_key]) {
                        $changes[$node_key] = $item[$item_key];
                    }
                }

                if (!empty($changes)) {
                    $sql = 'UPDATE `' . $owner->getSource() . '` SET ';
                    $value_pairs = array();
                    foreach ($changes as $k => $v) {
                        $value_pairs[] = '`' . $k . '` = ' . $v;
                    }
                    if (!empty($value_pairs)) {
                        $sql .= implode(', ', $value_pairs);
                    }
                    $sql .= ' WHERE `' . $this->primaryKey . '` = ' . $current_node_id;
                    $queries[] = $sql;
                }
            }
        }

        if (!empty($queries)) {
            /* @var $db \Phalcon\Db\AdapterInterface */
            // $db = $owner->getDI()->get('db');
            $db = $owner->getWriteConnection();
            if ($this->useTransactions) {
                $db->begin();
            }
            foreach ($queries as $query) {
                $result = $db->execute($query);
                if (!$result) {
                    $db->rollback();
                    return false;
                }
            }
            if ($this->useTransactions) {
                $db->commit();
            }
        }

        return true;
    }

    /**
     * Deletes a node and it's descendants
     *
     * @return boolean Whether deletion succeeded
     * @throws Exception
     */
    public function deleteNode()
    {
        $owner = $this->getOwner();

        if ($this->getIsNewRecord()) {
            throw new Exception('The node cannot be deleted because it is new.');
        }

        if ($this->getIsDeletedRecord()) {
            throw new Exception('The node cannot be deleted because it is already deleted.');
        }

        $old_lft = $owner->{$this->leftAttribute};
        $old_rgt = $owner->{$this->rightAttribute};
        $old_root = $this->hasManyRoots ? $owner->{$this->rootAttribute} : null;

        // Prepare variables before we delete the node(s)
        $first = $old_rgt + 1;
        $delta = $old_lft - $old_rgt - 1;

        /* @var $db \Phalcon\Db\AdapterInterface */
        $db = $owner->getDI()->get('db');
        if ($this->useTransactions) {
            $db->begin();
        }

        // Entering critical section :)
        $failed = false;
        $this->ignoreEvent = true;

        if ($owner->isLeaf()) {
            try {
                if (false === $owner->delete()) {
                    $failed = true;
                }
            } catch (\PDOException $e) {
                Bootstrap::log($e);
                $failed = true;
            }
        } else {
            $condition = $this->leftAttribute . ' >= ' . $old_lft . ' AND ';
            $condition .= $this->rightAttribute . ' <= ' . $old_rgt;

            if ($this->hasManyRoots) {
                $condition .= ' AND ' . $this->rootAttribute . ' = ' . $old_root;
            }

            /**
             * for_update is bugged currently:
             * @link https://github.com/phalcon/cphalcon/issues/2811
             * @link https://github.com/phalcon/cphalcon/issues/2636
             * @link https://github.com/phalcon/cphalcon/issues/2407
             */
            try {
                // foreach ($owner::find(array('conditions' => $condition, 'for_update' => true)) as $i) {
                foreach ($owner::find(array('conditions' => $condition)) as $i) {
                    if (false === $i->delete()) {
                        $failed = true;
                    }
                }
            } catch (\PDOException $e) {
                Bootstrap::log($e);
                $failed = true;
            }
        }

        $this->ignoreEvent = false;

        // If any of the above operations failed, rollback the transaction (if there is one)
        // and return false
        if ($failed) {
            if ($this->useTransactions) {
                $db->rollback();
            }
            return false;
        }

        // Close gap after deletion
        $this->shiftLeftRightRange($first, 0, $delta, $old_root);

        if ($this->useTransactions) {
            $db->commit();
        }

        return true;
    }

    /**
     * @param ModelInterface $target
     * @param int $key
     * @param int $levelUp
     * @return boolean
     * @throws Exception
     */
    private function moveNode($target, $key, $levelUp)
    {
        $owner = $this->getOwner();

        if (!$target) {
            throw new Exception('Target node is not defined.');
        }

        if ($this->getIsNewRecord()) {
            throw new Exception('The node should not be new record.');
        }

        if ($this->getIsDeletedRecord()) {
            throw new Exception('The node should not be deleted.');
        }

        if ($target->getIsDeletedRecord()) {
            throw new Exception('The target node should not be deleted.');
        }

        if ($owner === $target) {
            throw new Exception('The target node should not be self.');
        }

        if ($target->isDescendantOf($owner)) {
            throw new Exception('The target node should not be descendant.');
        }

        if (!$levelUp && $target->isRoot()) {
            throw new Exception('The target node should not be root.');
        }

        $db = $owner->getWriteConnection();
        if ($this->useTransactions) {
            $db->begin();
        }

        $left = $owner->{$this->leftAttribute};
        $right = $owner->{$this->rightAttribute};
        $levelDelta = $target->{$this->levelAttribute} - $owner->{$this->levelAttribute} + $levelUp;

        $old_root = (int) $owner->{$this->rootAttribute};
        $new_root = (int) $target->{$this->rootAttribute};

        $this->debug_print('moveNode: ' . $owner->name . ' -> ' . $target->name
            . ', key=' . $key . ', levelUp=' . $levelUp . ', old_root=' . $old_root
            . ', new_root=' . $new_root . ',levelDelta=' . $levelDelta);
        // $this->debug_print('o=' . var_export($owner->toArray(), true) . ' t=' . var_export($target->toArray(), true));

        // Update parent_id for the node if/when needed
        $current_id = (int) $owner->{$this->primaryKey};
        $current_parent_id = (int) $owner->{$this->parentAttribute};
        $target_id = (int) $target->{$this->primaryKey};
        $target_parent_id = (int) $target->{$this->parentAttribute};
        if ($levelUp) {
            // Node is changing level as a new child, update parent_id to the id of the target node
            if ($current_parent_id != $target_id) {
                $new_parent_id = $target_id;
            }
        } else {
            // Node is moving as a sibling of an existing node, so
            // update it to have the sibling's parent_id in case it doesn't already
            if ($current_parent_id != $target_parent_id) {
                $new_parent_id = $target_parent_id;
            }
        }
        if (isset($new_parent_id)) {
            $sql = 'UPDATE `' . $owner->getSource() . '` SET ';
            $sql .= '`' . $this->parentAttribute . '` = ' . $new_parent_id;
            $sql .= ' WHERE `' . $this->primaryKey . '` = ' . $current_id;
            $this->debug_print('moveNode (updating parent_id): ' . $sql);
            $db->execute($sql);
        }

        if ($this->hasManyRoots && $old_root !== $new_root) {
            // Moving nodes across different roots in multi-root mode

            // Preparing target tree for insertion
            $this->shiftLeftRight($key, $right - $left + 1, $new_root);

            // Update lft/rgt/root/level for all descendants
            $delta = $key - $left;

            $condition = $this->leftAttribute . ' >= ' . $left . ' AND ';
            $condition .= $this->rightAttribute . ' <= ' . $right . ' AND ';
            $condition .= $this->rootAttribute . ' = ' . $old_root;

            $sql = 'UPDATE `' . $owner->getSource() . '` SET ';
            $sql .= '`' . $this->leftAttribute . '` = (`' . $this->leftAttribute . '`' . sprintf('%+d', $delta) . ')';
            $sql .= ', `' . $this->rightAttribute . '` = (`' . $this->rightAttribute . '`' . sprintf('%+d', $delta) . ')';
            $sql .= ', `' . $this->levelAttribute . '` = (`' . $this->levelAttribute . '`' . sprintf('%+d', $levelDelta) . ')';
            $sql .= ', `' . $this->rootAttribute . '` = ' . $new_root;
            $sql .= ' WHERE ' . $condition;

            $this->debug_print('moveNode (between trees): executing raw SQL: ' . $sql);
            $db->execute($sql);

            // Closing the gap in the old tree
            $this->shiftLeftRight($right + 1, $left - $right - 1, $old_root);

            if ($this->useTransactions) {
                $db->commit();
            }
        } else {
            // Moving node within the same tree
            $this->debug_print('moveNode (same tree): ' . $owner->name . ' -> ' . $target->name);

            $delta = $right - $left + 1;
            $this->shiftLeftRight($key, $delta);

            if ($left >= $key) {
                $left += $delta;
                $right += $delta;
            }

            $condition = $this->leftAttribute . ' >= ' . $left . ' AND ' . $this->rightAttribute . ' <= ' . $right;

            if ($this->hasManyRoots) {
                $condition .= ' AND ' . $this->rootAttribute . ' = ' . $owner->{$this->rootAttribute};
            }

            $sql = 'UPDATE `' . $owner->getSource() . '` SET ';
            $sql .= '`' . $this->levelAttribute . '` = (`' . $this->levelAttribute . '`' . sprintf('%+d', $levelDelta) . ')';
            $sql .= ' WHERE ' . $condition;

            $this->debug_print('moveNode (same tree): executing SQL: ' . $sql);
            $db->execute($sql);

            foreach (array($this->leftAttribute, $this->rightAttribute) as $attribute) {
                $condition = $attribute . ' >= ' . $left . ' AND ' . $attribute . ' <= ' . $right;

                if ($this->hasManyRoots) {
                    $condition .= ' AND ' . $this->rootAttribute . ' = ' . $owner->{$this->rootAttribute};
                }

                $sql = 'UPDATE `' . $owner->getSource() . '` SET `' . $attribute . '` = (`' . $attribute . '`' . sprintf('%+d', $key - $left) . ')';
                $sql .= ' WHERE ' . $condition;

                $this->debug_print('moveNode (same tree): executing raw SQL: ' . $sql);
                $db->execute($sql);
            }

            $this->shiftLeftRight($right + 1, -$delta);

            if ($this->useTransactions) {
                $db->commit();
            }
        }

        return true;
    }

    /**
     * Adds $delta to all Left and Right values that are >= '$key'.
     * $delta can be negative.
     *
     * Note: Does not wrap queries in a transaction. Should be done by the calling code.
     *
     * @param $key
     * @param $delta
     * @param null $root_value
     */
    private function shiftLeftRight($key, $delta, $root_value = null)
    {
        $owner = $this->getOwner();

        // fallback for the old way in case no $root_value is provided to the method
        if ($this->hasManyRoots && null === $root_value) {
            $root_value = $owner->{$this->rootAttribute};
        }

        $this->debug_print('shiftLeftRight: ' . $owner->name . ' (id=' . $owner->{$this->primaryKey} . ') key=' . $key . ' delta=' . $delta . ' root=' . $root_value);

        foreach (array($this->leftAttribute, $this->rightAttribute) as $attribute) {
            $condition = $attribute . ' >= ' . $key;

            if (null !== $root_value) {
                $condition .= ' AND ' . $this->rootAttribute . ' = ' . $root_value;
            }

            $sql = 'UPDATE `' . $owner->getSource() . '` SET `' . $attribute . '` = (`' . $attribute . '`' . sprintf('%+d', $delta) . ')';
            $sql .= ' WHERE ' . $condition;

            $this->debug_print('ShiftLeftRight: executing raw SQL: ' . $sql);
            $owner->getWriteConnection()->execute($sql);
        }
    }

    /**
     * Adds $delta to all Left and Right values that are >= '$first' and <= '$last'.
     * If $last is 0, it is ignored and no upper bound exists.
     * $delta can also be negative.
     *
     * Note: Does not wrap queries in a transaction. Should be done by the calling code.
     *
     * @param int $first first left/right value to shift
     * @param int $last last left/right value to shift, or 0
     * @param int $delta offset to shift by
     * @param mixed $root_value the root value of entities to act upon
     */
    private function shiftLeftRightRange($first, $last, $delta, $root_value = null)
    {
        $owner = $this->getOwner();

        // fallback for the old way in case no $root_value is provided to the method
        if ($this->hasManyRoots && null === $root_value) {
            $root_value = $owner->{$this->rootAttribute};
        }

        foreach (array($this->leftAttribute, $this->rightAttribute) as $attribute) {
            $condition = $attribute . ' >= ' . (int) $first;

            if ($last > 0) {
                $condition .= ' AND ' . $attribute . ' <= ' . (int) $last;
            }

            if ($this->hasManyRoots) {
                $condition .= ' AND ' . $this->rootAttribute . ' = ' . (int) $root_value;
            }

            $sql = 'UPDATE `' . $owner->getSource() . '` SET `' . $attribute . '` = (`' . $attribute . '`' . sprintf('%+d', $delta) . ')';
            $sql .= ' WHERE ' . $condition;

            $this->debug_print('shiftLeftRightRange: executing SQL: ' . $sql);
            $owner->getWriteConnection()->execute($sql);
        }
    }

    /**
     * @param ModelInterface $target
     * @param int $key
     * @param int $levelUp
     * @param array $attributes
     * @return boolean
     * @throws Exception
     */
    private function addNode($target, $key, $levelUp, $attributes)
    {
        $owner = $this->getOwner();

        if (!$target) {
            throw new Exception('The node cannot be inserted because target is not defined.');
        }

        if (!$this->getIsNewRecord()) {
            throw new Exception('The node cannot be inserted because it is not new.');
        }

        if ($this->getIsDeletedRecord()) {
            throw new Exception('The node cannot be inserted because it is deleted.');
        }

        if ($target->getIsDeletedRecord()) {
            throw new Exception('The node cannot be inserted because target node is deleted.');
        }

        if ($owner === $target) {
            throw new Exception('The target node should not be self.');
        }

        if (!$levelUp && $target->isRoot()) {
            throw new Exception('The target node should not be root.');
        }

        if ($this->hasManyRoots) {
            $owner->{$this->rootAttribute} = $target->{$this->rootAttribute};
        }

        /* @var \Phalcon\Db\Adapter $db */
        // $db = $owner->getDI()->get('db');
        $db = $owner->getWriteConnection();
        if ($this->useTransactions) {
            $db->begin();
        }

        $this->shiftLeftRight($key, 2);
        $owner->{$this->leftAttribute} = $key;
        $owner->{$this->rightAttribute} = $key + 1;
        $owner->{$this->levelAttribute} = $target->{$this->levelAttribute} + $levelUp;
        $owner->{$this->parentAttribute} = $target->{$this->primaryKey};

        // $this->debug_print($key . ':' . $owner->name . ':' . $owner->lft . ':' . $owner->rght . ':' . $owner->level);

        $this->ignoreEvent = true;
        $result = $owner->create($attributes);
        $this->ignoreEvent = false;

        if (false === $result) {
            if ($this->useTransactions) {
                $db->rollback();
            }
            return false;
        }

        if ($this->useTransactions) {
            $db->commit();
        }

        return $result;
    }

    /**
     * @param array $attributes
     * @param array $whiteList
     * @return boolean
     * @throws Exception
     */
    private function makeRoot($attributes, $whiteList)
    {
        $owner = $this->getOwner();
        $owner->{$this->leftAttribute} = 1;
        $owner->{$this->rightAttribute} = 2;
        $owner->{$this->levelAttribute} = 1;
        $owner->{$this->parentAttribute} = null;

        if ($this->hasManyRoots) {
            /* @var \Phalcon\Db\Adapter $db */
            // $db = $owner->getDI()->get('db');
            $db = $owner->getWriteConnection();
            if ($this->useTransactions) {
                $db->begin();
            }
            $this->ignoreEvent = true;
            if (false === $owner->create($attributes, $whiteList)) {
                if ($this->useTransactions) {
                    $db->rollback();
                }
                $this->ignoreEvent = false;
                return false;
            }

            /**
             * TODO/FIXME: there appears to be a Phalcon bug affecting us here:
             * @link https://github.com/phalcon/cphalcon/issues/1867#issuecomment-66174353
             * It manifests as an exception caught within Blamable::audit_after_update()
             * which should not be happening really, since we've clearly had some changes done
             * to the model (updating the root attribute to point to self)
             */
            // set root to self
            $owner->{$this->rootAttribute} = (int) $owner->{$this->primaryKey};
            $owner->update();
            $this->ignoreEvent = false;

            if ($this->useTransactions) {
                $db->commit();
            }
        } else {
            if (count($owner->roots())) {
                throw new Exception('Cannot create more than one root in single root mode.');
            }

            $this->ignoreEvent = true;
            if (false === $owner->create($attributes, $whiteList)) {
                $this->ignoreEvent = false;
                return false;
            }
            $this->ignoreEvent = false;
        }

        return true;
    }
}
