<?php

namespace Baseapp\Library\Behavior;

use Baseapp\Bootstrap;
use Baseapp\Library\Debug;
use Baseapp\Models\Audit;
use Baseapp\Models\AuditDetail;
use Baseapp\Models\BaseModelBlamable;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

/**
 * \Phalcon\Mvc\Model\Behavior\Blamable
 */
class Blamable extends Behavior implements BehaviorInterface
{
    // Enabled by default
    protected $enabled = true;

    /**
     * {@inheritdoc}
     *
     * @param string $eventType
     * @param ModelInterface|BaseModelBlamable $model
     */
    public function notify($eventType, ModelInterface $model)
    {
        if ($model->is_auditing_disabled()) {
            return true;
        }

        if ($eventType == 'afterCreate') {
            return $this->audit_after_create($model);
        }

        if ($eventType == 'afterUpdate') {
            return $this->audit_after_update($model);
        }

        if ($eventType == 'afterDelete') {
            return $this->audit_after_delete($model);
        }
    }

    /**
     * Creates an Audit instance based on the current environment
     *
     * @param string $type
     * @param \Baseapp\Models\BaseModelBlamable $model
     * @return Audit
     */
    public function create_audit($type, BaseModelBlamable $model)
    {
        $audit = new Audit();

        // Get the user details, if available
        if ('cli' === PHP_SAPI) {
            $audit->user_id  = null;
            $audit->username = 'system';
        } else {
            $user = $model->getDI()->getShared('auth')->get_user();
            if ($user) {
                $audit->user_id = $user->id;
                $audit->username = $user->username;
            } else {
                $audit->user_id = null;
                $audit->username = 'anonymous';
            }
        }

        // Model name that performed the action
        $audit->model_name = get_class($model);
        $audit->message = $audit->model_name;
        $audit->model_pk_val = isset($model->id) ? $model->id : null;
        if ('cli' === PHP_SAPI) {
            $audit->ip = 'cli';
        } else {
            $request = $model->getDI()->getRequest();
            $audit->ip = $request->getClientAddress(true);
        }
        $audit->type = $type;
        $audit->created_at = date('Y-m-d H:i:s');

        return $audit;
    }

    /**
     * Audits a CREATE operation
     *
     * @param \Baseapp\Models\BaseModelBlamable $model
     * @return boolean|null
     */
    public function audit_after_create(BaseModelBlamable $model)
    {
        //Create a new audit
        $audit    = $this->create_audit('C', $model);
        $meta     = $model->getModelsMetaData();
        $fields   = $meta->getAttributes($model);
        $details  = array();

        foreach ($fields as $field) {
            $audit_detail = new AuditDetail();
            $audit_detail->field_name = $field;
            $audit_detail->old_value = null;
            $audit_detail->new_value = $model->readAttribute($field);

            $details[] = $audit_detail;
        }

        $audit->details = $details;

        return $audit->save();
    }

    /**
     * Audits an UPDATE operation
     *
     * @param \Baseapp\Models\BaseModelBlamable $model
     * @return boolean|null
     */
    public function audit_after_update(BaseModelBlamable $model)
    {
        $changed_fields = array();
        if ($model->hasSnapshotData()) {
            $changed_fields = $model->getChangedFields();
        } else {
            $model_class = get_class($model);
            Bootstrap::log('Model `' . $model_class . '` has no snapshot data but was audited for update!');
            $detailed_debug = false;
            if ($detailed_debug) {
                Bootstrap::log(array('debug' => "Trace:\n" . Debug::trace()));
                $data = var_export($model->toArray(), true);
                Bootstrap::log(array('debug' => "Model data: (" . $model_class . ")\n" . $data));
            }
        }

        if (count($changed_fields)) {
            // Create a new audit
            $audit = $this->create_audit('U', $model);

            // Data from the model before modifications
            $original_data = $model->getSnapshotData();

            $details = array();
            foreach ($changed_fields as $field) {
                $audit_detail = new AuditDetail();
                $audit_detail->field_name = $field;
                $audit_detail->old_value = $original_data[$field];
                $audit_detail->new_value = $model->readAttribute($field);

                $details[] = $audit_detail;
            }

            $audit->details = $details;

            return $audit->save();
        }

        return null;
    }

    /**
     * Audits a DELETE operation
     *
     * @param \Baseapp\Models\BaseModelBlamable $model
     * @return boolean|null
     */
    public function audit_after_delete(BaseModelBlamable $model)
    {
        $original_data = $model->getSnapshotData();
        /**
         * it can happen that there's nothing really deleted but the Model events
         * still occur?
         */
        if (null !== $original_data) {
            // Create a new audit
            $audit = $this->create_audit('D', $model);

            $details = array();
            foreach ($original_data as $field => $value) {
                $audit_detail = new AuditDetail();
                $audit_detail->field_name = $field;
                $audit_detail->old_value = $value;
                $audit_detail->new_value = null;

                $details[] = $audit_detail;
            }

            $audit->details = $details;

            return $audit->save();
        }
    }
}
