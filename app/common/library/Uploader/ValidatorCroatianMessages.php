<?php

namespace Baseapp\Library\Uploader;

class ValidatorCroatianMessages extends Validator
{
    const MESSAGE_INVALID_MIN_SIZE = 'Datoteka `%s` je premala. Mora biti barem `%s`';
    const MESSAGE_INVALID_MAX_SIZE = 'Datoteka `%s` je prevelika. Ne smije biti veća od `%s`';
    const MESSAGE_INVALID_EXTENSION = 'Datoteka `%s` nije prihvatljiva. Dozvoljene ekstenzije datoteka su: `%s`';
    const MESSAGE_INVALID_MIME_TYPE = 'Datoteka `%s` nije dozvoljenog tipa. Dozvoljeni tipovi su: `%s`';
    const MESSAGE_INVALID_UPLOAD_DIR = 'Odredišni direktorij `%s` ne postoji';
    const MESSAGE_INVALID_UPLOAD_DIR_PERMISSIONS = 'Odredišni direktorij `%s` nema dozvolu pisanja';
    const MESSAGE_NO_FILE_UPLOADED = 'Datoteka nije poslana';
    const MESSAGE_UNKNOWN_PHP_CORE_ERROR = 'Nepoznata greška';
}
