<?php

namespace Baseapp\Library\Validations;


class AdsBackend extends Ads
{
    protected $unique = array();

    public function setup()
    {

    }

    public function set_unique($unique) {
        $this->unique = $unique;
    }

    public function get()
    {
        $this->add_uniques();
        return $this->validation;
    }

    private function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name) {
                $this->validation->add($field_name, new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model' => self::MODEL,
                    'message' => ucfirst($field_name) . ' already exists'
                )));
            }
        }
    }

}
