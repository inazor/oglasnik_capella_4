<?php

namespace Baseapp\Library\Validations;

use Baseapp\Extension\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Baseapp\Extension\Validator\Uniqueness;
// use Baseapp\Extension\Validator\Oib;
use Baseapp\Extension\Validator\OibSimple;

class UsersAdSubmission
{
    const MODEL = '\Baseapp\Models\Users';

    protected $requiredData = array('fullName', 'oib');
    protected $user = null;

    public function __construct() {
        $this->validation = new Validation();
        $this->user       = \Phalcon\Di::getDefault()->getShared('auth')->get_user();
        $this->setup();
    }

    public function setup() {}

    public function enable($validatorMethod)
    {
        if (in_array($validatorMethod, $this->requiredData)) {
            $this->{$validatorMethod . 'Validation'}();
        }
    }

    public function enableAll()
    {
        foreach($this->requiredData as $validatorMethod) {
            $this->{$validatorMethod . 'Validation'}();
        }
    }

    private function fullNameValidation()
    {
        $this->validation->add('first_name', new PresenceOf(array(
            'message' => 'Molimo upišite Vaše ime'
        )));
        $this->validation->add('last_name', new PresenceOf(array(
            'message' => 'Molimo upišite Vaše prezime'
        )));
    }

    private function oibValidation()
    {
        $this->validation->add('oib', new PresenceOf(array(
            'message' => 'Molimo upišite OIB'
        )));
        /*
        $this->validation->add('oib', new Oib(array(
            'messageInvalidLen' => 'Upisani OIB nema traženi broj znamenki',
            'messageDigit'      => 'Upisani OIB trebao bi se sastojati samo od brojki',
            'message'           => 'Upisani OIB nije ispravan'
        )));
        */
        $this->validation->add('oib', new OibSimple(array(
            'messageDigit' => 'Upisani OIB trebao bi se sastojati samo od brojki',
            'message'      => 'Upisani OIB nema traženi broj znamenki'
        )));
        // Avoids duplicate OIBs
        $this->validation->add('oib', new Uniqueness(array(
            'model'            => self::MODEL,
            'extra_conditions' => array(
                'id != :id:',
                array(
                    'id' => $this->user->id
                )
            ),
            'message' => 'Već postoji korisnički račun s upisanim OIB-om'
        )));
    }

    /**
     * @return \Baseapp\Extension\Validation
     */
    public function get()
    {
        return $this->validation;
    }

    /**
     * @return \Phalcon\Validation\Message\Group
     */
    public function getMessages()
    {
        return $this->get()->getMessages();
    }

    /**
     * @param $data
     *
     * @return \Phalcon\Validation\Message\Group
     */
    public function validate($data)
    {
        return $this->get()->validate($data);
    }
}
