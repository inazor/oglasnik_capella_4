<?php

namespace Baseapp\Library\Validations;

use Phalcon\Validation\Validator\PresenceOf;

class UsersShopsFeaturedFrontend extends UsersShopsFeatured
{
    public function setup()
    {
        $this->validation->add('categories', new PresenceOf(array(
            'message' => 'Morate odabrati bar jednu kategoriju'
        )));
    }

    public function validate_if_set($field = null)
    {
        switch ($field) {
            case 'ads':
                $this->validation->add('ads', new PresenceOf(array(
                    'message' => 'Morate odabrati bar jedan oglas'
                )));
                break;

            case 'intro':
                $this->validation->add('intro', new PresenceOf(array(
                    'message' => 'Morate popuniti intro tekst'
                )));
                break;
        }
    }

}
