<?php

namespace Baseapp\Library\Validations;

class UsersFrontendEditCompany extends UsersFrontendEdit
{
    public function setup()
    {
        // Company validations are in addition to those of "regular" users
        parent::setup();

        $fields = array(
            'first_name' => 'Ime',
            'last_name' => 'Prezime',
            'company_name' => 'Naziv poslovnog subjekta',
            'phone1' => 'Telefon #1'
        );
        foreach ($fields as $field => $label) {
            $this->validation->add($field, new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Polje ' . $label . ' mora biti popunjeno'
            )));
        }

        $labels = array(
            'oib' => 'OIB',
            'phone2' => 'Telefon #2'
        );
        $labels = array_merge($fields, $labels);

        $this->validation->setLabels($labels);
    }
}
