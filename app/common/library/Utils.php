<?php

namespace Baseapp\Library;

use Baseapp\Bootstrap;
use Baseapp\Models\Orders;
use Closure;
use Cocur\Slugify\Slugify;

class Utils
{
    public static function detect_current_scheme($default = 'http')
    {
        $scheme = $default;
        $secure = false;
        // if (!empty($_SERVER['HTTPS']) && 'off' !== $_SERVER['HTTPS'] || '443' === $_SERVER['SERVER_PORT']) {
        if (!empty($_SERVER['HTTPS']) && 'off' !== $_SERVER['HTTPS']) {
            $secure = true;
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && 'https' === $_SERVER['HTTP_X_FORWARDED_PROTO'] ||
            !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && 'on' === $_SERVER['HTTP_X_FORWARDED_SSL']) {
            $secure = true;
        }
        if ($secure) {
            $scheme = 'https';
        }
        return $scheme;
    }

    public static function detect_current_hostname($default = '')
    {
        return isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : $default;
    }

    /**
     * Returns a DOMDocument object from potentially invalid HTML markup
     * forced into UTF-8 encoding
     *
     * @param string $html HTML markup
     *
     * @return \DOMDocument $doc A newly created DOMDocument object
     */
    public static function html2dom($html)
    {
        libxml_use_internal_errors(true);
        $doc = new \DOMDocument();
        // force the fragment into utf-8 via injected meta tag
        $meta = '<meta http-equiv="content-type" content="text/html; charset=utf-8">';
        $doc->loadHTML($meta . $html);
        $doc->preserveWhiteSpace = false;
        libxml_clear_errors();
        return $doc;
    }

    /**
     * Get <body> HTML from a DOMDocument object
     *
     * @param $doc
     *
     * @return mixed|string Markup between <body> tags
     */
    public static function dom2html(\DOMDocument $doc)
    {
        $html = $doc->saveHTML();
        // remove injected meta tag
        $meta = '<meta http-equiv="content-type" content="text/html; charset=utf-8">';
        $html = str_replace($meta, '', $html);
        $html = preg_replace(
            '/^<!DOCTYPE.+?>/',
            '',
            str_replace(
                array( '<html>', '</html>', '<body>', '</body>', '<head>', '</head>'),
                '',
                $html
            )
        );
        $html = trim($html);
        return $html;
    }

    /**
     * Encode arbitrary data into base-62
     *
     * Note that because base-62 encodes slightly less than 6 bits per character (actually 5.95419631038688), there is some wastage
     * In order to make this practical, we chunk in groups of up to 8 input chars, which give up to 11 output chars
     * with a wastage of up to 4 bits per chunk, so while the output is not quite as space efficient as a
     * true multiprecision conversion, it's orders of magnitude faster
     *
     * Note that the output of this function is not compatible with that of a multiprecision conversion, but it's a practical encoding implementation
     * The encoding overhead tends towards 37.5% with this chunk size; bigger chunk sizes can be slightly more space efficient, but may be slower
     * Base-64 doesn't suffer this problem because it fits into exactly 6 bits, so it generates the same results as a multiprecision conversion
     *
     * Requires PHP 5.3.2 and gmp 4.2.0
     *
     * @param string $data Binary data to encode
     * @return string Base-62 encoded text (not chunked or split)
     */
    public static function base62_encode($data)
    {
        $outstring = '';
        $l = strlen($data);
        for ($i = 0; $i < $l; $i += 8) {
            $chunk = substr($data, $i, 8);
            $outlen = ceil((strlen($chunk) * 8)/6); //8bit/char in, 6bits/char out, round up
            $x = bin2hex($chunk);  //gmp won't convert from binary, so go via hex
            $w = gmp_strval(gmp_init(ltrim($x, '0'), 16), 62); //gmp doesn't like leading 0s
            $pad = str_pad($w, $outlen, '0', STR_PAD_LEFT);
            $outstring .= $pad;
        }
        return $outstring;
    }

    /**
     * Decode base-62 encoded text into binary
     *
     * Note that because base-62 encodes slightly less than 6 bits per character (actually 5.95419631038688), there is some wastage
     * In order to make this practical, we chunk in groups of up to 11 input chars, which give up to 8 output chars
     * with a wastage of up to 4 bits per chunk, so while the output is not quite as space efficient as a
     * true multiprecision conversion, it's orders of magnitude faster
     *
     * Note that the input of this function is not compatible with that of a multiprecision conversion, but it's a practical encoding implementation
     *
     * The encoding overhead tends towards 37.5% with this chunk size; bigger chunk sizes can be slightly more space efficient, but may be slower
     * Base-64 doesn't suffer this problem because it fits into exactly 6 bits, so it generates the same results as a multiprecision conversion
     * Requires PHP 5.3.2 and gmp 4.2.0
     *
     * @param string $data Base-62 encoded text (not chunked or split)
     * @return string Decoded binary data
     */
    public static function base62_decode($data)
    {
        $outstring = '';
        $l = strlen($data);
        for ($i = 0; $i < $l; $i += 11) {
            $chunk = substr($data, $i, 11);
            $outlen = floor((strlen($chunk) * 6)/8); //6bit/char in, 8bits/char out, round down
            $y = gmp_strval(gmp_init(ltrim($chunk, '0'), 62), 16); //gmp doesn't like leading 0s
            $pad = str_pad($y, $outlen * 2, '0', STR_PAD_LEFT); //double output length as as we're going via hex (4bits/char)
            $outstring .= pack('H*', $pad); //same as hex2bin
        }
        return $outstring;
    }

    public static function array_where($array, Closure $callback)
    {
        $filtered = array();

        foreach ($array as $key => $value) {
            if (call_user_func($callback, $key, $value)) {
                $filtered[$key] = $value;
            }
        }

        return $filtered;
    }

    /**
     * Accepts an array, and returns an array of values from that array as
     * specified by $field. For example, if the array is full of objects
     * and you call Utils::array_pluck($array, 'name'), the function will
     * return an array of values from $array[]->name.
     *
     * @param array $array
     * @param string $field The field to get values from
     * @param bool $preserve_keys Whether or not to preserve the array keys
     * @param bool $remove_nomatches If the field doesn't appear to be set,remove it from the array
     *
     * @return array
     */
    public static function array_pluck(array $array, $field, $preserve_keys = true, $remove_nomatches = true)
    {
        $new_list = array();
        foreach ($array as $key => $value) {
            if (is_object($value)) {
                if (isset($value->{$field})) {
                    if ($preserve_keys) {
                        $new_list[$key] = $value->{$field};
                    } else {
                        $new_list[] = $value->{$field};
                    }
                } elseif (!$remove_nomatches) {
                    $new_list[$key] = $value;
                }
            } else {
                if (isset($value[$field])) {
                    if ($preserve_keys) {
                        $new_list[$key] = $value[$field];
                    } else {
                        $new_list[] = $value[$field];
                    }
                } elseif (!$remove_nomatches) {
                    $new_list[$key] = $value;
                }
            }
        }
        return $new_list;
    }

    /**
     * @return string
     */
    public static function debug_backtrace_simple()
    {
        $stack = '';
        $i = 1;
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        unset($trace[0]); // Removes this call from the trace
        foreach ($trace as $node) {
            $stack .= '#' . $i . ' ' . $node['file'] . '(' . $node['line'] . '): ';
            if(isset($node['class'])) {
                $stack .= $node['class'] . '->';
            }
            $stack .= $node['function'] . '()' . PHP_EOL;
            $i++;
        }
        return $stack;
    }

    /**
     * Breaks long chunks of text using zero-width spaces by adding them
     * after every 3rd char in words longer than 10 chars.
     *
     * @param $text string
     *
     * @return null|string
     */
    public static function soft_break($text)
    {
        $return = null;

        if (is_string($text)) {
            $return = '';

            // $breaker = '&#173;';
            $breaker = '&#8203;';

            // Add spaces around HTML tag delimiters, to process them as individual words (removed later)
            $text = str_replace('>', '> ', $text);
            $text = str_replace('<', ' <', $text);

            // Split text into words using whitespace as a delimiter, also capturing delimiters
            $words = preg_split('/(\s)/m', $text, 0, PREG_SPLIT_DELIM_CAPTURE);

            // Loop through all words
            for ($j = 0, $word_count = count($words); $j < $word_count; $j++) {

                // Split word into letters (this method added to work around issue with multi-byte characters)
                $letters  = preg_split('/(?<!^)(?!$)/u', $words[$j]);
                $word_len = count($letters);

                // If words is an HTML tag or attribute, skip it. Attributes are hard-coded but could be dynamic.
                if (
                    (strpos($words[$j], '<') === false)
                    && (strpos($words[$j], '>') === false)
                    && (strpos($words[$j], 'href="') === false)
                    && (strpos($words[$j], 'target="') === false)
                    && ($word_len > 10)
                ) {

                    $inside_character = false;
                    $delayed_insert   = false;
                    for ($i = 0; $i < $word_len; $i++) {
                        $return .= $letters[$i];
                        // Don't add breaker inside HTML-encoded entities
                        if ($letters[$i] == '&') {
                            $inside_character = true;
                        }
                        if (($inside_character) && ($letters[$i] == ';')) {
                            $inside_character = false;
                        }
                        if (!$inside_character) {
                            // Add a breaker every third letter, unless it's a period or near the end of the word
                            if ((($i % 3) == 2) && ($i < ($word_len - 5)) && ($i > 3)) {
                                // $return .= '&#173;';
                                $return .= $breaker;
                                if ($letters[$i] == '.') {
                                    $delayed_insert = true;
                                }
                            } elseif ($delayed_insert) {
                                // We added one near a period. Add one later, because the previous one will be removed.
                                // $return .= '&#173;';
                                $return .= $breaker;
                                $delayed_insert = false;
                            }
                        }
                    }
                } else {
                    // Add unaltered word to output.
                    $return .= $words[$j];
                }
            }

            // Remove breakers that are next to a period
            // $return = str_replace('.&#173;', '.', $return);
            // $return = str_replace('&#173;.', '.', $return);
            $return = str_replace('.' . $breaker, '.', $return);
            $return = str_replace($breaker . '.', '.', $return);

            // Remove the spaces we added earlier
            $return = str_replace('> ', '>', $return);
            $return = str_replace(' <', '<', $return);
        }

        // Return the formatted text or null
        return $return;
    }

    public static function remove_invisible_chars($str, $urlencoded = true)
    {
        $non_displayables = array();

        // Every control character except newline (dec 10),
        // carriage return (dec 13) and horizontal tab (dec 09)
        if ($urlencoded) {
            $non_displayables[] = '/%0[0-8bcef]/'; // url encoded 00-08, 11, 12, 14, 15
            $non_displayables[] = '/%1[0-9a-f]/'; // url encoded 16-31
        }

        $non_displayables[] = '/\xE2\x80\x8B/'; // ZWS
        $non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S'; // 00-08, 11, 12, 14-31, 127

        do {
            $str = preg_replace($non_displayables, '', $str, -1, $count);
        } while ($count);

        return $str;
    }

    public static function replace_croatian_chars_for_sorting($str)
    {
        $is_encoded = preg_match('~%[0-9A-F]{2}~i', $str);
        if ($is_encoded) {
            $str = urldecode($str);
        }
        $search = array('š', 'đ', 'ž', 'č', 'ć');
        $replace = array('sx', 'dxx', 'zx', 'cx', 'cy');
        $str = str_replace($search, $replace, $str);
        $search = array('Š', 'Đ', 'Ž', 'Č', 'Ć');
        $replace = array('SX', 'DXX', 'ZX', 'CX', 'CY');
        $str = str_replace($search, $replace, $str);
        return $str;
    }

    /**
     * Returns true if string is an SHA1 hash look-a-like
     * @param $str
     * @return bool
     */
    public static function str_is_sha1($str)
    {
        return (ctype_xdigit($str) && 40 === strlen($str));
    }

    /**
     * Returns true if string is an MD5-look-a-like
     * @param $str
     * @return bool
     */
    public static function str_is_md5($str)
    {
        return (ctype_xdigit($str) && 32 === strlen($str));
    }

    /**
     * Sanitize filename
     *
     * @param string $filename
     * @return string
     */
    public static function sanitize_filename($filename)
    {
        $special_chars = array(
            '?', '[', ']', '/', "\\", '=', '<', '>', ':', ';', ',',
            "'", '"', '&', '$', '#', '*', '(', ')', '|', '~', '`', '!',
            '@', '%',
            // '.', ' ',
            '{', '}', chr(0)
        );

        $filename = preg_replace("#\x{00a0}#siu", ' ', $filename);
        $filename = str_replace($special_chars, '', $filename);
        $filename = str_replace(array('%20', '+'), '-', $filename);
        $filename = preg_replace('/[\r\n\t -]+/', '-', $filename);
        $filename = trim($filename, '.-_');

        $filename = static::remove_invisible_chars($filename);
        return $filename;
    }

    /**
     * Returns the filename extension (without the dot) given a filename or filepath.
     *
     * If $normalized=true (default), so called 'double-dotted' extensions are taken
     * into consideration so that for "filename.tar.gz" the function returns "tar.gz"
     * (instead of just "gz").
     *
     * @param string $filename
     * @param bool $normalized
     *
     * @return mixed|string
     */
    public static function get_filename_extension($filename, $normalized = true)
    {
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        if ($normalized) {
            // Double-dotted extensions that have special treatment
            $doubledots = array_map('strrev', array(
                'tar.gz',
                'tar.bz',
                'tar.bz2',
                'tar.xz',
                'js.gz',
                'css.gz',
                'html.gz'
            ));

            /**
             * Test if $filename contains a 'doubledot' extension
             * and if so, override $ext to be the actual extension we'd like it
             * to be instead of the thing pathinfo() returns (chunk after the rightmost dot)
             */
            $reversed = strrev($filename);
            foreach ($doubledots as $ddot) {
                if (0 === stripos($reversed, $ddot)) {
                    $ext = strrev($ddot);
                }
            }
        }

        return $ext;
    }

    /**
     * Returns a FQPN (fully qualified path name) for a given filename, optionally
     * partitioned according to $options['partition_length'] and $options['num_partitions'], prefixed
     * with the specified $options['basedir'].
     *
     * $options['partition_length'] determines the length of each partition/subdirectory,
     * while $options['num_partitions'] controls the total amount of partitions/subdirectories created.
     *
     * If $options['partition_length'] is 0 or negative, no partitioning will occur.
     *
     * If $options['partition_length'] is given without specifying $options['num_partitions'] the number of
     * partitions will be calculated automatically based on $filename length (without the file extension, and
     * reversed and zero-padded from the left in case of numeric filenames).
     *
     * TODO: What's expected for num_partitions=0? No partitioning at all? Or is the current special case of creating
     * as many partitions as possible based on partition_length fine?
     * TODO: What if partition_length is 0 but num_partitions is not? Do we obey num_partitions and try to
     * calculate a partition_length based on filename length (and maybe pad if needed)?
     *
     * Use $options['create_missing_dirs'] = true to automatically create any missing directories.
     * Use $options['sanitize_filename'] = true to automatically sanitize the filename
     *
     * For numeric-looking filenames, the digits/numbers are reversed and then split (giving even distribution
     * across partitions for constant partition parameters).
     *
     * For SHA1 or MD5 hashes we're just partitioning based on characters (because the hashes themselves should
     * already have even distribution due to the random seeds in them?).
     *
     * For everything else we're just returning the FQPN prefixed with a basedir.
     *
     * @param $filename
     * @param array $options [
     *      'partition_length' => 2,
     *      'num_partitions' => 2,
     *      'basedir' => '',
     *      'create_missing_dirs' => false,
     *      'sanitize_filename' => true,
     *      'remove_accents' => true
     * ]
     *
     * @return bool|null|string Returning the new (possibly partitioned) FQPN (prefixed with a basedir) or false when:
     *                          - the directories could not be created (and were specified)
     *                          - sanitization and/or accent removal results in an empty filename
     */
    public static function get_fqpn($filename, $options = array())
    {
        $filename_raw = $filename;
        $filename     = trim($filename);

        $defaults = array(
            'partition_length' => 2,
            'num_partitions' => 2,
            'basedir' => '',
            'create_missing_dirs' => false,
            'sanitize_filename' => true,
            'remove_accents' => false
        );

        // If options are specified, but 'num_partitions' hasn't been set or is invalid,
        // set a default of 0 for 'num_partitions' which will make the function calculate
        // the appropriate amount of partitions automatically
        if (!empty($options) && (!isset($options['num_partitions']) || $options['num_partitions'] <= 0)) {
            $defaults['num_partitions'] = 0;
        }

        $options = array_merge($defaults, $options);

        if ($options['partition_length'] < 0) {
            $options['partition_length'] = 0;
        }

        // Make sure basedir includes a trailing separator
        $options['basedir'] .= substr($options['basedir'], -1) !== '/' ? '/' : '';

        $path = null;

        /**
         * Stripping extension from the filename (if there is one) and ensuring
         * that we only work on a basename, not a complete path.
         */
        $ext = static::get_filename_extension($filename);

        // Ensure basename is what we work with from now on
        $filename = basename($filename, '.' . $ext);

        /**
         * Sanitize if specified. Could maybe be moved and done earlier, but
         * it will then break the use case of sending whatever fqpn you currently
         * have, and get a new one based on a new strategy you specify...
         * (since '/' is stripped during sanitization)
         */
        if ($options['sanitize_filename']) {
            $filename = static::sanitize_filename(($filename));
            if (0 === strlen($filename)) {
                return false;
            }
        }

        // Remove accented characters if specified
        if ($options['remove_accents'] && !empty($filename)) {
            $filename = static::remove_accents($filename);
            if (0 === strlen($filename)) {
                return false;
            }
        }

        /**
         * If $options['partition_length'] is given without specifying $options['num_partitions'] the number of
         * partitions is calculated automatically based on $filename length (without the extension).
         * If they don't divide evenly, the target number of buckets is reduced by one.
         */
        if ($options['partition_length'] > 0 && !$options['num_partitions']) {
            // Partition into at least 1 bucket since a partition length was specified
            $options['num_partitions'] = 1;

            // Determine bucket size
            $length = strlen($filename);
            if ($length >= $options['partition_length']) {
                // If it divides evenly, we're good, if not, it will be reduced by 1
                $options['num_partitions'] = (int) floor($length / $options['partition_length']);
            }
        }

        $do_partitioning = false;
        if ($options['partition_length'] >= 1 && $options['num_partitions'] >= 1) {
            $do_partitioning = true;
        }

        $partitions = array();
        if ($do_partitioning) {
            if (is_numeric($filename)) {
                // Reverse the numeric string to get an even distribution
                $filename_reversed = strrev($filename);

                // Zero-pad up to needed length
                $length         = strlen($filename_reversed);
                $minimal_length = $options['num_partitions'] * $options['partition_length'];
                if ($length < $minimal_length) {
                    $filename_reversed = str_pad($filename_reversed, $minimal_length, '0', STR_PAD_LEFT);
                }

                // Create desired number of partitions
                $partitions = str_split($filename_reversed, $options['partition_length']);
            } elseif (static::str_is_sha1($filename) || static::str_is_md5($filename)) {
                // Hashed filenames could be handled differently if needed...
                $partitions = str_split($filename, $options['partition_length']);
            } else {
                // This might be unexpected behavior... I guess we'll see
                $partitions = str_split($filename, $options['partition_length']);
            }
        }

        // Process the created partitions if there are any
        if (!empty($partitions)) {
            // Potentially limit the number of partitions
            if ($options['num_partitions'] > 0) {
                $partitions = array_slice($partitions, 0, $options['num_partitions']);
            }

            // Join partitions back
            $path = implode('/', $partitions);

            // Append the trailing path separator if it's missing
            if (!empty($path)) {
                $path .= substr($path, -1) !== '/' ? '/' : '';
            }
        }

        // Create the potentially missing directory (if told to do so)
        if ($options['create_missing_dirs']) {
            if (!empty($path)) {
                $target = $options['basedir'] . $path;
                if (!file_exists($target)) {
                    if (!$rs = @mkdir($target, 0777, true)) {
                        // Failed creating the directory
                        return false;
                    }
                }
            }
        }

        // Build the complete FQPN
        $path = $options['basedir'] . $path . $filename;

        // Add back the extension if we had it
        if (!empty($ext)) {
            $path .= '.' . $ext;
        }

        return $path;
    }

    public static function fqpn_options(array $overrides = array())
    {
        // Default partitioning options
        $opts = array(
            'basedir'             => '',
            'num_partitions'      => 2,
            'partition_length'    => 1,
            'create_missing_dirs' => false,
            'sanitize_filename'   => false
        );
        $opts = array_merge($opts, $overrides);

        return $opts;
    }

    public static function esc_html($str)
    {
        return htmlspecialchars((string) $str, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Builds and returns an array of the form [':field'] => '%q%' or
     * [':field'] => 'q' (depending on $mode) for the specified field names.
     * Such an array is then used to easily build/create the SQL WHERE clause(s).
     *
     * @param array $fields
     * @param string $q
     * @param string $mode
     *
     * @return array|null
     */
    public static function build_search_sql_params($fields = array(), $q = '', $mode = 'exact')
    {
        // $bind_tpl = ':%s:'; // phalcon
        $bind_tpl = ':%s'; // pdo

        $sql_params = null;

        if (!empty($q)) {
            $sql_params = array();

            if (!is_array($fields)) {
                $fields = array($fields);
            }

            switch ($mode) {
                case 'exact':
                case 'phrase':
                    $value_tpl = '%s';
                    break;
                case 'fuzzy':
                    $value_tpl = '%%%s%%';
                    break;
                case 'fuzzy-end':
                default:
                    $value_tpl = '%s%%';
                    break;
            }

            foreach ($fields as $field) {
                $bind_name = sprintf($bind_tpl, $field);
                $sql_params[$bind_name] = sprintf($value_tpl, $q);
            }
        }

        return $sql_params;
    }

    /**
     * Builds and returns an array of the form [':field:'] => '%q%' or
     * [':field:'] => 'q%' (depending on $mode) for the specified field names.
     * Such an array is then used in PHQL and/orWhere() calls and similar.
     *
     * @param array $fields
     * @param string $q
     * @param string $mode
     *
     * @return array|null
     */
    public static function build_phalcon_search_sql_params($fields = array(), $q = '', $mode = 'exact')
    {
        $sql_params = null;

        if (!empty($q) || trim($q) == '0') {
            $sql_params = array();

            if (!is_array($fields)) {
                $fields = array($fields);
            }

            switch ($mode) {
                case 'exact':
                case 'phrase':
                    $value_tpl = '%s';
                    break;
                case 'fuzzy':
                    $value_tpl = '%%%s%%';
                    break;
                case 'fuzzy-end':
                default:
                    $value_tpl = '%s%%';
                    break;
            }

            foreach ($fields as $field) {
                $sql_params[$field] = sprintf($value_tpl, $q);
            }
        }

        return $sql_params;
    }

    /**
     * Checks to see if a string is utf8 encoded.
     *
     * NOTE: This function checks for 5-Byte sequences, UTF8
     *       has Bytes Sequences with a maximum length of 4.
     *
     * @author bmorel at ssi dot fr (modified)
     *
     * @param string $str The string to be checked
     * @return bool True if $str fits a UTF-8 model, false otherwise.
     */
    public static function seems_utf8($str)
    {
        static::mbstring_binary_safe_encoding();
        $length = strlen($str);
        static::reset_mbstring_encoding();
        for ($i=0; $i < $length; $i++) {
            $c = ord($str[$i]);
            if ($c < 0x80) $n = 0; # 0bbbbbbb
            elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
            elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
            elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
            elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
            elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
            else return false; # Does not match any model
            for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                    return false;
            }
        }
        return true;
    }

    public static function mbstring_binary_safe_encoding($reset = false)
    {
        static $encodings = array();
        static $overloaded = null;

        if (is_null($overloaded)) {
            $overloaded = function_exists('mb_internal_encoding') && (ini_get('mbstring.func_overload') & 2);
        }

        if (false === $overloaded) {
            return;
        }

        if (!$reset) {
            $encoding = mb_internal_encoding();
            array_push($encodings, $encoding);
            mb_internal_encoding('ISO-8859-1');
        }

        if ($reset && $encodings) {
            $encoding = array_pop($encodings);
            mb_internal_encoding($encoding);
        }
    }

    public static function reset_mbstring_encoding()
    {
        static::mbstring_binary_safe_encoding(true);
    }

    /**
     * Converts all accent characters to ASCII characters.
     *
     * If there are no accent characters, then the string given is just returned.
     *
     * @param string $string Text that might have accent characters
     * @return string Filtered string with replaced "nice" characters.
     */
    public static function remove_accents($string)
    {
        // iconv-based approach is really hit & miss sometimes.. version-dependant + tricky for edge-cases
        // (example: "čćšđž-ČĆŠĐŽ" becomes "c'csdz-C'CSDZ" on my Windows machine...
        // return iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);

        if (!preg_match('/[\x80-\xff]/', $string)) {
            return $string;
        }

        if (static::seems_utf8($string)) {
            $chars = array(
                // Decompositions for Latin-1 Supplement
                chr(194).chr(170) => 'a', chr(194).chr(186) => 'o',
                chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
                chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
                chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
                chr(195).chr(134) => 'AE',chr(195).chr(135) => 'C',
                chr(195).chr(136) => 'E', chr(195).chr(137) => 'E',
                chr(195).chr(138) => 'E', chr(195).chr(139) => 'E',
                chr(195).chr(140) => 'I', chr(195).chr(141) => 'I',
                chr(195).chr(142) => 'I', chr(195).chr(143) => 'I',
                chr(195).chr(144) => 'D', chr(195).chr(145) => 'N',
                chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
                chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
                chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
                chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
                chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
                chr(195).chr(158) => 'TH',chr(195).chr(159) => 's',
                chr(195).chr(160) => 'a', chr(195).chr(161) => 'a',
                chr(195).chr(162) => 'a', chr(195).chr(163) => 'a',
                chr(195).chr(164) => 'a', chr(195).chr(165) => 'a',
                chr(195).chr(166) => 'ae',chr(195).chr(167) => 'c',
                chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
                chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
                chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
                chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
                chr(195).chr(176) => 'd', chr(195).chr(177) => 'n',
                chr(195).chr(178) => 'o', chr(195).chr(179) => 'o',
                chr(195).chr(180) => 'o', chr(195).chr(181) => 'o',
                chr(195).chr(182) => 'o', chr(195).chr(184) => 'o',
                chr(195).chr(185) => 'u', chr(195).chr(186) => 'u',
                chr(195).chr(187) => 'u', chr(195).chr(188) => 'u',
                chr(195).chr(189) => 'y', chr(195).chr(190) => 'th',
                chr(195).chr(191) => 'y', chr(195).chr(152) => 'O',
                // Decompositions for Latin Extended-A
                chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
                chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
                chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
                chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
                chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
                chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
                chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
                chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
                chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
                chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
                chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
                chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
                chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
                chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
                chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
                chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
                chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
                chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
                chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
                chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
                chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
                chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
                chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
                chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
                chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
                chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
                chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
                chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
                chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
                chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
                chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
                chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
                chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
                chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
                chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
                chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
                chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
                chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
                chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
                chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
                chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
                chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
                chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
                chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
                chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
                chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
                chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
                chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
                chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
                chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
                chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
                chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
                chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
                chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
                chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
                chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
                chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
                chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
                chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
                chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
                chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
                chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
                chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
                chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
                // Decompositions for Latin Extended-B
                chr(200).chr(152) => 'S', chr(200).chr(153) => 's',
                chr(200).chr(154) => 'T', chr(200).chr(155) => 't',
                // Euro Sign
                chr(226).chr(130).chr(172) => 'E',
                // GBP (Pound) Sign
                chr(194).chr(163) => '',
                // Vowels with diacritic (Vietnamese)
                // unmarked
                chr(198).chr(160) => 'O', chr(198).chr(161) => 'o',
                chr(198).chr(175) => 'U', chr(198).chr(176) => 'u',
                // grave accent
                chr(225).chr(186).chr(166) => 'A', chr(225).chr(186).chr(167) => 'a',
                chr(225).chr(186).chr(176) => 'A', chr(225).chr(186).chr(177) => 'a',
                chr(225).chr(187).chr(128) => 'E', chr(225).chr(187).chr(129) => 'e',
                chr(225).chr(187).chr(146) => 'O', chr(225).chr(187).chr(147) => 'o',
                chr(225).chr(187).chr(156) => 'O', chr(225).chr(187).chr(157) => 'o',
                chr(225).chr(187).chr(170) => 'U', chr(225).chr(187).chr(171) => 'u',
                chr(225).chr(187).chr(178) => 'Y', chr(225).chr(187).chr(179) => 'y',
                // hook
                chr(225).chr(186).chr(162) => 'A', chr(225).chr(186).chr(163) => 'a',
                chr(225).chr(186).chr(168) => 'A', chr(225).chr(186).chr(169) => 'a',
                chr(225).chr(186).chr(178) => 'A', chr(225).chr(186).chr(179) => 'a',
                chr(225).chr(186).chr(186) => 'E', chr(225).chr(186).chr(187) => 'e',
                chr(225).chr(187).chr(130) => 'E', chr(225).chr(187).chr(131) => 'e',
                chr(225).chr(187).chr(136) => 'I', chr(225).chr(187).chr(137) => 'i',
                chr(225).chr(187).chr(142) => 'O', chr(225).chr(187).chr(143) => 'o',
                chr(225).chr(187).chr(148) => 'O', chr(225).chr(187).chr(149) => 'o',
                chr(225).chr(187).chr(158) => 'O', chr(225).chr(187).chr(159) => 'o',
                chr(225).chr(187).chr(166) => 'U', chr(225).chr(187).chr(167) => 'u',
                chr(225).chr(187).chr(172) => 'U', chr(225).chr(187).chr(173) => 'u',
                chr(225).chr(187).chr(182) => 'Y', chr(225).chr(187).chr(183) => 'y',
                // tilde
                chr(225).chr(186).chr(170) => 'A', chr(225).chr(186).chr(171) => 'a',
                chr(225).chr(186).chr(180) => 'A', chr(225).chr(186).chr(181) => 'a',
                chr(225).chr(186).chr(188) => 'E', chr(225).chr(186).chr(189) => 'e',
                chr(225).chr(187).chr(132) => 'E', chr(225).chr(187).chr(133) => 'e',
                chr(225).chr(187).chr(150) => 'O', chr(225).chr(187).chr(151) => 'o',
                chr(225).chr(187).chr(160) => 'O', chr(225).chr(187).chr(161) => 'o',
                chr(225).chr(187).chr(174) => 'U', chr(225).chr(187).chr(175) => 'u',
                chr(225).chr(187).chr(184) => 'Y', chr(225).chr(187).chr(185) => 'y',
                // acute accent
                chr(225).chr(186).chr(164) => 'A', chr(225).chr(186).chr(165) => 'a',
                chr(225).chr(186).chr(174) => 'A', chr(225).chr(186).chr(175) => 'a',
                chr(225).chr(186).chr(190) => 'E', chr(225).chr(186).chr(191) => 'e',
                chr(225).chr(187).chr(144) => 'O', chr(225).chr(187).chr(145) => 'o',
                chr(225).chr(187).chr(154) => 'O', chr(225).chr(187).chr(155) => 'o',
                chr(225).chr(187).chr(168) => 'U', chr(225).chr(187).chr(169) => 'u',
                // dot below
                chr(225).chr(186).chr(160) => 'A', chr(225).chr(186).chr(161) => 'a',
                chr(225).chr(186).chr(172) => 'A', chr(225).chr(186).chr(173) => 'a',
                chr(225).chr(186).chr(182) => 'A', chr(225).chr(186).chr(183) => 'a',
                chr(225).chr(186).chr(184) => 'E', chr(225).chr(186).chr(185) => 'e',
                chr(225).chr(187).chr(134) => 'E', chr(225).chr(187).chr(135) => 'e',
                chr(225).chr(187).chr(138) => 'I', chr(225).chr(187).chr(139) => 'i',
                chr(225).chr(187).chr(140) => 'O', chr(225).chr(187).chr(141) => 'o',
                chr(225).chr(187).chr(152) => 'O', chr(225).chr(187).chr(153) => 'o',
                chr(225).chr(187).chr(162) => 'O', chr(225).chr(187).chr(163) => 'o',
                chr(225).chr(187).chr(164) => 'U', chr(225).chr(187).chr(165) => 'u',
                chr(225).chr(187).chr(176) => 'U', chr(225).chr(187).chr(177) => 'u',
                chr(225).chr(187).chr(180) => 'Y', chr(225).chr(187).chr(181) => 'y',
                // Vowels with diacritic (Chinese, Hanyu Pinyin)
                chr(201).chr(145) => 'a',
                // macron
                chr(199).chr(149) => 'U', chr(199).chr(150) => 'u',
                // acute accent
                chr(199).chr(151) => 'U', chr(199).chr(152) => 'u',
                // caron
                chr(199).chr(141) => 'A', chr(199).chr(142) => 'a',
                chr(199).chr(143) => 'I', chr(199).chr(144) => 'i',
                chr(199).chr(145) => 'O', chr(199).chr(146) => 'o',
                chr(199).chr(147) => 'U', chr(199).chr(148) => 'u',
                chr(199).chr(153) => 'U', chr(199).chr(154) => 'u',
                // grave accent
                chr(199).chr(155) => 'U', chr(199).chr(156) => 'u',
            );

            // Used for locale-specific rules
            /*
            $locale = get_locale();

            if ( 'de_DE' == $locale ) {
                $chars[ chr(195).chr(132) ] = 'Ae';
                $chars[ chr(195).chr(164) ] = 'ae';
                $chars[ chr(195).chr(150) ] = 'Oe';
                $chars[ chr(195).chr(182) ] = 'oe';
                $chars[ chr(195).chr(156) ] = 'Ue';
                $chars[ chr(195).chr(188) ] = 'ue';
                $chars[ chr(195).chr(159) ] = 'ss';
            } elseif ( 'da_DK' === $locale ) {
                $chars[ chr(195).chr(134) ] = 'Ae';
                $chars[ chr(195).chr(166) ] = 'ae';
                $chars[ chr(195).chr(152) ] = 'Oe';
                $chars[ chr(195).chr(184) ] = 'oe';
                $chars[ chr(195).chr(133) ] = 'Aa';
                $chars[ chr(195).chr(165) ] = 'aa';
            }
            */

            $string = strtr($string, $chars);
        } else {
            // Assume ISO-8859-1 if not UTF-8
            $chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
                .chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
                .chr(195).chr(196).chr(197).chr(199).chr(200).chr(201).chr(202)
                .chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
                .chr(211).chr(212).chr(213).chr(214).chr(216).chr(217).chr(218)
                .chr(219).chr(220).chr(221).chr(224).chr(225).chr(226).chr(227)
                .chr(228).chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
                .chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
                .chr(244).chr(245).chr(246).chr(248).chr(249).chr(250).chr(251)
                .chr(252).chr(253).chr(255);

            $chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";

            $string = strtr($string, $chars['in'], $chars['out']);
            $double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254));
            $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
            $string = str_replace($double_chars['in'], $double_chars['out'], $string);
        }

        return $string;
    }

    /**
     * Truncates a passed string to a given length, with the possibility to append
     * an ellipsis at the truncation point (or anything else).
     * Optionally breaks words and/or cuts the string from the middle
     * [in which case word breaking might not still be perfectly functional ;)]
     *
     * @param string $string The string to truncate
     * @param int $length The length to which to truncate
     * @param string $etc String to append upon truncation
     * @param bool $break_words Break words and truncate at the specified $length, or don't break words
     *                          (in which case the truncated string might be longer than the specified $length)
     * @param bool $middle Cut the string from the middle
     * @param string $charset Character set of the string
     *
     * @return string
     * @throws \Exception
     */
    public static function str_truncate($string, $length = 80, $etc = '…', $break_words = false, $middle = false, $charset = 'UTF-8')
    {
        if (empty($length)) {
            return '';
        }
        // Normalize newlines somewhat
        $string = preg_replace("/(\r\n)+|(\n|\r)+/", "\n", $string);
        if (function_exists('mb_strlen') && function_exists('mb_substr')) {
            if (mb_strlen($string, $charset) > $length) {
                $length -= min($length, mb_strlen($etc, $charset));
                if (!$break_words && !$middle) {
                    $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, ($length + 1), $charset));
                }
                if (!$middle) {
                    return mb_substr($string, 0, $length, $charset) . $etc;
                } else {
                    $half = ($length / 2);
                    $first = mb_substr($string, 0, $half, $charset);
                    $last = mb_substr($string, -($half));
                    return $first . $etc . $last;
                }
            } else {
                return $string;
            }
        } else {
            if ('iso-8859-1' === $charset || 'us-ascii' === $charset) {
                // These charsets should work with regular php5 string functions
                if (strlen($string) > $length) {
                    $length -= min($length, strlen($etc));
                    if (!$break_words && !$middle) {
                        $string = preg_replace('/\s+?(\S+)?$/u', '', substr($string, 0, ($length + 1)));
                    }
                    if (!$middle) {
                        return substr($string, 0, $length) . $etc;
                    } else {
                        $half = ($length / 2);
                        $first = substr($string, 0, $half);
                        $last = substr($string, -($half));
                        return $first . $etc . $last;
                    }
                } else {
                    return $string;
                }
            } else {
                // No mbstring functions available. Throw  as we cannot
                // produce correct results for multibyte strings
                throw new \Exception( 'No multibyte string functions available
                    (in ' . __METHOD__ . ') and you appear to be using a charset
                    which would require them (' . $charset . ')' );
            }
        }
    }

    /**
     * Truncates the string to the given length by breaking words and truncating
     * the string from the middle. Appends the $etc (ellipsis by default) parameter
     * at the truncation point.
     *
     * @param $string
     * @param int $length
     * @param string $etc
     * @param string $charset
     *
     * @return string
     * @throws \Exception
     */
    public static function str_truncate_middle($string, $length = 80, $etc = '…', $charset = 'UTF-8')
    {
        return static::str_truncate($string, $length, $etc, true, true, $charset);
    }

    /**
     * Truncates HTML, closes opened tags. UTF-8 aware, aware of unpaired tags
     * (which don't need a matching closing tag)
     *
     * @param string $html
     * @param int $max_length Desired maximum length of the resulting string
     * @param string $indicator Suffix to use if string was truncated. Defaults to '&hellip;'
     *
     * @return string
     */
    public static function str_truncate_html($html, $max_length, $indicator = '&hellip;' )
    {
        $output_length = 0; // Number of counted characters stored so far in $output
        $position      = 0;      // Character offset within input string after last tag/entity
        $tag_stack     = array(); // Stack of tags we've encountered but not closed
        $output        = '';
        $truncated     = false;

        /**
         * These tags don't have matching closing elements, in HTML (in XHTML they
         * theoretically need a closing /> )
         * @see http://www.netstrider.com/tutorials/HTMLRef/a_d.html
         * @see http://stackoverflow.com/questions/3741896/what-do-you-call-tags-that-need-no-ending-tag
         */
        $unpaired_tags = array(
            'doctype', '!doctype', 'area', 'base', 'basefont', 'bgsound', 'br', 'col',
            'embed', 'frame', 'hr', 'img', 'input', 'link', 'meta', 'param', 'sound', 'spacer', 'wbr'
        );

        // Loop through, splitting at HTML entities or tags
        while ($output_length < $max_length
            && preg_match('{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}', $html, $match, PREG_OFFSET_CAPTURE, $position))
        {
            list($tag, $tag_position) = $match[0];

            // Get text leading up to the tag, and store it (up to max_length)
            $text = mb_strcut($html, $position, $tag_position - $position);
            if ($output_length + mb_strlen($text) > $max_length)
            {
                $output .= mb_strcut($text, 0, $max_length - $output_length);
                $truncated = true;
                $output_length = $max_length;
                break;
            }

            // Store everything, it wasn't too long
            $output .= $text;
            $output_length += mb_strlen($text);

            if ($tag[0] == '&') {
                // Handle HTML entity by copying straight through
                $output .= $tag;
                $output_length++; // Only counted as one character
            } else {
                // Handle HTML tag
                $tag_inner = $match[1][0];
                if ('/' === $tag[1]) {
                    // This is a closing tag.
                    $output .= $tag;
                    // If input tags aren't balanced, we leave the popped tag
                    // on the stack so hopefully we're not introducing more
                    // problems.
                    if (end($tag_stack) == $tag_inner) {
                        array_pop($tag_stack);
                    }
                } elseif ('/' === $tag[mb_strlen($tag) - 2] || in_array(strtolower($tag_inner),$unpaired_tags)) {
                    // Self-closing or unpaired tag
                    $output .= $tag;
                } else {
                    // Opening tag.
                    $output .= $tag;
                    $tag_stack[] = $tag_inner; // Push tag onto the stack
                }
            }

            // Continue after the tag we just found
            $position = $tag_position + mb_strlen($tag);
        }

        // Print any remaining text after the last tag, if there's room.
        if ($output_length < $max_length && $position < mb_strlen($html)) {
            $output .= mb_strcut($html, $position, $max_length - $output_length);
        }

        $truncated = (mb_strlen($html) - $position) > ($max_length - $output_length);

        // Add terminator if it was truncated in loop or just above here
        if ($truncated) {
            $output .= $indicator;
        }

        // Close any open tags
        while (!empty($tag_stack)) {
            $output .= '</' . array_pop($tag_stack) . '>';
        }

        return $output;
    }

    /**
     * If <a href...> is longer than 40 chars, shorten the text between
     * <a...> and </a> and return it
     *
     * @param $string
     *
     * @return mixed
     */
    public static function shorten_links($string)
    {
        $string = preg_replace_callback(
            '/(<a[^>]+>)([^<\s]{40,})(<\/a>)/i',
            function($matches) {
                return stripslashes($matches[1] . substr($matches[2], 0, 20) . '&hellip;&shy;' . substr($matches[2], -20) . $matches[3]);
            },
            $string
        );
        return $string;
    }

    /**
     * Transforms plain text links into clickable html
     *
     * @param $string
     * @param array $options
     *
     * @return mixed
     */
    public static function linkify_text($string, $options = array('attr' => ''))
    {
        $pattern = '~(?xi)
              (?:
                ((ht|f)tps?://)                    # scheme://
                |                                  #   or
                www\d{0,3}\.                       # "www.", "www1.", "www2." ... "www999."
                |                                  #   or
                www\-                              # "www-"
                |                                  #   or
                [a-z0-9.\-]+\.[a-z]{2,4}(?=/)      # looks like domain name followed by a slash
              )
              (?:                                  # Zero or more:
                [^\s()<>]+                         # Run of non-space, non-()<>
                |                                  #   or
                \(([^\s()<>]+|(\([^\s()<>]+\)))*\) # balanced parens, up to 2 levels
              )*
              (?:                                  # End with:
                \(([^\s()<>]+|(\([^\s()<>]+\)))*\) # balanced parens, up to 2 levels
                |                                  #   or
                [^\s`!\-()\[\]{};:\'".,<>?«»“”‘’]  # not a space or one of these punct chars
              )
        ~';

        $callback = function ($match) use ($options) {
            $caption = $match[0];
            $pattern = "~^(ht|f)tps?://~";

            if (0 === preg_match ($pattern, $match[0] ) ) {
                $match[0] = 'http://' . $match[0];
            }

            if ( isset( $options['callback'] ) ) {
                $cb = $options['callback']( $match[0], $caption, false );
                if ( ! is_null( $cb ) ) {
                    return $cb;
                }
            }

            return sprintf( '<a href="%s"%s>%s</a>', $match[0], $options['attr'], $caption );
        };

        $string = preg_replace_callback($pattern, $callback, $string);

        $string = static::shorten_links( $string );
        return $string;
    }

    /**
     * Tries to parse the string as an URL and prepends
     * a protocol to it if it's not detected.
     * Returns null if parsing fails.
     *
     * @param string $url
     * @param null|string $default_scheme Optional. Defaults to 'http'.
     *
     * @return null|string
     */
    public static function fix_url($url, $default_scheme = null)
    {
        $fixed_url = null;

        $url = trim($url);
        // Short-circuit for stupid cases such as '1'
        if (empty($url) || is_numeric($url)) {
            return $fixed_url;
        }

        $parsed = @parse_url($url);

        if (!$default_scheme) {
            $default_scheme = 'http';
        }

        if ($parsed) {
            $allowed_schemes = array(
                'http',
                'https'
            );
            // Check the url scheme and replace it with default if not one of above
            $scheme = (isset($parsed['scheme']) && !empty($parsed['scheme'])) ? $parsed['scheme'] : null;
            if ($scheme) {
                if (!in_array($scheme, $allowed_schemes)) {
                    $scheme = $default_scheme;
                }
                // replace everything
                // from the beginning of string up to and including '://' with
                // our default scheme
                // /\s*[^:]+:\/\//
                $fixed_url = preg_replace('#^[^:/.]{2,6}[:/]{1,3}#i', $scheme . '://', $url);
            } else {
                // no scheme present, prepend our default
                $fixed_url = $default_scheme . '://' . $url;
            }
        }

        return $fixed_url;
    }

    public static function format_filesize($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format(round($bytes / 1073741824, 2), 2) . ' Gb';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format(round($bytes / 1048576, 2), 2) . ' Mb';
        } elseif ($bytes >= 1024) {
            $bytes = number_format(round($bytes / 1024, 2), 2) . ' Kb';
        } else {
            $bytes = $bytes . ' b';
        }
        return $bytes;
    }

    /**
     * Get a filename that is unique for the given directory.
     *
     * If the filename is not unique, then a number will be added to the filename
     * (before the extension), and will continue adding numbers until the filename is
     * unique.
     *
     * The callback is passed three parameters, the first one is the directory, the
     * second is the filename, and the third is the extension.
     *
     * @param string $dir
     * @param string $filename
     * @param string|Closure $unique_filename_callback Callback.
     * @return string New filename, if given wasn't unique.
     */
    public static function build_unique_filename($dir, $filename,  $unique_filename_callback = null)
    {
        // TODO: sanitize the filename?

        // Separate the filename into a name and extension
        $info = pathinfo($filename);
        $ext = !empty($info['extension']) ? '.' . $info['extension'] : '';
        $name = basename($filename, $ext);

        // Edge case: if file is named '.ext', treat as an empty name
        if ($name === $ext) {
            $name = '';
        }

        // Increment the file number until we have a unique file to save in $dir. Use callback if supplied.
        if ($unique_filename_callback && is_callable($unique_filename_callback)) {
            $filename = call_user_func($unique_filename_callback, $dir, $name, $ext);
        } else {
            $number = '';

            // Change '.ext' to lower case
            if ($ext && strtolower($ext) != $ext) {
                $ext2 = strtolower($ext);
                $filename2 = preg_replace('|' . preg_quote($ext) . '$|', $ext2, $filename);

                // Check for both lower and upper case extension or image sub-sizes may be overwritten
                while (file_exists($dir . '/' . $filename) || file_exists($dir . '/' . $filename2)) {
                    $new_number = (string) ((int) $number + 1);
                    $filename = str_replace($number . $ext, $new_number . $ext, $filename);
                    $filename2 = str_replace($number . $ext2, $new_number . $ext2, $filename2);
                    $number = $new_number;
                }
                return $filename2;
            }

            while (file_exists($dir . '/' . $filename)) {
                if ('' === "$number$ext") {
                    $filename = $filename . ++$number . $ext;
                } else {
                    $filename = str_replace($number . $ext, ++$number . $ext, $filename);
                }
            }
        }

        return $filename;
    }

    /**
     * Retrieve a modified URL query string.
     *
     * You can rebuild the URL and append a new query variable to the URL query by
     * using this function. You can also retrieve the full URL with query data.
     *
     * Adding a single key & value or an associative array.
     * Setting a key value to an empty string removes the key.
     * Omitting oldquery_or_uri uses the $_SERVER value.
     * Additional values provided are expected to be encoded appropriately
     * with urlencode() or rawurlencode().
     *
     * @param string|array ... $param1 Either newkey or an associative_array.
     * @param string ... $param2 Either newvalue or oldquery or URI.
     * @param string ... $param3 Optional. Old query or URI.
     * @return mixed|string New URL query string.
     */
    public static function add_query_arg()
    {
        $args = func_get_args();
        if (is_array($args[0])) {
            if (count($args) < 2 || false === $args[1]) {
                $uri = $_SERVER['REQUEST_URI'];
            } else {
                $uri = $args[1];
            }
        } else {
            if (count($args) < 3 || false === $args[2]) {
                $uri = $_SERVER['REQUEST_URI'];
            } else {
                $uri = $args[2];
            }
        }

        if ($frag = strstr($uri, '#')) {
            $uri = substr($uri, 0, -strlen($frag));
        } else {
            $frag = '';
        }

        if (0 === stripos($uri, 'http://')) {
            $protocol = 'http://';
            $uri = substr($uri, 7);
        } elseif (0 === stripos($uri, 'https://')) {
            $protocol = 'https://';
            $uri = substr($uri, 8);
        } else {
            $protocol = '';
        }

        if (strpos($uri, '?') !== false) {
            list($base, $query) = explode('?', $uri, 2);
            $base .= '?';
        } elseif ($protocol || strpos($uri, '=') === false) {
            $base = $uri . '?';
            $query = '';
        } else {
            $base = '';
            $query = $uri;
        }

        parse_str($query, $qs);

        // TODO: might need these to be truly perfect
        // $qs = static::urlencode_deep($qs); // this should re-encode things that were already in the query string
        // $qs = static::rawurlencode_deep($qs); // this should re-encode things that were already in the query string

        if (is_array($args[0])) {
            $keyvals = $args[0];
            $qs = array_merge($qs, $keyvals);
        } else {
            $qs[$args[0]] = $args[1];
        }

        foreach ($qs as $k => $v) {
            if (false === $v) {
                unset($qs[$k]);
            }
        }

        $ret = http_build_query($qs, null, '&', false);
        $ret = trim($ret, '?');
        $ret = preg_replace('#=(&|$)#', '$1', $ret);
        $ret = $protocol . $base . $ret . $frag;
        $ret = rtrim($ret, '?');

        return $ret;
    }

    public static function remove_query_var($key, $url = false)
    {
        if (is_array($key)) {
            foreach ($key as $k) {
                $url = static::add_query_arg($k, false, $url);
            }
            return $url;
        }

        return static::add_query_arg($key, false, $url);
    }

    /**
     * Navigates through an array and encodes the values to be used in a URL.
     *
     * @param array|string $value The array or string to be encoded.
     *
     * @return array|string $value The encoded array (or string from the callback).
     */
    public static function urlencode_deep($value)
    {
        return is_array($value) ? array_map(array(__CLASS__, __METHOD__), $value) : urlencode($value);
    }

    /**
     * Navigates through an array and raw encodes the values to be used in a URL.
     *
     * @param array|string $value The array or string to be encoded.
     *
     * @return array|string $value The encoded array (or string from the callback).
     */
    public static function rawurlencode_deep($value)
    {
        return is_array($value) ? array_map(array(__CLASS__, __METHOD__), $value) : rawurlencode($value);
    }

    /**
     * Helper to gracefully handle the special case when uploaded file exceeds PHP's `post_max_size`
     * php.ini directive (in which case both $_POST and $_FILES come up empty, and you really
     * can't tell the user why the upload failed, even though it did.
     *
     * @return bool|string False if everything's fine, a string containing the detailed error message if it's not
     */
    public static function is_post_size_exceeded()
    {
        $max_post_size = static::ini_get_bytes('post_max_size');

        if (isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] > $max_post_size) {
            $msg = sprintf(
                'php.ini `post_max_size` exceeded! (got %s bytes but limit is %s bytes)',
                $_SERVER['CONTENT_LENGTH'],
                $max_post_size
            );
            return $msg;
        }

        return false;
    }

    /**
     * @param $ini_key
     *
     * @return int|string
     */
    public static function ini_get_bytes($ini_key)
    {
        $val = trim(ini_get($ini_key));

        if ('' !== $val) {
            $last = strtolower($val{strlen($val) - 1});
        } else {
            $last = '';
        }

        // The break statements are missing on purpose!
        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }

        return $val;
    }

    /**
     * Returns a string representation of $number for currency/price display purposes.
     * If $number is not a whole number (or if $force_decimals = true), the resulting string
     * will have 2 digits after the decimal separator.
     *
     * @param string|int|float $number
     * @param bool $force_decimals Optional, defaults to false. Force 2 decimal digits in the resulting string even when formatting a whole number.
     *
     * @return string
     */
    public static function format_money($number, $force_decimals = false)
    {
        if (!is_numeric($number)) {
            $number = 0;
        }

        $decimal_places = 0;
        if ($force_decimals || !static::is_whole_number($number)) {
            $decimal_places = 2;
        }

        return number_format($number, $decimal_places, ',', '.');
    }

    public static function is_whole_number($input)
    {
        return floatval($input) == intval($input);
    }

    /**
     * Returns a "slug" from a string (for use in URLs).
     *
     * @param $string
     *
     * @return string
     */
    public static function slugify($string)
    {
        $slugify = new Slugify();

        // https://en.wikipedia.org/wiki/Tshe
        $slugify->addRule('ћ', 'c');

        $string = $slugify->slugify($string);

        return $string;
    }

    /**
     * Convert a string or float/int from kn to lp (Croatian kuna minor currency unit, 1 kn = 100 lp)
     *
     * Since floats are bad (rounding issues), we're using this to always work with integers represented as strings
     * (representing the price value in the smallest currency units, in this case Croatian lipas)
     *
     * @param string|float|int $amount
     *
     * @return string
     */
    public static function kn2lp($amount)
    {
        if (is_string($amount)) {
            $amount = static::parse_currency($amount);
        }

        $amount = bcmul($amount, 100);

        return $amount;
    }

    /**
     * Convert $amount from lp to kn (1 kn = 100 lp)
     * Returns the result as a string with 2 decimal digits by default (separated by a '.')
     *
     * It supports fractional/float inputs, even if it doesn't make sense from
     * a real-world point of view (since lipas are the smallest possible currency unit and cannot be fractional).
     * Meaning we accept pretty much any input and discard the impossible part.
     *
     * @param int|float|string $amount
     * @param int $decimal_digits Optional, defaults to 2. Number of decimal digits shown in the resulting string
     *
     * @return string
     */
    public static function lp2kn($amount, $decimal_digits = 2)
    {
        if (is_string($amount)) {
            $amount = static::parse_currency($amount);
        }

        $kunas = bcdiv($amount, 100, $decimal_digits);

        return $kunas;
    }

    /**
     * Attempts to parse and return the value as a float regardless of locales/separators.
     * WARNING: Fails when the decimal part of $money has more than two digits.
     *
     * @param string $money
     *
     * @return float
     */
    public static function parse_currency($money)
    {
        $clean_string = preg_replace('/([^0-9\.,])/i', '', $money);
        $numbers_only = preg_replace('/([^0-9])/i', '', $money);

        $separators_cnt_erase = strlen($clean_string) - strlen($numbers_only) - 1;

        $string_with_comma_or_dot = preg_replace('/([,\.])/', '', $clean_string, $separators_cnt_erase);
        $removed_thousands_sep    = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $string_with_comma_or_dot);

        return (float) str_replace(',', '.', $removed_thousands_sep);
    }

    /**
     * Get time when the request was made. In case there isn't such information
     * available, we return current system time
     *
     * @return int
     */
    public static function getRequestTime()
    {
        static $request_time = null;

        if (null === $request_time) {
            $request_time = $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] : time();
        }

        return $request_time;
    }

    /**
     * Check value to find if it was serialized.
     *
     * If $data is not an string, then returned value will always be false.
     * Serialized data is always a string.
     *
     * @param string $data Value to check to see if was serialized.
     * @param bool $strict Optional. Whether to be strict about the end of the string. Default true.
     *
     * @return bool False if not serialized and true if it was.
     */
    public static function is_serialized($data, $strict = true)
    {
        if (!is_string($data)) {
            return false;
        }

        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }

        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace     = strpos($data, '}');
            // Either ; or } must exist.
            if (false === $semicolon && false === $brace) {
                return false;
            }
            // But neither must be in the first X characters.
            if (false !== $semicolon && $semicolon < 3) {
                return false;
            }
            if (false !== $brace && $brace < 4) {
                return false;
            }
        }

        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            // Or else fall through
            case 'a' :
            case 'O' :
                return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool) preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }

        return false;
    }

    public static function count_digits($str)
    {
        return preg_match_all( '/[0-9]/', $str);
    }

    /**
     * Builds 'poziv-na-broj-odobrenja' aka 'OFFERNUMBER' aka '<pbo>' based
     * on custom rules defined by Oglasnik d.o.o.:
     * - has to start with '900'
     * - must contain 9 chars/digits total
     *
     * The behavior for $id > 999999 is not really specified, so good luck
     * to whoever gets to deal with this at some point in the future!
     *
     * @param $id
     *
     * @return string
     */
    public static function build_pbo($id)
    {
        $prefix      = Orders::PBO_PREFIX;
        $pad_length  = 6;

        $id = (int) $id;

        // TODO: not really sure we want this... might be better to throw an exception if this happens?
        if ($id > 999999) {
            // we're reducing the prefix to '90', or just '9' for now, but that can also
            // become tricky if some other parts of the code rely on strict prefix length of 3?
            $number = $id;
            $digits_count = Utils::count_digits($number);
            $diff         = $digits_count - $pad_length;
            $prefix       = substr($prefix, 0, -$diff);
        }

        $offer_num = $prefix . str_pad($id, $pad_length, '0', STR_PAD_LEFT);

        return $offer_num;
    }

    public static function str_has_pbo_prefix($pbo)
    {
        // Utils::build_pbo() returns a 9 chars/digits string
        if (strlen($pbo) <= 8) {
            return false;
        }

        $prefix = Orders::PBO_PREFIX;
        $regex = '/^' . $prefix{0} . '0{1,2}/';

        return (bool) preg_match($regex, $pbo);
    }

    /**
     * @param string $pbo PBO (poziv-na-broj-odobrenja)
     *
     * @return string
     */
    public static function str_remove_pbo_prefix($pbo)
    {
        // PBOs are 9 chars/digits (at least for now), so just return original string (aka don't do anything)
        if (strlen($pbo) <= 8) {
            return $pbo;
        }

        $prefix = Orders::PBO_PREFIX;
        $regex = '/^' . $prefix{0} . '0{1,2}/';

        // remove the prefix
        $pbo = preg_replace($regex, '', $pbo);

        // remove potential leading zeroes (without converting to int, since that's platform dependant)
        return ltrim($pbo, '0');
    }

    public static function str_contains_boolean_mode_operators($string)
    {
        // $regex = '/[\<\>\~\-\+\*\(\)\"]/';
        $regex = '/([\<\>\~\-\+\*\(\)\"]\w)|(\w[\<\>\~\-\+\*\(\)\"])/';

        return preg_match($regex, $string);
    }

    public static function strip_boolean_operator_characters($string)
    {
        $string = str_replace(array('+', '<', '>', '-', '*', '(', ')', '~'), '', $string);

        return $string;
    }

    public static function build_boolean_mode_operators($search_terms, $words_prefix = '+', $words_suffix = null)
    {
        // Check if there are any special chars in there already and bail early assuming
        // whoever sent these knows what they're doing
        if (self::str_contains_boolean_mode_operators($search_terms)) {
            return $search_terms;
        }

        $search_terms       = trim($search_terms);
        $words = explode(' ', $search_terms);

        if (count($words) > 1) {
            // If there's more than one word present, prefix each one with a '+' sign
            $changed = array();

            foreach ($words as $word) {
                $changed[] = $words_prefix . $word . $words_suffix;
            }

            $search_terms = implode(' ', $changed);
        } else {
            // If there's only a single word, stick an asterisk at the end
            $search_terms = $search_terms . '*';
        }

        return $search_terms;
    }

    /**
     * Reduces multiple occurrences of various punctuation characters down to a single one.
     *
     * @param string $str
     *
     * @return string
     */
    public static function str_normalize_punctuation($str)
    {
        // First normalize whitespace
        $str = preg_replace('/\h+/u', ' ', $str);

        // Characters specified by Oglasnik: !, ?, ., ;, ,, -, (), "", /, :, ...
        $char_list = '-.!_\?;:()"\/\'';

        // Removes above characters from the beginning of string
        // $str = preg_replace('/^\PL+/u', '', $str);
        $str = ltrim($str, $char_list);

        // Reduce punctuation chars with multiple occurrences to a single one
        $regex = '/(?:([' . $char_list . '])\1+)/';
        // Perhaps a better variant, but we'll see
        // $regex = '/(?:(\pP)\1+)+/u';

        $normalized = trim(preg_replace($regex, '$1', $str));

        return $normalized;
    }

    /**
     * Converts uppercase words longer than the specified number of chars to lowercase.
     *
     * @param string $str
     * @param int $word_length
     *
     * @return string mixed
     */
    public static function str_no_screaming($str, $word_length = 4)
    {
        // Lowercase any ALL CAPS STRINGS longer than $word_length
        $result = preg_replace_callback(
            '/(\p{Lu}{' . $word_length . ',})/u',
            function ($matches) {
                return mb_strtolower($matches[0], 'UTF-8');
            },
            $str
        );

        return $result;
    }

    /**
     * Convert given phone number in a format suitable for AVUS
     * Suitable formats are:
     *     +38511234567
     *     01/1234567
     *     091/1234567
     *
     * @param  string $phoneNumber
     *
     * @return string|null
     */
    public static function convert_phone_number_to_avus_format($phoneNumber)
    {
        $convertedPhoneNumber = null;

        if ($parsedPhoneNumber = self::parsePhoneNumber($phoneNumber)) {
            $convertedPhoneNumber = $parsedPhoneNumber['international'];

            if ($parsedPhoneNumber['valid'] && $parsedPhoneNumber['formattedNationalNumber']) {
                $convertedPhoneNumber = str_replace('-', '', $parsedPhoneNumber['formattedNationalNumber']);
            }
        }

        return $convertedPhoneNumber;
    }

    public static function parsePhoneNumber($phoneNumber, $countryISOCode = 'HR')
    {
        $parsedPhoneNumber = null;

        if (!empty($phoneNumber)) {
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            try {

                if (substr($phoneNumber, 0, 2) == '00') {
                    // detect number entered as 00xxxxxx instead of +xxxxxx
                    $phoneNumber = '+' . substr($phoneNumber, 2);
                } elseif (substr($phoneNumber, 0, 1) == '0') {
                    // detect if maybe phone number is in 'classic' format xxx/yyyx-yxy
                    $possiblePhoneNumber = null;
                    $workingPhoneNumber  = preg_replace('/[^0-9]/', '', $phoneNumber);
                    if (strlen($workingPhoneNumber) >= 9) {
                        $possiblePhoneNumber = '+385' . substr($workingPhoneNumber, 1);
                    }
                    if ($possiblePhoneNumber) {
                        $phoneNumber = $possiblePhoneNumber;
                    }
                }

                if (substr($phoneNumber, 0, 1) == '+') {
                    $numberProto = $phoneUtil->parse($phoneNumber, null);
                } else {
                    $numberProto = $phoneUtil->parse($phoneNumber, $countryISOCode);
                }

                $parsedPhoneNumber = array(
                    'valid'                    => $phoneUtil->isValidNumber($numberProto),
                    'international'            => $phoneNumber,
                    'nationalNumberWithPrefix' => $phoneNumber,
                    'formattedNationalNumber'  => $phoneNumber
                );

                if ($parsedPhoneNumber['valid']) {
                    $parsedPhoneNumber['international']            = (string) trim($phoneUtil->format($numberProto, \libphonenumber\PhoneNumberFormat::E164));
                    $parsedPhoneNumber['countryCode']              = (int) trim($numberProto->getCountryCode());
                    $parsedPhoneNumber['nationalNumberWithPrefix'] = (string) trim($phoneUtil->format($numberProto, \libphonenumber\PhoneNumberFormat::NATIONAL));

                    if ($parsedPhoneNumber['nationalNumberWithPrefix']) {
                        if (385 == $parsedPhoneNumber['countryCode']) {
                            if ('0800' == substr($parsedPhoneNumber['nationalNumberWithPrefix'], 0, 4)) {
                                $prefix         = '0800';
                                $customerNumber = str_replace(' ', '-', trim(substr($parsedPhoneNumber['nationalNumberWithPrefix'], 4)));
                            } elseif ('01' == substr($parsedPhoneNumber['nationalNumberWithPrefix'], 0, 2)) {
                                $prefix         = '01';
                                $customerNumber = str_replace(' ', '-', trim(substr($parsedPhoneNumber['nationalNumberWithPrefix'], 2)));
                            } else {
                                $prefix         = substr($parsedPhoneNumber['nationalNumberWithPrefix'], 0, 3);
                                $customerNumber = str_replace(' ', '-', trim(substr($parsedPhoneNumber['nationalNumberWithPrefix'], 3)));
                            }
                            $parsedPhoneNumber['nationalPrefix']          = trim($prefix);
                            $parsedPhoneNumber['nationalNumber']          = trim($customerNumber);
                            $parsedPhoneNumber['formattedNationalNumber'] = self::formatNationalNumber($customerNumber, $prefix);
                        } else {
                            $nationalNumberWithPrefixParts = explode(' ', $parsedPhoneNumber['nationalNumberWithPrefix']);
                            if (count($nationalNumberWithPrefixParts) >= 2) {
                                $parsedPhoneNumber['nationalPrefix']          = trim($nationalNumberWithPrefixParts[0]);
                                $parsedPhoneNumber['nationalNumber']          = trim(implode('-', array_slice($nationalNumberWithPrefixParts, 1)));
                                $parsedPhoneNumber['formattedNationalNumber'] = '(+' . $parsedPhoneNumber['countryCode'] . ') ' . self::formatNationalNumber($parsedPhoneNumber['nationalNumber'], $parsedPhoneNumber['nationalPrefix']);
                            } else {
                                $parsedPhoneNumber['formattedNationalNumber'] = '(+' . $parsedPhoneNumber['countryCode'] . ') ' . $parsedPhoneNumber['nationalNumberWithPrefix'];
                            }
                        }
                    } else {
                        $parsedPhoneNumber['formattedNationalNumber'] = phoneNumber;
                    }
                }
            } catch (\libphonenumber\NumberParseException $e) {
                Bootstrap::log($e);
            }
        }

        return $parsedPhoneNumber;
    }

    public static function formatNationalNumber($number, $prefix = null)
    {
        return (string) (trim($prefix) ? trim($prefix) . '/' : '') . trim($number);
    }

    public static function quote_unquoted_words_with_dots($str)
    {
        $words = explode(' ', $str);
        $count = count($words);
        if ($count > 0) {
            foreach ($words as $k => $word) {
                // If word contains a dot and doesn't contain a quote
                if ((false !== strpos($word, '.')) && (false === strpos($word, '"'))) {
                    $word = '"' . $word . '"';
                }
                $words[$k] = $word;
            }
            $str = implode(' ', $words);
        }

        return $str;
    }

    public static function category_name_markup($str)
    {
        $words = explode(' ', $str);
        $count = count($words);
        if ($count > 1) {
            $str  = implode(' ', array_slice($words, 0, -1));
            $str .= ' <b>' . $words[($count - 1)] . '</b>';
        } else {
            $str = '<b>' . $words[0] . '</b>';
        }

        return $str;
    }

    /**
     * Generates specified number of random bytes. Output is binary string, not ASCII.
     *
     * @param integer $length the number of bytes to generate
     *
     * @return string the generated random bytes
     * @throws \Exception
     */
    public static function random_bytes($length)
    {
        if (function_exists('random_bytes')) {
            return random_bytes($length);
        }

        if (!is_int($length) || $length < 1) {
            throw new \Exception('Invalid first parameter ($length)');
        }

        // The recent LibreSSL RNGs are faster and likely better than /dev/urandom.
        // Parse OPENSSL_VERSION_TEXT because OPENSSL_VERSION_NUMBER is no use for LibreSSL.
        // https://bugs.php.net/bug.php?id=71143
        static $libreSSL;
        if ($libreSSL === null) {
            $libreSSL = defined('OPENSSL_VERSION_TEXT')
                && preg_match('{^LibreSSL (\d\d?)\.(\d\d?)\.(\d\d?)$}', OPENSSL_VERSION_TEXT, $matches)
                && (10000 * $matches[1]) + (100 * $matches[2]) + $matches[3] >= 20105;
        }
        // Since 5.4.0, openssl_random_pseudo_bytes() reads from CryptGenRandom on Windows instead
        // of using OpenSSL library. Don't use OpenSSL on other platforms.
        if ($libreSSL === true
            || (DIRECTORY_SEPARATOR !== '/'
                && PHP_VERSION_ID >= 50400
                && substr_compare(PHP_OS, 'win', 0, 3, true) === 0
                && function_exists('openssl_random_pseudo_bytes'))
        ) {
            $key = openssl_random_pseudo_bytes($length, $cryptoStrong);
            if ($cryptoStrong === false) {
                throw new \Exception(
                    'openssl_random_pseudo_bytes() set $crypto_strong false. Your PHP setup is insecure.'
                );
            }
            if ($key !== false && mb_strlen($key, '8bit') === $length) {
                return $key;
            }
        }
        // mcrypt_create_iv() does not use libmcrypt. Since PHP 5.3.7 it directly reads
        // CrypGenRandom on Windows. Elsewhere it directly reads /dev/urandom.
        if (PHP_VERSION_ID >= 50307 && function_exists('mcrypt_create_iv')) {
            $key = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
            if (mb_strlen($key, '8bit') === $length) {
                return $key;
            }
        }
        // If not on Windows, try a random device.
        if (DIRECTORY_SEPARATOR === '/') {
            // urandom is a symlink to random on FreeBSD.
            $device = PHP_OS === 'FreeBSD' ? '/dev/random' : '/dev/urandom';
            // Check random device for speacial character device protection mode. Use lstat()
            // instead of stat() in case an attacker arranges a symlink to a fake device.
            $lstat = @lstat($device);
            if ($lstat !== false && ($lstat['mode'] & 0170000) === 020000) {
                $key = @file_get_contents($device, false, null, 0, $length);
                if ($key !== false && mb_strlen($key, '8bit') === $length) {
                    return $key;
                }
            }
        }

        throw new \Exception('Unable to generate a random key');
    }

    /**
     * Generates a random UUID using the secure RNG.
     *
     * Returns Version 4 UUID format: xxxxxxxx-xxxx-4xxx-Yxxx-xxxxxxxxxxxx where x is
     * any random hex digit and Y is a random choice from 8, 9, a, or b.
     *
     * @return string the UUID
     */
    public static function random_uuid()
    {
        $bytes = static::random_bytes(16);
        $bytes[6] = chr((ord($bytes[6]) & 0x0f) | 0x40);
        $bytes[8] = chr((ord($bytes[8]) & 0x3f) | 0x80);
        $id = str_split(bin2hex($bytes), 4);

        return "{$id[0]}{$id[1]}-{$id[2]}-{$id[3]}-{$id[4]}-{$id[5]}{$id[6]}{$id[7]}";
    }

    /**
     * Dirty helper to avoid duplicating code for generating "top" (smaller) pagination
     * markup for new category/search/section listings...
     *
     * @param string $markup
     *
     * @return string
     */
    public static function get_top_pagination_markup($markup = '')
    {
        return str_replace('<ul class="pagination">', '<ul class="pagination pagination-md">', $markup);
    }

    /**
     * Dirty helper to avoid duplicating code for generating "top" (smaller) pagination
     * markup for new category/search/section listings...
     *
     * @param string $markup
     *
     * @return string
     */
    public static function get_bottom_pagination_markup($markup = '')
    {
        return str_replace('<ul class="pagination">', '<ul class="pagination pagination-bottom">', $markup);
    }

    /**
     * Goes through all the links in $markup and adds $anchor param to their href attributes
     * if they don't already have an anchor.
     *
     * @param string $markup
     * @param string $anchor
     *
     * @return string
     */
    public static function anchorify_links($markup, $anchor = 'classifieds')
    {
        $modified = false;
        $dom      = Utils::html2dom($markup);
        $links    = $dom->getElementsByTagName('a');

        foreach ($links as $link) {
            $href = $link->getAttribute('href');

            // If it doesn't have an anchor already, add it
            if ($anchor && false === strpos($href, '#')) {
                $href .= '#' . $anchor;
                $link->setAttribute('href', $href);
                $modified = true;
            }
        }

        if ($modified) {
            $markup = Utils::dom2html($dom);
        }

        return $markup;
    }

    /**
     * Modifies passed in $count according to rules specified by Oglasnik team.
     * They want inflated view counts because... who knows. So there's a method for it.
     *
     * The crude formula given was this:
     * ```
     * 1-10 x 4,5
     * 11-50 x 4 + 4,5
     * 51-100 x 3 + 54,5
     * 101-1000 x 2 + 154,5
     * 1001 - (neodredjeno) x 1 + 1154,5
     * ```
     *
     * They said rounding down was fine, so we round down.
     * @link https://trello.com/c/UkWWSlP0/563-
     *
     * @param int/float $count
     *
     * @return float
     */
    public static function inflate_view_count_numbers($count)
    {
        $count = (int) $count;

        // Treat 0 or potentially negative values as if it was 1
        if ($count <= 0) {
            $count = 1;
        }

        if ($count >= 1 && $count <= 10) {
            $inflated = $count * 4.5;
        } elseif ($count >= 11 && $count <= 50) {
            $inflated = ($count * 4) + 4.5;
        } elseif ($count >= 51 && $count <= 100) {
            $inflated = ($count * 3) + 54.5;
        } elseif ($count >= 101 && $count <= 1000) {
            $inflated = ($count * 2) + 154.5;
        } elseif ($count >= 1001) {
            $inflated = $count + 1154.5;
        }

        return floor($inflated);
    }

    public static function innerHTML($element)
    {
        $html = '';

        $children = $element->childNodes;
        foreach ($children as $child) {
            $tmp_dom = new \DOMDocument();
            $tmp_dom->appendChild($tmp_dom->importNode($child, true));
            $html .= trim($tmp_dom->saveHTML());
        }

        return $html;
    }

    /**
     * Modifies markup generated for email templates/layouts to strip out
     * certain things we don't need to have in there when the message is going
     * to be displayed in user's site inbox.
     * Currently removing:
     * - first <b>Oglasnik d.o.o.</b> element inside .box-body
     * - several empty/un-needed <tr> elements
     *
     * @param string $markup
     *
     * @return string
     */
    public static function modifyEmailBodyMarkupForSiteInbox($markup)
    {
        $dom      = Utils::html2dom($markup);

        // Delete a bunch of table rows
        $xpath = new \DOMXPath($dom);
        $rows = $xpath->query('//tr//tr'); // we're inside a nested table
        $cnt = 0;
        $rows_to_delete = array(1, 2, 3, 8, 11, 16, 17); // Depends on email layout/template design
        foreach ($rows as $row) {
            $cnt++;
            if (in_array($cnt, $rows_to_delete)) {
                $row->parentNode->removeChild($row);
            }
        }

        // Now just extract the <div> containing our stuff...
        $nodelist = $xpath->query('//div');
        foreach ($nodelist as $node) {
            $markup = Utils::innerHTML($node);
        }

        return $markup;
    }
}
