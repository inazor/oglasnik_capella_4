<?php

namespace Baseapp\Library\Mapping;

use Baseapp\Console;
use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Models\Users as User;
use Baseapp\Models\Categories as Category;
use Baseapp\Models\CategoriesFieldsetsParameters as FieldsetParameter;
use Baseapp\Models\Ads as Ad;
use Baseapp\Models\Media;
use Baseapp\Models\Dictionaries as Dictionary;
use Baseapp\Library\Email;
use Phalcon\Mvc\User\Component;

class MigrationRawDb extends Component
{
    use MigrationTrait;

    private $di;
    private $import_first_started = 0;
    private $import_started = 0;
    private $import_ended = 0;
    private $source_db;
    private $target_db;
    private $current_timestamp;
    private $local_images_path = null;
    private $try_download_if_local_image_not_found = true;
    private $starting_category_id = null;
    private $sql_limit = 250;
    private $processed_ads = 0;
    private $remaining_ads = null;
    private $use_pcntl = 1;
    private $import_new_only = 1;
    private $last_import_id = 0;
    private $order = 'ASC';
    private $import_category_id = null;
    private $bezProductCategoryIds = array();
    private $counters_total = array(
        'imported'  => 0,
        'duplicate' => 0,
        'error'     => 0,
        'fixed'     => 0
    );
    private $counters = array(
        'imported'  => 0,
        'duplicate' => 0,
        'error'     => 0,
        'fixed'     => 0
    );
    private $countries = array();
    private $locations = array();
    private $actionName = 'ads';
    private $avus_user = null;
    private $log2email = null;
    private $log = array(
        'msgs'      => array(),
        'errors'    => array()
    );

    public function __construct($options = null)
    {
        $this->import_started = isset($_SERVER['REQUEST_TIME_FLOAT']) ? $_SERVER['REQUEST_TIME_FLOAT'] : microtime(true);
        $this->import_first_started = $this->import_started;
        $this->di = $this->getDI();
        $this->current_timestamp = Utils::getRequestTime();
        // configure and initialize source db
        $source_db_config = array(
            'host'     => 'sql.oglasnik.hr',
            'username' => 'npongrac',
            'password' => 'fief7eej',
            'dbname'   => 'oglasnik',
            'options'  => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES latin1',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->source_db = new RawDB($source_db_config);
        // configure and initialize target db
        $target_db_config = array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->target_db = new RawDB($target_db_config);

        $this->avus_user = $this->target_db->findFirst("SELECT * FROM users WHERE username = :username", array('username' => 'avus'));
        $countries = $this->target_db->find("SELECT id, iso_code FROM location WHERE level = 1");
        if ($countries) {
            foreach ($countries as $location) {
                $this->countries['id'][(string)$location['id']] = (string)$location['iso_code'];
                $this->countries['iso_code'][(string)$location['iso_code']] = (string)$location['id'];
            }
        }

        $locations = $this->target_db->find("SELECT id, name FROM location");
        if ($locations) {
            foreach ($locations as $location) {
                $this->locations[(string)$location['id']] = (string)$location['name'];
            }
        }
        $this->initBezProductCategoryIds();

        if (!empty($options)) {
            if (!empty($options['import_first_started'])) {
                $this->import_first_started = trim($options['import_first_started']);
            }
            $first_time = empty($options['remaining_ads']);

            if ($first_time) {
                $this->add2Log('-- Setting up options..');
                $this->add2Log('--');
            }
            $this->options = $options;
            if (!empty($options['log2email'])) {
                $this->log2email = trim($options['log2email']);
                if ($first_time) {
                    $this->add2Log('-- log2email => ' . $this->log2email);
                }
            }
            if (!empty($options['actionName'])) {
                $this->actionName = trim($options['actionName']);
                if ($first_time) {
                    $this->add2Log('-- actionName => ' . $this->actionName);
                }
            }
            if (!empty($options['images_path'])) {
                $this->local_images_path = trim($options['images_path']);
                if ($first_time) {
                    $this->add2Log('-- local_images_path => ' . $this->local_images_path);
                }
            }
            if (!empty($options['start_category_id'])) {
                $this->starting_category_id = trim($options['start_category_id']);
                if ($first_time) {
                    $this->add2Log('-- starting_category_id => ' . $this->starting_category_id);
                }
            }
            if (!empty($options['sql_limit'])) {
                $this->sql_limit = intval($options['sql_limit']);
                if ($first_time) {
                    $this->add2Log('-- sql_limit => ' . $this->sql_limit);
                }
            }
            if (!empty($options['import_new_only'])) {
                $this->import_new_only = intval($options['import_new_only']);
                if ($first_time) {
                    $this->add2Log('-- import_new_only => ' . $this->import_new_only);
                }
            }
            if (!empty($options['processed_ads'])) {
                $this->processed_ads = intval($options['processed_ads']);
            }
            if (!empty($options['remaining_ads'])) {
                $this->remaining_ads = intval($options['remaining_ads']);
            }
            if (!empty($options['pcntl'])) {
                $this->use_pcntl = intval($options['pcntl']);
            }
            if (!empty($options['last_import_id'])) {
                $this->last_import_id = intval($options['last_import_id']);
            }
            if (!empty($options['order'])) {
                $this->order = trim($options['order']);
            }
            if (!empty($options['counters'])) {
                $counters_arr = explode('_', trim($options['counters']));
                $this->counters_total = array(
                    'imported'  => !empty($counters_arr[0]) ? (int)$counters_arr[0] : 0,
                    'duplicate' => !empty($counters_arr[1]) ? (int)$counters_arr[1] : 0,
                    'error'     => !empty($counters_arr[2]) ? (int)$counters_arr[2] : 0,
                    'fixed'     => !empty($counters_arr[3]) ? (int)$counters_arr[3] : 0,
                );
            }
            if (!empty($options['import_category_id'])) {
                $this->import_category_id = trim($options['import_category_id']);
            }
            if ($first_time) {
                $this->add2Log('--------------------------------------------------------');
            }
        }
    }

    private function initBezProductCategoryIds()
    {
        $this->bezProductCategoryIds = array();

        $rows = $this->target_db->find("SELECT category_id, products_cfg_json FROM categories_settings WHERE products_cfg_json LIKE '%IstaknutiNoExtras%' OR products_cfg_json LIKE '%PremiumNoExtras%'");
        if ($rows && count($rows)) {
            foreach ($rows as $row) {
                if (trim($row['products_cfg_json'])) {
                    $products_json = json_decode($row['products_cfg_json']);

                    if ($products_json) {
                        foreach ($products_json as $group => $products) {
                            if ('online' === $group) {
                                foreach ($products as $product) {
                                    if (stripos($product->fqcn, 'NoExtras') !== false && $product->disabled == false) {
                                        $this->bezProductCategoryIds[] = (int) $row['category_id'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->bezProductCategoryIds = array_unique($this->bezProductCategoryIds);
    }

    private function isCategoryWithBezProducts($category = null)
    {
        // if we don't have any bezProductCategoryIds set, then we don't have bezProducts
        if (empty($this->bezProductCategoryIds)) {
            return false;
        }

        if ($category && isset($category->id)) {
            return in_array($category->id, $this->bezProductCategoryIds);
        }

        return false;
    }

    private function get_location_name_by_id($location_id)
    {
        $name = null;

        if (!empty($this->locations)) {
            $name = !empty($this->locations[(string)$location_id]) ?
                    $this->locations[(string)$location_id] :
                    null;
        }

        if (!$name) {
            $name = (string)$this->target_db->findFirst(
                "SELECT name FROM location WHERE id = :location_id",
                array(
                    'location_id' => $location_id
                ),
                true
            );
            if (!$name) {
                $name = null;
            } elseif (!empty($this->locations)) {
                $this->locations[(string)$location_id] = $name;
            }
        }

        return $name;
    }

    private function get_country_id_by_iso_code($iso_code)
    {
        $country_id = null;

        if (!empty($this->countries['iso_code'])) {
            $country_id = !empty($this->countries['iso_code'][(string)$iso_code]) ?
                           $this->countries['iso_code'][(string)$iso_code] :
                           null;
        }

        if (!$country_id) {
            $country_id = (int)$this->target_db->findFirst(
                "SELECT id FROM location WHERE level = 1 AND iso_code = :iso_code",
                array(
                    'iso_code' => trim($iso_code)
                ),
                true
            );
            if (!$country_id) {
                $country_id = null;
            } elseif (!empty($this->countries['iso_code'])) {
                $this->countries['iso_code'][(string)$iso_code] = (string)$country_id;
            }
        }

        return $country_id;
    }

    private function get_iso_code_by_country_id($country_id)
    {
        $iso_code = null;

        if (!empty($this->countries['id'])) {
            $iso_code = !empty($this->countries['id'][(string)$country_id]) ?
                        $this->countries['id'][(string)$country_id] :
                        null;
        }

        if (!$iso_code) {
            $iso_code = (int)$this->target_db->findFirst(
                "SELECT iso_code FROM location WHERE id = :id",
                array(
                    'id' => trim($country_id)
                ),
                true
            );
            if (!$iso_code) {
                $iso_code = null;
            } elseif (!empty($this->countries['id'])) {
                $this->countries['id'][(string)$country_id] = (string)$iso_code;
            }
        }

        return $iso_code;
    }

    private function get_run_argv()
    {
        $run_argv = array(
            ROOT_PATH . '/private/index.php',
            'import',
            $this->actionName
        );

        if ($this->log2email) {
            $run_argv[] = 'log2email=' . $this->log2email;
        }
        if ($this->import_first_started) {
            $run_argv[] = 'import_first_started=' . $this->import_first_started;
        }
        if ($this->local_images_path) {
            $run_argv[] = 'images_path=' . $this->local_images_path;
        }
        if ($this->starting_category_id) {
            $run_argv[] = 'start_category_id=' . $this->starting_category_id;
        }
        if ($this->sql_limit) {
            $run_argv[] = 'sql_limit=' . $this->sql_limit;
        }
        if ($this->processed_ads) {
            $run_argv[] = 'processed_ads=' . $this->processed_ads;
        }
        if ($this->remaining_ads) {
            $run_argv[] = 'remaining_ads=' . $this->remaining_ads;
        }
        if ($this->use_pcntl == 0) {
            $run_argv[] = 'pcntl=false';
        }
        if ($this->import_new_only == 0) {
            $run_argv[] = 'import_new_only=false';
        }
        if ($this->last_import_id > 0) {
            $run_argv[] = 'last_import_id=' . $this->last_import_id;
        }
        if ($this->order == 'DESC') {
            $run_argv[] = 'order=DESC';
        }
        if ($this->counters_total['imported'] || $this->counters_total['duplicate'] || $this->counters_total['error'] || $this->counters_total['fixed']) {
            $run_argv[] = 'counters=' . $this->counters_total['imported'] . '_' . $this->counters_total['duplicate'] . '_' . $this->counters_total['error'] . '_' . $this->counters_total['fixed'];
        }
        if ($this->import_category_id) {
            $run_argv[] = 'import_category_id=' . $this->import_category_id;
        }

        return $run_argv;
    }

    private function get_run_command()
    {
        $run_command = $_SERVER['_'] . ' ' . ROOT_PATH . '/private/index.php import ' . $this->actionName;

        if ($this->log2email) {
            $run_command .= ' log2email=' . $this->log2email;
        }
        if ($this->import_first_started) {
            $run_command .= ' import_first_started=' . $this->import_first_started;
        }
        if ($this->local_images_path) {
            $run_command .= ' images_path=' . $this->local_images_path;
        }
        if ($this->starting_category_id) {
            $run_command .= ' start_category_id=' . $this->starting_category_id;
        }
        if ($this->sql_limit) {
            $run_command .= ' sql_limit=' . $this->sql_limit;
        }
        if ($this->processed_ads) {
            $run_command .= ' processed_ads=' . $this->processed_ads;
        }
        if ($this->remaining_ads) {
            $run_command .= ' remaining_ads=' . $this->remaining_ads;
        }
        if ($this->use_pcntl == 0) {
            $run_command .= ' pcntl=0';
        }
        if ($this->import_new_only == 0) {
            $run_command .= ' import_new_only=0';
        }
        if ($this->last_import_id > 0) {
            $run_command .= ' last_import_id=' . $this->last_import_id;
        }
        if ($this->order == 'DESC') {
            $run_command .= ' order=DESC';
        }
        if ($this->counters_total['imported'] || $this->counters_total['duplicate'] || $this->counters_total['error'] || $this->counters_total['fixed']) {
            $run_command .= ' counters=' . $this->counters_total['imported'] . '_' . $this->counters_total['duplicate'] . '_' . $this->counters_total['error'] . '_' . $this->counters_total['fixed'];
        }
        if ($this->import_category_id) {
            $run_command .= ' import_category_id=' . $this->import_category_id;
        }

        return $run_command;
    }

    /**
     * Validate import category id
     *
     * Import category id (as far as we know) is a string with even number
     * of characters (digits), and every category/subcategory is
     * represented with a two-digit code..
     *
     * @param string $import_category_id
     * @return bool
     */
    protected function valid_import_category_id($import_category_id)
    {
        $import_category_id = trim($import_category_id);

        if (strlen($import_category_id) % 2 == 0) {
            return true;
        }
        $this->add2Log('Import category: ' . $import_category_id . ' is not valid?', 'error');

        return false;
    }

    protected function getAllCategoriesFrom($starting_category_id = null)
    {
        $curr_starting_index = $starting_category_id ? array_search($starting_category_id[0], array_keys($this->cat_ids)) : 0;

        $categories = array();

        $curr_index = 0;
        foreach ($this->cat_ids as $cat_id => $cat_data) {
            if ($curr_index >= $curr_starting_index) {
                $categories[$cat_id] = $cat_data;
            }
            $curr_index++;
        }

        return $categories;
    }

    public function get($import_category_id, $src_ad_content)
    {
        $import_category_id = trim($import_category_id);

        $matching_category_data = null;

        if ($this->valid_import_category_id($import_category_id)) {
            $matching_category_data = array(
                'name'     => array(),
                'settings' => array(
                    'online_category_id' => null,
                    'parameters'         => array(),
                    'extract_data'       => null
                )
            );
            $curr_category_id = '';
            $curr_category = null;

            // split the import category id into an array with two-digit keys
            $categories = str_split($import_category_id, 2);
            $last_category_parameter = null;
            foreach ($categories as $i => $category_chunk) {
                // construct current category's id
                $curr_category_id .= $category_chunk;
                // get the category array
                if ($curr_category == null) {
                    $curr_category = isset($this->import_categories[$curr_category_id]) ? $this->import_categories[$curr_category_id] : null;
                } else {
                    $curr_category = isset($curr_category['items'][$curr_category_id]) ? $curr_category['items'][$curr_category_id] : null;
                }

                if ($curr_category) {
                    // this is for breadcrumb construction so we can see which category we're working on
                    if (isset($curr_category['name'])) {
                        $matching_category_data['name'][] = trim($curr_category['name']);
                    } elseif (is_string($curr_category)) {
                        $matching_category_data['name'][] = trim($curr_category);
                    }

                    // in every category we can have multiple elements with mapping data:
                    // online_id, parameter(s), items, ...


                    // online_id is a specific field. We can only have one online_id at the end but, in the nested array
                    // we can have an online_id field on every level. It is like that so we can override (or at least have
                    // a fallback id in case we don't find a match). So, this field (if found) is simply overwritten!
                    if (isset($curr_category['online_id'])) {
                        // this field can have multiple different types.. it can be a simple integer value,
                        // but it can have an array (array is used when we don't know for sure where to map current category
                        // so we have to run a custom method in order to determinate the right category_id)
                        if (is_array($curr_category['online_id'])) {
                            $custom_method = $curr_category['online_id']['method'];
                            $fallback_id = isset($curr_category['online_id']['fallback_id']) ? $curr_category['online_id']['fallback_id'] : null;
                            if (null === $fallback_id && intval($matching_category_data['settings']['online_category_id'])) {
                                // see if we have online_id from most recent parent? if so, use it as a fallback_id
                                $fallback_id = intval($matching_category_data['settings']['online_category_id']);
                            }
                            $matching_category_data['settings']['online_category_id'] = $this->$custom_method(
                                $src_ad_content,
                                $curr_category['online_id']['keyword'],
                                $fallback_id
                            );
                        } elseif (is_numeric($curr_category['online_id']) && intval($curr_category['online_id'])) {
                            $matching_category_data['settings']['online_category_id'] = intval($curr_category['online_id']);
                        }
                    }

                    // we have two different parameter settings... one is an array of multiple parameters that could be
                    // applied to the ad, the other one is one specific parameter that should be applied to the ad.
                    //
                    // it is important to note that they wont come both at the same time (as it makes no sense!) - it
                    // will be one or another! also, it's important to know that 'parameter' (single one) could be
                    // transferred in the next iteration of the loop (so we can filter subitems, if we want so!)
                    if (isset($curr_category['parameters']) && count($curr_category['parameters'])) {
                        $matching_category_data['settings']['parameters'] = array_merge(
                            $matching_category_data['settings']['parameters'],
                            $curr_category['parameters']
                        );
                    } elseif (isset($curr_category['parameter']) && is_array($curr_category['parameter'])) {
                        if (isset($curr_category['parameter']['slug']) && isset($curr_category['parameter']['type'])) {
                            $latest_parameter = $curr_category['parameter'];

                            // we have multiple type of parameter settings
                            //  - match_str -> we'll be looking for the value in next loop's iteration, so we'll be
                            //    transferring the parameter to next loop
                            if ($curr_category['parameter']['type'] == 'match_str') {
                                $latest_parameter = $curr_category['parameter'];
                            } else {
                                // for now we have only one more parameter type, but we'll clear the $latest_parameter
                                // as we don't need it anymore (as it's valid only for next levels items)..
                                $latest_parameter = null;

                                // check if we have any value set? if yes, add the parameter to stack!
                                if (isset($curr_category['parameter']['value'])) {
                                    $matching_category_data['settings']['parameters'][$curr_category['parameter']['slug']] = array(
                                        'type' => $curr_category['parameter']['type'],
                                        'value' => $curr_category['parameter']['value']
                                    );
                                }
                            }
                        } elseif ($latest_parameter) {
                            // it could happen that we have a parameter from earlier level, so we need to reset it!
                            $latest_parameter = null;
                        }
                    } elseif (isset($latest_parameter)) {
                        if ($latest_parameter['type'] == 'match_str' && trim($curr_category['name'])) {
                            $matching_category_data['settings']['parameters'][$latest_parameter['slug']] = array(
                                'type' => 'match_str',
                                'name' => trim($curr_category['name'])
                            );
                        }

                        // we're done with this level, so clear the $latest_parameter variable...
                        $latest_parameter = null;
                    }

                    // we can have some custom method we'd like to run in order to try to extract some specific data
                    // from the ad located in current category... if 'extract_data' property is set, that's the name of
                    // the custom method we need to call later in the process...
                    if (isset($curr_category['extract_data']) && trim($curr_category['extract_data'])) {
                        $matching_category_data['settings']['extract_data'] = trim($curr_category['extract_data']);
                    }
                } else {
                    $this->add2Log('[' . $import_category_id . '][' . $src_ad_content . '] for some reason we couldn\'t find the category ... break the loop?', 'error');
                    // for some reason we couldn't find the category ... break the loop?
                    //break;
                }
            }

            // handle the breadcrump part we talked about at the beginning of this method :)
            $matching_category_data['name'] = implode(' › ', $matching_category_data['name']);
        }

        if ($matching_category_data['settings']['online_category_id']) {
            $db_cat = $this->target_db->findFirst(
                "SELECT * FROM categories WHERE id = :id",
                array(
                    'id' => $matching_category_data['settings']['online_category_id']
                )
            );
            if ($db_cat) {
                return $matching_category_data;
            }
        }

        return null;
    }


    /**
     * Helper method for matching a string in first level of a specific parameters dictionary (if dict exists!)
     *
     * @param int $category_id Category where to look for parameter_slug
     * @param string $parameter_slug Parameter slug where to look for dictionary match
     * @param string $match_str String to match
     * @param null|string $level2_possible_match_str String to match in second level
     * @return null|int|array
     */
    protected function match_string_in_parameters_dictionary($category_id, $parameter_slug, $match_str, $level2_possible_match_str = null)
    {
        $matching_id = null;

        $working_dictionary_id = $this->target_db->findFirst(
            "SELECT dictionary_id FROM parameters p INNER JOIN categories_fieldsets_parameters cfp ON p.id = cfp.parameter_id AND cfp.category_id = :category_id WHERE cfp.parameter_slug = :parameter_slug",
            array(
                'category_id' => (int)$category_id,
                'parameter_slug' => trim($parameter_slug)
            ),
            true
        );

        if ($working_dictionary_id) {
            $match_id = $this->target_db->findFirst(
                "SELECT id FROM dictionaries WHERE root_id = :root_id AND level = 2 AND name LIKE CONCAT(:name, '%')",
                array(
                    'root_id' => $working_dictionary_id,
                    'name'    => trim($match_str)
                ),
                true
            );

            if ($match_id) {
                $matching_id = $match_id;

                // try findind id for second level?
                if ($level2_possible_match_str) {
                    $exploded_string = explode(',', $level2_possible_match_str);
                    if (count($exploded_string)) {
                        $first_word = trim($exploded_string[0]);

                        $level2_match_id = $this->target_db->findFirst(
                            "SELECT id FROM dictionaries WHERE root_id = :root_id AND parent_id = :parent_id AND name LIKE CONCAT(:name, '%')",
                            array(
                                'root_id'   => $working_dictionary_id,
                                'parent_id' => $matching_id,
                                'name'      => trim($match_str)
                            ),
                            true
                        );
                        if ($level2_match_id) {
                            $matching_id = array(
                                $matching_id,
                                $level2_match_id
                            );
                        } else {
                            $exploded_first_word = explode(' ', $first_word);
                            $first_word = trim($exploded_first_word[0]);

                            $level2_match_id = $this->target_db->findFirst(
                                "SELECT id FROM dictionaries WHERE root_id = :root_id AND level = 3 AND parent_id = :parent_id AND name LIKE CONCAT(:name, '%')",
                                array(
                                    'root_id'   => $working_dictionary_id,
                                    'parent_id' => $matching_id,
                                    'name'      => trim($first_word)
                                ),
                                true
                            );
                            if ($level2_match_id) {
                                $matching_id = array(
                                    $matching_id,
                                    $level2_match_id
                                );
                            }
                        }
                    }
                }
            }
        } else {
            $this->add2Log("match_string_in_parameters_dictionary($category_id, $parameter_slug, $match_str, $level2_possible_match_str) -> NO working_dictionary_id", 'error');
        }

        if (!$matching_id) {
            $this->add2Log("match_string_in_parameters_dictionary($category_id, $parameter_slug, $match_str, $level2_possible_match_str) -> NO match for dictionary id", 'error');
        }

        return $matching_id;
    }

    protected function getLocationByRegion($region_id)
    {
        $data = array(
            '0'  => array('country_id' => 1),                      // Hrvatska
            '1'  => array('country_id' => 1),                      // Hrvatska
            '2'  => array('country_id' => 1, 'county_id' => 232),  // Bjelovarsko-bilogorska županija
            '3'  => array('country_id' => 1, 'county_id' => 588),  // Brodsko-posavska županija
            '4'  => array('country_id' => 1, 'county_id' => 842),  // Dubrovačko-neretvanska županija
            '5'  => array('country_id' => 1, 'county_id' => 7442), // Grad Zagreb
            '6'  => array('country_id' => 1, 'county_id' => 1136), // Istarska županija
            '7'  => array('country_id' => 1, 'county_id' => 1867), // Karlovačka županija
            '8'  => array('country_id' => 1, 'county_id' => 2682), // Koprivničko-križevačka županija
            '9'  => array('country_id' => 1, 'county_id' => 3002), // Krapinsko-zagorska županija
            '10' => array('country_id' => 1, 'county_id' => 3460), // Ličko-senjska županija
            '11' => array('country_id' => 1, 'county_id' => 3755), // Međimurska županija
            '12' => array('country_id' => 1, 'county_id' => 3920), // Osječko-baranjska županija
            '13' => array('country_id' => 1, 'county_id' => 4242), // Požeško-slavonska županija
            '14' => array('country_id' => 1, 'county_id' => 4559), // Primorsko-goranska županija
            '15' => array('country_id' => 1, 'county_id' => 6174), // Šibensko-kninska županija
            '16' => array('country_id' => 1, 'county_id' => 5188), // Sisačko-moslavačka županija
            '17' => array('country_id' => 1, 'county_id' => 5677), // Splitsko-dalmatinska županija
            '18' => array('country_id' => 1, 'county_id' => 6416), // Varaždinska županija
            '19' => array('country_id' => 1, 'county_id' => 6766), // Virovitičko-podravska županija
            '20' => array('country_id' => 1, 'county_id' => 6985), // Vukovarsko-srijemska županija
            '21' => array('country_id' => 1, 'county_id' => 7149), // Zadarska županija
            '22' => array('country_id' => 1, 'county_id' => 7760), // Zagrebačka županija
            '23' => array('country_id' => 1, 'county_id' => 8571)  // Hrvatska -> Inozemstvo
        );

        return (isset($data[$region_id]) ? $data[$region_id] : null);
    }

    protected function downloadUrlToFile($url)
    {
        $outFileName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'tmp-download-'.md5($url);
        $httpcode = null;
        $result = false;

        if (is_file($url)) {
            $result = copy($url, $outFileName);
        } else {
            $options = array(
              CURLOPT_FILE    => fopen($outFileName, 'w'),
              CURLOPT_TIMEOUT => 28800, // set this to 8 hours so we dont timeout on big files
              CURLOPT_URL     => $url
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $result = ($httpcode>=200 && $httpcode<300) ? true : false;
        }

        if ($result) {
            return $outFileName;
        } else {
            $this->add2Log("downloadUrlToFile($url) -> HTTP CODE $httpcode", 'error');
        }

        return null;
    }

    protected function get_media_files($media_files)
    {
        $media = array();
        foreach ($media_files as $file) {
            if (isset($file['photo']) && trim($file['photo'])) {

                $tmp_file = null;

                // first we'll try to find the photo locally on the server
                if ($this->local_images_path) {
                    $tmp_file = $this->local_images_path . trim($file['photo']);
                    if (!file_exists($tmp_file) && $this->try_download_if_local_image_not_found) {
                        $tmp_file = $this->downloadUrlToFile('http://slike.oglasnik.hr' . trim($file['photo']));
                    }
                } else {
                    $tmp_file = $this->downloadUrlToFile('http://slike.oglasnik.hr' . trim($file['photo']));
                }

                if ($tmp_file && file_exists($tmp_file)) {
                    $finfo = @finfo_open(FILEINFO_MIME_TYPE);
                    if ($finfo) {
                        $mime = @finfo_file($finfo, $tmp_file);
                        if ($mime) {
                            $type = (explode('/', $mime)[1]);
                            $filesize = filesize($tmp_file);

                            $media[] = array(
                                'name'     => 'ad-picture-' . $file['id'] . '.' . $type,
                                'type'     => $mime,
                                'tmp_name' => $tmp_file,
                                'error'    => UPLOAD_ERR_OK,
                                'size'     => $filesize
                            );
                        }
                    }
                }
            }
        }

        return $media;
    }

    protected function addMediaFiles($media_files, $user = null)
    {
        $pictures = null;

        $media = new Media();
        if ($user && isset($user['id'])) {
            $media->created_by_user_id  = $user['id'];
            $media->modified_by_user_id = $user['id'];
        }
        $media->setCustomFiles($media_files);
        $results = $media->handle_upload(null, false);
        if (isset($results['media'])) {
            $pictures = array();
            foreach ($results['media'] as $media_row) {
                if (isset($media_row['id'])) {
                    $pictures[] = intval($media_row['id']);
                }
            }
        }

        return $pictures;
    }

    protected function fixPhoneNumber($str, $country_code = null)
    {
        $phone_number = trim($str);
        $country_code = trim($country_code) ? trim($country_code) : 'HR';

        if (trim($str)) {
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            try {
                $phoneData = $phoneUtil->parse($str, $country_code);
                if ($phone_number = $phoneUtil->isValidNumber($phoneData)) {
                    $phone_number = $phoneUtil->format($phoneData, \libphonenumber\PhoneNumberFormat::E164);
                }
            } catch (\libphonenumber\NumberParseException $e) {
                // handle this?
            }
        }

        return $phone_number;
    }

    protected function toUTF8($str)
    {
        $str = iconv('ISO-8859-2', 'UTF-8', $str);
        $str = str_replace(array('&#8364;', ''), '€', $str);
        return $str;
    }

    protected function getUsersPhoneNumbers($user)
    {
        $users_phone_numbers = array();

        if (isset($user['phone1']) && trim($user['phone1'])) {
            $users_phone_numbers[] = trim($user['phone1']);
        }
        if (isset($user['phone2']) && trim($user['phone2'])) {
            $users_phone_numbers[] = trim($user['phone2']);
        }

        return $users_phone_numbers;
    }

    protected function getUsersUniquePhoneNumber($users_phone_numbers, $phone_number)
    {
        $resulting_phone_number = null;

        if ($phone_number && trim($phone_number) && !in_array(trim($phone_number), $users_phone_numbers)) {
            $resulting_phone_number = trim($phone_number);
        }

        return $resulting_phone_number;
    }

    protected function findTargetUser(array $src_user, $return_only_newly_created = false)
    {
        $db_user = null;
        $newly_created = false;

        if (isset($src_user['id']) && intval($src_user['id'])) {
            if ($return_only_newly_created) {
                $db_user = $this->target_db->findFirst(
                    "SELECT * FROM users WHERE import_id = :import_id OR email = :email",
                    array(
                        'import_id' => $src_user['id'],
                        'email'     => $src_user['email']
                    )
                );
            } else {
                // find the user by import_id - if it doesn't exist, create a new one
                $db_user = $this->target_db->findFirst(
                    "SELECT * FROM users WHERE import_id = :import_id",
                    array(
                        'import_id' => $src_user['id']
                    )
                );

                if (!$db_user && isset($src_user['email']) && !empty($src_user['email'])) {
                    $db_user = $this->target_db->findFirst(
                        "SELECT * FROM users WHERE email = :email",
                        array(
                            'email' => trim($src_user['email'])
                        )
                    );
                }
            }

            if (!$db_user) {
                // we didn't find the user, so we'll create a new one with data we have

                $country_id = null;
                if (!empty($src_user['country'])) {
                    if (trim($src_user['country']) == 'HR') {
                        $country_id = 1;
                    } else {
                        $country_id = $this->get_country_id_by_iso_code(trim($src_user['country']));
                    }
                }

                if (trim($src_user['user_type']) != 'fizicka' && trim($src_user['tvrtka'])) {
                    $users_name = $this->toUTF8(trim($src_user['tvrtka']));
                } else {
                    $users_name = $this->toUTF8(trim(trim($src_user['name']) . ' ' . trim($src_user['surname'])));
                }
                $username = str_replace('-', '', Utils::slugify($users_name ? $users_name : 'user_' . $src_user['id']));
                // check if we have a user with this username
                $username_user = $this->target_db->findFirst("SELECT id FROM users WHERE username = :username", array('username' => $username));
                if ($username_user && isset($username_user['id']) && intval($username_user['id'])) {
                    // if we do, add import_id to the end of the username...
                    $username = $username . $src_user['id'];
                }

                $password = \Phalcon\Text::random(\Phalcon\Text::RANDOM_ALNUM, 22);

                $phone1 = null;
                if (trim($src_user['phone'])) {
                    $phone1 = $this->fixPhoneNumber(trim($src_user['phone']), (!empty($src_user['country']) ? trim($src_user['country']) : 'HR'));

                    if (!$phone1) {
                        $phone1 = trim($src_user['phone']);
                    }
                }
                $phone2 = null;
                if (trim($src_user['phone2'])) {
                    $phone2 = $this->fixPhoneNumber(trim($src_user['phone2']), (!empty($src_user['country']) ? trim($src_user['country']) : 'HR'));

                    if (!$phone2) {
                        $phone2 = trim($src_user['phone2']);
                    }
                }

                $oib        = null;
                $import_mbr = $src_user['mbr']; // keeping original as it was just in case
                $mbr        = trim($import_mbr);
                if (empty($mbr)) {
                    $mbr = null;
                }
                // Decide if it's oib or something else (oib should be 11 digits exactly)
                if (!empty($mbr) && is_numeric($mbr) && mb_strlen($mbr, 'UTF-8') == 11) {
                    $oib = $mbr;
                }

                $db_user = array(
                    'email'        => $src_user['email'],
                    'username'     => $username,
                    'password'     => $this->di->get('auth')->hash_password($password),
                    'old_password' => trim($src_user['password']) ? trim($src_user['password']) : null,
                    'phone1'       => $phone1,
                    'phone2'       => $phone2,
                    'company_name' => trim($src_user['tvrtka']) ? mb_convert_case($this->toUTF8(trim($src_user['tvrtka'])), MB_CASE_TITLE, 'UTF-8') : null,
                    'first_name'   => trim($src_user['name']) ? mb_convert_case($this->toUTF8(trim($src_user['name'])), MB_CASE_TITLE, 'UTF-8') : null,
                    'last_name'    => trim($src_user['surname']) ? mb_convert_case($this->toUTF8(trim($src_user['surname'])), MB_CASE_TITLE, 'UTF-8') : null,
                    'oib'          => $oib,
                    'address'      => trim($src_user['address']) ? mb_convert_case($this->toUTF8(trim($src_user['address'])), MB_CASE_TITLE, 'UTF-8') : null,
                    'city'         => trim($src_user['city']) ? mb_convert_case($this->toUTF8(trim($src_user['city'])), MB_CASE_TITLE, 'UTF-8') : null,
                    'zip_code'     => trim($src_user['zip']) ? trim($src_user['zip']) : null,
                    'country_id'   => $country_id,
                    'active'       => trim($src_user['active']) == 'Y' ? 1 : 0,
                    'created_at'   => trim($src_user['created']) ? trim($src_user['created']) : null,
                    'modified_at'  => trim($src_user['last_changed']) ? trim($src_user['last_changed']) : null,
                    'type'         => trim($src_user['user_type']) == 'fizicka' ? 1 : 2,
                    'newsletter'   => 1,
                    'comment'      => trim($src_user['komentari']) ? trim($src_user['komentari']) : null,
                    //'notes'        => trim($src_user['biljeske']) ? trim($src_user['biljeske']) : null,
                    'import_id'    => $src_user['id'],
                    'import_mbr'   => $import_mbr
                );

                $db_user_id = $this->target_db->insert(
                    "INSERT INTO users (email, username, password, old_password, phone1, phone2, company_name, first_name, last_name, oib, address, city, zip_code, country_id, active, created_at, modified_at, type, newsletter, comment, import_id, import_mbr) VALUES (:email, :username, :password, :old_password, :phone1, :phone2, :company_name, :first_name, :last_name, :oib, :address, :city, :zip_code, :country_id, :active, :created_at, :modified_at, :type, :newsletter, :comment, :import_id, :import_mbr)",
                    $db_user
                );
                if ($db_user_id) {
                    $newly_created = true;
                    $db_user['id'] = $db_user_id;

                    // add login role for this user
                    $db_user_id = $this->target_db->insert(
                        "INSERT INTO roles_users (user_id, role_id) VALUES (:user_id, 1)",
                        array(
                            'user_id' => $db_user_id
                        )
                    );
                }
            }
        }

        if (!$db_user) {
            // get 'avus' user
            $db_user = $this->avus_user;
        }

        if ($return_only_newly_created && !$newly_created) {
            $db_user = null;
        }

        return $db_user;
    }

    protected function getAdProducts($src_ad, $user=null, $category=null)
    {
        $ad_products = null;

        if (isset($src_ad['inet_type_id'])) {
            // time in days
            $ad_timeperiod = (strtotime($src_ad['expires']) - strtotime($src_ad['pbl_date'])) / (3600 * 24);

            if ($ad_timeperiod >= 0 && $ad_timeperiod < 8) {
                $ad_selected_duration = '5 dana';
            } elseif ($ad_timeperiod >= 8 && $ad_timeperiod < 17) {
                $ad_selected_duration = '15 dana';
            } else {
                $ad_selected_duration = '30 dana';
            }

            $bezProduct = $this->isCategoryWithBezProducts($category);

            switch (intval($src_ad['inet_type_id'])) {
                case  6:
                case  7:
                    if ($bezProduct) {
                        $POST = array(
                            'products-online' => 'istaknuti-online-bez',
                            'options' => array(
                                'istaknuti-online-bez' => $ad_selected_duration
                            )
                        );
                        $online_product = new \Baseapp\Library\Products\Online\IstaknutiNoExtras($category, $user, $POST);
                    } else {
                        $POST = array(
                            'products-online' => 'istaknuti-online',
                            'options' => array(
                                'istaknuti-online' => $ad_selected_duration
                            )
                        );
                        $online_product = new \Baseapp\Library\Products\Online\IstaknutiOnline($category, $user, $POST);
                    }
                    break;
                case  8:
                case  9:
                case 10:
                case 11:
                    if ($bezProduct) {
                        $POST = array(
                            'products-online' => 'premium-bez',
                            'options' => array(
                                'premium-bez' => $ad_selected_duration
                            )
                        );
                        $online_product = new \Baseapp\Library\Products\Online\PremiumNoExtras($category, $user, $POST);
                    } else {
                        $POST = array(
                            'products-online' => 'premium',
                            'options' => array(
                                'premium' => $ad_selected_duration
                            )
                        );
                        $online_product = new \Baseapp\Library\Products\Online\Premium($category, $user, $POST);
                    }
                    break;
                case 117:
                case 119:
                    $POST = array(
                        'products-online' => 'pinky',
                        'options' => array(
                            'pinky' => '7 dana'
                        )
                    );
                    $online_product = new \Baseapp\Library\Products\Online\Pinky($category, $user, $POST);
                    break;
                case 118:
                    $POST = array(
                        'products-online' => 'premium',
                        'options' => array(
                            'premium' => $ad_selected_duration
                        )
                    );
                    $online_product = new \Baseapp\Library\Products\Online\Premium($category, $user, $POST);
                    break;
                default:
                    $POST = array(
                        'products-online' => 'osnovni',
                        'options' => array(
                            'osnovni' => '90 dana'
                        )
                    );
                    $online_product = new \Baseapp\Library\Products\Online\Osnovni($category, $user, $POST);
            }

            $ad_products = array();
            $ad_products['online'] = array(
                'id'           => $online_product->getId(),
                'product_sort' => $online_product->getSortIdx(),
                'product'      => serialize($online_product)
            );
        }

        return $ad_products;
    }

    protected function generateTitle($original_title = '', $original_ad_content = '')
    {
        $original_title = trim($original_title);
        $original_body  = trim($this->fixTextFormatting(strip_tags($original_ad_content, '<br><br/>')));

        if (!$original_title || mb_substr($original_body, 0, mb_strlen($original_title, 'UTF-8'), 'UTF-8') == $original_title) {
            $ad_content = str_ireplace(array("<br>", "<br/>", "<br />"), "\n", $original_ad_content);
            if (strpos($ad_content, "\n") !== false) {
                $ad_content = strtok($ad_content, "\n");
            }

            $original_title = $this->fixTitleFormatting(Utils::str_truncate(strip_tags($ad_content), 120));
        }

        return $original_title;
    }

    protected function fixTitleFormatting($title)
    {
        $normalized_title = trim($title);

        if ($normalized_title) {
            $normalized_title = str_replace(array("\r\n", "\r"), " ", $normalized_title);
            $normalized_title = preg_replace("/ {2}/", " ", $normalized_title);

            // fix wrong space near ',' or '.' chars..
            $normalized_title = str_replace("…", "...", $normalized_title);
            $normalized_title = str_replace(" ,", ", ", $normalized_title);
            $normalized_title = str_replace(" .", ". ", $normalized_title);
            $normalized_title = preg_replace("/ {2,}/", " ", $normalized_title);


            // remove multiple occurances of '.' at the end of the string
            $normalized_title = preg_replace("/\.{2,}$/", "", $normalized_title);
        }

        return trim(strip_tags($normalized_title));
    }

    protected function fixTextFormatting($content, $trim_new_lines = false)
    {
        $normalized_content = trim($content);

        if ($normalized_content) {
            $normalized_content = str_replace(array("\r\n", "\r"), "\n", $normalized_content);
            $normalized_content = preg_replace("/ {3,}/", "\n\n", $normalized_content);
            $normalized_content = preg_replace("/ {2}/", "\n", $normalized_content);
            if ($trim_new_lines) {
                $normalized_content = str_replace("\n", ' ', $normalized_content);
            }

            $normalized_content = str_replace("…", "...", $normalized_content);
            // fix wrong space near ',' or '.' chars..
            $normalized_content = preg_replace("/\s?:\s?/", ": ", $normalized_content);
            $normalized_content = str_replace(" ,", ", ", $normalized_content);
            $normalized_content = str_replace(" .", ". ", $normalized_content);

            $normalized_content = preg_replace("/ {2,}/", " ", $normalized_content);

            // remove multiple occurances of '.' at the end of the string
            $normalized_content = preg_replace("/\.{2,}$/", ".", $normalized_content);
        }

        return trim($normalized_content);
    }

    protected function importAd(array $src_user, array $src_ad)
    {
        $user = $this->findTargetUser($src_user);

        $ad_content = trim($this->toUTF8($src_ad['body']));
        if (isset($src_ad['print_body']) && trim($src_ad['print_body'])) {
            $ad_content = trim($this->toUTF8($src_ad['print_body']));
        }

        if ($user && $src_category = $this->get($src_ad['inet_kat'], $ad_content)) {
            $user_object = User::findFirst($user['id']);
            $ad_content = strip_tags($ad_content, '<br><br/><br />');

            if (isset($src_category['settings']['online_category_id']) && intval($src_category['settings']['online_category_id'])) {
                $ad_category = Category::findFirst($src_category['settings']['online_category_id']);
                if ($ad_category && $user_object) {
                    $ad_products = $this->getAdProducts($src_ad, $user_object, $ad_category);
                    $users_phone_numbers = $this->getUsersPhoneNumbers($user);

                    $curr_datetime = Utils::getRequestTime();
                    $location = $this->getLocationByRegion($src_ad['region']);

                    $ad_title = trim(strip_tags($src_ad['title'])) ? trim(strip_tags($this->toUTF8(trim($src_ad['title'])))) : null;
                    $ad_body = trim($this->fixTextFormatting(strip_tags($ad_content), '<br><br/>'));

                    // if ad's title is not present or it's present in the body
                    // also, we generate the new title (with more chars) than it
                    // has now, and is better trimmed...
                    if (!$ad_title || mb_substr($ad_body, 0, mb_strlen($ad_title, 'UTF-8'), 'UTF-8') == $ad_title) {
                        $ad_title = trim(strip_tags(Utils::str_truncate($this->toUTF8($src_ad['body']), 120)));
                    }

                    $ad_phone1 = null;
                    if (trim($src_ad['phone1'])) {
                        $ad_phone1 = $this->fixPhoneNumber(trim($src_ad['phone1']), (!empty($src_user['country']) ? trim($src_user['country']) : 'HR'));

                        if (!$ad_phone1) {
                            $ad_phone1 = trim($src_ad['phone1']);
                        }
                    }
                    $ad_phone2 = null;
                    if (trim($src_ad['phone2'])) {
                        $ad_phone2 = $this->fixPhoneNumber(trim($src_ad['phone2']), (!empty($src_user['country']) ? trim($src_user['country']) : 'HR'));

                        if (!$ad_phone2) {
                            $ad_phone2 = trim($src_ad['phone2']);
                        }
                    }

                    $sort_date          = trim($src_ad['inet_objavi']) ? strtotime($src_ad['inet_objavi']) : null;
                    $created_at         = trim($src_ad['submitted']) && trim($src_ad['submitted']) !== '0000-00-00 00:00:00' ? strtotime($src_ad['submitted']) : ($sort_date ? $sort_date : null);
                    $modified_at        = trim($src_ad['updated']) ? strtotime($src_ad['updated']) : null;
                    $published_at       = trim($src_ad['pbl_date']) ? strtotime($src_ad['pbl_date']) : null;
                    $expires_at         = isset($src_ad['adr_expires']) && !empty($src_ad['adr_expires']) ? strtotime(trim($src_ad['adr_expires'])) : null;
                    $first_published_at = $published_at;

                    if (!$created_at) {
                        $created_at = $sort_date;
                        if (!$created_at) {
                            $created_at = $modified_at;
                            if (!$created_at) {
                                $created_at = $published_at;
                                if (!$sort_date) {
                                    $created_at = $this->current_timestamp;
                                }
                            }
                        }
                    }
                    if (!$first_published_at) {
                        $first_published_at = $created_at;
                    }
                    if (!$sort_date) {
                        $sort_date = $modified_at;
                        if (!$sort_date) {
                            $sort_date = $created_at;
                            if (!$sort_date) {
                                $sort_date = $this->current_timestamp;
                            }
                        }
                    }
                    if (!$expires_at) {
                        $expires_at = trim($src_ad['expires']) ? strtotime($src_ad['expires']) : $this->current_timestamp;
                    }

                    if (trim($ad_title)) {
                        $ad = new Ad();
                        $ad->category_id        = $ad_category->id;
                        $ad->user_id            = $user['id'];
                        $ad->created_by_user_id = $user['id'];
                        $ad->title              = $ad_title;
                        $ad->description        = $ad_body;
                        $ad->description_tpl    = $ad_body;
                        $ad->phone1             = $ad_phone1;
                        $ad->phone2             = $ad_phone2;
                        $ad->created_at         = $created_at;
                        $ad->first_published_at = $first_published_at;
                        $ad->published_at       = $first_published_at;
                        $ad->modified_at        = $modified_at;
                        $ad->expires_at         = $expires_at;
                        $ad->sort_date          = $sort_date;
                        $ad->sold               = 0;
                        $ad->active             = ($expires_at && $expires_at > $this->current_timestamp ? 1 : 0);
						
						//IN:
						$ad->n_is_display_ad = 0;
						$ad->n_source = 'avus_online';
						$ad->n_frontend_sync_status = 'unsynced';
						$ad->n_dont_sync_with_avus = 0;
						
						
						

                        if ($ad_products) {
                            if (isset($ad_products['online'])) {
                                $ad->online_product_id = $ad_products['online']['id'];
                                $ad->online_product    = $ad_products['online']['product'];
                                $ad->product_sort      = $ad_products['online']['product_sort'];
                            }
                        }

                        $ad->import_id = $src_ad['id'];

                        $parametrizator = new Parametrizator();
                        $parametrizator->setModule('import');

                        // turn off processing of description_tpl and description_offline for imported ads
                        $parametrizator->process_description_tpl_placeholders = false;
                        $parametrizator->process_description_offline_placeholders = false;

                        $parametrizator->setCategory($ad_category);
                        $parametrizator->setAd($ad);

                        // add description parameter
                        $parametrizator->addParameterBySlug('ad_description', $ad_body);

                        // add the title parameter only if it current ad's description doesn't contain its title!
                        //
                        // if this condition doesn't fulfill, the imported ad will still have its
                        // title set (we did this with $ad->title), but adding the parameter's value via addParameterBySlug
                        // method adds parameter's value to the json_data field also, and in later processing, we use
                        // json_data to prepare search_terms... so if we found the substring of title in
                        // description, then there's no need to double information in ads_search_terms table!
                        if (mb_stripos($ad_body, $ad_title, 0, 'UTF-8') === false) {
                            $parametrizator->addParameterBySlug('ad_title', $ad_title);
                        }

                        // add price parameter
                        if (isset($src_ad['cijena']) && intval($src_ad['cijena']) > 0) {
                            $ad->price       = (int)$src_ad['cijena'];
                            $ad->currency_id = intval($src_ad['valuta_id']) ? intval($src_ad['valuta_id']) : 1;
                        } else {
                            $ad->price       = 0;
                            $ad->currency_id = 1;
                        }
                        $parametrizator->addParameterBySlug(
                            'ad_price',
                            array(
                                'value'       => (int)$ad->price,
                                'currency_id' => $ad->currency_id
                            )
                        );

                        $location_set_via_parameters = false;
                        // set values for found parameters...
                        if (isset($src_category['settings']['parameters']) && count($src_category['settings']['parameters'])) {
                            foreach ($src_category['settings']['parameters'] as $parameter_slug => $parameter_data) {
                                if ((isset($parameter_data['name']) && trim($parameter_data['name'])) || isset($parameter_data['value'])) {
                                    $parameter_value = null;

                                    switch ($parameter_data['type']) {
                                        case 'field':
                                            if (isset($src_ad[trim($parameter_data['name'])]) && trim($src_ad[trim($parameter_data['name'])])) {
                                                $parameter_value = trim($src_ad[trim($parameter_data['name'])]);
                                            }
                                            break;
                                        case 'match_str':
                                            $parameter_value = $this->match_string_in_parameters_dictionary(
                                                $ad_category->id,
                                                $parameter_slug,
                                                trim($parameter_data['name']),
                                                trim($this->toUTF8($src_ad['body']))
                                            );
                                            break;
                                        case 'value':
                                            $parameter_value = isset($parameter_data['value']) ? $parameter_data['value'] : null;
                                            break;
                                    }

                                    if ($parameter_value) {
                                        if ($parameter_slug == 'ad_location') {
                                            if (count($parameter_value) < count($location)) {
                                                $parameter_value = $location;
                                            }
                                            $location_set_via_parameters = true;
                                        }
                                        $parametrizator->addParameterBySlug($parameter_slug, $parameter_value);
                                    }
                                }
                            }
                        }

                        if ($src_category['settings']['extract_data']) {
                            $method_name = $src_category['settings']['extract_data'];
                            $extracted_data = $this->$method_name($ad_content);

                            if (count($extracted_data)) {
                                foreach ($extracted_data as $parameter_slug => $parameter_value_raw) {
                                    $parameter_value = $parameter_value_raw;
                                    if ($parameter_slug == 'ad_location') {
                                        if (count($parameter_value) < count($location)) {
                                            $parameter_value = $location;
                                        }
                                        $location_set_via_parameters = true;
                                    }
                                    $parametrizator->addParameterBySlug($parameter_slug, $parameter_value);
                                }
                            }
                        }

                        $ads_pictures_count = 0;
                        if (isset($src_ad['medias'])) {
                            $ads_pictures_count = count($src_ad['medias']);
                            $parametrizator->addParameterBySlug('ad_media', $src_ad['medias']);
                        } elseif ($src_ad['pictures']) {
                            $media_files = $this->get_media_files($src_ad['pictures']);
                            if (count($media_files)) {
                                $pictures_array = $this->addMediaFiles($media_files, $user);

                                if (is_array($pictures_array) && count($pictures_array)) {
                                    $ads_pictures_count = count($pictures_array);
                                    $parametrizator->addParameterBySlug('ad_media', $pictures_array);
                                }
                            }
                        }

                        if (!$location_set_via_parameters && isset($location) && count($location)) {
                            $parametrizator->addParameterBySlug('ad_location', $location);
                        }

                        $parametrizator->parametrize_imported();

                        // generate AD insert SQL...
                        $ad_array = $ad->toArray();
                        $ad_array['price'] = $ad->price;
                        $fields   = array();
                        $vals     = array();
                        foreach ($ad_array as $field => $val) {
                            $fields[] = $field;
                            $vals[] = ':' . $field;
                        }
                        $ad_insert_sql = "INSERT INTO ads (" . implode(', ', $fields) . ") VALUES (" . implode(', ', $vals) . ")";
                        // insert the ad in DB
                        $inserted_ad_id = $this->target_db->insert($ad_insert_sql, $ad_array);
                        if ($inserted_ad_id) {
                            $ad_array['id'] = $inserted_ad_id;
                            // add assigned parameters if present
                            if (!empty($parametrizator->SQL_ad_parameters)) {
                                $ads_parameters_sql = str_ireplace('[AD_ID]', $inserted_ad_id, "INSERT INTO ads_parameters (ad_id, parameter_id, level, item_index, value) VALUES " . implode(', ', $parametrizator->SQL_ad_parameters));
                                $inserted_ad_params = $this->target_db->insert($ads_parameters_sql);
                            }

                            // add assigned medias if present
                            if (!empty($parametrizator->SQL_ad_media)) {
                                $ads_media_sql = str_ireplace('[AD_ID]', $inserted_ad_id, "INSERT INTO ads_media (ad_id, media_id, sort_idx) VALUES " . implode(', ', $parametrizator->SQL_ad_media));
                                $inserted_ad_media = $this->target_db->insert($ads_media_sql);
                            }

                            // add assigned search terms if present
                            $SQL_ad_search_terms = isset($src_ad['adr_search_text']) && !empty($src_ad['adr_search_text']) ? trim($this->toUTF8($src_ad['adr_search_text'])) : '';
                            if ($parametrizator->SQL_ad_search_terms) {
                                $SQL_ad_search_terms .= ' ' . $parametrizator->SQL_ad_search_terms;
                                $inserted_ad_search_terms = $this->target_db->insert(
                                    "INSERT INTO ads_search_terms (ad_id, search_data, search_data_unaccented) VALUES (:ad_id, :search_data, :search_data_unaccented)",
                                    array(
                                        'ad_id'                  => $inserted_ad_id,
                                        'search_data'            => trim($SQL_ad_search_terms),
                                        'search_data_unaccented' => trim(Utils::remove_accents($SQL_ad_search_terms))
                                    )
                                );
                            }

                            // Set total views count via viewCountsManager service
                            $vcm = $this->di->get('viewCountsManager');
                            $vcm->setTotalViewCount($inserted_ad_id, $src_ad['views']);

                            // write down avus_id and import_id in ads_additional_data table
                            $ads_additional_data = array(
                                'ad_id'     => $inserted_ad_id,
                                'import_id' => $src_ad['id']
                            );
                            if (isset($src_ad['avus_id']) && !empty($src_ad['avus_id']) && intval($src_ad['avus_id'])) {
                                $ads_additional_data['avus_id'] = intval($src_ad['avus_id']);
                            }
                            $additional_fields = array();
                            $additional_vals = array();
                            foreach ($ads_additional_data as $field => $val) {
                                $additional_fields[] = $field;
                                $additional_vals[] = ':' . $field;
                            }
                            $ads_additional_data_insert_sql = "INSERT INTO ads_additional_data (" . implode(', ', $additional_fields) . ") VALUES (" . implode(', ', $additional_vals) . ")";
                            $inserted_ads_additional_data_id = $this->target_db->insert($ads_additional_data_insert_sql, $ads_additional_data);

                            return array(
                                'ad' => $ad_array,
                                'pictures' => $ads_pictures_count
                            );
                        } else {
                            $this->add2Log($ad_insert_sql, 'error');
                        }
                    }
                } else {
                    $this->add2Log("importAd(".$src_ad['id'].") -> NO target category", 'error');
                }
            }
        } else {
            $this->add2Log("importAd(".$src_ad['id'].") -> NO src_category(".$src_ad['inet_kat'].")", 'error');
        }

        return null;
    }

    protected function fixAdsLocation(array $target_ad, array $src_ad)
    {
        $ad_content = $this->toUTF8($src_ad['body']);
        if (isset($src_ad['print_body']) && trim($src_ad['print_body'])) {
            $ad_content = $this->toUTF8($src_ad['print_body']);
        }

        if ($src_category = $this->get($src_ad['inet_kat'], $ad_content)) {
            if (isset($src_category['settings']['online_category_id']) && intval($src_category['settings']['online_category_id'])) {
                $location = $this->getLocationByRegion($src_ad['region']);
                $category_location = array();
                if (isset($src_category['settings']['parameters']['ad_location']['value']) && !empty($src_category['settings']['parameters']['ad_location']['value'])) {
                    $category_location = $src_category['settings']['parameters']['ad_location']['value'];
                }

                $final_location = $location;
                if (count($category_location) >= count($location)) {
                    $final_location = $category_location;
                }

                $ads_update_data         = array();
                $target_ad_location_json = array();
                $country_id              = 0;
                $county_id               = 0;
                $city_id                 = 0;
                $municipality_id         = 0;

                if (!empty($final_location['country_id'])) {
                    $country_id   = (int)$final_location['country_id'];
                    $country_name = $this->get_location_name_by_id($country_id);

                    $target_ad_location_json['country_id']['value'] = $country_id;
                    if ($country_name) {
                        $target_ad_location_json['country_id']['text_value'] = $country_name;
                    }
                    $ads_update_data['country_id'] = $country_id;
                }
                if (!empty($final_location['county_id'])) {
                    $county_id   = (int)$final_location['county_id'];
                    $county_name = $this->get_location_name_by_id($county_id);

                    $target_ad_location_json['county_id']['value'] = $county_id;
                    if ($county_name) {
                        $target_ad_location_json['county_id']['text_value'] = $county_name;
                    }
                    $ads_update_data['county_id'] = $county_id;
                }
                if (!empty($final_location['city_id'])) {
                    $city_id   = (int)$final_location['city_id'];
                    $city_name = $this->get_location_name_by_id($city_id);

                    $target_ad_location_json['city_id']['value'] = $city_id;
                    if ($city_name) {
                        $target_ad_location_json['city_id']['text_value'] = $city_name;
                    }
                    $ads_update_data['city_id'] = $city_id;
                }
                if (!empty($final_location['municipality_id'])) {
                    $municipality_id   = (int)$final_location['municipality_id'];
                    $municipality_name = $this->get_location_name_by_id($municipality_id);

                    $target_ad_location_json['municipality_id']['value'] = $municipality_id;
                    if ($municipality_name) {
                        $target_ad_location_json['municipality_id']['text_value'] = $municipality_name;
                    }
                    $ads_update_data['municipality_id'] = $municipality_id;
                }

                $target_ad_json = !empty($target_ad['json_data']) ? json_decode($target_ad['json_data'], true) : array();
                if (!empty($target_ad_location_json)) {
                    $target_ad_json['location'] = $target_ad_location_json;
                }

                if (!empty($target_ad_json)) {
                    $ads_update_data['json_data'] = json_encode($target_ad_json);
                }

                if (!empty($ads_update_data)) {
                    $fields   = array();
                    $vals     = array();
                    foreach ($ads_update_data as $field => $val) {
                        $fields[] = $field . ' = :' .$field;
                        $vals[$field] = $val;
                    }
                    $vals['ad_id'] = $target_ad['id'];

                    $update_query = "UPDATE ads SET " . implode(', ', $fields) . " WHERE id = :ad_id";

                    return $this->target_db->update($update_query, $vals);
                }
            }
        }

        return null;
    }

/******************************************************************************/
    public function sendMail()
    {
        if ($this->log2email && !is_bool($this->log2email)) {
            $this->import_ended = microtime(true);
            $time_spent = $this->import_ended - $this->import_started;
            $time_spent_min = (int)($time_spent/60);
            $time_spent_sec = $time_spent - ($time_spent_min*60);

            $time_spent_total = $this->import_ended - $this->import_first_started;
            $time_spent_total_arr = explode('.', $time_spent_total);
            $time_spent_total = date('H:i:s', $time_spent_total_arr[0]) . '.' . $time_spent_total_arr[1];

            $esitmated_import_end = microtime(true) + round(($time_spent/1000) * $this->remaining_ads);

            $body  = 'TOTALS:' . PHP_EOL;
            $body .= '========================================================' . PHP_EOL;
            $body .= 'Imported so far  : ' . $this->processed_ads . '(' . round(($this->processed_ads/($this->processed_ads+$this->remaining_ads))*100, 2) . '%)' .  PHP_EOL;
            $body .= 'Remaining        : ' . $this->remaining_ads . PHP_EOL;
            $body .= 'Time spent       : ' . $time_spent_total . PHP_EOL;
            $body .= 'Estimated finish : ' . date('H:i:s', $esitmated_import_end) . PHP_EOL;
            $body .= '--------------------------------------------------------' . PHP_EOL;
            $body .= ' This cycle: ' . PHP_EOL;
            $body .= ' - Imported      : ' . $this->counters['imported'] . PHP_EOL;
            $body .= ' - Duplicate     : ' . $this->counters['duplicate'] . PHP_EOL;
            $body .= ' - Errored       : ' . $this->counters['error'] . PHP_EOL;
            $body .= ' - Time spent    : ' . sprintf("%02d", $time_spent_min) . 'm ' . sprintf("%02d", $time_spent_sec). 's ' . PHP_EOL;
            $body .= ' - Average speed : ' . round(($time_spent / (intval($this->counters['imported']) ? intval($this->counters['imported']) : 1)), 4) . 'ms / ad' . PHP_EOL;
            if (count($this->log['errors'])) {
                $body .= '-[ Errors ]---------------------------------------------' . PHP_EOL;
                $body .= PHP_EOL . implode(PHP_EOL, $this->log['errors']) . PHP_EOL;
            }
            $body .= '-[ Messages ]-------------------------------------------' . PHP_EOL;
            $body .= PHP_EOL . implode(PHP_EOL, $this->log['msgs']) . PHP_EOL;
            $body .= '========================================================' . PHP_EOL;

            @mail(
                $this->log2email,
                '[' . $this->processed_ads . '/' . ($this->processed_ads+$this->remaining_ads) . '] - Oglasnik.hr ' . $this->actionName,
                $body
            );
        }
    }

    private function add2Log($msg, $type = 'info')
    {
        $msg = trim($msg);
        if ($type == 'info') {
            $this->log['msgs'][] = date('d.m.Y @ H:i:s') . ' ' . $msg;
        } else {
            $this->log['errors'][] = date('d.m.Y @ H:i:s') . ' ' . $msg;
        }

        if ($this->log2email) {
            echo $msg . PHP_EOL;
        }
    }

    public function importAdsByCategory($inet_kat)
    {
        if (!$this->last_import_id) {
            $this->last_import_id = $this->target_db->findFirst(
                "SELECT MAX(import_id) AS last_import_id FROM ads",
                null,
                true
            );
            if (!$this->last_import_id) {
                $this->last_import_id = 0;
            }
        }
        // TODO: remove this!
        $this->last_import_id = 0;

        $fields = array(
            'ad.id',
            'ad.submitted_by',
            'ad.expires',
            'ad.inet_type_id',
            'ad.inet_kat',
            'ad.title',
            'ad.papir_bold_title',
            'ad.body',
            'ad.papir_body',
            'ad.phone1',
            'ad.phone2',
            'ad.updated',
            'ad.submitted',
            'ad.cijena',
            'ad.valuta_id',
            'ad.views',
            'ad.inet_objavi',
            'ad.pbl_date',
            'ad.region',
            'ad.youtube',
            'adr.expires AS adr_expires',
            'adr.pbl_date AS adr_pbl_date',
            'adr.search_text AS adr_search_text',
            'adr.sa_photo AS adr_sa_photo',
            'adr.sa_photo2 AS adr_sa_photo2'
        );

//REAL        $ads_sql_query = 'SELECT ' . implode(', ', $fields) . ' FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id AND ad.inet_kat = :inet_kat AND adr.expires > :curr_date AND ad.approval = :approval ORDER BY ad.id ASC LIMIT 0,' . $this->sql_limit;
//REAL        $ads_remaining = 'SELECT COUNT(*) AS total FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id AND ad.inet_kat = :inet_kat AND adr.expires > :curr_date AND ad.approval = :approval';
        $ads_sql_query = 'SELECT ' . implode(', ', $fields) . ' FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id AND ad.inet_kat LIKE CONCAT(:inet_kat, \'%\') AND adr.expires > :curr_date AND ad.approval = :approval ORDER BY ad.id ASC LIMIT 0,' . $this->sql_limit;
        $ads_remaining = 'SELECT COUNT(*) AS total FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id AND ad.inet_kat LIKE CONCAT(:inet_kat, \'%\') AND adr.expires > :curr_date AND ad.approval = :approval';
//        $ads_sql_query = 'SELECT ' . implode(', ', $fields) . ' FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id = 15542293';
//        $ads_remaining = 'SELECT COUNT(*) AS total FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id = 15542293';

        $ads_sql_param = array(
//*
            'last_import_id' => $this->last_import_id,
            'inet_kat'       => $inet_kat,
            'curr_date'      => '2015-09-04 00:00:00',
            'approval'       => 'Y'
//*/
        );

        $this->add2Log('--------------------------------------------------------');
        $printable_query = $ads_sql_query;
        foreach ($ads_sql_param as $key => $val) {
            $printable_query = str_ireplace(':' . $key, $val, $printable_query);
        }
        echo $printable_query . PHP_EOL;
        $this->add2Log($printable_query);
        $this->add2Log('--------------------------------------------------------');

        if ($this->remaining_ads === null) {
            // count remaining ads for import...
            $this->remaining_ads = (int)$this->source_db->findFirst($ads_remaining, $ads_sql_param, true);
        }
        $src_ads = $this->source_db->find($ads_sql_query, $ads_sql_param);

        $found_ads = $src_ads && count($src_ads) ? count($src_ads) : 0;
        if ($found_ads) {
            $this->add2Log('Importing ' . $found_ads . ' ad' . ($found_ads > 1 ? 's' : '') . ($this->remaining_ads ? ' out of ' . $this->remaining_ads . ' remaining!' : ''));
            $this->add2Log('--------------------------------------------------------');

            foreach ($src_ads as $src_ad) {
                $ad = null;
                if ($src_ad) {
                    $this->last_import_id = $src_ad['id'];
                    $ad = $this->target_db->findFirst(
                        'SELECT * FROM ads WHERE import_id = :import_id LIMIT 0, 1',
                        array(
                            'import_id' => $src_ad['id']
                        )
                    );

                    if (!$ad) {
                        $src_ad['pictures'] = null;
                        $src_ad_pictures = $this->source_db->find(
                            'SELECT * FROM ads_photos WHERE ad_id = :ad_id',
                            array(
                                'ad_id' => intval($src_ad['id'])
                            )
                        );
                        if ($src_ad_pictures) {
                            $src_ad['pictures'] = $src_ad_pictures;
                        } else {
                            if (trim($src_ad['adr_sa_photo']) || trim($src_ad['adr_sa_photo2'])) {
                                $sa_photos = array();
                                if (trim($src_ad['adr_sa_photo'])) {
                                    $sa_photo = str_replace('_sa.', '.', trim($src_ad['adr_sa_photo']));
                                    $sa_photos[] = array('id' => 1, 'photo' => $sa_photo);
                                }
                                if (trim($src_ad['adr_sa_photo2'])) {
                                    $sa_photo2 = str_replace('_sa.', '.', trim($src_ad['adr_sa_photo2']));
                                    $sa_photos[] = array('id' => 2, 'photo' => $sa_photo2);
                                }
                                if (count($sa_photos)) {
                                    $src_ad['pictures'] = $sa_photos;
                                }
                            }
                        }

                        if (isset($src_ad['submitted_by']) && intval($src_ad['submitted_by'])) {
                            $src_user = $this->source_db->findFirst(
                                'SELECT * FROM users WHERE id = :id',
                                array(
                                    'id' => intval($src_ad['submitted_by'])
                                )
                            );
                        } else {
                            $src_user = array();
                        }

                        if ($ad = $this->importAd($src_user, $src_ad)) {
                            $this->counters['imported']++;
                            $this->counters_total['imported']++;
                            $ad_pics = '';
                            if ($ads_pics = count($src_ad['pictures'])) {
                                $ad_pics = ' + ' . $ads_pics . ' pic(s)';
                            }
                            $this->add2Log('Imported ad ' . $src_ad['id'] . ' -> ' . $ad['id'] . $ad_pics);
                        } else {
                            $this->counters['error']++;
                            $this->counters_total['error']++;
                            $this->add2Log('Error ad ' . $src_ad['id']);
                            $this->add2Log(print_r(array(
                                'msg'  => 'Import Error',
                                'user' => $src_user,
                                'ad'   => $src_ad
                            ), true), 'error');
                        }
                    } else {
                        $this->counters['duplicate']++;
                        $this->counters_total['duplicate']++;
                        $this->add2Log('Duplicate ad ' . $src_ad['id']);
                    }
                    $this->processed_ads++;
                    $this->remaining_ads--;
                }
            }
        }

        if ($this->remaining_ads > 0) {
            $this->add2Log('--------------------------------------------------------');
            $this->add2Log('-- RESTARTING IMPORT TASK -> ' . $this->remaining_ads . ' ads remaining!');
            $this->add2Log('--------------------------------------------------------');
            $this->sendMail();
            sleep(1);

            \pcntl_exec($_SERVER['_'], $this->get_run_argv());
        }

        return $this->counters_total;
    }

    public function importAds()
    {
        if (!$this->last_import_id) {
            $this->last_import_id = $this->target_db->findFirst(
                "SELECT MAX(import_id) AS last_import_id FROM ads_additional_data",
                null,
                true
            );
            if (!$this->last_import_id) {
                $this->last_import_id = 0;
            }
        }

        if (!$this->processed_ads) {
            $this->processed_ads = $this->target_db->findFirst(
                "SELECT COUNT(*) 'total' FROM ads WHERE import_id IS NOT NULL",
                null,
                true
            );
            if (!$this->processed_ads) {
                $this->processed_ads = 0;
            }
        }

        $fields = array(
            'ad.id',
            'ad.submitted_by',
            'ad.expires',
            'ad.inet_type_id',
            'ad.inet_kat',
            'ad.title',
            'ad.papir_bold_title',
            'ad.body',
            'ad.papir_body',
            'ad.phone1',
            'ad.phone2',
            'ad.updated',
            'ad.submitted',
            'ad.cijena',
            'ad.valuta_id',
            'ad.views',
            'ad.inet_objavi',
            'ad.pbl_date',
            'ad.region',
            'ad.youtube',
            'ad.avus_id',
            'adr.expires AS adr_expires',
            'adr.pbl_date AS adr_pbl_date',
            'adr.search_text AS adr_search_text',
            'adr.sa_photo AS adr_sa_photo',
            'adr.sa_photo2 AS adr_sa_photo2'
        );

        $ads_sql_query = 'SELECT ' . implode(', ', $fields) . ' FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id AND adr.expires > :curr_date AND ad.approval = :approval ORDER BY ad.id ASC LIMIT 0,' . $this->sql_limit;
        $ads_remaining = 'SELECT COUNT(*) AS total FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id AND adr.expires > :curr_date AND ad.approval = :approval';

        $ads_sql_param = array(
            'last_import_id' => $this->last_import_id,
            'curr_date'      => '2015-09-04 00:00:00',
            'approval'       => 'Y'
        );

        $this->add2Log('--------------------------------------------------------');
        $printable_query = $ads_sql_query;
        foreach ($ads_sql_param as $key => $val) {
            $printable_query = str_ireplace(':' . $key, $val, $printable_query);
        }
        $this->add2Log($printable_query);
        $this->add2Log('--------------------------------------------------------');

        if ($this->remaining_ads === null) {
            // count remaining ads for import...
            $this->remaining_ads = (int)$this->source_db->findFirst($ads_remaining, $ads_sql_param, true);
        }
        $src_ads = $this->source_db->find($ads_sql_query, $ads_sql_param);

        $found_ads = $src_ads && count($src_ads) ? count($src_ads) : 0;
        if ($found_ads) {
            $this->add2Log('Importing ' . $found_ads . ' ad' . ($found_ads > 1 ? 's' : '') . ($this->remaining_ads ? ' out of ' . $this->remaining_ads . ' remaining!' : ''));
            $this->add2Log('--------------------------------------------------------');

            foreach ($src_ads as $src_ad) {
                $ad = null;
                if ($src_ad) {
                    $this->last_import_id = $src_ad['id'];
                    $ad = $this->target_db->findFirst(
                        'SELECT * FROM ads WHERE import_id = :import_id LIMIT 0, 1',
                        array(
                            'import_id' => $src_ad['id']
                        )
                    );

                    if (!$ad) {
                        $src_ad['pictures'] = null;
                        if (intval($src_ad['submitted_by']) !== 155150) {
                            $src_ad_pictures = $this->source_db->find(
                                'SELECT * FROM ads_photos WHERE ad_id = :ad_id',
                                array(
                                    'ad_id' => intval($src_ad['id'])
                                )
                            );
                            if ($src_ad_pictures) {
                                $src_ad['pictures'] = $src_ad_pictures;
                            }
                            if (trim($src_ad['adr_sa_photo']) || trim($src_ad['adr_sa_photo2'])) {
                                if (trim($src_ad['adr_sa_photo'])) {
                                    if (!$src_ad['pictures']) {
                                        $src_ad['pictures'] = array();
                                    }
                                    $sa_photo = str_replace('_sa.', '.', trim($src_ad['adr_sa_photo']));
                                    $src_ad['pictures'][] = array('id' => 1, 'photo' => $sa_photo);
                                }
                                if (trim($src_ad['adr_sa_photo2'])) {
                                    if (!$src_ad['pictures']) {
                                        $src_ad['pictures'] = array();
                                    }
                                    $sa_photo2 = str_replace('_sa.', '.', trim($src_ad['adr_sa_photo2']));
                                    $src_ad['pictures'][] = array('id' => 2, 'photo' => $sa_photo2);
                                }
                            }
                        } else {
                            $medias = array();
                            if (trim($src_ad['adr_sa_photo']) === '/photos/13/68/PZcfXGUhFS7x-1975893_sa.jpeg') {
                                $medias[] = 919512;
                            }
                            if (trim($src_ad['adr_sa_photo']) === '/photos/92/d1/ayF4c76mHseG-1128639_sa.gif') {
                                $medias[] = 959667;
                                //$medias[] = 956651;
                            }
                            if (count($medias)) {
                                $src_ad['medias'] = $medias;
                            }
                        }

                        if (isset($src_ad['submitted_by']) && intval($src_ad['submitted_by'])) {
                            $src_user = $this->source_db->findFirst(
                                'SELECT * FROM users WHERE id = :id',
                                array(
                                    'id' => intval($src_ad['submitted_by'])
                                )
                            );
                        } else {
                            $src_user = array();
                        }

                        if ($ad_data = $this->importAd($src_user, $src_ad)) {
                            $ad                 = $ad_data['ad'];
                            $pictures = $ad_data['pictures'];

                            $this->counters['imported']++;
                            $this->counters_total['imported']++;
                            $ad_pics = '';
                            if ($ads_pics = count($src_ad['pictures'])) {
                                $ad_pics = ' + ' . $pictures . '/' . $ads_pics . ' pic(s)';
                            }
                            $this->add2Log('Imported ad ' . $src_ad['id'] . ' -> ' . $ad['id'] . $ad_pics);
                        } else {
                            $this->counters['error']++;
                            $this->counters_total['error']++;
                            $this->add2Log('Error ad ' . $src_ad['id']);
                            $this->add2Log(print_r(array(
                                'msg'  => 'Import Error',
                                'user' => $src_user,
                                'ad'   => $src_ad
                            )), 'error');
                        }
                    } else {
                        $this->counters['duplicate']++;
                        $this->counters_total['duplicate']++;
                        $this->add2Log('Duplicate ad ' . $src_ad['id']);
                    }
                    $this->processed_ads++;
                    $this->remaining_ads--;
                }
            }
        }

        if ($this->remaining_ads > 0) {
            $this->add2Log('--------------------------------------------------------');
            $this->add2Log('-- RESTARTING IMPORT TASK -> ' . $this->remaining_ads . ' ads remaining!');
            $this->add2Log('--------------------------------------------------------');
            $this->sendMail();
            sleep(1);

            \pcntl_exec($_SERVER['_'], $this->get_run_argv());
        }

        return $this->counters;
    }

    public function fixSearchTermsAndTitleDescriptions()
    {
        $last_in_db = $this->target_db->findFirst(
            "SELECT MAX(import_id) AS last_import_id FROM ads",
            null,
            true
        );

        if (!$this->processed_ads) {
            $this->processed_ads = $this->target_db->findFirst(
                "SELECT COUNT(*) 'total' FROM ads WHERE import_id IS NOT NULL",
                null,
                true
            );
            if (!$this->processed_ads) {
                $this->processed_ads = 0;
            }
        }

        $fields = array(
            'ad.id',
            'ad.title',
            'ad.body',
            'ad.papir_body',
            'adr.search_text'
        );

        $ads_sql_query = 'SELECT ' . implode(', ', $fields) . ' FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id ' . ($last_in_db ? 'AND ad.id <= :last_in_db ' : '') . 'AND adr.expires > :curr_date AND ad.approval = :approval ORDER BY ad.id ASC LIMIT 0,' . $this->sql_limit;
        $ads_remaining = 'SELECT COUNT(*) AS total FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id ' . ($last_in_db ? 'AND ad.id <= :last_in_db ' : '') . 'AND adr.expires > :curr_date AND ad.approval = :approval';

        $ads_sql_param = array(
            'last_import_id' => $this->last_import_id,
            'curr_date'      => '2015-09-04 00:00:00',
            'approval'       => 'Y'
        );
        if ($last_in_db) {
            $ads_sql_param['last_in_db'] = $last_in_db;
        }

        $this->add2Log('--------------------------------------------------------');
        $printable_query = $ads_sql_query;
        foreach ($ads_sql_param as $key => $val) {
            $printable_query = str_ireplace(':' . $key, $val, $printable_query);
        }
        $this->add2Log($printable_query);
        $this->add2Log('--------------------------------------------------------');

        if ($this->remaining_ads === null) {
            // count remaining ads for import...
            $this->remaining_ads = (int)$this->source_db->findFirst($ads_remaining, $ads_sql_param, true);
        }
        $src_ads = $this->source_db->find($ads_sql_query, $ads_sql_param);

        $found_ads = $src_ads && count($src_ads) ? count($src_ads) : 0;
        if ($found_ads) {
            $this->add2Log('Fixing ' . $found_ads . ' ad' . ($found_ads > 1 ? 's' : '') . ($this->remaining_ads ? ' out of ' . $this->remaining_ads . ' remaining!' : ''));
            $this->add2Log('--------------------------------------------------------');

            foreach ($src_ads as $src_ad) {
                $ad = null;
                if ($src_ad) {
                    $this->last_import_id = $src_ad['id'];
                    $ad = $this->target_db->findFirst(
                        'SELECT * FROM ads WHERE import_id = :import_id LIMIT 0, 1',
                        array(
                            'import_id' => $src_ad['id']
                        )
                    );
                    if ($ad) {
                        $source_search_terms = trim($src_ad['search_text']) ? trim($this->toUTF8($src_ad['search_text'])) : '';
                        $source_search_terms = $source_search_terms ? array_unique(array_map('trim', explode(',', $source_search_terms))) : array();

                        $ad_json_data = !empty($ad['json_data']) ? json_decode($ad['json_data'], true) : null;
                        $search_terms_tmp = trim(Parametrizator::get_search_terms($ad, $ad_json_data));
                        $search_terms = $search_terms_tmp ? array_unique(array_map('trim', explode(',', $search_terms_tmp))) : array();

                        $merged_search_terms = array_unique(array_merge($search_terms, $source_search_terms));

                        $ad_content = trim($this->toUTF8($src_ad['body']));
                        $ad_papir_body = $ad_content;
                        if (isset($src_ad['papir_body']) && !empty($src_ad['papir_body'])) {
                            $ad_papir_body = trim($this->toUTF8($src_ad['papir_body']));
                            if (!$ad_content) {
                                $ad_content = $ad_papir_body;
                            }
                        }
                        $ad_content = trim(strip_tags($ad_content, '<br><br/><br />'));

                        $ad_title = trim(strip_tags($src_ad['title'])) ? trim($this->toUTF8($src_ad['title'])) : null;
                        $ad_body = $this->fixTextFormatting(strip_tags($ad_content, '<br><br/>'));

                        $gen_title = $this->generateTitle($ad_title, $ad_content);

                        if (count($merged_search_terms) == 0) {
                            if (trim($ad_body)) {
                                $merged_search_terms[] = $ad_body;
                            }
                            if (trim($gen_title)) {
                                $merged_search_terms[] = $gen_title;
                            }
                        }
                        $merged_search_terms = $this->fixTextFormatting(implode(' ', $merged_search_terms), true);

                        $msg = '';
                        if ($gen_title && $ad_body) {
                            $update_ad_sql = "UPDATE ads SET title = :title, description = :description, description_tpl = :description_tpl, description_offline = :description_offline WHERE id = :ad_id";
                            $updated_ad = $this->target_db->update(
                                $update_ad_sql,
                                array(
                                    'ad_id'               => $ad['id'],
                                    'title'               => $gen_title,
                                    'description'         => $ad_body,
                                    'description_tpl'     => $ad_body,
                                    'description_offline' => $ad_papir_body
                                )
                            );
                            if ($updated_ad && $merged_search_terms) {
                                $msg = 'Updated AD [' . $ad['id'] . ']';
                                $update_ad_searchterm_sql = "UPDATE ads_search_terms SET search_data = :search_data, search_data_unaccented = :search_data_unaccented WHERE ad_id = :ad_id";
                                $updated_ad_searchterm = $this->target_db->update(
                                    $update_ad_searchterm_sql,
                                    array(
                                        'ad_id'                  => $ad['id'],
                                        'search_data'            => $merged_search_terms,
                                        'search_data_unaccented' => Utils::remove_accents($merged_search_terms)
                                    )
                                );
                                if ($updated_ad_searchterm) {
                                    $msg .= ' + search_data';
                                }
                            }
                        }

                        if ($msg) {
                            $this->counters['fixed']++;
                            $this->add2Log($msg);
                        } else {
                            $this->counters['error']++;
                            $this->add2Log('Error fixing AD [' . $ad['id'] . ']', 'error');
                            $this->add2Log(print_r(array(
                                'oad' => $src_ad,
                                'ad'  => $ad
                            )), 'error');
                        }
                    } else {
                        $this->counters['error']++;
                        $this->add2Log('AD not found in target DB [' . $src_ad['id'] . ']', 'error');
                    }
                    $this->processed_ads++;
                    $this->remaining_ads--;
                }
            }
        }

        if ($this->remaining_ads > 0 && $this->last_import_id < $last_in_db) {
            $this->add2Log('--------------------------------------------------------');
            $this->add2Log('$this->last_import_id ['.$this->last_import_id.'] < ['.$last_in_db.'] $last_in_db');
            $this->add2Log('-- RESTARTING FIX TASK -> ' . $this->remaining_ads . ' ads remaining!');
            $this->add2Log('--------------------------------------------------------');
            $this->sendMail();
            sleep(1);

            \pcntl_exec($_SERVER['_'], $this->get_run_argv());
        }

        return $this->counters;
    }

    public function fixSpecificSearchTermsAndTitleDescriptions()
    {
        $target_ads = $this->target_db->find(
            "SELECT a.* FROM ads a INNER JOIN ads_search_terms ast ON a.id = ast.ad_id WHERE ast.search_data_unaccented IS NULL ORDER BY a.id ASC LIMIT 0," . $this->sql_limit
        );

        if (count($target_ads)) {
            foreach ($target_ads as $ad) {
                $ad_json_data = !empty($ad['json_data']) ? json_decode($ad['json_data'], true) : null;
                $search_terms_tmp = trim(Parametrizator::get_search_terms($ad, $ad_json_data));
                $search_terms = $search_terms_tmp ? array_unique(array_map('trim', explode(',', $search_terms_tmp))) : array();

                $title = trim(strip_tags($ad['title'])) ? trim($ad['title']) : null;
                $description = $this->fixTextFormatting(strip_tags($ad['description'], '<br><br/>'));
                $description_offline = $description;

                $title = $this->generateTitle($title, $description);

                if (count($search_terms) == 0) {
                    if (trim($description)) {
                        $search_terms[] = $description;
                    }
                    if (trim($title)) {
                        $search_terms[] = $title;
                    }
                }
                $search_terms = $this->fixTextFormatting(implode(' ', $search_terms), true);

                $msg = '';
                if ($search_terms && $title && $description) {
                    $update_ad_searchterm_sql = "UPDATE ads_search_terms SET search_data = :search_data, search_data_unaccented = :search_data_unaccented WHERE ad_id = :ad_id";
                    $updated_ad_searchterm = $this->target_db->update(
                        $update_ad_searchterm_sql,
                        array(
                            'ad_id'                  => $ad['id'],
                            'search_data'            => $search_terms,
                            'search_data_unaccented' => Utils::remove_accents($search_terms)
                        )
                    );
                    if ($updated_ad_searchterm) {
                        $msg = 'Updated search_data for ad: ' . $ad['id'] . PHP_EOL;
                    }
                }

                if ($msg) {
                    $this->counters['fixed']++;
                    $this->add2Log($msg);
                } else {
                    $this->counters['error']++;
                    $this->add2Log('Error fixing AD [' . $ad['id'] . ']', 'error');
                    $this->add2Log(print_r(array(
                        'ad'  => $ad
                    )), 'error');
                }
            }
        }

        return $this->counters;
    }

    public function importUsers()
    {
        $user_sql_query = 'SELECT * FROM (SELECT `u`.* FROM `users` AS `u` INNER JOIN `login_log` AS `ul` ON `u`.`id` = `ul`.`user_id` WHERE `ul`.`time` >= :two_yrs_in_past AND `ul`.`action` = :log_action GROUP BY `u`.`id`) AS `tmp` WHERE `tmp`.`id` > :last_import_id ORDER BY `id` ' . $this->order . ' LIMIT 0,' . $this->sql_limit;
        $user_remaining = 'SELECT COUNT(*) FROM (SELECT `u`.* FROM `users` AS `u` INNER JOIN `login_log` AS `ul` ON `u`.`id` = `ul`.`user_id` WHERE `ul`.`time` >= :two_yrs_in_past AND `ul`.`action` = :log_action GROUP BY `u`.`id`) AS `tmp` WHERE `tmp`.`id` > :last_import_id ORDER BY `id` ' . $this->order . '';

        $user_sql_param = array(
            'last_import_id'  => $this->last_import_id,
            'two_yrs_in_past' => '2013-09-01 00:00:00',
            'log_action'      => 'login'
        );

        $this->add2Log('--------------------------------------------------------');
        $printable_query = $user_sql_query;
        foreach ($user_sql_param as $key => $val) {
            $printable_query = str_ireplace(':' . $key, $val, $printable_query);
        }
        $this->add2Log($printable_query);
        $this->add2Log('--------------------------------------------------------');

        if ($this->remaining_ads === null) {
            // count remaining ads for import...
            $this->remaining_ads = (int)$this->source_db->findFirst($user_remaining, $user_sql_param, true);
        }
        $src_users = $this->source_db->find($user_sql_query, $user_sql_param);

        $found_users = $src_users && count($src_users) ? count($src_users) : 0;
        if ($found_users) {
            $this->add2Log('Importing ' . $found_users . ' ad' . ($found_users > 1 ? 's' : '') . ($this->remaining_ads ? ' out of ' . $this->remaining_ads . ' remaining!' : ''));
            $this->add2Log('--------------------------------------------------------');

            foreach ($src_users as $i => $src_user) {
                $this->last_import_id = $src_user['id'];

                $db_user = $this->findTargetUser($src_user, $return_only_newly_created = true);

                if ($db_user) {
                    $this->counters['imported']++;
                    $this->counters_total['imported']++;
                    $this->add2Log( ($i+1) . '. Imported user ' . $src_user['id'] . ' -> ' . $db_user['id']);
                } else {
                    $this->counters['error']++;
                    $this->counters_total['error']++;
                    //$this->add2Log('Error user ' . $src_user['id']);
                }

                $this->processed_ads++;
                $this->remaining_ads--;
            }
        }

        if ($this->remaining_ads > 0) {
            $this->add2Log('--------------------------------------------------------');
            $this->add2Log('-- RESTARTING IMPORT TASK -> ' . $this->remaining_ads . ' users remaining!');
            $this->add2Log('--------------------------------------------------------');
            $this->sendMail();
            sleep(1);

            \pcntl_exec($_SERVER['_'], $this->get_run_argv());
        }

        return $this->counters_total;
    }

    public function fixAdBody()
    {
        $last_in_db = $this->target_db->findFirst(
            "SELECT MAX(import_id) AS last_import_id FROM ads",
            null,
            true
        );

        if (!$this->processed_ads) {
            $this->processed_ads = $this->target_db->findFirst(
                "SELECT COUNT(*) 'total' FROM ads WHERE import_id IS NOT NULL",
                null,
                true
            );
            if (!$this->processed_ads) {
                $this->processed_ads = 0;
            }
        }

        $fields = array(
            'ad.id',
            'ad.body',
            'ad.papir_body'
        );

        $ads_sql_query = 'SELECT ' . implode(', ', $fields) . ' FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id ' . ($last_in_db ? 'AND ad.id <= :last_in_db ' : '') . 'AND adr.expires > :curr_date AND ad.approval = :approval ORDER BY ad.id ASC LIMIT 0,' . $this->sql_limit;
        $ads_remaining = 'SELECT COUNT(*) AS total FROM ads AS ad INNER JOIN ads_read AS adr ON ad.id = adr.id WHERE ad.id > :last_import_id ' . ($last_in_db ? 'AND ad.id <= :last_in_db ' : '') . 'AND adr.expires > :curr_date AND ad.approval = :approval';

        $ads_sql_param = array(
            'last_import_id' => $this->last_import_id,
            'curr_date'      => '2015-09-04 00:00:00',
            'approval'       => 'Y'
        );
        if ($last_in_db) {
            $ads_sql_param['last_in_db'] = $last_in_db;
        }

        $this->add2Log('--------------------------------------------------------');
        $printable_query = $ads_sql_query;
        foreach ($ads_sql_param as $key => $val) {
            $printable_query = str_ireplace(':' . $key, $val, $printable_query);
        }
        $this->add2Log($printable_query);
        $this->add2Log('--------------------------------------------------------');

        if ($this->remaining_ads === null) {
            // count remaining ads for import...
            $this->remaining_ads = (int)$this->source_db->findFirst($ads_remaining, $ads_sql_param, true);
        }
        $src_ads = $this->source_db->find($ads_sql_query, $ads_sql_param);

        $found_ads = $src_ads && count($src_ads) ? count($src_ads) : 0;
        if ($found_ads) {
            $this->add2Log('Fixing ' . $found_ads . ' ad' . ($found_ads > 1 ? 's' : '') . ($this->remaining_ads ? ' out of ' . $this->remaining_ads . ' remaining!' : ''));
            $this->add2Log('--------------------------------------------------------');

            foreach ($src_ads as $src_ad) {
                $ad = null;
                if ($src_ad) {
                    $this->last_import_id = $src_ad['id'];
                    $ad_body = $this->fixTextFormatting(strip_tags($this->toUTF8($src_ad['body']), '<br><br/>'));
                    if ($ad_body) {
                        $ad_papir_body = $ad_body;
                        if (isset($src_ad['papir_body']) && !empty($src_ad['papir_body'])) {
                            $ad_papir_body = trim($this->toUTF8($src_ad['papir_body']));
                        }

                        $ad = $this->target_db->findFirst(
                            'SELECT * FROM ads WHERE import_id = :import_id LIMIT 0, 1',
                            array(
                                'import_id' => $src_ad['id']
                            )
                        );
                        if ($ad) {
                            $msg = '';
                            if ($ad_body && $ad_papir_body) {
                                $update_ad_sql = "UPDATE ads SET description = :description, description_tpl = :description_tpl WHERE id = :ad_id";
                                $updated_ad = $this->target_db->update(
                                    $update_ad_sql,
                                    array(
                                        'ad_id'           => $ad['id'],
                                        'description'     => $ad_body,
                                        'description_tpl' => $ad_papir_body
                                    )
                                );
                                if ($updated_ad) {
                                    $msg = 'Updated AD [' . $ad['id'] . ']';
                                }
                            }

                            if ($msg) {
                                $this->counters['fixed']++;
                                $this->add2Log($msg);
                            } else {
                                $this->counters['error']++;
                                $this->add2Log('Error fixing AD [' . $ad['id'] . ']', 'error');
                                $this->add2Log(print_r(array(
                                    'oad' => $src_ad,
                                    'ad'  => $ad
                                )), 'error');
                            }
                        } else {
                            $this->counters['error']++;
                            $this->add2Log('AD not found in target DB [' . $src_ad['id'] . ']', 'error');
                        }
                    }
                    $this->processed_ads++;
                    $this->remaining_ads--;
                }
            }
        }

        if ($this->remaining_ads > 0) {
            $this->add2Log('--------------------------------------------------------');
            $this->add2Log('-- RESTARTING FIX TASK -> ' . $this->remaining_ads . ' ads remaining!');
            $this->add2Log('--------------------------------------------------------');
            $this->sendMail();
            sleep(1);

            \pcntl_exec($_SERVER['_'], $this->get_run_argv());
        }

        return $this->counters;
    }

    public static function importFromAvus()
    {
        $result = null;
        $options = array(
            'sql_limit' => 1000
        );

        $migration = new \Baseapp\Library\Mapping\MigrationRawDb($options);
        $ads = $migration->importAds();
        if ($ads) {
            $cummulative_counters = array();
            if (isset($ads['imported']) && intval($ads['imported'])) {
                $cummulative_counters[] = 'Succesfull: ' . intval($ads['imported']);
            }
            if (isset($ads['duplicate']) && intval($ads['duplicate'])) {
                $cummulative_counters[] = 'Duplicated: ' . intval($ads['duplicate']);
            }
            if (isset($ads['error']) && intval($ads['error'])) {
                $cummulative_counters[] = 'Errored: ' . intval($ads['error']);
            }

            if (!empty($cummulative_counters)) {
                $result = implode('; ', $cummulative_counters);
            }
        }

        return $result;
    }

}
