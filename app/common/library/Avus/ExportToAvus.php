<?php

namespace Baseapp\Library\Avus;

use Baseapp\Models\Ads;
use Baseapp\Models\AdsOfflineExportHistory;
use Baseapp\Library\Utils;
use Baseapp\Bootstrap;
use Baseapp\Traits\AuditLog;

class ExportToAvus extends BaseAvus
{
    use AuditLog;

    /**
     * List of all the fields that need to be popullated in online2avus table
     * with a brief explanation of what we figured out base on just analysing
     * the table :)
     *
     * field_name      type                            description
     * --------------------------------------------------------------------------------
     * id              int(11)                         autoinc
     * SYNONYM         varchar(80)                     users phone number
     * PHONE           varchar(40)                     users phone2 number
     * EMAIL           varchar(40)                     users email address
     * URL             varchar(255)                    users web page url
     * CLASSNUMBER     varchar(40)                     classifieds (ad) id
     * ADTYPE          smallint(6)                     ad type - always 1?
     * ADFORMAT        varchar(100)                    ad format
     * ADLAYOUT        varchar(100)                    ad layout
     * ADTEXT          text                            ad text
     * ADRECORD        varchar(100)                    source ['Internet','SMS']
     * OBJECTS         varchar(255)                    week days ['ucs']
     * APPEARANCEDATE  varchar(255)                    ???
     * FOREIGNNUMBER   int(11)                         avus foreign number?
     * PAYMENTSTATEID  smallint(6)                     always is 2 (1 bank transfer, 2 bank debit)
     * BLOCKORDERID    int(11)                         always is 0
     * BLOCKTYPEID     smallint(6)                     always is 0
     * status          enum['Y','N']                   always is 'N'
     * broj_objava     int(11)                         number of repetitions
     * title           varchar(30)                     always is NULL
     * exported        enum['Y','E','P','N','R','D']   [Yes|Error|Pending|No|R?epeat|Duplicate]
     * export_time     datetime                        always is NULL
     * ad_id           bigint(20)                      id of the ad
     */

    protected $module  = null;
    protected $ad      = null;
    protected $product = null;
    protected $user    = null;
    protected $row     = null;

    public $export_table_name      = 'online2avus';
    public $paper_issue_table_name = 'paper_issues_future';
    public $appearance_date        = null;
    public $appearance_date_record = null;

    public function __construct(Ads $ad)
    {
        parent::__construct();

        $this->module = $this->router->getModuleName();

        if (!empty($this->config->avus->export_table)) {
            $this->export_table_name = trim($this->config->avus->export_table);
        }
        if (!empty($this->config->avus->paper_issue_table)) {
            $this->paper_issue_table_name = trim($this->config->avus->paper_issue_table);
        }

        if (isset($ad->id) && intval($ad->id) && !empty($ad->offline_product)) {
            $this->ad      = $ad;
            $this->product = unserialize($ad->offline_product);
            $this->user    = $ad->getUser();

            $this->appearance_date = $this->nextAppearanceDate();

            // initialize the row that should be imported to db
            $this->row = array(
                'CLASSNUMBER'    => $this->get_offline_category_id($ad),
                'APPEARANCEDATE' => $this->appearance_date,
                'OBJECTS'        => 'ucs',
                'BLOCKORDERID'   => 0,
                'BLOCKTYPEID'    => 0,
                'status'         => 'N',
                'title'          => null,
                'exported'       => 'N',
                'export_time'    => date('Y-m-d H:i:s', Utils::getRequestTime()),
                'ad_id'          => $ad->id
            );
            $this->initialize_user_details();
            $this->initialize_ad_details();
            $this->set_foreignnumber($ad->id);

            $this->appearance_date_record = AdsOfflineExportHistory::findFirst(array(
                'conditions' => 'ad_id = :ad_id: AND appearance_date = :appearance_date:',
                'bind'       => array(
                    'ad_id'           => $this->ad->id,
                    'appearance_date' => $this->appearance_date
                )
            ));
        }
    }

    private function saveAuditMsg($msg, $type = 'error')
    {
        $this->saveAudit($msg);

        if ('backend' === $this->module) {
            $this->flashSession->$type($msg);
        }
    }

    private function initialize_user_details()
    {

        $ad_phone_numbers = $this->ad->getPublicPhoneNumbers();
        if (!empty($ad_phone_numbers)) {
            // phone numbers should follow this pattern AREACODE/PHONENUMBER
            $this->row['SYNONYM'] = Utils::convert_phone_number_to_avus_format($ad_phone_numbers[0]);
            if (isset($ad_phone_numbers[1])) {
                $this->row['PHONE'] = Utils::convert_phone_number_to_avus_format($ad_phone_numbers[1]);
            }
        } elseif ($this->user) {
            // phone numbers should follow this pattern AREACODE/PHONENUMBER
            if (!empty($user->phone1) && $user->phone1_public) {
                $this->row['SYNONYM'] = Utils::convert_phone_number_to_avus_format(trim($user->phone1));
                if (!empty($user->phone2) && $user->phone2_public) {
                    $this->row['PHONE'] = Utils::convert_phone_number_to_avus_format(trim($user->phone2));
                }
            } elseif(!empty($user->phone2) && $user->phone2_public) {
                $this->row['SYNONYM'] = Utils::convert_phone_number_to_avus_format(trim($user->phone2));
            }
        }
        // currently we don't send/collect this info
        $this->row['EMAIL'] = null;
        $this->row['URL']   = null;
    }

    private function initialize_ad_details()
    {
        // based on current data, ADTYPE is always set to 1 (komercijalni = 2, privatni = 1)
        $this->row['ADTYPE']         = 1;
        $this->row['ADTEXT']         = $this->get_adtext();
        $this->row['ADRECORD']       = 'Internet'; // Internet|SMS
        // ad's product code (MaliBT, MaliKPT, ...)
        $this->row['ADFORMAT']       = $this->toAvusEncoding($this->product->get_avus_adformat());
        // Bold Pmali,Extra-bold,Istaknuti_Col,Istaknuti_Tis,Minibold Yellow,Obièni
        $this->row['ADLAYOUT']       = $this->toAvusEncoding($this->product->get_avus_adlayout());
        // number of repetitions
        $this->row['broj_objava']    = $this->product->getDuration();
        // payment type ... 1 - bank transfer | 2 - CreditCard
        $this->row['PAYMENTSTATEID'] = 2;
    }

    public static function getNextAppearanceDate()
    {
        return (new self(new Ads()))->nextAppearanceDate();
    }

    public static function deleteExportedRecord($exported_id)
    {
        $self    = new self(new Ads());
        $deleted = false;

        $exported_row = $self->avus_db->findFirst(
            "SELECT * FROM {$self->export_table_name} WHERE id = :exported_id",
            array(
                ':exported_id' => $exported_id
            )
        );
        if ($exported_row) {
            if ($exported_row['exported'] !== 'Y') {
                $deleted = $self->avus_db->delete(
                    "DELETE FROM {$self->export_table_name} WHERE id = :exported_id AND NOT(exported = 'Y')",
                    array(
                        ':exported_id' => $exported_id
                    )
                );
            }
        }

        return $deleted;
    }

    // Avus foreign key?
    private function set_foreignnumber($foreignnumber = null)
    {
        $this->row['FOREIGNNUMBER'] = $foreignnumber;
    }

    private function get_export_status()
    {
        $export_status = array(
            'processed'       => false,
            'appearance_date' => null
        );

        // As we save all offline export details, we have to see if we have a record where current ad is attached to 
        // current appearance_date we're working with. If the record doesn't exist, it means that we're dealing with 
        // old ads (before implementing offline export history) and we should treat them as 'processed'
        // 
        // In case we have a record, then we have to see if AVUS already processed the record (if it went to print or not).
        // If it did, there's nothing we can do
        if (!$this->appearance_date_record) {
            // see if this ad was ever exported in the past, and if yes, get the latest know export
            $last_known_offline_export = AdsOfflineExportHistory::findFirst(array(
                'conditions' => 'ad_id = :ad_id:',
                'bind'       => array(
                    'ad_id' => $this->ad->id
                ),
                'order'      => 'id DESC'
            ));

            $export_status['processed']       = true;
            $export_status['appearance_date'] = $last_known_offline_export ? $last_known_offline_export->appearance_date : 'unknown';
        } else {
            $export_status['appearance_date'] = $this->appearance_date;
            if ($this->appearance_date_record->processed) {
                $export_status['processed'] = true;
            }
        }

        return $export_status;
    }

    // ad's content - as far as we found out, max length is < 160chars and field type is TEXT?!
    private function get_adtext()
    {
        $ad_text = !empty($this->ad->description_offline) ? $this->ad->description_offline : $this->ad->description;
        return $this->toAvusEncoding(Utils::str_truncate(
            $ad_text,
            $length      = 157,
            $etc         = '...',
            $break_words = false,
            $middle      = false,
            $charset     = 'UTF-8'
        ));
    }

    public function is_valid()
    {
        $is_valid = true;
        $missing = array();

        if (empty($this->row['CLASSNUMBER'])) {
            $is_valid = false;
            $missing[] = 'CLASSNUMBER';
        }
        if (empty($this->row['SYNONYM'])) {
            $is_valid = false;
            $missing[] = 'SYNONYM';
        }
        if (empty($this->row['ADTEXT'])) {
            $is_valid = false;
            $missing[] = 'ADTEXT';
        }
        if (empty($this->row['ADFORMAT'])) {
            $is_valid = false;
            $missing[] = 'ADFORMAT';
        }
        if (empty($this->row['APPEARANCEDATE'])) {
            $is_valid = false;
            $missing[] = 'APPEARANCEDATE';
        }

        if (!$is_valid) {
            $this->saveAuditMsg('Export2Avus [ID: ' . $this->ad->id . '] is missing: ' . implode(', ', $missing) . '.', 'error');
        }

        return $is_valid;
    }

    protected function get_ad_record_from_export_table()
    {
        $appearance_date = isset($this->row['APPEARANCEDATE']) && intval($this->row['APPEARANCEDATE']) ? $this->row['APPEARANCEDATE'] : $this->nextAppearanceDate();
        return $this->avus_db->findFirst(
            "SELECT * FROM {$this->export_table_name} WHERE APPEARANCEDATE = :appearance_date AND FOREIGNNUMBER = :ad_id",
            array(
                'appearance_date' => $appearance_date,
                'ad_id'           => $this->ad->id
            )
        );
    }

    public function process()
    {
        $export_status = $this->get_export_status();
        $export_status['exported_id'] = null;

        if ($this->is_valid()) {
            if ($export_status['processed'] === false) {
                $process_action = null;

                if ($ad_record = $this->get_ad_record_from_export_table()) {
                    // we already have a record in db, so now we need to see if the record has been already processed by 
                    // AVUS (succesfully imported to avus) or not. If not (AVUS returned an error [E] or still hasn't done 
                    // anything [N]), we can make changes - otherwise we do nothing.

                    if (in_array($ad_record['exported'], array('N', 'E'))) {
                        $process_action = 'update';
                    } else {
                        $this->saveAuditMsg('Export2Avus [ID: ' . $this->ad->id . '] -> already fetched by AVUS: exported = ' . $ad_record['exported'], 'warning');
                    }
                } else {
                    $process_action = 'create';
                }

                if ('create' === $process_action) {
                    $insert_fields = array();
                    $insert_placeholders = array();
                    foreach ($this->row as $key => $value) {
                        $insert_fields[] = '`' . $key . '`';
                        $insert_placeholders[] = ':' . $key;
                    }
                    $sql = "INSERT INTO {$this->export_table_name} (" . implode(', ', $insert_fields) . ") VALUES (" . implode(', ', $insert_placeholders) . ")";
                    $result = $this->avus_db->insert(
                        $sql,
                        $this->row
                    );
                    if (intval($result)) {
                        $export_status['exported_id'] = intval($result);
                        $this->saveAuditMsg('Export2Avus [ID: ' . $this->ad->id . '] -> insert succesfull', 'success');
                    }
                } elseif ('update' === $process_action) {
                    $update_fields = array();
                    foreach ($this->row as $key => $value) {
                        $update_fields[] = '`' . $key . '` = :' . $key;
                    }
                    $sql = "UPDATE {$this->export_table_name} SET " . implode(', ', $update_fields) . " WHERE id = :id";
                    $result = $this->avus_db->update(
                        $sql,
                        array_merge($this->row, array(':id' => intval($ad_record['id'])))
                    );
                    if (intval($result)) {
                        $export_status['exported_id'] = intval($ad_record['id']);
                        $this->saveAuditMsg('Export2Avus [ID: ' . $this->ad->id . '] -> update succesfull', 'success');
                    }
                }

                if ($export_status['exported_id']) {
                    $this->appearance_date_record->exported_id = $export_status['exported_id'];
                    $this->appearance_date_record->product     = serialize($this->product);
                    $this->appearance_date_record->update();
                }
            } else {
                $this->saveAuditMsg('Export2Avus [ID: ' . $this->ad->id . '] -> already exported and linked to APPEARANCEDATE: ' . $export_status['appearance_date'], 'warning');
            }
        }

        return $export_status;
    }

    protected function get_ads_parameter_values(Ads $ad)
    {
        $parameter_values = null;

        $ad_json_data = !empty($ad->json_data) ? json_decode($ad->json_data) : null;
        if ($ad_json_data) {
            $parameter_values = array();

            foreach ($ad_json_data as $parameter_slug => $parameter_data) {
                if ('location' == $parameter_slug) {
                    $location = array();
                    foreach ($parameter_data as $index => $level) {
                        if ($index < 4) {
                            if (!empty($level->value) && intval($level->value)) {
                                $location[] = intval($level->value);
                            }
                        } else {
                            if (!empty($level->value) && trim($level->value)) {
                                $location[] = trim($level->value);
                            }
                        }
                    }
                    if (count($location)) {
                        $parameter_values[$parameter_slug] = $location;
                    }
                } elseif (!empty($parameter_data->level_1)) {
                    $levels = array();
                    foreach ($parameter_data as $level) {
                        if (!empty($level->dictionary_id) && intval($level->dictionary_id)) {
                            $levels[] = intval($level->dictionary_id);
                        }
                    }
                    if (count($levels)) {
                        $parameter_values[$parameter_slug] = $levels;
                    }
                } elseif (!empty($parameter_data->value) && intval($parameter_data->value)) {
                    $parameter_values[$parameter_slug] = intval($parameter_data->value);
                } elseif (!empty($parameter_data->dictionary_id) && intval($parameter_data->dictionary_id)) {
                    $parameter_values[$parameter_slug] = intval($parameter_data->dictionary_id);
                } elseif (count($parameter_data)) {
                    $values = array();
                    foreach ($parameter_data as $row) {
                        if (!empty($row->value) && intval($row->value)) {
                            $values[] = intval($row->value);
                        }
                    }
                    if (count($values)) {
                        $parameter_values[$parameter_slug] = $values;
                    }
                }
            }
        }

        return $parameter_values;
    }

    public function get_offline_category_id(Ads $ad)
    {
        $offline_category_id = null;

        $category = $ad->getCategory();
        $print_mapping = $category->getSettings('publication_print_mapping');

        if ($print_mapping) {
            $print_mapping = json_decode($print_mapping, true);
        }

        $ad_data = $this->get_ads_parameter_values($ad);

        if ($print_mapping && $ad_data) {
            switch ($print_mapping['type']) {
                case 'entire':
                    if (!empty($print_mapping['value']) && intval($print_mapping['value'])) {
                        $offline_category_id = trim($print_mapping['value']);
                    }
                    break;

                case 'parameter':
                    if (!empty($print_mapping['value']) && count($print_mapping['value'])) {
                        foreach ($print_mapping['value'] as $avus_rule) {
                            $avus_id = $avus_rule['avus_id'];

                            $total_rules          = count($avus_rule['rules']);
                            $found_rules          = 0;
                            $total_required_rules = 0;
                            $found_required_rules = 0;
                            $total_optional_rules = 0;
                            $found_optional_rules = 0;
                            $msgs = array();


                            // some of the rules that are listed under this avus_id are
                            // marked as 'Required', some are 'Optional'. At least one rule
                            // should be matched in order to assign an ad to this AVUS category.
                            // Also, all 'Required' rules should be fulfilled (if there are any!)
                            foreach ($avus_rule['rules'] as $parameter_rule) {
                                // by default, all parameters are considered as 'Required'
                                $is_required = true;
                                if (isset($parameter_rule['required']) && !$parameter_rule['required']) {
                                    $is_required = false;
                                    $total_optional_rules++;
                                } else {
                                    // possible 'required=false' has failed, so we assume this rule as 'Required'
                                    // and increase the $total_required_rules
                                    $total_required_rules++;
                                }

                                $rule_slug = $parameter_rule['slug'];
                                $rule_data = $parameter_rule['rule'];

                                $found = false;

                                // we found that ad has this parameter set, but we still need to check it's value(s)
                                if (!empty($ad_data[$rule_slug]) && (isset($rule_data) && !empty($rule_data))) {
                                    $ads_rule_value = $ad_data[$rule_slug];

                                    // single value parameter
                                    if (isset($rule_data['value']) && !empty($rule_data['value'])) {
                                        $rule_value = trim($rule_data['value']);

                                        if (is_array($ads_rule_value)) {
                                            if (in_array($rule_value, $ads_rule_value)) {
                                                // FOUND
                                                $found = true;
                                            }
                                        } else {
                                            if ($rule_value == $ads_rule_value) {
                                                // FOUND
                                                $found = true;
                                            }
                                        }
                                    }
                                    // multi-value parameter
                                    elseif (count($rule_data) && isset($rule_data[0]['value'])) {
                                        $total_sub_rules = count($rule_data);
                                        $found_sub_rules = 0;

                                        foreach ($rule_data as $rule) {
                                            $rule_value = trim($rule['value']);

                                            if (in_array($rule_value, $ads_rule_value)) {
                                                // FOUND SUB RULE
                                                $found_sub_rules++;
                                            }
                                        }

                                        if ($total_sub_rules == $found_sub_rules) {
                                            // FOUND
                                            $found = true;
                                        }
                                    }
                                    // what is this?
                                    else {

                                    }
                                }

                                if ($found) {
                                    $found_rules++;

                                    if ($is_required) {
                                        $found_required_rules++;
                                    } else {
                                        $found_optional_rules++;
                                    }
                                }
                            }

                            $fulfilled_rules = true;
                            if ($total_required_rules) {
                                $fulfilled_rules = ($total_required_rules == $found_required_rules);
                            }

                            if ($fulfilled_rules) {
                                // at least one optional rule must be found
                                // in order to export to this AVUS category.
                                if ($total_optional_rules) {
                                    // if we found no optional rules, and we should have,
                                    // the ad has not fulfilled requirements!
                                    if ($found_optional_rules == 0) {
                                        $fulfilled_rules = false;
                                    }
                                }
                            }

                            // if we have 'true' here, we know where this ad should be exported to,
                            // so we assign the $offline_category_id and break the loop...
                            if ($fulfilled_rules) {
                                $offline_category_id = $avus_id;
                                break;
                            }
                        }
                    }
                    break;
            }
        }

        return $offline_category_id;
    }

}
