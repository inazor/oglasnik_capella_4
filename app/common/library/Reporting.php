<?php

namespace Baseapp\Library;

use \Phalcon\Mvc\Model\Query\Builder as QueryBuilder;
use Baseapp\Models\Ads;
use Baseapp\Models\Users;
use Baseapp\Models\InfractionReports;
use Baseapp\Library\Utils;

class Reporting
{

    private $period = 31536000; // 1 year (3600 * 24 * 365);
    private $start_timestamp;
    private $start_date;

    public function __construct()
    {
        $this->start_timestamp = Utils::getRequestTime() - $this->period;
        $this->start_date = strtotime(date('Y-m-d', $this->start_timestamp));
    }

    public function getPublishedAdsByMonth()
    {

        $builder = new QueryBuilder();
        $builder
            ->addFrom('Baseapp\Models\Ads', 'ad')
            ->columns(array(
                'ad.created_at as date',
                'DATE_FORMAT(FROM_UNIXTIME(ad.created_at), \'%Y-%m\') as month_year',
                'COUNT(ad.id) as total'
            ))
            ->where(
                'ad.created_at >= :start_period:',
                array(
                    'start_period' => $this->start_date
                )
            )
            ->orderBy('month_year')
            ->groupBy('month_year');

        return $builder->getQuery()->execute();
    }

    public function getRegisteredUsers()
    {
        $builder = new QueryBuilder();
        $builder->addFrom('Baseapp\Models\Users', 'user')
        ->columns(array(
            'DATE_FORMAT(user.created_at, \'%m/%Y\') as month_year',
            'DATE_FORMAT(user.created_at, \'%Y-%m\') as year_month',
            'SUM(IF(user.type = ' . Users::TYPE_PERSONAL . ', 1, 0)) AS total_users_personal',
            'SUM(IF(user.type = ' . Users::TYPE_COMPANY . ', 1, 0)) AS total_users_company',
            'COUNT(user.id) as total'
        ))
        ->where(
            'user.created_at >= :start_period:',
            array(
                'start_period' => date('Y-m-d H:i:s', $this->start_date)
            )
        )
        ->orderBy('year_month')
        ->groupBy('year_month');

        $records = $builder->getQuery()->execute();

        $registeredUsers = array();

        $start = 1;
        $month = $this->start_date;
        while ($start <= 13) {
            $registeredUsers[date('m/Y', $month)] = array(
                'private' => 0,
                'company' => 0
            );
            $month = strtotime('first day of +1 month', $month);
            $start++;
        }

        foreach ($records as $record) {
            $mm_yyyy = (string) $record->month_year;
            $registeredUsers[$mm_yyyy]['private'] = (int) $record->total_users_personal;
            $registeredUsers[$mm_yyyy]['company'] = (int) $record->total_users_company;
        }

        return $registeredUsers;
    }

    public function getAdsCountByModerationStatus($status)
    {
        $count = Ads::count(array(
            'moderation = :moderation:',
            'bind' => array(
                'moderation' => $status
            )
        ));

        return $count;
    }

    public function getModerationWaitingCount($type = 'all')
    {
        if ($type == 'all') {
            $acceptable_payment_states = array(
                Ads::PAYMENT_STATE_FREE,
                Ads::PAYMENT_STATE_PAID,
                Ads::PAYMENT_STATE_ORDERED
            );

            $count = Ads::count(array(
                'moderation = :moderation: AND soft_delete = 0 AND online_product_id IS NOT NULL AND latest_payment_state IN (' . implode(',', $acceptable_payment_states) . ')',
                'bind' => array(
                    'moderation'    => Moderation::MODERATION_STATUS_WAITING
                )
            ));
        } else {
            $count = Ads::count(array(
                'moderation = :moderation: AND soft_delete = 0 AND online_product_id IS NOT NULL AND latest_payment_state = :payment_state:',
                'bind' => array(
                    'moderation'    => Moderation::MODERATION_STATUS_WAITING,
                    'payment_state' => $type
                )
            ));
        }

        return $count;
    }

    public function getReportedUnresolvedInfractionsCount()
    {
        $sql          = InfractionReports::buildSearchQuery($model = null, $resolvedStatus = 'no');
        $queryResults = InfractionReports::getQueryResults($sql, null, \Phalcon\Db::FETCH_OBJ);
        $rows         = InfractionReports::hydrateResults($queryResults, $resolvedStatus = 'no');
        $count        = count($rows);

        return $count;
    }
}
