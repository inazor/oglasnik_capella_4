<?php

namespace Baseapp\Library;


use Baseapp\Suva\Library\Nava;
use Baseapp\Suva\Library\libAppSetting;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;
use \Baseapp\Suva\Models\Ads as suvaAds;



class libSuva{
 
 
    const STATUS_NEW = 1; // Order record created (when "submit order" is clicked), could be payed offline
    const STATUS_COMPLETED = 2; // Order fulfilled/finalized
    const STATUS_CANCELLED = 3; // Order cancelled by the customer (or admin)
    const STATUS_EXPIRED = 4; // Will probably have to be processed via cron or something...
 
	public static function setContext( $pName, $pValue){
		if (!array_key_exists('_context', $_SESSION)){
			$_SESSION['_context']= array();
		}
		$_SESSION['_context'][$pName] = $pValue;
		
		
	}
	public static function getContext( $pName){
		$result =null;
		if (array_key_exists('_context', $_SESSION)){
			if (array_key_exists($pName, $_SESSION['_context'])){
				$result = $_SESSION['_context'][$pName];
			}
		}
		return $result;
	}

	// NIKOLA $logger se nalazi u funkcijama self::fMsg i self::fError i trenutno loga u public/test.log jer u app/logs nemamo pristup 
	private static $loggerPath = 'libCron_application_log.log';
	

	
	
    public static function Orders_saveForAd( \Baseapp\Models\Ads $ad, \Baseapp\Models\Orders $order = null, array $products = null, $extra_data = array()){

		
		// Grab the Ad's products if they're not explicitly specified/provided
        if (null === $products) {
            $products = $ad->getProducts();
        }
		
		$requiredFreeOrder = false;
		$requiredPaidOrder = false;
		
        foreach ($products as $ordered_product) {
            $cost = $ordered_product->getCost();
			if ($cost > 0) $requiredPaidOrder = true;
			else $requiredFreeOrder = true;
		}

		
		//error_log("requiredFreeOrder  $requiredFreeOrder, requiredPaidOrder $requiredPaidOrder");
		
		if ($requiredFreeOrder) $freeOrder = self::getFreeAdsListForUser($ad->user_id);
		
		
		//error_log("freeOrder ID  $freeOrder->id");
		
		
		if ( null === $order && $requiredPaidOrder ) $paidOrder = new \Baseapp\Models\Orders();
		elseif ( $order && $requiredPaidOrder ) $paidOrder = $order;
		
		if (null === $order && $requiredPaidOrder) {
            //$order = new Orders();

            // Set some defaults for new Orders created this way
            $default_data = array(
                'payment_method' => 'offline',
                'status'         => self::STATUS_NEW,
                'user_id'        => $ad->user_id
            );
        } else {
            $default_data = array();
        }

        // Merge any additional data with the defaults above (allowing override)
        $order_data = $default_data;
        if (!empty($extra_data)) {
            $order_data = array_merge($default_data, $extra_data);
        }


        if ($requiredPaidOrder){
			// Setting order data present in the array
			foreach ($order_data as $k => $v) {
				$paidOrder->$k = $v;
			}

			// Clear out any previously set related records
			$paidOrder->Items->delete();
		}

        $new_total_lp = 0;
        if (!isset($order_data['items']) || empty($order_data['items'])) {
            // Add all the given products as order line items
            if ($requiredFreeOrder) $freeItems = array();
			if ($requiredPaidOrder) $paidItems = array();
            foreach ($products as $ordered_product) {
                $cost = $ordered_product->getCost();
                // Skipping products/items with a cost of 0
                // TODO: ask Oglasnik which way should it be in the end...
                /*
                if ($cost <= 0) {
                    continue;
                }
                */

                $qty = 1;

                // TODO: do we separate product "extras" into separate order items or keep it together?
                $item             = new \Baseapp\Models\OrdersItems();
                $item->ad_id      = $ad->id;
                $item->product_id = $ordered_product->getId();
                $item->product    = serialize($ordered_product);
                $item->title      = $ordered_product->getBillingTitleWithEverything() . ' [Oglas: ' . $ad->id . ']';
                $item->qty        = $qty;
                $item->setPriceRaw($cost);
                $item->setTotalRaw($cost);

                $new_total_lp += $qty * $cost;

                // TODO: tax_rate? calculated tax_amount in a separate field?

                if ($cost > 0) $paidItems[] = $item;
				else $freeItems[] = $item;
            }
            
			if($requiredFreeOrder) $freeOrder->Items = $freeItems;
			if($requiredPaidOrder) $paidOrder->Items = $paidItems;
        } elseif($requiredPaidOrder) {
            // Add whatever was specified in parameters
            $paidOrder->Items  = $order_data['items'];
        }

		if ($requiredPaidOrder){
			// Update the new de-normalized total field if needed
			$current_total = $paidOrder->getTotal();
			if ($current_total != $new_total_lp) {
				$paidOrder->setTotalRaw($new_total_lp);
			}

			// Finally save the Order
			$paidOrderSaved = $paidOrder->save();

			if (! $paidOrderSaved) {
				return $paidOrder->getMessages();
			}
		}
		if ($requiredFreeOrder){
			// Finally save the Order
			$freeOrderSaved = $freeOrder->save();

			if (!$freeOrderSaved) {
				return $freeOrder->getMessages();
			}
		}
		
		if ($requiredPaidOrder){
			return $paidOrder;
		}
		if ($requiredFreeOrder){
			return $freeOrder;
		}
		
		
    }
	

	public function Ads_afterSave(  \Baseapp\Models\Ads $ad ){
		
		
		//dodavanje polja
		self::fUpdateAdForSuva($ad, true);
		self::setAdsInfoForFrontend($ad->id);

		// //ako se je izmijenila moderacija, treba refreshati oglas
		// if ( isset($this->id) && $this->_x_moderation !== null && $this->_x_moderation !== $this->moderation){
			// \Baseapp\Suva\Library\libCron::setAdsInfoForFrontend($this->id);
		// }
	}

//glavne funkcije
public static function fUpdateAdForSuva ( \Baseapp\Models\Ads $frontendAd = null , $pShowMessages = false ){
	//sinhronizira se ad iz Agimove funkcije na predajaOglasaController (oba order itema ve� postoje)

	global $gDebugLevel;
	
	//Agimova funkcija �alje njegovu klasu Ad, iz koje se u�itava na� ad
	if (!$frontendAd){
		Nava::sysError("updateFroontendAdForSuva: Ad not supplied");
		return;
	} 
	
	$ad = $frontendAd;
	
	
	if ($ad->n_frontend_sync_status == 'synced') {
		Nava::greska ("Frontend ad $ad->id is already synced");
		return;
	}
	
	// error_log("borna");
	
	$paidOrderId = null;
	
	//syncanje online order itema
	$txtWhere = "ad_id = $ad->id and product_id = '$ad->online_product_id' and n_frontend_sync_status = 'unsynced'";
	$ois = \Baseapp\Models\OrdersItems::find($txtWhere);
	if ($pShowMessages) Nava::sysMessage(" libFrontend:updateAdForSuva, $txtWhere found ".$ois->count()." records");
	foreach($ois as $oi){
		if (  intval($oi->price) > 0 ) $paidOrderId = $oi->order_id;
		
		$status = self::fUpdateOrderItemForSuva ( $oi,  $ad, $ad->online_product, $pShowMessages );
		if ( !$status ) Nava::greska("Ad $ad->id - order item $o->id  nije updatean");
		if ($status){
			Nava::runSql("update ads set n_fe_online_order_item_id = $oi->id where id = $ad->id");
		}
		else {
			Nava::greska("Ad $ad->id - nije kreiran frontend order item  za online proizvod ");
		}
	}

	//syncanje offline order itema
	$txtWhere = "ad_id = $ad->id and product_id = '$ad->offline_product_id' and n_frontend_sync_status = 'unsynced'";
	$ois = \Baseapp\Models\OrdersItems::find($txtWhere);
	if ($pShowMessages) Nava::sysMessage(" libFrontend:updateAdForSuva, $txtWhere found ".$ois->count()." records");	
	foreach($ois as $oi){
		if (  intval($oi->price) > 0 ) $paidOrderId = $oi->order_id;
		
		$status = self::fUpdateOrderItemForSuva ( $oi,  $ad, $ad->offline_product, $pShowMessages );
		if ( !$status ) Nava::greska("Ad $ad->id - order item $o->id  nije updatean");
		if ($status){
			Nava::runSql("update ads set n_fe_offline_order_item_id = $oi->id where id = $ad->id");
		}
		else {
			Nava::greska("Ad $ad->id - nije kreiran frontend order item  za online proizvod ");
		}
	}
		
	//Ako postoji pla�eni order updateanje ordera (tj. ako ima pla�enih oglasa - ako nema sve ide u free ads pa ne treba nista updateat)
	if ($paidOrderId){
		$status = self::fUpdateOrderForSuva ( $paidOrderId, $pShowMessages );
		if ( !$status ) Nava::greska ("Ad $ad->id - order $paidOrderId  nije updatean");
	}
}


public static function fUpdateOrderItemForSuva( \Baseapp\Models\OrdersItems $oi = null, \Baseapp\Models\Ads $ad = null, $pProductSerialized = null, $pShowMessages = false ){
	/*
	funkcija updateOrderItemForSuva (order_item.id)
		processSerializedProduct(oi.product)
		findSuvaProduct()
		usporediti cijenu na order itemu i cijenu na suva proizvodu
			ako nisu iste - error (OrdersItems, cijena nije ista kao i cijena na Productu No.11 )
		updateati na order itemu
			n_price
			n_publications_id
			n_products_id
			n_total
			n_total_with_tax tota- * tax_amount
			n_category_id
			n_is_active
			n_frontend_sync_status = synced
		ako je offline proizvod
			dodati slijedeaih insertiona koliko je broj objava
				ia po svim issues, status = prima, publikacija je OGLA
		
		vraaa: true ili false
	*/

	global $gDebugLevel;
	
	//PROVJERE PARAMETARA
	if ( !$oi ) {
		Nava::sysError ("Order Item not supplied to public static function self::fUpdateOrderItemForSuva ");
		return null;
	}
	
	if ( !$pProductSerialized ) {
		Nava::sysError ("Serialized product not supplied to public static function self::fUpdateOrderItemForSuva ");
		return null;
	}
 
	//vraca distionary s poljima po kojima se tra�i suva  proizvod
	$frontendProduct = self::fProcessSerializedProduct ( $pProductSerialized );
	
	$suvaProduct = self::fFindSuvaProduct ( $frontendProduct );
	if (!$suvaProduct ) {
		Nava::greska ("Suva product for orderitem $oi->id not found.");
		return false;
	}
	
	$vatRate = self::getAppSetting('fin_vat_rate');
	if (!$vatRate){
		Nava::sysError("VAT Rate needs to be entered in the Application Settings");
		return null;
	}
	
	$publication = $suvaProduct->Publication();

	
	//14.10.2016 - dogovor s Patricijom: Ako oglas nije aktivan, onda ostaje u statusu unsynced, i ponovo se poku�ava sinhronizirati
	
	// $arrOi = $oi->toArray();
	// $arrOi['product']="";
	// error_log("oi ".print_r($arrOi, true));
	// error_log("oi $oi->price ".((int)$oi->price + 1));
	//PRIPREMA SQL-A
	$n_price = ( (int)$oi->price ) / (1 + $vatRate) ;//patricija - na frintendu su cijene s PDV-om
	$n_publications_id = $publication->id;
	$n_products_id = $suvaProduct->id;
	
	$n_total = ( (int)$oi->total) / (1 + $vatRate) ; //patricija - na frintendu su cijene s PDV-om

	// //kod oglasa koji jo� nisu aktivni ne postoji first published at, samo created at i expires at
	// $n_first_published_at = date("Y-m-d H:i:s", ( $ad['first_published_at'] === null ? $ad['created_at'] : $ad['first_published_at'] ) );

	//2.1.2017 - dodaje se published_at a ne first_published_at (patricija)
	//kod oglasa koji jo� nisu aktivni ne postoji first published at, samo created at i expires at
	$n_first_published_at = date("Y-m-d H:i:s", ( $ad->first_published_at === null ? $ad->created_at : $ad->first_published_at ) );

	
	$n_expires_at = date("Y-m-d H:i:s", $ad->expires_at);
	$n_total_with_tax = $n_total * ( 1 + $vatRate );
	$n_category_id = $ad->category_id;
	$n_is_active = 1;
	$n_frontend_sync_status =  'synced';

	
	$txtSql = "
		update orders_items set
			n_price = $n_price
			, n_publications_id = $n_publications_id
			, n_expires_at = '$n_expires_at' 
			, n_products_id = $n_products_id
			, n_first_published_at = '$n_first_published_at' 
			, n_total = $n_total
			, n_total_with_tax = $n_total_with_tax
			, n_category_id = $n_category_id
			, n_is_active = $n_is_active
			, n_frontend_sync_status = '$n_frontend_sync_status'
		where id = $oi->id
	";
	
	if ($pShowMessages) Nava::sysMessage ($txtSql);
	Nava::runSql($txtSql);

	//INSERTIONI - ubacivanje insertiona za offline izdanje

	if ( $suvaProduct->is_online_product == 1 ) return true;
	//ISSUES - dohvat onoliko koliko je definirano na Suva productu
	//PRIPREMA SQL-A
	$publications_id = $publication->id;
	$noIssues = $suvaProduct->days_published > 0 ?  $suvaProduct->days_published : 1 ;
	if ($pShowMessages) Nava::sysMessage ("Suva proizvod $suvaProduct->id ima  $noIssues objava.");
	
	// $issues = \Baseapp\Suva\Models\Issues::find(array( " status = 'prima' and publications_id = $publications_id ", "limit" => $noIssues , "order" => "date_published" ) );
	$recIssues = Nava::sqlToArray("select * from n_issues where status = 'prima' and publications_id = $publications_id order by date_published limit $noIssues" );
	
	if ( count($recIssues) != $noIssues ){
		Nava::sysError (" wrong number of available issues found when creating insertions for order item $oi->id, product ".$suvaProduct['id'].", publication $publications_id, ( $noIssues required and ".$issues->count()." available). MAYBE PRODUCT IS WRONGLY SET TO BE OFFLINE?" );
		return null;
	}
	$maxDate = 0;
	foreach ( $recIssues as $issue ){
		$dateIssue = strtotime($issuedate_published);
		// self::fMsg("DATE ISSUE $dateIssue");
		if ( $dateIssue > $maxDate ) $maxDate = $dateIssue;
		// self::fMsg("IF DATE ISSUE > MAXDATE $maxDate");
		//PRIPREMA SQL-A
		$created_at = date ("Y-m-d h:I:s");
		$product_id = $suvaProduct->id;
		$orders_items_id = $oi->id;
		$issues_id = $issue['id'];

		$txtSql = "
			insert into n_insertions (
				created_at 
				, product_id 
				, orders_items_id 
				, issues_id 
			)
			values (
				'$created_at'
				, $product_id
				, $orders_items_id
				, $issues_id
			);
		";
		if ($pShowMessages) Nava::sysMessage("ISSUE: $issues_id  SQL:".$txtSql);
		Nava::runSql($txtSql);
	}
	if ($maxDate > 0){
		$dateString = date("Y-m-d h:I:s", $maxDate);
		Nava::runSql("update orders_items set n_expires_at = '$dateString' where id = $oi->id ");
		if ($pShowMessages)  Nava::sysMessage("Expires At for OrderItem $oi->id update to $dateString");
	}
	
	return true;
	
}

public static function fUpdateOrderForSuva( $pOrderId ,$pShowMessages = false ){
	/*
	funkcija self::fUpdateOrderForSuva (order_id)
		azurirat polja
			n_status
			n_total
			n_frontend_sync_status
		vraaa: true ili false
	*/
	error_log("borna");
	global $gDebugLevel;
	//PROVJERE PARAMETARA
	if ( !$pOrderId ) {
		Nava::sysError ("Order ID not supplied to public static function self::fUpdateOrderForSuva ", "SYS_ERR");
		return false;
	}
	
	//DOHVAT MODELA
	$order = Nava::sqlToArray("select * from orders where id = $pOrderId")[0];
	$fiscal_location = Nava::sqlToArray("select * from n_fiscal_locations where nav_fiscal_location_id = 'ONL1' limit 1")[0];
	$sumOrdersItems = Nava::sqlToArray("
		select 
			SUM(n_total) as n_total
			,SUM(n_total_with_tax) as n_total_with_tax
		from 
			orders_items
		where order_id = $pOrderId
	")[0];
	
	//PRIPREMA SQL-A
	
	//n_status
	/*
		const STATUS_NEW = 1; // Order record created (when "submit order" is clicked), could be payed offline
		const STATUS_COMPLETED = 2; // Order fulfilled/finalized
		const STATUS_CANCELLED = 3; // Order cancelled by the customer (or admin)
		const STATUS_EXPIRED = 4; // Will probably have to be processed via cron or something...
	*/

	
	$os = $order['status'];
	$orderTotal = (int)$order['total'];
	if ( $os == 1  and $orderTotal == 0 ) $n_status = 2;
	elseif ( $os == 1 and $orderTotal > 0 ) $n_status = 3;
	elseif ( $os == 2 and $orderTotal == 0 ) $n_status = 2;
	elseif ( $os == 2 and $orderTotal > 0 ) $n_status = 5;
	//elseif ( $os == 3 ) $n_status = 5;
	elseif ( $os == 4 ) $n_status = 9;
	else {
		Nava::sysError (" Order $pOrderId : status not recognized: ".$order['status'], "SYS_ERR" );
		return false;
	}

	if ($n_status == 3) {
		//preba�eno u ponudu, dodaje se n_quotation_date	
		$sql_n_quotation_date = " n_quotation_date = '".date("Y-m-d")."'";
	}
	else {
		$sql_n_quotation_date = "n_quotation_date = ''";
	}
	
	$operatorId =  self::getAppSetting('frontend_default_operator_id');
	if ($n_status == 5) {		
		//pla�eno preko frontenda, poiva se funkcija koja dodjeljuje payment method
		$n_operator_id = " n_operator_id = $operatorId";
		$n_payment_date = "n_payment_date = now()";
	}
	else {
		$n_operator_id = "n_operator_id = ''";
		$n_payment_date = "n_payment_date = null";
	}
	
	//n_invoice_date_time_issue
	$sqlInvoiceDateTimeIssue = $n_status == 5 ? ', n_invoice_date_time_issue = now() ' : '';
	
	
	$n_fiscal_location_id = self::getAppSetting('fin_default_wspay_fiscal_location_id');
	$n_nav_sync_status = $n_status == 5 ? 'unsynced' : 'no_sync';	
	$n_total = $sumOrdersItems['n_total'] ? $sumOrdersItems['n_total'] : 0.00;
	$n_total_with_tax = $sumOrdersItems['n_total_with_tax'] ? $sumOrdersItems['n_total_with_tax'] : 0.00;
	$n_tax_amount = $n_total_with_tax - $n_total;
	// $n_fiscal_location_id = $fiscal_location['id'];
	
	//14.2.2017
	$n_sales_rep_id = \Baseapp\Suva\Library\libAppSetting::get('frontend_default_sales_rep_id ');
	$sqlSalesRepId = $n_sales_rep_id > 0 ? ", n_sales_rep_id = $n_sales_rep_id " : "";
	$pboPrefix = \Baseapp\Suva\Library\libAppSetting::get('fin_invoice_prefix');
	$pbo = (intval($pboPrefix) + intval($pOrderId));
	
	//11.04.2017
	$sqlPaymentMethodId = '';
	$frontendPaymentMethod = strlen($order['payment_method']) > 0 ? $order['payment_method'] : 'n/a';
	$pm = \Baseapp\Suva\Models\PaymentMethods::findFirst( "frontend_payment_method = '$frontendPaymentMethod'" );
	if ($pm){
		$sqlPaymentMethodId =  ", n_payment_methods_id = $pm->id "; 
	}

	
	
	
	$txtSql = "
		update orders set
			n_status = $n_status
			, n_total = $n_total
			, n_tax_amount = $n_tax_amount 
			, $sql_n_quotation_date
			, n_total_with_tax = $n_total_with_tax 
			, n_fiscal_location_id = $n_fiscal_location_id
			, $n_operator_id
			, $n_payment_date
			, n_frontend_sync_status = 'synced'
			, n_nav_sync_status = '$n_nav_sync_status'
			, n_pbo = '$pbo'			
			$sqlSalesRepId
			$sqlPaymentMethodId
			$sqlInvoiceDateTimeIssue
		where id = $pOrderId
	";
	//error_log($txtSql);
	// error_log("libCron: fUpdateOrderForSuva : updating order $pOrderId");
	if ($pShowMessages) Nava::sysMessage($txtSql);
	Nava::runSql($txtSql);
	
	
	//a�urirati payment method, ako je pla�eno preko onlinea
	
	
	
	
	
	
	return true;
}

public static function fProcessSerializedProduct( $pSerialized = null ){
	//TODO - sredit gre�ke ako ne na?e ne�to obavezno, u ['error'];
	/* REGEX
		'/   - poaetak stringa
		/'   - kraj stringa
		/s'  - kraj stringa, ako se hoae matchat i linebreakovi
		(aabb) - kad se nadje aabb neka ga se doda u novi red arraya
		
		
	*/
	global $gDebugLevel;
	if (strlen ($pSerialized ) < 10) return null;
	
	$r = array (
		'is_online_product' => null,
		'name' => null,
		'old_product_id' => null,
		'old_product_option' => null,
		'old_product_extras' => null,
		'old_product_extras_options' => null,
		'is_pushup' => false,
		'cost' => null,
		'error' => null
	);
	
	
	//echo PHP_EOL."TEXT:".PHP_EOL.$pSerialized.PHP_EOL.PHP_EOL.PHP_EOL;

	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)"/'; //matchat  Online ili Offline, i onda ime proizvoda 
	//matchat  Online ili Offline, i onda ime proizvoda 
 
	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)".*s:5:"...id";s:[0-9]{1,2}:"([^"]+)";/'; // 
	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)":[0-9]{1,2}:{s:5:"...id";s:[0-9]{1,2}:"([^"]+)";/'; // 
	preg_match ($rgxOnline, $pSerialized, $arrMatch);
	if( array_key_exists (1, $arrMatch) ){
		if( $arrMatch[1] == 'Online' ) $r['is_online_product'] = true;
		elseif( $arrMatch[1] == 'Offline' ) $r['is_online_product'] = false;
	}
	$r['name'] = array_key_exists( 2, $arrMatch ) ? $arrMatch[2] : null;
	$r['old_product_id'] = array_key_exists (3, $arrMatch ) ? $arrMatch[3] : null;
	//var_dump ($arrMatch);

	
	if ( $r['name'] === 'Pushup' ){
		//self::fMsg ("tu sam".PHP_EOL);
		//tra�enje online_product_id  i spremanje u old_product_option
		
		//ako je Pushup onda se tra�i s:17:"online_product_id";s:16:"istaknuti-online"; i   s:7:" * cost";s:4:"1500";
		$rgxOption = '/s:17:"online_product_id";s:[0-9]{1,2}:"([^"]+).*s:7:"...cost";s:[0-9]{1,2}:"([0-9]+)";/s';
		
		preg_match ($rgxOption, $pSerialized, $arrMatch);
		$r['is_pushup'] = true;
		$r['old_product_id'] = 'push-up';
		$r['old_product_option'] = array_key_exists( 1, $arrMatch ) ? $arrMatch[1] : null;
		$r['cost'] = array_key_exists( 2, $arrMatch ) ? ((int)$arrMatch[2])/100 : null;
		//var_dump ($arrMatch);
	}
	else{
		//za sve koji nisu Pushup
		
		//tra�enje opcije
		$rgxOption = '/selected";s:[0-9]{1,2}:"([^"]+)"/'; // matchaj ;s:11:" * selected";s:7:"60 dana";s:18:"
		preg_match ($rgxOption, $pSerialized, $arrMatch);
		$r['old_product_option'] = array_key_exists ( 1 , $arrMatch )  ? $arrMatch[1] : null;
		//var_dump ($arrMatch);
		
		//tra�enje extras 
		$rgxExtras = '/selected_extras";a:[0-9]{1,2}:.s:[0-9]{1,2}:"([^"]+)";s:[0-9]{1,2}:"([^"]+)"/';//matchati: * selected_extras";a:1:{s:20:"Objava na naslovnici";s:6:"3 dana"
		preg_match ($rgxExtras, $pSerialized, $arrMatch);
		$r['old_product_extras'] = array_key_exists( 1, $arrMatch ) ? $arrMatch[1] : null;
		$r['old_product_extras_options'] = array_key_exists( 2, $arrMatch ) ? $arrMatch[2] : null;
		//var_dump ($arrMatch);
	}
	
	
	
	$errMsg = null;
	if ( $r['is_online_product'] === null ) $errMsg .=  "is_online_product, ";
	if (! $r['name'] ) $errMsg .=  "name, ";
	if (! $r['old_product_id'] ) $errMsg .=  "old_product_id, ";
	$errMsg = $errMsg ? "Could not find ".$errMsg : $errMsg ;
	if ( $r['is_pushup']  && !( $r['old_product_option'] && $r['cost'] ) ) $errMsg .= "  Pushup product parameters not found.";
	
	if ($errMsg) {
		$r['error'] = $errMsg;
		Nava::sysError ($errMsg." for serialized product:".PHP_EOL.$pSerialized, "SYS_ERR");
	}

	
	return $r;
	
}

public static function fFindSuvaProduct ( $p , $pShowMessages = false){
	global $gDebugLevel;
	if ( $p['old_product_id'] == null ) {
		Nava::sysError("parameters supplied to self::fFindSuvaProduct do not contain old_product_id :".print_r($p, true), "SYS_ERR");
		return null;
	}
	
	$txt_old_product_id = " and old_product_id = '".$p['old_product_id']."' ";
	
	$txt_old_product_option = $p['old_product_option'] === null ? " and old_product_option is null " : " and old_product_option = '".$p['old_product_option']."' ";
	
	$txt_old_product_extras = $p['old_product_extras'] === null ? " and old_product_extras is null " : " and old_product_extras = '".$p['old_product_extras']."' ";
	
	$txt_old_product_extras_options = $p['old_product_extras_options'] === null ? " and old_product_extras_options is null " : " and old_product_extras_options = '".$p['old_product_extras_options']."' ";
	
	
	$txtWhere = " 1 = 1 $txt_old_product_id $txt_old_product_option $txt_old_product_extras $txt_old_product_extras_options ";

	$txtSql = "select * from n_products where $txtWhere ";

	$products = \Baseapp\Suva\Models\Products::find( $txtWhere );
	
	if ($pShowMessages) Nava::sysMessage("Finding suva products with these conditions: $txtWhere " );
	
	//Ako upit ne na?e to�no 1 suva proizvod, gre�ka
	if ($products->count() == 1) {
		foreach($products as $product)
			return $product;
	}
	else{
		Nava::sysError("error, query: $txtSql Found ".count( $rs )." records!!");
		return null;
	}
	
}


	public static function fUpdateOrderStatusForOnlinePayment(){
		/*
			tra�i ordere koji nisu u statusu paid a pripadaju oglasima kojima je latest payment state 5
		
		*/
		global $gDebugLevel;
		$operatorId =  self::getAppSetting('fin_default_wspay_operator_id');
		$fiscalLocationId =  self::getAppSetting('fin_default_wspay_fiscal_location_id');
		
		$orders = \Baseapp\Suva\Models\Orders::find("status = 2 and n_status in ( 3,4 ) and n_frontend_sync_status = 'synced' and n_source = 'frontend'");

		if ( !$operatorId || !$fiscalLocationId || !$orders ){
			//Nava::greska ("")
			Nava::sysError("nije dohva�en operatorId, FiscalLocationId ili Orders ");
			return false;
		}
		
			
		
		$navSyncStatus = 'unsynced';
		foreach ($orders as $order){
			$frontendPaymentMethod = strlen($order->payment_method) > 0 ? $order->payment_method : 'n/a';
			$pm = \Baseapp\Suva\Models\PaymentMethods::findFirst( "frontend_payment_method = '$frontendPaymentMethod'" );
			if (!$pm){
				Nava::sysError ("nije dohva�en frontend paymnet method na orderu $order->id");
				return false;
			}
			
				
			$txtSql = "
				update orders set
					n_status = 5
					,n_payment_methods_id = $pm->id
					,n_operator_id = $operatorId
					,n_fiscal_location_id = $fiscalLocationId
					,n_invoice_date_time_issue = now()
					,n_payment_date = now()
					,n_nav_sync_status = '$navSyncStatus'
				where id = $order->id
			";
			// error_log($txtSql);
			if ($pShowMessages)  Nava::sysMessage( $txtSql );
			Nava::runSql ( $txtSql );
			
		}
}


	public static function getFreeAdsListForUser($pUserId = null){
		$existingOrder = \Baseapp\Suva\Models\Orders::findFirst(" user_id = $pUserId and n_status = 2 ");
		if ($existingOrder) return $existingOrder;
		else{
			$order = new Orders();
 
			$order->user_id = $pUserId;
			$order->status = 1;
			$order->created_at = date('Y-m-d H:i:s');
			$order->created_at = date('Y-m-d H:i:s');
			//$cart->ip = $this->request->getClientAddress();
			$order->n_status = 2;
			
			if ($order->save() == true) return $cart;
			else {
				$err_msg = "";
				foreach ($order->getMessages() as $message) {
					$err_msg .= $message . "\n\r";
				return $err_msg;
				}
			}
		}
	}


	
	
public static function setAdsInfoForFrontend( $pAdId = null ){
	
	/*
	
	
	1. deaktivirat aktivne online order iteme
	
	2. aktivirat neaktivne online order iteme
	
	3. 
	
	
	
	*/
	
	$sqlAdId = $pAdId > 0 ? " and a.id = $pAdId " : "";
	$sqlOiAdId = $pAdId > 0 ? " and oi.ad_id = $pAdId " : "";
	

	Nava::sysMessage("BORNA Activating ads and setting dates START:". date("Y-m-d h:i:s"));
	try {
		// Nava::sysMessage("Postavljanje n_publications_id na orders_items");
		// $txtSql = "
			// update orders_items oi
				// join n_products p on ( oi.n_products_id = p.id  and p.publication_id > 0)
			// set oi.n_publications_id = p.publication_id
			// where oi.n_publications_id is null
		// ";
		// Nava::runSql ($txtSql);
		
		Nava::sysMessage("BORNA Ispravljanje greske na adovima START:". date("Y-m-d h:i:s"));	
		//ispravljanje gre�ki na adovima
		Nava::runSql("update ads set n_active_online_order_item_id = null where active= 0 and n_active_online_order_item_id > 0");
		
		$sqlJoin = "
			join orders_items oi on (
				a.n_active_online_order_item_id = oi.id
				and oi.n_expires_at < now()
				and a.active = 1
				$sqlOiAdId
				$sqlAdId
			)
		";
		
		
		//10.6.2017
		$sqlWhere = "
			where 1=1
				and a.expires_at < UNIX_TIMESTAMP(now())
				and a.active = 1
				$sqlAdId
		";
		$sqlJoin = "";
		
		$txtSql = "
			select a.id 
			from ads a
				$sqlJoin
				$sqlWhere
		";
		$rs = Nava::sqlToArray ($txtSql);
		error_log($txtSql);
		$txtIds = "Deaktiviranje isteklih oglasa: ";
		Nava::sysMessage("BORNA Deaktiviranje isteklih oglasa START:". date("Y-m-d h:i:s"));	
		if ($rs){
			foreach ($rs as $row){
				$txtIds = " ".$row['id'];
			}
		}
		Nava::sysMessage ($txtIds.PHP_EOL);
		
		$txtSql = "
			update ads a 
				$sqlJoin
				
			set 
				a.active = 0
				,a.n_active_online_order_item_id = null
			$sqlWhere

		";
		Nava::runSql( $txtSql );
		
		
		Nava::sysMessage("BORNA Deaktiviranje ad-ova kojima je moderacija negativna START:". date("Y-m-d h:i:s"));	
		//Nava::sysMessage("Deaktiviranje ad-ova kojima je moderacija negativna ");
		$txtSql = "
			select a.id from 	ads a
				join categories c on (a.category_id = c.id $sqlAdId)
					join categories_settings cs on (c.id = cs.category_id)
			where
				(
					(
						a.moderation = 'nok' 
						and
						cs.moderation_type = 'post'
					)
				or
					(
						a.moderation <> 'ok' 
						and
						cs.moderation_type = 'pre'
					)
				)
				and a.active = 1
				$sqlAdId
					";
		
		$rs = Nava::sqlToArray ($txtSql);
		error_log($txtSql);
		Nava::sysMessage("BORNA Deaktiviranje ad-ova kojima je moderacija negativna START:". date("Y-m-d h:i:s"));
		$txtIds = "Deaktiviranje ad-ova kojima je moderacija negativna: ";
		if ($rs){
			foreach ($rs as $row){
				$txtIds .= " ".$row['id'];
			}
		}
		Nava::sysMessage ($txtIds.PHP_EOL);
		
		
		$txtSql = "
			/*select * from 	ads a */
			update ads a 
				join categories c on (a.category_id = c.id)
					join categories_settings cs on (c.id = cs.category_id)
			set 
				a.active = 0 
			
			where
				(
					(
						a.moderation = 'nok' 
						and
						cs.moderation_type = 'post'
					)
				or
					(
						a.moderation <> 'ok' 
						and
						cs.moderation_type = 'pre'
					)
				)
				and a.active = 1
				$sqlAdId;
		";
		Nava::runSql( $txtSql );
		error_log($txtSql);
		
		
		

Nava::sysMessage("BORNA Dodavanjesort_date, published_at, expires_at adovima iz najkasnije objavljenog order itema koji je aktivan ". date("Y-m-d h:i:s"));

		
			$txtSql = "
			select a.id , p3.id as oi_id
			from ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
		, UNIX_TIMESTAMP(oi.n_expires_at) as expires_at
		, oi.product as oi_product
		, oi.product_id as oi_product_id
		,p.is_pushup_product 
		, p.priority_online
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						$sqlOiAdId
						)
 
					join (
						select 
							oi.ad_id
							,max(oi.n_first_published_at) as max_fpa
						from 
							orders_items oi
								join orders o on (
									oi.order_id = o.id 
									and o.n_status in (2,4,5) 
									$sqlOiAdId
									)
								join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
						where 1=1 
							and oi.n_expires_at >= NOW() 
							and oi.n_first_published_at <= now() 
							and oi.n_is_published = 1 
							and oi.n_publications_id = 1
							$sqlOiAdId
						group by
							oi.ad_id
					) p1 on ( 
						oi.ad_id = p1.ad_id 
						and oi.n_first_published_at = p1.max_fpa 
						and oi.n_expires_at >= NOW()
						and oi.n_is_published = 1
						and oi.n_publications_id = 1
						$sqlOiAdId
						)
				group by p1.ad_id
			) p2 on (oi.id = p2.oi_id)
			join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
			
	) p3 on (
		a.id = p3.ad_id 
		and (
			a.n_active_online_order_item_id <> p3.id 
			or 
			a.n_active_online_order_item_id <> p3.id is null
			)
		)
		$sqlAdId
	join 
		categories c on ( a.category_id = c.id )
			join categories_settings cs on (
				
				c.id = cs.category_id 
				$sqlAdId
				
				and 	
					(
						(
							a.moderation in ( 'ok' , 'waiting' )
							and
							cs.moderation_type = 'post'
						)
					or
						(
							a.moderation = 'ok' 
							and
							cs.moderation_type = 'pre'
						)
					)
					and a.manual_deactivation = 0
			)
					";
		$rs = Nava::sqlToArray ($txtSql);
		error_log($txtSql);
		$txtIds = "DODJELA AKTIVNOG ORDER ITEMA na oglase: ";
		if ($rs){
			foreach ($rs as $row){
				$txtIds .= " ".$row['id']."-".$row['oi_id'];
			}
		}
		Nava::sysMessage ($txtIds.PHP_EOL);
		Nava::sysMessage("BORNA DODJELA AKTIVNOG ORDER ITEMA na oglase ". date("Y-m-d h:i:s"));
	
Nava::sysMessage("korak 2: ". date("Y-m-d h:i:s"));		
$txtSql = "
update 
	ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
		, UNIX_TIMESTAMP(oi.n_expires_at) as expires_at
		, oi.product as oi_product
		, oi.product_id as oi_product_id
		,p.is_pushup_product 
		, p.priority_online
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						)
 
					join (
						select 
							oi.ad_id
							,max(oi.n_first_published_at) as max_fpa
						from 
							orders_items oi
								join orders o on (
									oi.order_id = o.id 
									and o.n_status in (2,4,5) 
									)
								join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
						where 1=1 
							and oi.n_expires_at >= NOW() 
							and oi.n_first_published_at <= now() 
							and oi.n_is_published = 1 
							and oi.n_publications_id = 1
						group by
							oi.ad_id
					) p1 on ( 
						oi.ad_id = p1.ad_id 
						and oi.n_first_published_at = p1.max_fpa 
						and oi.n_expires_at >= NOW()
						and oi.n_is_published = 1
						and oi.n_publications_id = 1
						)
				group by p1.ad_id
			) p2 on (oi.id = p2.oi_id)
			join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
			
	) p3 on (
		a.id = p3.ad_id 
		and (
			a.n_active_online_order_item_id <> p3.id 
			or 
			a.n_active_online_order_item_id <> p3.id is null
			)
		)
	join 
		categories c on ( a.category_id = c.id )
			join categories_settings cs on ( 
				c.id = cs.category_id 
				and 	
					(
						(
							a.moderation in ( 'ok' , 'waiting' )
							and
							cs.moderation_type = 'post'
						)
					or
						(
							a.moderation = 'ok' 
							and
							cs.moderation_type = 'pre'
						)
					)
					and a.manual_deactivation = 0
			)
set
     a.sort_date =  UNIX_TIMESTAMP(now())
	,a.published_at = p3.published_at
	,a.product_sort = p3.priority_online
	,a.expires_at =  p3.expires_at
	,a.online_product_id = p3.oi_product_id 
	,a.online_product = p3.oi_product
	,a.n_active_online_order_item_id = p3.id
	,a.active = 1
";
Nava::runSql( $txtSql );
error_log($txtSql);


Nava::sysMessage("Dodavanje pushup proizvoda");
Nava::sysMessage("BORNA Dodavanje pushup proizvoda: ". date("Y-m-d h:i:s"));

$txtSql = "
update 
	ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join n_products p on ( oi.n_products_id = p.id and p.is_pushup_product = 1 )
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						)
					where 1=1 
						and oi.n_first_published_at <= now() 
						and oi.n_is_published = 1 
						and oi.n_publications_id = 1
						and oi.n_pushup_applied_at is null
				group by oi.id
			) p2 on (oi.id = p2.oi_id)
	where 1=1 
		$sqlOiAdId
	) p3 on (a.id = p3.ad_id and a.n_active_online_order_item_id is not null)
set
	
     a.sort_date =  p3.published_at	
	 ,a.n_last_active_pushup_order_item_id = p3.id
";
Nava::runSql( $txtSql );
error_log($txtSql);







//postavljanje oznake na pushup proizvode da su objavljeni
Nava::sysMessage("BORNA postavljanje oznake na pushup proizvode da su objavljeni: ". date("Y-m-d h:i:s"));
$txtSql = "
update
	orders_items oi 
		join ads a on (
			oi.ad_id = a.id
			and oi.id = a.n_last_active_pushup_order_item_id
			and oi.n_pushup_applied_at is null
			$sqlOiAdId
			)
	set oi.n_pushup_applied_at = now();
";
Nava::runSql ($txtSql);
error_log($txtSql);
		
		Nava::sysMessage("BORNA Activating ads and setting dates FINISH:". date("Y-m-d h:i:s"));
		
	}
	catch(PDOException $e){
		Nava:sysError( "Setting Ads Info for Frontend failed: " . $e->getMessage().PHP_EOL );
	}
}

	public static function PredajaOglasaController_UpdateOrderStatusForOnlinePayment( \Baseapp\Models\Orders $order = null){
		/*
			tra�i ordere koji nisu u statusu paid a pripadaju oglasima kojima je latest payment state 5
		
		*/
		global $gDebugLevel;
		$operatorId =  self::getAppSetting('fin_default_wspay_operator_id');
		$fiscalLocationId =  self::getAppSetting('fin_default_wspay_fiscal_location_id');
		
		if (!$order){
			$orders = \Baseapp\Suva\Models\Orders::find("status = 2 and n_status in ( 3,4 ) and n_frontend_sync_status = 'synced' and n_source = 'frontend'");
		}
		else {
			$orders = \Baseapp\Suva\Models\Orders::find("id = $order->id");
		}
		
		if ( !$operatorId || !$fiscalLocationId || !$orders ){
			//Nava::greska ("")
			Nava::sysError("nije dohva�en operatorId, FiscalLocationId ili Orders ");
			return false;
		}
		
			
		
		$navSyncStatus = 'unsynced';
		foreach ($orders as $order){
			$frontendPaymentMethod = strlen($order->payment_method) > 0 ? $order->payment_method : 'n/a';
			$pm = \Baseapp\Suva\Models\PaymentMethods::findFirst( "frontend_payment_method = '$frontendPaymentMethod'" );
			if (!$pm){
				self::fError ("nije dohva�en frontend paymnet method na orderu $order->id");
				return false;
			}
			
				
			$txtSql = "
				update orders set
					n_status = 5
					,n_payment_methods_id = $pm->id
					,n_operator_id = $operatorId
					,n_fiscal_location_id = $fiscalLocationId
					,n_invoice_date_time_issue = now()
					,n_payment_date = now()
					,n_nav_sync_status = '$navSyncStatus'
				where id = $order->id
			";
			// error_log($txtSql);
			Nava::sysMessage( $txtSql );
			Nava::runSql ( $txtSql );
			
		}
}

	public static function getAppSetting( $pName = null, $pDefaultValue = null ){
		
		//sprema se u session jer se app settings ne mijenja cesto
		if ( array_key_exists('_app_settings', $_SESSION ))
			if ( array_key_exists ( $pName , $_SESSION['_app_settings']  ) ){
				return $_SESSION['_app_settings'][ $pName ];
		}
		
		$rs = Nava::sqlToArray("select * from n_app_settings where name = '$pName' ");
		//Nava::poruka("select * from n_app_settings where name = '$pName' ");
		if ( count( $rs ) === 1 ){
			$rec = $rs[0];
			$type = $rec['type'];
			$valDecimal = $rec['value_decimal'];
			$valText = $rec['value_text'];
			
			switch ($type){
				case 'text': 	$result = $valText;							break;
				case 'decimal':	$result = $valDecimal;						break;
				case 'integer':	$result = intval( $valDecimal);				break;
				case 'boolean': $result = ( $valDecimal == 0 ? false : true);break;
				default: 		$result = $pDefaultValue;
			}
		}
		else {
			$result = $pDefaultValue;
		}
		
		return $result;
		
		
		
	}



	
	
	// public function Ads_beforeSave( \Baseapp\Models\Ads $this ){
		
		// //IN:
		// if ( \Baseapp\Library\libSuva::getContext('n_source_path') != 'suva' ){
			// \Baseapp\Library\libSuva::Order_saveForAd ($ad, $order, $products, $extra_data);
			// return;
		// }
		
		// if ($this->id > 0){
			// $xRec = $this::findFirst($this->id);
			// $this->_x_active = $xRec->active;
			// $this->_x_moderation = $xRec->moderation;
		// }
		// else {
			// $xRec = null;
			// $this->_x_active = null;
			// $this->_x_moderation = null;
		// }

		// //ako se active prebacuje sa 1 na 0, a aktivni order item nije ve� izbrisan
		// if (
			// $xRec 
			// && $xRec->active === 1 
			// && $this->active === 0 
			// && $this->n_active_online_order_item_id !== null 
		// ){
			// $this->n_active_online_order_item_id = null;
		// }

		// //ako se active prebacuje sa 1 na 0, a aktivni order item jo� nije postavljen (kad se mijenja preko Agimovog koda)
		// if (
			// $xRec 
			// && $xRec->active === 0 
			// && $this->active === 1 
			// && strlen($this->online_product_id) > 0 
			// && $this->n_active_online_order_item_id === null 
		// ){
			// $oi = \Baseapp\Suva\Models\OrdersItems::findFirst("ad_id = $this->id and product_id = '$this->online_product_id'");
			// if ($oi){
				// $this->n_active_online_order_item_id = $oi->id;
			// }
		// }
		
		// //ako je dodan ili izmijenjen pushup proizvod, dodaje se informacija na odgovarajuci order item daje objavljen
		// if (
			// $xRec 
			// && $this->n_last_active_pushup_order_item_id > 0
			// && $xRec->n_last_active_pushup_order_item_id !== $this->n_last_active_pushup_order_item_id
		// ){
			// $oi = \Baseapp\Suva\Models\OrdersItems::findFirst( $this->n_last_active_pushup_order_item_id );
			// if ($oi){
				// \Baseapp\Suva\Library\Nava::runSql("update orders_items set n_pushup_applied_at = now() where id = $oi->id");
			// }
		// }
	// }


	
	
}