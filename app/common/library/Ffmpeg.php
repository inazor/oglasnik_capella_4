<?php

namespace Baseapp\Library;

/**
 * Simple wrapper around the ffmpeg binary. Method names are the same as
 * ffmpeg-php, so calling code can be the same. But we don't have to
 * recompile php for using this.
 */
class Ffmpeg
{

    private $_file_info = null;
    private $_output;
    private $_ffmpeg;
    private $_file;

    public function __construct($filepath, $bin_path = '/usr/bin/ffmpeg')
    {
        $this->set_file($filepath);
        $this->set_bin_path($bin_path);
        $this->_output = array();
        $this->execute();
    }

    public function set_file($filepath)
    {
        $this->_file = $filepath;
    }

    public function set_bin_path($path)
    {
        $this->_ffmpeg = $path;
    }

    public function execute()
    {
        $dev_null = '/dev/null';

        $is_windows = ('\\' === DIRECTORY_SEPARATOR);
        if ($is_windows) {
            $dev_null = 'nul';
        }

        // $cmd = $this->_ffmpeg . " -vframes 1 -i " . escapeshellarg($this->_file);
        $cmd = $this->_ffmpeg . ' -i ' . escapeshellarg($this->_file) . ' -vframes 1';
        $cmd .= ' -y -f mjpeg ' . $dev_null . ' 2>&1';
        exec($cmd, $this->_output, $ret_code);

        if ($ret_code) {
            trigger_error(__METHOD__ . ': An error occured while executing ffmpeg (' . $this->_ffmpeg . ', cmd: ' . $cmd . ', return_code: ' . $ret_code . '). Output: ' . print_r($this->_output, true), E_USER_NOTICE);
            // error_log(__METHOD__ . ': An error occured while executing ffmpeg (' . $this->_ffmpeg . ', cmd: ' . $cmd . ', return_code: ' . $returncode . '). Output: ' . print_r($this->_output, true));
            return false;
        }

        $this->parse_output();
    }

    public function parse_output()
    {
        // This was tested with ffmpeg SVN-r15625 on Windows and SVN-r14473 under Linux
        $this->_file_info = array(
            'filename'         => '', // Filename (with optional path as given from command line)
            'filetype'         => '', // Filetype (eg: mpeg, avi, mp3)
            'duration'         => '', // Total Duration (Format hh:mm:ss:i)
            'bitrate'          => '', // Global Bitrate (video + audio) in kb/s
            'audio.codec'      => '', // Audio Codec (eg: mp2, mp3, etc.)
            'audio.frequency'  => '', // Audio Frequency in Hz (eg: 44100 Hz)
            'audio.chantype'   => '', // Channel Type (eg: mono, stereo)
            'audio.bitrate'    => '', // Audio Bitrate in kb/s
            'video.codec'      => '', // Video Codec (eg: mpeg1video, mpeg2video)
            'video.resolution' => '', // Video Resolution (Width x Height)
            'video.palette'    => '', // Video color palette (eg: yuv420p)
            'video.width'      => '', // Video Frame Width
            'video.height'     => '', // Video Frame Height
            'video.framerate'  => '', // Video Framerate in fps
            'video.bitrate'    => '', // Video Bitrate in kb/s
        );

        foreach ($this->_output as $line) {
            // Looking for strings like:
            // Input #0, mpeg, from 'robot.avi':
            preg_match("/Input #0, ([\w,]+), from '(.+)'/", $line, $matches);
            if ($matches) {
                $this->_file_info['filetype'] = $matches[1];
                $this->_file_info['filename'] = $matches[2];
            }
            // Looking for strings like:
            // Duration: 00:00:00.3, start: 0.177778, bitrate: 4905 kb/s
            preg_match("/Duration: (\d{2}:\d{2}:\d{2}.\d+),.*bitrate: (\d+) kb\/s/", $line, $matches);
            if ($matches) {
                $this->_file_info['duration'] = $matches[1];
                $this->_file_info['bitrate']  = $matches[2];
            }
            // Looking for strings like:
            // Stream #0.0: Audio: mp2, 32000 Hz, mono, 32 kb/s
            // Stream #0.0: Audio: 0x6134706d, 32000 Hz, stereo
            // Stream #0.1(eng): Audio: Qclp / 0x706C6351, 11025 Hz, mono
            // preg_match("/Audio: (\w+)( \/ \w+)?(, (\d+) Hz)?(, (mono|stereo))?(, (\d+) kb\/s)?/i", $line, $matches);
            preg_match("/Audio: (\w+)( \/ \w+)?(, (\d+) Hz)?(, (mono|stereo))(.*)+?(, (\d+) kb\/s)+/i", $line, $matches);
            if ($matches) {
                if (!preg_match('/0x[a-z0-9]+/', $matches[1])) {
                    $this->_file_info['audio.codec'] = $matches[1];
                }
                $this->_file_info['audio.frequency'] = $matches[4];
                $this->_file_info['audio.chantype']  = $matches[6];
                $this->_file_info['audio.bitrate']   = $matches[9];
            }
            // Looking for strings like:
            // Stream #0.1: Video: mpeg1video, 160x112, 25.00 fps, 104857 kb/s
            // Stream #0.1: Video: wmv2, 320x240, 1000.00 fps
            // Stream #0.1: Video: msmpeg4, yuv420p, 192x144, 1000.00 fps
            // Stream #0.0(eng),  8.00 fps(r): Video: svq1, yuv410p, 192x144
            preg_match("/((\d+.\d+) fps\(\w+\): )?Video: (\w+)(, (\w+))?, (\d+)x(\d+)(, (\d+.\d+) fps)?(, (\d+) kb\/s)?/i", $line, $matches);
            if ($matches) {
                $this->_file_info['video.codec']       = $matches[3];
                $this->_file_info['video.palette']     = $matches[5];
                $this->_file_info['video.width']       = $matches[6];
                $this->_file_info['video.height']      = $matches[7];
                if (isset($matches[2])) $this->_file_info['video.framerate']  = $matches[2];
                elseif (isset($matches[9])) $this->_file_info['video.framerate'] = $matches[9];
                if (isset($matches[11])) {
                    $this->_file_info['video.bitrate']     =  $matches[11];
                }
                if ($this->_file_info['video.width'] && $this->_file_info['video.height']) {
                    $this->_file_info['video.resolution'] = $this->_file_info['video.width'] . 'x' . $this->_file_info['video.height'];
                }
            }
        }
    }

    // getters
    // TODO: make this prettier/smarter by using __get or smtn like that.
    // Also, screw this studlyCaps crap! use O_o __underscores__ o_O
    public function getFileInfo()
    {
        return $this->_file_info;
    }

    public function getFilename()
    {
        return $this->_file_info['filename'];
    }

    public function getFileType()
    {
        return $this->_file_info['filetype'];
    }

    public function getDuration()
    {
        return $this->_file_info['duration'];
    }

    public function getBitRate()
    {
        return $this->_file_info['bitrate'];
    }

    public function getAudioCodec()
    {
        return $this->_file_info['audio.codec'];
    }

    public function getAudioFrequency()
    {
        return $this->_file_info['audio.frequency'];
    }

    public function getAudioChanType()
    {
        return $this->_file_info['audio.chantype'];
    }

    public function getAudioBitRate()
    {
        return $this->_file_info['audio.bitrate'];
    }

    public function getVideoCodec()
    {
        return $this->_file_info['video.codec'];
    }

    public function getVideoResolution()
    {
        return $this->_file_info['video.resolution'];
    }

    public function getVideoPalette()
    {
        return $this->_file_info['video.palette'];
    }

    public function getFrameWidth()
    {
        return $this->_file_info['video.width'];
    }

    public function getFrameHeight()
    {
        return $this->_file_info['video.height'];
    }

    public function getFrameRate()
    {
        return $this->_file_info['video.framerate'];
    }

    public function getVideoBitRate()
    {
        return $this->_file_info['video.bitrate'];
    }

    public function getHasAudio()
    {
        return isset($this->_file_info['audio.codec']) && !empty($this->_file_info['audio.codec']);
    }

    /**
     * Extracts a single frame from a video file and saves it to the specified
     * output filename. If not specified, extracts the first frame by default.
     *
     * @param string $output_filename Filename where to store the extracted frame.
     * @param string|int $seek_pos Seek to given time position in seconds. "hh:mm:ss[.xxx]" syntax is also supported.
     * @param int $w Frame width, if not specified, ffmpeg default is used (160).
     * @param int $h Frame height, ff not specified, ffmpeg default is used (128).
     */
    public function save_screenshot($output_filename, $seek_pos = 1, $w = 160, $h = 128)
    {
        if (empty($this->_file_info)) {
            trigger_error(__FUNCTION__ . '(): ffmpeg output wasn\'t parsed!', E_USER_WARNING);
            return false;
        }

        if (!$this->_file_info['duration']) {
            trigger_error(__FUNCTION__ . '(): Unknown file duration, aborting!', E_USER_WARNING);
            return false;
        }

        $cmd = $this->_ffmpeg . ' -ss ' . $seek_pos;
        $cmd .= ' -i ' . escapeshellarg($this->_file);
        $cmd .= ' -vcodec mjpeg -vframes 1 -an -f rawvideo -y';
        $cmd .= ' -s ';
        /**
         * Decrementing because incrementing could break the site layout,
         * so it's better to have 1px smaller images IMO than to spray
         * overflow-x:hidden css all over the selectors...
         */
        // Modifying w & h if they arent a multiple of 2 (ffmpeg needs frame size to be a multiple of 2)
        if ($w & 1) $w--;
        if ($h & 1) $h--;
        $cmd .= $w . 'x' . $h . ' ' . escapeshellarg($output_filename);
        // Redirect stderr to stdout
        $cmd .= ' 2>&1';
        exec($cmd, $output, $ret);
        if (!$ret) {
            // All should be fine, since exit code was 0
            // searching for 'frame= 1'
            foreach ($output as $line) {
                if (preg_match('/^frame=\s+1/', $line)) {
                    return true;
                }
            }
        } else {
            // Something wonky, good luck!
            trigger_error(__FUNCTION__ . '(): Failed - return_code: ' . $ret . ', cmd: ' . $cmd . ', output: ' . print_r($output, true), E_USER_WARNING);
        }
    }

    public function time_to_sec($time)
    {
        $parts = explode(':', $time);
        if (!empty($parts)) {
            $hours = $parts[0];
            $minutes = $parts[1];
            $seconds = intval($parts[2]);
            return $hours * 3600 + $minutes * 60 + $seconds;
        }
    }

}
