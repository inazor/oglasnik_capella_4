<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Models\Dictionaries as Dictionary;

class DependableDropdownParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'levels';
    protected $type = 'DEPENDABLE_DROPDOWN';
    protected $accept_dictionary = true;
    protected $can_default = true;
    protected $can_default_type = 'text';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = true;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('DEPENDABLE_DROPDOWN', 'DEPENDABLE_DROPDOWN_MULTIPLE');

    public function setValue($values)
    {
        if (!is_array($values) && intval($values)) {
            $values = array(intval($values));
        }

        if (is_array($values)) {
            $json_data = array();
            foreach ($values as $level => $value) {
                $curr_level = $level + 1;
                $this->setPreparedData(array(
                    'id' => (int) $this->parameter->id,
                    'level' => $curr_level,
                    'value' => (int) $value
                ));
                $json_level_data = array(
                    'level' => $curr_level
                );
                $json_level_data['dictionary_id'] = (int) $value;
                $dictionary = Dictionary::findFirst($value);
                if ($dictionary) {
                    $json_level_data['text_value'] = $dictionary->name;
                }
                $json_data['level_' . $curr_level] = $json_level_data;
            }

            if (count($json_data)) {
                $this->setPreparedDataJson(
                    'ad_params_' . $this->parameter->id,
                    $json_data
                );
            }
        }
    }

    public function process()
    {
        $curr_level = 1;

        $json_data = array();
        // we can have a situation where user selects the value on one level, and there are no more
        // levels after it, in that case we have to remove all 'required' validations on those levels
        // ----
        // the simplest way is to put a global tracker which will remove all level validations after
        // it has its' value 'true'
        $remove_child_validations = false;
        $last_level_dictionary_id = $this->parameter->dictionary_id;

        foreach ($this->parameter_settings as $level) {
            $verify_if_needed = false;
            $verify_dictionary_children = false;
            if (isset($this->data['ad_params_' . $this->parameter->id . '_' . $curr_level]) && trim($this->data['ad_params_' . $this->parameter->id . '_' . $curr_level])) {
                $curr_param_level_value = trim($this->data['ad_params_' . $this->parameter->id . '_' . $curr_level]);
                $last_level_dictionary_id = $curr_param_level_value;

                if (is_numeric($curr_param_level_value)) {
                    $curr_param_level_value = (int) $curr_param_level_value;
                } else {
                    $curr_param_level_value = null;
                }

                if ($curr_param_level_value) {
                    if (!$remove_child_validations) {
                        $this->setPreparedData(array(
                            'id' => (int) $this->parameter->id,
                            'level' => $curr_level,
                            'value' => $curr_param_level_value
                        ));
                        $json_level_data = array(
                            'level' => $curr_level
                        );
                        $json_level_data['dictionary_id'] = $curr_param_level_value;
                        $dictionary = Dictionary::findFirst($curr_param_level_value);
                        if ($dictionary) {
                            $json_level_data['text_value'] = $dictionary->name;
                            if (!$dictionary->hasChildren()) {
                                $remove_child_validations = true;
                            }
                        }
                        $json_data['level_' . $curr_level] = $json_level_data;
                    }
                    $verify_if_needed = true;
                } else {
                    $verify_dictionary_children = true;
                }
            } else {
                // the post was not set for this level... most probably the control was disabled
                // via javascript and it means that there are no levels in dictionary but, we have to check
                $verify_dictionary_children = true;
            }

            if ($last_level_dictionary_id && $verify_dictionary_children) {
                $dictionary = Dictionary::findFirst($last_level_dictionary_id);
                if ($dictionary) {
                    if (!$dictionary->hasChildren()) {
                        $remove_child_validations = true;
                    } else {
                        $verify_if_needed = true;
                    }
                }
            }
            if ($verify_if_needed && $level->is_required) {
                $this->validation->add('ad_params_' . $this->parameter->id . '_' . $curr_level, new \Phalcon\Validation\Validator\PresenceOf(array(
                    'message' => '\'' . $level->label_text . '\' ' . $this->getValidationString('cannot_be_empty')
                )));
            }

            $curr_level++;
        }

        if (count($json_data)) {
            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id,
                $json_data
            );
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => true,
            'html' => '',
            'js'   => ''
        );

        if ($this->parameter_settings) {
            $max_parameter_level = count($this->parameter_settings);

            if ($max_parameter_level) {
                $curr_level = 1;
                $render_data['js'] = <<<JS
    promise_parameter_{$this->parameter->id} = [];

    function parameter_{$this->parameter->id}_handle_needed_levels(last_needed_level) {
        var max_parameter_level = $max_parameter_level;

        for (var i = 1; i <= max_parameter_level; i++) {
            var \$current_level_select = \$('#parameter_{$this->parameter->id}_level_' + i);
            if ('undefined' !== typeof \$current_level_select && 1 === \$current_level_select.length) {
                var \$current_level_form_group = \$current_level_select.closest('.form-group');
                if (i <= last_needed_level) {
                    \$current_level_form_group.show();
                    \$current_level_select.prop('disabled', false).selectpicker('refresh');
                } else {
                    \$current_level_form_group.hide();
                    \$current_level_select.prop('disabled', true).selectpicker('refresh');
                }
            }
        }
    }

    function parameter_{$this->parameter->id}_onChange(\$source, \$target) {
        if ('undefined' != typeof \$source.val()) {
            var last_parameter_needed_level = parseInt(\$source.attr('id').replace('parameter_{$this->parameter->id}_level_', ''));
            if ('' != \$.trim(\$source.val())) {
                var load_dictionary_values = true;

                if (load_dictionary_values) {
                    promise_parameter_{$this->parameter->id}[\$source.attr('id')] = \$.ajax({
                        url: '/ajax/dictionary/' + \$source.val() + '/values',
                        dataType: 'json',
                        success: function(data) {
                            \$target.find('option').remove();
                            if (data.length) {
                                last_parameter_needed_level++;
                                \$target.append(
                                    \$('<option/>').attr('value', '').text('')
                                );
                                \$.each(data, function(idx, data){
                                    \$target.append(
                                        \$('<option/>').attr('value', data.id).text(data.name)
                                    );
                                });
                                \$target.selectpicker('refresh');
                            }
                            parameter_{$this->parameter->id}_handle_needed_levels(last_parameter_needed_level);
                        }
                    });
                } else {
                    parameter_{$this->parameter->id}_handle_needed_levels(last_parameter_needed_level);
                }
            } else {
                \$target.selectpicker('refresh');
                parameter_{$this->parameter->id}_handle_needed_levels(last_parameter_needed_level);
            }
        } else {
            \$target.selectpicker('refresh');
        }
    }
JS;
                $prev_level = null;
                $all_dropdowns = array();

                $curr_dictionary = $this->parameter->Dictionary;

                // levels for loop begin
                foreach ($this->parameter_settings as $level) {
                    $parameter_level_id = 'parameter_' . $this->parameter->id . '_level_' . $curr_level;
                    $parameter_level_name = 'ad_params_' . $this->parameter->id . '_' . $curr_level;
                    $all_dropdowns[] = '#'.$parameter_level_id;
                    $required_html = '';
                    if ($level->is_required) {
                        $required_html = ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>';
                    }

                    $level_selected_value = isset($this->data) && isset($this->data[$parameter_level_name]) ? intval($this->data[$parameter_level_name]) : null;

                    if ($curr_dictionary) {
                        $dictionary_level_values = Dictionary::getCachedChildren($curr_dictionary->id);
                        $form_group_style = count($dictionary_level_values) ? '' : ' style="display:none;"';
                    } else {
                        $dictionary_level_values = null;
                        $form_group_style = ' style="display:none;"';
                    }

                    $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-3 col-md-3';
                    $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;
                    $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_level_name) ? ' has-error' : '') . '"' . $form_group_style . '>' . PHP_EOL;
                    $render_data['html'] .= '        <label class="control-label" for="' . $parameter_level_id . '">' . trim($level->label_text) . $required_html . '</label>' . PHP_EOL;
                    $render_data['html'] .= '        <div class="icon_dropdown">' . PHP_EOL;
                    $render_data['html'] .= '            <select class="form-control show-tick" name="' . $parameter_level_name . '" id="' . $parameter_level_id . '" data-level-id="level_' . $curr_level . '">' . PHP_EOL;

                    // for loop begin
                    if ($curr_dictionary) {
                        if ($dictionary_level_values) {
                            $render_data['html'] .= '                <option value=""></option>' . PHP_EOL;
                            foreach ($dictionary_level_values as $value) {
                                $selected_option = $level_selected_value && $level_selected_value == intval($value->id) ? ' selected="selected"' : '';
                                $render_data['html'] .= '                <option value="' . ((int) $value->id) . '"' . $selected_option . '>' . trim($value->name) . '</option>' . PHP_EOL;
                            }
                        }
                    }
                    // for loop end;

                    $render_data['html'] .= '            </select>' . PHP_EOL;
                    $render_data['html'] .= '        </div>' . PHP_EOL;

                    if (isset($this->errors) && ($this->errors->filter($parameter_level_name))) {
                        $render_data['html'] .= '        <p class="help-block">' . current($this->errors->filter($parameter_level_name))->getMessage() . '</p>' . PHP_EOL;
                    } elseif (isset($level->help_text) && trim($level->help_text)) {
                        $render_data['html'] .= '        <p class="help-block">' . trim($level->help_text) . '</p>' . PHP_EOL;
                    }

                    $render_data['html'] .= '    </div>' . PHP_EOL;
                    $render_data['html'] .= '</div>' . PHP_EOL;

                    if ($prev_level) {
                        $prev_parameter_level_id = 'parameter_' . $this->parameter->id . '_level_' . $prev_level;
                        $render_data['js'] .= <<<JS

    \$('#$prev_parameter_level_id').change(function(){
        parameter_{$this->parameter->id}_onChange(\$(this), \$('#$parameter_level_id'));
    });
JS;
                    }
                    $prev_level = $curr_level;

                    // if we have something selected in current level, then, in next iteration we will fetch currently
                    // selected level's value children
                    $curr_dictionary = null;
                    if ($level_selected_value && (string) intval($level_selected_value) === (string) $level_selected_value) {
                        $curr_dictionary = Dictionary::findFirst($level_selected_value);
                    }

                    $curr_level++;
                }
                // levels for loop end

                if (count($all_dropdowns)) {
                    $all_dropdowns = implode(', ', $all_dropdowns);

                    $style = 'btn btn-default';
                    $noneSelectedText = $this->getValidationString('select_something');
                    $noneResultsText  = $this->getValidationString('no_values_found');

                    $render_data['js'] .= <<<JS

    \$('$all_dropdowns').selectpicker({
        style: '$style',
        size: 8,
        noneSelectedText: '$noneSelectedText',
        liveSearch: true,
        noneResultsText: '$noneResultsText',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary'
    });
JS;
                }
            }
        }

        return $render_data;
    }

}
