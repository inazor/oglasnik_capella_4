<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Library\Utils;

class TitleParameter extends BaseParameter
{
    protected $group_type = 'standard';
    protected $settings_location = 'data';
    protected $type = 'TITLE';
    protected $accept_dictionary = false;
    protected $can_default = false;
    protected $can_default_type = '';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = true;
    public $is_searchable = true;
    public $is_searchable_type = array('TEXT');
    protected $default_max_length = 80;
    protected $default_help_text = 'Naslov može sadržavati najviše 80 znakova.';

    public function setValue($value)
    {
        $value = trim($value);
        $value = Utils::str_normalize_punctuation($value);

        if ($value) {
            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id,
                array(
                    'type' => 'title',
                    'text_value' => (string) $value
                )
            );

            // set the property of an Ad object
            $this->ad->title = (string) $value;
        }
    }

    public function process()
    {
        $this->getParameterSettings();
        if ($this->validation) {
            if ($this->parameter_settings->is_required) {
                $this->validation->add('ad_title', new \Phalcon\Validation\Validator\PresenceOf(array(
                    'message' => '\'' . $this->parameter_settings->label_text . '\' ' . $this->getValidationString('cannot_be_empty')
                )));
            }

            $parameter_max_length = (isset($this->parameter_settings->max_length) ? intval($this->parameter_settings->max_length) : $this->default_max_length);
            if ($parameter_max_length) {
                $this->validation->add('ad_title', new \Phalcon\Validation\Validator\StringLength(array(
                    'max'            => $parameter_max_length,
                    'messageMaximum' => '\'' . $this->parameter_settings->label_text . '\' ' . $this->getValidationString('max_length') . ' (' . $parameter_max_length . ')',
                )));
            }
        }

        if (isset($this->data['ad_title'])) {
            $this->setValue(trim(strip_tags($this->data['ad_title'])));
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => true,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_title';
            $parameter_name = 'ad_title';
            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            if (isset($this->data) && isset($this->data[$parameter_name])) {
                $parameter_default_value = trim($this->data[$parameter_name]);
            } else {
                $parameter_default_value = ($this->parameter_settings->default_value ? $this->parameter_settings->default_value : '');
            }
            $parameter_default_value = strip_tags($parameter_default_value);

            // FIXME/TODO: A shit ton of parameter values is not being properly escaped like this, which is a major
            // potential security hole.

            // Escaping for use in value attribute during display.
            // How it gets treated when being saved is a wild guess at this point :/
            $escaper = $this->getDi()->get('escaper');
            $parameter_default_value = $escaper->escapeHtmlAttr($parameter_default_value);

            $parameter_placeholder = ($this->parameter_settings->placeholder_text ? ' placeholder="' . $this->parameter_settings->placeholder_text . '"' : '');
            $parameter_max_length = (isset($this->parameter_settings->max_length) ? intval($this->parameter_settings->max_length) : $this->default_max_length);

            $parameter_help_text = isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text) ? trim($this->parameter_settings->help_text) : $this->default_help_text;

            $render_data['html'] .= '<div id="' . $parameter_name . '_box" class="col-lg-12">' . PHP_EOL;
            $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;
            $render_data['html'] .= '        <input type="text" class="form-control" name="' . $parameter_name . '" id="' . $parameter_id . '" value="' . $parameter_default_value . '"' . $parameter_placeholder . ($parameter_max_length ? ' maxlength="' . $parameter_max_length . '"' : '') . ' />' . PHP_EOL;

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '      <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (trim($parameter_help_text)) {
                $render_data['html'] .= '      <p class="help-block">' . trim($parameter_help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;
        }

        return $render_data;
    }

}
