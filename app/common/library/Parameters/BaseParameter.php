<?php

namespace Baseapp\Library\Parameters;

use Phalcon\Mvc\User\Component;
use Baseapp\Models\CategoriesFieldsetsParameters as FieldsetParameter;
use Baseapp\Models\Parameters as Parameter;
use Baseapp\Models\Dictionaries;
use Baseapp\Bootstrap;

abstract class BaseParameter extends Component
{
    protected $settings_location = '';
    protected $group_type = '';
    protected $type = '';
    protected $accept_dictionary = false;
    protected $can_default = true;
    protected $can_default_type = '';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = false;
    protected $is_hidden = false;
    public $is_searchable = false;
    public $is_searchable_type;
    protected $special_is_searchable_type = array('TITLE', 'CURRENCY', 'LOCATION', 'UPLOADABLE', 'YEAR', 'MONTHYEAR');

    public $module = 'backend';
    public $render_as_full_row = false;
    public $validation = null;
    public $fieldset_parameter;
    public $parameter_settings;
    public $parameter;
    public $data;
    public $ad;
    public $ad_parameters;
    public $errors;
    protected $validation_strings;

    protected $prepared_data = array();
    protected $prepared_data_json = array();

    public function __construct($module = null)
    {
        $this->module = $module ? $module : 'backend';

        if ('backend' === $this->module) {
            $this->validation_strings = array(
                'cannot_be_empty'              => 'cannot be empty',
                'max_length'                   => 'exceeds max number of characters',
                'custom_value_cannot_be_empty' => 'custom value cannot be empty',
                'should_contain_only_numbers'  => 'should contain only numbers',
                'should_be_a_number'           => 'should be a number',
                'is_not_valid'                 => 'is not valid',
                'is_not_a_valid_URI'           => 'is not a valid URI',
                'invalid_data'                 => 'Invalid data',
                'invalid_range'                => 'Invalid range',
                'at_least_one_field_required'  => 'At least one field should be filled',
                'required_field'               => 'Required field',
                'range_from'                   => 'From',
                'range_to'                     => 'To',
                'select_something'             => 'Select something..',
                'no_values_found'              => 'No values found',
                'choose_files'                 => 'Choose files...',
                'location_should_be_defined'   => 'should be defined',
            );
        } elseif ('frontend' === $this->module) {
            $this->validation_strings = array(
                'cannot_be_empty'              => 'polje mora biti popunjeno',
                'max_length'                   => 'polje premašuje dozvoljeni broj znakova',
                'custom_value_cannot_be_empty' => 'vrijednost mora biti popunjena',
                'should_contain_only_numbers'  => 'mora sadržavati isključivo brojke',
                'should_be_a_number'           => 'vrijednost mora biti numerička',
                'is_not_valid'                 => 'vrijednost nije ispravna',
                'is_not_a_valid_URI'           => 'nije valjani URL',
                'invalid_data'                 => 'Neispravni podaci',
                'invalid_range'                => 'Neispravan raspon',
                'at_least_one_field_required'  => 'Bar jedno polje mora biti popunjeno',
                'required_field'               => 'Obavezno polje',
                'range_from'                   => 'Od',
                'range_to'                     => 'Do',
                'select_something'             => 'Odaberite..',
                'no_values_found'              => 'Nema rezultata pretrage',
                'choose_files'                 => 'Odaberite datoteke...',
                'location_should_be_defined'   => 'mora biti definirana',
            );
        }
    }

//    abstract protected function getFilterMarkup();

    public function getFilterMarkup()
    {
        $markup = 'null';

        if ($this->is_searchable && $this->is_searchable_type) {
            $filter = new Renderer\Filter();
            $filter->setModule($this->module);
            $filter->setSettingsLocation($this->settings_location);
            $filter->setFieldsetParameter($this->fieldset_parameter);
            if ($filter->init($this->is_searchable_type)) {
                $filter->setData($this->data);
                $markup = $filter->getMarkup();
            }
        }

        return $markup;
    }

    public function getMapSearchFilterMarkup()
    {
        $markup = 'null';

        if ($this->is_searchable && $this->is_searchable_type) {
            $filter = new Renderer\MapSearchFilter();
            $filter->setModule($this->module);
            $filter->setSettingsLocation($this->settings_location);
            $filter->setFieldsetParameter($this->fieldset_parameter);
            if ($filter->init($this->is_searchable_type)) {
                $filter->setData($this->data);
                $markup = $filter->getMarkup();
            }
        }

        return $markup;
    }

    public function setValue($value)
    {
        if (trim($value)) {
            $this->setPreparedData(array(
                'id' => (int) $this->parameter->id,
                'value' => $value
            ));

            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id,
                array(
                    'value' => $value,
                    'text_value' => $value
                )
            );
        }
    }

    public function process()
    {

    }

    public function setModule($module)
    {
        $this->module = $module;
    }

    public function setValidation(&$validation)
    {
        $this->validation = $validation;
    }

    public function getValidation()
    {
        return $this->validation;
    }

    public function setFieldsetParameter(FieldsetParameter $fieldset_parameter)
    {
        $this->fieldset_parameter = $fieldset_parameter;
        $this->parameter = Parameter::getCached($this->fieldset_parameter->parameter_id);
        $this->getParameterSettings();
    }

    public function setParameterById($parameter_id)
    {
        if (intval($parameter_id)) {
            if ($parameter = Parameter::getCached($parameter_id)) {
                $this->parameter = $parameter;
            }
        }
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setAd(&$ad)
    {
        $this->ad = $ad;
    }

    public function setAdParameters(&$ad_parameters)
    {
        $this->ad_parameters = $ad_parameters;
    }

    public function setErrors(&$errors)
    {
        $this->errors = $errors;
    }

    public function getParameterSettings()
    {
        $this->parameter_settings = null;

        if (isset($this->fieldset_parameter)) {
            if ('data' == $this->settings_location && null !== $this->fieldset_parameter->data) {
                $this->parameter_settings = json_decode($this->fieldset_parameter->data);
                if ($this->parameter_settings->is_searchable) {
                    $this->is_searchable = true;
                    if (isset($this->parameter_settings->is_searchable_options) && $this->parameter_settings->is_searchable_options) {
                        $is_searchable_options = $this->parameter_settings->is_searchable_options;
                        if ($is_searchable_options && isset($is_searchable_options->type)) {
                            $this->is_searchable_type = trim($is_searchable_options->type) ? trim($is_searchable_options->type) : null;
                        }
                    }
                }
                if (isset($this->parameter_settings->is_hidden) && $this->parameter_settings->is_hidden) {
                    $this->is_hidden = true;
                }
            } elseif ('levels' == $this->settings_location && null !== $this->fieldset_parameter->levels) {
                $this->parameter_settings = json_decode($this->fieldset_parameter->levels);

                $this->is_searchable = false;
                foreach ($this->parameter_settings as $level) {
                    if ($level->is_searchable) {
                        $this->is_searchable = true;
                        if (isset($level->is_searchable_options) && isset($level->is_searchable_options->type)) {
                            $this->is_searchable_type = trim($level->is_searchable_options->type) ? trim($level->is_searchable_options->type) : null;
                            break;
                        }
                    }
                }
            }

            if ($this->is_searchable && $this->is_searchable_type && in_array($this->parameter->type_id, $this->special_is_searchable_type)) {
                $this->is_searchable_type = $this->parameter->type_id;
            }
        }

        return $this->parameter_settings;
    }

    public function getParameter()
    {
        return $this->parameter;
    }

    public function getValidationString($key)
    {
        return $this->validation_strings[$key];
    }

    public function setPreparedData($prepared_data)
    {
        $this->prepared_data[] = $prepared_data;
    }

    public function setPreparedDataJson($key, $prepared_data_json)
    {
        $this->prepared_data_json[$key] = $prepared_data_json;
    }

    public function getPreparedData()
    {
        return $this->prepared_data;
    }

    public function getPreparedDataJson()
    {
        return $this->prepared_data_json;
    }

    protected function validate_cannot_be_empty()
    {
        if ($this->validation && $this->parameter_settings->is_required) {
            $this->validation->add('ad_params_' . $this->parameter->id, new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => '\'' . $this->parameter_settings->label_text . '\' ' . $this->getValidationString('cannot_be_empty')
            )));
        }
    }

    protected function validate_greater_than_zero()
    {
        if ($this->validation && $this->parameter_settings->is_required) {
            $this->validation->add('ad_params_' . $this->parameter->id, new \Baseapp\Extension\Validator\GreaterThanZero(array(
                'messageDigit' => 'Upisana vrijednost bi se trebala sastojati isključivo od brojki i decimalnog zareza',
                'message'      => 'Upisana vrijednost nije ispravna'
            )));
        }
    }

    /**
     * Fix price helper function. Maybe we should move this one somewhere else?
     * @param  string $str
     * @return float
     */
    protected function _fix_price($str)
    {
        $str = str_replace('.', '', $str);
        $str = str_replace(' ', '', $str);
        $str = str_replace(',', '.', $str);
        if (is_numeric($str) && is_float($str)) {
            $str = (float) $str;
        }
        return $str;
    }

    protected function render_input()
    {

    }

}
