<?php

namespace Baseapp\Library\Parameters;

class SpinIdGwParameter extends SpinIdParameter
{
    protected $type = 'SPINIDGW';
    protected $param_id = 'ad_params_spinidgw';
}
