<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;

class Dropdown extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => true
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            if (isset($this->data) && isset($this->data[$parameter_name]) && trim($this->data[$parameter_name])) {
                $markup_data['email_agent'] = true;
                $parameter_value = intval($this->data[$parameter_name]);

                $model_alias = 'ap_' . $this->parameter->id;
                $markup_data['filter'] = array(
                    'model' => 'Baseapp\Models\AdsParameters',
                    'alias' => $model_alias,
                    'filter' => array(
                        'where_conditions' => '(['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id: AND ['.$model_alias.'].value = :parameter_' . $this->parameter->id . '_value:)',
                        'where_params' => array(
                            'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                            'parameter_' . $this->parameter->id . '_value' => $parameter_value
                        ),
                        'where_types' => array(
                            'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT,
                            'parameter_' . $this->parameter->id . '_value' => \PDO::PARAM_INT
                        )
                    )
                );
            } else {
                $parameter_value = null;
            }

            $filter_label = $this->parameter_settings->label_text;
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            $placeholder_text = isset($this->parameter_settings->placeholder_text) && trim($this->parameter_settings->placeholder_text) ? trim($this->parameter_settings->placeholder_text) : null;

            $markup_data['html'] .= '<h3>' . $filter_label . '</h3>' . PHP_EOL;
            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            $markup_data['html'] .= '    <select class="form-control" id="' . $parameter_id . '" name="' . $parameter_name . '" data-type="select" data-default="" data-bit-name="' . $filter_label . '"' . ($placeholder_text ? ' title="' . $placeholder_text . '"' : '') . '>' . PHP_EOL;
            $markup_data['html'] .= '        <option value=""></option>' . PHP_EOL;

            // for loop begin
            $dictionary_level_values = $this->parameter->getDictionaryValues($this->parameter->dictionary_id);
            $liveSearch = 'false';
            if ($dictionary_level_values) {
                if (count($dictionary_level_values) > 20) {
                    $liveSearch = 'true';
                }
                foreach ($dictionary_level_values as $value) {
                    $selected_option = $parameter_value && $parameter_value == intval($value->id) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '        <option value="' . ((int) $value->id) . '"' . $selected_option . '>' . trim($value->name) . '</option>' . PHP_EOL;
                }
            }
            // for loop end;

            $markup_data['html'] .= '    </select>' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;

            $style = 'btn btn-default';

            $markup_data['js'] .= <<<JS

    \$('#$parameter_id').selectpicker({
        style: '$style',
        size: 8,
        noneSelectedText: '',
        liveSearch: $liveSearch,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary',
        dropupAuto: false
    });
JS;
        }

        return $markup_data;
    }

}
