<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;
use Baseapp\Models\Dictionaries as Dictionary;

class DependableDropdown extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if ($this->parameter_settings && count($this->parameter_settings)) {
            $curr_level = 1;
            $markup_data['js'] = <<<JS

    function parameter_{$this->parameter->id}_onChange(\$source, \$target) {
        \$target.prop('selectedIndex', 0).change();
        \$target.find('option').remove();
        if ('undefined' != typeof \$source.val()) {
            if ('' != \$.trim(\$source.val())) {
                \$.get(
                    '/ajax/dictionary/' + \$source.val() + '/values',
                    function(data) {
                        if (data.length) {
                            \$target.append(
                                \$('<option/>').attr('value', '').text('')
                            );
                            \$.each(data, function(idx, data){
                                \$target.append(
                                    \$('<option/>').attr('value', data.id).text(data.name)
                                );
                            });
                        }
                        \$('#' + \$target.attr('id') + '_box').slideDown(function(){
                            \$target.prop('disabled', false);
                            \$target.selectpicker('refresh');
                        });
                    },
                    'json'
                );
            } else {
                \$('#' + \$target.attr('id') + '_box').slideUp(function(){
                    \$target.prop('disabled', true);
                    \$target.selectpicker('refresh');
                });
            }
        } else {
            \$('#' + \$target.attr('id') + '_box').slideUp(function(){
                \$target.prop('disabled', true);
                \$target.selectpicker('refresh');
            });
        }
    }
JS;

            $model_alias = 'ap_' . $this->parameter->id;
            $prev_level = null;
            $all_dropdowns = array();
            $curr_dictionary = $this->parameter->Dictionary;
            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            foreach ($this->parameter_settings as $level) {
                if (!$level->is_searchable_options) {
                    break;
                }

                $parameter_level_id   = 'parameter_' . $this->parameter->id . '_level_' . $curr_level;
                $parameter_level_name = 'ad_params_' . $this->parameter->id . '_' . $curr_level;
                $all_dropdowns[]      = '#' . $parameter_level_id;

                if (isset($this->data) && isset($this->data[$parameter_level_name]) && intval($this->data[$parameter_level_name])) {
                    $markup_data['email_agent'] = true;
                    $level_selected_value = intval($this->data[$parameter_level_name]);
                } else {
                    $level_selected_value = null;
                }

                $filter_label = $level->label_text;
                if (isset($level->is_searchable_options->label_text) && !empty($level->is_searchable_options->label_text)) {
                    $filter_label = trim($level->is_searchable_options->label_text);
                }

                $placeholder_text = isset($level->placeholder_text) && trim($level->placeholder_text) ? trim($level->placeholder_text) : null;

                $markup_data['html'] .= '    <div id="' . $parameter_level_id . '_box" class="margin-bottom-sm"' . (!$curr_dictionary ? ' style="display:none"' : '') . '>' . PHP_EOL;
//                $markup_data['html'] .= '        <label class="control-label" for="' . $parameter_level_id . '">' . $filter_label . '</label>' . PHP_EOL;
                $markup_data['html'] .= '        <select class="form-control" id="' . $parameter_level_id . '" name="' . $parameter_level_name . '" data-type="select" data-default="" data-bit-name="' . $filter_label . '"' . ($placeholder_text ? ' title="' . $placeholder_text . '"' : '') . '>' . PHP_EOL;
                // for loop begin
                if ($curr_dictionary) {
                    $dictionary_level_values = Dictionary::getCachedChildren($curr_dictionary->id);
                    if ($dictionary_level_values) {
                        $markup_data['html'] .= '            <option value=""></option>' . PHP_EOL;
                        foreach ($dictionary_level_values as $value) {
                            $selected_option = $level_selected_value && $level_selected_value == intval($value->id) ? ' selected="selected"' : '';
                            $markup_data['html'] .= '            <option value="' . ((int) $value->id) . '"' . $selected_option . '>' . trim($value->name) . '</option>' . PHP_EOL;
                        }
                    }
                }
                // for loop end;
                $markup_data['html'] .= '        </select>' . PHP_EOL;
                $markup_data['html'] .= '    </div>' . PHP_EOL;

                if ($prev_level) {
                    $prev_parameter_level_id = 'parameter_' . $this->parameter->id . '_level_' . $prev_level;
                    $markup_data['js'] .= <<<JS

    \$('#$prev_parameter_level_id').change(function(){
        parameter_{$this->parameter->id}_onChange(\$(this), \$('#$parameter_level_id'));
    });
JS;
                }

                if ($level_selected_value) {
                    $markup_data['filter'] = array(
                        'model' => 'Baseapp\Models\AdsParameters',
                        'alias' => $model_alias,
                        'filter' => array(
                            'where_conditions' => '(['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id: AND ['.$model_alias.'].level = :parameter_' . $this->parameter->id . '_level_' . $curr_level . ': AND ['.$model_alias.'].value = :parameter_' . $this->parameter->id . '_level_' . $curr_level . '_value:)',
                            'where_params' => array(
                                'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level => $curr_level,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level . '_value' => $level_selected_value
                            ),
                            'where_types' => array(
                                'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level => \PDO::PARAM_INT,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level . '_value' => \PDO::PARAM_INT
                            )
                        )
                    );
                }

                $prev_level = $curr_level;

                // if we have something selected in current level, then, in next iteration we will fetch currently
                // selected level's value children
                $curr_dictionary = null;
                if ($level_selected_value && (string) intval($level_selected_value) === (string) $level_selected_value) {
                    $curr_dictionary = Dictionary::findFirst($level_selected_value);
                }
                $curr_level++;
            }
            $markup_data['html'] .= '</div>' . PHP_EOL;

            if (count($all_dropdowns)) {
                $all_dropdowns = implode(', ', $all_dropdowns);
                $markup_data['js'] .= <<<JS

    \$('$all_dropdowns').selectpicker({
        style: 'btn btn-default',
        size: 8,
        noneSelectedText: '',
        liveSearch: true,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary',
        dropupAuto: false
    });
JS;
            }
        }

        return $markup_data;
    }

}
