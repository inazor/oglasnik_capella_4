<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;
use Baseapp\Library\RawDB;

class IntervalSlider extends Filter
{

    protected function getMaxParameterValue()
    {
        $db_config = array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $db = new RawDB($db_config);

        $max_parameter_value = 0;

        $cast_type = 'UNSIGNED';
        $min_allowed_value = 0;
        $max_allowed_value = 0;
        if (isset($this->parameter_settings->number_decimals) && intval($this->parameter_settings->number_decimals)) {
            $cast_type = 'DECIMAL(20,' . intval($this->parameter_settings->number_decimals) . ')';
        }
        if (isset($this->parameter_settings->number_min_value) && intval($this->parameter_settings->number_min_value)) {
            $min_allowed_value = intval($this->parameter_settings->number_min_value);
        }
        if (isset($this->parameter_settings->number_max_value) && intval($this->parameter_settings->number_max_value)) {
            $max_allowed_value = intval($this->parameter_settings->number_max_value);
        }

        $model = new \Baseapp\Models\AdsParameters();
        if (isset($this->fieldset_parameter->category_id)) {
            $sql = 'SELECT CAST(adp.value AS ' . $cast_type . ') AS value FROM ads_parameters adp INNER JOIN ads ad ON ad.id = adp.ad_id WHERE ad.active = 1 AND adp.parameter_id = :parameter_id AND ad.category_id = :category_id ORDER BY `value` DESC';
            $params = array(
                'parameter_id' => $this->parameter->id,
                'category_id'  => $this->fieldset_parameter->category_id
            );
        } else {
            $sql = 'SELECT CAST(adp.value AS ' . $cast_type . ') AS value FROM ads_parameters adp INNER JOIN ads ad ON ad.id = adp.ad_id WHERE ad.active = 1 AND adp.parameter_id = :parameter_id ORDER BY `value` DESC';
            $params = array(
                'parameter_id' => $this->parameter->id
            );
        }
        $result = $db->findFirst($sql, $params);
        if ($result && isset($result['value'])) {
            $max_parameter_value = $result['value'];
        }
        $max_parameter_value = intval(ceil($max_parameter_value));

        return $max_allowed_value ? min($max_allowed_value, $max_parameter_value) : $max_parameter_value;
    }

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            // slider configuration - start
            $slider_min = 0;
            $slider_max = $this->getMaxParameterValue();

            $cast_type = 'UNSIGNED';
            // TODO: fix casting as DECIMAL
            //       -> this is producing errors - for now PHQL doesn't know how to cast something to DECIMAL...
            // ---------------------------------------------------------------------------------------------------------
            //if (isset($this->parameter_settings->number_decimals) && intval($this->parameter_settings->number_decimals)) {
            //    $cast_type = 'DECIMAL(20,' . intval($this->parameter_settings->number_decimals) . ')';
            //}

            // if $slider_min and $slider_max are the same, we'll skip the rendering of this parameter!
            if ($slider_min == $slider_max) {
                $markup_data = array(
                    'html'        => '',
                    'js'          => '',
                    'filter'      => null,
                    'email_agent' => false,
                    'assets'      => array(
                        'css' => array(),
                        'js'  => array()
                    )
                );
            } else {
                $markup_data = array(
                    'html'        => '',
                    'js'          => '',
                    'filter'      => null,
                    'email_agent' => false,
                    'assets'      => array(
                        'css' => array(
                            'assets/vendor/noUiSlider-7.0.9/jquery.nouislider.min.css',
                            'assets/vendor/noUiSlider-7.0.9/jquery.nouislider.pips.min.css',
                            'assets/vendor/noUiSlider-7.0.9/nouislider-oglasnik.css',
                        ),
                        'js'  => array(
                            'assets/vendor/noUiSlider-7.0.9/jquery.nouislider.all.min.js'
                        )
                    )
                );

                $parameter_id = 'parameter_' . $this->parameter->id . '_slider';
                $parameter_id_helper = 'parameter_' . $this->parameter->id . '_helper';
                $parameter_name = 'ad_params_' . $this->parameter->id;

                $parameter_value = array(
                    'from' => null,
                    'to' => null
                );
                if (isset($this->data)) {
                    $tmp_filters = array();
                    $from = null;
                    $to = null;

                    // first we collect data to temp variables as we don't know whether $from will really be smaller number than $to
                    if (isset($this->data[$parameter_name . '_from']) && trim($this->data[$parameter_name . '_from']) && intval($this->data[$parameter_name . '_from'])) {
                        $from = intval($this->data[$parameter_name . '_from']);
                    }
                    if (isset($this->data[$parameter_name . '_to']) && trim($this->data[$parameter_name . '_to']) && intval($this->data[$parameter_name . '_to'])) {
                        $to = intval($this->data[$parameter_name . '_to']);
                    }
                    $model_alias = 'ap_' . $this->parameter->id;
                    // test if we have at least one variable set... in case we have, we will will need to add first condition
                    // (it will be parameter_id)
                    if ($from || $to) {
                        // see if we really want filtering of this data (if they are the same ad min/max values -> no need to filter)
                        if ($from && $to) {
                            $filter_from = (intval($from) !== intval($slider_min) ? intval($from) : null);
                            $filter_to   = (intval($to) !== intval($slider_max) ? intval($to) : null);
                        } else {
                            if ($from) {
                                $filter_from = (intval($from) !== intval($slider_min) ? intval($from) : null);
                                $filter_to   = null;
                            }
                            if ($to) {
                                $filter_from = null;
                                $filter_to   = (intval($to) !== intval($slider_max) ? intval($to) : null);
                            }
                        }

                        if ($filter_from || $filter_to) {
                            $markup_data['email_agent'] = true;
                            $tmp_filters[] = array(
                                'condition' => '['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id:',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT
                                )
                            );
                            if ($filter_from && $filter_to) {
                                // if both variables have been set, we have to map right values to right positions...
                                // $from <= $to
                                $parameter_value['from'] = min($filter_from, $filter_to);
                                $parameter_value['to'] = max($filter_from, $filter_to);

                                if ($filter_from == $filter_to) {
                                    $tmp_filters[] = array(
                                        'condition' => 'CAST([' . $model_alias . '].value AS ' . $cast_type . ') = :parameter_' . $this->parameter->id . '_value:',
                                        'params' => array(
                                            'parameter_' . $this->parameter->id . '_value' => $parameter_value['from'],
                                        ),
                                        'types' => array(
                                            'parameter_' . $this->parameter->id . '_value' => \PDO::PARAM_INT,
                                        )
                                    );
                                } else {
                                    $tmp_filters[] = array(
                                        'condition' => '(CAST([' . $model_alias . '].value AS ' . $cast_type . ') BETWEEN :parameter_' . $this->parameter->id . '_from_value: AND :parameter_' . $this->parameter->id . '_to_value:)',
                                        'params' => array(
                                            'parameter_' . $this->parameter->id . '_from_value' => $parameter_value['from'],
                                            'parameter_' . $this->parameter->id . '_to_value' => $parameter_value['to']
                                        ),
                                        'types' => array(
                                            'parameter_' . $this->parameter->id . '_from_value' => \PDO::PARAM_INT,
                                            'parameter_' . $this->parameter->id . '_to_value' => \PDO::PARAM_INT
                                        )
                                    );
                                }
                            } else {
                                // we're not sure which value is set, so we test both of them
                                if ($filter_from) {
                                    $parameter_value['from'] = $filter_from;
                                    $tmp_filters[] = array(
                                        'condition' => 'CAST([' . $model_alias . '].value AS ' . $cast_type . ') >= :parameter_' . $this->parameter->id . '_from_value:',
                                        'params' => array(
                                            'parameter_' . $this->parameter->id . '_from_value' => $parameter_value['from']
                                        ),
                                        'types' => array(
                                            'parameter_' . $this->parameter->id . '_from_value' => \PDO::PARAM_INT
                                        )
                                    );
                                }
                                if ($filter_to) {
                                    $parameter_value['to'] = $filter_to;
                                    $tmp_filters[] = array(
                                        'condition' => 'CAST([' . $model_alias . '].value AS ' . $cast_type . ') <= :parameter_' . $this->parameter->id . '_to_value:',
                                        'params' => array(
                                            'parameter_' . $this->parameter->id . '_to_value' => $parameter_value['to']
                                        ),
                                        'types' => array(
                                            'parameter_' . $this->parameter->id . '_to_value' => \PDO::PARAM_INT
                                        )
                                    );
                                }
                            }
                        }
                    }

                    if ($combined_filter = $this->render_filter_combine($tmp_filters)) {
                        $markup_data['filter'] = array(
                            'model' => 'Baseapp\Models\AdsParameters',
                            'alias' => $model_alias,
                            'filter' => $combined_filter
                        );
                    }
                }

                //var_dump($markup_data['filter']);exit;

                $suffix = $this->parameter_settings->suffix ? ' (' . $this->parameter_settings->suffix . ')' : '';

                $slider_start = $parameter_value['from'] ? $parameter_value['from'] : $slider_min;
                $slider_end = $parameter_value['to'] ? $parameter_value['to'] : $slider_max;


                $slider_step = floor(($slider_end - $slider_start) / 25);

                $slider_hidden_from = $parameter_id . '_from';
                $slider_hidden_to = $parameter_id . '_to';
                // slider configuration - start

                $filter_label = $this->parameter_settings->label_text;
                if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                    $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
                }


                $markup_data['html'] .= '<h3>' . $filter_label . $suffix . '</h3>' . PHP_EOL;

                $markup_data['html'] .= '<div class="responsive-group">' . PHP_EOL;
                $markup_data['html'] .= '    <div class="noUi-slider-box" data-defaults="[' . $slider_min . ',' . $slider_max . ']">' . PHP_EOL;
                $markup_data['html'] .= '        <input type="hidden" id="' . $slider_hidden_from . '" name="' . $parameter_name . '_from" value="' . $slider_start . '" data-type="slider" data-default="' . $slider_min . '" data-bit-name="' . $filter_label . $suffix . ' od" />' . PHP_EOL;
                $markup_data['html'] .= '        <input type="hidden" id="' . $slider_hidden_to . '" name="' . $parameter_name . '_to" value="' . $slider_end . '" data-type="slider" data-default="' . $slider_max . '" data-bit-name="' . $filter_label . $suffix . ' do" />' . PHP_EOL;
                $markup_data['html'] .= '        <div id="' . $parameter_id . '"></div>' . PHP_EOL;
                $markup_data['html'] .= '        <div class="range-helper" id="' . $parameter_id_helper . '">[' . $slider_start . ' - ' . $slider_end . ']</div>' . PHP_EOL;
                $markup_data['html'] .= '    </div>' . PHP_EOL;
                $markup_data['html'] .= '</div>' . PHP_EOL;

                $noUiSliderOptions = $parameter_id . '_options';
                $noUiSliderObject = $parameter_id . '_noUiSlider';

                $markup_data['js'] = <<<JS

    var $noUiSliderOptions = {
        start: [$slider_start, $slider_end],
        step: $slider_step,
        connect: true,
        range: {
            'min': $slider_min,
            'max': $slider_max
        },
        format: wNumb({
            decimals:0
        })
    };
    var \$$noUiSliderObject = \$('#$parameter_id').noUiSlider($noUiSliderOptions);
    \$("#$parameter_id").Link('lower').to(function(value){
        \$('#$slider_hidden_from').val(value);
        \$('#$parameter_id_helper').html('[' + \$('#$slider_hidden_from').val() + ' - ' + \$('#$slider_hidden_to').val() + ']');
    });
    \$("#$parameter_id").Link('upper').to(function(value){
        \$('#$slider_hidden_to').val(value);
        \$('#$parameter_id_helper').html('[' + \$('#$slider_hidden_from').val() + ' - ' + \$('#$slider_hidden_to').val() + ']');
    });
    \$('#$parameter_id').noUiSlider_pips({
        mode: 'range',
        density: 10
    });

JS;
            }
        }

        return $markup_data;
    }

}
