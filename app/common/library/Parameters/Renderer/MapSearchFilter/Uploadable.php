<?php

namespace Baseapp\Library\Parameters\Renderer\MapSearchFilter;

use Baseapp\Library\Parameters\Renderer\MapSearchFilter;

class Uploadable extends MapSearchFilter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false,
            'full_row'    => false,
            'name'        => 'ad_params_uploadable'
        );

        if (!$this->is_hidden) {
            $parameter_id   = 'parameter_uploadable';
            $parameter_name = 'ad_params_uploadable';

            $filter_label = isset($this->parameter_settings->label_text) ? $this->parameter_settings->label_text : 'Samo oglasi sa slikom';
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            // check for parameters default value in db (categories fieldset parameters)
            // but, by default, show all ads (don't filter them automatically by not having pictures!)
            $checkbox_selected  = false;
            if (isset($this->parameter_settings->is_searchable_options->default_value)) {
                $checkbox_selected = $this->parameter_settings->is_searchable_options->default_value;
            }

            if (isset($this->data)) {
                if (isset($this->data[$parameter_name])) {
                    $parameter_value = $this->data[$parameter_name];
                    $checkbox_selected = intval($parameter_value) ? true : false;
                }
            }
            if ($checkbox_selected) {
                $markup_data['email_agent'] = true;
                $model_alias = 'ap_' . (isset($this->parameter) && $this->parameter ? $this->parameter->id : 'uploadable');
                $markup_data['filter'] = array(
                    'model'  => 'Baseapp\Models\AdsMedia',
                    'alias'  => $model_alias,
                    'filter' => null,
                    'bits'   => array(
                        array(
                            'id'    => $parameter_id,
                            'name'  => 'Samo oglasi sa slikom',
                            'value' => null
                        )
                    )
                );
            }

            /*
            if (!$this->get_only_sql_filter_data) {
                $markup_data['html'] .= '<div class="form-group checkbox checkbox-primary">' . PHP_EOL;
                $markup_data['html'] .= '    <input type="checkbox" id="' . $parameter_id . '"' . ($checkbox_selected ? ' checked="checked"' : '') . ' name="' . $parameter_name . '" value="1" data-type="checkbox" data-default="false" data-bit-name="Samo oglasi sa slikom">' . PHP_EOL;
                $markup_data['html'] .= '    <label for="' . $parameter_id . '" title="' . $filter_label . '">' . trim($this->parameter_settings->is_searchable_options->label_text) . '</label>' . PHP_EOL;
                $markup_data['html'] .= '</div>';
            }
            */
        }

        return $markup_data;
    }

}
