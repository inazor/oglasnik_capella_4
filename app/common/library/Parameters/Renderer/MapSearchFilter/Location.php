<?php

namespace Baseapp\Library\Parameters\Renderer\MapSearchFilter;

use Baseapp\Library\Parameters\Renderer\MapSearchFilter;
use Baseapp\Library\GPSHandler;
use Baseapp\Models\Locations;

class Location extends MapSearchFilter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false,
            'full_row'    => true,
            'name'        => 'ad_location'
        );

        $parameter_category_id = $this->fieldset_parameter->category_id;
        $db_level_names        = array('country_id', 'county_id', 'city_id', 'municipality_id');

        if ($this->parameter_settings) {
            $tmp_filters = array();

            if (count($this->parameter_settings)) {
                $curr_level = 1;

                $markup_data['js'] = <<<JS

    function parameter_location_{$parameter_category_id}_onChange(\$source, \$target) {
        \$target.prop('selectedIndex', 0).trigger('change');
        \$target.find('option').remove();
        if ('undefined' != typeof \$source.val()) {
            if ('' != \$.trim(\$source.val())) {
                \$.get(
                    '/ajax/location/' + \$.trim(\$source.val()) + '/values',
                    function(data) {
                        if (data.length) {
                            \$target.append(
                                \$('<option/>').attr('value', '').text('')
                            );
                            \$.each(data, function(idx, itemData){
                                var \$targetOption = \$('<option/>').attr('value', itemData.id).text(itemData.name);
                                if (
                                    'undefined' !== typeof itemData.south && 
                                    'undefined' !== typeof itemData.west && 
                                    'undefined' !== typeof itemData.north && 
                                    'undefined' !== typeof itemData.east
                                ) {
                                    var gpsBounds = itemData.south + ',' + itemData.west + ',' + itemData.north + ',' + itemData.east;
                                    \$targetOption.attr('data-gps-bounds', gpsBounds).data('gps-bounds', gpsBounds);
                                }
                                \$target.append(\$targetOption);
                            });
                            \$target.prop('disabled', false);
                            \$target.selectpicker('refresh');
                            \$('#' + \$target.attr('id') + '_box').show();
                        } else {
                            \$target.prop('disabled', true);
                            \$target.selectpicker('refresh');
                            \$('#' + \$target.attr('id') + '_box').hide();
                        }
                    },
                    'json'
                );
            } else {
                \$target.prop('disabled', true);
                \$target.selectpicker('refresh');
                \$('#' + \$target.attr('id') + '_box').hide();
            }
        } else {
            \$target.prop('disabled', true);
            \$target.selectpicker('refresh');
            \$('#' + \$target.attr('id') + '_box').hide();
        }
    }
JS;

                $prev_level = null;
                $all_dropdowns = array();
                $curr_location = new Locations();
                foreach ($this->parameter_settings as $level) {
                    $parameter_level_id = 'parameter_location_' . $this->fieldset_parameter->category_id . '_level_' . $curr_level;
                    $parameter_level_name = 'ad_location_' . $this->fieldset_parameter->category_id . '_' . $curr_level;

                    if (isset($this->data) && isset($this->data[$parameter_level_name]) && intval($this->data[$parameter_level_name])) {
                        $markup_data['email_agent'] = true;
                        $level_selected_value = intval($this->data[$parameter_level_name]);
                    } else {
                        $level_selected_value = null;
                    }

                    if (1 === $level->is_hidden) {
                        if (null === $level_selected_value) {
                            $level_selected_value = $level->default_value ? intval($level->default_value) : null;
                        }

                        //$markup_data['html'] .= '<input type="hidden" name="' . $parameter_level_name . '" id="' . $parameter_level_id . '" value="' . ($level_selected_value ? $level_selected_value : '') . '" />' . PHP_EOL;
                    } elseif ($level->is_searchable) {

                        $filter_label = $level->label_text;
                        if (isset($level->is_searchable_options->label_text) && !empty($level->is_searchable_options->label_text)) {
                            $filter_label = trim($level->is_searchable_options->label_text);
                        }

                        $all_dropdowns[] = '#'.$parameter_level_id;
                        $required_html = '';

                        $markup_data['html'] .= '    <div class="col-md-3 col-sm-6">' . PHP_EOL;
                        $markup_data['html'] .= '        <div class="form-group">' . PHP_EOL;
                        $markup_data['html'] .= '            <div id="' . $parameter_level_id . '_box" class="margin-bottom-sm"' . (!$curr_location ? ' style="display:none"' : '') . '>' . PHP_EOL;
                        $markup_data['html'] .= '                <label for="' . $parameter_level_id . '">' . $filter_label . '</label>' . PHP_EOL;
                        $markup_data['html'] .= '                <div class="icon_dropdown">' . PHP_EOL;
                        $markup_data['html'] .= '                    <select class="form-control" id="' . $parameter_level_id . '" name="' . $parameter_level_name . '" data-type="select" data-default="" data-bit-name="' . $filter_label . '" data-gps-location="true">' . PHP_EOL;

                        // for loop begin
                        if ($curr_location) {
                            $location_level_values = Locations::getCachedChildren($curr_location->id);
                            if ($location_level_values) {
                                $markup_data['html'] .= '                        <option value=""></option>' . PHP_EOL;
                                foreach ($location_level_values as $value) {
                                    $gpsHandler = new GPSHandler();
                                    $gpsHandler->setBounds($value->south, $value->west, $value->north, $value->east);
                                    $boundsString = $gpsHandler->getGoogleMapsBoundsString();

                                    $selected_option = $level_selected_value && $level_selected_value == intval($value->id) ? ' selected="selected"' : '';
                                    $markup_data['html'] .= '                        <option value="' . ((int) $value->id) . '"' . $selected_option . ($boundsString ? ' data-gps-bounds="' . $boundsString . '"' : '') . '>' . trim($value->name) . '</option>' . PHP_EOL;
                                }
                            }
                        }
                        // for loop end;

                        $markup_data['html'] .= '                    </select>' . PHP_EOL;
                        $markup_data['html'] .= '                </div>' . PHP_EOL;
                        $markup_data['html'] .= '            </div>' . PHP_EOL;
                        $markup_data['html'] .= '        </div>' . PHP_EOL;
                        $markup_data['html'] .= '    </div>' . PHP_EOL;

                        if ($prev_level) {
                            $prev_parameter_level_id = 'parameter_location_' . $parameter_category_id . '_level_' . $prev_level;
                            $markup_data['js'] .= <<<JS

    \$('#$prev_parameter_level_id').change(function(){
        parameter_location_{$parameter_category_id}_onChange(\$(this), \$('#$parameter_level_id'));
    });
JS;
                        }
                    }

                    if ($level_selected_value && !$level->is_hidden) {
                        $paramName = $db_level_names[($curr_level - 1)] . '_' . $parameter_category_id;
                        $tmp_filters[] = array(
                            'condition' => 'ad.' . $db_level_names[($curr_level - 1)] . ' = :' . $paramName .  ':',
                            'params' => array(
                                $paramName => intval($level_selected_value)
                            ),
                            'types' => array(
                                $paramName => \PDO::PARAM_INT
                            )
                        );
                    }

                    $prev_level = $curr_level;

                    // if we have something selected in current level, then, in next iteration we will fetch currently
                    // selected level's value children
                    $curr_location = null;
                    if ($level_selected_value && (string) intval($level_selected_value) === (string) $level_selected_value) {
                        $curr_location = Locations::findFirst($level_selected_value);
                    }

                    $curr_level++;
                }

                if (count($all_dropdowns)) {
                    $all_dropdowns = implode(', ', $all_dropdowns);
                    $markup_data['js'] .= <<<JS

    \$('$all_dropdowns').selectpicker({
        style: 'btn btn-default',
        noneSelectedText: '',
        liveSearch: true,
        noneResultsText: '',
        mobile: is_mobile_browser(),
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary',
        dropupAuto: false
    });
JS;
                }

            }
            $markup_data['filter'] = $this->render_filter_combine($tmp_filters);
        }

        return $markup_data;
    }

}
