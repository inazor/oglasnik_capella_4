<?php

namespace Baseapp\Library\Parameters\Renderer;

class View extends BaseRenderer
{
    protected $renderer;
    public $caret = '<small><span class="fa fa-angle-right fa-fw"></span></small>';

    public $type;
    public $parameter;
    public $parameter_settings;
    public $dictionary_id;
    public $data;
    public $fieldset_style;
    public $errors;

    public function __construct($parameter = null)
    {
        $this->type = null;
        if ($parameter) {
            $this->parameter = $parameter;
            if (isset($parameter['parameter_type_id']) && trim($parameter['parameter_type_id'])) {
                $this->type = trim($parameter['parameter_type_id']);
            }
            if (isset($parameter['parameter_dictionary_id']) && intval($parameter['parameter_dictionary_id'])) {
                $this->dictionary_id = intval($parameter['parameter_dictionary_id']);
            }
            $this->get_parameter_settings();
        }
    }

    public function setData($data = null)
    {
        $this->data = $data;
    }

    public function init($fieldset_style)
    {
        $this->fieldset_style = !empty($fieldset_style) ? $fieldset_style : 'group';
        switch ($this->type) {
            case 'LOCATION':
                $this->renderer = new View\LocationParameter();
                break;
            case 'DROPDOWN':
            case 'DEPENDABLE_DROPDOWN':
            case 'CHECKBOXGROUP':
            case 'RADIO':
                $this->renderer = new View\DictionaryParameter();
                break;
            case 'CHECKBOX':
            case 'TEXT':
            case 'TEXTAREA':
            case 'DATE':
            case 'DATEINTERVAL':
            case 'NUMBER':
            case 'YEAR':
            case 'MONTHYEAR':
                $this->renderer = new View\ClassicParameter();
                break;
            // url parameters
            case 'URL':
                $this->renderer = new View\URLParameter();
                break;
            case 'URL_BUTTON':
                $this->renderer = new View\URLButtonParameter();
                break;
            case 'SPINID':
                $this->renderer = new View\SpinIdParameter();
                break;
            case 'SPINIDGW':
                $this->renderer = new View\SpinIdGwParameter();
                break;
            default:
                $this->renderer = null;
                break;
        }

        if ($this->renderer) {
            $this->renderer->caret = $this->caret;
            $this->renderer->fieldset_style = $this->fieldset_style;
            $this->renderer->type = $this->type;
            $this->renderer->parameter = $this->parameter;
            $this->renderer->parameter_settings = $this->parameter_settings;
        }

        return $this->renderer ? true : false;
    }

    private function get_parameter_settings()
    {
        $this->parameter_settings = null;

        if (isset($this->parameter)) {
            if (isset($this->parameter['data']) && $this->parameter['data']) {
                // parameter settings in 'data' field
                $this->parameter_settings = json_decode($this->parameter['data']);
            } elseif (isset($this->parameter['levels']) && $this->parameter['levels']) {
                // parameter settings in 'levels' field
                $this->parameter_settings = json_decode($this->parameter['levels']);
            }
        }

        return $this->parameter_settings;
    }

    public function render()
    {
        if ($this->renderer) {
            $this->renderer->data = $this->data;
            return $this->renderer->render();
        }

        return null;
    }

}
