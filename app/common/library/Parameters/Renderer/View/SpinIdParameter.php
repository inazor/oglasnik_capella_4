<?php

namespace Baseapp\Library\Parameters\Renderer\View;

use Baseapp\Library\Parameters\Renderer\View;

class SpinIdParameter extends View
{
    /**
     * @return array|null
     */
    public function render()
    {
        $ret  = null;

        $parameter_name = 'ad_params_spinid';
        $parameter_option = isset($this->data->$parameter_name) ? $this->data->$parameter_name : null;

        if (!$this->parameter_settings->is_hidden && $parameter_option) {
            $ids = array();

            $value = trim($parameter_option->text_value);

            // Multiple 360 ids on the same ad can be comma separated
            if (false !== strpos($value, ',')) {
                $ids = preg_split('@(?:\s*,\s*|^\s*|\s*$)@', $value, null, PREG_SPLIT_NO_EMPTY);
            } else {
                // Treating single value as array too
                $ids[] = $value;
            }

            $url_prefix = 'http://www.oglasnik.hr';
            $ret = array(
                '360' => array(),
                'assets' => array(
                    'js' => array(
                        'assets/vendor/three.min.js',
                        'assets/vendor/photo-sphere-viewer.min.js'
                    )
                )
            );

            foreach ($ids as $id) {
                $ret['360'][] = array(
                    'spinid' => $id,
                    // TODO/FIXME: `.JPG` always. yeah :(
                    'spinid_url' => $url_prefix . '/media/360/' . $id . '.JPG',
                    // TODO/FIXME: if/when we move static assets to cdn or similar, this will need updating probably
                    'spinid_thumb_url' => '/assets/img/360-square-blue.svg'
                );
            }
        }

        return $ret;
    }
}
