<?php

namespace Baseapp\Library\Parameters\Renderer\View;

use Baseapp\Library\Parameters\Renderer\View;

class URLButtonParameter extends View
{
    /**
     * @return array|null
     */
    public function render()
    {
        $ret  = null;

        $parameter_name = 'ad_params_' . $this->parameter->parameter_id;
        $parameter_option = isset($this->data->$parameter_name) ? $this->data->$parameter_name : null;

        if (!$this->parameter_settings->is_hidden && $parameter_option) {
            $ret['external_url_button'] = array(
                'label' => $parameter_option->label,
                'url'   => $parameter_option->url
            );
        }

        return $ret;
    }
}
