<?php

namespace Baseapp\Library\Parameters\Renderer;

use \Phalcon\Mvc\Model\Query\Builder as QueryBuilder;

class FieldsetRenderer extends BaseRenderer
{
    protected $fieldset;
    public $data;
    private $fieldset_style;

    public function __construct($fieldset = null)
    {
        $this->fieldset = $fieldset;
    }

    protected function get_fieldset_parameters()
    {
        $builder = new QueryBuilder();
        $builder->columns(array(
            'p.id AS parameter_id',
            'p.name AS parameter_name',
            'p.type_id AS parameter_type_id',
            'p.dictionary_id AS parameter_dictionary_id',
            'cfp.data AS data',
            'cfp.levels AS levels'
        ));
        $builder->addFrom('Baseapp\Models\CategoriesFieldsetsParameters', 'cfp');
        $builder->innerJoin('Baseapp\Models\Parameters', 'cfp.parameter_id = p.id', 'p');
        $builder->where(
            'cfp.category_fieldset_id = :fieldset_id:',
            array(
                'fieldset_id' => $this->fieldset->id
            ),
            array(
                'fieldset_id' => \PDO::PARAM_INT
            )
        );
        $builder->orderBy('cfp.sort_order ASC');

        return $builder->getQuery()->execute()->setHydrateMode(\Phalcon\Mvc\Model\Resultset::HYDRATE_RECORDS);
    }

    public function setData($data = null)
    {
        $this->data = $data;
    }

    public function render()
    {
        $output = array(
            'html'                => '',
            'map'                 => null,
            '360'                 => null,
            'external_url_button' => null,
            'assets'              => array(
                'css' => array(),
                'js'  => array()
            )
        );

        $parameters = $this->get_fieldset_parameters();
        if ($parameters && count($parameters)) {
            $this->fieldset_style = null;
            if ($this->fieldset->settings && $fieldset_settings = json_decode($this->fieldset->settings)) {
                $this->fieldset_style = isset($fieldset_settings->style) ? trim($fieldset_settings->style) : null;
            }

            $parameters_html = '';

            foreach ($parameters as $parameter) {
                $parameter_view_renderer = new View($parameter);
                if ($parameter_view_renderer->init($this->fieldset_style)) {
                    $parameter_view_renderer->setData($this->data);
                    $render_output = $parameter_view_renderer->render();

                    if (isset($render_output['html']) && trim($render_output['html'])) {
                        $parameters_html .= trim($render_output['html']);
                    }
                    if (isset($render_output['map'])) {
                        $output['map'] = $render_output['map'];
                    }
                    if (isset($render_output['external_url_button'])) {
                        $output['external_url_button'] = $render_output['external_url_button'];
                    }
                    if (isset($render_output['360'])) {
                        if (!is_array($output['360'])) {
                            $output['360'] = array();
                        }
                        $output['360'] = array_merge($output['360'], $render_output['360']);
                    }
                    if (isset($render_output['assets']['css']) && count($render_output['assets']['css'])) {
                        $output['assets']['css'] = array_merge($output['assets']['css'], $render_output['assets']['css']);
                    }
                    if (isset($render_output['assets']['js']) && count($render_output['assets']['js'])) {
                        $output['assets']['js'] = array_merge($output['assets']['js'], $render_output['assets']['js']);
                    }
                }
            }

            if ($parameters_html) {
                $output['html'] .= '<div class="row ad-details">' . PHP_EOL;
                $output['html'] .= '    <div class="col-sm-2">' . PHP_EOL;
                $output['html'] .= '        <strong class="convert-to-tab-xs">' . ($this->fieldset->name ? $this->fieldset->name : '') . '<span class="caret hidden-sm hidden-md hidden-lg"></strong>' . PHP_EOL;
                $output['html'] .= '    </div>' . PHP_EOL;
                $output['html'] .= '    <div class="col-sm-10">' . PHP_EOL;
                $output['html'] .= '        <div class="row">' . PHP_EOL;
                $output['html'] .= $parameters_html;
                $output['html'] .= '        </div>' . PHP_EOL;
                $output['html'] .= '    </div>' . PHP_EOL;
                $output['html'] .= '</div>' . PHP_EOL;
            }
        }

        return $output;
    }

}
