<?php

namespace Baseapp\Library\Parameters\Renderer;

use Baseapp\Models\CategoriesFieldsetsParameters as FieldsetParameter;
use Baseapp\Models\Parameters as Parameter;
use Phalcon\Mvc\User\Component;

abstract class BaseRenderer extends Component
{
    public $settings_location;
    public $type;
    public $module;
    public $fieldset_parameter;
    public $parameter_settings;
    public $parameter;
    public $data;
    public $is_hidden = false;
    public $errors;


    public function setModule($module)
    {
        $this->module = $module;
    }

    public function setSettingsLocation($settings_location)
    {
        $this->settings_location = $settings_location;
    }

    public function setFieldsetParameter(FieldsetParameter $fieldset_parameter)
    {
        $this->fieldset_parameter = $fieldset_parameter;
        $this->parameter = Parameter::getCached($this->fieldset_parameter->parameter_id);
        $this->getParameterSettings();
    }

    public function setParameterById($parameter_id)
    {
        if (intval($parameter_id)) {
            if ($parameter = Parameter::getCached($parameter_id)) {
                $this->parameter = $parameter;
            }
        }
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setErrors(&$errors)
    {
        $this->errors = $errors;
    }

    public function getParameterSettings()
    {
        $this->parameter_settings = null;

        if (isset($this->fieldset_parameter)) {
            if (!isset($settings_location)) {
                if (isset($this->fieldset_parameter->data) && null !== $this->fieldset_parameter->data) {
                    $this->settings_location = 'data';
                } elseif (isset($this->fieldset_parameter->levels) && null !== $this->fieldset_parameter->levels) {
                    $this->settings_location = 'levels';
                }
            }

            if ('data' == $this->settings_location && null !== $this->fieldset_parameter->data) {
                $this->parameter_settings = json_decode($this->fieldset_parameter->data);
                if (isset($this->parameter_settings->is_hidden) && $this->parameter_settings->is_hidden) {
                    $this->is_hidden = true;
                }
            } elseif ('levels' == $this->settings_location && null !== $this->fieldset_parameter->levels) {
                $this->parameter_settings = json_decode($this->fieldset_parameter->levels);
            }
        }

        return $this->parameter_settings;
    }


}
