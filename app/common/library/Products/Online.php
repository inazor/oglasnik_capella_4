<?php

namespace Baseapp\Library\Products;

use Baseapp\Library\Utils;

abstract class Online extends Base
{
    protected $type = 'online';

    // Only some online products have the push_up option and the price as the value (in lp, as a string usually)
    protected $push_up = false;

    protected $sort_idx = 100;

    public function getConfigForm()
    {
        $product_id = $this->getId();

        $disabled_checkbox_checked = $this->isDisabled() ? ' checked="checked"' : '';
        $panel_classnames = 'panel product ';
        if (!empty($disabled_checkbox_checked)) {
            $panel_classnames .= 'panel-danger';
        } else {
            $panel_classnames .= 'panel-info';
        }

        $markup = '';
        $markup .= '<div class="' . $panel_classnames . '" data-product-id="' . $product_id . '">';

        $markup .= '<div class="panel-heading">';
        $markup .= '<div class="pull-right"><div class="checkbox no-margin"><label>';
        $markup .= '<input type="checkbox" class="product-disabler-checkbox" name="' . $product_id . '[disabled]"';
        $markup .= $disabled_checkbox_checked . ' value="1"> Disabled</label>';
        $markup .= '</div></div>';
        $markup .= '<h3 class="panel-title">' . $this->getTitle() . '</h3>';
        $markup .= '</div>';

        $markup .= '<div class="panel-body">';
        $markup .= '<div class="row">';
        // Now preparing a more complicated version - for each possible option and/or extras,
        // show their corresponding inputs which will allow overriding prices, titles and maybe
        // some other stuff

        $options = $this->getOptions();
        foreach ($options as $option_key => $option_value) {
            $option_id = $product_id . '-' . $option_key;
            $option_value_display = Utils::lp2kn($option_value);
            // $option_repeat = $this->str_first_word($option_key);
            $markup .= '<div class="col-md-3 col-lg-3">';
            $markup .= $this->getPriceInput($option_id, $option_key, $product_id . '[options][' . $option_key . ']', $option_value_display);
            $markup .= '</div>';
        }

        $markup .= '</div>'; // close row

        $extras = $this->getExtras();
        $has_push_up = $this->hasPushUp();

        // Open new row if needed (everything in it is in the left col)
        if ($has_push_up || $extras) {
            $markup .= '<h4>Extras</h4>';
            $markup .= '<div class="row">';
            $markup .= '<div class="col-md-6 col-lg-6">';
        }

        // Display push_up config input if needed
        if ($has_push_up) {
            $option_id = $product_id . '-push_up';
            $option_value_display = Utils::lp2kn($this->push_up);
            $markup .= $this->getPriceInput($option_id, 'Push up', $product_id . '[push_up]', $option_value_display);
        }

        // Display extras config inputs if needed
        if ($extras) {
            //$markup .= '<div class="col-md-6 col-lg-6">';
            // Extras are slightly different in the sense that $extra_value can be
            // either a string or an array. If it's a string, it's a single price
            // for that option, and if it's an array, it's multiple options each
            // with it's own name/title and price
            $extras_cnt = 0;
            foreach ($extras as $extra_key => $extra_value) {
                $extras_cnt++;
                if (is_array($extra_value)) {
                    // Create a list group of option
                    $markup .= '<label>' . $extra_key . '</label>';
                    $markup .= '<div class="row">';
                    $nested_cnt = 0;
                    foreach ($extra_value as $extra_name => $extra_price) {
                        $nested_cnt++;
                        $input_id = $product_id . '-extras-' . $extras_cnt . '-' . $nested_cnt;
                        $value_display = Utils::lp2kn($extra_price);
                        $input_name = $product_id . '[extras][' . $extra_key . '][' . $extra_name . ']';
                        $markup .= '<div class="col-md-6 col-lg-6">';
                        // $markup .= '<h5 class="list-group-item-heading">' . $extra_name . '</h5>';
                        $markup .= $this->getPriceInput($input_id, $extra_name, $input_name, $value_display);
                        $markup .= '</div>';
                    }
                    $markup .= '</div>';
                } else {
                    $input_id = $product_id . '-extras-' . $extras_cnt;
                    $input_name = $product_id . '[extras][' . $extra_key . ']';
                    $value_display = Utils::lp2kn($extra_value);
                    $markup .= $this->getPriceInput($input_id, $extra_key, $input_name, $value_display);
                }
            }
            //$markup .= '</div>'; // close col
        }

        if ($has_push_up || $extras) {
            $markup .= '</div></div>'; // close second row and col (extras or push_up stuff)
        }

        $markup .= '</div></div>'; // close .panel-body and .panel

        return $markup;
    }

    private function getPriceInput($id, $label, $name, $value)
    {
        $markup = '';
        $markup .= '<div class="form-group">';
        $markup .= '<label class="control-label" for="' . $id . '">' . $label . '</label>';
        $markup .= '<div class="input-group">';
        $markup .= '<input class="form-control text-right price" name="' . $name . '" type="text" ';
        $markup .= 'id="' . $id . '" placeholder="0" value="' . $value . '">';
        $markup .= '<span class="input-group-addon">Kn</span>';
        $markup .= '</div></div>';

        return $markup;
    }

    public function getSortIndex()
    {
        return $this->sort_idx;
    }

    public function getCode()
    {
        return Utils::slugify($this->getTitle());
    }

    // Online products don't really have any kind of special billing info
    public function getBillingIdentifier()
    {
        $ident = parent::getBillingIdentifier();

        // Collect any selected extras and append those too
        $selected_extras = $this->getSelectedExtras();
        if (null !== $selected_extras) {
            foreach ($selected_extras as $name => $selected_extra) {
                $ident .= ' + ' . $name . ':' . $selected_extra;
            }
        }

        return $ident;
    }

    /**
     * Overriding getSelectedOption() for Online products to support a slightly different way
     * of setting the default choice in various product selectors.
     * It was requested that the default choice for free online products/options be changed
     * to the 90 days one ('90 dana'), which is usually the last key in the options list.
     * This way the free ads would perhaps "last longer" by default.
     *
     * TODO/FIXME: The logic to check/select the default is now slightly duplicated across these two implementation methods.
     *
     * @return null
     */
    public function getSelectedOption()
    {
        // Preventing having nothing selected since all the products that actually have
        // options need to have one selected (at least for now). If/when that changes,
        // one can override specific product's getSelectedOption() to change this.
        if (null === $this->selected) {
            $options = $this->getOptions();
            // If the product has options, set the first one as selected if nothing
            // was selected yet, but only if the last option doesn't have 0 cost
            if (!empty($options)) {
                // Get first and last option keys
                reset($options);
                $first_option_key = key($options);
                end($options);
                $last_option_key = key($options);

                // Check if last option is free (has 0 cost), and if so set that as the selected option,
                // otherwise set the first option as selected (the same way that's done in Products\Base)
                $last_option_cost = $options[$last_option_key];
                if (0 === $last_option_cost || '0' === $last_option_cost) {
                    $this->setSelectedOption($last_option_key);
                } else {
                    $this->setSelectedOption($first_option_key);
                }
            }
        }

        return $this->selected;
    }
}
