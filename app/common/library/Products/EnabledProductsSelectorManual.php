<?php

namespace Baseapp\Library\Products;

use Baseapp\Models\Categories;
use Baseapp\Models\Users;

/**
 * Class EnabledProductsSelectorManual is a specialization of the EnabledProductsSelector supporting
 * "pre-selected" (and pre-configured) product instances. They're specified via the 4-th constructor
 * parameter or via `setPreselected()`.
 * "Manual" because it doesn't call `$this->process()` automatically inside the constructor.
 *
 * @package Baseapp\Library\Products
 */
class EnabledProductsSelectorManual extends EnabledProductsSelector
{
    protected $preselected = array(
        // lowercase key names
        'online'  => null,
        'offline' => null
    );

    public function __construct(Categories $category = null, Users $user = null, array &$post_data = null, array $preselected = null)
    {
        $this->category  = $category;
        $this->user      = $user;
        $this->post_data = $post_data;

        $this->setPreselected($preselected);

        return $this;
    }

    /**
     * @param array $preselected Array/hash with two keys, `online` and `offline`. Each one should be
     *                           holding a ProductsInterface instance (or be null/empty)
     *
     * @return $this
     */
    public function setPreselected(array $preselected = null)
    {
        if (null !== $preselected) {
            $this->preselected = $preselected;
        }

        return $this;
    }

    /**
     * Overridden implementation supporting our slightly different use case
     */
    public function process()
    {
        foreach ($this->getPossibleList() as $group => $classes) {
            $group_lowercase = strtolower($group);
            foreach ($classes as $class_name) {
                // Create a default instance
                $product = $this->createProduct($group, $class_name);
                $fqcn    = $product->getFqcn();

                // Check if we have an already preset/configured instance that matches
                // this one and use that instead of a default one but still allow for
                // $this->post_data to override it properly
                if ($this->isEmptyPostData() && isset($this->preselected[$group_lowercase])) {
                    if (!empty($this->preselected[$group_lowercase])) {
                        $preselected      = $this->preselected[$group_lowercase];
                        $preselected_fqcn = $preselected->getFqcn();
                        // Replace the default instance with a preselected/pre-configured one
                        if ($fqcn === $preselected_fqcn) {
                            $product = $preselected;
                            // Make sure the preselected product gets fresh prices
                            $product->configureByCategory($this->category);
                        }
                    }
                }

                // Handle posted data
                if (!empty($this->post_data)) {
                    $product->selectViaPost($this->post_data);
                }

                // Add to internal list
                $this->addProduct($product, $group_lowercase);

                // Add to internal chosen list if chosen
                if ($product->isChosen()) {
                    $this->addChosenProduct($product, $group_lowercase);
                }
            }
        }
    }
}
