<?php

namespace Baseapp\Library\Products\Online;

class IstaknutiNoExtras extends IstaknutiOnline
{
    protected $id = 'istaknuti-online-bez';

    // All NoExtras are disabled by default and are only enabled in certain categories.
    protected $disabled = true;

    // Disable extras
    protected $extras = false;

    // Disable upgrading
    // protected $upgradable = false;
}
