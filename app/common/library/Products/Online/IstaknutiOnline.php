<?php

namespace Baseapp\Library\Products\Online;

use Baseapp\Library\Products\Online;

class IstaknutiOnline extends Online
{
    use NoHomepageFeaturingGetExtrasTrait;

    protected $id = 'istaknuti-online';
    protected $title = 'Istaknuti oglas';
    protected $description = <<<HTML
<ul>
<li>Uvijek se bolje istaknuti – plava boja će istaknuti vaš oglas od ostalih, Osnovnih oglasa</li>
<li>Najnoviji oglasi su na vrhu; poredani po datumu objave</li>
<li>Ne zaboravite na opciju „Skok na vrh“ kako bi uvijek bili ispred konkurencije</li>
</ul>
HTML;

    protected $options = array(
        '5 dana'  => '1900',
        '15 dana' => '3700',
        '30 dana' => '6300'
    );

    protected $extras = array(
        'Objava na naslovnici' => array(
            '3 dana' => '25000',
            '7 dana' => '35000'
        )
    );

    protected $push_up = '1500';
}
