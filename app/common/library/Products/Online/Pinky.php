<?php

namespace Baseapp\Library\Products\Online;

use Baseapp\Library\Products\Online;

/**
 * Pinky should not be orderable for common users, and acts differently than other products.
 * Contact Oglasnik for exact details.
 */
class Pinky extends Online
{
    protected $id = 'pinky';
    protected $title = 'Pinky oglas';
    protected $description = 'Opis pinky oglasa.';

    // Pinky is disabled by default and is only enabled in certain categories.
    // Even so, it's only available from the backend, regular users shouldn't be
    // able to purchase it.
    protected $disabled = true;
    protected $orderable = false;
    protected $upgradable = false;

    protected $options = array(
        '7 dana' => '50000'
    );
    protected $extras = false;
    protected $push_up = false;
    protected $sort_idx = 5;
}
