<?php

namespace Baseapp\Library\Products\Offline;

use Baseapp\Library\Products\Offline;

class IstaknutiOffline extends Offline
{
    protected $id = 'istaknuti-offline';
    protected $code = 'IstaPT';
    protected $title = 'Istaknuti oglas';
    protected $description = '<p>Želite se istaknuti. Nudimo jednostavi istaknuti oglas s okvirom. Za redovito pojavljivanje uzmite paket objava i ostvarite popust.</p>';

    protected $avus_adformat_single = 'Istaknuti singleT';
    protected $avus_adlayout_single = 'Istaknuti_Tis';
    protected $avus_adformat_multi  = 'Istaknuti pack T';
    protected $avus_adlayout_multi  = 'Istaknuti_Tis';

    protected $options = array(
        '1 objava' => '5500',
        '2 objave' => '10700',
        '4 objave' => '18900',
        '8 objava' => '37800'
    );

    protected $extras = false;
}
