{# Admin Media Edit/Create View #}
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ form_title }}</h1>
        {{ flashSession.output() }}

{% set original_src = entity.get_url() %}
{# spitting out a modal for each image style defined #}
{% if (not(previews is empty)) %}
{% for cnt, thumb in previews %}
    {%- set style = thumb.getStyle() -%}
        <div class="modal" id="cropper-modal-{{ cnt }}" tabindex="-1" role="dialog" aria-labelledby="modal-label-{{ cnt }}" aria-hidden="true" data-media-id="{{ entity.id }}" data-slug="{{ style['slug'] }}">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="modal-label-{{ cnt }}">Edit "{{ style['slug'] }}" style <small>({{ style['width'] }} &times; {{ style['height'] }} {% if style['crop'] %}<i title="Hard-crop mode (resulting image will always have these dimensions)" class="fa fa-crop"></i> {% else %}max{% endif %})</small></h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-cropper-container">
                            <div class="modal-cropper">
                                <img src="{{ original_src }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span class="ajax-status"></span>
                        <button type="button" class="btn btn-primary cropper-save-close"><i class="spinner hidden fa fa-fw fa-cog fa-spin"></i>Save &amp; close</button>
                    </div>
                </div>
            </div>
        </div>
{% endfor %}
{% endif %}

        {{ form(NULL, 'id' : 'media-edit', 'method' : 'post', 'enctype' : 'multipart/form-data', 'autocomplete' : 'off') }}
        {{ hiddenField('next') }}
        {{ hiddenField('_csrftoken') }}

        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Meta</h3>
                    </div>
                    <div class="panel-body">
                        {%- set field = 'title' -%}
                        <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                            <label class="control-label" for="{{ field }}">Title</label>
                            <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ entity.title|default('') }}" maxlength="255">
                            {%- if errors is defined and errors.filter(field) -%}
                                <p class="help-block">{{- current(errors.filter(field)).getMessage() -}}</p>
                            {%- endif -%}
                        </div>
                        {%- set field = 'author' -%}
                        <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                            <label class="control-label" for="{{ field }}">Author</label>
                            <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ entity.author|default('') }}" maxlength="255">
                            {%- if errors is defined and errors.filter(field) -%}
                                <p class="help-block">{{- current(errors.filter(field)).getMessage() -}}</p>
                            {%- endif -%}
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="description">Description</label>
                            <textarea rows="4" class="form-control" name="description" id="description">{{ entity.description|default('') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="copyright">Copyright</label>
                            <textarea rows="4" class="form-control" name="copyright" id="copyright">{{ entity.copyright|default('') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="keywords">Keywords</label>
                            <textarea rows="4" class="form-control" name="keywords" id="keywords">{{ entity.keywords|default('') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">File</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
{% if not entity.id %}
                                {%- if errors is defined -%}
                                    {%- set upload_errors = errors.filter('upload') -%}
                                {%- else -%}
                                    {% set upload_errors = [] %}
                                {%- endif -%}
                                <div class="form-group{{ not(upload_errors is empty) ? ' has-error' : '' }}">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-info btn-file"><span class="fileinput-new"><span class="glyphicon glyphicon-upload"></span> Select file</span><span class="fileinput-exists"><span class="glyphicon glyphicon-edit"></span> Change file</span><input type="file" name="upload" id="upload"></span>
                                        <span class="fileinput-filename-wrap">
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"><span class="glyphicon glyphicon-remove"></span></a>
                                        </span>
                                    </div>
                                    {%- for err in upload_errors -%}
                                    <p class="help-block">{{ err.getMessage() }}</p>
                                    {%- endfor -%}
                                </div>
{% endif %}
{% if entity_details is defined %}
    {% set preview = entity.get_backend_thumb().toArray() %}
                                <div data-id="{{ entity.id }}" class="attachment">
                                    <div class="attachment-preview type-{{ preview['type'] }} subtype-{{ preview['subtype'] }} {{ preview['orientation'] }}">
                                        <div class="thumb">
                                            <div class="centered">
                                                {{- preview['tag'] -}}
                                            </div>
                                            {%- if 'image' !== preview['type'] -%}
                                                <div class="filename">
                                                    <div>{{- entity.filename_orig|escape -}}</div>
                                                </div>
                                            {%- endif -%}
                                        </div>
                                    </div>
                                </div>
                                <table class="table-condensed table-striped table-bordered media-details">
                                    <tr>
                                        <th colspan="3" class="text-center text-muted">{{ entity.filename_orig }}</th>
                                    </tr>
                                    <tr>
                                        <td>Size</td>
                                        <td colspan="2">
                                            <span class="glyphicon glyphicon-paperclip"></span> {{ entity_details['filesize'] }}
                                            {%- if entity_details['width'] is defined and entity_details['height'] is defined -%}
                                            ({{ entity_details['width'] }} <strong>&times;</strong> {{ entity_details['height'] }})
                                            {%- endif -%}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td colspan="2">
                                            <span class="glyphicon {{ media_types[entity.type]['glyphicon'] }}"></span>
                                            {{- media_types[entity.type]['slug'] -}}
                                            <span class="text-muted">({{ entity.mimetype }})</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>URI</td>
                                        <td colspan="2">
                                            <a href="{{ original_src }}" target="_blank" title="{{ entity.filename|escape_attr }}"><span class="glyphicon glyphicon-link"></span>{{ entity.filename|truncate_middle(14) }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Path</td>
                                        <td colspan="2">
                                            <span class="filepath" title="{{ entity_details['filepath']|escape_attr }}"><span class="glyphicon glyphicon-file"></span> {{ entity_details['filepath']|truncate_middle(14) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Created</td>
                                        <td><span class="glyphicon glyphicon-time"></span> {{ entity.created_at }}</td>
                                        <td class="text-nowrap">
                                            {%- if not user_created is null -%}
                                                {{ linkTo(['admin/users/edit/' ~ user_created.id, '<span class="glyphicon glyphicon-user"></span>' ~ user_created.username, 'class': 'text-nowrap']) }}
                                            {%- else -%}
                                                <span class="glyphicon glyphicon-user"></span> anonymous
                                            {%- endif -%}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Modified</td>
                                        <td><span class="glyphicon glyphicon-time"></span> {{ entity.modified_at }}</td>
                                        <td class="text-nowrap">
                                            {%- if not user_modified is null -%}
                                                {{ linkTo(['admin/users/edit/' ~ user_modified.id, '<span class="glyphicon glyphicon-user"></span>' ~ user_modified.username, 'class': 'text-nowrap']) }}
                                            {%- else -%}
                                                <span class="glyphicon glyphicon-user"></span> anonymous
                                            {%- endif -%}
                                        </td>
                                    </tr>
                                </table>
{% endif %}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

{% if (not(previews is empty)) %}
        <div class="row media-grid">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Generated styles</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
{%- set cachebust = Library_Utils__getRequestTime() -%}
{%- for cnt, thumb in previews -%}
    {%- set style = thumb.getStyle() -%}
    {%- set data = thumb.getData(['type', 'subtype', 'orientation']) -%}
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <div id="preview-{{ style['slug'] }}" class="attachment">
                                    <div class="attachment-preview type-{{ data['type'] }} subtype-{{ data['subtype'] }} {{ data['orientation'] }}">
                                        <a class="crop-modal-link" data-target="#cropper-modal-{{ cnt }}" data-toggle="modal">
                                            <div class="thumb">
                                                <div class="centered">
                                                    {{- thumb.getSrcOnlyTag(cachebust) -}}
                                                </div>
                                                <div class="filename style-details">
                                                    <div>{{ style['slug'] }} ({{ style['width'] }} &times; {{ style['height'] }} {% if style['crop'] %}<i class="fa fa-crop"></i>{% else %}max{% endif %})</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
{%- endfor -%}
                        </div>
                    </div>
                </div>
            </div>
        </div>
{% endif %}

{%- if (not(linked_content is empty)) -%}
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Linked content</h3>
                    </div>
                    <div class="panel-body">
                    {%- for group, objects in linked_content -%}
                        <h4>{{ group }}</h4>
                        <div class="list-group">
                        {%- for object in objects -%}
                            <a href="{{ object['edit_link'] }}" class="list-group-item list-group-item-info"><span class="glyphicon glyphicon-file"></span>{{ object['ident'] }}</a>
                        {%- endfor -%}
                        </div>
                    {%- endfor -%}
                    </div>
                </div>
            </div>
        </div>
{%- endif -%}

        <div class="row entity-actions">
            <div class="col-lg-12">
                {%- if entity_details is defined -%}
                <a href="{{ this.url.get('admin/media/delete/' ~ entity.id ~ '?next=admin/media') }}" class="btn btn-danger pull-right">Delete</a>
                {%- endif -%}
                <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                <button class="btn btn-primary" type="submit" name="save">Save</button>
                <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
            </div>
        </div>
        {{ endForm() }}
    </div>
</div>
