{# Ad moderation OK email #}
<p>Poštovani,</p>
<p>
    Vaš oglas <b>{{ ad.title }}</b> je od {{ date('d.m.Y H:i:s', ad.published_at) }} aktivan na internetskoj
    stranici <a href="{{ url.get('') }}">{{ url.get('') }}</a>.
</p>
{% if ad_products['online'] is defined %}
{% set online_product = ad_products['online'] %}
<p>
    Oglas je predan kao "<b>{{ online_product.getTitle() }}</b>", a trajanje oglasa je do {{ date('d.m.Y H:i:s', ad.expires_at) }}.
</p>
{% endif %}
{% if ad_products['offline'] is defined %}
<p>
    Vaš će oglas "<b>{{ ad.title }}</b>" biti objavljen u idućem tiskanom izdanju Oglasnika.
</p>
{% endif %}
<p>
    Oglas je prošao postupak autorizacije kojime se osigurava kvaliteta oglasa
    i sigurnost korisnicima tiskanog i internetskog izdanja. Sadržaj
    autoriziranog oglasa možete pogledati na:
    <a href="{{ ad.get_frontend_view_link() }}">{{ ad.get_frontend_view_link() }}</a>.
</p>
{% if moderation_reason %}
<p><u>Moderator koji je pogledao vaš oglas želi vam skrenuti pažnju na sljedeće:</u></p>
<p>{{ moderation_reason }}</p>
{% endif %}
<p>
    Za daljnje upravljanje vašim oglasima, u Vašem kutku:
    <a href="{{ url.get('moj-kutak') }}">{{ url.get('moj-kutak') }}</a> možete
    produžiti trajanje ili ih nakon uspješne prodaje obrisati (deaktivirati).
</p>
<p>
    U slučaju bilo kakvih pitanja naša korisnička podrška dostupna vam je na
    01/6102-885 ili <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.
</p>
