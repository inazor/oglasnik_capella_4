<div class="post-box row" data-classified-id="{{ ad['id'] }}">
    <div class="col-lg-8 col-md-8">
        <div class="img-wrapper">{{ ad['thumb_pic'] }}</div>
        <h3 data-href="{{ ad['frontend_url'] }}">{{ ad['title']|escape }}</h3>
        {% set desc = ad['description_tpl']|default(ad['description'])|striptags|truncate(150)|nl2br -%}
        <p>{{ desc }}</p>
    </div>
    <div class="col-lg-4 col-md-4">
        <strong>Predan:</strong> {{ ad['created_at'] }}<br />
        <strong>Objavljen:</strong> {{ ad['published_at'] }}<br />
        <strong>Kategorija:</strong> {{ ad['category_path'] }}<br />
    </div>
</div>
<div class="post-box-actions clear-both text-right">
    <button class="btn btn-primary btn-sm add-to-shopping-window hidden"><span class="fa fa-fw fa-plus"></span> Add</button>
    <button class="btn btn-danger btn-sm remove-from-shopping-window hidden"><span class="fa fa-fw fa-times"></span> Remove</button>
    <span class="no-image-warning text-small text-danger hidden">* Only ads with photos can be added</span>
</div>
