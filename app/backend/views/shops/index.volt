{# Admin Shops Listing/View #}
{% set current_time = time() %}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                {{ linkTo(['admin/shops/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            <h1 class="page-header">Shops</h1>
        </div>
        {{ flashSession.output() }}

        <form class="form-inline" role="form">
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {%- for f in field_options -%}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {%- endfor -%}
                                <li class="divider"></li>
                                <li><a href="#all">All fields</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6 text-right">
                    <select id="order-by" name="order_by" class="form-control" title="Order by">
                        {%- for value, title in order_by_options -%}
                            <option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>
        <table class="table table-striped table-hover table-condensed table-double-rows">
            <tr>
                <th>Shop name <small class="text-muted">[user]</small></th>
                <th width="150" class="text-left">Status</th>
            </tr>
            {% for shop in page.items %}
            {% set status_class = '' %}
            {% set status_text = '' %}
            {% if shop['banned'] %}
                {% set status_class = 'danger' %}
                {% set status_text = 'Banned user' %}
            {% elseif shop['active'] == 0 %}
                {% set status_class = 'danger' %}
                {% set status_text = 'Inactive user' %}
            {% elseif shop['expires_at'] == null %}
                {% set status_class = 'info' %}
                {% set status_text = 'New shop' %}
            {% elseif shop['expires_at'] >= current_time %}
                {% set status_class = '' %}
                {% set status_text = 'Active shop' %}
            {% else %}
                {% set status_class = 'warning' %}
                {% set status_text = 'Expired shop' %}
            {% endif %}
            <tr class="primary-info{{ status_class ? ' ' ~ status_class : '' }}{{ shop['infraction_reports_count'] ? ' danger' : '' }}">
                <td>
                    {{ shop['info'] }}
                    {% if shop['infraction_reports_count'] %}
                    <small class="text-danger"><span class="fa fa-exclamation-triangle fa-fw"></span> Unresolved infraction reports: {{ shop['infraction_reports_count'] }}</small>
                    {% endif %}
                </td>
                <td class="text-left">{{ status_text }}</td>
            </tr>
            <tr class="details{{ status_class ? ' ' ~ status_class : '' }}">
                <td colspan="2">
                    <div class="table-responsive">
                        <table class="table table-condensed table-striped table-dates">
                            <tr>
                                <td width="25%">Created<br>{{ shop['created_at'] ? date('Y-m-d H:i:s', shop['created_at']) : '-' }}</td>
                                <td width="25%">Modified<br>{{ shop['modified_at'] ? date('Y-m-d H:i:s', shop['modified_at']) : '-' }}</td>
                                <td width="25%">Published<br>{{ shop['published_at'] ? date('Y-m-d H:i:s', shop['published_at']) : '-' }}</td>
                                <td width="25%">Expires<br>{{ shop['expires_at'] ? date('Y-m-d H:i:s', shop['expires_at']) : '-' }}</td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr class="spacer">
                <td colspan="2">&nbsp;</td>
            </tr>
            {% else %}
            <tr><td colspan="3">No shops found</td></tr>
            {% endfor %}
        </table>
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
