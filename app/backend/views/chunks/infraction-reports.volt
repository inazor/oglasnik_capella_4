{% if auth.logged_in(['admin','supersupport','support','moderator','sales']) %}

    {% set hasInfractionReports = false %}
    {% if infraction_reports is defined and infraction_reports is iterable and infraction_reports|length > 0 %}
        {% set hasInfractionReports = true %}
        {% if infraction_reports['all'] is defined and infraction_reports['all']|length == 0 %}
            {% set hasInfractionReports = false %}
        {% endif %}
    {% endif %}

    {% if hasInfractionReports %}
    <div id="infraction-reports" class="panel panel-danger">
        <div class="panel-heading">
            {% set reportedCount = infraction_reports|length %}
            {% if infraction_reports['all'] is defined %}{% set reportedCount = infraction_reports['all']|length %}{% endif %}
            <h3 class="panel-title">Reported {{ reportedCount ~ ' time' ~ (reportedCount > 1 ? 's' : '') }}</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-condensed table-hover master-detail">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User</th>
                                <th>Reason</th>
                                <th>Reported at <small class="text-muted">(IP address)</small></th>
                                <th class="text-center">Resolved at</th>
                            </tr>
                        </thead>
                        {% if infraction_reports['all'] is defined %}
                        {# display grouped infraction reports by resolved status #}
                        {% if infraction_reports['unresolved']|length %}
                        <tbody class="unresolved" data-visibility="visible">
                            <tr><th colspan="5"><span class="text-danger">Unresolved</span> <span class="text-muted">infraction reports</span> <span class="badge">{{ infraction_reports['unresolved']|length }}</span></th></tr>
                            {% for report in infraction_reports['unresolved'] %}
                            <tr id="report-row-{{ report['id'] }}" class="report-row master-row danger unresolved" data-id="{{ report['id'] }}">
                                <td>{{ loop.index }}.</td>
                                <td>{{ linkTo(['admin/users/edit/' ~ report['reporter_id'], report['reporter_username'], 'target': '_blank']) }}</td>
                                <td>{{ report['report_reason'] }}{{ (report['report_message'] ? ' + <span class="fa fa-file-text-o fa-fw" title="Additional message"></span>' : '') }}</td>
                                <td>{{ date('d.m.Y H:i:s', strtotime(report['reported_at'])) }} <small class="text-muted">({{ report['reported_ip'] }})</small></td>
                                <td class="text-center"><span class="text-danger">Unresolved</span></td>
                            </tr>
                            {% if report['report_message'] %}
                            <tr data-master="report-row-{{ report['id'] }}" class="detail-row">
                                <td colspan="4">
                                    <p><code>{{ report['report_message'] }}</code></p>
                                </td>
                            </tr>
                            {% endif %}
                            {% endfor %}
                        </tbody>
                        {% endif %}
                        {% if infraction_reports['resolved']|length %}
                        <tbody class="resolved" data-visibility="hidden">
                            <tr><th colspan="5"><span class="text-success">Resolved</span> <span class="text-muted">infraction reports</span> <span class="badge">{{ infraction_reports['resolved']|length }}</span></th></tr>
                            {% for report in infraction_reports['resolved'] %}
                            <tr id="report-row-{{ report['id'] }}" class="report-row master-row success resolved" data-id="{{ report['id'] }}" style="display:none">
                                <td>{{ loop.index }}.</td>
                                <td>{{ linkTo(['admin/users/edit/' ~ report['reporter_id'], report['reporter_username'], 'target': '_blank']) }}</td>
                                <td>{{ report['report_reason'] }}{{ (report['report_message'] ? ' + <span class="fa fa-file-text-o fa-fw" title="Additional message"></span>' : '') }}</td>
                                <td>{{ date('d.m.Y H:i:s', strtotime(report['reported_at'])) }} <small class="text-muted">({{ report['reported_ip'] }})</small></td>
                                <td class="text-center"><span class="text-success">{{ date('d.m.Y H:i:s', strtotime(report['resolved_at']))}}</span></td>
                            </tr>
                            {% if report['report_message'] %}
                            <tr data-master="report-row-{{ report['id'] }}" class="detail-row">
                                <td colspan="4">
                                    <p><code>{{ report['report_message'] }}</code></p>
                                </td>
                            </tr>
                            {% endif %}
                            {% endfor %}
                        </tbody>
                        {% endif %}
                        {% else %}
                        {# display non-grouped infraction reports #}
                        <tbody class="all">
                            {% for report in infraction_reports %}
                            <tr id="report-row-{{ report['id'] }}" class="report-row master-row {{ report['resolved_at'] ? 'success resolved' : 'danger unresolved'}}" data-id="{{ report['id'] }}">
                                <td>{{ loop.index }}.</td>
                                <td>{{ linkTo(['admin/users/edit/' ~ report['reporter_id'], report['reporter_username'], 'target': '_blank']) }}</td>
                                <td>{{ report['report_reason'] }}{{ (report['report_message'] ? ' + <span class="fa fa-file-text-o fa-fw" title="Additional message"></span>' : '') }}</td>
                                <td>{{ date('d.m.Y H:i:s', strtotime(report['reported_at'])) }} <small class="text-muted">({{ report['reported_ip'] }})</small></td>
                                <td class="text-center">{{ report['resolved_at'] ? '<span class="text-success">' ~ date('d.m.Y H:i:s', strtotime(report['resolved_at'])) ~ '</span>' : '<span class="text-danger">Unresolved</span>' }}</td>
                            </tr>
                            {% if report['report_message'] %}
                            <tr data-master="report-row-{{ report['id'] }}" class="detail-row">
                                <td colspan="4">
                                    <p><code>{{ report['report_message'] }}</code></p>
                                </td>
                            </tr>
                            {% endif %}
                            {% endfor %}
                        </tbody>
                        {% endif %}
                    </table>
                </div>
            </div>
        </div>
    </div>
    {% endif %}
{% endif %}
