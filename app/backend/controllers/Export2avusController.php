<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Models\Categories;
use Baseapp\Models\CategoriesFieldsets;
use Baseapp\Models\CategoriesFieldsetsParameters as FieldsetParameter;
use Baseapp\Models\CategoriesSettings as CategorySettings;
use Baseapp\Models\Currency;
use Baseapp\Models\Locations;
use Baseapp\Models\Parameters;
use Baseapp\Library\Parameters\MixedParameter;
use Baseapp\Traits\ParametrizatorHelpers;

/**
 * Backend Export2avus Controller
 */
class Export2avusController extends IndexController
{
    use ParametrizatorHelpers;

    protected $allowed_roles = array('admin');

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Category mapping for Avus export');

        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/jquery.nestedSortable.js');
        $this->assets->addJs('assets/backend/js/nested-items-sortable.js');
        $this->assets->addJs('assets/backend/js/categories-index.js');

        $tree = $this->getDI()->get(Categories::MEMCACHED_KEY);
        // TODO: since categories are using a single root tree, everything
        // is wrapped within a master root node, which has id=1 currently,
        // so this grabs those children. But this feels very hackish.
        // Need to try to come up with a better api for this...
        $tree_root_id = 1;
        Tag::setDefault('tree_root_id', $tree_root_id);
        $categories = $tree[$tree_root_id]->children;
        $this->view->setVar('categories', $categories);
        $this->view->setVar('listing_type', 'export2avus');

        $this->view->setVar('available_item_actions', array(
            array(
                'path' => 'admin/export2avus/map/',
                'name'   => '<span class="fa fa-link fa-fw"></span>Map'
            )
        ));
    }

    /**
     * Map category to avus
     */
    public function mapAction($category_id = null)
    {
        $this->tag->setTitle('Map category parameters to Avus');

        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/backend/js/export2avus-map.js');

        $category_id = (int) $category_id;

        if (!$category_id) {
            $this->flashSession->error('Missing required Category ID parameter for map action');
            return $this->redirect_back();
        }

        $category = Categories::findFirst($category_id);

        if (!$category) {
            $this->flashSession->error(sprintf('Category not found [ID: %s]', $category_id));
            return $this->redirect_back();
        }

        if (!$category->TransactionType) {
            $this->flashSession->error(sprintf('Category <strong>%s</strong> has no TransactionType set!', $category->name));
            return $this->redirect_back();
        }

        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                'p.id',
                'p.name',
                'p.slug',
                'p.type_id',
                'pt.class',
                'cfp.*'
            ))
            ->addFrom('Baseapp\Models\Parameters', 'p')
            ->innerJoin('Baseapp\Models\ParametersTypes', 'p.type_id = pt.id', 'pt')
            ->innerJoin('Baseapp\Models\CategoriesFieldsetsParameters', 'p.id = cfp.parameter_id', 'cfp')
            ->where(
                'cfp.category_id = :category_id: AND (p.dictionary_id IS NOT NULL OR p.type_id = "LOCATION")',
                array(
                    'category_id' => $category_id
                )
            )
            ->orderBy('p.name ASC');

        $dictionary_parameters = $builder->getQuery()->execute();

        $parameter_markup = array();
        $parameter_scripts = array();
        foreach ($dictionary_parameters as $dict_parameter) {
            $parameter = new MixedParameter($dict_parameter->cfp);
            if ($parameter = $parameter->get()) {
                $parameter->render_as_full_row = true;
                $render_input = $parameter->render_input();
                if (trim($render_input['html'])) {
                    $parameter_markup['parameter_' . $dict_parameter->id] = $render_input['html'];
                    if (trim($render_input['js'])) {
                        $parameter_scripts[] = $render_input['js'];
                    }
                }
            }
        }
        if (count($parameter_scripts)) {
            $this->scripts['category_specific_js'] = implode(PHP_EOL, $parameter_scripts);
        }

        $category_path = array();
        if ($category_ancestors = $category->ancestors(2)) {
            foreach ($category_ancestors as $category_ancestor) {
                $category_path[] = $category_ancestor->name;
            }
        }
        $category_path[] = $category->name;
        $category_path = implode(' › ', $category_path);

        $map_type  = 'entire';
        $map_value = '';
        if ($category->settings->publication_print_mapping) {
            $map_settings = json_decode($category->settings->publication_print_mapping);
            $map_type     = $map_settings->type ? $map_settings->type : 'entire';
            $map_value    = $map_settings->value;
        }

        $this->view->setVar('map_type', $map_type);
        if ('entire' === $map_type) {
            $this->view->setVar('avus_category', $map_value);
        } else {
            $this->view->setVar('avus_rules', $map_value);
        }

        $this->tag->setTitle('Map category to Avus [' . $category_path . ']');
        $this->view->setVar('dictionary_parameters', $dictionary_parameters);
        $this->view->setVar('parameter_markup', $parameter_markup);
        $this->view->setVar('category_path', $category_path);
        $this->view->setVar('category', $category);
    }

    /**
     * Save Action
     */
    public function saveAction($category_id)
    {
        if ($this->request->isPost()) {

            // get JSON encoded string
            $json_post = null;
            if ($this->request->hasPost('category_mapping') && '' != trim($this->request->getPost('category_mapping'))) {
                $json_post = json_decode(trim($this->request->getPost('category_mapping')));
            }

            // first, find category we will work on...
            $category_id = (int) $category_id;

            if (!$category_id) {
                $this->flashSession->error('Missing/invalid Category ID parameter for save action');
                return $this->redirect_back();
            }

            $category = Categories::findFirst($category_id);

            if (!$category) {
                $this->flashSession->error(sprintf('Category not found [ID: %s]', $category_id));
                return $this->redirect_back();
            }

            if (!$category->TransactionType) {
                $this->flashSession->error(sprintf('Category <strong>%s</strong> cannot be mapped as it has no TransactionType set!', $category->name));
                return $this->redirect_back();
            }

            if (null !== $json_post) {
                $category_settings = CategorySettings::findFirst(array(
                    'category_id = :category_id:',
                    'bind' => array(
                        'category_id' => $category_id
                    )
                ));
                if ($category_settings) {
                    $category_settings->publication_print_mapping = json_encode($json_post);
                    $category_settings->save();
                }
            }
        }

        // Redirect back to map when done with save
        return $this->redirect_to('admin/export2avus/map/' . $category_id);
    }

}
