<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Controllers\BaseBackendController;
use Baseapp\Library\Reporting;
use Baseapp\Library\Moderation;
use Baseapp\Library\Utils;
use Baseapp\Models\Ads;
use Baseapp\Models\Orders;

/**
 * Backend Index Controller
 */
class IndexController extends BaseBackendController
{
    /**
     * Index Action
     */
    public function indexAction()
    {
        $reporting = new Reporting();
        $new_orders = Orders::count('status = ' . Orders::STATUS_NEW);

        $ts = $this->getTodayTimestampRange();

        $topCounter = array(
            'total_ads_today'                => Ads::count('created_at >= ' . $ts['start'] . ' AND created_at <= ' . $ts['end']),
            'orders'                         => $new_orders,
            'reported_infractions'           => $reporting->getReportedUnresolvedInfractionsCount(),
            'paid_waiting_for_moderation'    => $reporting->getModerationWaitingCount(Ads::PAYMENT_STATE_PAID),
            'ordered_waiting_for_moderation' => $reporting->getModerationWaitingCount(Ads::PAYMENT_STATE_ORDERED),
            'free_waiting_for_moderation'    => $reporting->getModerationWaitingCount(Ads::PAYMENT_STATE_FREE)
        );

        $this->tag->setTitle('Admin panel');
        $this->assets->addCss('assets/vendor/sb-admin-2/css/plugins/morris.css');
        $this->assets->addJs('assets/vendor/sb-admin-2/js/plugins/morris/raphael.min.js');
        $this->assets->addJs('assets/vendor/sb-admin-2/js/plugins/morris/morris.min.js');
        $this->assets->addJs('assets/backend/js/statistics-index.js');
        $this->view->setVar('topCounter', $topCounter);
        $this->view->setVar('registered_users_by_month', $reporting->getRegisteredUsers());
    }

    protected function getTodayTimestampRange()
    {
        $now  = Utils::getRequestTime();
        $date = new \DateTime();
        $date->setTimestamp($now);

        $date->setTime(0, 0, 0);
        $today_start_ts = $date->getTimestamp();

        $date->setTime(23, 59, 0);
        $today_end_ts = $date->getTimestamp();

        return array(
            'start' => $today_start_ts,
            'end'   => $today_end_ts
        );
    }
}
