<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Models\Ads;
use Baseapp\Models\Categories as AdsCategories;
use Baseapp\Models\MiscSettings;
use Baseapp\Models\Users;
use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Models\CmsArticles;
use Baseapp\Models\CmsCategories as Categories;
use Baseapp\Models\Media;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Phalcon\Paginator\Adapter\QueryBuilder;

/**
 * Backend Cms articles Controller
 */
class CmsArticlesController extends IndexController
{
    use CrudHelpers;
    use CrudActions;

    /* @var CmsArticles */
    public $crud_model_class = 'Baseapp\Models\CmsArticles';

    protected $allowed_roles = array('admin', 'content');

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('CMS - Articles');

        $page       = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode       = $this->processSearchMode();
        $dir        = $this->processAscDesc('desc');
        $order_by   = $this->processOrderBy('created_at');
        $limit      = $this->processLimit();

        Tag::setDefaults(array(
            'q'        => $search_for,
            'mode'     => $mode,
            'dir'      => $dir,
            'order_by' => $order_by,
            'limit'    => $limit
        ), true);

        $category_id = null;
        if ($this->request->has('category_id')) {
            $category_id = $this->request->get('category_id', 'int', null);
        }

        // Category tree (view variable) + category dropdown generation
        $this->category_id_dropdown($category_id, array(
            'emptyText' => 'Any category',
            'subtext'   => false
        ));

        // If there's a certain category specified, include any potential subcategories in the search too
        $category_ids = array();
        if (null !== $category_id) {
            $category_ids[] = (int) $category_id;
            $category = Categories::findFirst($category_id);
            if ($category) {
                $descendants = $category->descendants();
                if ($descendants) {
                    foreach ($descendants as $descendant) {
                        $category_ids[] = (int) $descendant->id;
                    }
                }
            }
        }

        // Available search fields
        $available_fields = array(
            'title',
            'slug',
            'excerpt',
            'content',
            'raw_tags',
            'publish_date',
            'created_at',
            'modified_at',
            'featured',
            'created_by_user_id',
            'modified_by_user_id',
            'active'
        );
        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array('
                Baseapp\Models\CmsArticles.id,
                Baseapp\Models\CmsArticles.title,
                Baseapp\Models\CmsArticles.slug,
                Baseapp\Models\CmsArticles.category_id,
                Baseapp\Models\CmsArticles.active,
                Baseapp\Models\CmsArticles.created_at,
                Baseapp\Models\CmsArticles.modified_at,
                Baseapp\Models\CmsArticles.publish_date,
                Baseapp\Models\CmsArticles.featured,
                Baseapp\Models\CmsArticles.featured_start,
                Baseapp\Models\CmsArticles.featured_end
            '))
            ->from('Baseapp\Models\CmsArticles');

        if (null !== $category_id) {
            $category_ids_in = 'Baseapp\Models\CmsCategories.id IN (' . implode(', ', $category_ids) . ')';
            $builder->innerJoin(
                'Baseapp\Models\CmsCategories',
                'Baseapp\Models\CmsArticles.category_id = Baseapp\Models\CmsCategories.id' .
                ' AND ' . $category_ids_in
            );
        } else {
            $builder->innerJoin(
                'Baseapp\Models\CmsCategories',
                'Baseapp\Models\CmsArticles.category_id = Baseapp\Models\CmsCategories.id'
            );
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                $conditions[] = 'Baseapp\Models\CmsArticles.' . $fld . ' LIKE ' . $field_bind_name;
                $binds[$fld] = $val;
                // $builder->orWhere($fld . ' LIKE ' . $field_bind_name, array($fld => $val));
            }
            $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        $builder->orderBy('Baseapp\Models\CmsArticles.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array(
            'builder' => $builder,
            'limit'   => $limit,
            'page'    => $page
        ));
        $current_page = $paginator->getPaginate();
        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/cms-articles',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
    }

    /**
     * @param int|null $selected Value to preselect
     * @param array|null $options Tag::select() option overrides
     *
     * @return array|null
     */
    protected function category_id_dropdown($selected = null, $options = array())
    {
        // Category tree is used so we don't have to call getPath for every node,
        // (and instead just get already queried data) + to populate the categories dropdown
        $model       = new Categories();
        $tree        = $model->getTree();
        $selectables = $model->getSelectablesFromTreeArray($tree, '', $with_root = false, $depth = null, $breadcrumbs = true);

        // Prevent invalid choices from being set as default
        if (!array_key_exists($selected, $selectables)) {
            $selected = null;
        }

        // Preselected value, if there's a valid one
        if ($selected) {
            Tag::setDefault('category_id', $selected);
        }

        // Process $selectables before using them in Tag::select() in order to
        // display a more useful dropdown (for when it's not being handled by selectpicker())
        if (isset($options['subtext']) && !$options['subtext']) {
            foreach ($selectables as $k => $data) {
                if (isset($data['attributes'])) {
                    if (isset($data['attributes']['data-subtext'])) {
                        $subtext                 = $data['attributes']['data-subtext'];
                        $selectables[$k]['text'] = $subtext . ' › ' . $selectables[$k]['text'];
                        unset($selectables[$k]['attributes']['data-subtext']);
                    }
                }
            }
            unset($options['subtext']);
        }

        // Default options
        $defaults = array(
            'category_id',
            $selectables,
            'useEmpty' => true,
            'emptyText' => 'Choose category...',
            'emptyValue' => '',
            'class' => 'form-control',
            // 'style' => 'width:100%;'
        );
        // Merge passed in $options
        $options = array_merge($defaults, $options);

        // Build the dropdown
        $category_id_dropdown = Tag::select($options);
        $this->view->setVar('category_id_dropdown', $category_id_dropdown);

        // Set the entire tree as a view variable (for displaying an article's category path or similar)
        $this->view->setVar('tree', $tree);

        return $tree;
    }

    public function createAction()
    {
        $selected_category_id = null;
        if ($this->request->has('category_id')) {
            $selected_category_id = (int) $this->request->get('category_id');
        }

        $this->tag->setTitle('Create new article');
        $this->view->setVar('form_title_long', 'Create new article');
        $this->view->setVar('form_action', 'add');
        $this->add_common_crud_assets();
        $this->view->pick('cms-articles/edit');  // Use the same form/view as editAction

        $article = new CmsArticles();
        if ($this->request->isPost()) {
            $user_id = $this->auth->get_user()->id;
            $article->created_by_user_id = $user_id;
            $article->modified_by_user_id = $user_id;
            $created = $article->add_new($this->request);
            if ($created instanceof CmsArticles) {
                $this->flashSession->success('<strong>Article created!</strong> ' . $article->get_frontend_view_link('html'));
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/cms-articles/edit/' . $article->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->view->setVar('errors', $created);
                $this->prepare_article_media($this->request->getPost());
                $article->active = $this->request->hasPost('active') ? 1 : 0;
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        } else {
            $article->active = 1;
            $this->prepare_article_media();
        }

        $this->category_id_dropdown($selected_category_id);
        $this->view->setVar('article', $article);
    }

    public function add_common_crud_assets()
    {
        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/ckeditor/ckeditor.js');
        $this->assets->addJs('assets/vendor/ckeditor/adapters/jquery.js');
        $this->assets->addJs('assets/vendor/moment/moment.js');
        $this->assets->addJs('assets/vendor/moment/locale/hr.js');
        $this->assets->addJs('assets/vendor/typeahead/typeahead.bundle.min.js');
        $this->assets->addCss('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->assets->addCss('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css');
        $this->assets->addJs('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->assets->addCss('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css');
        $this->assets->addJs('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
        $this->assets->addCss('assets/css/uploadable-sorter.css');
        $this->assets->addJs('assets/js/uploadable-sorter.js');
        $this->assets->addJs('assets/backend/js/cms-articles-edit.js');
    }

    /**
     * Edit Action
     */
    public function editAction($entity_id = null)
    {
        $entity_id = (int) $entity_id;

        /* @var $entity CmsArticles */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        $title = 'Edit article';
        $this->tag->setTitle($title);
        $this->view->setVar('form_title_long', $title);
        $this->add_common_crud_assets();

        if ($this->request->isPost()) {
            $entity->modified_by_user_id = $this->auth->get_user()->id;
            $saved = $entity->save_changes($this->request);
            if ($saved instanceof CmsArticles) {
                $this->flashSession->success('<strong>Article updated!</strong> ' . $saved->get_frontend_view_link('html'));
                $next_url = $this->get_next_url();
                $save_action = $this->get_save_action();
                if ('save' === $save_action) {
                    $next_url = 'admin/cms-articles/edit/' . $saved->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->view->setVar('errors', $saved);
                $this->prepare_article_media($this->request->getPost());
                $entity->active = $this->request->hasPost('active') ? 1 : 0;
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        } else {
            $this->prepare_article_media($entity);
        }

        $this->category_id_dropdown($entity->category_id);

        $this->view->setVar('category', $entity->getCategory());
        $this->view->setVar('article', $entity);
        $this->view->setVar('user_created', $entity->CreatedBy);
        $this->view->setVar('user_modified', $entity->ModifiedBy);
    }

    /**
     * Helper method to prepare article's media based on the given $source
     * @param  null|CmsArticles|array $source
     */
    protected function prepare_article_media($source = null)
    {
        $media_ids = array();
        $article_media = array();
        if ($source instanceof CmsArticles) {
            $entity_media = $source->getMedia(array('order' => 'sort_idx'));
            if ($entity_media && count($entity_media)) {
                foreach($entity_media as $media) {
                    $media_ids[] = $media->media_id;
                }
            }
        } elseif (is_array($source) && isset($source['article_media_gallery'])) {
            $media_ids = $source['article_media_gallery'];
        }

        if (count($media_ids)) {
            foreach ($media_ids as $media_id) {
                $media_obj = Media::findFirst($media_id);
                if ($media_obj) {
                    $article_media[] = $media_obj;
                }
            }
        } else {
            $article_media = null;
        }

        $this->view->setVar('article_media', $article_media);
    }

    /**
     * activeToggle Action
     */
    public function activeToggleAction($entity_id = null)
    {
        $entity_id = (int) $entity_id;

        /* @var $entity CmsArticles */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($entity->activeToggle()) {
            $this->flashSession->success('Active status for <strong>' . $entity->ident() . '</strong> changed successfully.');
        } else {
            $this->flashSession->error('Failed changing active status for <strong>' . $entity->ident() . '</strong>.');
        }

        return $this->redirect_back();
    }

    public function aktualnoAction()
    {
        $this->tag->setTitle('"Aktualno:" Edit');

        if ($this->request->isPost()) {
            if ($this->request->hasPost('save')) {
                $new_code = trim($this->request->getPost('textarea_contents'));
                if (!empty($new_code)) {
                    $saved = MiscSettings::saveHomepageAktualno($new_code);
                    if (!$saved) {
                        $this->flashSession->error('Failed saving new "aktualno" data');
                    }
                }
            }
        }

        // If new contents have been posted, display them instead.
        // This way in case of errors we have the previous state available (otherwise it'd be lost)
        if (isset($new_code)) {
            $contents = $new_code;
        } else {
            $contents = '';
            $record = MiscSettings::getAktualnoRecord();
            if ($record) {
                $contents = $record->value;
            }
        }

        $textarea_contents = trim($contents);
        $this->view->setVar('textarea_contents', $textarea_contents);
    }

    public function subHeaderLinksAction()
    {
        $this->tag->setTitle('"SubHeader links:" Edit');

        if ($this->request->isPost()) {
            if ($this->request->hasPost('save')) {
                $new_code = trim($this->request->getPost('textarea_contents'));
                if (!empty($new_code)) {
                    $saved = MiscSettings::saveSubHeaderLinks($new_code);
                    if (!$saved) {
                        $this->flashSession->error('Failed saving new "subHeader Links" data');
                    }
                }
            }
        }

        // If new contents have been posted, display them instead.
        // This way in case of errors we have the previous state available (otherwise it'd be lost)
        if (isset($new_code)) {
            $contents = $new_code;
        } else {
            $contents = '';
            $record = MiscSettings::getSubHeaderLinksRecord();
            if ($record) {
                $contents = $record->value;
            }
        }

        $textarea_contents = trim($contents);
        $this->view->pick('cms-articles/subHeaderLinks');
        $this->view->setVar('textarea_contents', $textarea_contents);
    }

    public function latestAdsExclusionsAction()
    {
        $this->tag->setTitle('Exclusion Lists (for latest sidebar ads)');

        // Process submit
        if ($this->request->isPost()) {
            if ($this->request->hasPost('save') || $this->request->hasPost('save-and-rebuild')) {
                $new_exclude_category_ids = $this->request->getPost('categories');
                $saved = MiscSettings::saveLatestAdsCategoryIdExclusions($new_exclude_category_ids);
                if (!$saved) {
                    $this->flashSession->error('Failed saving fresh categories exclusion data');
                } else {
                    $this->flashSession->success('Category exclusion data saved');
                }

                $new_exclude_user_ids = trim($this->request->getPost('users'));
                $saved = MiscSettings::saveLatestAdsUserIdExclusions($new_exclude_user_ids);
                if (!$saved) {
                    $this->flashSession->error('Failed saving fresh users exclusion data');
                } else {
                    $this->flashSession->success('User exclusion data saved');
                }
            }

            if ($this->request->hasPost('save-and-rebuild')) {
                // Rebuild the cached version of something on save-and-rebuild
                // TODO/FIXME: do we need all this anymore?
                // $this->flashSession->success('Latest ads markup regenerated');
            }

            // Redirect to self so that hitting refresh doesn't re-submit and re-build
            return $this->redirect_self();
        }

        // Get existing exclusion data
        $exclude_category_ids = MiscSettings::getLatestAdsCategoryIdExclusionList();
        if (empty($exclude_category_ids)) {
            $exclude_category_ids = null;
        }

        $exclude_user_ids = MiscSettings::getLatestAdsUserIdExclusionList();

        $model       = new AdsCategories();
        $tree        = $this->getDI()->get(AdsCategories::MEMCACHED_KEY);
        $selectables = $model->getSelectablesFromTreeArray(
            $tree,
            '-',
            $with_root = false,
            $depth = null,
            $breadcrumbs = true
        );
        $this->buildAdsCategoriesDropdown(
            'categories',
            $selectables,
            $exclude_category_ids,
            array(
                'multiple' => 'multiple'
            )
        );

        // Prep a list of existing user data for tagsinput component
        $excluded_users_json_script = '';
        if (!empty($exclude_user_ids)) {
            $excluded_users_data = Users::decorateUsersDataForJsonFromIds($exclude_user_ids);
            $excluded_users_json_script = "<script>\n";
            $excluded_users_json_script .= sprintf("var excluded_users = %s\n", json_encode( $excluded_users_data ));
            $excluded_users_json_script .= "\n</script>";
        }
        $this->view->setVar('excluded_users_json_script', $excluded_users_json_script);

        // Register assets needed
        $this->assets->addCss('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->assets->addCss('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css');
        $this->assets->addJs('assets/vendor/typeahead/typeahead.bundle.min.js');
        $this->assets->addJs('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js');
        $this->assets->addJs('assets/backend/js/cms-latest-ads-exclusions.js');
    }

    protected function buildAdsCategoriesDropdown($dropdown_name, $selectables = array(), $selected = null, $options = array())
    {
        // Prevent invalid choices from being set as default
        if ($selected) {
            if (is_array($selected)) {
                $selected_array = array();
                foreach ($selected as $sel) {
                    if (array_key_exists($sel, $selectables)) {
                        $selected_array[] = $sel;
                    }
                }
                if (count($selected_array) == 0) {
                    $selected = null;
                } else {
                    $selected = $selected_array;
                }
            } else {
                if (!array_key_exists($selected, $selectables)) {
                    $selected = null;
                }
            }
        }

        $dropdown_id = $dropdown_name;

        if (isset($options['multiple'])) {
            $dropdown_name = $dropdown_name . '[]';
        }

        // Preselected value, if there's a valid one
        if ($selected) {
            if (!is_array($selected)) {
                Tag::setDefault($dropdown_name, $selected);
            }
        }

        // Default options
        $defaults = array(
            $dropdown_name,
            $selectables,
            'class' => 'form-control bs-select-hidden',
            'id'    => $dropdown_id
        );

        if ($selected) {
            if (is_array($selected)) {
                $selected = implode(',', $selected);
            }
            $defaults['data-preselect'] = $selected;
        }

        // Merge passed in $options
        $options = array_merge($defaults, $options);

        // Build the dropdown
        $category_dropdown = Tag::select($options);
        $this->view->setVar($dropdown_id . '_dropdown', $category_dropdown);
    }

    /**
     * @param array $ids
     *
     * @return \stdClass[] Array of \stdClass objects each with a 'text' and 'value' properties
     */
    protected function decorateCategoriesDataForJsonFromIds(array $ids)
    {
        $ids = implode(',', array_map(function($id){
            return (int) trim($id);
        }, $ids));

        /* @var $entities AdsCategories[] */
        $condition = 'id IN (' . $ids . ') ORDER BY FIELD(id,' . $ids . ')';
        $entities = AdsCategories::find($condition);

        $data = array();
        foreach ($entities as $entity) {
            $o = new \stdClass();
            $o->text = $entity->name;
            $o->value = (string) $entity->id;

            $data[] = $o;
        }

        return $data;
    }
}
