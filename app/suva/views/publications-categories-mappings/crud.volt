<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Publications Categories Mapping</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="_gfid" value="{{_gfid}}"/>
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<input type="hidden" name="publication_id" value="{{entity.publication_id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Publications Categories Mapping</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									{{ partial("partials/ctlText",[
										'value' : entity.Publication().name, 
										'title' : 'Publication', 
										'field' : '',
										'width' : 3, 
										'readonly' : 1
									] )}} 
									
									{{ partial("partials/ctlTextTypeV3",[
										'title' : 'Publication Category'
										, 'field' : 'category_mapping_id'
										, 'width' : 6
										, 'value' : entity.category_mapping_id
										, '_context' : _context 
										, 'value_show' :  entity.CategoryMapping().n_path
										, 'model_name' : 'CategoriesMappings' 
										, 'expr_search' : 'n_path'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'n_path'
										, 'expr_show_list' : 'n_path'
										, 'additional_where' : ' and active = 1 and  n_publications_id = ' ~ entity.publication_id ~ ' order by n_path '
										, 'nr_returned_rows' : 100
									]) }}
								</div>
								
								
								<div class="row">
									{{ partial("partials/ctlTextTypeV3",[
										'title' : 'Mapped Global Category'
										,'width' : 6
										,'field' : 'mapped_category_id'
										, 'value' : entity.mapped_category_id
										, '_context' : _context 
										, 'value_show' :  entity.MappedCategory().n_path
										, 'model_name' : 'Categories' 
										, 'expr_search' : 'n_path'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'n_path'
										, 'expr_show_list' : 'n_path'
										, 'additional_where' : ' and active = 1  order by n_path '
										, 'nr_returned_rows' : 30
									]) }}
									
									
									{{ partial("partials/ctlTextTypeV3",[
										'title' : 'Mapped Dictionary Value'
										,'width': 5
										,'field' : 'mapped_dictionary_id'
										, 'value' : entity.mapped_dictionary_id
										, '_context' : _context 
										, 'value_show' :  entity.MappedDictionary().n_path
										, 'model_name' : 'Dictionaries' 
										, 'expr_search' : 'name'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'n_path'
										, 'expr_show_list' : 'n_path'
										, 'additional_where' : ' and active = 1 order by n_path '
										, 'nr_returned_rows' : 600
									]) }}
									
									{{ partial("partials/ctlTextTypeV3",[
										'title' : 'Mapped Location Value'
										,'width': 5
										,'field' : 'mapped_location_id'
										, 'value' : entity.mapped_location_id
										, '_context' : _context 
										, 'value_show' :  entity.MappedLocation().name
										, 'model_name' : 'Locations' 
										, 'expr_search' : 'name'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'n_path'
										, 'expr_show_list' : 'n_path'
										, 'additional_where' : ' order by name '
										, 'nr_returned_rows' : 30
									]) }}
									
									{#{ partial("partials/ctlDropdown",[
										'value': entity.layout_id, 
										'title': 'DTP Layout', 
										'list': _model.Layouts(), 
										'field': 'layout_id', 
										'width': 4, 
										'option_fields':['id','name'],
										'_context': _context				
									] )}#}	
									
									
									{{ partial("partials/ctlCheckbox",[
										'value':entity.is_active, 
										'title':'active', 
										'width':1, 
										'field':'is_active'
									] ) }}	
									
								
										
										
										
										
								</div>
								<div class="row">
									<div class="col-lg-4 col-md-4"> 
										<input name="imageUpload" value id="imageUpload"  type="file"></input>
									</div>							
									
									<div class="col-lg-3 col-md-3"> 	
										<input 
											class="submit_picture"
											value="submit picture"
											type="button"
											onclick="var file_data = $('#imageUpload').prop('files')[0];
													var form_data = new FormData();
													form_data.append('file', file_data);  
													form_data.append('n_publications_categories_mappings_id', {{ entity.id }});
													$.ajax({
														type:'POST',
														url: '/suva/ajax/categoryMappingImageUpload',
														data:form_data,
														cache:false,
														contentType: false,
														processData: false,
														success:function(data){
															alert('image uploaded')
															console.log(data)
															
															
														}						
													});"
										></input>	
									</div>
									<div class="col-md-3 col-lg-2 image_preview">									
									<label>Uploaded image:</label>
										{% if entity.photo_url != '' %}
											<img id="preview" src="/{{entity.photo_url}}" style="width:100%;"/>
										{% endif %}
									</div>	
									
								</div>			
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}

		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>



