{# Admin Category Products Config #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Category products config <small>{{ category.getPath() }}</small></h1>
        {{ partial("partials/blkFlashSession") }}
        {% set category_settings = category.getSettings() %}
        {{ form('admin/products-categories/save/' ~ category.id, 'id' : 'products-config', 'method' : 'post') }}
            {{ hiddenField('_csrftoken') }}
            <div role="tabpanel">
                <ul class="nav pull-right">
                    <li><button class="btn btn-primary" type="submit" name="saveConfig" id="saveConfig"><span class="fa fa-save fa-fw"></span> Save changes</button></li>
                </ul>
                <ul class="nav nav-tabs margin-bottom-15" role="tablist" id="types-tabs">
                    <li role="presentation" class="active"><a href="#online-products" aria-controls="online-products" role="tab" data-toggle="tab">Online products</a></li>
                    {# <li role="presentation"><a href="#user_products" aria-controls="user-products" role="tab" data-toggle="tab">User products</a></li> #}
                    <li role="presentation"><a href="#offline-products" aria-controls="offline-products" role="tab" data-toggle="tab">Offline products</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="online-products">
                        {{ forms_markup['online'] }}
                    </div>
                    {#
                    <div role="tabpanel" class="tab-pane fade in" id="user-products">
                        {{ forms_markup['user'] }}
                    </div>
                    #}
                    <div role="tabpanel" class="tab-pane fade in" id="offline-products">
                        {{ forms_markup['offline'] }}
                    </div>
                </div>
            </div>
        {{ endForm() }}
    </div>
</div>

