 {# CONTEXT<pre>{{ print_r( _context, true ) }}</pre> #}
 
 
	{% set publication = null %}	
	{% if array_key_exists ('default_filter', _context) %}
		{% if array_key_exists ('__Products__publication_id', _context['default_filter']) %}
			{% if _context['default_filter']['__Products__publication_id'] > 0 %}
				{% set publicationId = _context['default_filter']['__Products__publication_id'] %}
				{% set publication = _model.Publication( publicationId ) %}
			{% endif %}
		{% endif %}
	{% endif %}
	
	{% if array_key_exists ('default_filter', _context) %}
		{% if array_key_exists( 'publication_id', _context['default_filter'] ) %}
			{% set publication = _model.Publication( _context['default_filter']['publication_id'] ) %}
		{% endif %}
	{% endif %}
	
	{% set productsPubFilter = ( publication ? ' publication_id = ' ~ publication.id ~ ' ' : ' 1 = 1 ' ) %}
	{% set catMapsPubFilter = ( publication ? ' n_publications_id = ' ~ publication.id ~ ' ' : ' 1 = 1 ' ) %}
	
<div class="row">	 
	<div class="col-lg-12 col-md-12">
		<div>
			{#{ partial("partials/btnAddNew", ['controller':'products-categories-prices'] ) }#}
			{#% if publication %}
				<div class = "pull-right">
					<a  href = "/suva/publications/crud/{{ publication.id }}">
						<span class="btn btn-warning ">Edit {{ publication.name }} </span>&nbsp;
					</a>
				</div>
			{% endif %#} 
			<h1 class="page-header">
				Product prices based on Categories 
				{% if publication is defined %}
					for <i>{{ publication.name }}</i>
				{% endif %}
			</h1>
		</div>					
			

		<br/>
		
		{{ partial("partials/blkFlashSession") }}


		<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",['fields':[ 
				['title':'Options'														,'width':150	,'show_navigation' : true ] 
				,['title':'ID'						,'name':'id'						,'width':100		,'search_ctl':'ctlNumeric'	]
				,['title':'Product'					,'name':'products_id'				,'width':400	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Products( productsPubFilter ) , 'option_fields':[ 'publication_id', 'id','name'] ]] 
				,['title':'Publication Category'	,'name':'categories_mappings_id'	,'width':350		,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.CategoriesMappings( [ catMapsPubFilter, 'order': 'n_path' ] ) , 'option_fields':[ 'n_path', 'id'] ]] 
				,['title':'Price'					,'name':'unit_price'				,'width':100	,'search_ctl':'ctlNumeric'	,'search_ctl_params': [ 'decimals':2 ]] 
				,['title':'Product Displayed'		,'name':'is_displayed'				,'width':100		,'search_ctl':'ctlNumeric'	] 
			
			] ]) }}

			{% for id in ids %}
				
				{% set entity = _model.getOne('ProductsCategoriesPrices',id) %}
				{% set product = _model.getOne('Products', entity.products_id) %}
				{% set category = _model.getOne('Categories', entity.categories_id) %}
				{% set category_mapping = _model.getOne('CategoriesMappings', entity.categories_mappings_id) %}
				
				
							<!-- AJAX -->
				{% set _context['ajax_edit'] = true %}
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['current_entity_table_name'] = 'n_products_categories_prices' %}

				
				
				<tr>
					<td >{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'products-categories-prices'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>{{ product.id ~ ' - ' ~ product.name ~ ' - ' ~ product.days_published ~ ' dana/objava ' ~ product.Publication().name }}</td> 
					
					<td>{{ entity.categories_mappings_id ~ ' - ' ~ category_mapping.n_publications_id ~ ' - ' ~ category_mapping.n_path }}</td>
					<td style="text-align:right;">
						{{ partial("partials/ctlNumeric",[
							'value':entity.unit_price, 
							'field':'unit_price', 
							'decimals':2, 
							'_context': _context
						] ) }}				
					
					</td>
					
					<td>
						{{ partial("partials/ctlCheckbox", [
							'value' : entity.is_displayed, 
							'field' : 'is_displayed',
							'_context' : _context
						] ) }} 
					</td>
					{#
					<td>
						{{ partial("partials/ctlCheckbox", [
							'value' : entity.is_active, 
							'field' : 'is_active',
							'_context' : _context
						] ) }} 
					</td>
					#}
					{#% if ! publication %}
					
						<td>{{ category.id ~ ' - ' ~ category.path }}</td>
					{% endif %#}
					

					
				</tr>
            {% else %}
            	<tr><td colspan="6">Currently, there are no combinations od products, discounts and prices here!</td></tr>
            {% endfor %}
        </table>
    </div>
</div>


{#
stari

,['title':'Publication Category'	,'name':''	,'width':500	,'search_ctl':'ctlTextTypeV3'	,'search_ctl_params': [
																																						 'model_name' : 'CategoriesMappings' 
																																						, 'expr_search' : 'n_path'
																																						, 'expr_value' : 'id' 
																																						, 'expr_show_input' : 'n_path'
																																						, 'expr_show_list' : 'n_path'
																																						, 'additional_where' : ' and active = 1 and ' ~ catMapsPubFilter ~ ' and transaction_type_id is not null order by n_path '
																																						, 'readonly' : false
																																						, 'nr_returned_rows' : 100
																																					]]  




#}