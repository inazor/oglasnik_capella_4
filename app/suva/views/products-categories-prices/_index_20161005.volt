<div class="row">
	
	{% set txtPublication = '' %}
	{% set publicationId = null %}
	{% if array_key_exists ('filter', _context) %}
		{% if array_key_exists ('__Products__publication_id', _context['filter']) %}
			{% if _context['filter']['__Products__publication_id'] > 0 %}
				{% set publicationId = _context['filter']['__Products__publication_id'] %}
				{% set publication = _model.Publication( publicationId ) %}
				{% set txtPublication = ' for <i>' ~ publication.name ~ '</i>' %}
			{% endif %}
		{% endif %}
	{% endif %}
	
	
	
	
	<div class="col-lg-12 col-md-12">
		<div>
			{{ partial("partials/btnAddNew", ['controller':'products-categories-prices'] ) }}
			{% if publicationId %}
				<div class = "pull-right">
					<a  href = "/suva/publications/crud/{{ publicationId }}">
						<span class="btn btn-warning ">Edit {{ publication.name }} </span>&nbsp;
					</a>
				</div>
			{% endif %} 
			<h1 class="page-header">Product prices based on Categories {{ txtPublication }}</h1>
		</div>					
			

		<br/>
		
		{{ partial("partials/blkFlashSession") }}

			{% if publicationId %}
				<table style="width:1000 !important;" class="table table-striped table-hover table-condensed">
				{{ partial("partials/blkTableSort",['fields':[ 
					['title':'Options'														,'width':150	,'show_navigation' : true ] 
					,['title':'ID'						,'name':'id'						,'width':50		,'search_ctl':'ctlNumeric'	]
					,['title':'Product'					,'name':'products_id'				,'width':500	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Products(" publication_id =  " ~ publicationId ) , 'option_fields':[ 'publication_id', 'id','name'] ]] 
					,['title':'Publication Category'	,'name':'categories_mappings_id'	,'width':200	,'search_ctl':'ctlTextTypeV3'	,'search_ctl_params': [
																																							 'model_name' : 'CategoriesMappings' 
																																							, 'expr_search' : 'n_path'
																																							, 'expr_value' : 'id' 
																																							, 'expr_show_input' : 'n_path'
																																							, 'expr_show_list' : 'n_path'
																																							, 'additional_where' : ' and active = 1 and n_publications_id = ' ~ publicationId ~ ' and transaction_type_id is not null order by n_path '
																																							, 'readonly' : false
																																							, 'nr_returned_rows' : 100
																																						]]  

					,['title':'Price'					,'name':'unit_price'				,'width':200	,'search_ctl':'ctlNumeric'	,'search_ctl_params': [ 'decimals':2 ]] 
					,['title':'Product Displayed'		,'name':'is_displayed'				,'width':50		,'search_ctl':'ctlNumeric'	] 
					,['title':'Active'					,'name':'is_active'					,'width':50		,'search_ctl':'ctlNumeric'	]
				] ]) }}
			{% else %}
				<table style="width:1900px !important;" class="table table-striped table-hover table-condensed">
				{{ partial("partials/blkTableSort",['fields':[ 
					['title':'Options'														,'width':150	,'show_navigation' : true ] 
					,['title':'ID'						,'name':'id'						,'width':50		,'search_ctl':'ctlNumeric'	]
					,['title':'Product'					,'name':'products_id'				,'width':300	,'search_ctl':'ctlDropdown'		,'search_ctl_params': [ 'list':_model.Products() , 'option_fields':[ 'publication_id', 'id','name'] ]] 
					,['title':'Publication Category'	,'name':'categories_mappings_id'	,'width':300	,'search_ctl':'ctlTextTypeV3'	,'search_ctl_params': [
																																							 'model_name' : 'CategoriesMappings' 
																																							, 'expr_search' : 'n_path'
																																							, 'expr_value' : 'id' 
																																							, 'expr_show_input' : 'n_path'
																																							, 'expr_show_list' : 'n_path'
																																							, 'additional_where' : ' and active = 1 and transaction_type_id is not null order by n_path '
																																							, 'readonly' : false
																																						]]  


					,['title':'Publication Category'	,'name':'categories_mappings_id'	,'width':300	,'search_ctl':'ctlTextTypeV3'	,'search_ctl_params': [
																																							 'model_name' : 'CategoriesMappings' 
																																							, 'expr_search' : 'n_path'
																																							, 'expr_value' : 'id' 
																																							, 'expr_show_input' : 'n_path'
																																							, 'expr_show_list' : 'n_path'
																																							, 'additional_where' : ' and active = 1 and transaction_type_id is not null order by n_path '
																																							, 'readonly' : false
																																						]]  

					,['title':'Price'					,'name':'unit_price'				,'width':200	,'search_ctl':'ctlNumeric'	,'search_ctl_params': [ 'decimals':2 ] ] 
					,['title':'Product Displayed'		,'name':'is_displayed'				,'width':50		,'search_ctl':'ctlNumeric'	] 
					,['title':'Active'					,'name':'is_active'					,'width':50		,'search_ctl':'ctlNumeric'	]
					,['title':'Category'				,'name':'categories_id'				,'width':500	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Categories(), 'option_fields':['id','n_path'] ]] 
				] ]) }}
			
			{% endif %}

			{% for id in ids %}
				
				{% set entity = _model.getOne('ProductsCategoriesPrices',id) %}
				{% set product = _model.getOne('Products', entity.products_id) %}
				{% set category = _model.getOne('Categories', entity.categories_id) %}
				{% set category_mapping = _model.getOne('CategoriesMappings', entity.categories_mappings_id) %}
				
				
							<!-- AJAX -->
				{% set _context['ajax_edit'] = true %}
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['current_entity_table_name'] = 'n_products_categories_prices' %}

				
				
				<tr>
					<td >{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'products-categories-prices'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>{{ product.id ~ ' - ' ~ product.name ~ ' - ' ~ product.days_published ~ ' dana/objava ' ~ product.Publication().name }}</td> 
					
					<td>{{ entity.categories_mappings_id ~ ' - ' ~ category_mapping.n_publications_id ~ ' - ' ~ category_mapping.n_path }}</td>
					<td style="text-align:right;">
						{{ partial("partials/ctlNumeric",[
							'value':entity.unit_price, 
							'field':'unit_price', 
							'decimals':2, 
							'_context': _context
						] ) }}				
					
					</td>
					
					<td>
						{{ partial("partials/ctlCheckbox", [
							'value' : entity.is_displayed, 
							'field' : 'is_displayed',
							'_context' : _context
						] ) }} 
					</td>
					<td>
						{{ partial("partials/ctlCheckbox", [
							'value' : entity.is_active, 
							'field' : 'is_active',
							'_context' : _context
						] ) }} 
					</td>
					{% if !publicationId %}
					
						<td>{{ category.id ~ ' - ' ~ category.path }}</td>
					{% endif %}
					

					
				</tr>
            {% else %}
            	<tr><td colspan="6">Currently, there are no combinations od products, discounts and prices here!</td></tr>
            {% endfor %}
        </table>
    </div>
</div>