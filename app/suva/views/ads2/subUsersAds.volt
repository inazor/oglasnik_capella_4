											
{% set _url = '/suva/ads2/subUsersAds/' ~ user.id  %}
{% set _is_ajax_submit = true %}
{% set _window_id = 'subUsersAds' %}


{% set arrFreeAds = _model.isAdFree (null, user.id) %}
{% set arrExtendableAds = _model.isAdExtendable (null, user.id) %}

{#{var_dump (arrFreeAds)}#}


	<!-- User's Ads -->
		<div class="row">
		
			{{ form(NULL, 'id': '_form_' ~ _window_id , 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
				{{ hiddenField('next') }}
				{{ hiddenField('_csrftoken') }}
				{% if ad is defined and ad.id is defined %}
					{{ hiddenField(['ad_id', 'value': ad.id]) }}
				{% endif %}
				{{ hiddenField(['user_id', 'value': user.id]) }}
				{% if user.id > 0 %}
					<input type="hidden" name="_context[selected_user_id]" value = "{{user.id}}" ></input>
					<input type="hidden" name="_context[clicked_action]" id="_clicked_action_{{_window_id}}" value = "" ></input>
					<input type="hidden" name="_context[clicked_action_params][user_id]" value="{{ user.id }}"/> 
					<input type="hidden" name="_context[clicked_action_params][logged_user_id]" value="{{ auth.get_user().id }}"/> 


					<input type="hidden" name="n_first_uploaded_photo_id" value="{{( ad is defined ? ad.n_first_uploaded_photo_id : '' ) }}"/> 
					<input type="hidden" name="moderation" value="ok"/> 
					<input type="hidden" id="_context_active_tab_{{_window_id}}" name = "_context[_active_tab]" value="{{ ( array_key_exists('_active_tab' , _context) ? _context['_active_tab'] : '' ) }}"></input>

					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{#
								<span 
									class="btn btn-primary" 
									style="text-align:right; height:25px; margin-bottom:5px;" 
									onclick="var options = document.getElementById('users_ads_tab').options, count = 0;			
											 for (var i=0; i < options.length; i++) {
												if (options[i].selected){ 
													count++;
													console.log(options[i])
												}
											 }
											 $('.selected_ads').html(count);"
									>
									Calculate
								</span>
								
								<span 
									class="btn btn-primary" 
									style="text-align:right; height:25px; margin-bottom:5px;" 
									onclick="
										$('#_clicked_action_subUsersAds').val('actExtendFreeAds'); 
										ajaxSubmit( '{{_url}}' , 'subUsersAds' , '_form_subUsersAds' );
										">
									Extend free ads
								</span>
								<span 
									class="btn btn-primary" 
									style="text-align:right; height:25px; margin-bottom:5px;" 
									onclick="
										$('#_clicked_action_subUsersAds').val('actResyncFrontend'); 
										ajaxSubmit( '{{_url}}' , 'subUsersAds' , '_form_subUsersAds' );
										">
									Refresh Frontend
								</span>
								#}
								{% for action_id in _model.getAllowedActionIds( auth.get_user().id, null , 'ads' ) %}
										{% set action = _model.Action( action_id ) %}
										
										
										{{ action.buttonHtml_submitViaActionInputOnForm( [
											'action_input_id': '_clicked_action_' ~ _window_id
											,'is_ajax' : _is_ajax_submit
											,'action_url' : _url
											,'action_window_id' : _window_id
											,'require_confirmation': action.require_confirmation
										] ) }}
								
								{% endfor  %}
							</div>
						</div>
						<div class ="panel-default" style = "overflow-x:scroll;">
							<div class="panel-heading" >
								<h3 class="panel-title" style="font-family:Courier; font-size:12px; padding-left:3px; display:block !important;" >
									{{ str_replace("+","&nbsp","AD+ID+++Ext.ON OFF+++++NASLOV+++++++++++++++++++OPIS+ZA+TISAK(prvih+50+znakova)+++++++++++++++++++++ZADNJA AKTIVNOST++++KATEGORIJA")}}
								</h3>
							</div>
							<div class="panel-body">
								<select id="users_ads_tab" style="display:block !important; height:300px;" name="_context[clicked_action_params][ads][]" multiple>
									{% for uad in user.Ads(['order': 'id desc', 'limit':200 ]) %}
										{% set lpoi = uad.last_published_OrderItem( true ) %} 
										{% set _descr = (uad.description_offline ? uad.description_offline : uad.description ) %}
										
										{# ovo treba bolje definirati 
										{% if uad.n_category_mapping_id > 0 and uad.CategoryMapping() %}
											{% set _catMap = _model.getOne('CategoriesMappings', uad.n_category_mapping_id ) %}
											{% if _catMap %}
												{% set _catMapName = _catMap.n_path %}
											{% endif %}
										{%  %}
										#}
										{% set _catMapName = uad.Category().n_path %}
										
										
										{% set _ad_catMaps = uad.CategoriesMappings() %}
										{% set _ad_catMaps_text = '' %}
										
										{% if _ad_catMaps %}
											{% for catMap in _ad_catMaps %}
												{%set _ad_catMaps_text = _ad_catMaps_text ~ catMap.Publication().slug ~ ' - ' ~ catMap.n_path ~ '&#013;' %}
											{% endfor %}
										{% else %}
											{% set _ad_catMaps_text = 'ERROR NO PUBLICATION CATEGORIES FOUND' %}
										{% endif %}
										
										{# 13.1.2017 krivo je prikazivalo zbog " u tekstu opisa #}
										
										{% set _descr = str_replace ("<", " ", _descr) %}
										{% set _descr = str_replace (">", " ", _descr) %}
										{% set _descr = str_replace ("/", " ", _descr) %}
										{% set _descr = str_replace ('"', "''", _descr) %}
										{#{ error_log ('subUsersAds.volt ' ~ _descr) }#}
										
										{% set _title = uad.title %}
										{% set _title = str_replace ("<", " ", _title) %}
										{% set _title = str_replace (">", " ", _title) %}
										{% set _title = str_replace ("/", " ", _title) %}
										{% set _title = str_replace ('"', "''", _title) %}
										
										
										
										<option 
												class = "users_ads_options"
												onclick="onclick='function(); return false;' "
												{#
												ondblclick = "
													//alert(window.location.href );
													window.location.href = '/suva/ads2/edit/' + $(this).val();
													" 
												#}
												{#title = " {{ _descr }}"#}
												title = "PUNI TEKST OGLASA:&#013;{{ _descr }}&#013;&#013;KATEGORIJE PO PUBLIKACIJAMA:&#013;{{ _ad_catMaps_text }}"
												{%  if uad.n_is_display_ad %}
													ondblclick = "
													$('#tabSingleAd').attr('class', 'active');
													$('#tabUsersAds').removeAttr('class');
													$('#single_ad').attr('class', 'tab-pane active');
													$('#users_ads').attr('class', 'tab-pane');
														console.log('display');
														ads2RefreshAd( {
															adId : {{uad.id}}
															, userId : {{uad.user_id}}
															, isDisplayAd : true
															, isSubmitForm : false
															} );
													"
												{% else %}
													ondblclick = "
													$('#tabSingleAd').attr('class', 'active');
													$('#tabUsersAds').removeAttr('class');
													$('#single_ad').attr('class', 'tab-pane active');
													$('#users_ads').attr('class', 'tab-pane');
														console.log('basic');
														ads2RefreshAd( {
															adId : {{uad.id}}
															, userId : {{uad.user_id}}
															, isDisplayAd : false
															, isSubmitForm : true
															} );
													"
												{% endif %}
												style = "font-family:Courier; font-size:12px; cursor:pointer; border-bottom:1px solid #F0F0F0;   {% if uad.active == 0 %} background-color:#D7D3D3; {% else %} background-color:''; {% endif %}" 
												onmouseover = "this.style.background='#ffa6a6'"
												onmouseout = "{% if uad.active == 0 %} this.style.background='#D7D3D3'; {% else %} this.style.background=''; {% endif %}"
												value = "{{uad.id}}">
											{{ _model.txtPad( uad.id, 7) }}
											{#{ _model.txtPad( arrFreeAds[uad.id], 6) }#}
											{{ _model.txtPad( arrExtendableAds[uad.id]['online'], 4) }}
											{{ _model.txtPad( arrExtendableAds[uad.id]['offline'], 3) }}
											
											{{ _model.txtPad( _title, 25) }}
											{{ _model.txtPad( _descr, 50) }}
											{{ _model.txtPad( date("Y-m-d H:i:s", uad.modified_at), 19, 'datetime') }}
											{#{% if lpoi %}
												{{ _model.txtPad( (lpoi.Category() ? lpoi.Category().path : '') , 60) }}
											{% else %}
												{{ _model.txtPad( uad.Category().n_path, 60) }}
											{% endif %} #}		
											{{ _model.txtPad( _catMapName , 60) }}
										</option>
									{% endfor %}
								</select>
							</div>
						</div>
					</div>

				{% endif %}
			{{ endForm() }}
		</div>
		<div class="row">
			<div class="col-lg-1 col-md-1">
				<span>Selected: </span>
			</div>
			<div class="col-lg-1 col-md-1">
				<span class="selected_ads"></span>
			</div>
		</div>

	

