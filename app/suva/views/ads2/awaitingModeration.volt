{# Admin Ads awaiting moderation Listing/View #}


<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <h1 class="page-header">Ads awaiting moderation</h1>
        </div>
        {{ flashSession.output() }}
        <form class="form-inline" role="form">
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {%- for f in field_options -%}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {%- endfor -%}
                                <li class="divider"></li>
                                <li><a href="#all">All fields</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6 text-right">
                    <select id="payment_type" name="payment_type" class="form-control" title="Ad's payment type">
                        {%- for value, title in payment_type_options -%}
                            <option {% if value == payment_type %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    {{ filter_category_id_dropdown }}
                    <select id="order-by" name="order_by" class="form-control" title="Order by">
                        {%- for value, title in order_by_options -%}
                            <option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>
        {% if page.items|length %}
        <section>
            <div class="table table-striped">
                <div class="row table-header">
                    <div class="cell col-lg-1 col-md-2 col-sm-2 hidden-xs">Status</div>
                    <div class="col-lg-11 col-md-10 col-sm-10 hidden-xs">
                        <div class="row">
                            <div class="cell col-lg-10 col-md-10">Ad details</div>
                            <div class="cell col-lg-2 col-md-2">Product</div>
                        </div>
                    </div>
                </div>
                {% for ad in page.items %}
				{% set _ad = _model.Ad(ad['id']) %}
				{% for am in _ad.AdsMedia() %}
								{#ADSMEDIA :{{ am.media_id }}#}
							
								{%if am %}
										
									{% if am.Media() %}
										
										{#URL PIC :{{ am.Media().get_url()}}#}
									{% endif %}
								{% endif%}
				{% endfor %}
				
				
                <div class="row table-row">
                    <div class="cell col-lg-1 col-md-2 col-sm-2 hidden-xs status status-{{ ad['status_class'] }}">
                        <div class="hidden-xs">
                            <strong>{{ ad['status_text'] }}</strong>
                            <br /><span class="help {{ (ad['is_spam'] == '1' ? 'text-default' : ad['moderation_class']) }}" title="{{ ad['moderation_msg'] }}"><span class="fa fa-{{ ad['moderation_icon'] }} fa-fw"></span> Moderation</span>
                            {% if ad['reported'] %}
                            <br /><span class="help {{ (ad['is_spam'] == '1' ? 'text-default' : 'text-danger') }}" title="Reported ad"><span class="fa fa-exclamation-triangle fa-fw"></span> Reported ad</span>
                            {% endif %}
                        </div>
                    </div>
                    <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                        <div class="xs-status status-{{ ad['status_class'] }}">
                            <div class="classified-pic hidden-sm hidden-xs">
                                <div class="cell img text-center">{{ ad['thumb_pic'] }}</div>
                                <div class="clearfix"><!--IE--></div>
                            </div>
                            <div class="classified-info">
                                <div class="row">
                                    <div class="cell col-lg-10 col-md-10">
                                        <a href="{{ ad['frontend_url'] }}" target="_blank" title="Frontend view"><span class="glyphicon glyphicon-eye-open"></span></a>
                                        {% if auth.logged_in(['admin','support','moderator']) %}
                                        {{ linkTo(['admin/ads/edit/' ~ ad['id'], ad['title']|escape]) }}
                                        {% else %}
                                        <span>{{ ad['title']|escape }}</span>
                                        {% endif %}
                                        <small class="text-info">[ID: {{ ad['id'] }}]</small>{% if ad['import_id'] %} <small class="text-muted">[ImportID: {{ ad['import_id'] }}]</small>{% endif %}<br />
                                        <span class="text-muted">
                                            <small>
                                                {% set cat = tree[ad['category_id']] %}
                                                {% if auth.logged_in(['admin']) %}
                                                    {{ linkTo(['admin/categories/edit/' ~ ad['category_id'], '<span class="fa fa-folder fa-fw"></span>' ~ cat.path['text'], 'class': 'text-muted']) }}<br />
                                                {% else %}
                                                    <span class="text-muted"><span class="fa fa-folder fa-fw"></span>{{ cat.path['text'] }}</span><br />
                                                {% endif %}
                                                {% if ad['user_id'] %}
                                                    {% if ad['user_remark'] is defined and ad['user_remark'] %}
                                                        <span class="fa fa-warning fa-fw text-danger" data-toggle="tooltip" data-type="html" data-placement="right" title="{{ ad['user_remark']|nl2br }}"></span>
                                                    {% endif %}
                                                    {% if auth.logged_in(['admin','support']) %}
                                                        {{ linkTo(['admin/users/edit/' ~ ad['user_id'], '<span class="fa fa-' ~ ad['user_icon'] ~ ' fa-fw"></span>' ~ ad['user_username'], 'class': 'text-muted']) }}
                                                    {% else %}
                                                        <span class="text-muted"><span class="fa fa-{{ ad['user_icon'] }} fa-fw"></span>{{ ad['user_username'] }}</span>
                                                    {% endif %}
                                                    ·
                                                {% endif %}
                                                <span title="Publish date"><abbr title="Published at">P<span class="fa fa-clock-o"></span></abbr>: {{ ad['published_at'] }}</span> ·
                                                <span title="Created at"><abbr title="Created at">C<span class="fa fa-clock-o"></span></abbr>: {{ ad['created_at'] }}</span> ·
                                                {% if ad['modified_at'] -%}
                                                <span title="Modified at"><abbr title="Modified at">M<span class="fa fa-clock-o"></span></abbr>: {{ ad['modified_at'] }}</span>
                                                {% endif -%}
                                                {% if ad['sort_date'] -%}
                                                    <span title="Virtual publish date (sort)"><abbr title="Virtual publish date (sort)">S<span class="fa fa-clock-o"></span></abbr>: {{ ad['sort_date_formatted'] }}</span>
                                                {% endif -%}
                                            </small>
                                        </span>
                                    </div>
                                    <div class="cell col-lg-2 col-md-2">
                                        <small>
                                            <strong class="visible-xs">Products</strong>
                                            Online: {{ ad['online_product_id'] }}<br/>
                                            Offline: {{ ad['offline_product_id'] }}
                                        </small>
                                    </div>
                                    <div class="cell visible-xs text-right">
                                        <small>
                                            <span class="{{ ad['moderation_class'] }}"><span class="fa fa-{{ ad['moderation_icon'] }} fa-fw"></span> {{ ad['moderation_msg'] }}</span>
                                            {% if ad['reported'] %}
                                            · <span class="text-danger" title="Reported ad"><span class="fa fa-exclamation-triangle fa-fw"></span> Reported ad</span>
                                            {% endif %}
                                        </small>
                                    </div>
                                    <div class="cell hover-options col-lg-12 col-md-12">
                                        <div class="options">
                                            {{ linkTo(['admin/ads/edit/' ~ ad['id'], 'Edit']) }}
                                            {#
                                            · {{ linkTo(['admin/ads/activeToggle/' ~ ad['id'], (ad['active'] ? 'Deactivate' : 'Activate')]) }}
                                            · {{ linkTo(['admin/ads/spamToggle/' ~ ad['id'], (ad['is_spam'] ? 'Not spam' : 'Mark as spam')]) }}
                                            #}
                                            {% if auth.logged_in(['admin', 'support']) %}
                                                {% if ad['soft_delete'] == 0 %}
                                                · {{ linkTo(['admin/ads/softDelete/' ~ ad['id'], '<span class="fa fa-trash-o text-danger fa-fw" title="SoftDelete"></span>', 'class': 'text-danger' ]) }}
                                                {% elseif auth.logged_in('admin') %}
                                                · {{ linkTo(['admin/ads/delete/' ~ ad['id'], '<span class="fa fa-trash-o fa-fw"></span>Delete from DB', 'class': 'text-danger' ]) }}
                                                {% endif %}
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {% endfor %}
            </div>
        </section>
        {% else %}
        <div class="no-results alert alert-success">
            <p class="text-center">Nothing matched your search query</p>
        </div>
        {% endif %}
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
