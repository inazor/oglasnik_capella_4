
{% set _url = '/suva/ads2/subUsersFreeAds/' ~ user.id  %}
{% set _is_ajax_submit = true %}
{% set _window_id = 'subUsersFreeAds' %}


<!-- ZA AŽURIRANJE BROJA ELEMENATA NA GORNJEM PROZORU -->
<script>
	{% set count_elements = 0 %}
	{% for ao in user.Orders('n_status = 2')  %}
		{% for oi in ao.OrdersItems() %}
			{% set count_elements +=1 %}
		{% endfor %}
	{% endfor %}
	{% if count_elements > 0 %}
		$( '#count_items_users_free_ads' ).html( '({{ count_elements }}) ');
	{% else %}
		$( '#count_items_users_free_ads' ).html('');
	{% endif %}
</script>



{{ form(NULL, 'id': '_form_' ~ _window_id, 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
	{{ hiddenField('next') }}
	{{ hiddenField('_csrftoken') }}
{#
	{% if ad is defined and ad.id is defined %}
		{{ hiddenField(['ad_id', 'value': ad.id]) }}
	{% endif %}
	{{ hiddenField(['user_id', 'value': ad.user_id]) }}
	<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
	<input type="hidden" name="n_first_uploaded_photo_id" value="{{ad.n_first_uploaded_photo_id}}"/>
	<input type="hidden" name="moderation" value="ok"/>
#}
	<input type="hidden" id="_context_active_tab_subUsersFreeAds" name = "_context[_active_tab]" value="{{ ( array_key_exists('_active_tab' , _context) ? _context['_active_tab'] : '' ) }}"></input>
	
	{% set user = _model.User( ( _context['selected_user_id'] > 0 ? _context['selected_user_id'] : - 1 ) ) %}
	
	<!-- DODANO KAO PARAMETAR AKCIJE -->
	<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subUsersFreeAds" value = "" ></input>
	<input type="hidden" name="_context[clicked_action_params][user_id]" value="{{ user.id }}"/> 
	<input type="hidden" name="_context[clicked_action_params][logged_user_id]" value="{{ auth.get_user().id }}"/> 


	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					
					{% for action_id in _model.getAllowedActionIds( auth.get_user().id, 2, 'orders-items' ) %}
							{% set action = _model.Action( action_id ) %}
							{{ action.buttonHtml_submitViaActionInputOnForm( [
								'action_input_id': '_clicked_action_' ~ _window_id
								,'is_ajax' : _is_ajax_submit
								,'action_url' : _url
								,'action_window_id' : _window_id
							] ) }}
					{% endfor  %}
					{#% for action_id in _model.getAllowedActionIds( auth.get_user().id, 2, 'orders' ) %}
							{% set action = _model.Action( action_id ) %}
							{{ action.buttonHtml_submitViaActionInputOnForm( [
								'action_input_id': '_clicked_action_' ~ _window_id
								,'is_ajax' : _is_ajax_submit
								,'action_url' : _url
								,'action_window_id' : _window_id
							] ) }}
					{% endfor  %#}
					
					
					<h3 class="panel-title" style="font-family:Courier; font-size:12px;" >
						{{ str_replace("+","&nbsp;","ITEM+++OBJ.++AD+++++DATE+FROM+++DATE+TO+++QTY+++++PRICE+PRODUCT++++++++++++++++++++++++ID++++CATEGORY+++++++++++++++++++++++NASLOV OGLASA")}}
					</h3>

				</div>
				<div class="panel-body">
					<select id="users_cart_tab" style="display:block !important; width:100%; height:300px;" name="_context[clicked_action_params][orders_items][]" multiple>
						{#% for oi in user.Orders("n_status = 2").OrdersItems(['order': 'id desc'])  %#}{# ovo gore ne radi #}

						{% for ao in user.Orders( [ 'n_status = 2' , 'order': 'id desc' ] ) %}
							{% for oi in ao.OrdersItems(['order': 'id desc']) %}														
							
							{#{ dump (oi.Category()) }#}
							
							{% set _ad = oi.Ad() %}
							{% set _publication = oi.Publication() %}
							{% set _category = oi.Category() %}
							{% set _product = oi.Product() %}
							{#% set _cat_map_id = _ad.getCategoryMappingForPublication( oi.n_publications_id ) %#}
							{% set _cat_map_id = ( oi.n_categories_mappings_id ? oi.n_categories_mappings_id : _ad.getCategoryMappingForPublication( oi.n_publications_id ) ) %}
							
							{% set _cat_map = ( _cat_map_id > 0 ? _model.CategoryMapping ( _cat_map_id ) : null ) %}
							
							{% set _product_name = ( _product ? _product.name : '')  %}
							{% set _category_id = _cat_map ? _cat_map.id : ( _category ? _category.id : '' ) %}
							{% set _category_path = _cat_map ? _cat_map.n_path : ( _category ? _category.n_path : '' ) %}
							{% set _ad_description = ( _ad ? _ad.description : '') %}
							{% set _publication_slug = _publication ? _publication.slug : ''  %}
							

							
							<option 
									price = "{{oi.total}}"
									id = "{{oi.id}}"
									onmouseover="this.style.backgroundColor='#ffa6a6'"
									onmouseout="this.style.backgroundColor=''"
									style="
										font-family:Courier; font-size:12px; 
										{% if oi.n_price >0  %} font-weight:bold; {% endif %}
										"
									ondblclick = "window.open ('/suva/orders-items/edit/' + $(this).val(),'_blank');" 
									target="_blank";
									value="{{oi.id}}"
								>
								
								<!-- public function txtPad ( $pValue, $pLength, $pType = 'text', $pAlign = null, $pDecimals = null ) -->
								{{ _model.txtPad( oi.id , 6 ) }}
								{{ _model.txtPad( _publication_slug , 4 ) }}
								{{ _model.txtPad( oi.ad_id , 6 ) }}
								
								{{ _model.txtPad( oi.n_first_published_at , 10, 'date' ) }}
								{{ _model.txtPad( oi.n_expires_at , 10, 'date' ) }}
								
								{{ _model.txtPad( oi.qty , 3, 'numeric', null, 0 ) }}
								{{ _model.txtPad( oi.n_total , 10, 'currency' ) }}
								{{ _model.txtPad( ( _product_name ) , 30 ) }}
								
								{{ _model.txtPad( _category_id , 4 ) }}
								{{ _model.txtPad( _category_path ~ '  ' , 30, 'text', 'right' ) }}
								{{ _model.txtPad( _ad_description , 100 ) }}
							</option>
							{% endfor %}
						{% endfor %}

					</select>
				</div>
			</div>
		</div>
	</div>
{{ endForm() }}
