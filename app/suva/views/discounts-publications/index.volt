{# Admin User Listing/View #}
<div class="row mb0">
    <div class="col-lg-12 col-md-12">
        <div>
            {% if auth.logged_in(['admin','support']) %}
            <div class="pull-right">
                
            </div>
            {% endif %}
            <h1 class="page-header">Discounts on publications</h1>
        </div>
		{#{ partial("partials/blkFilterSort",['field_options':field_options, 'mode_options':mode_options, 'order_by_options':order_by_options, 'dir_options':dir_options, 'limit_options':limit_options ] ) }#}
        <br>
		{{ linkTo(['suva/discounts-publications/crud', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
        {{ partial("partials/blkFlashSession") }}
			
        <table class="table table-striped table-hover table-condensed">
			
		
			
			{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				 ['title':'Options'									,'width':150	,'show_navigation' : true ] 
				,['title':'ID'			,'name':'id'				,'width':100		,'search_ctl':'ctlNumeric'	]
				,['title':'Discount'	,'name':'discounts_id'		,'width':300	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Discounts()	]] 
				,['title':'Publication'	,'name':'publications_id'	,'width':300	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Publications() ]]  
				,['title':'Is active'	,'name':'is_active'			,'width':100		,'search_ctl':'ctlNumeric'	]
				
			] ]) }}
			
			
            {% for id in ids %}
				{% set entity = _model.getOne('DiscountsPublications',id) %}
				{% set publication = _model.Publication(entity.publications_id) %}
				{% set discount = _model.Discount(entity.discounts_id) %}
				{% set discount_name = discount ? discount.name : '' %}
				{% set publication_name = publication ? publication.name : '' %}
            <tr>
				<td class="text-left">
                    {{ linkTo(['suva/discounts-publications/crud/' ~ entity.id, '<span class="fa fa-edit fa-fw"></span>Edit']) }}

                    {{ linkTo(['suva/discounts-publications/delete/' ~ entity.id, '<span class="fa fa-edit fa-fw"></span>Delete']) }}
                </td>
				<td>{{ entity.id }}</td>
				<td>{{ discount_name }} </td>
				<td>{{ publication_name }}</td>
				<td>
					<div class="col-lg-3 col-md-3">
					{% set field = 'is_active' %} {% set value = entity.is_active %}{% set title = '' %}
					<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
						<input disabled="disabled" type="checkbox" name="{{ field }}" id="{{ field }}" value="1" {% if value == 1  %} checked{% endif %} maxlength="2" />
						{% if errors is defined and errors.filter(field) %}
						<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
						{% endif %}
					</div>
					</div>
				</td>

                
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are no discounts  on publications here!</td></tr>
            {% endfor %}
        </table>
        {#% if pagination_links %}{{ pagination_links }}{% endif %#}
    </div>
</div>