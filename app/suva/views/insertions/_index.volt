{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre>#}


{% if _context['default_filter']['issues_id'] is defined %}
	{% set issue = _model.Issue( _context['default_filter']['issues_id'] ) %}
	
{% else %}
	
{% endif %}
<!-- <style>
td {
border: 1px solid black;

}

</style> -->


<div class="row">
    <div class="col-lg-12 col-md-12">
       
		<div>
			
			<h1 class="page-header">
				Insertions
				{% if issue is defined %}
					for Issue nr.<i>{{ issue.id }} / {{ issue.date_published }}</i>
				{% else %}
	
				{% endif %}
			</h1>
			
        </div>
		
        <br>
        {{ partial("partials/blkFlashSession") }}
        <table style="width:950 !important;" class="table table-striped table-hover table-condensed">

		{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				 ['title':'Options'													,'width': 150	,'show_navigation' : true ] 
				,['title':'ID'						,'name':'id'					,'width': 100		,'search_ctl':'ctlText'		] 
				,['title':'Issue'					,'name':'issues_id'				,'width': 300	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list' : _model.Issues( ) , 'option_fields':['id', 'date_published', 'status', 'publications_id'] ]]  

				,['title':'Order Item'				,'name':'orders_items_id'		,'width': 200	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.OrdersItems(  ) , 'option_fields':['id', 'ad_id'] ]]  

				,['title':'Published status'		,'name':'published_status'		,'width': 100	,'search_ctl':'ctlText']]  
				
				,['title':'Is Active'			,'name':'is_active'					,'width': 100		] 
				]) }}

			
			{% for id in ids %}
				{% set entity = _model.getOne('Insertions',id) %}
				
				<!-- editiranje preko ajaxa -->
				{% set _context['current_entity_table_name'] = 'n_insertions' %}
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['ajax_edit'] = true %}
				 
				<tr>
					<td>{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'insertions'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>
						{% set issue = entity.Issue() %}
						{{ issue.id ~ ' - ' ~ issue.status ~ ' - ' ~ issue.date_published }}
					</td>
					<td>
						{% set oi = entity.OrderItem() %}
						{{ oi.id ~ ' - ' ~ oi.ad_id  }}
					</td>
					<td>
						{{ entity.published_status }}
					</td>
					<td>
						{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'field':'is_active', '_context':_context ] ) }}
					</td>
					
				</tr>
            {% else %}
				<tr><td colspan="6">Currently, there are no Insertions!</td></tr>
            {% endfor %}
        </table>
    </div>
</div>
