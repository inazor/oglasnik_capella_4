<!-- TRENUTNO JE ZAMIJENJENO KOPIRANOM FUNKCIONALNOSCU UPLOADANJA SLIKA 							 -->
							<!-- DISPLAY AD BLOK -->
							{% if entity.id is defined %}
								<div class="row">
									<div class="col-md-3 col-lg-3">
										<input name="imageUpload" id="imageUpload" type="file" onchange="loadFile(event)"></input>
									</div>
									<div class="col-md-3 col-lg-3">
										<span 
											class="image_submit btn btn-primary"
											value="Image submit" 
											onclick="
												if($('#imageUpload').prop('files')[0] > ''){
													var file_data = $('#imageUpload').prop('files')[0];
													var form_data = new FormData();
													form_data.append('file', file_data); 
													form_data.append('ad_id', {{ entity.id }});  
													$.ajax({
														type:'POST',
														url: '/suva/ajax/imageUpload',
														data:form_data,
														cache:false,
														contentType: false,
														processData: false,
														success:function(data){
															$('#uploaded_preview_append').children().remove()															
															$('#uploaded_preview_append').append($('#current_preview').clone());
															console.log($('#current_preview').attr('src'));
															alert('data');
														}						
													});
												}else{
													alert('Odaberite sliku')
												}
											">Image submit								
										</span>
									</div>						
								</div>
								<br/>
								<div class="row">
									<div class="col-md-3 col-lg-3">
										<span>Current preview</span>
									</div>
									<div class="col-md-3 col-lg-3">
										<span>Uploaded preview</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3 col-lg-3">
										<span
											id="remove_display_photo"
											onclick="$('#imageUpload').replaceWith($('#imageUpload').val('').clone(true))
													 $('#current_preview').attr('src','')
													 $('#current_preview').css('width', '')"
											style="cursor:pointer;">X
										</span>
									</div>
									<div class="col-md-3 col-lg-3">
										<span
											onclick="
													var form_data = new FormData();
													form_data.append('ad_id', {{ entity.id }}); 
													if(confirm('Are you sure you want to delete this picture from the database?')){
														$.ajax({
															type:'POST',
															url: '/suva/ajax/removeUploadedImage',
															data:form_data,
															cache:false,
															contentType: false,
															processData: false,
															success:function(data){															
															$('#uploaded_preview').remove()
															$('#uploaded_preview_append').find('#current_preview').remove();

															}						
														})
													}"
											class="remove_uploaded_photo" 
											id="delete_uploaded_photo"
											>X
										</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3 col-lg-3">
										<img id="current_preview"/>
									</div>
									<div class="col-md-3 col-lg-3" id="uploaded_preview_append">
										{% if entity.n_picture_path != '' %}
											<img src="/{{entity.n_picture_path}}" id="uploaded_preview"/>
										{% endif %}
									</div>
								</div>																			
								<br/>
							{% else %}
								<div class="row">
									<div class="col-md-3 col-lg-3">
										<input name="imageUpload" id="imageUpload" type="file" onchange="loadFile(event)"></input>
									</div>
									<div class="col-md-3 col-lg-3">
										<span 
											class="image_submit btn btn-primary"
											value="Image submit" 
											onclick="
												if($('#imageUpload').prop('files')[0] > ''){
													var file_data = $('#imageUpload').prop('files')[0];
													var form_data = new FormData();
													form_data.append('file', file_data);  
													$.ajax({
														type:'POST',
														url: '/suva/ajax/imageUpload',
														data:form_data,
														cache:false,
														contentType: false,
														processData: false,
														success:function(data){
															alert('image uploaded')
														}						
													});
												}else{
													alert('Odaberite sliku')
												}
											">Image submit								
										</span>
									</div>						
								</div>
								<br/>
								<div class="row">
									<div class="col-md-3 col-lg-3">
										<span>Image preview</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3 col-lg-3">
										<span
											id="remove_display_photo"
											onclick="$('#imageUpload').replaceWith($('#imageUpload').val('').clone(true))
													 $('#current_preview').attr('src','')
													 $('#current_preview').hide()"
											style="color:red; cursor:pointer;">X
										</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3 col-lg-3 current_image_preview">
										<img id="current_preview" style="width:100%;"/>
									</div>
								</div>
							{% endif %}
<script>
	var loadFile = function(event) {
		var current_preview = document.getElementById('current_preview');
		current_preview.style.display = "inline"
		current_preview.style.width = "100%"
		current_preview.src = URL.createObjectURL(event.target.files[0]);			
	}
</script>