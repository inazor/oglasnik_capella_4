{# Admin User Listing/View #}
<div class="row">
   <div class="col-lg-12 col-md-12">
        <div>
            {% if auth.logged_in(['admin','support']) %}
            <div class="pull-right">
                {{ linkTo(['admin/order-status-transitions/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            {% endif %}
            <h1 class="page-header">{{ entity_name }}</h1>
        </div>
        <br>
        {{ partial("partials/blkFlashSession") }}
        <table class="table table-striped table-hover table-condensed">
            <tr>
                <th width="60">ID</th>
                <th width="150">Name</th>
                <th width="150">Order status id</th>
                <th>Next order status id</th>
				<th>Action id</th>
				<th>Active</th>
            </tr>
            {% for entity in page.items %}
            <tr>
                <td>{{ entity.id }}</td>
                <td>{{ entity.name }}</td>
				<td>{{ entity.order_status_id }} {{ order_statuses[entity.order_status_id]['name'] }}</td>
				<td>{{ entity.next_order_status_id }} {{ order_statuses[entity.next_order_status_id]['name'] }}</td>
				<td>{{ actions[entity.action_id]['name'] }}</td>
				<td>{{ entity.is_active }}</td>
                <td class="text-right">
                    {{ linkTo(['admin/order-status-transitions/edit/' ~ entity.id, '<span class="fa fa-edit fa-fw"></span>Edit']) }}

                    {{ linkTo(['admin/order-status-transitions/delete/' ~ entity.id, '<span class="fa fa-edit fa-fw"></span>Delete']) }}
                </td>
				
				
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are none here!</td></tr>
            {% endfor %}
        </table>
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>