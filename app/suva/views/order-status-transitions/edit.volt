{# Admin User Edit View #}
<div class="row">
   <div class="col-lg-12 col-md-12">
        <div>
            <h1 class="page-header">{{entity_name}} <small>{{ form_title_long }}</small></h1>
        </div>

        {{ partial("partials/blkFlashSession") }}

        {% if auth.logged_in(['admin', 'support']) %}
        {{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('next') }}
            <input type="hidden" name="action" value="{{form_action}}">
        {% endif %}
	
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {% if entity.id is defined %}
                        <div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
                        {% endif %}
                        <h3 class="panel-title">{{ entity_name }} details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                {% set field = 'name' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Name</label>
                                    <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{entity.name | default('')}}" maxlength="24" />
                                {% if errors is defined and errors.filter(field) %}
                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                                </div>
                            </div>
							<div class="col-lg-2 col-md-2">
                                {% set field = 'order_status_id' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Order status</label>
                                    <select id="{{field}}" name="{{field}}" class="form-control" title="Order status">
			                             {%- for order_status in order_statuses -%}
			                            <option {% if order_status['id'] == entity.order_status_id %} selected="selected" {% endif %}value="{{ order_status['id'] }}">
											{{ order_status['id'] }} {{ order_status['name'] }}
										</option>
			                             {%- endfor -%}
                                 	</select>
	                                {% if errors is defined and errors.filter(field) %}
	                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
	                                {% endif %}
                                </div>
                            </div>

							<div class="col-lg-2 col-md-2">
                                {% set field = 'next_order_status_id' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Next Order status</label>
                                    <select id="{{field}}" name="{{field}}" class="form-control" title="Next Order status">
			                             {%- for order_status in order_statuses -%}
			                            <option {% if order_status['id'] == entity.next_order_status_id %} selected="selected" {% endif %}value="{{ order_status['id'] }}">
											{{ order_status['id'] }} {{ order_status['name'] }}
										</option>
			                             {%- endfor -%}
                                 	</select>
	                                {% if errors is defined and errors.filter(field) %}
	                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
	                                {% endif %}
                                </div>
                            </div>
							
							<div class="col-lg-2 col-md-2">
                                {% set field = 'action_id' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Action</label>
                                    <select id="{{field}}" name="{{field}}" class="form-control" title="Action">
			                             {%- for action in actions -%}
			                            <option {% if action['id'] == entity.action_id %} selected="selected" {% endif %}value="{{ action['id'] }}">
											{{ action['id'] }} {{ action['name'] }}
										</option>
			                             {%- endfor -%}
                                 	</select>
	                                {% if errors is defined and errors.filter(field) %}
	                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
	                                {% endif %}
                                </div>
							</div>
							<div class="col-lg-2 col-md-2">
                                {% set field = 'is_active' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Active</label>
                                    <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{entity.is_active | default('')}}" maxlength="24" />
                                {% if errors is defined and errors.filter(field) %}
                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                                </div>
                            </div>
 

                    </div>
                   
                </div>
            </div>
        </div>
        {% if auth.logged_in(['admin', 'support']) %}
        <div>
            <button class="btn btn-primary" type="submit" name="save">Save</button>
            <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
            <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
        </div>
        {{ endForm() }}
        {% else %}
        <div>
            <button class="btn btn-default" onclick="history.go(-1);">Back</button>
        </div>
        {% endif %}
    </div>
</div>
