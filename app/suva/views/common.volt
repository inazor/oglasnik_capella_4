{# Admin/Backend Template View #}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{ getTitle() }}
    {{ this.assets.outputCss() }}
    {% if head_scripts is defined and count(head_scripts) %}
        {%- for script in head_scripts -%}
            <script type="text/javascript">{{ script }}</script>
        {%- endfor -%}
    {%- endif %}
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{- linkTo(['admin', 'Oglasnik.hr Administracija', 'class' : 'navbar-brand']) -}}
            </div>

            <ul class="nav navbar-top-links navbar-right">
                {% if auth.logged_in() %}
                <li>{{ linkTo('admin/cart/'~ auth.get_user().id , '<span class="fa fa-shopping-cart"></span> Košarica &nbsp;<span class="badge">'  ~ orders_count ~  '</span></span>') }}
                <li class="dropdown">
                    {{ linkTo([ '#', 'class' : 'dropdown-toggle', 'data-toggle' : 'dropdown', '<span class="fa fa-user fa-fw"></span> ' ~ auth.get_user().username ~ ' <span class="fa fa-caret-down fa-fw"></span>' ]) }}
                    <ul class="dropdown-menu dropdown-user">
                        <li class="dropdown-header">{{ auth.get_user().email }}</li>
                        <li class="divider"></li>
                        <li>{{ linkTo('user', '<span class="fa fa-user fa-fw"></span> Account') }}</li>
                        <li>{{ linkTo(NULL, '<span class="fa fa-home fa-fw"></span> Frontpage') }}</li>
                        <li class="divider"></li>
                        <li>{{ linkTo('user/signout', '<span class="fa fa-sign-out fa-fw"></span> Logout') }}</li>
                    </ul>
                </li>
                {% endif%}
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        {#
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <span class="fa fa-search fa-fw"></span>
                                    </button>
                                </span>
                            </div>
                        </li>
                        #}
                        <li>{{ linkTo(['admin', '<span class="fa fa-dashboard fa-fw"></span> Dashboard', 'class': controller_name == 'index' ? 'active' : '']) }}</li>

                        {% if auth.logged_in(['admin','support']) %}
                        <li{% if controller_group == 'users' %} class="active"{% endif %}>
                            {{ linkTo('#', '<span class="fa fa-users fa-fw"></span> Users<span class="fa arrow fa-fw"></span>') }}
                            <ul class="nav nav-second-level">
                        {% endif %}
                                {% if auth.logged_in(['admin','support','finance','moderator']) %}
                                <li>{{ linkTo(['admin/users', '<span class="fa fa-user fa-fw"></span> Users', 'class': controller_name == 'users' ? 'active' : '']) }}</li>
                                {% endif %}
                                {% if auth.logged_in(['admin','support']) %}
                                <li>{{ linkTo(['admin/shops', '<span class="fa fa-shopping-cart fa-fw"></span> Shops', 'class': controller_name == 'shops' ? 'active' : '']) }}</li>
                                {% endif %}
                        {% if auth.logged_in(['admin','support']) %}
                            </ul>
                        </li>
                        {% endif %}

                        {% if auth.logged_in(['admin']) %}
                        <li{% if controller_group == 'site-structure-management' %} class="active"{% endif %}>
                            {{ linkTo('#', '<span class="fa fa-sitemap fa-fw"></span> Site structure management<span class="fa arrow fa-fw"></span>') }}
                            <ul class="nav nav-second-level">
                                <li{% if controller_group == 'site-structure-management' %} class="active"{% endif %}>
                                    {{ linkTo('#', '<span class="fa fa-folder fa-fw"></span> Categories<span class="fa arrow fa-fw"></span>') }}
                                    <ul class="nav nav-third-level">
                                        <li>{{ linkTo(['admin/categories', '<span class="fa fa-list fa-fw"></span> List all', 'class': controller_name == 'categories' ? 'active' : '']) }}</li>
                                        <li>{{ linkTo(['admin/sections', '<span class="fa fa-angle-double-right fa-fw"></span> Manage Sections', 'class': controller_name == 'sections' ? 'active' : '']) }}</li>
                                        <li>{{ linkTo(['admin/category-parameterization', '<span class="fa fa-angle-double-right fa-fw"></span> Manage Parameters', 'class': controller_name == 'category-parameterization' ? 'active' : '']) }}</li>
                                        <li>{{ linkTo(['admin/products-categories', '<span class="fa fa-angle-double-right fa-fw"></span> Manage Products', 'class': controller_name == 'products-categories' ? 'active' : '']) }}</li>
                                    </ul>
                                </li>
                                <li>{{ linkTo(['admin/parameters', '<span class="fa fa-cube fa-fw"></span> Parameters', 'class': controller_name == 'parameters' ? 'active' : '']) }}</li>
                                <li>{{ linkTo(['admin/dictionaries', '<span class="fa fa-book fa-fw"></span> Dictionaries', 'class': controller_name == 'dictionaries' ? 'active' : '']) }}</li>
                            </ul>
                        </li>
                        {% endif %}

                        {% if auth.logged_in(['admin','support','moderator','finance']) %}
                        <li{% if controller_group == 'ads' %} class="active"{% endif %}>
                            {{ linkTo('#', '<span class="fa fa-file fa-fw"></span> Ads<span class="fa arrow fa-fw"></span>') }}
                            <ul class="nav nav-second-level">
                                <li>{{ linkTo(['admin/ads', '<span class="fa fa-file fa-fw"></span> All ads', 'class': controller_action == 'index' ? 'active' : '']) }}</li>
                                <li>{{ linkTo(['admin/ads/awaiting-moderation', '<span class="fa fa-clock-o fa-fw"></span> Awaiting moderation', 'class': controller_action == 'awaitingModeration' ? 'active' : '']) }}</li>
                            </ul>
                        </li>
                        {% endif %}

                        {% if auth.logged_in(['admin','finance','support']) %}
                        <li>{{ linkTo(['admin/orders', '<span class="fa fa-credit-card fa-fw"></span> Orders', 'class': controller_name == 'orders' ? 'active' : '']) }}</li>
                        {% endif %}
												
						{% if auth.logged_in(['admin','finance','support']) %}
                        
                        <li{% if controller_group == 'fakture' %} class="active"{% endif %}>
                             {{ linkTo('#', '<span class="fa fa-file fa-fw"></span> Fakture<span class="fa arrow fa-fw"></span>') }}
                            <ul class="nav nav-second-level">
                                
                                    {% for status in n_svi_options %}
                                     <li class="bg-success"> 
                                     {{ linkTo(['admin/fakture?field=&mode=fuzzy-end&q=&n_status=' ~ status['id'] ~ '&order_by=created_at&dir=desc&limit=20', '<span class="fa fa-file fa-fw"></span> ' ~ status['desc'], 'class': controller_action == 'fakture' ? 'active' : '']) }}</li>
                                    {% endfor  %}
                            </ul>
                        </li>
                        {% endif %}

                        {% if auth.logged_in(['admin','content']) %}
                        <li>{{ linkTo(['admin/media', '<span class="fa fa-th fa-fw"></span> Media', 'class': controller_name == 'media' ? 'active' : '']) }}</li>
                        {% endif %}

                        {% if auth.logged_in('admin') %}
                        <li>{{ linkTo(['admin/image-styles', '<span class="fa fa-cog fa-fw"></span> Image styles', 'class': controller_name == 'image-styles' ? 'active' : '']) }}</li>
                        {% endif %}

                        {% if auth.logged_in(['admin','content']) %}
                        <li{% if controller_group == 'cms' %} class="active"{% endif %}>
                            {{ linkTo('#', '<span class="fa fa-newspaper-o fa-fw"></span> CMS<span class="fa arrow fa-fw"></span>') }}
                            <ul class="nav nav-second-level">
                                <li>{{ linkTo(['admin/cms-categories', '<span class="fa fa-folder fa-fw"></span> Categories', 'class': controller_name == 'cms-categories' ? 'active' : '']) }}</li>
                                <li>{{ linkTo(['admin/cms-articles', '<span class="fa fa-pencil fa-fw"></span> Articles', 'class': controller_name == 'cms-articles' ? 'active' : '']) }}</li>
                            </ul>
                        </li>
                        {% endif %}

                        {% if auth.logged_in('admin') %}
                        <li>{{ linkTo(['admin/export2avus', '<span class="fa fa-share-square-o fa-fw"></span> Export2Avus', 'class': controller_name == 'export2avus' ? 'active' : '']) }}</li>
                        <li>{{ linkTo(['admin/notices', '<span class="fa fa-envelope fa-fw"></span> Company notices', 'class': controller_name == 'notices' ? 'active' : '']) }}</li>
                        <li>{{ linkTo(['admin/email-templates', '<span class="fa fa-envelope-square fa-fw"></span> Email Templates', 'class': controller_name == 'email-templates' ? 'active' : '']) }}</li>
                        <li>{{ linkTo(['admin/audit', '<span class="fa fa-history fa-fw"></span> Audit Log', 'class': controller_name == 'audit' ? 'active' : '']) }}</li>
                        {% endif %}
                    </ul>
                    <div class="text-center" style="padding:15px;"><small>&copy; {{ date('Y') }} <span class="text-muted"> | {{ version() }}</span></small></div>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            {{ content() }}
        </div>

    </div>

    {{ this.assets.outputJs() }}

    {% if scripts is defined AND count(scripts) %}
        {% for script_id, script in scripts %}
            <script type="text/javascript"{% if script_id %} id="{{ script_id }}"{% endif %}>
                {{ script }}
            </script>
        {% endfor %}
    {% endif %}

</body>
</html>
