<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">{{ _m.t('XML Layouts') }}</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'layoutss'] ) }}
			{{ partial("partials/blkFlashSession") }}
			<table class="table table-striped table-hover table-condensed">
				{{ partial("partials/blkTableSort",['fields':[ 
				['title':	 _m.t('Options')								,'width':150	]
				,['title':	 _m.t('ID')				,'name':'id'			,'width':100		,'search_ctl':'ctlNumeric'	]
				,['title':	 _m.t('Name')			,'name':'name'			,'width':150	,'search_ctl':'ctlText'	] 
				,['title': _m.t('Description')		,'name':'description'	,'width':100	,'search_ctl':'ctlText'	] 
				,['title':	 _m.t('XML')			,'name':'xml'			,'width':400	,'search_ctl':'ctlText' ] 
				,['title':	 _m.t('Variables')		,'name':'variables'		,'width':150	,'search_ctl':'ctlText'	] 
				,['title': _m.t('Is active')		,'name':'is_active'		,'width':100	,'search_ctl':'ctlNumeric']
				
			] ]) }}

            {% for id in ids %}
				{% set entity = _model.getOne('Layoutss', id) %}
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['ajax_edit'] = true %}
				{#{ print_r(entity.toArray(),true) }#}
				<tr>
					<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'layoutss'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>{{ entity.name }}</td>
					<td>{{ entity.description }}</td>
					<td>{{ partial("partials/ctlTextArea",[
							'value': entity.xml, 
							'field': 'xml',
							'readonly': true,
							'_context': _context 
						] ) }}</td>
					<td>{{ entity.variables }}</td>
					<td>
						{{ partial("partials/ctlCheckbox",[
							'value': entity.is_active, 
							'field': 'is_active',
							'readonly': true,
							'_context': _context 
						] ) }}
					</td>
					
				</tr>
            {% else %}
            <tr><td colspan="6">{{ _m.t('No layouts found !') }}</td></tr>
            {% endfor %}
        </table>

    </div>
</div>
