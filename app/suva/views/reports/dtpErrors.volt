<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"/>


<h1> DTP ERRORS REPORT</h1>

{% set issue = _model.Issue(_context['xml_report_issue_id']) %}
<h4>Issue: {{ issue.id }}</h4>
<h4>Date: {{ issue.date_published }}</h4>


<table>
	<tr>
		<th width=70 >ID</th>
		<th width=40 >CAT .ID</th>
		<th width=40 >FWD CAT. ID</th>
		<th width=400 >Description</th>
		<!-- <th width=100 >Picture Uploaded</th> -->
		<th width=100 >Picture To Process</th>
		<th width=100 >Picture Processed</th>
		<th width=100 >Picture Required</th>
		<th width=100 >Picture Exists</th>
		
		
	</tr>
	
	{{ ksort(ids) }}
	
	{% for id in ids %}
		{% set ad = _model.Ad(id) %}
		{% set user = ad.User() %}
		{#% set is_originals = ad.has_attached_picture('repository/dtp/originals') %#}
		{% set is_to_process = ad.has_attached_picture('repository/dtp/to_process') %}
		{% set is_processed = ad.has_attached_picture('repository/dtp/processed') %}
		
		{% set pubcat = _model.PublicationsCategories(" publication_id = " ~ issue.publications_id ~ " and category_id = " ~ ad.category_id) %}
		{% set forward_id = ad.category_id %}
		{% if pubcat.id > 0 %}
			{% set forward_id = pubcat.forward_category_id %}
		{% endif %}

		<tr style="border:1px solid black;">
			<td>{{ ad.id }}</td>
			<td>{{ ad.category_id }}</td>
			<td>{{ forward_id }}</td>
			<td>{{ ad.description }}</td>
			{#<td>{% if is_originals === true %}YES{% else %}NO{% endif %}</td>#}
			<td>{% if is_to_process === true %}YES{% else %}NO{% endif %}</td>
			<td>{% if is_processed === true %}YES{% else %}NO{% endif %}</td>
		</tr>
	{% endfor %}
</table>