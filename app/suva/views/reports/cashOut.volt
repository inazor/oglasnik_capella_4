{#
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"/>
#}
<div class="page" style="font:12px arial, sans-serif;  margin:0;" >

{#CONTEXT<pre>{{ print_r( _context, true ) }}</pre> #}

<!-- OVDE REPORT -->

	{% if _model.n_validate(_context['cashout_report_fiscal_location_id'], 'id')  %}
		{% set fl = _model.FiscalLocation(_context['cashout_report_fiscal_location_id']) %}
	{% endif %}
	{% if _model.n_validate(_context['cashout_report_operator_id'], 'id')  %}
		{% set operator = _model.User(_context['cashout_report_operator_id']) %}
	{% endif %}
	{% if _model.n_validate(_context['cashout_report_date'], 'date')  %}
		{% set report_date = _model.n_interpret_value_from_request(_context['cashout_report_date'], 'date') %}
	{% endif %}
	 

	{# parametri za upit #}
	{% set txtUpit = " 1=1 and n_status in (4,5,9)  "	%}
	{% if fl is defined %} {% set txtUpit = txtUpit ~ " and n_fiscal_location_id = " ~ fl.id 	%}{% endif %}
	{% if operator is defined %} {% set txtUpit = txtUpit ~ " and n_operator_id = " ~ operator.id 	%}{% endif %}
	{% if report_date is defined %} {% set txtUpit = txtUpit ~ " and n_payment_date = '" ~ report_date ~ "'"	%}{% endif %}
	{{ txtUpit }}
	
		<div style="text-align:center;" class="row">
			<h2>Promet blagajne</h2>		
		</div>
		<br/>
		<br/>
		<br/>
	<table style="width:100%;">
		
		{% if fl is defined %}
			<tr>
				<th style="width:20%; font-weight: bold; text-align:right;">Fiskalna lokacija:</th>
				<th style="width:30%; font-weight: normal; ">{{ fl.name }}</th>
				<th style="width:50%; "></th>
			</tr>
		{% endif %}
		
		{% if operator is defined %}
			<tr>
				<th style="width:20%; font-weight: bold; text-align:right;">Operater:</th>
				<th style="width:30%; font-weight: normal; ">{{operator.first_name ~ ' ' ~ operator.last_name }}</th>
				<th style="width:50%; "></th>
			</tr>
		{% endif %}

		{% if report_date is defined %}
			<tr>
				<th style="width:20%; font-weight: bold; text-align:right;">Datum:</th>
				<th style="width:30%; font-weight: normal; ">{{ date("d.m.Y", strtotime(report_date)) }}</th>
				<th style="width:50%; "></th>
			</tr>
		{% endif %}
	</table>
	
	
	<br/>
	<hr/>
	 
	<table style="width:100%;">
			<tr style="border:1px solid black; background-color:#F2F1F1; text-align:right;">
				<th style="width:20%" >Vrsta plaćanja</th>
				<th style="width:10%">Vrijeme</th>
				<th style="width:30%">Naziv kupca</th>
				<th style="width:10%">ID Kupca</th>
				<th style="width:10%">Br.Računa</th> 
				<th style="width:15%; text-align:right;">Iznos</th>
			</tr>
			<tr>
				<td colspan ="6"><hr/></td>
			</tr>
			{% set suma = 0 %}
			{% for pm in _model.PaymentMethods() %}
				{% set orders = _model.Orders( txtUpit ~ ' and n_payment_methods_id = ' ~ pm.id) %}
				{% if count(orders) > 0 %}
					{% set sumaGroup = 0 %}
					
					<tr>	
						<td colspan = "6">{{ pm.name }}</td>
					</tr>

					
					{% for order in orders %}
						{% set ou = order.User() %}
						{% set sumaGroup = sumaGroup + order.n_total_with_tax %}
						{% set suma = suma + order.n_total_with_tax %}
						
						{% if ou.type == 1 %} 
							{% set custName = ou.first_name ~ " " ~ ou.last_name %} 
						{% else %} 
							{% set custName = ou.company_name %} 
						{% endif %}
						
							<tr>	
								<td></td>
								<td>{{ ( order.n_invoice_date_time_issue ? date( "H:i:s", strtotime( order.n_invoice_date_time_issue ) ) : 'ERROR' ) }} </td>
								<td>{{ custName }}</td>
								<td>{{ ou.id }}</td>
								<td>{{ order.id }}</td>
								<td style="text-align:right; margin-left:10px;">{{ number_format(order.n_total_with_tax, 2, ',', '.') }}</td>
							</tr>
					{% endFor %}
					
					<tr>	
						<td colspan = "5"><I>UKUPNO ZA {{ pm.name }}</I></td>

						<td style="text-align:right; margin-left:10px;"><I>{{ number_format(sumaGroup, 2, ',', '.') }}</I></td>
					</tr>
					<br/>
					<tr>
						<td colspan ="6"><hr/></td>
					</tr>				
				{% endif %}	
			{% endFor %}

			<tr>	
				<td colspan = "5"><B>UKUPNO ZA IZVJEŠTAJ</B></td>
				<td style="text-align:right; margin-left:10px;"><B>{{ number_format(suma, 2, ',', '.') }}</B></td>
			</tr>
	</table>
	

</div>

<div class="row" style="position:absolute; bottom:0; height:40px; margin-top:40px; font:8.5px arial, sans-serif; width:85%">
	<hr/>
			Poduzeće je registrirano u Trgovačkom sudu u Zagrebu, Reg.broj 080109869; Temeljni kapital: 2.320.000&nbsp;HRK u potpunosti uplaćen; 
			Članovi uprave: Željko Hudoletnjak 
	<br/>
			Poslovna banka: RBA 11; HRK Žiro račun: 2484008-1100153885; Devizni žiro račun: 2484008-1100153885; IBAN: HR1624840081100153885; SWIFT: RZBHHR2X
</div>
