{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre>#}

{# Suva User Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
						
            <h1 class="page-header">Publications</h1>
        </div>
        <br>
		{{ partial("partials/btnAddNew", ['controller':'publications'] ) }}
        {{ partial("partials/blkFlashSession") }}
        <table class="table table-striped table-hover table-condensed">

			
			{{ partial("partials/blkTableSort",['fields':[ 
				['title':'Options'										,'width':200	,'show_navigation' : true	] 
				,['title':'ID'				,'name':'id'				,'width':100	,'search_ctl':'ctlText'		] 
				,['title':'Name'			,'name':'name'				,'width':400	,'search_ctl':'ctlText'		] 
				,['title':'Is Active'		,'name':'is_active'			,'width':100	,'search_ctl':'ctlText'		] 
				
			] ]) }}
			
		{% for id in ids %}
			{% set entity = _model.getOne('Publications',id) %}
            <tr>
                <td>{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'publications'] ) }}</td>
				<td>{{ entity.id }}</td>
                <td>{{ entity.name }}</td>
				<td>{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'readonly':1 ] ) }}</td>
                
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are no publications here!</td></tr>
            {% endfor %}
        </table>

    </div>
</div>
