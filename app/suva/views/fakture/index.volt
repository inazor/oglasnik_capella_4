{# Orders Viewer #}
<div class="row">
    <div class="col-lg-12 col-md-12">
<!--         <div> -->
			<!--UČITAVANJE BANKOVNOG IZVODA-->
            <div class="pull-right text-right col-md-6 search-upload-wrapper">
                {% if import_warnings_download_href %}
                <div class="pull-right" style="margin-left:2em">
                    <a href="{{ import_warnings_download_href }}" class="btn btn-danger"><span class="fa fa-download"></span> Download import-warnings.csv</a>
                </div>
                {% endif %}
                {%- if errors is defined -%}
                    {%- set upload_errors = errors.filter('upload') -%}
                {%- else -%}
                    {% set upload_errors = [] %}
                {%- endif -%}
                {{ form(NULL, 'id' : 'orders-upload', 'action' : '/suva/orders/bank-xml-upload', 'method' : 'post', 'enctype' : 'multipart/form-data', 'autocomplete' : 'off', 'class' : 'form-inline orders-xml-upload') }}
                {{ hiddenField('_csrftoken') }}
                <div class = "form-group{{ not(upload_errors is empty) ? ' has-error' : '' }}">
                    <div class = "fileinput fileinput-new" data-provides="fileinput">
                        <span class = "btn btn-info btn-file"><span class="fileinput-new"><span class="glyphicon glyphicon-upload"></span> Select file</span><span class="fileinput-exists"><span class="glyphicon glyphicon-edit"></span> Change file</span><input type="file" name="upload" id="upload"></span>
                        <span class = "fileinput-filename-wrap">
                            <span class = "fileinput-filename"></span>
                            <a href = "#" class = "close fileinput-exists" data-dismiss="fileinput"><span class="glyphicon glyphicon-remove"></span></a>
                        </span>
                    </div>
                    {%- for err in upload_errors -%}
                        <p class="help-block">{{ err.getMessage() }}</p>
                    {%- endfor -%}
                </div>
                <div class="form-group text-right">
                    <button class="btn btn-primary">Upload</button>
                </div>
                {{ endForm() }}
            </div>
			<div class="pull-right"><p><a href="{{ json_download_href|escape }}"><span class="fa fa-fw fa-download"></span>Export current page as JSON</a></p></div>
			<h4 class="page-header">
				{{_context['selected_order_status_id']}}
				{% if  _context['selected_order_status_id'] > 0  %}
					Document status: {{ _model.OrdersStatus(_context['selected_order_status_id']).description }}
				{% else %}
					Sales documents 
				{% endif %}
			</h4>
<!--         </div> -->
		
		{% if fisk_error is defined %}
			<br/><strong><h2>Greška fiskalizacije:</h2></strong>
			<br/><strong>{{ fisk_error }}</strong>
		{% endif %}	
			
        {{ partial("partials/blkFlashSession") }}
		
		<!-- PRETRAŽIVAČ -->
		<div class="row">
        <form class="form-inline" role="form">
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
           
                <div class="col-lg-4 col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {%- for f in field_options -%}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {%- endfor -%}
                                <li class="divider"></li>
                                <li><a href="#all">All fields</a></li>
                            </ul>
                        </div>
					
						

						<div class="btn-group" role="group" aria-label="..." >
							<input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
							<button style="height: 35px; " type="submit" class="btn btn-default" ><span class="glyphicon glyphicon-search"></span></button>
							
							<div class="btn-group" role="group">
								<button style="height: 35px; " type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								</button>
								<ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
									<li role="presentation" class="dropdown-header">Search mode</li>
									{% for m, m_desc in mode_options %}
										<li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
									{% endfor %}
								</ul>
							</div>
							
						</div>
						
						
						{#<div class="input-group-btn" >
                            <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
							<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>#}

						
						
						
                    </div>
                </div>
				<div class="col-lg-8 col-sm-6 text-right">
                    <!-- select id="n_status" name="n_status" class="form-control" title="Order status">
                        {%- for value, title in status_options -%}
                            <option {% if value == status %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select -->
                    <select id="order-by" name="order_by" class="form-control" title="Order by">
                        {%- for value, title in order_by_options -%}
                            <option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            
			
        </form>
		</div>
        <br>
		
       
		<!--LISTA ORDERA -->
		<table class="table table-striped table-hover master-detail">
			<tr>
				<th>Order</th>
				<th class="text-right">Order Total</th>
				<th class="text-right">Discounts / Commissions</th>
				<th class="text-right">Total incl.VAT</th>
				<th>Status</th>
				<th>Actions</th>
				<th class="text-right">Valid until</th>
				<th class="text-right">Created at</th>
				<th class="text-right">Modified at</th> 
				<th>Paid at</th>
				<th>User</th>
			</tr>


            {% for ord_rec in page.items %}
				{% set order = _model.Order(ord_rec['id']) %}
               
                <tr id="order-{{ order.id }}" > 
									
                    <td class="col-md-3"
						onclick="$('#order_{{order.id}}_items').toggle();"
						>
						ID: {{ order.id }} 
						<small class="text-muted">PBO: {{ order.pbo }}</small> 
						<small>{{ order.payment_method }}</small>
					</td>
					
					<td class="col-md-1 text-right">{{ number_format((order.n_total), 2, ',', '.') }}</td>
					<td class="col-md-1 text-right">{{ number_format((order.n_agency_commission_amount), 2, ',', '.') }}</td>
                    <td class="col-md-1 text-right">{{ number_format((order.n_total_with_tax), 2, ',', '.') }}</td>

                   
<!-- 					STATUS FAKTURE I AKCIJE -->
					<td class="col-md-1"><span> {{ order.OrderStatus().description }}</span></td>
					<td class="col-md-1">
						<span class="btn btn-warning btn-xs" 
							  onclick="$('#_actions_list_order_{{order.id}}').toggle();">
							Actions 
							<span id="_actions_list_order_down_{{order.id}}"  
								  class="glyphicon glyphicon-arrow-down"></span>
						</span> 
						<div id="_actions_list_order_{{order.id}}" style="display:none;">

							{% for id in _model.getAllowedActionIds( _user.id, order.n_status, 'orders' ) %}
								{% set action = _model.Action(id) %} 								
								{{ action.buttonHtml( order.id ) }}<br/>										
							{% endfor  %}
						</div>
                    </td>
                    <td class="col-md-1 text-right nowrap">{{ order.valid_until }}</td>
                    <td class="col-md-1 text-right nowrap">{{ date('d.m.Y H:i', strtotime(order.created_at)) }}</td>
                    <td class="col-md-1 text-right nowrap">{{ date('d.m.Y H:i', strtotime(order.modified_at)) }}</td>
					<td class="col-md-1 text-right nowrap">{{ order.n_payment_date }}</td>
					<td class="col-md-1">
						&nbsp;{{ linkTo(['suva/korisnici/crud/' ~ order.user_id, order.User().username]) }}
						
					</td>
                </tr>
			
			
                {% if order.OrdersItems() is not null %}
					<tr data-master="order-{{ order.id }}" id="order_{{order.id}}_items" style="display:none;">
						<td colspan="9">
							
							<!-- REDAK FAKTURE -->
							<table class="table table-condensed table-striped table-bordered order-details">    
								<thead>
									<tr>
										<th class="text-left">Ad</th>
										<th class="text-left">Order item</th>
										<th class="title">Product</th>
										<th class="title">Publication</th>
										<th class="title">From</th>
										<th class="title">To</th>
										<th class="text-right price">Price</th>
										<th class="text-right qty">Quantity</th>
										<th class="text-right value">Total</th>{{order.n_agency_commission}}
										<th class="text-right value"></th>
									</tr>
								</thead>
								<tfoot>
									{% if order.n_agency_commission_amount > 0  %}
										<tr class="summary total">
											<td colspan="7">&#160;</td>
											<td class="text-right"><strong>Discounts</strong></td>
											<td class="text-right value"><strong>{{number_format((order.n_agency_commission_amount), 2, ',', '.')}}</strong></td>
											<td class="text-right"></td>
											<td></td>
										</tr>
									{% endif %}
									
									<tr class="summary total">
										<td colspan="7">&#160;</td>
										<td class="text-right"><strong>Total</strong></td>
										
										<td class="text-right value"><strong>{{number_format((order.n_total), 2, ',', '.')}}</strong></td>
										
										<td class="text-right"><strong>Total incl. VAT: {{number_format((order.n_total)+((order.n_total)*0.25), 2, ',', '.')}}</strong></td>
										<td></td>
									</tr>
								</tfoot>
								<tbody>
									{% for item in order.OrdersItems() %}
									<tr>
<!-- 										<pre>{ { print_r(item, true) } }</pre> -->
										<td class="text-left"><a href="/suva/ads2/edit/{{ item.ad_id }}/{{ _model.User(order.user_id).id }}">{{ item.ad_id }}</a></td>
										<td class="text-left">{{ item.id }}</td>
										<td class="text-left">{{ item.title }}</td>
										<td class="text-left">{{ item.Publication().name }}</td>
										<td class="text-left">{{ _model.n_dateUs2DateHr(item.n_first_published_at) }}</td>
										<td class="text-left">{{ _model.n_dateUs2DateHr(item.n_expires_at) }}</td>
										<td class="text-right price">{{number_format(item.n_price, 2, ',', '.') }}</td>
										<td class="text-right qty">{{ item.qty }}</td>
										
										<td class="text-right value">{{ number_format(item.n_total , 2, ',', '.') }}</td>
									
										<td class="text-right">
<!-- 											{ { print_r (item,true) } } -->
<!-- 											AKCIJE NA RAZINI ITEMA -->

									
										{% for id in _model.getAllowedActionIds( _user.id, order.n_status, 'orders-items' )  %}
											{% set action = _model.Action(id) %}
											{{ action.buttonHtml( item.id ) }}<br/>
										{% endfor  %}
										
										</td>
									</tr>
									{% endfor  %}
								</tbody>
							</table>
						</td>
					</tr>
                {% endif %}
			
				
            {% endfor %}
        </table>

			
			
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
