<?php

namespace Baseapp\Suva\Models;

/**
 * SysTranslations Model
 */
class SysTranslations extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_sys_translations");
    }

        public function getSource()
    {
        return 'n_sys_translations';
    }

}