<?php

namespace Baseapp\Suva\Models;

// use Baseapp\Library\Utils;

/**
 * InfractionReports Model
 */
class InfractionReports extends \Baseapp\Models\InfractionReports
{
	use traitBaseSuvaModel;
    // const ALLOWED_ROLES = array('admin', 'supersupport', 'support');

    // /**
     // * InfractionReports initialize
     // */
    // public function initialize()
    // {
        // parent::initialize();

        // $this->belongsTo(
            // 'reporter_id', __NAMESPACE__ . '\Users', 'id',
            // array(
                // 'alias' => 'Reporter',
                // 'foreignKey' => array(
                    // 'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                // )
            // )
        // );
    // }

    // /**
     // * Builds and returns the SQL statement used for the backend search UI
     // *
     // * @param string $model
     // * @param array $fields
     // * @param string $q
     // * @param string $mode
     // *
     // * @return string
     // */
    // public static function buildSearchQuery($model = null, $resolvedStatus = null, $fields = array(), $q = '', $mode = 'exact')
    // {
        // $params = Utils::build_search_sql_params($fields, $q, $mode);

        // // Build (raw) sql WHERE clause if needed
        // $where_sql = '';
        // if ($model || !empty($params) || $resolvedStatus) {
            // $where_sql = 'WHERE ';
            // $where_or_parts = array();
            // $where_and_parts = array();
            // if (!empty($params)) {
                // foreach ($params as $bind_name => $value ) {
                    // // TODO: check and harden against SQLi
                    // $where_or_parts[] = sprintf('ir.%s LIKE \'%s\'', str_replace(':', '', $bind_name), $value);
                // }

                // if (count($params) > 1) {
                    // $where_or_parts[] = sprintf('a.title LIKE \'%s\'', $value);
                    // $where_or_parts[] = sprintf('u.username LIKE \'%s\'', $value);
                    // $where_or_parts[] = sprintf('s.title LIKE \'%s\'', $value);
                // }
            // }

            // if ($model) {
                // $model_name = explode('\\', $model);
                // $model_name = $model_name[count($model_name) - 1];
                // $where_and_parts[] = '(ir.model_name LIKE \'%' . $model_name . '\')';
            // }

            // if ($resolvedStatus) {
                // $where_and_parts[] = '(ir.resolved_at IS' . ('yes' == $resolvedStatus ? ' NOT' : '') . ' NULL)';
            // }

            // if (count($where_or_parts)) {
                // $where_and_parts[] = '(' . implode(' OR ', $where_or_parts) . ')';
            // }

            // if (count($where_and_parts)) {
                // $where_sql .= implode(' AND ', $where_and_parts);
            // }
        // }

        // $sql = "
            // SELECT
                // ir.model_name as model_name,
                // ir.model_pk_val as model_pk_val,
                // IF(a.title IS NOT NULL, a.title, IF(u.username IS NOT NULL, u.username, s.title)) as model_title,
                // COUNT(ir.id) as count
            // FROM
                // infraction_reports AS ir
            // LEFT JOIN
                // ads AS a ON ir.model_name LIKE '%Ads' AND a.id = ir.model_pk_val AND a.soft_delete = 0
            // LEFT JOIN
                // users AS u ON ir.model_name LIKE '%Users' AND u.id = ir.model_pk_val
            // LEFT JOIN
                // users_shops AS s ON ir.model_name LIKE '%UsersShops' AND s.id = ir.model_pk_val
            // $where_sql
            // GROUP BY
                // ir.model_name, ir.model_pk_val
            // ORDER BY
                // count DESC, ir.id DESC";

        // return trim($sql);
    // }

    // public static function hydrateResults($rows, $resolvedStatus = null)
    // {
        // if (empty($rows)) {
            // return;
        // }

        // $ad_ids   = array();
        // $user_ids = array();
        // $shop_ids = array();

        // foreach ($rows as $row) {
            // switch($row->model_name) {
                // case 'Baseapp\Suva\Models\Ads':
                    // // for ads that have no model_title (for now only soft-deleted ads), we have to
                    // // exclude their id's from further processing as there's no meaning of handling
                    // // infraction reports for such ads
                    // if ($row->model_title) {
                        // $ad_ids[] = (int) $row->model_pk_val;
                    // }
                    // break;
                // case 'Baseapp\Suva\Models\Users':
                    // $user_ids[] = (int) $row->model_pk_val;
                    // break;
                // case 'Baseapp\Suva\Models\UsersShops':
                    // $shop_ids[] = (int) $row->model_pk_val;
                    // break;
            // }
        // }

        // $where_sql = array();
        // if (!empty($ad_ids)) {
            // $where_sql[] = '(model_name LIKE "%Ads" AND model_pk_val IN (' . implode(',', $ad_ids) . '))';
        // }
        // if (!empty($user_ids)) {
            // $where_sql[] = '(model_name LIKE "%Users" AND model_pk_val IN (' . implode(',', $user_ids) . '))';
        // }
        // if (!empty($shop_ids)) {
            // $where_sql[] = '(model_name LIKE "%UsersShops" AND model_pk_val IN (' . implode(',', $shop_ids) . '))';
        // }

        // if (empty($where_sql)) {
            // return null;
        // }

        // $where_sql = implode(' OR ', $where_sql);

        // if ($resolvedStatus) {
            // $where_sql .= ' AND resolved_at IS' . ('yes' == $resolvedStatus ? ' NOT' : '') . ' NULL';
        // }

        // $sql = trim("
            // SELECT
                // ir.model_name as model_name,
                // ir.model_pk_val as model_pk_val,
                // ir.report_reason as report_reason,
                // ir.report_message as report_message,
                // ir.reported_at as reported_at,
                // ir.reported_ip as reported_ip,
                // ir.reporter_id as reporter_id,
                // ir.resolved_at as resolved_at,
                // r.username as reporter_username,
                // IF(ir.resolved_at IS NULL, 0, 1) as resolve_status
            // FROM
                // infraction_reports AS ir
            // INNER JOIN
                // users AS r ON ir.reporter_id = r.id
            // WHERE $where_sql
            // ORDER BY
                // resolve_status ASC, ir.id DESC");

        // $details = self::getQueryResults($sql);

        // $reports = array();

        // foreach ($rows as $i => $row) {
            // if ($row->model_title) {
                // $report    = $row;
                // $modelName = explode('\\', $row->model_name);
                // $modelName = mb_strtolower($modelName[count($modelName) - 1], 'UTF-8');
                // $reportID  = $modelName . '-' . (int) $row->model_pk_val;

                // $report->id      = $reportID;
                // $report->details = array();

                // foreach ($details as $detail) {
                    // $detailModelName = explode('\\', $detail['model_name']);
                    // $detailModelName = mb_strtolower($detailModelName[count($detailModelName) - 1], 'UTF-8');
                    // $detailReportID  = $detailModelName . '-' . (int) $detail['model_pk_val'];

                    // if ($detailReportID == $reportID) {
                        // $detail['model_name']   = null;
                        // $detail['model_pk_val'] = null;
                        // unset($detail['model_name'], $detail['model_pk_val']);

                        // // Break the possibly huge strings for display purposes
                        // if (!empty($detail['report_message'])) {
                            // $detail['report_message'] = Utils::soft_break(Utils::esc_html($detail['report_message']));
                        // }
                        // // TODO: maybe add a flag if the detail row should be visible
                        // // on page load (if it matches the search value or we want to
                        // // highlight it etc...) -- will also need some tweaks to view/js
                        // $report->details[] = $detail;
                    // }
                // }
                // $reports[] = $report;
            // }
        // }

        // return count($reports) ? $reports : null;
    // }

    // public static function getInfractionReports($model, $groupStatuses = false)
    // {
        // $modelName = explode('\\', get_class($model));
        // $modelName = mb_strtolower($modelName[count($modelName) - 1], 'UTF-8');

        // $sql = trim("
            // SELECT
                // ir.id as id,
                // ir.report_reason as report_reason,
                // ir.report_message as report_message,
                // ir.reported_at as reported_at,
                // ir.reported_ip as reported_ip,
                // ir.reporter_id as reporter_id,
                // ir.resolved_at as resolved_at,
                // r.username as reporter_username,
                // IF(ir.resolved_at IS NULL, 0, 1) as resolve_status
            // FROM
                // infraction_reports AS ir
            // INNER JOIN
                // users AS r ON ir.reporter_id = r.id
            // WHERE ir.model_name LIKE '%{$modelName}' AND ir.model_pk_val = {$model->id}
            // ORDER BY
                // resolve_status ASC, ir.id DESC");

        // $rows = self::getQueryResults($sql);

        // if ($groupStatuses) {
            // $results = array(
                // 'all'        => array(),
                // 'resolved'   => array(),
                // 'unresolved' => array()
            // );

            // foreach ($rows as $row) {
                // $results['all'][] = $row;
                // if ($row['resolved_at']) {
                    // $results['resolved'][] = $row;
                // } else {
                    // $results['unresolved'][] = $row;
                // }
            // }
        // } else {
            // $results = $rows;
        // }

        // return $results;
    // }

    // public static function getQueryResults($sql, $params = null, $fetchMode = \Phalcon\Db::FETCH_ASSOC)
    // {
        // $di = \Phalcon\Di::getDefault();
        // /**
         // * @var $db \Phalcon\Db\AdapterInterface
         // */
        // $db = $di->getShared('db');

        // // Fire the actual query and store the results
        // $results = $db->query($sql, $params);
        // $results->setFetchMode($fetchMode);

        // return $results->fetchAll();
    // }

    // public static function resolveID($id)
    // {
        // $result = null;

        // $di   = \Phalcon\Di::getDefault();
        // $auth = $di->get('auth');
        // if ($auth->logged_in(self::ALLOWED_ROLES)) {
            // $currentUserId = $auth->get_user()->id;
            // $ids           = array();

            // if (is_array($id)) {
                // $ids = $id;
            // } elseif (strpos($id, ',') !== false) {
                // $ids = explode(',', $id);
            // } elseif ((int) $id) {
                // $ids[] = (int) $id;
            // }

            // if (count($ids)) {
                // if ($infractionReports = self::find('id IN (' . implode(',', $ids) . ')')) {
                    // $result = $infractionReports->update(array(
                        // 'resolved_by' => $currentUserId, 
                        // 'resolved_at' => date('Y-m-d H:i:s', Utils::getRequestTime())
                    // ));
                // }
            // }
        // }

        // return $result;
    // }

}
