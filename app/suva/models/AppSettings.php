<?php

namespace Baseapp\Suva\Models;

/**
 * DtpGroups Model
 */
class AppSettings extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_app_settings");
    }

        public function getSource()
    {
        return 'n_app_settings';
    }

}