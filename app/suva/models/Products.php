<?php

namespace Baseapp\Suva\Models;

/**
 * Publications Model
 */
class Products extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_products");
    }

		public function getSource()
    {
        return 'n_products';
    }




    /**
     * Add a new Publication
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->initialize_model_with_post($request);

            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
    }

    /**
     * Save Publication data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {

        $this->initialize_model_with_post($request);
		
//         if (count($messages)) {
//             return $validation->getMessages();
//         } else {
		
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
//         }
    }
	 
	
	public function getPrice($pCategoryId = null){
		
		
		if($pCategoryId){
			$txtWhere = "products_id = $this->id and categories_mappings_id = $pCategoryId";			
			$pcp = \Baseapp\Suva\Models\ProductsCategoriesPrices::findFirst( $txtWhere );
		} 
		
		if ($pcp){
			// error_log ("Product.php.getPrice txtWhere = $txtWhere , pcp = ".print_r($pcp->toArray(), true));
			if($pcp->unit_price == null){
				return $this->unit_price;
			}
			return $pcp->unit_price;
		} 
		
		else return $this->unit_price;
		
	}

	public function Category(){
		$id = $this->category_id > 0 ? $this->category_id : -1;
		return \Baseapp\Suva\Models\Categories::findFirst($id);
	}
	public function Publication(){
		$pubId = $this->publication_id > 0 ? $this->publication_id : -1;
		return \Baseapp\Suva\Models\Publications::findFirst($pubId);
	}

	public function Layout(){
		$model = new \Baseapp\Suva\Models\Model();
		return $model->getOne('Layouts', $this->layouts_id); 
	}
	

	
}
