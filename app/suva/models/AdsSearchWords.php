<?php

namespace Baseapp\Suva\Models;

/**
 * AdsSearchTerms Models
 */
// class AdsSearchWords extends \Baseapp\Models\AdsSearchWords
// {
	// use traitBaseSuvaModel;
// }
class AdsSearchWords extends BaseModelBlamable
{
	use traitBaseSuvaModel;
    /**
     * AdsSearchTerms initialize
     */
    public function initialize()
    {
        // parent::initialize();

        // $this->belongsTo(
            // 'ad_id', __NAMESPACE__ . '\Ads', 'id',
            // array(
                // 'alias'      => 'Ad',
                // 'reusable'   => true,
                // 'foreignKey' => array(
                    // 'action' => \Phalcon\Mvc\Models\Relation::ACTION_CASCADE,
                // )
            // )
        // );
		
		$this->setSource("n_ads_search_words");
    }

    public function getSource()
    {
        return 'n_ads_search_words';
    }

    // public static function getByPrimaryKeyOrCreateNew($ad_id = null)
    // {
        // $ads_search_words = null; 

        // if ($ad_id) {
            // $ads_search_words = self::findFirst(
                // array(
                    // 'conditions' => 'ad_id = :ad_id:',
                    // 'bind'       => array(
                        // 'ad_id' => $ad_id,
                    // )
                // )
            // );
        // }

        // if (!$ads_search_words) {
            // $ads_search_words = new self();
            // $ads_search_words->ad_id = $ad_id;
            // $ads_search_words->ad_full_text = null;
        // }

        // return $ads_search_words;
    // }
}
