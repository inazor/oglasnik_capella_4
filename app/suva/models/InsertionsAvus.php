<?php

namespace Baseapp\Suva\Models;

/**
 * Insertions Model
 */
class InsertionsAvus extends BaseModelBlamable
{
    

	public function initialize()
    {
        $this->setSource("n_insertions_avus");
    }

        public function getSource()
    {
        return 'n_insertions_avus';
    }

	
	//veze many2one
	public function Issue(){
		$id = $this->issue_id > 0 ? $this->issue_id : -1 ;
		return \Baseapp\Suva\Models\Issues::findFirst($id);
	}

	public function Ad(){
		$id = $this->ad_id > 0 ? $this->ad_id : -1 ;
		return \Baseapp\Suva\Models\Ads::findFirst($id);
	}

}