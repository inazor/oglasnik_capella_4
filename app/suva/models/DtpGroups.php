<?php

namespace Baseapp\Suva\Models;

/**
 * DtpGroups Model
 */
class DtpGroups extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_dtp_groups");
    }

        public function getSource()
    {
        return 'n_dtp_groups';
    }

}