<?php

namespace Baseapp\Suva\Models;

/**
 * AdsParameters Model
 */
class AdsParameters extends \Baseapp\Models\AdsParameters
{
	use traitBaseSuvaModel;
	

	//many2one
	public function Parameter(){
        return \Baseapp\Suva\Models\Parameters::findFirst(  ( $this->parameter_id > 0 ? $this->parameter_id : -1 )  );
    }
	public function Dictionary(){
        return \Baseapp\Models\Dictionaries::findFirst(" id = '$this->type_id' ");
    }

}
