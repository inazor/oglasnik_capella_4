<?php
namespace Baseapp\Suva\Models;

use Phalcon\Mvc\Model\Query\Builder;
use Baseapp\Library\Utils;
use Phalcon\Mvc\Model\Resultset;

class FiscalLocations extends BaseModelBlamable
{

public function initialize()
    {
        $this->setSource("n_fiscal_locations");
    }

public function getSource()
    {
        return 'n_fiscal_locations';
    }



public function save_changes($request)
    {

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }
 }