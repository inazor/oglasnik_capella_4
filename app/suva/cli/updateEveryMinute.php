


<?php

include '../vendor/pokreniObradu.php';
 
fMsg( "___________________________________________________ ".PHP_EOL."POČETAK:". date("Y-m-d h:i:sa") );

//glavna petlja
$dbOgla = fConnectOgla();
fMsg ("1");
$arrLockedTables = fCheckLockedTables(); 
if ( $arrLockedTables ) {
	fMsg("Locked tables exist: ".print_r( $arrLockedTables, true ));
	exit();
}
else{
	fMsg ("no locked tables");
}

setIssueDeadline();
setAdsInfoForFrontend();

$dbOgla = null;

fMsg("KRAJ:". date("Y-m-d h:i:sa"));

//============================================================================================================
// funkcije

function setIssueDeadline(){
	global $dbOgla;

	//AUTOMATSKO NAMJEŠTANJE DEADLINEA
	try {
		fMsg("UPDATING ISSUE STATUS DEADLINE.."); 

		$txtOglaWhere = " where status = 'prima' and deadline < now()";
		$sqlCount = "select count(*) as broj from n_issues $txtOglaWhere";
		$count = sqlToArray( $sqlCount )[0]['broj'];
		fMsg($sqlCount);
		//fMsg("count: $count");
		if ( $count > 0 ){
			$sqlUpdate = "update n_issues set status = 'deadline' $txtOglaWhere";
			fMsg($sqlUpdate);
			runSql ($sqlUpdate);
			fMsg("Status of $count issues updated successfully.");
		} elseif ( intval($count) === 0 ){
			fMsg("0 issues found to update");
		}
	}
	catch(PDOException $e){
		fMsg("Issue status update  failed: " . $e->getMessage());
	}

}

function setAdsInfoForFrontend(){
	
	/*
	
	
	1. deaktivirat aktivne online order iteme
	
	2. aktivirat neaktivne online order iteme
	
	3. 
	
	
	
	*/
	

	fMsg("Activating ads and setting dates START:". date("Y-m-d h:i:sa"));
	try {
		fMsg("Postavljanje n_publications_id na orders_items");
		$txtSql = "
			update orders_items oi
				join n_products p on ( oi.n_products_id = p.id  and p.publication_id > 0)
			set oi.n_publications_id = p.publication_id
			where oi.n_publications_id is null
		";
		runSql ($txtSql);
		
		
		//ispravljanje greški na adovima
		runSql("update ads set n_active_online_order_item_id = null where active=0 and n_active_online_order_item_id > 0");
		
		//fMsg("Deaktiviranje isteklh ad-ova.");
		$txtSql = "
			select a.id 
			from ads a
				join orders_items oi on (
					a.n_active_online_order_item_id = oi.id
					and oi.n_expires_at < now()
					and a.active = 1
					)";
		$rs = sqlToArray ($txtSql);
		$txtIds = "Deaktiviranje isteklih oglasa: ";
		if ($rs){
			foreach ($rs as $row){
				$txtIds .= " ".$row['id'];
			}
		}
		fMsg ($txtIds.PHP_EOL);
		
		$txtSql = "
			update ads a 
				join orders_items oi on (
					a.n_active_online_order_item_id = oi.id
					and oi.n_expires_at < now()
					and a.active = 1
					)
			set 
				a.active = 0
				,a.n_active_online_order_item_id = null

		";
		runSql( $txtSql );
		
		
		
		//fMsg("Deaktiviranje ad-ova kojima je moderacija negativna ");
		$txtSql = "
			select a.id from 	ads a
				join categories c on (a.category_id = c.id)
					join categories_settings cs on (c.id = cs.category_id)
			where
				(
					(
						a.moderation = 'nok' 
						and
						cs.moderation_type = 'post'
					)
				or
					(
						a.moderation <> 'ok' 
						and
						cs.moderation_type = 'pre'
					)
				)
				and a.active = 1;
					";
		$rs = sqlToArray ($txtSql);
		$txtIds = "Deaktiviranje ad-ova kojima je moderacija negativna: ";
		if ($rs){
			foreach ($rs as $row){
				$txtIds .= " ".$row['id'];
			}
		}
		fMsg ($txtIds.PHP_EOL);
	
		
		$txtSql = "
			/*select * from 	ads a */
			update ads a 
				join categories c on (a.category_id = c.id)
					join categories_settings cs on (c.id = cs.category_id)
			set 
				a.active = 0 
			
			where
				(
					(
						a.moderation = 'nok' 
						and
						cs.moderation_type = 'post'
					)
				or
					(
						a.moderation <> 'ok' 
						and
						cs.moderation_type = 'pre'
					)
				)
				and a.active = 1;
		";
		runSql( $txtSql );
		
		
		

		//fMsg("Dodavanje sort_date, published_at, expires_at adovima iz najkasnije objavljenog order itema koji je aktivan");
		
			$txtSql = "
			select a.id , p3.id as oi_id
			from ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
		, UNIX_TIMESTAMP(oi.n_expires_at) as expires_at
		, oi.product as oi_product
		, oi.product_id as oi_product_id
		,p.is_pushup_product 
		, p.priority_online
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						)
 
					join (
						select 
							oi.ad_id
							,max(oi.n_first_published_at) as max_fpa
						from 
							orders_items oi
								join orders o on (
									oi.order_id = o.id 
									and o.n_status in (2,4,5) 
									)
								join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
						where 1=1 
							and oi.n_expires_at >= NOW() 
							and oi.n_first_published_at <= now() 
							and oi.n_is_published = 1 
							and oi.n_publications_id = 1
						group by
							oi.ad_id
					) p1 on ( 
						oi.ad_id = p1.ad_id 
						and oi.n_first_published_at = p1.max_fpa 
						and oi.n_expires_at >= NOW()
						and oi.n_is_published = 1
						and oi.n_publications_id = 1
						)
				group by p1.ad_id
			) p2 on (oi.id = p2.oi_id)
			join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
			
	) p3 on (
		a.id = p3.ad_id 
		and (
			a.n_active_online_order_item_id <> p3.id 
			or 
			a.n_active_online_order_item_id <> p3.id is null
			)
		)
	join 
		categories c on ( a.category_id = c.id )
			join categories_settings cs on ( 
				c.id = cs.category_id 
				and 	
					(
						(
							a.moderation in ( 'ok' , 'waiting' )
							and
							cs.moderation_type = 'post'
						)
					or
						(
							a.moderation = 'ok' 
							and
							cs.moderation_type = 'pre'
						)
					)
					and a.manual_deactivation = 0
			)
					";
		$rs = sqlToArray ($txtSql);
		$txtIds = "DODJELA AKTIVNOG ORDER ITEMA na oglase: ";
		if ($rs){
			foreach ($rs as $row){
				$txtIds .= " ".$row['id']."-".$row['oi_id'];
			}
		}
		fMsg ($txtIds.PHP_EOL);
	

		
		
		
		
		
		
$txtSql = "
update 
	ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
		, UNIX_TIMESTAMP(oi.n_expires_at) as expires_at
		, oi.product as oi_product
		, oi.product_id as oi_product_id
		,p.is_pushup_product 
		, p.priority_online
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						)
 
					join (
						select 
							oi.ad_id
							,max(oi.n_first_published_at) as max_fpa
						from 
							orders_items oi
								join orders o on (
									oi.order_id = o.id 
									and o.n_status in (2,4,5) 
									)
								join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
						where 1=1 
							and oi.n_expires_at >= NOW() 
							and oi.n_first_published_at <= now() 
							and oi.n_is_published = 1 
							and oi.n_publications_id = 1
						group by
							oi.ad_id
					) p1 on ( 
						oi.ad_id = p1.ad_id 
						and oi.n_first_published_at = p1.max_fpa 
						and oi.n_expires_at >= NOW()
						and oi.n_is_published = 1
						and oi.n_publications_id = 1
						)
				group by p1.ad_id
			) p2 on (oi.id = p2.oi_id)
			join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
			
	) p3 on (
		a.id = p3.ad_id 
		and (
			a.n_active_online_order_item_id <> p3.id 
			or 
			a.n_active_online_order_item_id <> p3.id is null
			)
		)
	join 
		categories c on ( a.category_id = c.id )
			join categories_settings cs on ( 
				c.id = cs.category_id 
				and 	
					(
						(
							a.moderation in ( 'ok' , 'waiting' )
							and
							cs.moderation_type = 'post'
						)
					or
						(
							a.moderation = 'ok' 
							and
							cs.moderation_type = 'pre'
						)
					)
					and a.manual_deactivation = 0
			)
set
     a.sort_date =  UNIX_TIMESTAMP(now())
	,a.published_at = p3.published_at
	,a.product_sort = p3.priority_online
	,a.expires_at =  p3.expires_at
	,a.online_product_id = p3.oi_product_id 
	,a.online_product = p3.oi_product
	,a.n_active_online_order_item_id = p3.id
	,a.active = 1
";
runSql( $txtSql );


fMsg("Dodavanje pushup proizvoda");

$txtSql = "
update 
	ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join n_products p on ( oi.n_products_id = p.id and p.is_pushup_product = 1 )
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						)
					where 1=1 
						and oi.n_first_published_at <= now() 
						and oi.n_is_published = 1 
						and oi.n_publications_id = 1
						and oi.n_pushup_applied_at is null
				group by oi.id
			) p2 on (oi.id = p2.oi_id)
	) p3 on (a.id = p3.ad_id and a.n_active_online_order_item_id is not null)
set
	
     a.sort_date =  p3.published_at	
	 ,a.n_last_active_pushup_order_item_id = p3.id
";
runSql( $txtSql );

//postavljanje oznake na pushup proizvode da su objavljeni
$txtSql = "
update
	orders_items oi 
		join ads a on (
			oi.ad_id = a.id
			and oi.id = a.n_last_active_pushup_order_item_id
			and oi.n_pushup_applied_at is null
			)
	set oi.n_pushup_applied_at = now();
";
runSql ($txtSql);

		
		fMsg("Activating ads and setting dates FINISH:". date("Y-m-d h:i:sa"));
		
	}
	catch(PDOException $e){
		echo "Setting Ads Info for Frontend failed: " . $e->getMessage().PHP_EOL;
	}
}




function getJirZik(){
	//DOHVAĆANJE JIR I ZIK IZ NAVISIONA
	try {
		echo PHP_EOL.PHP_EOL."SYNCING FISCAL INFO FROM NAV ".PHP_EOL; 
		$dbOgla = new PDO("mysql:host=127.0.0.1;dbname=oglasnik_capella", 'root', 'as9Thood');
		// set the PDO error mode to exception
		$dbOgla->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "Connected to ogladev.oglasnik successfully".PHP_EOL; 
		
		$dbNav = new PDO('dblib:host=195.245.255.22;port=1433;dbname=OGLASNIK_PROD', 'sa', 'Tassadar1337');
		// set the PDO error mode to exception
		$dbNav->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "Connected successfully to NAV".PHP_EOL; 
		
		
	// 	foreach($dbOgla->query("select * from orders where n_status in (4,5,9) and n_invoice_zik is NULL and n_invoice_jir is NULL and n_nav_sync_status = 'unsynced'") as $row){
		
		//na fiskalizaciju idu fakture, plaćene fakture i zakašnjelo plaćene fakture
		$txtSqlOgla = "select count(*) as broj from orders where n_status in (4,5,9) and n_nav_sync_status = 'unsynced'";
		$rsOgla = $dbOgla->query($txtSqlOgla);
		foreach($rsOgla as $rowOgla)
			echo "Found ".$rowOgla['broj']." orders to sync.";
		
		$txtSqlOgla = "select * from orders where n_status in (4,5,9) and n_nav_sync_status = 'unsynced'";
		$rsOgla = $dbOgla->query($txtSqlOgla);

		foreach($rsOgla as $rowOgla){
			//echo print_r($row, true);
			$txtSqlNav = 
				" select "
				." [External Document No_]" 
				.", [Sell-to Customer No_]"
				.", [Issuer Protection Code]" 
				.", [Unique Invoice No_]"
				.", [Fisc_ Wholesale Terminal]"
				.", [Fisc_ Location Code]"
				.", [Fisc_ Doc_ No_]"
				.", [Salesperson Code]"
				.", convert(varchar(30), [Posting DateTime],120) as date_time "
				.", convert(varchar(30), [Due Date],120) as due_date "
				." from [Oglasnik d_o_o_".'$'."Sales Invoice Header] "
				." where 1=1 "
				." and [External Document No_] = '90000".$rowOgla['id']."'";
			 
			$rsNav = $dbNav->query( $txtSqlNav );
			echo "Found ".count($rsNav)." Nav Invoices for order 90000".$rowOgla['id'].PHP_EOL;
			
			foreach ( $rsNav as $rowNav ) {
				//echo print_r($rowNav, true);
				$txtSqlOgla =  
					" UPDATE orders SET " 
						." n_invoice_zik = '".$rowNav['Issuer Protection Code']."'"
						.", n_invoice_jir = '".$rowNav['Unique Invoice No_']."'"
						.", n_nav_salesperson_code = '".$rowNav['Salesperson Code']."'"
						.", n_invoice_fiscal_number = '".$rowNav['Fisc_ Doc_ No_']."-".$rowNav['Fisc_ Location Code']."-".$rowNav['Fisc_ Wholesale Terminal']."'"
						.", n_invoice_date_time_issue = '".$rowNav['date_time']."'"
						.", n_invoice_due_date = '".$rowNav['due_date']."'"
						.", n_nav_sync_status = 'synced'"
					." WHERE id = ".$rowOgla['id'];
				echo "OGLA ROW UPDATE QUERY: $txtSqlOgla".PHP_EOL;
				$dbOgla->query($txtSqlOgla);
			}
		}
	}
	catch(PDOException $e){
		echo "Connection to ogladev.oglasnik failed: " . $e->getMessage().PHP_EOL;
	}
}

function syncPaymentMethods(){
	//SINHRONIZIRANJE TABLICE PAYMENT METHODS IZ NAVISIONA
	global $dbNav;
	global $dbOgla;


	try {
		echo PHP_EOL.PHP_EOL."SYNCING PAYMENT METHODS FROM NAV ".PHP_EOL."Connecting 127.0.0.1 ...".PHP_EOL; 
		//$dbOgla = new PDO("mysql:host=127.0.0.1;dbname=oglasnik", 'oglasnik', 'PZQBgZLMgkGH9Mz');
		$dbOgla = new PDO("mysql:host=127.0.0.1; dbname=oglasnik_capella", 'root', 'as9Thood');
		// set the PDO error mode to exception
		$dbOgla->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "Connected to ogladev.oglasnik successfully".PHP_EOL; 
		
		
		//zakomentirano
		$sqlNav = " select * from [Oglasnik d_o_o_".'$'."Payment Method] ";
		$rsNav =  $dbNav->query($sqlNav);
		
		foreach ( $rsNav as $rowNav ) {
			
			$txtSqlOgla = "select count(*) as broj from n_payment_methods where nav_id = '".$rowNav['Code']."'"; 
			echo $txtSqlOgla.PHP_EOL;
			$rsOgla = $dbOgla->query($txtSqlOgla);
			foreach ( $rsOgla as $recOgla ) {
				echo "RECOGLA:".print_r($recOgla, true).PHP_EOL;
				if ( $recOgla['broj'] == 0 ) {
					//TODO: ovone radi IGOR
					// //ako nema payment methoda s brojem nav_id
					$txtSqlOgla = 
						" insert into n_payment_methods ( slug, name, nav_id, is_fiscalized ) values( "
						." '".$rowNav['Code']."' "
						.", '".$rowNav['Description']."' "
						.", '".$rowNav['Code']."' " 
						.", ".$rowNav['Fisc_ Subject']
						." )";
					echo "INSERTING NEW PAYMENT METHOD: $txtSqlOgla".PHP_EOL;
					$dbOgla->query( $txtSqlOgla );
				}
			}
		}
		
	} 
	catch(PDOException $e){
		echo "Syncing payment methods failed " . $e->getMessage().PHP_EOL;
	}

}

//============================================================================================================
// SQL , MSG funkcije

function fMsg ( $pMessage , $pTitle = "" ){
	
	$msg = is_array($pMessage) ? print_r($pMessage, true) : $pMessage;
	$title = $pTitle ? $pTitle.":".PHP_EOL : "";
	
	echo PHP_EOL.$title.$msg;
}

function sqlToArray ( $pSql ){
	global $dbOgla;
	$rs = $dbOgla->query($pSql);
	$result = array();
	foreach ($rs as $row)
		array_push ($result, $row);
	return $result;
}

function runSql ( $pSql ){
	global $dbOgla;
	$dbOgla->query($pSql);
}

function fConnectOgla (){
	try {

		$oglaHost = "127.0.0.1";
		$oglaDb = "oglasnik_capella";
		$oglaUser = "root";
		$oglaPassword = "as9Thood";
		
		fMsg("Connecting to $oglaHost ..");
		$db = new PDO("mysql:host=$oglaHost; dbname=$oglaDb", $oglaUser, $oglaPassword);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
		fMsg("Connected to $oglaHost successfully"); 
		return $db;
		
	}
	catch(PDOException $e){ 
		fMsg("SQL failed: " . $e->getMessage());
	}
}

function fConnectNav (){
	try {
		$navHost = "195.245.255.22";
		$navPort = "1433";
		$navDb = "OGLASNIK_PROD";
		$navUser = "sa";
		$navPassword = "Tassadar1337";
		
		fMsg("Connecting to $navHost ..");
		$db = new PDO("dblib:host=$navHost;port=$navPort;dbname=$navDb", $navUser, $navPassword );
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
		fMsg("Connected to $navHost successfully"); 
		return $db;
	}
	catch(PDOException $e){ 
		fMsg("SQL failed: " . $e->getMessage());
	}
	

}

function fCheckLockedTables(){
	$rs = sqlToArray("show open tables from test_oglasnik2_6 where In_use > 0");
	return $rs;
}