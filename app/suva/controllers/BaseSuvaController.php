<?php

namespace Baseapp\Suva\Controllers;


use Baseapp\Extension\Tag;
use Phalcon\Dispatcher;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ControllerGroupTrait;

use Baseapp\Suva\Models\OrdersStatuses;
//use Baseapp\Suva\Models\Cart;
use Baseapp\Suva\Models\FiscalLocations;
//use Baseapp\Controllers\BaseController; //dodati putanju do basekontrollera


//DODANO
use Baseapp\Suva\Library\Nava;
use Baseapp\Library\Utils;
use Phalcon\Paginator\Adapter\QueryBuilder;
use Baseapp\Library\Tool;
/**
 * Base suva Controller Class
 */
class BaseSuvaController extends \Baseapp\Controllers\BaseController
{

    use ControllerGroupTrait;

    protected $next_url;

	public $moduleRootDir = 'suva';
	
	protected $allowed_roles = array('admin', 'support', 'finance', 'moderator', 'content');
    protected $insufficient_privileges = 'Sorry, you have insufficient privileges to perform the attempted action!';
 
 
    /** Runs before any route is executed.
     * 
     *
     * @param Dispatcher $dispatcher
     *
     * @return void | \Phalcon\Http\ResponseInterface
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher){
		
		
		//$this->flashSession->success("POZIV <pre>".print_r($_REQUEST, true)."</pre>");
		
		
		
		//$this->flashSession->success("<pre>".print_r($_REQUEST, true)."<pre>");
		
		//POSTAVLJANJE ARRAYOVA ZA PRIKAZ GREŠAKA NA KLIJENTU
		if ( isset($_SESSION) ) {
			if( !array_key_exists('_context', $_SESSION) ) $_SESSION['_context'] = array();
		}
		else{
			$_SESSION['_context'] = array();
		}
		

		if ( isset($_SESSION) ) {
			if( !array_key_exists('_nava', $_SESSION) ) $_SESSION['_nava'] = array();
		}
		else{
			$_SESSION['_nava'] = array();
		}
		
		
		
		$this->fill_SESSIONDataForSuva();
	
		
		
		//error_log("baseSuva::beforeexecuteRoute REQUEST ".print_r($_REQUEST, true));
		//error_log("BaseSuvaController.php::beforeExecuteRoute 1");
		
	
		//IGOR: Zakrpa da ne provjerava login u nekim situacijama (npr kad se ide preko nekretnina
		$checkLogin = true;	
		if ($this->moduleRootDir == 'nekretnin') $checkLogin = false;

		
		if ($checkLogin )
			if (!$this->auth->logged_in($this->allowed_roles)) {
				if ($this->request->isAjax() || $this->auth->logged_in()) {
					//error_log("BaseSuvaController.php::beforeExecuteRoute 3");
					$this->do_403();
				} else {
					//error_log("BaseSuvaController.php::beforeExecuteRoute 4");
					$this->redirect_signin();
					//error_log("BaseSuvaController.php::beforeExecuteRoute 5");
				}

				// Returning false within beforeExecuteRoute tells the dispatcher to stop the current operation
				return false;
			}
			
		// NIKOLA
		// basesuvacontroller extenda basecontroller gdje se generira csrf token unutar initialize funkcije
		// initialize funkcija se poziva jedin u slucaju da je beforeExecuteRoute u basesuvacontroller-u uredno izvrsen
		// ako je beforeExecuteRoute uredno izvrsen znaci da ovde mozemo pozvati funkciju za provjeru izgeneriranog tokena
		// i mozemo ga koristit unutar VIEWS gdje je to vec potrebno
        // {{ hiddenField('_csrftoken') }} ide unutar forme koju zelimo zastiti s CSRF tokenom 
		$this->check_csrf();
		
		//TODO070916: suva.js?, može li bit u folderu suva?
		$this->assets->addCss('assets/suva/css/suva.css');
		$this->assets->addJs('assets/suva/js/suva.js');

		$this->assets->addJs('assets/vendor/jquery.mask.min.js');//
		$this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');//
		$this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');//
		$this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');  //css za bolji izgled checkboxeva
		$this->assets->addJs('assets/vendor/jquery.mask.min.js');
		
		// VAŽNO: js-i se ne includaju u partialima, SAMO OVDJE
		$this->assets->addJs('assets/suva/js/ajaxPozivi.js');
		$this->assets->addJs('assets/suva/js/ctlPartials.js');
		$this->assets->addCss('assets/suva/css/ctlPartials.css');
		
		//gfid - formiranje arraya
		if ( ! array_key_exists('_globalFormData', $_SESSION)  ){
			//$this->flashSession->success("novi globalFormData");
			$_SESSION['_globalFormData'] = array( 'maxGfId' => 0, 'currGfId' => 0 );			
		} 		
		//gfId - dohvat contexta iz storagea
		if ( array_key_exists('_gfId', $_REQUEST ) && strlen($_REQUEST['_gfId']) > 0 && intval($_REQUEST['_gfId']) > 0 ){			
			//uzimanje gfid oz requesta
			$gfId = $_REQUEST['_gfId'];

			if (!array_key_exists($gfId, $_SESSION['_globalFormData'] )){
				$_SESSION['_globalFormData'][ $gfId ] = array();				
			}			
			//ode se poremeti session
			if ($_SESSION['_globalFormData'][ $gfId ])
				$_SESSION['_context'] = $_SESSION['_globalFormData'][ $gfId ];						
			$_SESSION['_globalFormData']['currGfId'] = $gfId;
			$_SESSION['_context']['_gfId'] = $gfId;
			$this->view->setVar('_gfId', $gfId);
			//Nava::poruka("gfid stari $gfId");
			//$this->flashSession->success("gfid stari $gfId");
		}	
		else{ 
			//dodjela novog gfida
			
			$_SESSION['_globalFormData']['maxGfId'] = $_SESSION['_globalFormData']['maxGfId'] + 1;
			$gfId = $_SESSION['_globalFormData']['maxGfId'];
			//$this->flashSession->success("dodjela novog, u requestu je ".$_REQUEST['_gfId'].", max = ".$_SESSION['_globalFormData']['maxGfId'].", novi gfId = $gfId." );
			$_SESSION['_globalFormData']['currGfId'] = $gfId;
			$_SESSION['_globalFormData'][ $gfId ] = array();
			$_SESSION['_context']['_gfId'] = $gfId;
			$this->view->setVar('_gfId', $gfId);
			//Nava::poruka("gfid novi $gfId");
			//$this->flashSession->success("gfid novi $gfId");
		}
		//$this->flashSession->success("gfid izvan $gfId");

         

        // Prepare 'next url' (where we want the user to end up after this action)
        $this->setup_next_url($dispatcher);
		//error_log("BaseSuvaController beforeExecuteRoute next_url ".$this->next_url);
        
		
		
		$_SESSION['_context']['logged_in_user_id'] = $this->auth->get_user()->id;
		
		$module = $this->router->getModuleName();
		if($module)
			$_SESSION['_context']['n_source_path'] = $module;
		
 
        if ($this->request->isPost() || $this->request->isGet()) {
			//error_log("REQUEST JE POST ILI GET");
			//BRISANJE ONIH KOJE ŽIVE JEDAN CIKLUS
			
			//$_SESSION['_context']['fiscal_location'] = $this->request->getPost('fiscal_lok');
			
			if ( array_key_exists('_keep_active_tab_setting', $_SESSION['_context']) ) unset($_SESSION['_context']['_keep_active_tab_setting']); 
			else unset($_SESSION['_context']['_active_tab']); 
			
			//ARRAYI ZA PRIKAZ PORUKA NA KLIJENTU
			$_SESSION['_context']['_msg_flashsession'] = array();
		
			//error_log("BaseSuvaController::beforeExecuteRoute SESSION CONTEXT prije porebacivanja iz requesta ".print_r($_SESSION['_context'],true));
		
			//ako u requestu nije došla naredba da se u SESSION zadrže prethodne postavkle forme, izbriši ih
			$keepListParams = false;

			
			
			if ( array_key_exists('_context', $_REQUEST) ){
				if (
					array_key_exists('order_by', $_REQUEST['_context'])
					|| array_key_exists('default_order_by', $_REQUEST['_context'])
					|| array_key_exists('filter', $_REQUEST['_context'])
					|| array_key_exists('default_filter', $_REQUEST['_context'])
					)  {
					$keepListParams = false;
					//error_log("keep list params = false jer je u requestu došao filter");
				}
			}

			if ($this->request->isGet()) {
				//Nava::printr($_REQUEST, "REQUEST GET");
			}
			
			if ($this->request->isPost()) {
				//Nava::sysMessage($_REQUEST, "REQUEST POST");
			}
			
			if (array_key_exists ('_context', $_REQUEST)){
				//Nava::poruka("postoji context u requestu");
				if (array_key_exists ('keep_list_params', $_REQUEST['_context'])){
					//Nava::poruka("postoji KLP u request contextu");
					if ($_REQUEST['_context']['keep_list_params'] == 1){
						//Nava::poruka("KLP je 1");
						$keepListParams = true;
						//error_log("keep list params = true jer je u requestu keep_list_params");
					}
				}
			}

			if (array_key_exists ('_context', $_SESSION))
				if (array_key_exists ('keep_list_params', $_SESSION['_context']))
					if ($_SESSION['_context']['keep_list_params'] == 1){
						$keepListParams = true;
						//error_log("keep list params = true jer je u session contextu bio keep_list_params");
					}
					
			//Nava::poruka("Keeo LIst Params je $keepListParams");
			if (!$keepListParams) $this->n_reset_list_params();		

			//error_log("BaseSuvaController::beforeExecuteRoute SESSION CONTEXT prije 2 porebacivanja iz requesta ".print_r($_SESSION['_context'],true));
			
			//REQUEST->CONTEXT ako su poslane _context varijable iz klijenta
			if (array_key_exists('_context',$_REQUEST)){
				//ubacivanje svega iz REQUEST u CONTEXT
				foreach($_REQUEST['_context'] as $key=>$value) {
			
					//FILTER, ORDER_BY: poseban slučaj za filtere i orderby jer se pushaju na SESSION, a ne brišu prethodno
					if (($key === "order_by" || $key === "filter" ) && is_array($value)){
						foreach($value as $subKey => $subValue){
							//ako je poslana prazna vrijednost, briše se sa liste filtera ili orderby-a
							if ($subValue === "" ) {
								unset($_SESSION['_context'][$key][$subKey]);
							}
							else $_SESSION['_context'][$key][$subKey] = $subValue;
						}
					}
					elseif ( $value == '__TRUE__' ){
						$_SESSION['_context'][$key] = true;
					} 
					elseif ( $value == '__FALSE__' ){
						$_SESSION['_context'][$key] = false;
					} 
					elseif ( $value == '__NULL__' ){
						$_SESSION['_context'][$key] = null;
					}
					elseif ( $value == '__UNSET__' ){
						unset ($_SESSION['_context'][$key]);
					} 

					else $_SESSION['_context'][$key] = $value;
				}
			}
			
			//ako su u requestu došli filteri ili order_by, zadrži ih do kraja
			if (
				array_key_exists('order_by', $_SESSION['_context'])
				|| array_key_exists('default_order_by', $_SESSION['_context'])
				|| array_key_exists('filter', $_SESSION['_context'])
				|| array_key_exists('default_filter', $_SESSION['_context'])
				) 
				$_SESSION['_context']['keep_list_params'] = 1;
            
			//error_log("BaseSuvaController::beforeExecuteRoute SESSION CONTEXT nakon porebacivanja iz requesta ".print_r($_SESSION['_context'],true));
			
			//VRAĆANJE NA next_url ako  se stisne Exit. next_url je definiran na formi pomoću {{ hiddenField('next') }}
			if ($this->request->hasPost('cancel')) {
                $this->redirect_to($this->get_cancel_url($dispatcher));
                // Tells the dispatcher to stop the current operation
                return false;
            }
			
			
			//VRAĆANJE NA next_url ako  se stisne Save and Exit. next_url je definiran na formi pomoću {{ hiddenField('next') }}
			//izvršava se ostatak koda, samo se postavlja redirect_to
			if ($this->request->hasPost('saveexit')) {
                $this->redirect_to($this->get_next_url($dispatcher));
                // Tells the dispatcher to stop the current operation
                //return false;
            }

			//VRAĆANJE NA next_url ako  se stisne Save and Exit. next_url je definiran na formi pomoću {{ hiddenField('next') }}
			//izvršava se ostatak koda, samo se postavlja redirect_to
			if ($this->request->hasPost('savenew')) {
				$novi_url = $this->get_default_next_url($dispatcher);
				$novi_url_crud = $novi_url."/crud";
                $this->redirect_to( $novi_url_crud );
                // Tells the dispatcher to stop the current operation
                //return false;
            }
			
        
		}

        // Set default title
        $this->tag->setTitle('Index');
		
		
		$this->view->setVar('moduleRootDir', $this->moduleRootDir);
		
		//TODO070916: Vidit di se koristi u suvi?
        // Add global css and js assets (built with grunt)
        // If in debug mode, load non-minified assets (concat only)
        $dbg = $this->getDI()->get('config')->app->debug;
        //If user has role "Ad taker" set localisation js
        if ($this->auth->logged_in(array('n_adtaker'))) {
			$this->assets->addJs('assets/build/jquery.cookie.js');
			$this->assets->addJs('assets/build/fisc_location.js');
        }       

		//gfid - Punjenje storagea s tekućim contextom koji je napunjen iz requesta
		if ( array_key_exists('_context', $_REQUEST )) {
			foreach ( $_REQUEST['_context'] as $key=>$value ){
				$_SESSION['_globalFormData'][ $gfId ] = $_SESSION['_context'];
			}
		}
			//$this->poruka("gfid $gfid");
		//$this->printr($_SESSION['_context'], "BaseBackendCOntroller::beforeExecuteRoute AFTER CONTEXT");
    }

    /** After every Action
     *
     * @param Dispatcher  $dispatcher
     */
    public function afterExecuteRoute(Dispatcher $dispatcher){
        //BRISANJE ONIH KOJE ŽIVE JEDAN CIKLUS CLIENT->SERVER
        /*brisanje nekih varijabli iz $_SESSION['_context'] koje žive samo u jednom pozivu */
		//$this->printr($_REQUEST);
		//Nava::sysMessage("BaseSuvaontroller:afterExecuteRoute: ".time());
		
		
		if (array_key_exists('n_source_path', $_SESSION['_context'])) unset($_SESSION['_context']['n_source_path']);		 
		if (array_key_exists('clicked_action', $_SESSION['_context'])) unset($_SESSION['_context']['clicked_action']);
		if (array_key_exists('clicked_action_params', $_SESSION['_context'])) unset($_SESSION['_context']['clicked_action_params']);
        if (array_key_exists('selected_roles_users', $_SESSION['_context'])) unset($_SESSION['_context']['selected_roles_users']);
		
		if (array_key_exists('reset_paginator', $_SESSION['_context'])) unset($_SESSION['_context']['reset_paginator']);

		
		

		$this->view->setVar('loadFiscalLocation', false);
		if ( $this->auth->logged_in() )
			 //error_log("BaseSuvaController KORISNIK JE LOGIRAN ");
			 // $_SESSION['_context']['fiscal_location'] = true;
				if ( !array_key_exists('fiscal_location', $_SESSION['_context']) )
					$this->view->setVar('loadFiscalLocation', true);
				
		 if ($this->request->isPost() || $this->request->isGet()) {
			//error_log("REQUEST JE POST ILI GET");
			//BRISANJE ONIH KOJE ŽIVE JEDAN CIKLUS
			
		
		 }
		
		//postavljanje sistemskih
		$_SESSION['_context']['_controller_name'] = $this->router->getControllerName();
		$_SESSION['_context']['_controller_action'] = $this->router->getActionName();
		
		if ( ! array_key_exists( '_SID', $_SESSION['_context'] ) ) { 
			//ako je već postavljen _SID onda ne postavljati ponovo, jer je možda postavljen SID od parent sessiona
			$_SESSION['_context']['_SID'] = session_id();
		}
		
		//ako je postavljena $keepListParams
		$keepListParams = false;
		if (array_key_exists('keep_list_params', $_SESSION['_context']))
			if ( $_SESSION['_context']['keep_list_params'] == 1 )
				$keepListParams = true;
	
		if ($keepListParams) {
			//error_log( "unsetting keep_list_params" );
			unset($_SESSION['_context']['keep_list_params']);
			//error_log( "context na kraju afterexecuteroute ".print_r($_SESSION['_context'],true) );
		}
		else $this->n_reset_list_params();
		
        // Set final title
        $this->tag->appendTitle(' - Suva');

        //TODO070916: Vidit di se koristi u Suvi - služi za dohvat skripti
		// Set scripts
        $this->view->setVar('scripts', $this->scripts);

        // Sets some view vars (helps controlling menu display mostly for now)
        $controller_name = $this->router->getControllerName();
        $controller_action = $this->router->getActionName();
		//$orders_count = Cart::getOrdersCount($this->auth->get_user()->id);
        
        $fiscal_lok   = FiscalLocations::find()->toArray();
				
		$this->view->setVar('_model', new \Baseapp\Suva\Models\Model() );
		
		//19.1.2017
		$this->view->setVar('_m', new \Baseapp\Suva\Models\Model() );
		
		$this->view->setVar('_user', \Baseapp\Suva\Models\Users::findFirst( $this->auth->get_user()->id ) );

		if ( !array_key_exists("_context", $_SESSION) ) $_SESSION['_context'] = array();
		
		
		$this->view->setVar('_context', $_SESSION['_context']);

		//BRISANJE ONIH KOJE ŽIVE JEDAN CIKLUS CLIENT->SERVER->CLIENT  (nakon slanja konteksta)
		
				
		//$this->printr($_SESSION['_context']);		
        //$this->view->setVar('n_svi_options', $n_svi_statusi);
        // $this->view->setVar('orders_count', $orders_count);
        $this->view->setVar('fiscal_lok', $fiscal_lok);
        
        $this->view->setVar('controller_name', $controller_name);
        $this->view->setVar('controller_action', $controller_action);
        $this->set_controller_group($controller_name);


        //$this->printr($_SESSION['_context']);
        
        /**
         * TODO/FIXME: see if this can be de-duplicated, or just
         * apply the same fix so it works for both frontend and backend
         */
        // Minify css and js collection
        // \Baseapp\Library\Tool::assetsMinification();
		
		//NOVA VERZIJA 
		//ovo se izvršava ako dođe ajax poziv
		if ($this->request->isAjax()){
			
			
			//error_log("BasesuvaController::afterexecuteroute::isAjax messages :".print_r($_SESSION['_context']['_msg_flashsession'],true));

			//slanje window_id-ja pomoću get-a ili Posta
			if ($this->request->has('_window_id'))
				$this->view->setVar('_window_id',  $this->request->get('_window_id') );
			//error_log("afterExecuteRoute IsAjax 1 _window_id:".$this->request->get('_window_id'));

			//slanje caller URL-ja pomoću get-a ili Posta (da bi se moglo vratiti Back) - treba naći nešto bolje
			if ($this->request->has('_caller_url'))
				$this->view->setVar('_caller_url',  $this->request->get('_caller_url') );
			//error_log("afterExecuteRoute IsAjax 1 _caller_url:".$this->request->get('_caller_url'));


			
			$_SESSION['_context']['_is_ajax_submit'] = true;
			$this->view->setVar('_context', $_SESSION['_context']);
			$this->view->setVar('_is_ajax_submit', true);


			$model = new \Baseapp\Suva\Models\Model();
			$this->view->setVar('_model', $model);
			
			if (array_key_exists('_error', $_SESSION['_context']))
				$this->view->setVar('_error', $_SESSION['_context']['_error']);

			
			//error_log("BaseBackendController: afterExecuteRoute IsAjax 2 controller_name: $controller_name, controller_action $controller_action");
			$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
			$this->view->render( $controller_name, $controller_action);
			
			
			$this->view->finish();
			$html = $this->view->getContent();
			
			//error_log("BaseBackendController: afterExecuteRoute IsAjax 3 HTML:".$html);
	
			
			//ako je PDF, samo ga se šalje u text
			if (substr($html,1,3) == 'PDF'){ 
				$this->response->setContentType('text/plain', 'UTF-8');
				$this->response->setStatusCode(200, "OK");
				$this->response->setContent($html);
			}
			else {
				$content = array(
					'html' => $html,
					'_context' => $_SESSION['_context'] 
					);
				// error_log("CONTENT PRIJE SLANJA".print_r($content,true));
				 
				$jsonContent = json_encode($content);
				//error_log ("BaseSuvaController.php:afterExecuteRoute jsonContent:".$jsonContent);
				
				// Problem je bio što se ajax kotroler nije zaustavljao na vrijeme te je upisivao dodatan content.
				// Provjerava se ako content ne postoji postavi ga inace ostavi kako je bilo. 
				if ( ! $this->response->getContent() ) {
					$this->response->setContent($jsonContent);
					$this->response->setContentType('application/json', 'UTF-8');
					$this->response->setStatusCode(200, "OK");
				}
				
			}

			//error_log("BaseBackendController:afterExecuteRoute: 13");
			$this->response->send();
			//error_log("BaseBackendController:afterExecuteRoute: 14");
			$this->disable_view();
			
			unset($_SESSION['_context']['_is_ajax_submit']);

			
			//gfid - punjenje storagea s zadnjim kontextom prije slanja
			if ( array_key_exists ('currGfid', $_SESSION['_globalFormData'] ) ){
				$_SESSION['_globalFormData'][ $_SESSION['_globalFormData']['currGfid'] ] = $_SESSION['_context'];
			}

			//error_log("AAAAAAAAAAAAAAAAAAA");

			
			return $this->response;
			
			
		}
		else {  //nije Ajax
			//AKO NIJE AJAX ONDA SE PORUKE IZ KONTEKSTA MORAJU PREBACITI U FLASHSESSION
			//error_log("NIJE AJAX, PORUKE: basesuvacontroller CONTEXT".print_r($_SESSION['_context']['_msg_flashsession'],true));
			//Nava::printr($_SESSION['_context']['_msg_flashsession'], "FLASHSESSION");
			foreach($_SESSION['_context']['_msg_flashsession'] as $message){
				foreach ($message as $key=>$value){	
					
					// if ($key == 'error'){
						// error_log("saljem gresku $value");
						// $this->flashSession->error($value);
					// }
					// elseif ( $key == 'info'){
						// error_log("saljem info $value");
						// $this->flashSession->success($value);
					// }
					// elseif ($key == 'system'){
						// error_log("saljem info $value");
						// $this->flashSession->success($value);
					// }
					
					if ($key == 'error'){
						//error_log("saljem gresku $value");
						$this->flashSession->success("<span style='background-color:lightSalmon; color:red;'>$value</span>");
					}
					elseif ( $key == 'info'){
						//error_log("saljem info $value");
						$this->flashSession->success($value);
					}
					elseif ($key == 'system'){
						//error_log("saljem info $value");
						$this->flashSession->success($value);
					}
				}
			}
		}	
		
		//gfid - punjenje storagea s zadnjim kontextom prije slanja
		if ( array_key_exists ('currGfid', $_SESSION['_globalFormData'] ) ){
			$currGfid = $_SESSION['_globalFormData']['currGfid'];
			$_SESSION['_globalFormData'][ $currGfid ] = $_SESSION['_context'];
		}
		//Nava::sysMessage("BaseSuvaontroller:afterExecuteRoute-non-ajax END: ".microtime(true));
		
		
		
		
		//error_log("SESSION['_globalFormData']:".print_r($_SESSION['_globalFormData'], true));
    }

	public function n_reset_list_params(){
		//error_log("n_reset_list_params 1");
		//Nava::poruka("BaseSuva:afterExecuteRoute Brišem list_params");
		if (array_key_exists('order_by', $_SESSION['_context'])) unset($_SESSION['_context']['order_by']);
		if (array_key_exists('default_order_by', $_SESSION['_context'])) unset($_SESSION['_context']['default_order_by']);
		if (array_key_exists('filter', $_SESSION['_context'])) unset($_SESSION['_context']['filter']);
		if (array_key_exists('default_filter', $_SESSION['_context'])) unset($_SESSION['_context']['default_filter']);
		if (array_key_exists('from_rec', $_SESSION['_context'])) unset($_SESSION['_context']['from_rec']);
		if (array_key_exists('to_rec', $_SESSION['_context'])) unset($_SESSION['_context']['to_rec']);
		if (array_key_exists('count_rec', $_SESSION['_context'])) unset($_SESSION['_context']['count_rec']);
		if (array_key_exists('step_rec', $_SESSION['_context'])) unset($_SESSION['_context']['step_rec']);
	}

    public function set_popup_layout(){
        $this->view->setMainView('layouts/popup');
    }

	public function n_delete_action($pModelName = null, $pId = null){
		
		if (!$pModelName) return $this->greska("Nije naveden naziv modela");
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		
		if (!$pId) return $this->greska("Nije naveden ID elementa za brisanje ");
		//$this->poruka ("tu sam");
		$entity = $class::findFirst ($pId);
		
		$success = $entity->delete();
		if ($success === true){	
			//error_log("BaseController:n_crud_action 10");
			$this->flashSession->success("<strong> $pModelName No. $pId deleted.</strong> ");
			$this->saveAudit("Model $pModelName, ID= $entity->id , deleted.");
		} 
		else {
			//error_log("BaseController: deleteAction 2");
			$greske = $entity->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
			$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);
			$this->view->setVar('errors', $greske);
			$_SESSION['_context']['errors'] = $greske; //sada ide i preko konteksta
		}

		
	}
	
    /**
     * Overriding suva's redirect_to() method to automatically prefix
     * any locations with 'admin/' if it's not already prefixed.
     * If no location is specified we end up on admin index which is convenient.
     *
     * @param string $location
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function redirect_to($location = '', $status_code = null)
    {
        $location =  str_replace('', 'suva/', $location);
        return parent::redirect_to($location, $status_code);
    }

    /**
     * Overriding BaseController's redirect_back() for suva usage since suva has slightly different
     * requirements and defaults.
     * Redirects back to where the user came from (or where he was specified to go
     * via "next" query/form param). If parsing those fails, we return him to the suva homepage.
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function redirect_back()
    {
        $this->disable_view();
        $fallback = $this->get_default_next_url($this->dispatcher);
        $redirect_path = $this->get_redir_path_from_current_request($fallback);
        if ($redirect_path) {
            return $this->redirect_to($redirect_path);
        } else {
            return $this->redirect_to('suva/');
        }
    }
	
	public function get_redirect_back_url()
    {
        //$this->disable_view();
        $fallback = $this->get_default_next_url($this->dispatcher);
        $redirect_path = $this->get_redir_path_from_current_request($fallback);
        if ($redirect_path) {
            return $redirect_path;
        } else {
            return 'suva/';
        }
    }

    /**
     * Returns which save button was submitted (if it was a POST)
     *
     * @return null|string
     */
    public function get_save_action()
    {
        $save_action = null;

        if ($this->request->isPost()) {
            $save_action = $this->request->hasPost('save') ? 'save' : 'saveexit';
        }

        return $save_action;
    }

    /**
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return mixed|null|string
     */
    public function determine_next_url_from_request($dispatcher)
    {
        // First check the query string
        if ($this->request->hasQuery('next')) {
            $next_url = $this->request->getQuery('next', 'trim');
        } else {
            if ($this->request->hasPost('next')) {
                // If there's an already posted next param, maintain it
                $next_url = $this->request->getPost('next', 'trim');
            } else {
                // If not, use the referer
                // TODO: what about cases when the referer already includes it's own ?next parameter?
                // TODO: what do we do when next ends up being: user/signin?next=admin%2Fimage-styles%2Fedit%2F4 ?
                $next_url = $this->request->getHTTPReferer();
                // Avoiding using the referer if it already contains a next param for now...
                if (stripos($next_url, 'next=') !== false) {
                    $next_url = null;
                }
            }
            // Fall back to default determined from the $dispatcher
            if (empty($next_url)) {
                $next_url = $this->get_default_next_url($dispatcher);
            }
        }

        // Make sure whatever we got is safe to use
        $next_url = $this->get_safe_redir_path($next_url);

        return $next_url;
    }

    /**
     * Sets up the default 'next' url form field
     *
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return null|string
     */
    public function setup_next_url($dispatcher)
    {
        if ('saveexit' === $this->get_save_action()) {
            // Modify 'next url' to point to 'cancel url' in case of 'saveexit'
            $next_url = $this->get_cancel_url($dispatcher);
        } else {
            // Set 'next url' to point to whatever was specified in the request
            $next_url = $this->determine_next_url_from_request($dispatcher);
        }

        Tag::setDefault('next', $next_url);
        $this->next_url = $next_url;

        return $this->next_url;
    }

    /**
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return string
     */
    public function get_default_next_url($dispatcher)
    {
        $next_url = 'suva/' . $dispatcher->getControllerName();
        //error_log("BASE SUVA CONTROLLER NIKOLA : $next_url");
		return $next_url;
    }

    public function get_next_url()
    {
        return $this->next_url;
    }

    /**
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return string
     */
    public function get_default_cancel_url($dispatcher)
    {
        // Default cancel url is (currently at least) really the same one as the default next url
        return $this->get_default_next_url($dispatcher);
    }

    /**
     * Hitting the 'Cancel' button should generally redirect you either to where explicitly
     * specified by the current request's 'next' POST parameter or to the $dispatcher's default
     * for the current controller.
     *
     * @param \Phalcon\Mvc\DispatcherInterface $dispatcher
     *
     * @return null|string
     */
    public function get_cancel_url($dispatcher)
    {
        //NIKOLA
		// Cancel should redirect you either to where explicitly specified by ?next
        // or to the $dispatcher's default
        if ($this->request->hasPost('next')) {
            $url = $this->request->getPost('next');
			//error_log("BASESUVACONTROLLER NIKOLA URL1 : $url");
			$url = $this->get_safe_redir_path($url);
        } else {
			
            $url = $this->get_default_cancel_url($dispatcher);
        }
		//error_log("BASESUVACONTROLLER NIKOLA URL2 : $url");
        return $url;
    }

//DODANO


  
    public function poruka( $pTxtPoruka = null ){
        if ($pTxtPoruka && isset($_SESSION) && array_key_exists('_context', $_SESSION) ){
			array_push($_SESSION['_context']['_msg_flashsession'], array('info' => $pTxtPoruka) );
        }
    }

	public function sysMessage( $pTxtPoruka = null ){
        if ($pTxtPoruka){
			array_push($_SESSION['_context']['_msg_flashsession'], array('system' => $pTxtPoruka) );
        }
    }
    public function printr( $pTxtPoruka = null , $pTxtNaslov = null){
        if ($pTxtPoruka){
			$naslov = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/>" : "";
            array_push($_SESSION['_context']['_msg_flashsession'], array('system' => "$naslov<br/><pre>".print_r($pTxtPoruka,true)."</pre>") );
        }
    }
    public function greska( $pTxtGreska = null , $pTxtNaslov = null ){
		//naslov se može dodati, ali poruka se ne ispisuje ako nema $pTxtGreska (koristi se za slanje poruke ako je greska u update)
        // if ($pTxtGreska){
			// if($this->request->isAjax()){
				// array_push($ajax_messages, $pTxtPoruka);
			// }
			// $naslov = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/>" : "";
            // $this->flashSession->error("$naslov<pre>".$pTxtGreska."</pre>");
            // //redirect_back ne radi jer se mora iz controllera pozvat 
            // //return $this->redirect_back();
        // }
		if ($pTxtGreska){
			$poruka = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/><pre>".$pTxtGreska."</pre>" : $pTxtGreska ;
			array_push($_SESSION['_context']['_msg_flashsession'], array('error' => $poruka) );
        }
    }
	
	public function sysError( $pTxtPoruka = null ){
		if ($pTxtGreska){
			$poruka = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/><pre>".$pTxtGreska."</pre>" : $pTxtGreska ;
			array_push($_SESSION['_context']['_msg_flashsession'], array('error' => $poruka) );
        }
    }

    public $n_table_model_mapping = array(
        'ads' => 'Ads',
        'categories' => 'Categories',
        'n_actions' => 'Actions',
		'n_categories_mappings' => 'CategoriesMappings',
        'n_discounts' => 'Discounts',
        'n_discounts_publications' => 'DiscountsPublications',
        'n_fiscal_locations' => 'FiscalLocations',
        'n_insertions' => 'Insertions',
        'n_issues' => 'Issues',
        'n_order_statuses' => 'OrdersStatuses',
        'n_order_status_transitions' => 'OrderStatusTransitions',
        'n_payment_types' => 'PaymentTypes',
        'n_products' => 'Products',
        'n_products_categories_types' => 'ProductsCategoriesTypes',
        'n_publications' => 'Publications',
		'n_publications_categories' => 'PublicationsCategories',
		'n_publications_categories_mappings' => 'PublicationsCategoriesMappings',
        'n_roles_permissions' => 'RolesPermissions',
		'n_users_contacts' => 'UsersContacts',
        'orders' => 'Orders',
        'orders_items' => 'OrdersItems',
        'orders' => 'Orders',
        'parameters' => 'Parameters',
        'roles' => 'Roles',
        'roles_users' => 'RolesUsers',
        'users' => 'Users',
		'n_products_categories_prices' => 'ProductsCategoriesPrices'
    );
    


	public function n_query_index($pModelName = null, $pExistingListOfIds = null, $pJoins = null ){
		//error_log("BaseController:n_query_index 1");
		//$this->printr($_SESSION['_context'], "SESSION CONTEXT");
		//$this->printr($_REQUEST,"REQUEST");
		//$this->poruka("IMA CONTEXT ".$this->request->hasQuery('_context'));
		//todo nikola učitat css za prilsgodbu index
		
		if (!$pModelName) return $this->greska("Nije naveden naziv modela");
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$entity = new $class();
		
		//NOVO - dodavanje parametara za update pomoću ajax-a. Osnovni parametri se dodaju ovdje, a parametar ajax_edit u voltu
		if (!array_key_exists('_context', $_SESSION)) $_SESSION['_context'] = array();
		$_SESSION['_context']['current_entity_table_name'] = $entity->getSource();

		//$this->printr($_SESSION['_context']);
		
		if (array_key_exists('reset_paginator',$_SESSION['_context'])) {
			unset ($_SESSION['_context']['reset_paginator']); 
			
			unset ($_SESSION['_context']['from_rec']); 
			unset ($_SESSION['_context']['to_rec']); 
			unset ($_SESSION['_context']['count_rec']); 
		}
		
		//GET nema _context ako dolazi preko linka, inače najčešće ima
		if($this->request->isGet() && $this->request->hasQuery('_context') == false) {
			//$this->poruka("Briše from-to");
			//unset ($_SESSION['_context']['default_filter']);
			unset ($_SESSION['_context']['filter']);
			unset ($_SESSION['_context']['order_by']);
			unset ($_SESSION['_context']['from_rec']);
			unset ($_SESSION['_context']['to_rec']);
			unset ($_SESSION['_context']['count_rec']); 
		}
		
		// $this->printr($_SESSION['_context']);
		
		//if (!$pExistingListOfIds)  {
			//formiranje SQL-a za pretragu
			
			$types = $entity->n_field_types($entity->getSource());
			$txtWhere =  " where 1=1 "; // defaultni where
			$txtJoins = " "; //defaultno nema joina
			
			//JOINI ako postoji pJoins
			/*
			$joins = array (
				array(
					'joinedModelTable' => 'users_roles',
					'originalModelField' => 'id',
					'joinedModelField' => 'user_id',
					'joinedModelConditions' => array (
						array(
							'field' => 'name', 
							'operator' => 'like',
							'value' => "'%admin%'"
						)
					)
				),
				array(
					'joinedModelTable' => 'users_roles',
					'originalModelField' => 'id',
					'joinedModelField' => 'user_id',
					'joinedModelConditions' => array (
						array(
							'field' => 'name', 
							'operator' => 'like',
							'value' => "'%admin%'"
						)
					)
				)
			)
			
			
			$joins = null;
			if ( is_array($_SESSION['_context']['filter'])  && array_key_exists('__UsersRoles__role_id' ,$_SESSION['_context']['filter'])){ 
				$joins = array (
					array(
						'joinedModelTable' => 'roles_users',
						'originalModelField' => 'id',
						'joinedModelField' => 'user_id',
						'joinedModelConditions' => array (
							array(
								'field' => 'role_id', 
								'operator' => '=',
								'value' => $_SESSION['_context']['filter']['__UsersRoles__role_id']
							)
						)
					)
				);
			}
			
			
			*/
			
			if ($pJoins){
				foreach ($pJoins as $join){
					$txtJoins .= PHP_EOL." join ".$join['joinedModelTable']
					." on (".$entity->getSource().".".$join['originalModelField']." = ".$join['joinedModelTable'].".".$join['joinedModelField'].") ";
					
					foreach ($join['joinedModelConditions'] as $jmc){
						$txtJoins .= PHP_EOL." and ".$join['joinedModelTable'].".".$jmc['field']." ".$jmc['operator']." ".$jmc['value'];
					}
				}
			}
			

			
			//query za listu id-jeva koja se šalje voltu
			$txtIds = " select ".$entity->getSource().".id from ".$entity->getSource().$txtJoins;
			
			//query za ukupni broj elemenata
			$txtCount = " select count(".$entity->getSource().".id) as broj from ".$entity->getSource().$txtJoins;
			
			//$this->printr($_REQUEST);
			//$this->printr($_SESSION['_context']);
			
			//error_log("BaseSuvaController::n_query_index SESSION CONTEXT  prije izrade queryja ".print_r($_SESSION['_context'],true));
			$fieldsListForModel = $entity->n_fields_list( $entity->getSource() );
			//$this->printr($fieldsListForModel, "LISTA POLJA ZA MODEL $pModelName ");
			
			//DEFAULT FILTER uvodi se default_filter - koji ne ovisi o requestu nego se uvijek stavlja
			if (array_key_exists('default_filter',$_SESSION['_context'] )){
				//$this->poruka("Ima default filter");
				foreach( $_SESSION['_context']['default_filter'] as $key => $value ){
					//ako ne počinje posebnim karakterima, (to se rješava preko joina)
					if ( substr($key,0,2) != '__' ) {
						//za svaki filter po polju iz liste filtera
						//error_log("BaseController:n_query_index 3");
						
						//provjerit je li taj key postoji u modelu
						if ( in_array($key, $fieldsListForModel) ){
						
							$family = $entity->n_data_family($types[$key]);
					
							//if (is_array($value)) $this->printr($value, "DEFAULT_FILTER");
							
							if (is_array($value) && $family == 'integer' && count($value) > 0 ){
								//ako je vrijednost filtera skup vrijednosti u arrayu onda se radi lista where .. in (..,..,..)
								
								$strFilter = "";
								foreach($value as $item){
									$strFilter.=", $item";
								}
								if (substr($strFilter,0,1) == ",") $strFilter = substr($strFilter,1); //micanje prvog zareza
								$txtWhere .= " and ".$entity->getSource().".$key in ( $strFilter ) ";
								//$this->poruka ($txtWhere);
							} 
							elseif (is_array($value) && $family == 'integer' && count($value) == 0 ){
								//ako je array ali prazni, onda se ne radi lista		
							}
							else {
								$txtWhere .= " and ".$key.$this->dataFamilyToFilterSql($family, $value);
							}
						}
						else {
							$this->sysMessage("Model $pModelName Default-filtered by invalid field : $key = $value ");
						}
					}
				}
			}
			//FILTER
			if (array_key_exists('filter',$_SESSION['_context'] )){				
				foreach( $_SESSION['_context']['filter'] as $key => $value ){
					//ako ne počinje posebnim karakterima, (to se rješava preko joina)
					if ( substr($key,0,2) != '__' ) {
						
						
												//provjerit je li taj key postoji u modelu
						if ( in_array($key, $fieldsListForModel) ){
							
							//error_log("BaseController:n_query_index 4");
							$family = $entity->n_data_family($types[$key]);
							$txtWhere .= " and ".$entity->getSource().".".$key.$this->dataFamilyToFilterSql($family, $value);
						}
						else {
							$this->sysMessage("Model $pModelName filtered by invalid field : $key = $value ");
						}						
					}
				}
			}	
			//error_log("BaseController:n_query_index 4.1 txtCount txtWhere $txtCount $txtWhere");
			
			 
			//računanje count nakon primjene filtera
			$rsCount = Nava::sqlToArray($txtCount." ".$txtWhere);
			$_SESSION['_context']['count_rec'] = $rsCount[0]['broj'];
			
			//$this->poruka($txtCount." ".$txtWhere);	
			 
			//DEFAULT ORDERBY uvodi se default orderBy, mora biti ispred klasičnog jer ima veći prioritet
			/* PRIMJER DEFAULT ORDER BY:
			
				$_SESSION['_context']['default_order_by']['id'] = 'desc';
			*/
			


			//ORDER BY
			$arrOrderBy = array();
			//default_order_by
			if (array_key_exists('default_order_by',$_SESSION['_context'] )){
				if (count ($_SESSION['_context']['default_order_by']) > 0 ) {
					foreach( $_SESSION['_context']['default_order_by'] as $key => $value )
						$arrOrderBy[$key] = $value;
				}
			}
			//order_by
			if (array_key_exists('order_by',$_SESSION['_context'] )){
				if (count ($_SESSION['_context']['order_by']) > 0 ) {
					foreach( $_SESSION['_context']['order_by'] as $key => $value ){
						//error_log ("BaseSuvaController.php::n_query_index. key $key  value $value ");
						if ( !array_key_exists($key, $arrOrderBy))
							$arrOrderBy[$key] = $value;
					}
				}
			}

			//generiranje sql upita iz arraya
			$txtOrderBy = "";
			foreach ($arrOrderBy as $key=>$value)
				$txtOrderBy .= " ".$entity->getSource().".$key $value ,";
			$txtOrderBy = rtrim($txtOrderBy, ',');
			
			if ($txtOrderBy)
				$txtOrderBy = PHP_EOL." order by ".$txtOrderBy;
			//error_log($txtWhere);


		
			//$_SESSION['_context']['count_rec'] = count($resultFull);
			
			$fromRec = array_key_exists('from_rec', $_SESSION['_context']) ? (int)$_SESSION['_context']['from_rec'] : 1;
			$stepRec = array_key_exists('step_rec', $_SESSION['_context']) ? (int)$_SESSION['_context']['step_rec'] : 100;
			$toRec = array_key_exists('to_rec', $_SESSION['_context']) ? (int)$_SESSION['_context']['to_rec'] : $fromRec + $stepRec - 1;
			$_SESSION['_context']['from_rec'] = $fromRec;
			$_SESSION['_context']['step_rec'] = $stepRec;
			$_SESSION['_context']['to_rec'] = $toRec;
			//$this->printr($_SESSION['_context'], "BaseSuvaController:n_index CONTEXT");
			//error_log("NIK BaseSuvaController:n_index CONTEXT".print_r($_SESSION['_context'],true));
			$offset = $fromRec >= 1 ? $fromRec - 1 : 0;
			$txtLimit = PHP_EOL." limit $offset, $stepRec ";
			
			$txtSqlFinal = $txtIds." ".$txtWhere." ".$txtOrderBy.$txtLimit;
			//error_log ("BaseSuvaController.php::n_query_index. txtSql: $txtSqlFinal ");
			//$this->sysMessage($txtSqlFinal);
			//error_log ("BaseSuvaController.php::n_query_index. request: ".print_r($_SESSION['_context'], true));
			
			//return;
			//Nava::poruka($txtSqlFinal);
			$result = Nava::sqlToArray($txtSqlFinal);
			//error_log("NIK BaseSuvaController:n_index RESULT".print_r($result,true));
			$ids = array(); 
			if ($result ){
				foreach($result as $rec){
					array_push($ids, $rec['id']);
				}
			}
			
			//novo 7,10,2016 da se ne brišu parametri na afterexecuteroute..
			$_SESSION['_context']['keep_list_params'] = 1;
			
			$this->view->setVar('ids', $ids);
			
		// }
		// else {
			// //radi formiranja liste za brzo pretraživanje
			// //ako postoji lista id-jeva koja je već poslana, tu se "umeće". Nema navigacije.
			
			// $ids = $pExistingListOfIds;
			// $this->view->setVar('ids', $pExistingListOfIds);
		// }
		
		
		//$this->printr($ids);
		$this->assets->addCss('assets/suva/css/table_sort.css');
	}
		
	public function dataFamilyToFilterSql($pFamily, $pValue){
		
		//pomoćna funkcija koja se samo koristi u gornjoj
		switch($pFamily){
			case 'integer':
			case 'decimal':
				return " = $pValue ";
			break;
			case 'textual':
				return " like '%$pValue%' ";
			break;
			case 'date':
			default:
				return " = '$pValue' ";
			break;
		}
	}
		
	public function n_crud_action($pModelName = null, $pEntityId = null, $pYnShowRequiredFields = false, $pDefaultValues = null){
		//$this->printr($_REQUEST, "n_crud_action:REQUEST");
		
		if (!$pModelName) return $this->greska("Nije naveden naziv modela");
		 
		$this->view->setLayout('layouts/n_crud.volt');
		
		//Čuvaju podaci o prethodnoj formi (count, filter, order) 
		//da bi se moglo vratiti pomoću back
		$_SESSION['_context']['keep_list_params'] = true;
		
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		//error_log("BaseSuvaController:n_crud_action : KLASA".$class);
		
		$entity = new $class();
		
		unset($_SESSION['_context']['errors']); //ovo bi trebalo 2 razine iznad
		//error_log($pModelName);
		//error_log($pEntityId);

		//NOVO - dodavanje parametara koji koriste partiali u voltu za update pomoću ajax-a. 
		//		Osnovni parametri se dodaju ovdje, a parametar ajax_edit u voltu
		if (!array_key_exists('_context', $_SESSION)) $_SESSION['_context'] = array();
		// TODO kaze da nema property a izlistava ga ?
		
		$_SESSION['_context']['current_entity_table_name'] = $entity->getSource();
		//error_log ("BasesuvaController 1 this:".print_r($entity->toArray(), true));
		 
		//provjera načina poziva funkcije
		$isPost = $this->request->isPost() ? true : false;
		$idInPost = false;
		if ($isPost) if ($this->request->hasPost('id')) if((int)$this->request->getPost('id') > 0) 	$idInPost = true;
		//error_log("BaseController:n_crud_action 3");
		$snimanjeNakonEditiranja = $isPost && $idInPost ? true : false;
		$snimanjeNovog = $isPost && !$idInPost && !$pEntityId ? true : false;
		$kreiranjeNovog = !$isPost && !$pEntityId ? true : false;
		$editPostojecega = $pEntityId && !$idInPost ? true : false;
		
		//$this->poruka("isPost: $isPost , idInPost: $idInPost , pEntityId: $pEntityId, editPostojecega: $editPostojecega ");
		//$this->poruka("snimanjeNakonEditiranja: $snimanjeNakonEditiranja , snimanjeNovog: $snimanjeNovog , kreiranjeNovog: $kreiranjeNovog, editPostojecega: $editPostojecega ");
		
		//IGOR: Treba li ovo?
		
		//Traženje varijable entity
		if ( $kreiranjeNovog || $snimanjeNovog ){
			$entity = new $class();
			
			//postavljanje svih vrijednosti na null
			$arrFieldsList = $this->n_fields_list($entity->getSource());
			$arrAssign = array();
			foreach ($arrFieldsList as $field)
				$arrAssign[$field] = null;
			$entity->assign($arrAssign);
			
			
			
			$entity->n_set_default_values($entity->getSource());
			
			if (!$entity) {
				$this->greska("Variable entity not created");
				return;
			}
		} elseif ($snimanjeNakonEditiranja || $editPostojecega){
			$entity = $entity::findFirst($pEntityId);
			if (!$entity) {
				$this->greska ("Invalid Entity ID supplied ($pEntityId)");
				return;
			}
		} else {
			$this->greska ("Ovdje ne treba biti");
			return;
		}
		
		if ( property_exists($entity, 'id') )
			if (  $entity->id > 0) $_SESSION['_context']['current_entity_id'] = $entity->id; 
		
		//dodavanje defaultnih vrijednosti ako su poslane u pozivu funkcije
		//treba proć kroz sve elemente iz default values i pravilno ih interpretirat (da se npr. stringovi prebace u integere)
		//jer putem requesta sve stiže kao text
		if ($pDefaultValues){
			$tipovi = $entity->n_field_types( $entity->getSource() );
			foreach ( $pDefaultValues as $key=>$value ){	
				if (array_key_exists($key, $tipovi)){
					$pDefaultValues[$key] = $entity->n_interpret_value_from_request( $value, $tipovi[$key] );
				}
			}
			$entity->assign($pDefaultValues);
		}
		// Nava::sysMessage($entity->toArray(), "PRIJE SNIMANJA");
		//snimanje
		if ($snimanjeNovog || $snimanjeNakonEditiranja){
			$entity->initialize_model_with_post($this->request);
			//Nava::sysMessage($entity->toArray(),"NAKON SNIMANJA");
			$greska = $entity->n_before_save($this->request);
			// Nava::sysMessage($entity->toArray());
			if ($greska) {
				$this->greska($greska);
				$greskeModela = $entity->getMessages();
				$err_msg = '';
				foreach ($greskeModela as $greska)  
					$err_msg .= $greska.", ";
				if ( strlen($err_msg) > 0 ) $this->greska('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);	

				$this->view->setVar('entity', $entity);
				$this->view->setVar('errors', $greskeModela);
				$_SESSION['_context']['errors'] = $greskeModela; //sada ide i preko konteksta
			}
			else{
				
				/* 16.1.2017 - bi li default values trebalo biti nakon initialize_model_with_post?
				 AKO BUDE TU, ONDA  Ads 2controller /crudAction ne treba mijenjati price preko sql-a
				*/
				// if ($pDefaultValues){
					// $tipovi = $entity->n_field_types( $entity->getSource() );
					// foreach ( $pDefaultValues as $key=>$value ){	
						// if (array_key_exists($key, $tipovi)){
							// $pDefaultValues[$key] = $entity->n_interpret_value_from_request( $value, $tipovi[$key] );
						// }
					// }
					// $entity->assign($pDefaultValues);
				// }				
				
				
				
				
				//ako n_before_save nije javio grešku, onda ide snimanje ili kreiranje
				$success = $snimanjeNovog ? $entity->create() : $entity->update( $this );
				//hendlanje greske
				if ($success === true){	//nema greške kod snimanja
					$greska = $entity->n_after_save($this->request);
					//error_log("BaseController:n_crud_action 9");
					if ($greska) return $this->greska($greska);
					//error_log("BaseController:n_crud_action 10");
					$this->flashSession->success('<strong> Data saved.</strong> ');
					$this->saveAudit("Model $pModelName, ID= $entity->id , saved.");
				} 
				else {//greška kod snimanja ili kreiranja
					$greskeModela = $entity->getMessages();
					foreach ($greskeModela as $greska)  
						$err_msg .= $greska.", ";
					$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);
					// $this->sysMessage("snimanjeNakonEditiranja: $snimanjeNakonEditiranja , snimanjeNovog: $snimanjeNovog , kreiranjeNovog: $kreiranjeNovog, editPostojecega: $editPostojecega");
					$this->view->setVar('errors', $greske);
					$_SESSION['_context']['errors'] = $greske; //sada ide i preko konteksta
				}
			}
		}
		
		$id = isset($entity->id) ? $entity->id : null;
		$this->view->setVar('entity', $entity);
		$this->checkIfImageExists($id);

		//trik da odmah pokazuje crvena polja koja treba unijeti;
		if ($kreiranjeNovog && $pYnShowRequiredFields){
		//error_log("BaseController:n_crud_action 13");		
			$ent2 = new $class();
			$ent2->create();
			$this->view->setVar('errors', $ent2->getMessages());
			
			$greske = $ent2->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
			$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);
		}
		
		//error_log("NAKON N_CRUD ACTION:  ".print_r($entity->toArray(),true));
		$this->assets->addCss('assets/suva/css/table_sort.css');

		//uloviti slike koje su uploadane
		if ( $id > 0 && $this->request->isPost() && array_key_exists('title', $_REQUEST) && ( $snimanjeNovog || $snimanjeNakonEditiranja  ) ){
			$mediaIds = array_key_exists ('ad_media_gallery', $_REQUEST) ? $_REQUEST['ad_media_gallery'] : array();
			$count = 0;
			$adsMedia = $entity->AdsMedia();
			$adsMedia->rewind();
			$max = count($mediaIds) > count($adsMedia) ? count( $mediaIds ) : count($adsMedia);
			$adsMedia->getFirst();
			for ( $i=1 ; $i <= $max ; $i++ ){
				$am = $adsMedia->current();
				if ( $i <= count($adsMedia) && $i <= count($mediaIds) ){
					
					//mijenja se postojeći adsMedia
					$mediaId = $mediaIds [ $i - 1 ];
					$sortIdx = $i;
					$id = $am->id;
					
					if ( $am->media_id != $mediaId || $am->sort_idx != $sortIdx ){
						//da se ne updatea ako nema potrebe
						$txtSql = " update ads_media set media_id = $mediaId,  sort_idx = $sortIdx where id = $id ";
						// $this->sysMessage ($txtSql);
						\Baseapp\Suva\Library\Nava::runSql( $txtSql );
					}
				}
				elseif ( $i > count( $adsMedia ) && $i <= count( $mediaIds ) ){
					//dodaje se novi AdsMedia
					$adId = $entity->id;
					$mediaId = $mediaIds [ $i - 1 ];
					$sortIdx = $i;
					$txtSql = "insert into ads_media ( ad_id, media_id, sort_idx ) values ( $adId, $mediaId, $sortIdx )";
					// $this->sysMessage ($txtSql);
					\Baseapp\Suva\Library\Nava::runSql( $txtSql );
					
				}
				elseif ( $i <= count( $adsMedia ) && $i > count( $mediaIds ) ){
					//briše se adsMedia
					$id = $am->id;
					$txtSql = "delete from ads_media where id = $id ";
					// $this->sysMessage ($txtSql);
					\Baseapp\Suva\Library\Nava::runSql( $txtSql );
				}
				if ( $i < count($adsMedia) ) $adsMedia->next();
			}
		}
		
		return $entity;
	}
		
	public function n_build_query( $p = null ){
        /* upotreba:
        $response = $this->n_build_query( array(
            'select_fields' => array(
                'id',
                'name',
                'type',
                'amount',
                'tag',
		     	'category_id',
				'is_active'
            ),
            'available_fields' => array(
                'id',
                'name',
                'type',
                'amount',
                'tag',
		     	'category_id',
				'is_active'
            ),
            'date_fields' => array(
                'created_at'
            ),
            'class_name' => 'Baseapp\Suva\Models\Discounts',
            'controller_name' => 'discounts',
            'pagination_limit' => 100

        ));
        
        if (is_string($response)) return $this->greska($response);
        $current_page = $response['current_page'];
		$pagination_links = $response['pagination_links'];
        
        */
        $debug = null;
        
        
        // Build the query
        
        if (!$p['select_fields']) return ("n_build_query: nije poslan popis polja za select");
        if (!$p['class_name']) return ("n_build_query: nije poslano ime klase za from");
        if (!$p['available_fields']) return ("n_build_query: nije poslan popis polja za pretragu");
        if (!$p['controller_name']) return ("n_build_query: nije poslano ime kontrolera");
        
        if (!$p['pagination_limit']) $p['pagination_limit'] = 20;
        if (!$p['date_fields']) $p['date_fields'] = array();
        
        
        $builder = $this->modelsManager->createBuilder()
            ->columns($p['select_fields'])
            ->addFrom($p['class_name']);
		
		
		if ($debug) $this->printr($_REQUEST);
		
		//DODATAK ZA PRETRAŽIVANJE
		

			$page = $this->getPageParam();
			$search_for = $this->processSearchParam();
			$mode = $this->processSearchMode();
			$dir = $this->processAscDesc('desc');
			$order_by = $this->processOrderBy('id');
			$limit = $this->processLimit();

			Tag::setDefaults(array(
				'q'         => $search_for,
				'mode'      => $mode,
				'dir'       => $dir,
				'order_by'  => $order_by,
				'limit'     => $limit,
				'user_role' => $user_role
			), true);

			if($debug) $this->poruka("page $page");
			if($debug) $this->poruka("sve");
			if($debug) $this->printr(array(
				'q'         => $search_for,
				'mode'      => $mode,
				'dir'       => $dir,
				'order_by'  => $order_by,
				'limit'     => $limit,
				'user_role' => $user_role
			));
		
		
			// Available search fields
			$available_fields = $p['available_fields'];
			$date_fields = $p['date_fields'];
        
			$field = $this->processOptions('field', $available_fields, null);
			Tag::setDefault('field', $field);
			if (null === $field || empty($field)) {
				$field = 'all';
			}


			 // If a specific field is specified, search only that one, otherwise search across all the fields
			// (but only if there's an actual search term entered)
			$fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
			$search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
			if($debug) $this->poruka("search_params");	
			if($debug) $this->printr($search_params);
			if (null !== $search_params && is_array($search_params)) {
				$conditions = array();
				$binds = array();
				if($debug) $this->poruka("test1");
				foreach ($search_params as $fld => $val) {
					$field_bind_name = sprintf(':%s:', $fld);
					if (in_array($fld, $date_fields)) {
						$conditions[] = 'DATE(FROM_UNIXTIME(' . $fld . ')) LIKE ' . $field_bind_name;
					} else {
						$conditions[] =  $fld . ' LIKE ' . $field_bind_name;
					}
					$binds[$fld] = $val;
				}
				if($debug) $this->printr($conditions);
				if($debug) $this->printr($binds);
				$builder->andWhere(implode(' OR ', $conditions), $binds);
			}

        		//PRIVREMENO ZA SORtiranja
		if ( array_key_exists('order_by', $_REQUEST) && array_key_exists('dir', $_REQUEST) ) {
			if ($_REQUEST['order_by'] && $_REQUEST['dir'] ) {
	// 			$this->poruka("IMA ORDER BY");
				$builder->orderBy((string)trim($_REQUEST['order_by'].' '.$_REQUEST['dir']));
			} 
		}
        
        $page = $this->getPageParam();
        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $p['pagination_limit'], 'page' => $page));
        $current_page = $paginator->getPaginate();
        
        $pagination_links = Tool::pagination(
            $current_page,
            'suva/'.$p['controller_name'],
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );
        
        
        $result = array(
            'current_page' => $current_page,
            'pagination_links' => $pagination_links
        );
        return $result;
    }
    

	public function n_novi_query($pClassName = null ){
		
		$orderByField = ($this->request->hasPost("__orderByField")) ? $this->request->getPost("__orderByField") : null;
		$orderByJoinedField = ($this->request->hasPost("__orderByJoinedField")) ? $this->request->getPost("__orderByJoinedField") : null;
		$orderByDirection = ($this->request->hasPost("__orderByDirection")) ? $this->request->getPost("__orderByDirection") : 'asc';
		
		$orderByField = $_REQUEST['__orderByField'] ? $_REQUEST['__orderByField']: null;
		$orderByJoinedField = $_REQUEST['__orderByJoinedField'] ? $_REQUEST['__orderByJoinedField']: null;
		$orderByDirection = $_REQUEST['__orderByDirection'] ? $_REQUEST['__orderByDirection']: null;
		
		//error_log("POST: $orderByJoinedField");	
		$className = $pClassName;
		$classPath = "Baseapp\\Models\\$className";
		//error_log($classPath);
		//error_log(print_r($this->n_table_model_mapping,true));
		$tableName = array_search($className, $this->n_table_model_mapping);
    //error_log($tableName);
		//$this->logger->info($model->getSource());
		
		//DODAVANJE IZRAZA ZA SELECT
		$fieldsList = $this->n_fields_list($tableName);
		$select = array();
		foreach($fieldsList as $field) //SELECT POLJA U GLAVNOJ TABLICI
			$select[$field] = $classPath.".".$field;
		//error_log(print_r($select,true));
			
		$fKeys = $this->n_foreign_keys($tableName);
		//error_log(print_r($fKeys,true));
		foreach ($fKeys as $fKey){ //ZA SVE TABLICE POVEZANE STRANIM KLJUČEM
			$columnName = $fKey['COLUMN_NAME'];
			$refTableName = $fKey['REFERENCED_TABLE_NAME'];
			$refClassName = $this->n_table_model_mapping[$refTableName];
			$refClassPath = "Baseapp\\Models\\$refClassName";
			$refFields = $this->n_fields_list($refTableName);
			foreach($refFields as $refField) //SELECT SVIH POLJA U JOINANIM TABLICAMA   
				//$select["_".$refClassName."_".$refField] = $refClassPath.".".$refField;
				$select["__".$columnName."__".$refField] = $refClassPath.".".$refField;
		}
		//error_log(print_r($select,true));	
		
		//DODAVANJE LEFT JOINA
		$leftJoins = array();
		foreach($fKeys as $fKey){
			$columnName = $fKey['COLUMN_NAME'];
			$refColumnName = $fKey['REFERENCED_COLUMN_NAME'];
			$refTableName = $fKey['REFERENCED_TABLE_NAME'];
			$refClassName = $this->n_table_model_mapping[$refTableName];
			$refClassPath = "Baseapp\\Models\\$refClassName";
			$values = array($refClassPath, $classPath.".".$columnName." = ".$refClassPath.".".$refColumnName );
			array_push($leftJoins,$values);
		}
		// //error_log("left_joins");
		// //error_log(print_r($leftJoins,true));
		
		//ORDER BY
		$orderBy = null;
		if ($orderByField && !$orderByJoinedField) { //ako se sortira po polju iz glavne tablice
			$orderBy = $classPath.".".$orderByField;
		} elseif ($orderByField && $orderByJoinedField){ //ako se sortira po polju iz vezane tablice
			//pronaći na koju se klasu odnosi
			foreach($fKeys as $fKey){
				if ($fKey['COLUMN_NAME'] == $orderByField)
					$orderByTableName = $fKey['REFERENCED_TABLE_NAME'];
			}
			$orderByClassName = $this->n_table_model_mapping[$orderByTableName];
			$orderByClassPath = "Baseapp\\Models\\$orderByClassName";
			$orderBy = $orderByClassPath.".".$orderByJoinedField;
		}
		if ($orderBy && $orderByDirection == 'DESC') $orderBy .=" DESC ";
		//error_log("ORDER BY ".$orderBy);
		
			//BUILDER
		$builder = $this->modelsManager->createBuilder()
				->columns($select)
				->addFrom($classPath);
		foreach ($leftJoins as $leftJoin)
			$builder->leftJoin($leftJoin[0],$leftJoin[1]);
		if ($orderBy)
			$builder->orderBy($orderBy);

		// //error_log("TU SAM");
				
	    // $builder = $this->modelsManager->createBuilder()
				// ->columns(array(
					// 'id'=>'Baseapp\Suva\Models\Discounts.id'
					// ,'name'=>'Baseapp\Suva\Models\Discounts.name'
					// ,'category_id'=>'Baseapp\Suva\Models\Discounts.category_id'
					// ,'_Categories_name' => 'Baseapp\Suva\Models\Categories.name'	
				// ))
				// ->addFrom('Baseapp\Suva\Models\Discounts')
				// ->leftJoin('Baseapp\Suva\Models\Categories')
				// ->where('1 = 1')
				// ->andWhere("Baseapp\Suva\Models\Categories.name = 'Kamioni'")
				// ->orderBy("Baseapp\Suva\Models\Categories.name");
			// return($builder->getQuery()->getSql());
			
			$page = $this->getPageParam();
					$paginator = new QueryBuilder(array('builder' => $builder, 'limit' => 20, 'page' => $page));
					$current_page = $paginator->getPaginate();
				
			//error_log(count($current_page->items));
			// foreach($current_page->items as $item)
				// //error_log(print_r($item->toArray(),true));
			
			//error_log("NA KRAJU");
			//error_log(get_class($current_page->items));
			//error_log(get_class($current_page->items[0]));
			// //error_log(print_r($current_page->items[0]->toArray(),true));
			
			// __category_id__name  => __=>category_id=>name
			foreach($current_page->items as $item){
					$temp = array();
					foreach ($item->toArray() as $key => $value){
							//error_log("KEY:".$key);
							$nameParts = explode("__", $key);
						
							if (count($nameParts) == 3){
									//error_log("nameParts:".print_r($nameParts,true));
								
									//ako ima  category_id, name
									// if (!$item->__m21__) $item->__m21__ = new \Phalcon\Mvc\Model\Row;
									// if (!$item->__m21__->$nameParts[0]) !$item->__m21__->$nameParts[0] = new \Phalcon\Mvc\Model\Row;
									if (!$temp[$nameParts[1]]) $temp[$nameParts[1]] = array();
									$temp[$nameParts[1]][$nameParts[2]] = $value;
							}
					}
					$item->__m21__ = $temp;
					//error_log("TEMP:".print_r($temp,true));
					return;
			}
			
			
			$result['paginator'] = $paginator;
			$result['current_page'] = $current_page;
			return $result;
			
    }   
	
    public function n_prepare_index_list( $p = null ){
        /* upotreba:
        $response = $this->n_build_query( array(
            'select_fields' => array(
                'id',
                'name',
                'type',
                'amount',
                'tag',
		     	'category_id',
				'is_active'
            ),
            'available_fields' => array(
                'id',
                'name',
                'type',
                'amount',
                'tag',
		     	'category_id',
				'is_active'
            ),
            'date_fields' => array(
                'created_at'
            ),
            'class_name' => 'Baseapp\Suva\Models\Discounts',
            'controller_name' => 'discounts',
            'pagination_limit' => 100

        ));
        
        if (is_string($response)) return $this->greska($response);
        $current_page = $response['current_page'];
		$pagination_links = $response['pagination_links'];
        
        */
        $debug = 1;
        
        
        // Build the query
        
        if (!$p['select_fields']) return ("n_build_query: nije poslan popis polja za select");
        if (!$p['class_name']) return ("n_build_query: nije poslano ime klase za from");
        if (!$p['available_fields']) return ("n_build_query: nije poslan popis polja za pretragu");
        if (!$p['controller_name']) return ("n_build_query: nije poslano ime kontrolera");
        
        if (!$p['pagination_limit']) $p['pagination_limit'] = 20;
        if (!$p['date_fields']) $p['date_fields'] = array();
        
        $className = $p['class_name'];
		$className = "BaseApp\\Suva\\Models\\$className";
		$model = $this->$className;
		
        $builder = $this->modelsManager->createBuilder()
			->columns($model->n_fields_list)
			->addFrom($className);
		
		
		if ($debug) $this->printr($_REQUEST);
		
		//DODATAK ZA PRETRAŽIVANJE
		

			$page = $this->getPageParam();
			$search_for = $this->processSearchParam();
			$mode = $this->processSearchMode();
			$dir = $this->processAscDesc('desc');
			$order_by = $this->processOrderBy('id');
			$limit = $this->processLimit();

			Tag::setDefaults(array(
				'q'         => $search_for,
				'mode'      => $mode,
				'dir'       => $dir,
				'order_by'  => $order_by,
				'limit'     => $limit,
				'user_role' => $user_role
			), true);

			if($debug) $this->poruka("page $page");
			if($debug) $this->poruka("sve");
			if($debug) $this->printr(array(
				'q'         => $search_for,
				'mode'      => $mode,
				'dir'       => $dir,
				'order_by'  => $order_by,
				'limit'     => $limit,
				'user_role' => $user_role
			));
		
		
			// Available search fields
			$available_fields = $p['available_fields'];
			$date_fields = $p['date_fields'];
        
			$field = $this->processOptions('field', $available_fields, null);
			Tag::setDefault('field', $field);
			if (null === $field || empty($field)) {
				$field = 'all';
			}


			 // If a specific field is specified, search only that one, otherwise search across all the fields
			// (but only if there's an actual search term entered)
			$fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
			$search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
			if($debug) $this->poruka("search_params");	
			if($debug) $this->printr($search_params);
			if (null !== $search_params && is_array($search_params)) {
				$conditions = array();
				$binds = array();
				if($debug) $this->poruka("test1");
				foreach ($search_params as $fld => $val) {
					$field_bind_name = sprintf(':%s:', $fld);
					if (in_array($fld, $date_fields)) {
						$conditions[] = 'DATE(FROM_UNIXTIME(' . $fld . ')) LIKE ' . $field_bind_name;
					} else {
						$conditions[] =  $fld . ' LIKE ' . $field_bind_name;
					}
					$binds[$fld] = $val;
				}
				if($debug) $this->printr($conditions);
				if($debug) $this->printr($binds);
				$builder->andWhere(implode(' OR ', $conditions), $binds);
			}

        		//PRIVREMENO ZA SORtiranja
		if ($_REQUEST['order_by'] && $_REQUEST['dir'] ) {
// 			$this->poruka("IMA ORDER BY");
			$builder->orderBy((string)trim($_REQUEST['order_by'].' '.$_REQUEST['dir']));
		} 
		
        
        $page = $this->getPageParam();
        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $p['pagination_limit'], 'page' => $page));
        $current_page = $paginator->getPaginate();
        
        $pagination_links = Tool::pagination(
            $current_page,
            'suva/'.$p['controller_name'],
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );
        
        
        $result = array(
            'current_page' => $current_page,
            'pagination_links' => $pagination_links
        );
        return $result;
    }
	
	public function n_errors_to_volt($pEntity = null){
		foreach ($pEntity->getMessages() as $message)  
			$err_msg .= $message.", ";
		$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);
		$this->view->setVar('errors', $created);
	}
	
	//FUNKCIJE ZA DOHVAT POLJA, STRANIH KLJUČEVA I SL
    public function n_fields_list($pTableName = null){
        $rows = Nava::sqlToArray("select COLUMN_NAME from information_schema.columns where TABLE_NAME = '$pTableName' group by COLUMN_NAME ");
        $result = array();
        foreach($rows as $row)
            array_push($result, $row['COLUMN_NAME']);
        return $result;
    }
    
    public function n_foreign_keys($pTableName = null){
        $result = Nava::sqlToArray("select COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME from information_schema.key_column_usage where TABLE_NAME = '$pTableName'  and REFERENCED_TABLE_NAME is not null");
        return $result;
    }
    
    public function n_data_type_to_sql_string ($pDataType, $pValue){
        
        switch($pDataType){
            case 'bigint':
            case 'int':
            case 'smallint':
            case 'mediumint':
            case 'tinyint':
                return " ".(string)$pValue." ";
            case 'decimal':
            case 'double':
                return " ".(string)$pValue." ";
            case 'varchar':
            case 'char':
            case 'varchar':
            case 'longtext':
            case 'mediumtext':
            case 'text':
            case 'longtext':
                return " '".$pValue."' ";
            default:
                return null;
        }
    }
	
	// NIKOLA DODANO RADI USER LINK KOJI TREBA VODIT NA MOJ KUTAK
	// public function setLayoutFeature($feature, $value)
    // {
        // $this->layout_features[$feature] = (bool) $value;
    // }

    /**
     * @param string $feature
     *
     * @return bool
     */
    public function hasLayoutFeature($feature)
    {
        return (isset($this->layout_features[$feature]) && $this->layout_features[$feature]);
    }

    /**
     * @param array $features
     */
    public function setLayoutFeatures(array $features)
    {
        $this->layout_features = $features;
    }
	
	public function checkIfImageExists($entity_id){
		$imageName = $this->request->getPost('imageUpload');
		$str = explode(".",$imageName);
		if ( count ($str) == 2 ) {
			$imageExtension = $str[1];
			//error_log($imageName);
			if($imageName > ''){
				$targetPath = "repository/dtp/originals/".$imageName; // Path di je spremljena slika
				$targetPath2 = "repository/dtp/to_process/".$imageName;
 
				$renamedPath = "repository/dtp/originals/".$entity_id.".".$imageExtension;
		
				if (file_exists($targetPath)) {
					rename($targetPath, "repository/dtp/originals/".$entity_id.".".$imageExtension);
					Nava::runSql("update ads set n_picture_path = '$renamedPath' where id = $entity_id");
				}
				if (file_exists($targetPath2)) {
					rename($targetPath2, "repository/dtp/to_process/".$entity_id.".".$imageExtension);
				} 
			}
		}
	}
	
	
	public function fill_SESSIONDataForSuva( $pForceRefresh = false ){
		
		//_context
		if ( !array_key_exists('_context', $_SESSION) ) {
			$_SESSION['_context'] = array();
		}
		
		//_app_settings
		if ( !array_key_exists('_app_settings', $_SESSION ) || $pForceRefresh ) {
			//Nava::sysMessage("ne postoji appsettings");
			$_SESSION['_app_settings'] = array();
			$appSettings = \Baseapp\Suva\Models\AppSettings::find();
			foreach ( $appSettings as $appSetting ){
				$_SESSION['_app_settings'][ $appSetting->name ] = \Baseapp\Suva\Library\libAppSetting::get( $appSetting->name );
			}	
			//Nava::sysMessage( $_SESSION['_app_settings'],"CREATED APPSETTINGS");
		}
		else{
			//Nava::sysMessage("postoji appsettings");
		}
		
		//_translations
		// if ( !array_key_exists('_translations', $_SESSION )|| $pForceRefresh ) {
			// $lang = $_SESSION['_app_settings']['sys_current_language'];
			// //$this->flashSession->success("LANG = $lang");
			// if ($lang === null ){
				// Nava::sysError("Current language not set in the system. Setting to Default");
			// }
			// elseif ( $lang === 'ORIGINAL' ){
				// //ništa se ne ubacuje, ne koriste se prijevodi
			// }
			// else{
				// //$this->flashSession->success("$lang");
				// $_SESSION['_translations'] = array();
				// //puni se array s prijevodom jezika
				// $sysTranslations = \Baseapp\Suva\Models\SysTranslations::find(" lang = '$lang' ");
				// foreach ($sysTranslations as $sysTranslation){
					// $_SESSION['_translations'][ $sysTranslation->original ] = $sysTranslation->translation;
				// }
				// Nava::sysMessage($_SESSION['_translations'],"CREATED TRANSLATIONS");
			// }
		// }
		
	}
}
