<?php
namespace Baseapp\Suva\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Library\Moderation;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Library\Avus\ExportToAvus;
use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\N_Publications;
use Baseapp\Suva\Models\N_Products;
use Baseapp\Suva\Models\N_Issues;
use Baseapp\Suva\Models\AdsHomepage;
use Baseapp\Suva\Models\Categories;

use Baseapp\Suva\Models\Users; //igor AAA
use Baseapp\Suva\Models\OrdersItems; //igor da se nađu povezani order itemi sa ad-om ;
use Baseapp\Suva\Library\Nava; // igor - radi sqlToArray
use Baseapp\Suva\Models\Products;
use Baseapp\Suva\Models\Orders;
use Baseapp\Suva\Models\OrdersItemsEx;
use Baseapp\Suva\Models\OrdersStatuses;
use Baseapp\Suva\Models\Publications;
use Baseapp\Suva\Models\Actions;

use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Baseapp\Library\Products\ProductsSelector;
use Phalcon\Paginator\Adapter\QueryBuilder;

/**
 * Suva Ads Controller
*/ 
class AdsController extends IndexController {
    use ParametrizatorHelpers;
    use CrudActions;
    use CrudHelpers;

    public $crud_model_class = 'Baseapp\Suva\Models\Ads';

    // Overriding the default list of suva-allowed roles to exclude 'content' here
    protected $allowed_roles = array('admin', 'support', 'finance', 'moderator');

	
    public function createAction() {
        //$this->poruka("adscontroller:cretae: REQUEST ".print_r($_REQUEST, true));
		// Only 'admin' and 'support' roles are allowed to create ads from the backend
        if (!$this->auth->logged_in(array('admin', 'supersupport', 'sales'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('suva/ads2');
        }

		//selected_category_id može i preko contexta
		if (array_key_exists ('selected_category_id', $_SESSION['_context'] ) ){
			$selected_category_id = (int)$_SESSION['_context']['selected_category_id'];
		}
		else{
			if (!$this->request->has('category_id')) {
				$this->flashSession->error('Missing required category ID for create action!');
				return $this->redirect_to('suva/ads2');
			}
			$selected_category_id = (int)$this->request->get('category_id');
		}

        if (!$selected_category_id) {
            $this->flashSession->error('Invalid category ID!');
            return $this->redirect_to('suva/ads2');
        }

        $selected_category = Categories::findFirst($selected_category_id);

        if (!$selected_category) {
            $this->flashSession->error(sprintf('Category not found [ID: %s]', $selected_category));
            return $this->redirect_back();
        } else {
            if (0 == count($selected_category->FieldsetParameters)) {
                $this->flashSession->error('Selected category does not have parameters set up!');
                return $this->redirect_back();
            }

            $this->tag->setTitle('Create new ad');
            $this->view->setVar('form_title_long', 'Create new ad');
            $this->view->setVar('form_action', 'add');
            $this->add_common_crud_assets();
            // Use the same form/view as editAction
            $this->view->pick('ads2/create');

            $ad = new Ads();
            $ad->created_by_user_id = $this->auth->get_user()->id;
            $parametrizator = new Parametrizator();
            $parametrizator->setModule('backend');
            $parametrizator->setCategory($selected_category);
            $parametrizator->setAd($ad);

            if ($this->request->isPost()) {
                $created = $ad->backend_add_new($this->request);
                if ($created instanceof Ads) {
                    $this->flashSession->success('<strong>Ad created!</strong> ' . $ad->get_frontend_view_link('html'));
                    $save_action = $this->get_save_action();
                    $next_url = $this->get_next_url();
					Nava::runSql("update ads set active = 0 where id = ".$created->id);
                    // Override next url to go to entity edit in save button case
                    if ('save' === $save_action) {
						$_SESSION['_context']['_active_tab'] = 'users_ads';
						$_SESSION['_context']['_keep_active_tab_setting'] = true;
						$next_url = 'suva/ads2/edit/0/' . $ad->user_id;
                    }
                    return $this->redirect_to($next_url);
                } else {
                    $this->view->setVar('errors', $created);
                    $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                    $parametrizator->setRequest($this->request);
                    $parametrizator->setErrors($created);
                }
            }
            //$this->setup_products_chooser($ad, $selected_category);
            $this->setup_rendered_parameters($parametrizator->render_input());
            $this->view->setVar('category', $selected_category);
            $this->view->setVar('ad', $ad);
            $this->view->setVar('show_save_buttons', true);
        }
    }
	
	
    /**
     * Index Action
     */
	public function indexActionOLD() {
        $this->tag->setTitle('Ads');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode('exact');
        $dir = $this->processAscDesc('desc');
        $order_by = $this->processOrderBy('id');
        $limit = $this->processLimit();
        $moderation = $this->processFilterModeration();
        $product = $this->processFilterProducts();
        $payment_state = $this->processFilterPaymentState();

        Tag::setDefaults(array(
            'q'        => $search_for,
            'product'  => $product,
            'mode'     => $mode,
            'dir'      => $dir,
            'order_by' => $order_by,
            'limit'    => $limit
        ), true);

        // Available search fields
        $available_fields = array(
            'id',
            'title',
            'description',
            'active',
            'user_id',
            'username',
            'online_product_id',
            'created_at',
            'first_published_at',
            'modified_at',
            'expires_at',
            'sort_date',
            'is_spam',
            'soft_delete',
            'import_id',
            'avus_id',
            'remark',
			'phone1'
        );
        $date_fields = array(
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date'
        );
        $field = $this->processOptions('field', $available_fields, 'id');
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                'ad.*',
                'user.type AS user_type',
                'user.username AS user_username',
                'user.remark AS user_remark',
                'shop.title AS user_shop_title'
            ))
            ->addFrom('Baseapp\Suva\Models\Ads', 'ad')
            ->innerJoin('Baseapp\Suva\Models\Users', 'ad.user_id = user.id', 'user')
            ->leftJoin('Baseapp\Suva\Models\UsersShops', 'ad.user_id = shop.user_id', 'shop')
            ->leftJoin('Baseapp\Suva\Models\AdsAdditionalData', 'ad.id = aad.ad_id', 'aad')
            ->where('ad.id > 0');   // for query optimization because of GROUP BY usage

        // only if there is a filtering by a specific category, inner join categories model

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
		
		
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
			//if($debug) $this->poruka("test1");
            foreach ($search_params as $fld => $val) {
				//$this->poruka("test2");
                $field_bind_name = sprintf(':%s:', $fld);
// 				$this->poruka(print_r($date_fields,true));
                if (in_array($fld, $date_fields)) {
					//$this->poruka("test3");
                    $conditions[] = 'DATE(FROM_UNIXTIME(ad.' . $fld . ')) LIKE ' . $field_bind_name;
// 					$this->poruka("test");
// 					$this->poruka(print_r($conditions,true));
                } else {
//					if($debug) $this->poruka("test4");
                    if ('username' === $fld) {
//						if($debug) $this->poruka("a");
                        $conditions[] = 'user.' . $fld . ' LIKE ' . $field_bind_name;
                    } elseif (in_array($fld, array('import_id', 'avus_id', 'remark'))) {
//						if($debug) $this->poruka("b");
                        $conditions[] = 'aad.' . $fld . ' LIKE ' . $field_bind_name;
                    } else {
//						if($debug) $this->poruka("c");
                        $conditions[] = 'ad.' . $fld . ' LIKE ' . $field_bind_name;
                    }
                }
                $binds[$fld] = $val;
				//$this->poruka("test5");
// 				$this->poruka(print_r($binds));
            }
            $builder->andWhere(implode(' OR ', $conditions), $binds);
//			$this->poruka(print_r($conditions,true));
//            $this->poruka(print_r($binds,true));
        }
// 		$this->poruka(print_r($search_params,true));
// 		$this->poruka(print_r($field,true));
        // $builder->groupBy('ad.id');
        $builder->orderBy('ad.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $limit, 'page' => $page));
        
// 		$intermediate = $paginator->getQuery()->parse();
// 		$dialect      = DI::getDefault()->get('db')->getDialect();
// 		$sql          = $dialect->select($intermediate);
// 		$this->poruka("SQL:".$sql);
		
		
//		$this->poruka(print_r($binds,true));
		$current_page = $paginator->getPaginate();
		//$this->poruka("broj komada ".count($current_page->items));

        // Process the resulting page and extend with some additional data (ad image if it exists)
        $results_array = Ads::buildResultsArray($current_page->items);
//		$this->poruka(print_r($results_array,true));
        $current_page->items = $results_array;
//		$this->poruka(print_r($current_page,true));
        $pagination_links = Tool::pagination(
            $current_page,
            'admin/ads',
            'pagination',
            $this->config->settingsSuva->pagination_count_out,
            $this->config->settingsSuva->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
		
        $this->assets->addJs('assets/suva/js/classifieds-index.js');
    }
	
    public function indexAction() {
		$debug = null; 
		
        $this->tag->setTitle('Ads');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode('exact');
        $dir = $this->processAscDesc('desc');
        $order_by = $this->processOrderBy('id');
        $limit = $this->processLimit();
        $moderation = $this->processFilterModeration();
        $product = $this->processFilterProducts();
        $payment_state = $this->processFilterPaymentState();

        Tag::setDefaults(array(
            'q'        => $search_for,
            'product'  => $product,
            'mode'     => $mode,
            'dir'      => $dir,
            'order_by' => $order_by,
            'limit'    => $limit
        ), true);
	
		if ($debug) $this->printr($_REQUEST);
		
        $filter_category_id = null;
        $filter_category = null;
        if ($this->request->has('filter_category_id')) {
            $filter_category_id = $this->request->get('filter_category_id', 'int', null);
            $filter_category = Categories::findFirst($filter_category_id);
        }

        // Filter category tree (view variable) + category dropdown generation
        $this->category_id_dropdown(
            'filter_category_id',
            $filter_category_id,
            array(
                'emptyText' => 'Any category',
                'subtext'   => false
            )
        );

        // Populates the hidden dropdown for the 'create new ad' modal
        $this->category_id_dropdown(
            'category_id',
            $filter_category_id,
            array(
                'disabled' => Categories::getCategoriesWithoutTransactionType()
            )
        );

        // Available search fields
        $available_fields = array(
            'id',
            'title',
            'description',
            'active',
            'user_id',
            'username',
            'online_product_id',
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date',
            'is_spam',
            'soft_delete',
            'import_id',
            'avus_id',
            'remark',
			'phone1'
        );
        $date_fields = array(
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date'
        );
        $field = $this->processOptions('field', $available_fields, 'id');
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                'ad.*',
                'user.type AS user_type',
                'user.username AS user_username',
                'user.remark AS user_remark',
                'shop.title AS user_shop_title'
            ))
            ->addFrom('Baseapp\Suva\Models\Ads', 'ad')
            ->innerJoin('Baseapp\Suva\Models\Users', 'ad.user_id = user.id', 'user')
            ->leftJoin('Baseapp\Suva\Models\UsersShops', 'ad.user_id = shop.user_id', 'shop')
            ->leftJoin('Baseapp\Suva\Models\AdsAdditionalData', 'ad.id = aad.ad_id', 'aad')
            ->where('ad.id > 0');   // for query optimization because of GROUP BY usage

//         // only if there is a filtering by a specific category, inner join categories model
//         if (null !== $filter_category) {
//             $builder->innerJoin('Baseapp\Suva\Models\Categories', 'ad.category_id = category.id', 'category');
//             $builder->andWhere(
//                 'category.lft >= :category_left: AND category.rght <= :category_right:',
//                 array(
//                     'category_left'  => $filter_category->lft,
//                     'category_right' => $filter_category->rght
//                 )
//             );
//         }

        if (null !== $moderation) {
            $builder->andWhere('ad.moderation = :moderation:', array('moderation' => $moderation));
        }

        if (null !== $product) {
            $product_details = explode('_', $product);
            $builder->andWhere('ad.' . strtolower($product_details[0]) . '_product_id = :product:', array('product' => $product_details[1]));
        }

        if ($payment_state) {
            $builder->andWhere('ad.latest_payment_state = :payment_state:', array('payment_state' => $payment_state));
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
//			if($debug) $this->poruka("test1");
            foreach ($search_params as $fld => $val) {
//				if($debug) $this->poruka("test2");
                $field_bind_name = sprintf(':%s:', $fld);
// 				$this->poruka(print_r($date_fields,true));
                if (in_array($fld, $date_fields)) {
					if($debug) $this->poruka("test3");
                    $conditions[] = 'DATE(FROM_UNIXTIME(ad.' . $fld . ')) LIKE ' . $field_bind_name;
// 					$this->poruka("test");
// 					$this->poruka(print_r($conditions,true));
                } else {
					if($debug) $this->poruka("test4");
                    if ('username' === $fld) {
						if($debug) $this->poruka("a");
                        $conditions[] = 'user.' . $fld . ' LIKE ' . $field_bind_name;
                    } elseif (in_array($fld, array('import_id', 'avus_id', 'remark'))) {
						if($debug) $this->poruka("b");
                        $conditions[] = 'aad.' . $fld . ' LIKE ' . $field_bind_name;
                    } else {
						if($debug) $this->poruka("c");
                        $conditions[] = 'ad.' . $fld . ' LIKE ' . $field_bind_name;
                    }
                }
                $binds[$fld] = $val;
//				if($debug) $this->poruka("test5");
// 				$this->poruka(print_r($binds));
            }
            $builder->andWhere(implode(' OR ', $conditions), $binds);
//			if($debug) $this->poruka(print_r($conditions,true));
 //           if($debug) $this->poruka(print_r($binds,true));
        }
// 		$this->poruka(print_r($search_params,true));
// 		$this->poruka(print_r($field,true));
        // $builder->groupBy('ad.id');
        $builder->orderBy('ad.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $limit, 'page' => $page));
        
// 		$intermediate = $paginator->getQuery()->parse();
// 		$dialect      = DI::getDefault()->get('db')->getDialect();
// 		$sql          = $dialect->select($intermediate);
// 		$this->poruka("SQL:".$sql);
		
		
//		if($debug) $this->poruka(print_r($binds,true));
		$current_page = $paginator->getPaginate();
//		if($debug) $this->poruka("broj komada ".count($current_page->items));

        // Process the resulting page and extend with some additional data (ad image if it exists)
        $results_array = Ads::buildResultsArray($current_page->items);
//		if($debug) $this->poruka(print_r($results_array,true));
        $current_page->items = $results_array;
//		if($debug) $this->poruka(print_r($current_page,true));
        $pagination_links = Tool::pagination(
            $current_page,
            'admin/ads',
            'pagination',
            $this->config->settingsSuva->pagination_count_out,
            $this->config->settingsSuva->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
		
        $this->assets->addJs('assets/suva/js/classifieds-index.js');
    }
	
	public function createActionOLD() {
        // Only 'admin' and 'support' roles are allowed to create ads from the suva
        if (!$this->auth->logged_in(array('admin', 'support'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('admin/ads');
        }

        if (!$this->request->has('category_id')) {
            $this->flashSession->error('Missing required category ID for create action!');
            return $this->redirect_to('admin/ads');
        }
		
        $selected_category_id = (int)$this->request->get('category_id');

        if (!$selected_category_id) {
            $this->flashSession->error('Invalid category ID!');
            return $this->redirect_to('admin/ads');
        }
		
        $selected_category = Categories::findFirst($selected_category_id);

        if (!$selected_category) {
            $this->flashSession->error(sprintf('Category not found [ID: %s]', $selected_category));
            return $this->redirect_back();
        }

		if (0 == count($selected_category->FieldsetParameters)) {
			$this->flashSession->error('Selected category does not have parameters set up!');
			return $this->redirect_back();
		}

		$this->tag->setTitle('Create new ad');
		$this->view->setVar('form_title_long', 'Create new ad');
		$this->view->setVar('form_action', 'add');

		

		$this->add_common_crud_assets();
		// Use the same form/view as editAction
		$this->view->pick('ads/edit');

		$ad = new Ads();
		$ad->created_by_user_id = $this->auth->get_user()->id;

		$parametrizator = new Parametrizator();
		$parametrizator->setModule('suva');
		$parametrizator->setCategory($selected_category);
		$parametrizator->setAd($ad);

		//IGOR - SELECTED USER REC
		if ($this->request->has('user_id')) {
			$selected_user_id = (int)$this->request->get('user_id');
			$selected_user = Users::findFirst($selected_user_id);

			//INT58 - izmijeniti createActiuon na Adu da odmah dodaje telefone od usera
			$ad->phone1 = $selected_user->phone1;
			$ad->phone2 = $selected_user->phone2;

			if (!is_bool($selected_user)) {
				$selected_user = $selected_user->toArray();
				$this->view->setVar('selected_user_rec', $selected_user );
			} else {
				//nakon snimanja ada findfirst vraća boolean i ruši se server
			}
		}

		//SNIMANJE
		if ($this->request->isPost()) {
			$created = $ad->suva_add_new($this->request);
 
			if ($created instanceof Ads) {
				$this->flashSession->success('<strong>Ad created!</strong> ' . $ad->get_frontend_view_link('html'));
				$save_action = $this->get_save_action();
				$next_url = $this->get_next_url();

				
				$ad_taker_id = $this->auth->get_user()->id;

				Nava::runSql("update ads set created_by_user_id = $ad_taker_id  where id =".$created->id); 
				Nava::runSql("update ads set online_product_id = 1 where id =".$created->id); 
				Nava::runSql("update ads set online_product = 'O:47:' where id =".$created->id); 
				
				// Override next url to go to entity edit in save button case
				if ('save' === $save_action) {
					$next_url = 'admin/ads/edit/' . $ad->id;
				}

				//IGOR -- OVDJE TREBA PROVJERIT JE LI VEĆ POSTOJI ONLINE ORDER LINE ZA SPREMLJENI AD
// 							$this->flashSession->error(sprintf('TU SAM'));
//							return $this->redirect_back();

				$this->insertToCartDefaultOnlineProduct($created->id);


				return $this->redirect_to($next_url);
			} else {
				foreach ($ad->getMessages() as $message)  $err_msg .= $message.", ";
				$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);
				$this->view->setVar('errors', $created);
				//$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.print_r($created));
				$parametrizator->setRequest($this->request);
				$parametrizator->setErrors($created);
			}
		}
	   // if (!$created instanceof Ads) {
	   //     $this->view->setVar('products_chooser_markup', 'Save Ad first.');
	   // }else{
			$this->setup_products_chooser($ad, $selected_category);
		//}

				//dodat usera ako postoji



		$this->setup_rendered_parameters($parametrizator->render_input());
		$this->view->setVar('category', $selected_category);
		$this->view->setVar('ad', $ad);
		$this->view->setVar('show_save_buttons', true);
		
		
        
    }
	
    protected function getPossibleOrderByOptions() {
        $order_bys = array(
            'id'                 => 'ID',
            'created_at'         => 'Date created',
            'modified_at'        => 'Date modified',
            'first_published_at' => 'First Publish date',
            'published_at'       => 'Last Publish date',
            'sort_date'          => 'Virtual publish date',
            'expires_at'         => 'Date expired',
            'title'              => 'Title'
        );

        return $order_bys;
    }

    /**
     * Helper method to process moderation filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterModeration($default = null) {
        $moderation = new Moderation();

        $moderation_option = $this->processOptions(
            'moderation',
            array_merge(
                array('' => 'Any moderation'),
                $moderation->getPossibleModerationStatuses()
            ),
            (string) $default,
            'string'
        );

        if (trim($moderation_option)) {
            return (string)$moderation_option;
        }

        return null;
    }

    /**
     * Helper method to process products filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterProducts($default = null) {
        $selector = new ProductsSelector();

        $product_option = $this->processOptions(
            'product',
            array_merge(
                array('' => 'Any product'),
                $selector->getPossibleIdsList()
            ),
            (string) $default,
            'string'
        );

        if (trim($product_option)) {
            return (string)$product_option;
        }

        return null;
    }

    /**
     * Helper method to process ad's payment state filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterPaymentState($default = null) {
        $payment_state_option = $this->processOptions(
            'payment_state',
            array_merge(
                array('0' => 'Any payment state'),
                (new Ads())->getAllPaymentStates()
            ),
            (string) $default,
            'string'
        );

        if (trim($payment_state_option)) {
            return (string)$payment_state_option;
        }

        return null;
    }

    /**
     * Helper method to process ad's payment type filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterPaymentType($default = null) {
        $payment_type_option = $this->processOptions(
            'payment_type',
            array_merge(
                array('0'       => 'Any payment type'),
                array('paid'    => 'Paid ads'),
                array('ordered' => 'Ordered paid ads'),
                array('free'    => 'Free ads')
            ),
            (string) $default,
            'string'
        );

        if (trim($payment_type_option)) {
            return (string)$payment_type_option;
        }

        return null;
    }

    /**
     * Helper method to generate moderation dropdowns for ad editing..
     *
     * @param string $status Default status
     * @param string $reason Default reason
     */
    protected function processModerationReasons($status, $reason) {
        $moderation = new Moderation();
        $moderation_statuses = $moderation->getPossibleModerationStatuses();
        $moderation_reasons = $moderation->getModerationReasonsData();
        $moderation_statuses_final = array();
        $moderation_reasons_final = array();

        foreach ($moderation_statuses as $k => $title) {
            if (isset($moderation_reasons[$k])) {
                $moderation_statuses_final[$k] = array(
                    'text' => $title,
                    'attributes' => array(
                        'data-has-reasons' => 1
                    )
                );

                $curr_reason = $reason;

                $available_reasons = array();
                foreach ($moderation_reasons[$k] as $row_key => $row_data) {
                    $available_reasons[$row_key] = $row_data['title'];
                }


                // Prevent invalid choices from being set as default
                if (!array_key_exists($curr_reason, $available_reasons)) {
                    $curr_reason = null;
                }

                // Preselected value, if there's a valid one
                if ($curr_reason) {
                    Tag::setDefault('moderation_reason_' . $k, $curr_reason);
                }

                // Default options
                $moderation_reason_options = array(
                    'moderation_reason_' . $k,
                    $available_reasons,
                    'useEmpty'   => false,
                    'class'      => 'form-control show-tick moderation-reasons'
                );
                // Build the dropdown
                $moderation_reasons_final[$k] = Tag::select($moderation_reason_options);
            } else {
                $moderation_statuses_final[$k] = array(
                    'text' => $title,
                    'attributes' => array(
                        'data-has-reasons' => 0
                    )
                );
            }
        }

        // Prevent invalid choices from being set as default
        if (!array_key_exists($status, $moderation_statuses_final)) {
            $status = null;
        }

        // Preselected value, if there's a valid one
        if ($status) {
            Tag::setDefault('moderation', $status);
        }

        // Default options
        $moderation_select_options = array(
            'moderation',
            $moderation_statuses_final,
            'useEmpty'   => false,
            'class'      => 'form-control show-tick'
        );

        // Build the dropdown
        $moderation_dropdown = Tag::select($moderation_select_options);
        $this->view->setVar('moderation_dropdown', $moderation_dropdown);
        $this->view->setVar('moderation_reasons', $moderation_reasons_final);
    }

    /**
     * @param string $dropdown_field_name Name/ID of the dropdown
     * @param int|null $selected Value to preselect
     * @param array|null $options Tag::select() option overrides
     *
     * @return array|null
     */
    public function category_id_dropdown($dropdown_field_name, $selected = null, $options = array()) {

        // Category tree is used so we don't have to call getPath for every node,
        // (and instead just get already queried data) + to populate the categories dropdown
        $model = new Categories();
        $tree = $this->getDI()->get('categoryTree');
        $selectables = $model->getSelectablesFromTreeArray($tree, '', $with_root = false, $depth = null, $breadcrumbs = true);

        // Prevent invalid choices from being set as default
        if (!array_key_exists($selected, $selectables)) {
            $selected = null;
        }

        // Preselected value, if there's a valid one
        if ($selected) {
            Tag::setDefault($dropdown_field_name, $selected);
        }

        // Process $selectables before using them in Tag::select() in order to
        // display a more useful dropdown (for when it's not being handled by selectpicker())
        if (isset($options['subtext']) && !$options['subtext']) {
            foreach ($selectables as $k => $data) {
                if (isset($data['attributes'])) {
                    if (isset($data['attributes']['data-subtext'])) {
                        $subtext = $data['attributes']['data-subtext'];
                        $selectables[$k]['text'] = $subtext . ' › ' . $selectables[$k]['text'];
                        unset($selectables[$k]['attributes']['data-subtext']);
                    }
                }
            }
            unset($options['subtext']);
        }

        // Default options
        $defaults = array(
            $dropdown_field_name,
            $selectables,
            'useEmpty'   => true,
            'emptyText'  => 'Choose category...',
            'emptyValue' => '',
            'class'      => 'form-control'
        );

        // Merge passed in $options
        $options = array_merge($defaults, $options);

        // Build the dropdown
        $category_id_dropdown = Tag::select($options);
        $this->view->setVar($dropdown_field_name . '_dropdown', $category_id_dropdown);

        // Set the entire tree as a view variable (for displaying an article's category path or similar)
        $this->view->setVar('tree', $tree);

        return $tree;
    }

    /**
     * AwaitingModeration Action
     */
    public function awaitingModerationAction() {
        $this->tag->setTitle('Ads awaiting moderation');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode();
        $dir = $this->processAscDesc('asc');
        $order_by = $this->processOrderBy('created_at');
        $payment_type = $this->processFilterPaymentType();
        $limit = $this->processLimit();

        Tag::setDefaults(array(
            'q'            => $search_for,
            'mode'         => $mode,
            'dir'          => $dir,
            'order_by'     => $order_by,
            'payment_type' => $payment_type,
            'limit'        => $limit
        ), true);

        $filter_category_id = null;
        $filter_category = null;
        if ($this->request->has('filter_category_id')) {
            $filter_category_id = $this->request->get('filter_category_id', 'int', null);
            $filter_category = Categories::findFirst($filter_category_id);
        }

        // Filter category tree (view variable) + category dropdown generation
        $this->category_id_dropdown(
            'filter_category_id',
            $filter_category_id,
            array(
                'emptyText' => 'Any category',
                'subtext'   => false
            )
        );

        // Populates the hidden dropdown for the 'create new ad' modal
        $this->category_id_dropdown(
            'category_id',
            $filter_category_id,
            array(
                'disabled' => Categories::getCategoriesWithoutTransactionType()
            )
        );

        // Available search fields
        $available_fields = array(
            'id',
            'title',
            'description',
            'active',
            'user_id',
            'username',
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date',
            'import_id'
        );
        $date_fields = array(
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date'
        );
        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        $acceptable_payment_states = array(
            Ads::PAYMENT_STATE_FREE,
            Ads::PAYMENT_STATE_ORDERED,
            Ads::PAYMENT_STATE_PAID
        );

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                'ad.*',
                'user.type AS user_type',
                'user.username AS user_username',
                'user.remark AS user_remark',
                'shop.title AS user_shop_title'
            ))
            ->addFrom('Baseapp\Suva\Models\Ads', 'ad')
            ->innerJoin('Baseapp\Suva\Models\Users', 'ad.user_id = user.id', 'user')
            ->leftJoin('Baseapp\Suva\Models\UsersShops', 'ad.user_id = shop.id', 'shop')
            ->where('ad.id > 0 AND ad.soft_delete = 0')   // for query optimization because of GROUP BY usage
            ->andWhere('ad.online_product_id IS NOT NULL')
            ->andWhere(
                'ad.moderation = :moderation_status:',
                array(
                    'moderation_status' => 'waiting'
                )
            )
            ->andWhere('ad.latest_payment_state IN (' . implode(',', $acceptable_payment_states) . ')');

        // only if there is a filtering by a specific category, inner join categories model
        if (null !== $filter_category) {
            $builder->innerJoin('Baseapp\Suva\Models\Categories', 'ad.category_id = category.id', 'category');
            $builder->andWhere(
                'category.lft >= :category_left: AND category.rght <= :category_right:',
                array(
                    'category_left'  => $filter_category->lft,
                    'category_right' => $filter_category->rght
                )
            );
        }

        if (null !== $payment_type) {
            if ('paid' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_PAID));
            } else if ('ordered' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_ORDERED));
            } else if ('free' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_FREE));
            }
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                if (in_array($fld, $date_fields)) {
                    $conditions[] = 'DATE(FROM_UNIXTIME(ad.' . $fld . ')) LIKE ' . $field_bind_name;
                } else {
                    if ('username' === $fld) {
                        $conditions[] = 'user.' . $fld . ' LIKE ' . $field_bind_name;
                    } else {
                        $conditions[] = 'ad.' . $fld . ' LIKE ' . $field_bind_name;
                    }
                }
                $binds[$fld] = $val;
            }
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        // $builder->groupBy('ad.id');
        $builder->orderBy('ad.active ASC, ad.product_sort ASC, ad.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $limit, 'page' => $page));
        $current_page = $paginator->getPaginate();

        // Process the resulting page and extend with some additional data (ad image if it exists)
        $results_array = Ads::buildResultsArray($current_page->items);
        $current_page->items = $results_array;

        $pagination_links = Tool::pagination(
            $current_page,
            'admin/ads/awaiting-moderation',
            'pagination',
            $this->config->settingsSuva->pagination_count_out,
            $this->config->settingsSuva->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);

        $this->assets->addJs('assets/suva/js/classifieds-index.js');
    }


    public function add_common_crud_assets() {
        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');

        $this->assets->addCss('assets/vendor/magicsuggest/magicsuggest-min.css');
        $this->assets->addJs('assets/vendor/magicsuggest/magicsuggest-min.js');

        $this->assets->addCss('assets/vendor/datepicker3.css');
        $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');

        $this->assets->addJs('assets/suva/js/classifieds-edit.js');;

        $this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');
        $this->assets->addJs('assets/js/classified-submit-choose-ad-type.js');
		
		
		
		
		
		
		$this->assets->addJs('assets/vendor/jquery.mask.min.js');
        $this->assets->addJs('assets/suva/js/issues-edit.js');
        $this->assets->addJs('assets/suva/js/users-edit.js');
        $this->assets->addJs('assets/suva/js/input_search.js');
        
        
        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');

        $this->assets->addCss('assets/vendor/magicsuggest/magicsuggest-min.css');
        $this->assets->addJs('assets/vendor/magicsuggest/magicsuggest-min.js');

        $this->assets->addCss('assets/vendor/datepicker3.css');
        $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');

//         $this->assets->addJs('assets/suva/js/classifieds-edit.js');
		
		$this->assets->addJs('assets/suva/js/input_search.js');

        $this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');
        $this->assets->addJs('assets/js/classified-submit-choose-ad-type.js');  
		
    }

    /**
     * Edit Action
     */
    public function editAction($entity_id) {
        // Editing ads is allowed only to 'admin', 'support', 'moderator' roles
		//error_log("edit");
        if (!$this->auth->logged_in(array('admin', 'support', 'moderator'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('admin/ads');
        }

        $title = 'Edit ad';
        $this->tag->setTitle($title);
        $this->view->setVar('form_title_long', $title);
        $this->add_common_crud_assets();
		
		// $this->printr($_REQUEST);
		
        $entity_id = (int)$entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for edit action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Suva\Models\Ads */
        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Suva\Models\Ads */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }
		$_SESSION['_context']['selected_ad_id'] = $entity->id;
		

				
				//igor
				//pronać sve order items koji imaju na sebi ad čiji id je $entity_id
				/* select * from orders_items where ads_id = $entity_id
				*/
				
// 				$orders_items = OrdersItems::find(
// 					array(
// 							"conditions" => "ad_id = ?1",
// 							"bind"       => array(1 => $entity_id)
// 					)
// 				);
				
				
        $this->category_id_dropdown(
            'new_category_id',
            null,
            array(
                'disabled' => Categories::getCategoriesWithoutTransactionType()
            )
        );

        $ad_category = $entity->Category;
        if ($this->request->hasPost('category_id') && intval($this->request->getPost('category_id')) !== intval($ad_category->id)) {
            $ad_category = Categories::findFirst(intval($this->request->getPost('category_id')));
        }

        $parametrizator = new Parametrizator();
        $parametrizator->setModule('suva');
        $parametrizator->setCategory($ad_category);
        $parametrizator->setAd($entity);

        if ($this->request->isPost() && $this->request->hasPost('category_id')) {
            $next_url = $this->get_next_url();
			//error_log("is_post");
            if ($entity->isSoftDeleted()) {
                $this->flashSession->error('<strong>Error!</strong> SoftDeleted ad cannot be modified!');
                return $this->redirect_to($next_url);
            }

            // detect which button submitted the form.. in case the detected button is 'savemoderation' then we
            // handle only saving of 'Ads moderation part', otherwise, save all submitted data.
            if ($this->request->hasPost('savemoderation')) {
                $saved = $entity->save_moderation($this->request);
                $success_msg = '<strong>Ad moderated!</strong>';
                $save_action = 'savemoderation';
				//error_log("save moderation");
            } else {
                $saved = $entity->suva_save_changes($this->request);
				//error_log(print_r($saved));
                $success_msg = '<strong>Ad updated!</strong>';
                $save_action = $this->get_save_action();
				//error_log("backed save");
            }

            if ($saved instanceof Ads) {
				//error_log('sejvanje ada');
                $this->flashSession->success($success_msg . ' ' . $saved->get_frontend_view_link('html'));

                if ('save' === $save_action) {
                    $next_url = 'admin/ads/edit/' . $saved->id;
                }
							
									//IGOR -- OVDJE TREBA PROVJERIT JE LI VEĆ POSTOJI ONLINE ORDER LINE ZA SPREMLJENI AD
									$this->insertToCartDefaultOnlineProduct($saved->id);
														
                return $this->redirect_to($next_url);
            } else {
// 								ako nije instanceOf Ads onda je Phalcon\Validation\Message\Group
				//error_log("nije sejvano");
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.aa'.(get_class($saved)));
                $parametrizator->setRequest($this->request);
                $parametrizator->setErrors($saved);
                $this->processModerationReasons($this->request->getPost('moderation', 'string', null), $this->request->getPost('moderation_reason', 'string', null));
            }

        } else {
            $parametrizator->setData($entity->getData());
            $this->processModerationReasons($entity->moderation, $entity->moderation_reason);
        }
        $this->setup_products_chooser($entity, $ad_category);
        $this->setup_rendered_parameters($parametrizator->render_input());
        $this->view->setVar('category', $ad_category);
        $this->view->setVar('ad', $entity);
		
		//dohvar order itema sa odgovarajućim dopuštenim akcijam
		//lista order itema vezanih uz tekuću akciju sa dodanim poljem n_status
		//79 - lista related order items mora biti sortirana silazno prema vremenu unošenja
		$orders_items = Nava::arrToDict(
			Nava::sqlToArray(
				" select "
				."	oi.* "
				."	,o.n_status "
				."	,os.description as status_name "
				."	,(case when oi.n_expires_at < UNIX_TIMESTAMP() then 0 else 1 end) as is_active_now "
				." from orders_items oi "
				." 	join orders o on (oi.order_id = o.id and oi.ad_id = $entity_id  and oi.n_is_active = 1) "
				."		join n_order_statuses os on (o.n_status = os.id) "
				." order by oi.id desc"
			)
		);
		// foreach ($orders_items as $oi){
			// // dopuštene akcije na  related order itemu za pojedinog usera i status ordera
			// $oi['actions'] = Nava::arrToDict( Nava::getAllowedActions($this->auth->get_user()->id, $oi['n_status'], 'related-order-item') );
			// $orders_items[$oi['id']]['actions'] = Nava::arrToDict( Nava::getAllowedActions($this->auth->get_user()->id, $oi['n_status'], 'related-order-item') );
			// $orders_items[$oi['id']]['n_status'] = $oi['n_status'];
			// $orders_items[$oi['id']]['user_id'] = $this->auth->get_user()->id;
			
		// }
		// $this->view->setVar('orders_items',$orders_items);
// 		$this->view->setVar('orders_items', Nava::sqlToArray("select oi.*, o.n_status from orders_items oi join orders o on (oi.order_id = o.id and oi.ad_id = $entity_id )"));
				

        $can_be_moderated = true;
        $moderation_warning = null;

        $ad_status = $entity->get_status();
        if ($ad_status == 0) {
            $can_be_moderated = false;
            $moderation_warning = 'This ad has not yet finished with <b>ad submission</b> process and cannot be moderated!';
        }
        if ($ad_status == 20) {
            $can_be_moderated = false;
            $moderation_warning = 'This ad has not yet finished with <b>ad republish</b> process (<b>waiting for payment</b>) and cannot be moderated!';
        }
        if ($entity->soft_delete === 1) {
            $can_be_moderated = false;
            $moderation_warning = 'This ad is <b>soft deleted</b> and cannot be moderated or edited!';
        }
		
		
		
        $this->view->setVar('can_be_moderated', $can_be_moderated);
        $this->view->setVar('moderation_warning', $moderation_warning);
        $this->view->setVar('ad_reports', $entity->getReports());
        // get ad's latest (last 10) related orders
        $this->view->setVar('ads_related_orders', $entity->getAllRelatedOrders(10));

        $show_save_buttons = true;
        if ($entity->isSoftDeleted()) {
            $show_save_buttons = false;
        }
        $this->view->setVar('ads_remark', (isset($entity->AdditionalData->remark) ? $entity->AdditionalData->remark : ''));
        $this->view->setVar('show_save_buttons', $show_save_buttons);
		$this->view->setVar('ad_taker_username', $ad_taker_username);
		
		$this->view->setVar('orders', Nava::arrToDict(Orders::find()->toArray()));
		$this->view->setVar('orders_statuses', Nava::arrToDict(OrdersStatuses::find()->toArray()));
		$this->view->setVar('publications', Nava::arrToDict(Publications::find()->toArray()));
		$this->view->setVar('products', Nava::arrToDict(Products::find()->toArray()));
		$this->view->setVar('category', Categories::findFirst($entity->category_id));
		
    }

// 	AJAX NIJE OVDJE NEGO /fontend/controllers/AjaxController
// 	public function ajaxGetCategoryAction( $pUpit){
// 		//error_log("TU SAM ajaxGetCategoryAction ");
		
// 		 if (!$this->request->isAjax()) 
// 			 $this->printr($_REQUEST);
// 			return "{id:1234,name:'blabla'}";
		
		
// 	}
	
    public function saveRemarkAction() {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            $ad = Ads::findFirst(array(
                'conditions' => 'id = :id:',
                'bind'       => array(
                    'id' => $_POST['ad_id']
                )
            ));
            if ($ad) {
                if ($ad->save_remark($_POST['remark'])) {
                    $response_array['status'] = true;
                } else {
                    $response_array['msg'] = 'Error saving remark for this ad!';
                }
            } else {
                $response_array['msg'] = 'Ad could not be found!?!';
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    public function softDeleteAction($entity_id = null) {
        // Soft delete allowed only for 'admin' and 'support' roles
        if (!$this->auth->logged_in(array('admin', 'support'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('admin/ads');
        }

        $this->view->pick('chunks/delete');

        $title = 'SoftDelete ad';
        $this->tag->setTitle($title);
        $this->view->setVar('form_title_long', $title);
        $this->view->setVar('is_soft_delete', true);
        $this->add_common_crud_assets();

        // Check if multiple resources are to be handled
        if (false !== strpos($entity_id, ',')) {
            $entity_ids = explode(',', $entity_id);
        } else {
            $entity_id = (int) $entity_id;
            if (!$entity_id) {
                $this->flashSession->error('Missing required ID parameter for SoftDelete action');

                return $this->redirect_back();
            }
            $entity_ids = array($entity_id);
        }

        /* @var $model \Baseapp\Suva\Models\BaseModel */
        $model = $this->crud_model_class;

        $ids = implode(',', array_map(function($id){
            return (int) trim($id);
        }, $entity_ids));

        /* @var $entities \Baseapp\Suva\Models\BaseModel[]|\Phalcon\Mvc\Model\Resultset */
        $condition = 'id IN (' . $ids . ')';
        $entities = $model::find($condition);

        // Do actual deletion if needed
        if ($this->request->isPost()) {
            foreach ($entities as $entity) {
                $ident = $entity->ident();
                $result = $entity->soft_delete();
                if ($result) {
                    // When SoftDeleting an ad, delete the potential AdsHomepage record too
                    AdsHomepage::deleteByAdId($entity->id);
                    $this->flashSession->success('Resource <strong>' . $ident . '</strong> has been SoftDeleted.');
                } else {
                    $this->flashSession->error('Failed SoftDeleting <strong>' . $ident . '</strong>!');
                }
            }
            return $this->redirect_to($this->get_next_url());
        }

        // Setup view vars
        $this->tag->setTitle('SoftDelete Confirmation');
        Tag::setDefault('entity_id', $entity_id);
        $this->view->setVar('entity_id', $entity_id);
        $this->view->setVar('entities', $entities);
        $this->view->setVar('model', $model);
    }

    public function refreshOfflineDescriptionAction() {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            $ad = new Ads();
            // clear the offline description!
            $_POST['ad_description_offline'] = null;
            $parametrizator = new Parametrizator();
            $parametrizator->setValidation((new \Baseapp\Library\Validations\AdsSuva())->get());
            $parametrizator->setModule('suva');
            $parametrizator->setMode('edit');
            $parametrizator->setCategoryById($this->request->getPost('category_id'));
            $parametrizator->setAd($ad);
            $parametrizator->setRequest($this->request);
            $parametrizator->prepare_and_validate();
            $parametrizator->parametrize();

            if (!empty($ad->description_offline)) {
                $export2avus = new ExportToAvus($ad);
                $ads_avus_category = $export2avus->get_offline_category_id($ad);

                $response_array['status'] = true;
                $response_array['data']   = array(
                    'avus'    => '[AVUS ID: ' . ($ads_avus_category ? '<span class="text-success">' . $ads_avus_category . '</span>' : '<span class="text-danger">unknown</span>') . ']',
                    'content' => $ad->description_offline
                );
            } else {
                $response_array['msg'] = 'Not enough parameters were filled!';
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    /**
     * @param Ads $entity
     * @param Categories $category
     */
    protected function setup_products_chooser(Ads $entity, Categories $category) {
        // only certain roles have ability to attach/update ad's product
			// igor - html je definiran na \app\common\library\Products\SuvaProductsChooser.php:48
			
        if ($this->auth->logged_in(array('admin', 'support'))) {
            $chooser = $entity->getProductsChooser($this->request, 'suva', $category);
            $this->view->setVar('products_chooser_markup', $chooser->getMarkup());
        }
    }

    /**
     * spamToggle Action
     */
    public function spamToggleAction($entity_id) {
        $entity_id = (int)$entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for this action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Suva\Models\Ads */
        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Suva\Models\Ads */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($entity->spamToggle()) {
            $this->flashSession->success('Spam status for ad <strong>' . $entity->ident() . '</strong> changed successfully.');
        } else {
            $this->flashSession->error('Failed changing spam status for ad <strong>' . $entity->ident() . '</strong>.');
        }

        return $this->redirect_back();
    }

    /**
     * activeToggle Action
     */
    public function activeToggleAction($entity_id) {
        $entity_id = (int)$entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for this action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Suva\Models\Ads */
        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Suva\Models\Ads */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($entity->activeToggle()) {
            $this->flashSession->success('Active status for ad <strong>' . $entity->ident() . '</strong> changed successfully.');
        } else {
            $this->flashSession->error('Failed changing active status for ad <strong>' . $entity->ident() . '</strong>.');
        }

        return $this->redirect_back();
    }

    public function publicationsAction($publication_id, $ad_id = null, $pCategoryId = 0) {
		$debug = null;	
		//vraća popis proizvoda koji su bazirani na kategoriji i publikaciji
		//tj. popis proizvoda na temelju publikacije sa cijenama na temelju kategorije
		//error_log("publicationsAction 1 publication_id: $publication_id ,category_id: $pCategoryId");
		
		//194 - New Offline product -> Issues - nudi prošla izdanja novina
		// Automatsko mijenjanje statusa u deadline issues
		Nava::updateIssuesStatuses();
		
        $this->view->disable();

        $request =$this->request;

        if ($request->isPost()==true) {
			//if ($debug) //error_log("publicationsAction 2");
            if ($request->isAjax() == true) {
				//if ($debug) //error_log("publicationsAction 3");
                
                /* GET  PRODUCT ITEMS BASED ON THE PRODUCT ID*/
				if (is_int($ad_id)){
					//error_log("publicationsAction 4");
					//na temlju ada- doznat kategoroiju i izuračunat cijene proizvoda..
					
// 					$result_products = Nava::sqlToArray(
// 						"select p.* from n_products p"
// 						."  join n_products_categories_prices pcp on (p.id = pcp.products_id)"
// 						."  where pcp.categories_id = (select category_id from ads where id= ".$ad_id.")"
// 					);
					
					//INT56 - AdsController publicationsAction - riješit popis proizvoda
					$ad_rec = Nava::sqlToArray("select * from ads where id = ".$ad_id);
					$lCategoryId = ($pCategoryId > 0) ?  $pCategoryId : $ad_rec[0]['category_id'];
					
					
					$result_products = Nava::getProductsWithPricesForCategoryAndPublication( $lCategoryId, $publication_id);
					//error_log("PCP PROIZVODI:".print_r($result_products,true));
					
					
				}else{
					//error_log("publicationsAction 5");
					// ovo se nikad neće izvršit jer ad_id uvijek dolazi
					$result_products = N_Products::find(
							 array(
								   "conditions" => "publication_id = ?1 ",
									"bind"      => array(1 => $publication_id)
							)
						)->toArray();
				
				}
				if ($debug) //error_log("publicationsAction 6");
				
				//dodavanje display ad-a
// 				$result_products = Nava::arrToDict($result_products);
// 				if ($is_display_ad == 1) {
// 					//error_log("publicationsAction 7");
// 					foreach ($result_products as $rp){
// 						if ($rp['is_display_ad'] == 0) {
// 							//error_log("publicationsAction 8");
// 							unset($result_products[$rp['id']]);
// 						}
// 					}
// 				}

				$data['products'] = $result_products;

				//igor 3.3.2016 - ako je publikacija ONLINE, nema issues
				
				$publication_rec = Publications::findFirst($publication_id)->toArray();
				$data['publications_rec'] = $publication_rec;
				//error_log ("PUBS_REC:".print_r($publication_rec,true));
				//error_log ("PUBS_REC->is_online:".$publication_rec->is_online);
				if ($publication_rec['is_online'] == 0){
					//if ($debug) //error_log("publicationsAction 9");
					// if ($debug) error_log ("NIJE ONLINE $publication_id");
					$result_issues = N_Issues::find(
							array(
								   "conditions" => "publications_id = ?1 AND status='prima'",
									"bind"      => array(1 => $publication_id)
							)
						)->toArray();

					$txtSql = "select p1.*, concat(day,'.',month,'.',year,' ',day_of_week)  as name from ("
					." select iss.* ,"
					." year(iss.date_published) as year, "
					." month(iss.date_published) as month, "
					." day(iss.date_published) as day, "
					." iss.date_published as datum, "
					." (case "
					."          when weekday(iss.date_published) = 0 then 'Pon' "
					."          when weekday(iss.date_published) = 1 then 'Uto' "
					."          when weekday(iss.date_published) = 2 then 'Sri' "
					."          when weekday(iss.date_published) = 3 then 'Čet' "
					."          when weekday(iss.date_published) = 4 then 'Pet' "
					."          when weekday(iss.date_published) = 5 then 'Sub' "
					."          when weekday(iss.date_published) = 6 then 'Ned' "
					."          else '' end ) as day_of_week "
					.", 0 as is_online"
					." from n_issues iss "
					." where "
					." iss.status = 'prima' "
// 					." and iss.date_published >= CURDATE()-1 "
					." and iss.publications_id = " . $publication_id
					." ) p1"
					." order by p1.datum ";
					
					$data['issues'] = Nava::sqlToArray($txtSql);
					
				} else {
					//error_log("publicationsAction 10");
					//error_log ("JE ONLINE $publication_id");
// 					//ako je publikacija online, ne klikaju se issues nego se odabire proizvod koji na sebi ima trajanje
					$data['issues'] = Array();
				}
				
			//if ($debug) //error_log("publicationsAction 11");
            echo json_encode($data);
            }
        }
    }
	
	//IGOR  
	public function insertToCartDefaultOnlineProduct($ad_id) {
			
			
// 					$this->flashSession->error(sprintf('TU SAM u insertToCartDefaultOnlineProduct'));
// 					return $this->redirect_back();
			
					$orders_items = Nava::sqlToArray(
						"select * from orders_items oi "
						." join n_publications pub on (pub.id = oi.n_publications_id) "
						." where oi.ad_id = ".$ad_id
						." and pub.is_online = 1"
					);	

					//ako već postoji neki online proizvod
					if (sizeof($orders_items) > 0){
// 							$this->flashSession->error(sprintf('IMA ORDER ITEMA'));
// 							return $this->redirect_back();
								return;
						
					}
						
// 					$this->flashSession->error(sprintf('NEMA ORDER ITEMA'));
// 					return $this->redirect_back();
						
					$default_product = Products::findFirst( array("conditions" => "is_default_product = 1" ) );
					if (!$default_product) {
						/*
						GREŠKA - treba definirat defaultni proizvod
						*/
							$this->flashSession->error(sprintf('NEMA DEFAULTNOG PRODUCTA'));
							return $this->redirect_back();
						
					}
					
// 					$this->flashSession->error(sprintf('IMA DEFAULTNOG PRODUCTA'));
// 					return $this->redirect_back();

			
					$ad = Ads::findFirst($ad_id);
			
					if (!$ad){
							$this->flashSession->error(sprintf('NEMA ADA'));
							return $this->redirect_back();
						
						
					}
					
// 							$this->flashSession->error(sprintf('IMA ADA '.$ad->id));
// 							return $this->redirect_back();
						
			
					$product_id  = $default_product->id;
					$discount_id = Null;
					$publications_id = $default_product->publication_id;
// 				$offline_issues = $_POST['offline-issues'];

					$user_id = $ad->user_id;

					 $this->disable_view();
					 $this->response->setContentType('application/json', 'UTF-8');
					 $response_array = array('status' => true);

// 					if (!$this->request->isAjax()) {
						
// 							$this->flashSession->error(sprintf('NIJE AJAX'));
// 							return $this->redirect_back();

// 							$this->response->setStatusCode(400, 'Bad Request');
// 							$response_array['msg'] = 'Bad Request';
// 					} else {
			
			
// 							$this->flashSession->error(sprintf('JE AJAX'));
// 							return $this->redirect_back();
						

							$order = Orders::findFirst(
													array(
															"conditions" => "n_status = 1 AND user_id = ?1",
															"bind" => array(1 => $_POST['user_id'])
															)
													);

							if (!$order){
								$order = new Orders();

								$order->user_id = $user_id;
								$order->status = 1;
								$order->created_at = date('Y-m-d H:i:s');
								$order->created_at = date('Y-m-d H:i:s');
								$order->ip = $this->request->getClientAddress();
								$order->n_status = 1;

								if ($order->save() == false) {
									$response_array['status'] = false;
									$err_msg = "";
									foreach ($order->getMessages() as $message) {
													 $err_msg .= $message . "\n\r";
									}	
								 $response_array['msg'] = $err_msg;
								}	
							}

							$product = N_Products::findFirst((int)$product_id);


							$product_add = new OrdersItemsEx();     

							$product_add->n_products_id = $product_id;
							$product_add->n_publications_id = $publications_id;
							$product_add->order_id = $order->id;
							$product_add->ad_id = $ad_id;
							$product_add->price = 0;
							$product_add->qty = 1;
							$product_add->title = $product->name;
							$product_add->total =  $product_add->price * $product_add->qty;
							$product_add->tax_rate = 0;
							$product_add->tax_amount = 0;
							$product_add->product = serialize($product_add);
							$product_add->product_id = $product->name." OBAVEZNA OBJAVA ONLINE OGLASA";
						
							//IGOR - dodano da se koristi za online izdanja, kopira se sa Ad-a
							$product_add->n_first_published_at = $ad->first_published_at;
							$product_add->n_expires_at = $ad->expires_at;
								
							if ($product_add->save() == false) {
									$response_array['status'] = false;
									$err_msg = "";
									foreach ($product_add->getMessages() as $message) {
													 $err_msg .= $message . "\n\r";
									}
									$this->flashSession->error(sprintf($err_msg));
									return $this->redirect_back();
// 									 $response_array['msg'] = $err_msg;
							}else{
									$order->total = $order->total + ($product_add->total / 100 );
									if ($order->save() == false) {
											$response_array['status'] = false;
											$err_msg = "";
											foreach ($order->getMessages() as $message) {
															 $err_msg .= "INSERT INSERTION - " . $message . "\n\r";
											}
// 											$response_array['msg'] = $err_msg;
											$this->flashSession->error(sprintf($err_msg));
											return $this->redirect_back();
									}
									if ($response_array['status'] == true){
											$response_array['msg'] = 'Added item!';
									}
							}
// 				 }
// 					$this->response->setJsonContent($response_array);
// 					return $this->response;
						return;
			}
}
