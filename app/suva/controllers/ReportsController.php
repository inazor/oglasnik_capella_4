<?php

namespace Baseapp\Suva\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Suva\Models\Roles;
use Baseapp\Suva\Models\Locations;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Paginator\Adapter\QueryBuilder;
use Baseapp\Suva\Library\Nava;
use Baseapp\Suva\Library\libAppSetting;


use Baseapp\Suva\Models\Publications;
use Baseapp\Suva\Models\Products;

use mPDF;
use Phalcon\Mvc\View;



class ReportsController extends IndexController{
    use CrudActions;
    use CrudHelpers;

  
    public $ime_foldera = "reports";
    public $ime_objekta = "reports";
    

    public function indexAction() {
		//css da inputi izgledaju bolje
		$this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
		// $this->sysMessage("Reports index");
		// $this->poruka ("Reports controller");
        $this->tag->setTitle("Reports");

    }
	
	public function cashOutAction( $pSalesRepId = null, $pFiscalLocationId = null, $pDatum = null ) {
		//error_log("CashOutController:cashOutAction 1");
		//error_log(print_r($_REQUEST,true));
		
		$this->view->setVar("_context", $_SESSION['_context']);
		$this->view->setVar("_model", new \Baseapp\Suva\Models\Model());
		
		//naslov
		$this->tag->setTitle('CashOut');
		$this->view->setLayout('common');
		$mpdf = new mPDF();
			$this->view->start();
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
			
		//$this->view->render('cash-out', 'cashOut');
		$this->view->render('reports','cashOut');
			
			$this->view->finish();
		$content = $this->view->getContent();  //ovo bi trebao bit izgled stranice
			//error_log($content);
			$mpdf->WriteHTML($content );
		$mpdf->Output("Cash-out report.php", "I");
		$this->view->disable();
		
	}

	public function ordersListAction() {
		//error_log("CashOutController:cashOutAction 1");
		//error_log(print_r($_REQUEST,true));
		
		$this->view->setVar("_context", $_SESSION['_context']);
		$this->view->setVar("_model", new \Baseapp\Suva\Models\Model());
		
		//naslov
		$this->tag->setTitle('Orders List');
		$this->view->setLayout('common');
		$mpdf = new mPDF();
		$this->view->start();
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
			
		//$this->view->render('cash-out', 'cashOut');
		$this->view->render('reports','ordersList');
			
		$this->view->finish();
		$content = $this->view->getContent();  //ovo bi trebao bit izgled stranice
			//error_log($content);
		$mpdf->WriteHTML($content );
		$mpdf->Output("Orders List.php", "I");
		$this->view->disable(); //ne ide na afterexecuteroute??
	}
	
	public function displayAdListAction() {
		
		if(!array_key_exists('xml_report_issue_id', $_SESSION['_context'] )){
			$this->greska('Nije poslan parametar Issue ID');
			return $this->redirect_back();
		}
		$issueId = $_SESSION['_context']['xml_report_issue_id'];
		if($issueId == ''){
			$this->greska("Nije odabran Issue");
			return $this->redirect_back();
		}
		$issue = \Baseapp\Suva\Models\Issues::findFirst($issueId);
		if(!$issue){
			$this->greska("Nije pronaden issue sa brojem  $issueId");
			return $this->redirect_back();
		}
				
		 $txt = "";
		 
	     $txt .="Publication;Issue;Category;Ad Taker;Layout;Product;Ad ID;File Name;Width;Height\n";
		 
		 //oglasi iz SUVE
		 $insertions = $issue->Insertions("is_active = 1");
		 foreach ( $insertions as $insertion ){
			$order_item = $insertion->OrderItem();			
			if ( $order_item ) {
				$order = $order_item->Order();
				if($order){
					if($order->n_status == 2 or $order->n_status == 5){
													
						$ad_taker = $order_item->AdTaker();
						$ad = $order_item->Ad();						
						$catMap = $ad->CategoryMapping();
						$category = $ad->Category();
						$product = $order_item->Product();	
						if($product){								
							$publication = $product->Publication();													
							$layout = $product->Layout();							
							if  ( $ad->n_is_display_ad ) {
									// error_log("KATEGORIJA".$category->n_path);
									// error_log("ADID diSPLAY AD ".$ad->id);
									$txt .= $publication->name.";";
									$txt .= $issue->id.";";
									$txt .= $catMap->n_path.";";
									$txt .= $ad_taker->username.";";
									$txt .= $layout->name.";";
									$txt .= $product->name.";";
									$txt .= "$ad->id".";";
									$txt .= $ad->user_id."_".$ad->id.".eps".";";
									$txt .= $product->display_ad_width.";";
									$txt .= $product->display_ad_height;
									$txt .= "\n";
							}	
						}else{
							Nava::greska("Na order itemu ".$order_item->id." ne postoji n_products_id");
						}
					}
				}
			}
		}
		
		//po insertionima AVUS
		$insertionsAvus = $issue->InsertionsAvus();
		foreach ( $insertionsAvus as $insertionAvus ) {
			$ad = $insertionAvus->Ad();
			if ($ad){
				$adTakerUserName = 'n/a';
				$publication = \Baseapp\Suva\Models\Publications::findFirst(2); //oglasnik tiskano
				$product = null;
				if ( $ad->n_avus_adlayout_id > 0 ){
					$product = \Baseapp\Suva\Models\Products::findFirst("avus_adlayout_id = $ad->n_avus_adlayout_id"); 
				}
				if ($product && $product->is_display_ad == 1) {
					
					$category = $ad->CategoryMapping();
					$layout = $product && $product->Layout() ? $product->Layout(): null;

					$catMapId = $ad->n_avus_class_id > 0 ? $ad->n_avus_class_id : -1;
					$catMap = \Baseapp\Suva\Models\CategoriesMappings::findFirst(" avus_class_id = $catMapId ");
					$filename = \Baseapp\Suva\Library\libDTP::getPictureFileName ( null, $ad->id );
					
					$txt .= $publication->name.";";
					$txt .= $issue->id.";";
					$txt .= $catMap->n_path.";";
					$txt .= "n/a;";
					$txt .= ( $layout ? $layout->name : 'n/a' ).";";
					$txt .= $product->name.";";
					$txt .= "AVUS:$ad->n_avus_order_id SUVA:$ad->id".";";
					$txt .= $filename.".eps".";";
					$txt .= $product->display_ad_width.";";
					$txt .= $product->display_ad_height;
					$txt .= "\n";
				}
			}
			else {  
				//MAKNUTO DOK SE NE NAĐU GREŠKE NA INSERTIONIMA BEZ ORDER ITEMA
				//Nava::poruka("Order item for insertion $insertion->id does not exist");
			}
					
		}		

		
		$this->view->setVar ('text', $txt);
		
		// /*kreirat textualni file sadržajem $txt */
		$issueFolderTitle = $issue->Publication()->slug."_REPORTS_CSV";
		$issueFolder = "repository/dtp/issues/$issueFolderTitle/";
		
		if (!file_exists($issueFolder)) {
			mkdir($issueFolder, 0755, true);
		}
		
		
				
		$txtFileName = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published))."_display_ad".".csv";
		$txtFilePath = $issueFolder.$txtFileName;
		$this->sysMessage("Adding TXT to file $txtFilePath "); 
		file_put_contents($txtFilePath, $txt);
		
				
		$_SESSION['_context']['file_url'] = $txtFilePath;
		
		$this->view->setVar("_context", $_SESSION['_context']);
		
		//naslov
		$this->tag->setTitle('DisplayAd List');
		$this->view->pick ("reports/rptTextFileDownload");
		
	}

	public function slikeListAction() {
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 600000);
		ini_set ('request_terminate_timeout', 600000);

		if(!array_key_exists('xml_report_issue_id', $_SESSION['_context'] )){
			$this->greska('Nije poslan parametar Issue ID');
			return $this->redirect_back();
		}
		$issueId = $_SESSION['_context']['xml_report_issue_id'];
		if($issueId == ''){
			$this->greska("Nije odabran Issue");
			return $this->redirect_back();
		}
		$issue = \Baseapp\Suva\Models\Issues::findFirst($issueId);
		if(!$issue){
			$this->greska("Nije pronaden issue sa brojem  $issueId");
			return $this->redirect_back();
		}
		
		//29.12.2016 - nema više dtp grupa
		// //dtp Group
		// $dtpGroupId = null;
		// $dtpGroup = null;
		// if ( array_key_exists('xml_report_group_id', $_SESSION['_context']) && $_SESSION['_context']['xml_report_group_id'] > 0 ){
			// $dtpGroupId = $_SESSION['_context']['xml_report_group_id'];
			// $dtpGroup = \Baseapp\Suva\Models\DtpGroups::findFirst( $dtpGroupId );
		// }
			

		// $publication = $issue->Publication();
		
		// //kreiranje foldera za issue u dtp: OGLT_1016-1-1_GR1
		// $issueFolderTitle = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published));
		// if ( $dtpGroup ) $issueFolderTitle .= "_".$dtpGroup->name;
		// $issueFolder = "repository/dtp/issues/$issueFolderTitle/";
		
		$processedFolder = \Baseapp\Suva\Library\libDTP::getProcessedFolderName ($issue);

		$error = null;
		if (!is_dir($processedFolder)){
			Nava::greska ("Folder $processedFolder does not exist");
			return $this->redirect_back();
		}

			
		
		$txt = "";
		$txt .="Publications;Issue;Category;Layouts Name;ID;Ad Taker;File Name;File Format;\n";
		
		//po insertionima SUVA
		//error_log('reportsController:slikeList:271 - po Insertionima SUVA');
		$insertions = $issue->Insertions();
		foreach ( $insertions as $insertion ) {

			$order_item = $insertion->OrderItem();
			if ($order_item){
				//error_log("reportsController:slikeList, insertion $insertion->id, oi $order_item->id");
				
				$ad_taker_name = $order_item->AdTaker() ? $order_item->AdTaker()->username : 'n/a';
				
				if( $order_item ) $ad = $order_item->Ad();
				if( $order_item ) $product = $order_item->Product();
				if( $product){
					
					$publication = $product->Publication();
					$category = $ad->Category();
					$layout = $product->Layout();
					$oi_id = $order_item->order_id;
					$catMapId = $ad->getCategoryMappingForPublication(2);
					$catMap= \Baseapp\Suva\Models\CategoriesMappings::findFirst($catMapId);
					$order = \Baseapp\Suva\Models\Orders::findFirst($oi_id);
					
						
					$sr_id = $order->n_sales_rep_id;
					$sales_rep = \Baseapp\Suva\Models\Users::findFirst($sr_id);
					$filename = \Baseapp\Suva\Library\libDTP::getPictureFileName ( $order_item->id );
					
					
					$txtSearch = $processedFolder.$filename.".*";
					//$this->poruka($txtSearch);
					$list = glob( $txtSearch );
					$count = count( $list );
					if ( $count == 1 ){
						//OK nađen je jedan file
						$foundFilePath = $list[0];
						$foundFileName = substr($foundFilePath, strrpos($foundFilePath, '/') + 1);
						$fileExtension = substr($foundFilePath, strrpos($foundFilePath, '.', - 1) + 1);
						$msgFileFormat = $fileExtension;
					}
					elseif ( $count > 1 ){
						//GREŠKA
						$msgFileFormat = "Too many files. $txtSearch";
					}
					elseif ( $count == 0 ){
						//GREŠKA
						$msgFileFormat = "Missing. $txtSearch";
					}

					if ( $product->is_picture_required ) {
						$txt .= $publication->name.";";
						$txt .= $issue->id.";";
						$txt .= $catMap->n_path.";";
						$txt .= $layout->name.";";
						$txt .= $ad->id.";";
						$txt .= $ad_taker_name.";";
						$txt .= $filename.";";
						$txt .= $msgFileFormat.";"; // $txt .= ($ad && $ad->AdsMedia() && $ad->AdsMedia()->Media() ? $ad->AdsMedia()->Media()->mimetype : 'n/a');
						// $txt .= $product->display_ad_width.";";
						// $txt .= $product->display_ad_height.";";
						// $txt .= "COLOR MODE";
						$txt .= "\n";
					}
				}else{
					Nava::greska("Na order itemu ".$order_item->id." ne postoji n_products_id");
				}
			}
			else {  
				Nava::greska("Na insertionu ".$insertion->id." ne postoji order_item_id");
				//MAKNUTO DOK SE NE NAĐU GREŠKE NA INSERTIONIMA BEZ ORDER ITEMA
				//Nava::poruka("Order item for insertion $insertion->id does not exist");
			}
					
		}
		
		//po insertionima AVUS
		//error_log('reportsController:slikeList:340 - po Insertionima AVUS');
		$insertionsAvus = $issue->InsertionsAvus();
		foreach ( $insertionsAvus as $insertionAvus ) {

			$ad = $insertionAvus->Ad();
			if ($ad){
				//error_log("reportsController:slikeList, insertionAvus $insertionAvus->id, ad $ad->id");
				
				$adTakerUserName = 'n/a';
				$publication = \Baseapp\Suva\Models\Publications::findFirst(2); //oglasnik tiskano
				$product = null;
				if ( $ad->n_avus_adlayout_id > 0 ){
					$product = \Baseapp\Suva\Models\Products::findFirst("avus_adlayout_id = $ad->n_avus_adlayout_id");
					$productName = $product->name;
				}

				$category = $ad->CategoryMapping();
				$layout = $product && $product->Layout() ? $product->Layout(): null;

				$catMapId = $ad->n_avus_class_id > 0 ? $ad->n_avus_class_id : -1;
				$catMap = \Baseapp\Suva\Models\CategoriesMappings::findFirst(" avus_class_id = $catMapId ");
				$filename = \Baseapp\Suva\Library\libDTP::getPictureFileName ( null, $ad->id );
				
				
				$txtSearch = $processedFolder.$filename.".*";
				//$this->poruka($txtSearch);
				$list = glob( $txtSearch );
				$count = count( $list );
				if ( $count == 1 ){
					//OK nađen je jedan file
					$foundFilePath = $list[0];
					$foundFileName = substr($foundFilePath, strrpos($foundFilePath, '/') + 1);
					$fileExtension = substr($foundFilePath, strrpos($foundFilePath, '.', - 1) + 1);
					$msgFileFormat = $fileExtension;
				}
				elseif ( $count > 1 ){
					//GREŠKA
					$msgFileFormat = "Too many files. $txtSearch";
				}
				elseif ( $count == 0 ){
					//GREŠKA
					$msgFileFormat = "Missing. $txtSearch";
				}

				if ( $product && $product->is_picture_required ) {
					$txt .= $publication->name.";";
					$txt .= $issue->id.";";
					$txt .= $catMap->n_path.";";
					$txt .= $layout->name.";";
					$txt .= "AVUS:$ad->n_avus_order_id SUVA:$ad->id".";";
					$txt .= $adTakerUserName.";";
					$txt .= $filename.";";
					$txt .= $msgFileFormat.";"; // $txt .= ($ad && $ad->AdsMedia() && $ad->AdsMedia()->Media() ? $ad->AdsMedia()->Media()->mimetype : 'n/a');
					// $txt .= $product->display_ad_width.";";
					// $txt .= $product->display_ad_height.";";
					// $txt .= "COLOR MODE";
					$txt .= "\n";
				}
			}
			else {  
				//MAKNUTO DOK SE NE NAĐU GREŠKE NA INSERTIONIMA BEZ ORDER ITEMA
				//Nava::poruka("Order item for insertion $insertion->id does not exist");
			}
					
		}		
		$this->view->setVar ('text', $txt);
		
		$issueFolder = \Baseapp\Suva\Library\libDTP::getIssueFolderName($issue);	
		$txtFileName = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published))."_is_picture_required.csv";
		$txtFilePath = $issueFolder.$txtFileName;
		$this->sysMessage("Adding TXT to file $txtFilePath "); 
		file_put_contents($txtFilePath, $txt);
				
		$_SESSION['_context']['file_url'] = $txtFilePath;
		
		$this->view->setVar("_context", $_SESSION['_context']);
		
		//naslov
		$this->tag->setTitle('Slike List');
		$this->view->pick ("reports/rptTextFileDownload");
		
		
		ini_set('max_execution_time', $appExecutionTime);
		ini_set('request_terminate_timeout', $appExecutionTime);
	}

	public function avusAdsListAction() {
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
 
		 $txt = "";
		 
	     $txt .="SUVA ID;AVUS ID;ISSUE;AVUS CLASS ID;SUVA CATEGORY ID;SUVA CATEGORY NAME;AVUS ADLAYOUT ID;SUVA PRODUCT ID;SUVA PRODUCT NAME;SUVA USER ID; AVUS USER ID;TEXT\n";
		 
		 $rsInsertions = Nava::sqlToArray("select * from n_insertions_avus order by issue_id, ad_id limit 100000");
		$i=1;
		 foreach ( $rsInsertions as $row ){
			$ad = \Baseapp\Suva\Models\Ads::findFirst( $row['ad_id'] );
			$issue = \Baseapp\Suva\Models\Issues::findFirst( $row['issue_id'] );
			$catMap = \Baseapp\Suva\Models\CategoriesMappings::findFirst( "avus_class_id = ".( $ad->n_avus_class_id > 0 ? $ad->n_avus_class_id : -1 ) );
			$customer = \Baseapp\Suva\Models\Users::findFirst( "n_avus_customer_id = ".( $ad->n_avus_customer_number > 0 ? $ad->n_avus_customer_number  : -1 ) );
			$product = \Baseapp\Suva\Models\Products::findFirst( "avus_adlayout_id = ".( $ad->n_avus_adlayout_id > 0 ? $ad->n_avus_adlayout_id  : -1 ));
			
			$txt .= $ad->id.";";
			$txt .= $ad->n_avus_order_id.";";
			$txt .= ( $issue ? $issue->id : 'n/a').";";
			$txt .= $ad->n_avus_class_id.";";
			$txt .= ( $catMap ? $catMap->id : 'n/a').";";
			$txt .= ( $catMap ? $catMap->n_path : 'n/a').";";
			$txt .= $ad->n_avus_adlayout_id.";";
			$txt .= ( $product ? $product->id : 'n/a').";";
			$txt .= ( $product ? $product->name : 'n/a').";";
			$txt .= ( $customer ? $customer->id : 'n/a').";";
			$txt .= ( $customer ? $customer->n_avus_customer_number : 'n/a').";";
			//$txt .= $ad->description_offline.";";
			$txt .= ";\n";
			//error_log($i++);
		}
		
		$this->view->setVar ('text', $txt);
		
		// /*kreirat textualni file sadržajem $txt */
		$issueFolderTitle = $issue->Publication()->slug."_REPORTS_CSV";
		$issueFolder = "repository/dtp/";
		
		if (!file_exists($issueFolder)) {
			mkdir($issueFolder, 0755, true);
		}
		
		
				
		$txtFileName = "AVUS_imported_ads_list.csv";
		$txtFilePath = $issueFolder.$txtFileName;
		$this->sysMessage("Adding TXT to file $txtFilePath "); 
		file_put_contents($txtFilePath, $txt);
		
				
		$_SESSION['_context']['file_url'] = $txtFilePath;
		
		$this->view->setVar("_context", $_SESSION['_context']);
		
		//naslov
		$this->tag->setTitle('AVUS imported ads List');
		$this->view->pick ("reports/rptTextFileDownload");
		
	}
	
	public function offlineCopyPicturesToFolderAction( $pFolderName = null, $pTest = 0, $pDeleteExisting = 0 ) {
		// $this->sysMessage("offlineCopyPicturesToWorkingFolderAction");
		// error_log('Reports::offlineCopyPicturesToFolderAction 1');
		//verzija s mapiranjem kategorija  > kategorija publikacije --> globalna kategorija + dictionary id
		// $this->poruka("offlineCopyPicturesToFolderAction");
		
		if (!$pFolderName){
			$this->sysMessage("offlineCopyPicturesToFolderAction: folder name not supplied.");
			// error_log('Reports::offlineCopyPicturesToFolderAction 2');
		}
		//issue
		if(!array_key_exists('xml_report_issue_id', $_SESSION['_context'] )){
			$this->greska('Nije poslan parametar Issue ID');
			// error_log('Reports::offlineCopyPicturesToFolderAction 3');
			return $this->redirect_back();
		}
		$issueId = $_SESSION['_context']['xml_report_issue_id'];
		if($issueId == ''){
			// error_log('Reports::offlineCopyPicturesToFolderAction 4');
			$this->greska("Nije odabran Issue");
			return $this->redirect_back();
		}
		$issue = \Baseapp\Suva\Models\Issues::findFirst($issueId);
		if(!$issue){
			// error_log('Reports::offlineCopyPicturesToFolderAction 5');
			$this->greska("Nije pronaden issue sa brojem  $issueId");
			return $this->redirect_back();
		}
		
		//DTP Group
		$dtpGroupId = null;
		$dtpGroup = null;
		if ( array_key_exists('xml_report_group_id', $_SESSION['_context']) && $_SESSION['_context']['xml_report_group_id'] > 0 ){
			$dtpGroupId = $_SESSION['_context']['xml_report_group_id'];
			$dtpGroup = \Baseapp\Suva\Models\DtpGroups::findFirst( $dtpGroupId );
		}

		if ($pFolderName == 'to_process' ){
			// \Baseapp\Suva\Library\libDTP::copyPicturesToWorkingFolder ( $issue->id, $dtpGroupId );
			\Baseapp\Suva\Library\libDTP::copyPicturesToWorkingFolderNEW ( $issue->id, $dtpGroupId );
		}
		elseif ($pFolderName == 'processed' ){
			\Baseapp\Suva\Library\libDTP::copyPicturesToProcessedFolder ( $issue->id, $dtpGroupId );
		}
			
		
	
		$this->view->pick("reports/index");
	}
	
	
	public function offLineIssueXmlAction() {
		
				 
		$_context = $_SESSION['_context'];
		//Nava::printr($_context);
		//error_log (print_r( $_context, true ));
		
		$issueId = array_key_exists('xml_report_issue_id', $_context) && $_context['xml_report_issue_id'] > 0 ? $_context['xml_report_issue_id'] : null;
		
		
		$dtpGroupId = array_key_exists('xml_report_group_id', $_context) && $_context['xml_report_group_id'] > 0 ? $_context['xml_report_group_id'] : null;
		
		$isThreaded = array_key_exists('xml_report_is_threaded', $_context) && $_context['xml_report_is_threaded'] > 0 ? true : false;
		
		
		if ( $isThreaded ){
			//Ova akcija se izvršava u threaded - pozove se php funkcija koja poziva akciju, akcija je složena da uzima varijable preko rute, a ne iz konteksta
			
			error_log('is_threaded');
			$sessionId = session_id();
			$_SESSION['_context']['_is_threaded_execution'] = true;
			
			$strIssueId = $issueId > 0 ? $issueId : '1';
			$strGroupId = $dtpGroupId > 0 ? $dtpGroupId : '';
			
			error_log ("prije execa");
			// exec("/usr/bin/php -q /home/oglasnik2/oglasnik.hr/current/vendor/pokreniObradu.php offLineIssueXmlThreaded/$sessionId/$strIssueId/$strGroupId >> /home/oglasnik2/oglasnik.hr/current/logs/php-errors.log ");
			
			$strExec = "/usr/bin/php -q /home/oglasnik2/oglasnik.hr/current/vendor/pokreniObradu.php offLineIssueXmlThreaded/$sessionId/$strIssueId/$strGroupId > /dev/null & ";
			
			error_log ("strExec: $strExec");
			exec( $strExec );
		
			error_log ("nakon execa");
			$this->view->pick('reports/index');
			return;
		}
		else {
			if ( array_key_exists ( '_is_threaded_execution', $_SESSION['_context'] ) ){
				unset ( $_SESSION['_context']['_is_threaded_execution'] );
			}
		}
  
		
		//$result = \Baseapp\Suva\Library\libDTP::createXmlForDtp ( $issueId, $dtpGroupId );
		
		
		$result = \Baseapp\Suva\Library\libDTP::createXmlForDtpNEWNEW( $issueId, $dtpGroupId, $this->auth->get_user() );
		//$result = -1;
		
		//$result = \Baseapp\Suva\Library\libDTP::createXmlForDtpNEW ( $issueId, $dtpGroupId );
		//$result = -1;
		if ( $result === -1 ){
			return $this->redirect_back();
		}
		else{
			$this->view->setVar("_context", $_SESSION['_context']);
			$this->view->setVar("_model", new \Baseapp\Suva\Models\Model());
			$this->view->setVar("xml", $result);
			$this->view->pick("reports/offLineIssueXml");
		}
	}
    
	public function orderPdfAction($order_id = null, $pStatus = null) {
		//error_log ("orderPdfAction:".$order_id."  #######  1");
		//error_log ("orderPdfAction:".$order_id."  #######  1.0.1 context:".print_r($_SESSION['_context'],true));
		
		$this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
		$this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');

		if (!array_key_exists('_context', $_SESSION)) $_SESSION['_context'] = array();
		

		
		if ($order_id && $order_id > 0){
			$oids = array($order_id);
		} 
		elseif (array_key_exists('orderPdf_report_order_ids', $_SESSION['_context'] )) {
			$order_id = $_SESSION['_context']['orderPdf_report_order_ids'][0];
		
			$oids = $_SESSION['_context']['orderPdf_report_order_ids'];
		}else{
			$oids = array();
		}
		
		
		\Baseapp\Suva\Library\libNavision::testFiscalization( $order_id );
		
		$name = "";
		foreach($oids as $oid)
			$name = $name."90000$oid ";
		
		$this->tag->setTitle('Fakture');
		$this->view->setLayout('common');
		$this->view->setVar("_context", $_SESSION['_context']);
		$this->view->setVar("_model", new \Baseapp\Suva\Models\Model());
		$this->view->setVar('ids', $oids);
		
		$mpdf = new \mPDF();
		$this->view->start();
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$this->view->render('reports', 'orderPdf');
		$this->view->finish();
		
		$content = $this->view->getContent();
		$mpdf->WriteHTML($content );
		$mpdf->Output("$name.pdf", "I");
		$this->view->disable();
		
	}
	
	public function dtpErrorsAction($order_id = null, $pStatus = null) {
		//error_log ("ReportsController:dtpErrorsAction: 1");
		
		if(!array_key_exists('_context', $_SESSION))
		return $this->greska('Nije definirana varijabla context');
		//error_log ("ReportsController:dtpErrorsAction: 2");
		
		if(!array_key_exists('xml_report_issue_id', $_SESSION['_context'] ))
		return $this->greska('Nije poslan parametar Issue ID');
		//error_log ("ReportsController:dtpErrorsAction: 3");
		
		$issueId = $_SESSION['_context']['xml_report_issue_id'];
		if($issueId == '')
		return $this->greska("Nije odabran Issue");
		//error_log ("ReportsController:dtpErrorsAction: 4");
		
		$issue = \Baseapp\Suva\Models\Issues::findFirst($issueId);
		if(!$issue)
		return $this->greska("Nije pronaden issue sa brojem  $issueId");
		//error_log ("ReportsController:dtpErrorsAction: 5");
		
		$adIds = array();
		$categories = \Baseapp\Suva\Models\Categories::find(array('order' => 'n_path'));
		foreach ($categories as $cat){
			//error_log ("ReportsController:dtpErrorsAction: 6");
			$catAdIds = Nava::getAdIdsByIssueAndCategory( $issue->id, $cat->id );	
			if($catAdIds)
				foreach($catAdIds as $catAdId){
					//error_log ("ReportsController:dtpErrorsAction: 7");
					$adIds[$catAdId['id']] = $catAdId['id'];
				}
		}
		
		
		//error_log ("ReportsController:dtpErrorsAction: 8".print_r($adIds,true));
		
		$this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
		$this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');

		
		$this->tag->setTitle('DTP Errors');
		$this->view->setLayout('common');
		$this->view->setVar("_context", $_SESSION['_context']);
		$this->view->setVar("_model", new \Baseapp\Suva\Models\Model());
		$this->view->setVar('ids', $adIds);
		
		//$mpdf = new \mPDF();
		
		//LANDSCAPE, http://stackoverflow.com/questions/20842347/how-to-set-the-page-in-landscape-mode-in-mpdf
		$mpdf = new \mPDF('c','A4-L'); 
		
		$this->view->start();
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$this->view->render('reports', 'dtpErrors');
		$this->view->finish();
		
		$content = $this->view->getContent();
		$mpdf->WriteHTML($content );
		$mpdf->Output("DTP ERRORS FOR ".$issue->Publication()->slug." , issue $issue->id, published on ".date("Y-m-d", strtotime($issue->date_published)), "I");
		$this->view->disable();
		//error_log ("ReportsController:dtpErrorsAction: 9");
		
		
	}
	
	public function dtpMissingPicturesAction($order_id = null, $pStatus = null) {
		//error_log ("ReportsController:dtpErrorsAction: 1");
		
		
		if(!array_key_exists('xml_report_issue_id', $_SESSION['_context'] ))
		return $this->greska('Nije poslan parametar Issue ID');
		//error_log ("ReportsController:dtpErrorsAction: 3");
		
		$issueId = $_SESSION['_context']['xml_report_issue_id'];
		if(!$issueId )	return $this->greska("Nije odabran Issue");
		$issue = \Baseapp\Suva\Models\Issues::findFirst($issueId);
		if(!$issue) return $this->greska("Nije pronaden issue sa brojem  $issueId");
		//error_log ("ReportsController:dtpErrorsAction: 5");
		
		$this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
		$this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');
		
		$this->tag->setTitle('DTP Errors');
		$this->view->setLayout('common');
		$this->view->setVar("_context", $_SESSION['_context']);
		$this->view->setVar("_model", new \Baseapp\Suva\Models\Model());
		$this->view->setVar("issue", $issue);
		
		//$mpdf = new \mPDF();
		//LANDSCAPE, http://stackoverflow.com/questions/20842347/how-to-set-the-page-in-landscape-mode-in-mpdf
		$mpdf = new \mPDF('c','A4-L'); 
		
		$this->view->start();
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		$this->view->render('reports', 'dtpMissingPictures');
		$this->view->finish();
		
		$content = $this->view->getContent();
		$mpdf->WriteHTML($content );
		$mpdf->Output("DTP MISSING PICTURES FOR ".$issue->Publication()->slug." , issue $issue->id, published on ".date("Y-m-d", strtotime($issue->date_published)), "I");
		$this->view->disable();
		//error_log ("ReportsController:dtpErrorsAction: 9");
		
		
	}

	
	
	public function categoriesMappingsListAction($order_id = null, $pStatus = null) {
		//error_log("CashOutController:cashOutAction 1");
		//error_log(print_r($_REQUEST,true));
		
		$this->view->setVar("_context", $_SESSION['_context']);
		$this->view->setVar("_model", new \Baseapp\Suva\Models\Model());
		
		//Naslov
		$this->tag->setTitle('Categories Mappings List');
		$this->view->setLayout('common');
		$mpdf = new mPDF();
		$this->view->start();
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
			
		//$this->view->render('cash-out', 'cashOut');
		$this->view->render('reports','categoriesMappingsList');
			
		$this->view->finish();
		$content = $this->view->getContent();  //ovo bi trebao bit izgled stranice
			//error_log($content);
		$mpdf->WriteHTML($content );
		$mpdf->Output("Categories Mappings List.php", "I");
		$this->view->disable(); //ne ide na afterexecuteroute??
		
		
	}
	

	public function allAdsListActionOLD() {
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 600000);
		ini_set ('request_terminate_timeout', 600000);

		if(!array_key_exists('xml_report_issue_id', $_SESSION['_context'] )){
			$this->greska('Nije poslan parametar Issue ID');
			return $this->redirect_back();
		}
		$issueId = $_SESSION['_context']['xml_report_issue_id'];
		if($issueId == ''){
			$this->greska("Nije odabran Issue");
			return $this->redirect_back();
		}
		$issue = \Baseapp\Suva\Models\Issues::findFirst($issueId);
		if(!$issue){
			$this->greska("Nije pronaden issue sa brojem  $issueId");
			return $this->redirect_back();
		}
		 
		$txtSql ="
select 
	a.id as ad_id
	,ins.id as ins_id
	,l.name as layout_name
	,a.n_avus_order_id 
	,a.title as ad_title
	,p.display_ad_height
	,p.display_ad_width
	,cm.name
	,sr.username as salesrep_name
	,a.n_source as ad_source
from
	n_insertions ins
		join orders_items oi on (ins.orders_items_id = oi.id)
			join n_products p on (oi.n_products_id = p.id)
				join n_layouts l on (p.layouts_id = l.id)
			join n_categories_mappings cm on (oi.n_categories_mappings_id = cm.id)
			join orders o on (oi.order_id = o.id)
				join users sr on (o.n_sales_rep_id = sr.id)
			join ads a on (oi.ad_id = a.id)
	
where
	ins.issues_id = $issue->id
	and ins.is_active = 1
		"; 
		  
		$insertions = $issue->Insertions("is_active = 1");
		
		error_log(print_r($insertions->toArray(),true));
		// Nava::poruka ($insertions->count());
		
		$txt = "";
		$txt .="AD ID; AVUS ID; AD LAYOUT;Naslov;Ad height mm;Ad Width mm;SUVA CATEGORY;Grupa;Status;Salesrep;Source;\n";
		foreach( $insertions as $insertion){
						
			$orderItem = $insertion->OrderItem(); 			
			$product = $orderItem->Product();			
			$ad = $orderItem->Ad();
			$order = $orderItem->Order();
			$catMap  = $orderItem->CategoryMapping();
			$catMap2 = $ad->CategoryMapping();
			$dtpGroup = $catMap ? $catMap->DtpGroup() : null;
			$salesRep = $order->SalesRep();
			$adLayout = $product->Layout();
			
			$adAvusId = $ad->n_avus_order_id ? $ad->n_avus_order_id : 'n/a';
			// $adLayout = $product->old_product_id;
			
			$adLayoutName = $adLayout->name ? $adLayout->name : n/a;
			$adTitle = $ad->title;
			$adHeight = $product->display_ad_height;
			$adWidth = $product->display_ad_width;
			$catMapName = $catMap ? $catMap->name : 'n/a';
			$catMapPath = $catMap ? $catMap->n_path : 'n/a';
			$adGroupName = $dtpGroup ? $dtpGroup->name : 'n/a';
			$adStatus = $order->OrderStatus()->name;
			$adSalesRepName = $salesRep ?  $salesRep->username : 'n/a';
			$adSource = $ad->n_source;
			
			
			
			$txt .= $ad->id.";";
			$txt .= $adAvusId.";";
			$txt .= $adLayoutName.";";
			$txt .= $adTitle.";";
			$txt .= $adHeight.";";
			$txt .= $adWidth.";";
			$txt .= $catMapPath.";";
			$txt .= $adGroupName.";";
			$txt .= $adStatus.";";
			$txt .= $adSalesRepName.";"; 
			$txt .= $adSource.";";
			$txt .= "\n";
		
		
		}


		// Nava::poruka ($issue->InsertionsAvus()->count());
		
		// $this->view->pick ("reports/index");
		
		// return;

		
		foreach( $issue->InsertionsAvus() as $insertionAvus ){
			$ad = $insertionAvus->Ad();
			$issue = $insertionAvus->Issue();
			
			if (! $ad->n_avus_adlayout_id){
				Nava::greska("Na oglasu $ad->id koji je importiran iz Avusa (AVUS ID: $ad->n_avus_order_id  ) nije unesen avus_adlayout_id ");
				continue;
			}
			$avusAdLayoutId = $ad->n_avus_adlayout_id > 0 ? $ad->n_avus_adlayout_id : -1;
			$product = \Baseapp\Suva\Models\Products::findFirst( " avus_adlayout_id = $avusAdLayoutId " );
			if (!$product) {
				Nava::greska("Na oglasu $ad->id koji je importiran iz Avusa (AVUS ID: $ad->n_avus_order_id  ) nije pronađen proizvod koji ima avus_adlayout_id = $avusAdLayoutId");
				continue;
			}
			
			
			if (! $ad->n_avus_class_id){
				Nava::greska("Na oglasu $ad->id koji je importiran iz Avusa (AVUS ID: $ad->n_avus_order_id  ) nije unesen avus_class_id ");
				continue;
			}
			$avusClassId = $ad->n_avus_class_id > 0 ? $ad->n_avus_class_id : -1;
			
			$catMap  = \Baseapp\Suva\Models\CategoriesMappings::findFirst( " avus_class_id = $avusClassId " );
			if (!$catMap) {
				Nava::greska("Na oglasu $ad->id koji je importiran iz Avusa (AVUS ID: $ad->n_avus_order_id  ) nije pronađena kategorija ( Publication Category ) koja ima avus_class_id = $avusClassId");
				continue;
			}
			
			$dtpGroup = $catMap ? $catMap->DtpGroup() : null;
			
			
			$adAvusId = $ad->n_avus_order_id ? $ad->n_avus_order_id : 'n/a';
			// $adLayout = $product->old_product_id;
			$adLayout = $product->Layout();
			$adTitle = str_replace(";"," ",$ad->title);
			$adHeight = $product->display_ad_height;
			$adWidth = $product->display_ad_width;
			$catMapName = $catMap ? $catMap->name : 'n/a';
			$catMapPath = $catMap ? $catMap->n_path : 'n/a';
			$adGroupName = $dtpGroup ? $dtpGroup->name : 'n/a';
			$adStatus = 'n/a';
			$adSalesRepName =  'n/a';
			$adSource = $ad->n_source;
			$adLayoutName = $adLayout->name ? $adLayout->name : n/a;
			
			$txt .= $ad->id.";";
			$txt .= $adAvusId.";";
			$txt .= $adLayoutName.";";
			$txt .= $adTitle.";";
			$txt .= $adHeight.";";
			$txt .= $adWidth.";";
			$txt .= $catMapPath.";";
			$txt .= $adGroupName.";";
			$txt .= $adStatus.";";
			$txt .= $adSalesRepName.";"; 
			$txt .= $adSource.";";
			$txt .= "\n";
		
		
		}

		
		
		// //po insertionima AVUS
		// error_log('reportsController:slikeList:340 - po Insertionima AVUS');
		// $insertionsAvus = $issue->InsertionsAvus();
		// foreach ( $insertionsAvus as $insertionAvus ) {

			// $ad = $insertionAvus->Ad();
			// if ($ad){
				// error_log("reportsController:slikeList, insertionAvus $insertionAvus->id, ad $ad->id");
				
				// $adTakerUserName = 'n/a';
				// $publication = \Baseapp\Suva\Models\Publications::findFirst(2); //oglasnik tiskano
				// $product = null;
				// if ( $ad->n_avus_adlayout_id > 0 ){
					// $product = \Baseapp\Suva\Models\Products::findFirst("avus_adlayout_id = $ad->n_avus_adlayout_id");
					// $productName = $product->name;
				// }

				// $category = $ad->CategoryMapping();
				// $layout = $product && $product->Layout() ? $product->Layout(): null;

				// $catMapId = $ad->n_avus_class_id > 0 ? $ad->n_avus_class_id : -1;
				// $catMap = \Baseapp\Suva\Models\CategoriesMappings::findFirst(" avus_class_id = $catMapId ");
				// $filename = \Baseapp\Suva\Library\libDTP::getPictureFileName ( null, $ad->id );
				
				
				// $txtSearch = $processedFolder.$filename.".*";
				// //$this->poruka($txtSearch);
				// $list = glob( $txtSearch );
				// $count = count( $list );
				// if ( $count == 1 ){
					// //OK nađen je jedan file
					// $foundFilePath = $list[0];
					// $foundFileName = substr($foundFilePath, strrpos($foundFilePath, '/') + 1);
					// $fileExtension = substr($foundFilePath, strrpos($foundFilePath, '.', - 1) + 1);
					// $msgFileFormat = $fileExtension;
				// }
				// elseif ( $count > 1 ){
					// //GREŠKA
					// $msgFileFormat = "Too many files. $txtSearch";
				// }
				// elseif ( $count == 0 ){
					// //GREŠKA
					// $msgFileFormat = "Missing. $txtSearch";
				// }

				// if ( $product && $product->is_picture_required ) {
					// $txt .= $publication->name.";";
					// $txt .= $issue->id.";";
					// $txt .= $catMap->n_path.";";
					// $txt .= $layout->name.";";
					// $txt .= "AVUS:$ad->n_avus_order_id SUVA:$ad->id".";";
					// $txt .= $adTakerUserName.";";
					// $txt .= $filename.";";
					// $txt .= $msgFileFormat.";"; // $txt .= ($ad && $ad->AdsMedia() && $ad->AdsMedia()->Media() ? $ad->AdsMedia()->Media()->mimetype : 'n/a');
					// // $txt .= $product->display_ad_width.";";
					// // $txt .= $product->display_ad_height.";";
					// // $txt .= "COLOR MODE";
					// $txt .= "\n";
				// }
			// }
			// else {  
				// //MAKNUTO DOK SE NE NAĐU GREŠKE NA INSERTIONIMA BEZ ORDER ITEMA
				// //Nava::poruka("Order item for insertion $insertion->id does not exist");
			// }
					
		// }		
		$this->view->setVar ('text', $txt);
		
		$issueFolder = \Baseapp\Suva\Library\libDTP::getIssueFolderName($issue);	
		$txtFileName = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published))."_all_ads_in_issue.csv";
		$txtFilePath = $issueFolder.$txtFileName;
		$this->sysMessage("Adding TXT to file $txtFilePath "); 
		file_put_contents($txtFilePath, $txt);
				
		$_SESSION['_context']['file_url'] = $txtFilePath;
		
		$this->view->setVar("_context", $_SESSION['_context']);
		
		//naslov
		$this->tag->setTitle('All Ads in issue');
		$this->view->pick ("reports/rptTextFileDownload");
		
		
		ini_set('max_execution_time', $appExecutionTime);
		ini_set('request_terminate_timeout', $appExecutionTime);
	}
	

	public function allAdsListAction() {
		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 600000);
		ini_set ('request_terminate_timeout', 600000);

		if(!array_key_exists('xml_report_issue_id', $_SESSION['_context'] )){
			$this->greska('Nije poslan parametar Issue ID');
			return $this->redirect_back();
		}
		$issueId = $_SESSION['_context']['xml_report_issue_id'];
		if($issueId == ''){
			$this->greska("Nije odabran Issue");
			return $this->redirect_back();
		}
		$issue = \Baseapp\Suva\Models\Issues::findFirst($issueId);
		if(!$issue){
			$this->greska("Nije pronaden issue sa brojem  $issueId");
			return $this->redirect_back();
		}
		 
		$txtSql ="
select 
	a.id as ad_id
	,ins.id as ins_id
	,l.name as layout_name
	,a.n_avus_order_id 
	,a.title as ad_title
	,p.display_ad_height
	,p.display_ad_width
	,cm.name as catmap_name
	,cm.n_path as catmap_n_path
	,dg.name as dtp_group_name
	,os.name as order_status
	,sr.username as salesrep_name
	,a.n_source as ad_source
from
	n_insertions ins
		join orders_items oi on (ins.orders_items_id = oi.id)
			join n_products p on (oi.n_products_id = p.id)
				join n_layouts l on (p.layouts_id = l.id)
			join n_categories_mappings cm on (oi.n_categories_mappings_id = cm.id)
				join n_dtp_groups dg on( dg.id  = cm.dtp_group_id )
			join orders o on (oi.order_id = o.id)
				join users sr on (o.n_sales_rep_id = sr.id)
				join n_order_statuses os on (os.id = o.n_status)
			join ads a on (oi.ad_id = a.id)
	
where
	ins.issues_id = $issue->id
	and ins.is_active = 1
		"; 
		  
		$insertions = Nava::sqlToArray($txtSql);
		
		$txt .="AD ID; AVUS ID; AD LAYOUT;Naslov;Ad height mm;Ad Width mm;SUVA CATEGORY;Grupa;Status;Salesrep;Source;\n";
		
		foreach($insertions as $insertion){
						
			$txt .= $insertion['ad_id'].";";
			$txt .= $insertion['n_avus_order_id'].";";
			$txt .= $insertion['layout_name'].";";
			$txt .= $insertion['ad_title'].";";
			$txt .= $insertion['display_ad_height'].";";
			$txt .= $insertion['display_ad_width'].";";
			$txt .= $insertion['catmap_n_path'].";";			
			$txt .= $insertion['dtp_group_name'].";";
			$txt .= $insertion['order_status'].";";
			$txt .= $insertion['salesrep_name'].";";
			$txt .= $insertion['ad_source'].";";
			$txt .= "\n";
			
			
		}
		


		// Nava::poruka ($issue->InsertionsAvus()->count());
		
		// $this->view->pick ("reports/index");
		
		// return;

		
		//NAPRAVIT SQL DA BUDE BRZE
		foreach( $issue->InsertionsAvus() as $insertionAvus ){
			$ad = $insertionAvus->Ad();
			$issue = $insertionAvus->Issue();
			
			if (! $ad->n_avus_adlayout_id){
				Nava::greska("Na oglasu $ad->id koji je importiran iz Avusa (AVUS ID: $ad->n_avus_order_id  ) nije unesen avus_adlayout_id ");
				continue;
			}
			$avusAdLayoutId = $ad->n_avus_adlayout_id > 0 ? $ad->n_avus_adlayout_id : -1;
			$product = \Baseapp\Suva\Models\Products::findFirst( " avus_adlayout_id = $avusAdLayoutId " );
			if (!$product) {
				Nava::greska("Na oglasu $ad->id koji je importiran iz Avusa (AVUS ID: $ad->n_avus_order_id  ) nije pronađen proizvod koji ima avus_adlayout_id = $avusAdLayoutId");
				continue;
			}
			
			
			if (! $ad->n_avus_class_id){
				Nava::greska("Na oglasu $ad->id koji je importiran iz Avusa (AVUS ID: $ad->n_avus_order_id  ) nije unesen avus_class_id ");
				continue;
			}
			$avusClassId = $ad->n_avus_class_id > 0 ? $ad->n_avus_class_id : -1;
			
			$catMap  = \Baseapp\Suva\Models\CategoriesMappings::findFirst( " avus_class_id = $avusClassId " );
			if (!$catMap) {
				Nava::greska("Na oglasu $ad->id koji je importiran iz Avusa (AVUS ID: $ad->n_avus_order_id  ) nije pronađena kategorija ( Publication Category ) koja ima avus_class_id = $avusClassId");
				continue;
			}
			
			$dtpGroup = $catMap ? $catMap->DtpGroup() : null;
			
			
			$adAvusId = $ad->n_avus_order_id ? $ad->n_avus_order_id : 'n/a';
			// $adLayout = $product->old_product_id;
			$adLayout = $product->Layout();
			$adTitle = str_replace(";"," ",$ad->title);
			$adHeight = $product->display_ad_height;
			$adWidth = $product->display_ad_width;
			$catMapName = $catMap ? $catMap->name : 'n/a';
			$catMapPath = $catMap ? $catMap->n_path : 'n/a';
			$adGroupName = $dtpGroup ? $dtpGroup->name : 'n/a';
			$adStatus = 'n/a';
			$adSalesRepName =  'n/a';
			$adSource = $ad->n_source;
			$adLayoutName = $adLayout->name ? $adLayout->name : n/a;
			
			$txt .= $ad->id.";";
			$txt .= $adAvusId.";";
			$txt .= $adLayoutName.";";
			$txt .= $adTitle.";";
			$txt .= $adHeight.";";
			$txt .= $adWidth.";";
			$txt .= $catMapPath.";";
			$txt .= $adGroupName.";";
			$txt .= $adStatus.";";
			$txt .= $adSalesRepName.";"; 
			$txt .= $adSource.";";
			$txt .= "\n";
		
		
		}

		
		
		// //po insertionima AVUS
		// error_log('reportsController:slikeList:340 - po Insertionima AVUS');
		// $insertionsAvus = $issue->InsertionsAvus();
		// foreach ( $insertionsAvus as $insertionAvus ) {

			// $ad = $insertionAvus->Ad();
			// if ($ad){
				// error_log("reportsController:slikeList, insertionAvus $insertionAvus->id, ad $ad->id");
				
				// $adTakerUserName = 'n/a';
				// $publication = \Baseapp\Suva\Models\Publications::findFirst(2); //oglasnik tiskano
				// $product = null;
				// if ( $ad->n_avus_adlayout_id > 0 ){
					// $product = \Baseapp\Suva\Models\Products::findFirst("avus_adlayout_id = $ad->n_avus_adlayout_id");
					// $productName = $product->name;
				// }

				// $category = $ad->CategoryMapping();
				// $layout = $product && $product->Layout() ? $product->Layout(): null;

				// $catMapId = $ad->n_avus_class_id > 0 ? $ad->n_avus_class_id : -1;
				// $catMap = \Baseapp\Suva\Models\CategoriesMappings::findFirst(" avus_class_id = $catMapId ");
				// $filename = \Baseapp\Suva\Library\libDTP::getPictureFileName ( null, $ad->id );
				
				
				// $txtSearch = $processedFolder.$filename.".*";
				// //$this->poruka($txtSearch);
				// $list = glob( $txtSearch );
				// $count = count( $list );
				// if ( $count == 1 ){
					// //OK nađen je jedan file
					// $foundFilePath = $list[0];
					// $foundFileName = substr($foundFilePath, strrpos($foundFilePath, '/') + 1);
					// $fileExtension = substr($foundFilePath, strrpos($foundFilePath, '.', - 1) + 1);
					// $msgFileFormat = $fileExtension;
				// }
				// elseif ( $count > 1 ){
					// //GREŠKA
					// $msgFileFormat = "Too many files. $txtSearch";
				// }
				// elseif ( $count == 0 ){
					// //GREŠKA
					// $msgFileFormat = "Missing. $txtSearch";
				// }

				// if ( $product && $product->is_picture_required ) {
					// $txt .= $publication->name.";";
					// $txt .= $issue->id.";";
					// $txt .= $catMap->n_path.";";
					// $txt .= $layout->name.";";
					// $txt .= "AVUS:$ad->n_avus_order_id SUVA:$ad->id".";";
					// $txt .= $adTakerUserName.";";
					// $txt .= $filename.";";
					// $txt .= $msgFileFormat.";"; // $txt .= ($ad && $ad->AdsMedia() && $ad->AdsMedia()->Media() ? $ad->AdsMedia()->Media()->mimetype : 'n/a');
					// // $txt .= $product->display_ad_width.";";
					// // $txt .= $product->display_ad_height.";";
					// // $txt .= "COLOR MODE";
					// $txt .= "\n";
				// }
			// }
			// else {  
				// //MAKNUTO DOK SE NE NAĐU GREŠKE NA INSERTIONIMA BEZ ORDER ITEMA
				// //Nava::poruka("Order item for insertion $insertion->id does not exist");
			// }
					
		// }		
		$this->view->setVar ('text', $txt);
		
		$issueFolder = \Baseapp\Suva\Library\libDTP::getIssueFolderName($issue);	
		$txtFileName = $issue->Publication()->slug."_".date("Y-m-d", strtotime($issue->date_published))."_all_ads_in_issue.csv";
		$txtFilePath = $issueFolder.$txtFileName;
		$this->sysMessage("Adding TXT to file $txtFilePath "); 
		file_put_contents($txtFilePath, $txt);
				
		$_SESSION['_context']['file_url'] = $txtFilePath;
		
		$this->view->setVar("_context", $_SESSION['_context']);
		
		//naslov
		$this->tag->setTitle('All Ads in issue');
		$this->view->pick ("reports/rptTextFileDownload");
		
		
		ini_set('max_execution_time', $appExecutionTime);
		ini_set('request_terminate_timeout', $appExecutionTime);
	}
	
	public function bankXmlUploadAction(){
        // Only 'admin' and 'finance' roles should be allowed here
        if (!$this->auth->logged_in(array('admin', 'finance'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('suva/reports');
        }

        if (!$this->request->hasFiles()) {
            $this->flashSession->error('Nothing uploaded!');
            return $this->redirect_to('suva/reports');
        }

        $files = $this->request->getUploadedFiles();

        if (count($files)) {
            // clear the session for last uploaded set of files
            $this->session->set($this->session_import_warnings_key, array());

            foreach ($files as $file) {
                $this->processBankXML($file);
            }
        }

        // return $this->redirect_to('suva/reports');
		
    }
	
	protected function processBankXML($file){
        if ($file instanceof FileInterface) {
            $filepath = $file->getTempName();
            $filename = $file->getName();
        } else {
            $filepath = $file;
            $filename = basename($filepath);
        }

        // Fix crap 4 bytes in the beggining of file
        $filepath = $this->fixInvalidXmlFile($filepath);

        $processor = new SimpleXMLProcessorRba();

        // Avoiding errors/warnings from libxml since the RBA xml DTD is missing
        if (!$processor->open($filepath, LIBXML_NOERROR | LIBXML_NOWARNING)) {
            throw new \RuntimeException('Unable to open XML file');
        }

        // Trying to fully/properly validate the XML fails since the referenced DTD is missing:
        // `xmlns="http://www.etna.hr/schemas/Izvod"` -> 404
        /*
        $processor->setParserProperty(\XMLReader::VALIDATE, true);
        if (!$processor->isValid()) {
            $this->flashSession->error('Failed to validate XML');
        }
        */

        $processor->parse();
        $processor->close();

        if ($processor->called()) {

            $audit_log_prefix = 'Bank XML upload (' . $filename . '): ';
            $this->flashSession->success('XML (' . $filename . ') successfully parsed.');

            $processor->processFoundPbos();

            $stats = $processor->getStats();
            if ($stats['pbo_matches'] > 0) {
                $message_matches = sprintf('Found %d payment record(s) matching our PBO prefix', $stats['pbo_matches']);
                if ($stats['pbo_duplicates'] > 0) {
                    $message_matches .= sprintf(' (%d duplicate(s))', $stats['pbo_duplicates']);
                }
                $this->flashSession->notice($message_matches);
                $this->saveAudit($audit_log_prefix . $message_matches);
            } else {
                $message_no_matches = 'No payments matching our PBO prefix were found';
                $this->flashSession->notice($message_no_matches);
                $this->saveAudit($audit_log_prefix . $message_no_matches);
            }
            if ($stats['completed'] > 0) {
                $message_completed = sprintf('Finalized %d orders', $stats['completed']);
                $this->flashSession->notice($message_completed);
                $this->saveAudit($audit_log_prefix . $message_completed);

                // Save info about finalized/completed orders in the audit log
                $completed_messages = $processor->getCompletedMessages();
                if (!empty($completed_messages)) {
                    foreach ($completed_messages as $message) {
                        $this->saveAudit($audit_log_prefix . $message);
                    }
                }
            }

            // Show any warnings that might have have occurred
            $warnings = $processor->getWarnings();
            if (!empty($warnings)) {
                foreach ($warnings as $warning) {
                    $this->flashSession->warning($warning);
                    $this->saveAudit($audit_log_prefix . $warning);
                }

                // get warnings formatted for export also
                $export_warnings = $processor->getWarnings($for_export = true);
                if (!empty($export_warnings)) {
                    $session_order_export_warnings = $this->session->get($this->session_import_warnings_key);
                    $session_order_export_warnings[] = array(
                        'filename' => $filename,
                        'warnings' => $export_warnings,
                    );
                    $this->session->set($this->session_import_warnings_key, $session_order_export_warnings);
                }
            }
        } else {
            $this->flashSession->error('XML parser callbacks have not been called! Is the XML file structure ok?');
        }
		
		// error_log(print_r($export_warnings,true));
		// error_log("XoXoXoXoX".$completed_messages);
		// $rptsController = \Baseapp\Suva\Controllers\Reports();
		$this->view->pick('reports/rptTextFileDownload');
		$allMsgs = $this->convert_multi_array($export_warnings);
		
		$this->view->setVar('text',$allMsgs.";");
		
		/*kreirat textualni file sadržajem $txt */
		
		$bsFolderTitle ="bankStatement". date("Y-m-d", time())."_REPORTS_CSV";
		$issueFolder = "repository/reports/$issueFolderTitle/";
		
		if (!file_exists($issueFolder)) {
			mkdir($issueFolder, 0755, true);
		}
		
		
				
		$txtFileName = "bankStatement_".date("Y-m-d h-I-s", time()).".csv";
		$txtFilePath = $issueFolder.$txtFileName;
		$this->sysMessage("Adding TXT to file $txtFilePath "); 
		file_put_contents($txtFilePath, $allMsgs);
		
				
		$_SESSION['_context']['file_url'] = $txtFilePath;
		
		$this->view->setVar("_context", $_SESSION['_context']);
		
    }
	
	
}



