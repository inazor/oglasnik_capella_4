<?php

namespace Baseapp\Suva\Controllers;

class AppSettingsController extends IndexController{

    public function indexAction() {
		 \Baseapp\Suva\Library\Nava::poruka2("Message from ajax1 controller %s %s",'aaa','bbb');
		 \Baseapp\Suva\Library\Nava::poruka2("TATA");
		$this->tag->setTitle("App Settings");
		$this->n_query_index("AppSettings"); //ovo je definirano u BaseController
    }

	public function crudAction($pEntityId = null) {
		$this->view->pick('app-settings/crud');
		$this->n_crud_action("AppSettings", $pEntityId); //ovo je definirano u BaseController
	}
}
