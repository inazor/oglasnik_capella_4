<?php

namespace Baseapp\Suva\Controllers;

use Baseapp\Extension\CsrfException;
use Baseapp\Extension\Tag;
use Baseapp\Library\Email;
use Baseapp\Library\Utils;
use Baseapp\Library\Validations\ContactForm;
use Baseapp\Library\AdsFiltering;
use Baseapp\Suva\Models\ResetPasswords;
use Baseapp\Suva\Models\Users;
use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\Locations;
use Baseapp\Suva\Models\UsersMessages;
use Baseapp\Suva\Models\UsersOauth;
use Phalcon\Validation;
use Phalcon\Mvc\View;
use Baseapp\Traits\InfractionReportsHelpers;

/**
 * Suva User Controller
 */
class UserController extends IndexController
{
    use InfractionReportsHelpers;

    public $infractionReportModelClass = 'Baseapp\Suva\Models\Users';

    public function initialize()
    {
        parent::initialize();

        $this->setLayoutFeature('extra_sidebar_boxes', false);
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        //$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if ($this->auth->logged_in()) {
            return $this->redirect_to('moj-kutak');
        } else {
            return $this->redirect_signin();
        }
    }

    private function redirect_if_wrong_url(Users $user)
    {
        $our_url_full       = $user->get_ads_page();
        $requested_url_full = $this->url->get($this->requested_uri_no_qs);

        if ($our_url_full !== $requested_url_full) {
            return $this->redirect_to($our_url_full, 301);
        }
    }

    public function adsAction($username = null, $view = null)
    {
        $base_url = 'korisnik/'. $username . '/oglasi';

        if (!$view) {
            return $this->redirect_to($base_url);
        }

        if (!$username || 'oglasi' !== $view) {
            return $this->trigger_404();
        }

        $user = Users::findFirstByUsername($username);

        if (!$user) {
            return $this->trigger_404();
        }

        $this->add_banner_zone('sidebar_left_1', '2285083', '<div id="zone2285083" class="goAdverticum"></div>');

        $auth_user = $this->auth->get_user();

        //$this->redirect_if_wrong_url($user);

        $this->setLayoutFeature('full_page_width', true);
        //$this->setLayoutFeature('extra_sidebar_boxes', true);

        $this->tag->setTitle('Svi oglasi korisnika: ' . $user->username);

        $logged_in_owner = false;
        if ($auth_user instanceof Users && $auth_user->id == $user->id) {
            $logged_in_owner = true;
        }

        $show_contact_form = true;
        $contact_form_url = $this->request->getURI();
        $show_contact_modal_on_load = false;

        // You don't have to contact yourself, at least not via this site...
        if ($logged_in_owner) {
            $show_contact_form = false;
        }
        // TODO/FUTURE: perhaps certain users or user-states or whatever will
        // have the contact form disabled for some reason... or maybe they can pay
        // for that privilege :) -- we can disable it easily here

        // Setup re-captcha for anon users if contact form is in use
        if ($show_contact_form && empty($auth_user)) {
            // $this->assets->addJs('https://www.google.com/recaptcha/api.js?hl=hr');
            $this->assets->collection('head')->addJs('https://www.google.com/recaptcha/api.js?hl=hr');
            $this->view->setVar('recaptcha_sitekey', $this->recaptcha_sitekey);
        }

        // Process user contact form submission
        if ($this->request->isPost()) {
            $abort               = false;
            $validation_messages = null;

            // If/when csrf checking fails, return an empty 403
            try {
                $this->check_csrf();
            } catch (CsrfException $ex) {
                return $this->do_403();
            }

            // Anon users need a verified captcha response
            if (empty($auth_user)) {
                $g_response = $this->request->getPost('g-recaptcha-response');
                $validation_messages = new Validation\Message\Group();
                if (empty($g_response)) {
                    $abort = true;
                    $validation_messages->appendMessage(new Validation\Message('Molimo potvrdite da niste robot', 'captcha'));
                }
                if (!$abort) {
                    $recaptcha = new \ReCaptcha\ReCaptcha($this->recaptcha_secret);
                    $response  = $recaptcha->verify($g_response, $this->request->getClientAddress(true));
                    if (!$response->isSuccess()) {
                        $abort = true;
                        foreach ($response->getErrorCodes() as $code) {
                            $validation_messages->appendMessage(new Validation\Message('Captcha greška: ' . $code, 'captcha'));
                        }
                    }
                }
            }

            // Don't even bother doing our validations if recaptcha failed
            if (!$abort) {
                $validation          = new ContactForm();
                $validation_messages = $validation->validate($this->request->getPost());
            }

            $messageData = array(
                'user_id'      => $user->id,
                'entity'       => 'users',
                'entity_id'    => $user->id,
                'sender_name'  => $this->request->getPost('name', 'string'),
                'sender_email' => $this->request->getPost('email'),
                'message'      => $this->request->getPost('message')
            );
            if ($this->request->isAjax()) {
                $this->disable_view();
                $this->response->setContentType('application/json', 'UTF-8');
                $response_array = array('status' => false);

                if (!count($validation_messages)) {

                    if (UsersMessages::send($messageData)) {
                        $response_array['status'] = true;
                        $response_array['msg']    = 'Poruka uspješno poslana korisniku';
                    } else {
                        $response_array['msg'] = 'Slanje poruke nije uspjelo (serverska greška)!';
                    }
                } else {
                    // Show errors
                    $errors = array();
                    foreach ($validation_messages as $message) {
                        $errors[] = array(
                            'field' => $message->getField(),
                            'value' => $message->getMessage()
                        );
                    }
                    $response_array['msg']    = '<h2>Oops!</h2><p>Molimo ispravite uočene greške.</p>';
                    $response_array['errors'] = $errors;
                }
                $this->response->setJsonContent($response_array);
                return $this->response;
            } else {
                if (!count($validation_messages)) {
                    if (UsersMessages::send($messageData)) {
                        $this->flashSession->success('Poruka uspješno poslana korisniku');
                    } else {
                        $this->flashSession->error('Slanje poruke nije uspjelo (serverska greška)!');
                    }

                    return $this->redirect_self();
                } else {
                    // Show errors
                    $this->view->setVar('errors', $validation_messages);
                    $show_contact_modal_on_load = true;
                    $this->flashSession->error('<h2>Oops!</h2><p>Molimo ispravite uočene greške.</p>');
                }
            }
        }

        $this->view->setVar('viewType', $this->getViewTypeData());

        $adTitle      = $this->request->has('ad_params_title') ? trim($this->request->get('ad_params_title')) : null;
        $picturesOnly = $this->request->has('ad_params_uploadable') && (int) $this->request->get('ad_params_uploadable', 'int', 0) ? (int) $this->request->get('ad_params_uploadable', 'int', 0) : null;
        $adPriceFrom  = $this->request->has('ad_price_from') && (int) $this->request->get('ad_price_from', 'int', 0) ? (int) $this->request->get('ad_price_from', 'int', 0) : null;
        $adPriceTo    = $this->request->has('ad_price_to') && (int) $this->request->get('ad_price_to', 'int', 0) ? (int) $this->request->get('ad_price_to', 'int', 0) : null;
        $filtersData  = array(
            'TITLE'      => array('ad_params_title' => $adTitle),
            'UPLOADABLE' => array('ad_params_uploadable' => $picturesOnly),
            'CURRENCY'   => array('ad_price_code' => 'HRK', 'ad_price_from' => $adPriceFrom, 'ad_price_to' => $adPriceTo)
        );
        $this->view->setVar('fields', array(
            'ad_params_title' => $adTitle,
            'ad_params_uploadable' => $picturesOnly,
            'ad_price_from' => $adPriceFrom,
            'ad_price_to' => $adPriceTo
        ));

        // Get users ads listing
        $adsFiltering = new AdsFiltering();
        $adsFiltering->cacheResults(false);
        $adsFiltering->cachePrefix('user-' . $user->id . '-ads');
        $adsFiltering->setLoggedInUser($auth_user);
        $adsFiltering->setCustomFilter($filtersData);
        $adsFiltering->setSortParamData($this->getSortParamData());
        $adsFiltering->showOnlyRootCategoriesCounts(false);
        $adsFiltering->allowSpecialProducts(true);
        $adsFiltering->setBaseURL($base_url);
        $adsFiltering->setAdUser($user);
        if ($searchCategoryId = $this->request->has('category_id') ? intval($this->request->get('category_id')) : null) {
            $adsFiltering->setAdCategoryByID($searchCategoryId);
        }

        $ads = $adsFiltering->getAds();
        if ($flashSession = $adsFiltering->getFlashSession()) {
            $this->flashSession->$flashSession['type']($flashSession['text']);
        }
        if ($adsFiltering->shouldTrigger404()) {
            return $this->trigger_404();
        }
        if (!$adsFiltering->hasError()) {
            $this->buildSortingDropdown();
            $this->view->setVar('ads', $ads);
            $this->view->setVar('searchCategoryId', $searchCategoryId);
            if ($pagination = $adsFiltering->getPagination()) {
                $this->view->setVar('pagination_links_top', $pagination['top']);
                $this->view->setVar('pagination_links', $pagination['bottom']);
            }
            if ($generatedViewVars = $adsFiltering->getGeneratedViewVars()) {
                foreach ($generatedViewVars as $varTitle => $varValue) {
                    $this->view->setVar($varTitle, $varValue);
                }
            }
            $this->assets->addJs('assets/js/users-classifieds.js');
            // setup assets
            if ($pageSpecificJS = $adsFiltering->getPageSpecificJS()) {
                $this->scripts['page_specific_js'] = $pageSpecificJS;
            }
            if ($pageJS = $adsFiltering->getPageAssetsJS()) {
                foreach ($pageJS as $assetJS) {
                    $this->assets->addJs($assetJS);
                }
            }
            if ($pageCSS = $adsFiltering->getPageAssetsCSS()) {
                foreach ($pageCSS as $assetCSS) {
                    $this->assets->addCss($assetCSS);
                }
            }
        }

        $this->view->setVar('user', $user);
        $this->view->setVar('logged_in_owner', $logged_in_owner);
        $this->view->setVar('show_contact_form', $show_contact_form);
        $this->view->setVar('contact_form_url', $contact_form_url);

        if ($show_contact_modal_on_load) {
            $this->scripts[] = 'jQuery(\'#modal-send-message\').modal(\'show\');';
        }
    }

    /**
     * Goes through "simple" oauth user profile fields and copies their values
     * into our $user model. "Simple" here refers to lack of processing username/email
     * and any other potentially "complicated" fields (in the sense that they would cause
     * more changes, uniqueness checks etc.).
     *
     * @param $user Users Our Users model
     * @param $user_profile \Hybrid_User_Profile
     *
     * @return Users
     */
    protected function oauth_set_user_fields_from_profile($user, $user_profile)
    {
        // mapping of hybridauth's field names and our local user model field names
        $profile_fields_mapping = array(
            // hybridauth => local
            'firstName' => 'first_name',
            'lastName' => 'last_name',
            'phone' => 'phone1',
            'zip' => 'zip_code',
            'city' => 'city',
            'address' => 'address'
        );
        foreach ($profile_fields_mapping as $hybrid_key => $local_key) {
            if (!empty($user_profile->$hybrid_key)) {
                $user->$local_key = $user_profile->$hybrid_key;
            }
        }

        // query country and region by name and use our location IDs if matched
        $db_checks_map = array(
            'country' => 'country_id',
            'region' => 'county_id'
        );
        foreach ($db_checks_map as $hybrid_key => $local_key) {
            if (!empty($user_profile->$hybrid_key)) {
                $location = Locations::findFirst(array(
                    'conditions' => 'name = :name:',
                    'bind' => array(
                        'name' => $user_profile->$hybrid_key
                    )
                ));
                if ($location) {
                    $user->$local_key = $location->id;
                }
            }
        }

        return $user;
    }

    /**
     * Retrieves a \Baseapp\Models\Users object for use in oauth flows.
     *
     * First checks the "fastest" case, which is an existing local user with an existing
     * oauth provider association.
     * If that fails, we attempt to find a local user by email.
     * If no user is found, we create a new one.
     *
     * In all cases we update the local account with the latest values received from the oauth profile.
     *
     * @param string $provider
     * @param \Hybrid_User_Profile $user_profile
     *
     * @return \Baseapp\Models\Users | \Phalcon\Mvc\Model | \Phalcon\Mvc\Model\Resultset
     */
    protected function oauth_get_or_create_user($provider, $user_profile)
    {
        // require a valid email within the profile no matter what
        $oauth_email = $this->oauth_get_email_from_profile($user_profile);
        $oauth_identifier = $user_profile->identifier;

        $new_user_created = false;

        // First we check by provider's id, if that fails try by email, or create a new user account
        $oauth_user = UsersOauth::findFirst(array(
            'conditions' => 'identifier = :identifier: AND provider = :provider:',
            'bind' => array(
                'identifier' => $oauth_identifier,
                'provider'   => $provider,
            )
        ));

        if ($oauth_user) {
            $user = $oauth_user->user;
        } else {
            // check if we have a user with $oauth_email potentially registered earlier?
            $user = Users::findFirstByEmail($oauth_email);

            // create a new user account if we don't
            if (!$user) {
                if ($user = $this->oauth_prepare_new_user($user_profile, $oauth_email)) {
                    $new_user_created = true;
                }
            }
        }

        // Update any of the myriad of profile fields we could've gotten back
        $user = $this->oauth_set_user_fields_from_profile($user, $user_profile);

        // Update the user's email address in case it's changed
        $existing_email = $user->email;
        if ($existing_email !== $oauth_email) {
            $user->email = $oauth_email;
        }

        // Save any changes we made to the $user object. This also handles creating a new user account!
        // TODO: should we check for errors? what if they do happen?
        $user->save();

        // Create the oauth association if there isn't one (which is the case when we haven't found an existing oauth user)
        if (!$oauth_user) {
            $this->oauth_create_association($user->id, $oauth_identifier, $provider, $user_profile);

            if ($new_user_created) {
                $user->send_oauth_welcome_email();
            }
        }

        /**
         * TODO/FIXME: This effectively means you can unconditionally 'activate' your previously registered (but not activated)
         * account by logging in with an Oauth provider... Not sure if there are any "bad" consequences?
         * It also means that if their login role is revoked at any point in time, they can re-acquire it
         * automatically just by logging in with an Oauth provider. This will influence how we do banning and such.
         */
        // Grants the user the login role if he doesn't have it already
        $user->activation();

        // return the updated $user object
        return $user;
    }

    /**
     * Instantiates and returns a new Users object with required data for a new oauth account.
     * NB: Calling save() or update()/create() is the caller's responsibility!
     *
     * @param \Hybrid_User_Profile $user_profile
     * @param string $email
     *
     * @return Users
     */
    protected function oauth_prepare_new_user($user_profile, $email)
    {
        $user = new Users();
        $user->assignDefaults(array('active' => 1));

        $user->email = $email;

        // Checking username uniqueness and generating a new one if needed
        $username = Users::sanitizeUsername($user_profile->displayName);
        $existing = $this->oauth_find_by_username($username);
        if ($existing) {
            $username = $this->oauth_create_unique_username_from_existing_user($existing);
        }

        $user->username = $username;

        // Creating a random password for the user, this way he can later request a pwdforgot/pwdreset even without oauth
        $rnd_pwd = \Phalcon\Text::random(\Phalcon\Text::RANDOM_ALNUM, 22);
        $user->password = $this->auth->hash_password($rnd_pwd);

        return $user;
    }

    /**
     * @param $username
     *
     * @return \Phalcon\Mvc\Model
     */
    protected function oauth_find_by_username($username)
    {
        $existing_user = Users::findFirst(array(
            'conditions' => 'username = :username:',
            'bind'       => array(
                'username' => $username
            )
        ));

        return $existing_user;
    }

    protected function oauth_create_unique_username_from_existing_user(Users $user)
    {
        $suffix       = 1;
        $original     = $user->username;

        do {
            $new_username = $original . $suffix++;
        } while ($this->oauth_find_by_username($new_username));

        return $new_username;
    }

    /**
     * Tries to get a valid e-mail address from the oauth profile, throws an exception if
     * no address is found, or if the address is not valid.
     *
     * @param \Hybrid_User_Profile $user_profile
     *
     * @return string E-mail address
     * @throws \Exception When an invalid or non-existing e-mail address is not found
     */
    protected function oauth_get_email_from_profile($user_profile)
    {
        $oauth_email = $user_profile->emailVerified;
        if (empty($oauth_email)) {
            // fall back on just email
            $oauth_email = $user_profile->email;
        }

        // if still empty, throw
        if (empty($oauth_email)) {
            throw new \Exception('Prijava putem socijanih mreža zahtjeva ispravnu e-mail adresu (a vraćena nam je prazna/nepostojeća adresa)');
        }

        // TODO: check email validity, since there've been cases of emailVerified being a phone number?

        return $oauth_email;
    }

    /**
     * Handles oauth flows (facebook/google for now, registers unregistered users and
     * redirects them after success or flashes errors)
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function oauthAction()
    {
        // No point in attempting oauth if they're already logged in?
        if ($this->auth->logged_in()) {
            return $this->redirect_home();
        }

        // Get the 'provider' parameter
        $provider = null;
        $params = $this->router->getParams();
        if (isset($params[0]) && !empty($params[0])) {
            $provider = trim($params[0]);
        }

        // TODO: is this enough to completely handle the HA oauth flow? I have a feeling something is missing...
        if ($this->request->hasQuery('hauth_start') || $this->request->hasQuery('hauth_done')) {
            \Hybrid_Endpoint::process();
        }

        try {

            /**
             * @var \Hybrid_Provider_Adapter
             */
            $adapter = $this->hybridauth->authenticate($provider);
            if (!empty($adapter)) {
                /**
                 * get user's profile
                 * @var \Hybrid_User_Profile
                 */
                $user_profile = $adapter->getUserProfile();
                $local_user = $this->oauth_get_or_create_user($provider, $user_profile);
                $errors = $local_user->getMessages();
                if (count($errors)) {
                    // some errors occurred
                    \Baseapp\Bootstrap::log($errors);
                    $this->flashSession->error('<h2>Ups!</h2><p>Došlo je do greške prilikom spremanja podataka, molimo pokušajte ponovo.</p>');
                    return $this->redirect_to('user/signin');
                } else {
                    // everything seems to be ok. we need to check if current user is not banned and attempt password-less login and redirect.
                    // in case the user is banned, we should display a specific message.
                    if ($local_user->banned) {
                        $this->saveAudit(sprintf('Banned user oauth-login attempt: %s', $local_user->username));
                        $this->flashSession->error('<h2>Greška!</h2><p>Poštovani, Vaš korisnički profil je deaktiviran. Za sva dodatna pitanja obratite se na mail: <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a></p>');
                        return $this->redirect_to('user/signin');
                    } else {
                        return $this->oauth_finalize_login($local_user);
                    }
                }
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $code = $e->getCode();
            if (5 == $code) {
                switch ($message) {
                    // Facebook
                    case 'Authentication failed! The user denied your request.':
                        $message = 'Niste dozvolili pristup svojim Facebook podacima';
                        break;
                    case 'Authentication failed! Invalid nonce used for reauthentication.';
                        $message = 'Neispravni podaci prilikom pokušaja prijave';
                        break;

                    // Google
                    case 'Authentication failed! User has canceled authentication!':
                        $message = 'Odustali ste od prijave';
                        break;
                    case 'Authentication failed. Invalid request received!':
                        $message = 'Primljen nedozvoljeni zahtjev';
                        break;
                }
            }
            $this->flashSession->error('<h2>Ups!</h2><p>Došlo je do greške prilikom prijave: <strong>' . $message . '</strong></p>');
            $this->hybridauth->logoutAllProviders();
            return $this->redirect_to('user/signin');
        }
    }

    /**
     * Attempt to login a user account without a password and redirects back to
     * where they came from (if successful, if not, it throws an exception).
     *
     * @param string|Users $user A Users model or a username
     *
     * @return \Phalcon\Http\ResponseInterface
     * @throws \Exception
     */
    protected function oauth_finalize_login($user)
    {
        $login = $this->auth->login_without_password($user);
        if (!$login) {
            $reason = 'Nobody likes you :)'; // shouldn't happen, but...
            if (null === $login) {
                $reason = 'Nepostojeći korisnički račun';
            }
            if (false === $login) {
                $reason = 'Korisnički račun nema "login" privilegije';
            }
            throw new \Exception('Neuspješan pokušaj prijave (' . $reason . ')');
        }

        $this->flashSession->success('<h2>Prijava uspješna!</h2><p>Dobrodošli na Oglasnik.hr - upoznajte se s <a href="' . $this->url->get('moj-kutak/podaci') . '">vašim korisničkim profilom</a> ako već niste!</p>');
        return $this->redirect_back();
    }

    /**
     * Creates a UsersOauth record associating a local user account with an oauth provider
     *
     * @param int $user_id A local user's id
     * @param string $identifier Oauth provider's identifier
     * @param string $provider Oauth provider's name
     * @param null $profile_data Optional, complete profile data returned by hybridauth to be stored (will be json encoded)
     *
     * @return bool
     */
    protected function oauth_create_association($user_id, $identifier, $provider, $profile_data = null) {
        $oauth_user = new UsersOauth();
        $oauth_user->user_id = $user_id;
        $oauth_user->identifier = $identifier;
        $oauth_user->provider = $provider;
        if ($profile_data !== null) {
            $oauth_user->data = json_encode($profile_data);
        }
        return $oauth_user->create();
    }

    /**
     * Sign in Action
     */
    public function signinAction()
    {
        $this->setLayoutFeature('megamenu', false);

        if ($this->auth->logged_in()) {
            return $this->redirect_back();
        }

        $this->view->setRenderLevel(View::LEVEL_AFTER_TEMPLATE);
        $this->view->setLayout('modal');

        $this->check_csrf();

        $this->tag->setTitle('Prijava');

        if ($this->request->hasPost('username') && $this->request->hasPost('password')) {
            $login_user = $this->auth->check_login_user_object($this->request->getPost('username'));
            $login = $this->auth->login(
                $login_user,
                $this->request->getPost('password'),
                $this->request->getPost('remember') ? TRUE : FALSE
            );
            if (!$login) {
                // Log the bad signin attempt along with some data
                $log_data = array(
                    'u' => $this->request->getPost('username'),
                    'p' => $this->request->getPost('password'),
                    'remember' => $this->request->getPost('remember') ? 1 : 0
                );

                // Prepare and set view errors
                $errors = new \Phalcon\Validation\Message\Group();
                if ($login_user && $login_user->banned) {
                    $this->saveAudit(sprintf('Banned user login attempt: %s', var_export($log_data, true)));
                    $errors->appendMessage(new \Phalcon\Validation\Message('Poštovani, Vaš korisnički profil je deaktiviran. Za sva dodatna pitanja obratite se na mail: <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>'));
                } else {
                    $this->saveAudit(sprintf('Failed login attempt: %s', var_export($log_data, true)));
                    $errors->appendMessage(new \Phalcon\Validation\Message('Korisničko ime i/ili lozinka nisu ispravni'));
                }
                $this->view->setVar('errors', $errors);
            } else {
                // Log the successful signin event and redirect
                $this->saveAudit(sprintf('User %s signed in', $this->auth->get_user()->ident()));
                return $this->redirect_back();
            }
        }
    }

    /**
     * Sign up Action
     */
    public function signupAction()
    {
        if ($this->auth->logged_in()) {
            // $this->flashSession->warning('<h2>Upozorenje</h2> Registracija nije moguća dok ste prijavljeni u sustav.');
            return $this->redirect_back();
        }

        $registrationType = Users::TYPE_PERSONAL;
        $viewFile = 'personal';
        if ($this->request->has('company')) {
            $registrationType = Users::TYPE_COMPANY;
            $viewFile = 'company';
        }

        $this->check_csrf();

        $this->tag->setTitle('Registracija novog korisničkog računa');
        $this->assets->addJs('assets/vendor/jquery.date-dropdowns.js');
        $this->assets->addJs('assets/js/register.js');

        $show_form = true;
        if ($this->request->isPost()) {
            $user = new Users();
            $signup = $user->signup($this->request);

            if ($signup instanceof Users) {
                $this->flashSession->success('<strong>Registracija uspješna!</strong> Aktivirajte svoj korisnički račun slijedeći link u e-mail poruci koju smo Vam upravo poslali.');
                $show_form = false;
            } else {
                $this->view->setVar('errors', $signup);
                $errors_markup = $this->buildErrorsListMarkupFromValidationMessages($signup);
                $this->flashSession->error('<strong>Ooops!</strong> ' . $errors_markup);
            }
        }

        $this->view->setVar('show_form', $show_form);

        if ($show_form) {
            // registration type (hidden field)
            Tag::setDefault('registration_type', $registrationType);

            // country dropdown
            $chosen_country_id = 1;
            if (!$this->request->hasPost('country_id')) {
                // set default country to Croatia for non-post requests
                Tag::setDefault('country_id', $chosen_country_id);
            } else {
                $chosen_country_id = $this->request->getPost('country_id');
            }
            $countries = Locations::findCountries();
            $selectables = array();
            foreach ($countries as $country) {
                $selectables[$country->id] = array(
                    'text'       => $country->name,
                    'attributes' => array(
                        'data-iso-code'     => $country->iso_code,
                        'data-calling-code' => $country->calling_code
                    )
                );
            }
            $country_dropdown = Tag::select(array(
                'country_id',
                $selectables,
                'useEmpty' => true,
                'emptyText' => 'Odaberite državu...',
                'emptyValue' => '',
                'class' => 'form-control'
            ));
            $this->view->setVar('country_dropdown', $country_dropdown);

            // county dropdown
            $county_dropdown = Tag::select(array(
                'county_id',
                Locations::findCounties($chosen_country_id),
                'using' => array('id', 'name'),
                'useEmpty' => true,
                'emptyText' => 'Odaberite županiju...',
                'emptyValue' => '',
                'class' => 'form-control'
            ));
            $this->view->setVar('county_dropdown', $county_dropdown);
        }

        $this->view->pick('user/signup-' . $viewFile);
        $this->view->setVar('viewFile', $viewFile);
    }

    /**
     * Log out Action
     */
    public function signoutAction()
    {
        if ($this->auth->logged_in()) {
            // Logging early so the audit log gets the session data properly (in theory, log could show "signed out",
            // but something crashed before actually fully logging out, but screw it for now...)
            $this->saveAudit(sprintf('User %s signed out', $this->auth->get_user()->ident()));

            // Actually log out (and destroy session)
            $this->auth->logout(true);

            if ($this->hybridauth) {
                $this->hybridauth->logoutAllProviders();
            }

            // Only show the logout success message if we weren't internally forwarded/forced to log out
            // TODO/FIXME: Cannot use flashSession here since we've destroyed the session above
            // We'll have to implement this differently if we want to show a message on logout...
            /*
            if (!$this->dispatcher->wasForwarded()) {
                $this->flashSession->success('<h2>Odjava uspješna!</h2><p>Uspješno ste se odjavili.</p>');
            }
            */

            // TODO: might be wiser to just redirect to the homepage?
            return $this->redirect_back();
        } else {
            return $this->redirect_home();
        }
    }

    /**
     * Activation Action
     */
    public function activationAction()
    {
        // no reason for a logged in user to visit this, right?
        if ($this->auth->logged_in()) {
            // $this->flashSession->warning('<strong>Upozorenje</strong> Aktivacija korisničkog računa nije moguća dok ste prijavljeni.');
            return $this->redirect_back();
        }

        // if no required params found, bail
        $params = $this->router->getParams();
        if (!isset($params[0]) || empty($params[0])
            &&
            !isset($params[1]) || empty($params[1])
        ) {
            return $this->redirect_back();
        }

        $this->tag->setTitle('Aktivacija korisničkog računa');
        $this->view->setVar('title', 'Aktivacija korisničkog računa');

        $user = Users::findFirst(array('username=:user:', 'bind' => array('user' => $params[0])));

        if ($user && md5($user->id . $user->email . $user->password . $this->config->auth->hash_key) == $params[1]) {
            $activation = $user->activation();
            if ($activation === null) {
                $this->flashSession->success('<h2>Super!</h2><p>Vaš korisnički račun je već ranije aktiviran.</p>');
            } elseif ($activation === true) {
                $this->flashSession->success('<h2>Vaš korisnički račun je uspješno aktiviran!</h2><p>Prijavite se koristeći svoje pristupne podatke.</p>');
                // TODO: send another email (informative confirmation message that the account is activated/confirmed)?
            }
            $this->dispatcher->forward(array('controller' => 'user', 'action' => 'signin'));
        } else {
            $this->flashSession->error('<h2>Greška!</h2><p>Neispravni aktivacijski podaci.</p>');
        }
    }

    /**
     * Request a password reset by filling out a form with
     * an email address of a valid account and send it a password reset email
     */
    public function pwdforgotAction()
    {
        /**
         * TODO: there's a lot of code duplication for this check already...
         * Maybe have a special 'UserActionController' or something that will
         * handle all those cases and create the 'is user logged in?' check
         * only in a single place (@see beforeExecuteRoute())
         */
        if ($this->auth->logged_in()) {
            $this->flashSession->warning('<h2>Upozorenje!</h2><p>Zahtjev za promjenu zaboravljene lozinke nije moguć dok ste prijavljeni.</p>');
            return $this->redirect_back();
        }

        $this->check_csrf();

        $this->tag->setTitle('Zaboravljena lozinka');
        $this->view->setVar('title', 'Zaboravljena lozinka');

        $show_form = true;

        $is_post = $this->request->isPost();
        if ($is_post) {
            // process form data/validation
            $validation = new \Baseapp\Extension\Validation();
            $validation->add('email', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Molimo upišite vašu e-mail adresu'
            )));
            $validation->add('email', new \Phalcon\Validation\Validator\Email(array(
                'message' => 'Neispravna e-mail adresa'
            )));

            /**
            * FIXME: should we reveal this info? Or just send an email to the
            * entered address anyway?
            * When we don't have a matching email, we can send something like:
            *
            * This covers the case of users entering their "secondary" email address
            * thinking they registered with that one...
            * @link http://www.troyhunt.com/2012/05/everything-you-ever-wanted-to-know.html
            */
            // make sure we have a matching email address
            $validation->add('email', new \Baseapp\Extension\Validator\RecordExistence(array(
                'model' => '\Baseapp\Models\Users',
                'message' => 'Upisana E-mail adresa nije pronađena'
            )));

            $errors = $validation->validate($_POST);
        }

        if ($is_post && count($errors)) {
            $this->flashSession->error('<h2>Pažnja!</h2><p>Došlo je do greške prilikom obrade podataka.</p>');
            $this->view->setVar('errors', $errors);
        }

        if ($is_post && !count($errors)) {
            // all is good so far
            $user = Users::findFirst(array('email=:email:', 'bind' => array('email' => $this->request->getPost('email'))));
            if ($user) {
                // 1. delete any existing reset tokens for the user
                // 2. create and store new timestamped reset token
                // 3. send email with pwdreset url
                $reset_token = new ResetPasswords();
                $reset_token->user_id = $user->id;
                $reset_token->delete_user_tokens();
                $token_created = $reset_token->create();
                if (true === $token_created) {
                    $this->flashSession->success('<h2>Super!</h2><p>Upute za promjenu lozinke su uspješno poslane na upisanu e-mail adresu.</p>');
                    $show_form = false;
                    // TODO/FIXME: we can redirect to self after success to avoid double submits when
                    // someone tries refreshing the current page, but then we'll have to store the
                    // $show_form thingy in the session too...
                    // return $this->redirect_to('user/pwdforgot');
                } else {
                    $this->flashSession->success('<h2>Greška!</h2><p>Nije moguće kreirati podatke za promjenu lozinke.</p>');
                    $this->view->setVar('errors', $reset_token->getMessages());
                }

            } else {
                // this can only really happen if a user got deleted or DB is down
                // between the validation check and user fetch which only means
                // we should be doing a single query but fuck it for now...
                $this->flashSession->error('<h2>Greška!</h2><p>Došlo je do greške prilikom dohvata korisničkih podataka.</p>');
            }
        }

        $this->view->setVar('show_form', $show_form);
    }

    /**
     * Somewhat similar to user activation, a pwdreset token (sent to the
     * user's email address) allows the user to initiate a password reset.
     *
     * Upon successful submission of the pwdreset form the user's password is reset
     * to the new one (the one provided in the form) and he is automatically signed in.
     *
     * We also notify the user via email about the password being successfully reset.
     *
     * @param string|null $token
     *
     * @return \Phalcon\Http\ResponseInterface
     * @throws \Baseapp\Extension\CsrfException
     */
    public function pwdresetAction($token = null)
    {
        // FIXME: see if logged in users can do this or not!
        // FIXME: in theory, we could re-use a lot of this action for regular password changes too?
        if ($this->auth->logged_in()) {
            $this->flashSession->warning('<h2>Upozorenje!</h2><p>Resetiranje zaboravljene lozinke nije izvedivo jer ste već prijavljeni.</p>');
            return $this->redirect_back();
        }

        $this->check_csrf();

        $this->tag->setTitle('Postavljanje nove lozinke');
        $this->view->setVar('title', 'Postavljanje nove lozinke');

        /**
         * The general flow should be something like:
         * 1. verify the required parameters are present in the request
         * 2. match the hashed token from the url with the token in the DB
         * 3. If tokens match, show a pwdreset form, with two fields (password and passwordRepeat)
         * 4. Upon successful POST submit on step 3, change the user's password to the newly entered one, delete the reset token, email the user about the pwd change event
         */
        $error = false;

        // If there's a token in the POST request, override the one from the url
        if ($this->request->isPost() && $this->request->hasPost('token')) {
            $token = trim($this->request->getPost('token'));
        }

        // Check the token's presence
        if (empty($token)) {
            $this->flashSession->error('<h2>Greška!</h2><p>Nedostaje jedinstveni verifikacijski kôd.</p>');
            $error = true;
        } else {
            // Validate the token against the stored hash of it
            $token_hash = $this->auth->hash($token);
            $reset_token = new ResetPasswords();
            $reset_token = $reset_token->findFirst(array('token=:token:', 'bind' => array('token' => $token_hash)));
            if (!$reset_token) {
                $this->flashSession->error('<h2>Greška!</h2><p>Neispravan verifikacijski kôd.</p>');
                $error = true;
            }
        }

        // if no errors occurred so far, set it to show for now
        $show_form = !$error;

        // Process form submission, update the user's password, sign them in and redirect if successful
        if (!$error && $this->request->isPost()) {

            $show_form = true;

            // check that password fields match
            $validation = new \Baseapp\Extension\Validation();
            $validation->add('password', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Molimo upišite željenu lozinku'
            )));
            $validation->add('repeatPassword', new \Phalcon\Validation\Validator\Confirmation(array(
                'with' => 'password',
                'message' => 'Upisana lozinka nije identična onoj u polju "Nova željena lozinka"'
            )));

            $messages = $validation->validate($this->request->getPost());

            if (!count($messages)) {
                // no errors, change the user's password, sign them in and redirect
                if (isset($reset_token) && !empty($reset_token)) {
                    $user = $reset_token->user;
                    $pwd = $this->request->getPost('password');
                    $user->password = $this->auth->hash_password($pwd);
                    if (true === $user->update()) {
                        // delete the now used reset token
                        $reset_token->delete();
                        // send a notification email about successful password change
                        $user->send_pwdchanged_email();
                        // sign in the user
                        $signed_in = $this->auth->login($user, $pwd);
                        if ($signed_in) {
                            $this->flashSession->success('<h2>Cool!</h2><p>Uspješno ste promjenili svoju lozinku te smo Vas automatski prijavili s vašim novim podacima!</p>');
                            return $this->redirect_home();
                        } else {
                            $this->flashSession->warning('<h2>Ups!</h2><p>Došlo je do greške prilikom automatske prijave, molimo prijavite se pomoću svoje nove lozinke.</p>');
                            $this->dispatcher->forward(array('controller' => 'user', 'action' => 'signin'));
                        }
                    } else {
                        $this->flashSession->error('<h2>Ups!</h2><p>Došlo je do greške prilikom spremanja podataka, molimo pokušajte ponovo.</p>');
                        $this->view->setVar('errors', $user->getMessages());
                    }
                }
            } else {
                $this->flashSession->error('<h2>Ups!</h2><p>Molimo ispravite uočene greške.</p>');
                $this->view->setVar('errors', $messages);
            }

        }

        Tag::setDefault('token', $token);
        $this->view->setVar('token', $token);
        $this->view->setVar('show_form', $show_form);
    }

    public function pwdchangeAction()
    {
        // only logged in users can change their password
        if (!$this->auth->logged_in()) {
            return $this->redirect_signin();
        }

        $this->check_csrf();
    }

}
