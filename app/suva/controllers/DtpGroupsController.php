<?php

namespace Baseapp\Suva\Controllers;

class DtpGroupsController extends IndexController{

    public function indexAction() {
			$this->tag->setTitle("Dtp Groups");
			$this->n_query_index("DtpGroups"); //ovo je definirano u BaseController
    }

		public function crudAction($pEntityId = null) {
			$this->n_crud_action("DtpGroups", $pEntityId); //ovo je definirano u BaseController
			$this->view->pick('dtp-groups/crud');
		}
}