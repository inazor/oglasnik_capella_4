<?php
namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;
use Baseapp\Suva\Library\libAppSetting;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;




class libFirstSetup{
	
	public static function firstSetup(){
		$iniMemoryLimit = ini_get('memory_limit');
		ini_set('memory_limit', '8192M');
		
		$iniMaxExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		
		$old_error_log = ini_get('error_log');
		ini_set('error_log',ROOT_PATH."/logs/firstSetup.log");
		
		
		Nava::sysMessage("FIRST SETUP START ");
		
		Nava::sysMessage("START: Updating orders items");
		foreach( \Baseapp\Suva\Models\OrdersItems::find() as $oi ){
			$oi->update();
		}
		Nava::sysMessage("FINISH: Updating orders items");
		
		
		Nava::sysMessage("START: Updating orders totals");
		foreach( \Baseapp\Suva\Models\Orders::find( array("n_total > 0 and total = '0'", 'order' => 'id desc' )) as $order ){
			Nava::updateOrderTotal($order->id);
		}
		Nava::sysMessage("FINISH: Updating orders totals");
		
		Nava::sysMessage("START: Updating n_path");
        $dictionaries = \Baseapp\Suva\Models\Dictionaries::find();
		foreach ($dictionaries as $dictionary){
			$path = $dictionary->getPath();
			$path = str_replace("'" , "", $path);
			//($dictionary->path());
			
			//$dictionary->update(); //ne radi jer se na modelu Dictionaries koristi NestedSetBehavoiur
			$txtSql = "update dictionaries set n_path = '$path' where id = $dictionary->id";
			//error_log("ObradeController: updateDictionaryPathsAction: txtSql:".$txtSql);
			if ( $dictionary->n_path != $path )
				Nava::runSql ($txtSql);
		}
		Nava::sysMessage("Dictionaries n_paths updated");
		
		
		
		//error_log("ActionsController:obradaAction 11");
		foreach ( \Baseapp\Suva\Models\Locations::find() as $loc ){
			$path = $loc->getPath();
			$path = str_replace("'" , "", $path);
			//error_log("ActionsController:obradaAction 12 $path");
			if ( $loc->n_path != $path )
				Nava::runSql ("update location set n_path = '$path' where id = $loc->id");
		} 
		Nava::sysMessage("Location n_paths updated");

		foreach ( \Baseapp\Suva\Models\Categories::find() as $cat ){
			$path = $cat->getPath();
			$path = str_replace("'" , "", $path);
			if ( $cat->n_path != $path )
				Nava::runSql ("update categories set n_path = '$path' where id = $cat->id");
		} 
		Nava::sysMessage("Categories n_paths updated");
		
		foreach ( \Baseapp\Suva\Models\CategoriesMappings::find() as $cat ){
			$cat->save();
		} 
		Nava::sysMessage("Categories Mappings updated");
		
		Nava::sysMessage("START: Updating orders totals");
		Nava::runSql("update orders set n_status = 3 where status = 1");
		Nava::runSql("update orders set n_status = 5 where status = 2");
		Nava::runSql("update orders set n_status = 6 where status = 4");
		Nava::sysMessage("FINISH: Updating orders totals");
		
		

		
		
		//import iz Avusa
		
		//Agimov import iz Avusa
		Nava::sysMessage("START: Agim - imported from Avus");
		\Baseapp\Library\Mapping\MigrationRawDb::importFromAvus();
		Nava::sysMessage("FINISH: Agim - imported from Avus");
		
		//Suva Import iz Avusa
		//namještanje statusa avus_online 
		Nava::sysMessage("START: namještanje statusa avus_online");
		Nava::runSql("update ads a join ads_additional_data aad on (a.id = aad.ad_id) set a.n_avus_order_id = aad.avus_id , a.n_source = 'avus_online' where n_source = 'frontend'");
		Nava::sysMessage("FINISH: namještanje statusa avus_online");
		
		Nava::sysMessage("START: SUVA - import from Avus");
		\Baseapp\Suva\Library\libAVUS::syncAvusAdsWithRemainingInsertions();
		Nava::sysMessage("FINISH: SUVA - import from Avus");		
		
		
		Nava::sysMessage("START: Update Users Contacts");
		$obrade = new \Baseapp\Suva\Controllers\ObradeController();
		$obrade->updateUsersContactsAction();
		Nava::sysMessage("FINISH: Update Users Contacts");
		
		
		
		//sinhronizacija oglasa
		
		//Agimova obrada markExpiredAds
		Nava::sysMessage("START:Agim - markExpiredAds ");
		$ads = new \Baseapp\Models\Ads();
        $affected_rows = $ads->markExpired();
		Nava::sysMessage("FINISH:Agim - markExpiredAds ");
		
		Nava::sysMessage("START: Samo aktivni oglasi su stavljeni da se syncaju za frontend ");
		Nava::runSql("update ads set n_frontend_sync_status = 'no_sync' where active = 0");
		Nava::sysMessage("FINISH: Samo aktivni oglasi su stavljeni da se syncaju za frontend ");
		
		
		Nava::sysMessage("START: UpdateEveryMinute");
		\Baseapp\Suva\Library\libCron::updateEveryMinute();
		Nava::sysMessage("FINISH: UpdateEveryMinute");
		
		

		Nava::sysMessage("START: get fiscal info from NAV !");
		Nava::runSql("update orders set n_nav_sync_status  ='retry' where n_nav_sync_status = 'no_snyc' and n_status = 5");
		
		$orders = \Baseapp\Suva\Models\Orders::find("n_status = 5 and n_nav_sync_status = 'retry'");
		foreach( $orders as $order ){
			\Baseapp\Suva\Library\libNavision::testFiscalization( $order->id );
		}
		Nava::sysMessage("FINISH: get fiscal info from NAV !");
		
		
		
		Nava::sysMessage("FIRST SETUP FINISH ");
		
		ini_set('error_log',$old_error_log);		
		
		ini_set('max_execution_time', $iniMaxExecutionTime);
		ini_set('request_terminate_timeout', $iniMaxExecutionTime);		
		ini_set('memory_limit',$iniMemoryLimit);
		
	}
	
}
