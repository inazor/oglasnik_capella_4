<?php

namespace Baseapp\Suva\Library;

use Baseapp\Suva\Models\OrdersItems;
use Baseapp\Suva\Models\Products;
use Baseapp\Suva\Models\Orders;
use Baseapp\Suva\Models\Users;
use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\Model;


use Baseapp\Suva\Models\Categories; // radi categoriesWithPathsDict

use Baseapp\Library\RawDB;
use Phalcon\Db\Adapter\Pdo;  // SQL SERVER

class Nava {
	
	
	/*
	W A R N I N G ! ! !  
	ovo po defaultu sve mora biti true!!!
	*/
	public static $debugLogSysMessage = true;
	public static $debugLogSysError = true;
	public static $debugPrintOnClientSysMessage = true;
	public static $debugPrintOnClientSysError = true;
	
	

  public static function arrToDict($pArray){
//       provjerit je li ulaz Array, i ima li mu svaki element id. Ako ne, onda greška
      
      $result = array();
			if (!is_array($pArray)) return $result;
			foreach ($pArray as $value) { 
				$result[$value['id']] = $value; 
			} 
      return $result;
    }
	
	public static function getAllowedStatuses($user_id, $current_status_id) {
		//IGORE PIŠI OVDE
		
// 		  // configure and initialize source db
//         $source_db_config = array(
//             'host'     => 'sql.oglasnik.hr',
//             'username' => 'npongrac',
//             'password' => 'fief7eej',
//             'dbname'   => 'oglasnik',
//             'options'  => array(
//                 \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES latin1',
//                 \PDO::ATTR_EMULATE_PREPARES   => false,
//                 \PDO::ATTR_STRINGIFY_FETCHES  => false,
//             )
//         );
//         $source_db = new RawDB($source_db_config);
        // configure and initialize target db
        $target_db_config = array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $target_db = new RawDB($target_db_config);
				//$countries = $this->target_db->find("SELECT id, iso_code FROM location WHERE level = 1");
				$str = "select nos.id,nos.name,nos.description from users u ";
				$str = $str." join roles_users ru on u.id = ru.user_id ";
				$str = $str." join n_roles_permissions rp on ru.role_id = rp.role_id ";
				$str = $str." join n_order_statuses os on (rp.order_status_id = os.id) ";
				$str = $str." join n_order_status_transitions ost on rp.order_status_id = ost.order_status_id ";
				$str = $str." join n_order_statuses nos on ost.next_order_status_id = nos.id ";
				$str = $str." where ";
				$str = $str." u.id = :par_user_id ";
				$str = $str." and	os.id = :par_status_id ";
				$str = $str." and	ost.is_active = 1 ";
				$str = $str." and	os.is_active = 1 ";
				$str = $str." and	nos.is_active = 1 ";
// 				$str = $str." and	a.is_active = 1 ";
				$str = $str." group by nos.id, nos.name, nos.description ";
				//$str = $str." union select 99,'BLA', 'BLABLA' from users where id=1 ";
				
				//$str = "select  * from n_order_statuses";
				$statuses = $target_db->find($str,array(
                    'par_user_id' => $user_id,
										'par_status_id' => $current_status_id
										));
				//ne treba -> toArray jer već vraća Array
			
		

		//$moguci_statusi = list()
		//doznat koje role ima user

		//foreach rola in role_od_usera
		//		statusi_za_rolu = getStatusesForRole(rola)
		//		foreach status_za_rolu in statusi_za_rolu	
		//				if ! moguci_statusi[status_za_rolu]
		//					moguci_statusi.add (status_za_rolu)
							
		
		
		//return self::find(array('id = :status',
        //    					'bind' => array('status' => $current_status)
        //    					));

		//return self::find();
		return $statuses;
		//return array('id = 1', 'name = KOSARICA', 'desc = Kosarica');
	}
	
	
	//MYSQL FUNKCIJE
	public static function sqlToArray($pSql) {
				//pokreće sql query i vraća rezultat u array	

		//error_log ("SQLTOARR- SQL:".$pSql);
        
		$target_db_config = array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $target_db = new RawDB($target_db_config);
		$result = $target_db->find($pSql,array());
		
		
		//error_log ("SQLTOARR- RESULT:".print_r($result, true));
		
		return $result;
		
	}
	public static function findFirst($pSql) {
				//pokreće sql query i vraća rezultat u array	

		$result = Nava::sqlToArray($pSql);
		
		return $result[0];
		
		
	}

	public static function runSql($pSql){
						//pokreće sql query i vraća rezultat u array	

		//error_log ("SQLTOARR- SQL:".$pSql);
		try{
			$target_db_config = array(
				'options'      => array(
					\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
					\PDO::ATTR_EMULATE_PREPARES   => false,
					\PDO::ATTR_STRINGIFY_FETCHES  => false,
					\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION
				)
			);
			$target_db = new RawDB($target_db_config);
			$result = $target_db->query($pSql,array());
		}
		
		catch(\PDOException $exception){ 
			self::sysError("SQL ERROR: ".$exception->getMessage()."<br/> while executing query : $pSql " ); 
		} 	
		catch(\Exception $exception){ 
			self::sysError("SQL ERROR: ".$exception->getMessage()."<br/> while executing query : $pSql " ); 
		} 	

		//error_log ("SQLTOARR- RESULT:".print_r($result, true));
		
	}

	public static function dbQuote($pText){
        
		$target_db_config = array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $target_db = new RawDB($target_db_config);
		$result = $target_db->quote($pText);
		return result;
		
	}

	
	
	public static function newId( $pTableName = null ){
		
		return Nava::sqlToArray("select max(id) +1 as id from $pTableName" )[0]['id'];
	}
	
	//SQL SERVER FUNKCIJE
	public static function msSqlToArray($pSql) {
				//pokreće sql query i vraća rezultat u array	
		//error_log("SQLTOARR:".$pSql);
		
		$db = new \PDO('dblib:host=195.245.255.22;port=1433;dbname=OGLASNIK_PROD', 'sa', 'Tassadar1337');
		//$sqlStatement ="select count(*) as broj from [Oglasnik d_o_o_".'$'."Sales Invoice Header] where [Posting Date] > '2016-02-01'";		
		//$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$statement = $db->prepare((string)$pSql);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
		
// 		return $db->errorCode(); // vraća 00000 uvijek
// 		return $statement->errorCode(); // vraća 00000 uvijek
		//return $statement->errorInfo(); // vraća 00000 uvijek
		/*$greska = "borna";
		if($result = 0){
			return $greska;
		}*/
// 		if ($greska) return $greska;
		
		

		$greska = "sql error";
		if(empty( $result )){
			return $greska;
		} else {
			
		}
		
		
		
	}
	
	
	public static function getCheckedDiscountsForOrderItem( $pOrdersItemsId = null ){
		//152 - izmijeniti vezu discounta i producta, dodati i publikaciju. Discount koji je vezan samo uz publikaciju se pojavljuje meovisno o proizvodu
		// ....or oi.n_publications_id = dp.publications_id
		
		if (is_int($pOrdersItemsId)){
			$txtSql = "select d.*, "
				." (case when oid.orders_items_id is null then false else true end ) as checked "
				." from orders_items oi "
				." 		join n_discounts_products dp on (oi.n_products_id = dp.products_id or oi.n_publications_id = dp.publications_id) "
				." 			join n_discounts d on dp.discounts_id = d.id "
				."		join n_orders_items_discounts oid on (oi.id = oid.orders_items_id and d.id = oid.discounts_id) "
				." where "
				."		oi.id = ".$pOrdersItemsId;
				
			return Nava::sqlToArray($txtSql);
		} else {
			return null;
		}
	}

	 
	public static function getCheckedInsertionsForOrderItem( $pOrdersItemsId = null ){
		intval($pOrdersItemsId);
        (int)$pOrdersItemsId; 
        //error_log(gettype($pOrdersItemsId));
        
		if ($pOrdersItemsId){
			$txtSql = " select "
				."	ins.id as 'insertions_id' "
				."	,iss.id as 'issues_id' "
				."	,oi.id as 'orders_items_id' "
				."	,iss.date_published "
				."  ,iss.status "
				."	,(case "
				."			when weekday(iss.date_published) = 0 then 'Pon' "
				."			when weekday(iss.date_published) = 1 then 'Uto'	"
				."			when weekday(iss.date_published) = 2 then 'Sri' "
				."			when weekday(iss.date_published) = 3 then 'Čet' "
				."			when weekday(iss.date_published) = 4 then 'Pet' "
				."			when weekday(iss.date_published) = 5 then 'Sub' "
				."			when weekday(iss.date_published) = 6 then 'Ned' "
				."			else '' end ) as day_of_week "
				."	 ,(case when ins.orders_items_id is null then false else true end ) as checked "
				." from orders_items oi "
				."	join n_issues iss on (oi.n_publications_id = iss.publications_id  ) "
				."	join n_insertions ins on (oi.id = ins.orders_items_id and iss.id = ins.issues_id) "
				." where "
				."		oi.id = ".$pOrdersItemsId;
				// //error_log($txtSql);
			return Nava::sqlToArray($txtSql);
		} else {
           // //error_log('Nava::getCheckedDiscountsForOrderItem 2');
			return null;
		}
	}

		
	public static function getProductsWithPricesForCategoryAndPublication( $pCategoriesId = null, $pPublicationsId = null ){
		
	
		$sqlPublications = ( $pPublicationsId > 0 ) ? "		and p.publication_id = ".$pPublicationsId : " ";
		$sqlCategories = ( $pCategoriesId > 0 ) ? "		and p1.categories_id = ".$pCategoriesId : " ";
		
		
			$txtSql = 
			" 	select "
			."		p.id, p.name "
			."		,p1.categories_id "
			."		,p1.products_id "
			."		,( case  when pcp.unit_price is null then p.unit_price else pcp.unit_price end ) as pcp_unit_price  "
			."	from  "
			."		( "
			."		select "
			."			p.id as products_id "
			."			,c.id as categories_id "
			."		from  "
			."			n_products p "
			."			,categories c "
			."		) p1 "
			."			left join n_products_categories_prices pcp on (p1.products_id = pcp.products_id and p1.categories_id = pcp.categories_id ) "
			."			join n_products p on (p1.products_id = p.id) "
			."	where 1=1 "
			.$sqlCategories
			.$sqlPublications;
			//error_log("getProductsWithPricesForCategoryAndPublication: $pCategoriesId , $pPublicationsId ");	
			//error_log($txtSql);
			$result = Nava::sqlToArray($txtSql);
			//error_log(print_r($result, true));
			return $result;
	}

	public static function categoriesWithPathsDict(){
//       provjerit je li ulaz Array, i ima li mu svaki element id. Ako ne, onda greška
		$categories = Nava::sqlToArray("select * from categories order by lft");
			$result = array();
				foreach ($categories as $value) { 
					$id = $value['id'];
					$result[$id] = $value; 
					$result[$id]['path'] = Categories::findFirst($id)->getPath();
				} 
		return $result;
    }
	
	public static function updateOrderTotal( $pOrdersId = null){

		$order = Orders::findFirst($pOrdersId);
		if (!$order) return null;
		//error_log("BEFORE BEFORE SAVE:".print_r($order->toArray(),true));
		$greska = $order->n_before_save();
		if ($greska) return $greska;
		//error_log("AFTER BEFORE SAVE:".print_r($order->toArray(),true));
		
		if ($order->update()!== true){
			$greske = $order->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
		} else $err_msg = null;
		//if ($err_msg) //error_log("Nava::updateOrderTotal 2, id=$pOrdersId  GREŠKA: $err_msg");
		//else //error_log("Nava::updateOrderTotal 3 SUCCESS");
		return true;
    }
	
	public static function updateIssuesStatuses (){
		
		//109 Automatski ažurirati status Issue-a: prima->deadline kad je istekao deadline
		Nava::runSql("update n_issues i join n_publications p on (i.publications_id = p.id and p.is_active = 1) set status = 'deadline' where status = 'prima' and CURDATE() > i.deadline");
			
	}
	
	public static function getJirZikFromNav( $pOrdersId = null ){
		
		//ako nema greške vraća null
		return null;
	}
	
	public static function activateFreeAds($pOrdersId){
		
		//46 - kada se Košarica prebacuje u Ponudu ili Besplatne, svi besplatni oglasi na njoj postaju aktivni (polje Active postaje TRUE)
		
		/*
		1. nać order iteme koji su na tom orderu i total im je 0, a na sebi imaju adove
		2. za svaki ad iz order itema stavit da je aktivan i dodat mu "product"
		*/
		//error_log("activateFreeAds:".$pOrdersId);
		if ( ! $pOrdersId  > 0 ) return null;
		//1.
		$order_items = Nava::sqlToArray("select * from orders_items where order_id = $pOrdersId and CAST(total AS DECIMAL(12,2)) = 0 and ad_id > 0 ");
		
		//2.
		//error_log(print_r($order_items,true));
		foreach ($order_items as $oi){
			Nava::runSql("update ads set online_product = 'active', active=1 where id = ".$oi['ad_id']);
		}
		
		
	}
	
	
	public static function createNewCart($pUserId = null, $pSalesRepId = null){
		
		// 132 - kad se kreira cart, sales rep je onaj koji je bio logiran kad je cart kreiran
		
		Nava::runSql("insert into orders (status, total, created_at, modified_at, n_frontend_sync_status ) values (1,0, now(), now() , 'no_sync')");
        $cart_id = Nava::sqlToArray("select max(id) as id  from orders ")[0]['id'];
		$lUserSql = $pUserId ? ", user_id = $pUserId " : "";
		$lSalesRepSql = $pSalesRepId ? ", n_sales_rep_id = $pSalesRepId " : "";
		
		Nava::runSql(
			" update orders set "
			." n_status = 1 "
			.", status = 1 "
			.$lUserSql
			.$lSalesRepSql
			." where id = $cart_id "
		);
		
		return Nava::sqlToArray("select * from orders where id = $cart_id ")[0];
	}
	
	
	//PERMISSIONS
	public static function getPermissionsArray($pUserId = null){
	/* SQL ZA ANALIZIRANJE permissiona po akcijama 
				select 
				user_id
				,action_name
				,role_name

				,rp_id
				,rp_name
				,(case
				when vrsta = 'name_filter-name_filter' and is_permission = 1 then 1
				when vrsta = 'name_filter-name_filter' and is_permission = 0 then -2
				when vrsta = 'name_filter-id' and is_permission = 1 then 4
				when vrsta = 'name_filter-id' and is_permission = 0 then -8
				
				when vrsta = 'id-name_filter' and is_permission = 1 then 16
				when vrsta = 'id-name_filter' and is_permission = 0 then -32
				when vrsta = 'id-id' and is_permission = 1 then 64
				when vrsta = 'id-id' and is_permission = 0 then -128
			
			end) points
			
			from (

				select
					u.id as user_id
					,r.name as role_name
					,a.name as action_name
					,rp.is_permission
					,'id-id' as vrsta
					,rp.id as rp_id
					,rp.name as rp_name
				from 
					users u 
						join roles_users ru on (u.id = ru.user_id)
						join roles r on (ru.role_id = r.id)
						join n_roles_permissions rp on (r.id = rp.role_id and rp.is_active = 1)
						join n_actions a on (rp.action_id = a.id and a.is_active = 1)
				where 1=1
					and u.id = 120096

				group by
					u.id
					,r.name
					,a.name
					,rp.is_permission
					,rp.id
					,rp.name
				
				union
				

				select
					u.id as user_id 
					,r.name as role_name
					,a.name as action_name
					,rp.is_permission
					,'id-name_filter' as vrsta
					,rp.id as rp_id
					,rp.name as rp_name
				from 
					users u 
						join roles_users ru on (u.id = ru.user_id)
						join roles r on (ru.role_id = r.id)
						join n_roles_permissions rp on (r.id = rp.role_id and rp.is_active = 1)
						join n_actions a on (a.name like rp.action_name_filter and a.is_active = 1)
				where 1=1 
					and u.id = 120096

				group by
					u.id
					,r.name
					,a.name
					,rp.is_permission
					,rp.id
					,rp.name
				
				union
				

				select
					u.id as user_id
					,r.name as role_name
					,a.name as action_name
					,rp.is_permission
					,'name_filter-id' as vrsta
					,rp.id as rp_id
					,rp.name as rp_name
				from 
					users u 
						join roles_users ru on (u.id = ru.user_id)
						join roles r on (ru.role_id = r.id)
						join n_roles_permissions rp on (r.name like rp.role_name_filter and rp.is_active = 1)
						join n_actions a on (rp.action_id = a.id and a.is_active = 1)
				where 1=1
					and u.id = 120096

				group by
					u.id
					,r.name
					,a.name
					,rp.is_permission
					,rp.id
					,rp.name
				
				union 
				

				select
					u.id as user_id
					,r.name as role_name
					,a.name as action_name
					,rp.is_permission
					,'name_filter-id' as vrsta
					,rp.id as rp_id
					,rp.name as rp_name
				from 
					users u 
						join roles_users ru on (u.id = ru.user_id)
						join roles r on (ru.role_id = r.id)
						join n_roles_permissions rp on (r.name like rp.role_name_filter and rp.is_active = 1)
						join n_actions a on (a.name like rp.action_name_filter and a.is_active = 1)
				where 1=1
				and u.id = 120096

				group by
					u.id
					,r.name
					,a.name
					,rp.is_permission
					,rp.id
					,rp.name
			
			) p1
			order by
				user_id
				,action_name
				,role_name
				,points desc
	
	*/
		
			if (!$pUserId) return array();
			$user = \Baseapp\Suva\Models\Users::findFirst($pUserId);
			if (!$user) return array();
			
			$txtSql = "
		select 
		user_id
		,action_name
		,max(points) as max_points
		from 
		(
			select 
				user_id
				,role_name
				,action_name
				,sum(case
				when vrsta = 'name_filter-name_filter' and is_permission = 1 then 1
				when vrsta = 'name_filter-name_filter' and is_permission = 0 then -2
				when vrsta = 'name_filter-id' and is_permission = 1 then 4
				when vrsta = 'name_filter-id' and is_permission = 0 then -8
				
				when vrsta = 'id-name_filter' and is_permission = 1 then 16
				when vrsta = 'id-name_filter' and is_permission = 0 then -32
				when vrsta = 'id-id' and is_permission = 1 then 64
				when vrsta = 'id-id' and is_permission = 0 then -128
			
			end) points
			
			from (
				/* id-id */
				select
					u.id as user_id
					,r.name as role_name
					,a.name as action_name
					,rp.is_permission
					,'id-id' as vrsta
				from 
					users u 
						join roles_users ru on (u.id = ru.user_id)
						join roles r on (ru.role_id = r.id)
						join n_roles_permissions rp on (r.id = rp.role_id and rp.is_active = 1)
						join n_actions a on (rp.action_id = a.id and a.is_active = 1)
				where 1=1
					and u.id = $pUserId
				/*and a.name = 'actOrderDelete'*/
				group by
					u.id
					,r.name
					,a.name
					,rp.is_permission
				
				union
				
				/* id-name_filter	*/
				select
					u.id as user_id 
					,r.name as role_name
					,a.name as action_name
					,rp.is_permission
					,'id-name_filter' as vrsta
				from 
					users u 
						join roles_users ru on (u.id = ru.user_id)
						join roles r on (ru.role_id = r.id)
						join n_roles_permissions rp on (r.id = rp.role_id and rp.is_active = 1)
						join n_actions a on (a.name like rp.action_name_filter and a.is_active = 1)
				where 1=1 
					and u.id = $pUserId
				/*and a.name = 'actOrderDelete'*/
				group by
					u.id
					,r.name
					,a.name
					,rp.is_permission
				
				union
				
				/* name_filter-id */
				select
					u.id as user_id
					,r.name as role_name
					,a.name as action_name
					,rp.is_permission
					,'name_filter-id' as vrsta
				from 
					users u 
						join roles_users ru on (u.id = ru.user_id)
						join roles r on (ru.role_id = r.id)
						join n_roles_permissions rp on (r.name like rp.role_name_filter and rp.is_active = 1)
						join n_actions a on (rp.action_id = a.id and a.is_active = 1)
				where 1=1
					and u.id = $pUserId
				/*and a.name = 'actOrderDelete'*/
				group by
					u.id
					,r.name
					,a.name
					,rp.is_permission
				
				union 
				
				/* name_filter-name_filter */
				select
					u.id as user_id
					,r.name as role_name
					,a.name as action_name
					,rp.is_permission
					,'name_filter-id' as vrsta
				from 
					users u 
						join roles_users ru on (u.id = ru.user_id)
						join roles r on (ru.role_id = r.id)
						join n_roles_permissions rp on (r.name like rp.role_name_filter and rp.is_active = 1)
						join n_actions a on (a.name like rp.action_name_filter and a.is_active = 1)
				where 1=1
				and u.id = $pUserId
				/*and a.name = 'actOrderDelete'*/
				group by
					u.id
					,r.name
					,a.name
					,rp.is_permission
			
			) p1
			group by 
				user_id
				,role_name
				,action_name
		) p2
		
		group by
			user_id
			,action_name
			";
		$rs = Nava::sqlToArray($txtSql);
		//error_log(print_r($rs, true));
		$result = array();
		foreach ($rs as $row)
			$result[$row['action_name']] = $row['max_points'] > 0 ? true : false;

		//error_log(print_r($result, true));
		return $result;
	}
	
	
	//FRONTEND
	public static function processFrontend($pOrdersId){
				/*
		1. prominit status u 3(insertions se nesmiju brisati pa zato moraju odma ic u offer tj ponudu)
		2. u order prebacit n_product_id sa n_productsa
		3. isto napravit sa publikacijama, pribacit sa n_publicationsa
		4. ubacit category_id preko ad-a
		5. ubacit quantity: ako je online onda je quantity 1, a iz proizvoda se kuži koliko dana objave ima. Ako je offline onda je quantity broj objava
		6. ako je offline onda ubacit onoloko insertiona koliko ima objava (od današnjeg datuma naprijed)
		7. ako je offline onda su datumi (first published i expires at) prvi i zadnji datum objave 
		8. ako je online onda promijanit datume first published i expires at, kopirati sa ad-a
		
		ubaciti ono insertions koliko ima objava offline izdanje
		preko frontenda se ne mogu odabirat discounti
		
		*/
		
		$debug = null;
		
		
		if ($debug) $this->flashSession->success("<strong>.$pOrdersId.</strong>");

	//	1. prominit status u 3(insertions se nesmiju brisati pa zato moraju odma ic u offer tj ponudu)
 		Nava::runSql("update orders set n_status = '3' where id = $pOrdersId");
		
		$order = Orders::findFirst($pOrdersId);
		if (!$order) return "Nije pronađen order br. $pOrdersId";
		
		$order_items = Nava::sqlToArray("select * from orders_items where order_id = $pOrdersId"); //svi order itemi iz našeg ordera
		foreach($order_items as $order_item){
			$title = $order_item['title'];
			//nać broj objava iz teksta
			//skužit jel se radi o online ili o offline
			if ($debug) $this->poruka($title);
			
			
			$kolicina = 0;
			if (strpos($title, 'Online') !== false) {
				$is_online = 1;
				if ($debug) $this->poruka("online je");
				$pos = strpos($title, ' dan');
				if ($pos > 0){
					if ($debug) $this->poruka($pos);
					$kolicina = (int)substr($title,$pos-2,2);
				}
			}elseif(strpos($title, 'Offline') !== false){
				$is_online = 0;
				if ($debug) $this->poruka("ofline je");
				$pos = strpos($title, ' objav');
				if ($pos > 0){
					if ($debug) $this->poruka($pos);
					$kolicina = (int)substr($title,$pos-2,2);
				}
			} else return "Nije prepoznato je li proizvod online ili offline na order itemu ".$order_item['id'];

			if ($kolicina < 1) return "nije prepoznat broj dana na order itemu ".$order_item['id'];
			
// 		5. ubacit quantity: ako je online onda je quantity 1, a iz proizvoda se kuži koliko dana objave ima. Ako je offline onda je quantity broj objava
			$quantity = null;
			if($is_online == 1)	{
				$quantity = 1;
				Nava::runSql("update orders_items set qty = $quantity where id = ".$order_item['id']);
			}elseif($is_online == 0){
				//quantity
				$quantity = $kolicina;				
				Nava::runSql("update orders_items set qty = ".$quantity." where id = ".$order_item['id']);
			}
			
			//4. category_id
			$ad = Ads::findFirst($order_item['ad_id']);
			if (!$ad) return $this->greska("Nije pronađen AD na order itemu ".$order_item['id']);
			
			$category = Categories::findFirst($ad->category_id);
			if (!$category) return "Nije pronađena kategorija za AD na order itemu ".$order_item['id'];
			
			Nava::runSql("update orders_items set n_category_id = ".$ad->category_id." where id = ".$order_item['id']);

// 			2. u order prebacit n_product_id sa n_productsa
			if ($is_online == 0) {
				$txtSql = "select * from n_products where old_product_id collate utf8_croatian_ci ='".$order_item['product_id']."'";
			} else {
				$txtSql = "select * from n_products where days_published = $kolicina and old_product_id collate utf8_croatian_ci ='".$order_item['product_id']."'";
			}
 			if ($debug) $this->poruka($txtSql);
			$products = Nava::sqlToArray($txtSql);
 			if ($debug) $this->printr($products);
			if ($products){
				$product = $products[0];  //prvi s reda
				Nava::runSql("update orders_items set n_products_id = ".$product['id']." where id = ".$order_item['id']);
				
				//3. izmijeniti publication_id
				if ($product['publication_id'] > 0) Nava::runSql("update orders_items set n_publications_id = ".$product['publication_id']." where id = ".$order_item['id']);
				else return ("Na proizvodu ".$product['id']." nije navedena publikacija na koju se odnosi.");
				
			}else{
				return ("Nije pronađen proizvod sa OldProduct ID = ".$order_item['product_id']." i Days Published = $kolicina");
			}
// 		6. ako je offline onda ubacit onoliko insertiona koliko ima objava (od današnjeg datuma naprijed) i promijenit datume (first published i expires at)	
			if ($is_online == 0){
				//pronaći issuses na kojma ćemo unosit insertione
				
				$txtSql = "select * from n_issues where status='prima' and publications_id = ".$product['publication_id']." and deadline > '".$order->created_at."' order by date_published";
				if ($debug) $this->poruka($txtSql);
				$issues = Nava::sqlToArray($txtSql);
				//$this->printr($issues);
				
				//provjerit je li postoji dovoljno issua 
				
				if (count($issues) < $kolicina) return "Nema dovoljno izdanja za popunjavanje (potrebno ih je $kolicina, a ima ih ".count($issues).")";
				//ubacit u tablicu Insertions
				$brojac = 0;
				foreach($issues as $issue){
					$brojac +=1;
					if ($brojac <= $kolicina) {
						$txtSql = "insert into n_insertions (product_id, orders_items_id, issues_id, is_active) select ".$product['id'].",".$order_item['id'].",".$issue['id'].",1";
						//$this->poruka($txtSql);
						Nava::runSql($txtSql);
					}
				}
			}
			
// 			7. ako je offline onda su datumi (first published i expires at) prvi i zadnji datum objave 
			if ($is_online == 0){
				//pronaći issuses na kojma ćemo unosit insertione
				
				$txtSql = "select min(deadline) as prvi, max(deadline) as zadnji from n_issues where status='prima' and publications_id = ".$product['publication_id']." and deadline > '".$order->created_at."'";
				if ($debug) $this->poruka($txtSql);
				$datumi = Nava::sqlToArray($txtSql)[0];
				if ($debug) $this->printr($datumi);
				$txtSql = "update orders_items set n_first_published_at = UNIX_TIMESTAMP('".$datumi['prvi']."'), n_expires_at = UNIX_TIMESTAMP('".$datumi['zadnji']."'),  where id = ".$order_item['id'];
				if ($debug) $this->poruka($txtSql);
				Nava::runSql($txtSql);
			}
			
// 		8. ako je online onda promijanit datume first published i expires at, kopirati sa ad-a
			if ($is_online == 1){
				if ($debug) $this->printr($ad->toArray());
				$txtSql = "update orders_items set n_first_published_at = ".$ad->created_at.", n_expires_at = ".$ad->expires_at." where id = ".$order_item['id'];
				if ($debug) $this->poruka($txtSql);
				Nava::runSql($txtSql);
			}
		}
				
         return null;

		
	}
	
	//PORUKA, GREŠKA, PRINTR
		
	
	public static function sysMessage ( $pTxtPoruka = null, $pTxtNaslov = null, $pReturnValue = null){
		
	
		if( self::$debugLogSysMessage)
				error_log("NAVA sysMessage: ".$pTxtNaslov." ".$pTxtPoruka);
	
		if ( self::$debugPrintOnClientSysMessage &&  isset($_SESSION) && array_key_exists('_context',$_SESSION) && array_key_exists('logged_in_user_id',$_SESSION['_context'] ) ) {
			$isAllowed = \Baseapp\Suva\Models\Model::isUserAllowed('actShowSysMessage', $_SESSION['_context']['logged_in_user_id']);
		}else{
			return $pReturnValue;
		}
		
		
		$porukaType = gettype($pTxtPoruka);
		
		if ($isAllowed){
			$txtToSend = '';
			if ($pTxtPoruka && $porukaType != 'array'){
				$txtToSend = $pTxtPoruka;
			}
			elseif($pTxtPoruka && $porukaType == 'array')
			{
				$naslov = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/>" : "";
				$txtToSend = "$naslov<br/><pre>".print_r($pTxtPoruka,true)."</pre>";
			}

			array_push($_SESSION['_context']['_msg_flashsession'], array('system' => $txtToSend) );
			
			if ( array_key_exists ('_is_threaded_execution', $_SESSION['_context'] ) ){
				//self::saveMessagesToFileThreaded ( array('info' => $txtToSend) );
				self::saveMessagesToParentSessionThreaded ( array('info' => $txtToSend) );
			}
			
			//da se može koristit kao return Nava::sysMessage("greška", null);
			
			
		}
		return $pReturnValue;

	}

	public static function sysError( $pTxtGreska = null , $pTxtNaslov = null , $pReturnValue = null){
		
		
		//naslov se može dodati, ali poruka se ne ispisuje ako nema $pTxtGreska (koristi se za slanje poruke ako je greska u update)
        if ($pTxtGreska){

			if( self::$debugLogSysError)
				error_log("NAVA SYSERROR: ".$pTxtNaslov." ".$pTxtGreska);
			
			
			if(self::$debugPrintOnClientSysError){
				$poruka = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/><pre>".$pTxtGreska."</pre>" : $pTxtGreska ;
				array_push($_SESSION['_context']['_msg_flashsession'], array('error' => $poruka) );

				if ( array_key_exists ('_is_threaded_execution', $_SESSION['_context'] ) ){
					self::saveMessagesToParentSessionThreaded ( array('error' => $poruka) );
				}
			}
        }
		//da se može koristit kao return Nava::greska("greška", null);
		return $pReturnValue;
    }
	
	public static function printr( $pTxtPoruka = null , $pTxtNaslov = null){
        if ($pTxtPoruka){
			$naslov = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/>" : "";
			array_push($_SESSION['_context']['_msg_flashsession'], array('system' => "$naslov<br/><pre>".print_r($pTxtPoruka,true)."</pre>") );
        }
	}
	
	public static function poruka ( $pTxtPoruka = null ){
		// error_log("$pTxtPoruka ");
		if ($pTxtPoruka){
			array_push($_SESSION['_context']['_msg_flashsession'], array('info' => $pTxtPoruka) );
			
			if ( array_key_exists ('_is_threaded_execution', $_SESSION['_context'] ) ){
				//self::saveMessagesToFileThreaded ( array('info' => $pTxtPoruka) );
				self::saveMessagesToParentSessionThreaded ( array('info' => $pTxtPoruka) );
			}
        }
	}
	
	public static function statusMessage ( $pTxtPoruka = null ){
		if ($pTxtPoruka){
			$_SESSION['_context']['_msg_status'] = $pTxtPoruka;
			
			if ( array_key_exists ('_is_threaded_execution', $_SESSION['_context'] ) ){
				//self::saveMessagesToFileThreaded ( array('info' => $pTxtPoruka) );
				self::saveMessagesToParentSessionThreaded ( array('status' => $pTxtPoruka ) );
			}
        }
	}
	
	public static function poruka2 ( $pTxtPoruka = null, $p1=null, $p2=null, $p3=null, $p4=null, $p5=null ){
        if ($pTxtPoruka){
			
			$x = new Model();
			$pTxtPoruka = $x->t($pTxtPoruka,$p1,$p2,$p3,$p4,$p5);
			array_push($_SESSION['_context']['_msg_flashsession'], array('info' => $pTxtPoruka) );
        }
	}
	
    public static function greska( $pTxtGreska = null , $pTxtNaslov = null , $pReturnValue = null){
		//naslov se može dodati, ali poruka se ne ispisuje ako nema $pTxtGreska (koristi se za slanje poruke ako je greska u update)
        if ($pTxtGreska){
			$poruka = $pTxtNaslov ? "<b>$pTxtNaslov</b><br/><pre>".$pTxtGreska."</pre>" : $pTxtGreska ;
			array_push($_SESSION['_context']['_msg_flashsession'], array('error' => $poruka) );

			if ( array_key_exists ('_is_threaded_execution', $_SESSION['_context'] ) ){
				//self::saveMessagesToFileThreaded ( array('info' => $pTxtGreska) );
				self::saveMessagesToParentSessionThreaded ( array('error' => $poruka) );
			}

        }
		//da se može koristit kao return Nava::greska("greška", null);
		return $pReturnValue;
    }
		
	public static function saveMessageToFile ($pTxtMessage = null, $pType = null){
		
		$sid = session_id();
		$fileUri = "_messagesToClient/$sid.json";
		$file = fopen ( $fileUri, 'w' );
		
		$timeMicroS = microtime(true);
		//$message = array ( $timeMicroS => $pTxtMessage );
		$message = array( 'info' => $pTxtMessage  );
	
		
		//error_log(print_r($message, true));
		$jsonMessage = json_encode($message);
		fwrite ($file, $jsonMessage);
		//error_log ($jsonMessage);
	}
	
	public static function getAdIdsByIssueAndCategory( $pIssueId = null, $pCategoryId = null ){
		$catIdList = $pCategoryId;
		$pubCats = Nava::sqlToArray("select pc.category_id from n_publications_categories pc join n_issues i on (pc.publication_id = i.publications_id) where i.id =  $pIssueId and pc.forward_category_id = $pCategoryId");
		if ($pubCats)
			foreach ( $pubCats as $pubCat )
				$catIdList .= ", ".$pubCat['category_id'];
		
		
		//ad-ovi pod tom kategorijom i tim oglasom
		$adIds = Nava::sqlToArray("
			select a.id as id
			from ads a
				join orders_items oi on oi.ad_id = a.id 
					join n_insertions i on oi.id = i.orders_items_id and i.issues_id = $pIssueId 
			where	
				a.category_id in ( $catIdList )
			order by
				a.description
		");
		
		return $adIds;
		
	}
		

	//spremanje poruka u file ako se radi o threaded izvršavanju
	public static function saveMessagesToParentSessionThreaded ( $pArrMsg ){
		//dodavanje poruka u session od parenta
		$timeMicroS = microtime( true );
		$timeKey = (string)$timeMicroS;
		$sidParent = $_SESSION['_context']['_SID'];
		$sidCurrent = session_id();
		
		session_write_close(); // this closes and saves the data in session 1

		session_id($sidParent); // identify that you want to go into the other session - this *must* come before the session_start
		session_start(); // this will start your second session
 
		if (! array_key_exists ( '_msg_threaded', $_SESSION['_context'] ) ) {
			$_SESSION['_context']['_msg_threaded'] = array();
		}
		//error_log("getContextAction _msgThreaded:".print_r ( $_SESSION['_context']['_msg_threaded'], true ));
		
		//ne prema timestampu nego prema ID-ju
		// if (! array_key_exists ( $timeKey, $_SESSION['_context']['_msg_threaded'] ) ) {
			// $_SESSION['_context']['_msg_threaded'][ $timeKey ] = $pArrMsg;
		// }

		//ako je status poruka onda ide u jednu varijablu, ne ide u array
		if ( array_key_exists ('status', $pArrMsg) ){
			$_SESSION['_context']['_msg_status'] = $pArrMsg['status'];
		}
		else {
		
		
			if (! array_key_exists ( '_msg_threaded_last_id', $_SESSION['_context'] ) ) {
				$_SESSION['_context']['_msg_threaded_last_id'] = 0;
			}
			$lastId = $_SESSION['_context']['_msg_threaded_last_id'];
			$lastId++;
			$_SESSION['_context']['_msg_threaded'][ $lastId ] = $pArrMsg;
			$_SESSION['_context']['_msg_threaded_last_id'] = $lastId;
		}
			
		
		session_write_close(); // this closes and saves the data in session 1
		session_id($sidCurrent); // identify that you want to go into the other session - this *must* come before 
		session_start(); // this will start your session
	} 
	public static function unsetThreadedExecutionInParentSession(){
		$sidParent = $_SESSION['_context']['_SID'];
		$sidCurrent = session_id();

		session_write_close(); // this closes and saves the data in session 1

		session_id($sidParent); // identify that you want to go into the other session - this *must* come before the session_start
		session_start(); // this will start your second session
 
		if ( array_key_exists ( '_is_threaded_execution', $_SESSION['_context'] ) ) {
			unset($_SESSION['_context']['_is_threaded_execution']);
		}

		session_write_close(); // this closes and saves the data in session 1

		session_id($sidCurrent); // identify that you want to go into the other session - this *must* come before 
		session_start(); // this will start your session
	}
	

	public static function saveMessagesToFileThreadedOLD ( $pArrMsg ){
		//snimati zadnjih 10 sekundi poruka u file, kojeg će klijent čitati
		$timeToKeepMessagesInFile = 5;
		$timeMicroS = microtime( true );
		$timeKey = (string)$timeMicroS;
		$sid = $_SESSION['_context']['_SID'];
		
		//najprije poruku staviti u array.
		if ( ! array_key_exists ('_msg_filestorage', $_SESSION['_context'] ) ){
			$_SESSION['_context']['_msg_filestorage'] = array();
		}
		//poruke je key-value gdje je key timestamp u mikrosekundama
		if (! array_key_exists ( $timeKey, $_SESSION['_context']['_msg_filestorage'] ) ) {
			$_SESSION['_context']['_msg_filestorage'][ $timeKey ] = $pArrMsg;
		}
		//sada se brišu sve poruke koje su starije od 10 sekundi
		foreach ( $_SESSION['_context']['_msg_filestorage'] as $time=>$message ){
			if ( (int) $time < time() - $timeToKeepMessagesInFile ){
				unset ($_SESSION['_context']['_msg_filestorage'][ $time ]);
			}
		}
		
		//Preostale poruke se spremaju u file, koji ima ime prema session ID-ju.
		//Ako se radi o Threaded akciji, onda je session ID broj od sessiona koji je pozvao tu akciju
		// ( šalje se kao parametar u poziv akcije )
		$fileUri = "_messagesToClient/$sid.json";
		$txtJson = json_encode( $_SESSION['_context']['_msg_filestorage'] );
		file_put_contents ( $fileUri, $txtJson );
		//error_log ( $txtJson );
	}
	
	//copy file
	
	public static function r_deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        return ("$dirPath must be a directory");
		
		//throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            self::r_deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}
	
	public static function file_copy($fromPath, $toPath, $image_name){	
		// je li pronađen file iz fromPatha?
		//je li postoji To Path?
		//je li već postoji u ToPathu file koji se želi tamo kopirat?
		
		if ( !file_exists($fromPath) ) return "File $fromPath does not exist";
		if ( !file_exists($toPath) ) return "Folder $toPath does not exist";
		
		
		//$targetPath2 = "repository/dtp/to_process/"  //ovako neka izgleda path
		$toFullPath = $toPath.$image_name;
		
		if ( file_exists($toFullPath) ) return "Destination file $toFullPath already exists.";
		
		//error_log ("file_copy  1 fromPath: $fromPath,  toFullPath: $toFullPath");
		
		if(copy($fromPath, $toFullPath)){
			return null;
		 }else{
			return "File $fromPath not copied to $toFullPath due to unknown reason.";
		}
	}	
	 
	public static function folder_create($directoryName){
		
		//ako folder već postoji neka ga izbriše sa svim slikama
		
		if(! is_dir($directoryName) ){
			//error_log('NavaSuva::folder_create 1');
			if (mkdir($directoryName, 0755)) return null;
			else return "Could not create $directoryName";
		} 
		else{
			//error_log('NavaSuva::folder_create 2');
			rmdir($directoryName);
			if (mkdir($directoryName, 0755)) return null;
		}
			
	
	}
	
	
	public static function actOrderStatusPlaceno( $pOrdersId = null, $pPaymentDate = null ) {

        /* 
        time() - trenutno vrijeme u unix timestampu
        
        
        */
        //error_log('Nava::actOrderStatusPlaceno 1');
//         $this->printr($_REQUEST);
		if (!$pOrdersId > 0 ){
			//error_log('Nava::actOrderStatusPlaceno 2');
			array_push($_SESSION['_context']['_messages_system'],"Parameter Order ID not sent  - $pOrdersId");
			return;
			// return ("Parameter Order ID not sent  - $pOrdersId");
		} 
		$order = \Baseapp\Suva\Models\Orders::findFirst($pOrdersId);
		//error_log('Nava::actOrderStatusPlaceno 3');
		if (!$order ){
			//error_log('Nava::actOrderStatusPlaceno 4');
			array_push($_SESSION['_context']['_messages_system'],"Order No.  $pOrdersId not found.");
			return true;
			// return ("Order No.  $pOrdersId not found.");
		} 
		
		if (!$pPaymentDate) $pPaymentDate = date("Y-m-d" , time());
		if ($order->n_payment_date) $pPaymentDate = $order->n_payment_date;
		//error_log('Nava::actOrderStatusPlaceno 5');
		if (!$order->n_payment_methods_id){
			//error_log('Nava::actOrderStatusPlaceno 6');
			array_push($_SESSION['_context']['_messages_system'],"Payment method not supplied.");
			return true;
			// return("Payment method not supplied.");
		} 
		
		
// 		385 - sat - time stamp kad će oglas biti objavljen. sada nije dobro. 
// 		treba napraviti da se sat (ako je u postavkama kod plaćanja 00.00) automatski postavi s trenutkom plaćanja.
		
		$order_item = \Baseapp\Suva\Models\OrdersItems::findFirst("order_id = $pOrdersId");
		//error_log('Nava::actOrderStatusPlaceno 7');

		if($order_item && $order_item->n_products_id > 0){
// 			$sqlTxt = "update ads set active = 0 where id = ".$order_item->ad_id;
// 			//error_log('txtsql je '.$sqlTxt);
// 			Nava::runSql($sqlTxt);			
			$product = \Baseapp\Suva\Models\Products::findFirst($order_item->n_products_id);
// 			//error_log(1);
			//error_log('Nava::actOrderStatusPlaceno 8');
			if($product && $product->is_online_product == 1){
				if($order_item->n_first_published_at > 0){
					
					$order_item_hours = explode(" ", $order_item->n_first_published_at);
					if($order_item_hours[1] == '00:00:00'){
						$order_item_hours[1] = date("H:i:s");
// 						//error_log(2);
						$order_item_date = implode(" ",$order_item_hours);
// 						//error_log(3);
// 						$order_item->n_first_published_at = $order_item_date;
// 						//error_log(4);
// 						//error_log($order_item_date);
						Nava::runSql("update orders_items set n_first_published_at = '$order_item_date' where order_id = $pOrdersId");
						
						//error_log('Nava::actOrderStatusPlaceno 9');
						// //error_log(5);
					}
					
				}
			}
		}
		
		
		
			
			
		
         
        //115 - RUČNO PLAĆANJE tako da se na popisu preliminary invoice odabere "EDIT", 
		//      i da se odabere vrsta plaćanja, i eventualno izmijenen ostali podaci. Na Editor Order Headera staviti dugme Potvrdi plaćanje
        //prvovjerit jeli u orderu postoji payment_date i payment_method ako ne postoji javit gresku (flash-session-error)
		
		
        //143 - kod prelaska u status plaćeno vrši se slijedeća provjera: ako je bilo koji od datuma objave plaćenih oglasa raniji od datuma plaćanja, order ide u status Late Paid.
        //unix timestamp
        $lPrvi = Nava::sqlToArray("select min(n_first_published_at) as prvi from orders_items where total > 0 and order_id = $pOrdersId")[0]['prvi'];
        if (!$lPrvi){
			array_push($_SESSION['_context']['_messages_system'],"Could not find first publication date on order No. $pOrdersId");
			return true;
			// return ("Could not find first publication date on order No. $pOrdersId");
		} 

        
        if (strtotime($pPaymentDate) > strtotime($lPrvi)) {
            //ako ima zakašnjelih ide u late payment
			// $order->n_status = 9;
			 //$order->update();
			
			//array_push($_SESSION['_context']['_messages_system'],"Status of Order No. $pOrdersId has been changed to 'Late Paid' because there was a paid product that was published ".$order->n_dateTimeUs2DateTimeHr($lPrvi).", before payment date (".$order->n_dateUs2DateHr($pPaymentDate).")");
			array_push($_SESSION['_context']['_messages_system'],"Status of Order No. $pOrdersId cannot be paid.");
			return ;
			// return ("Status of Order No. $pOrdersId has been changed to 'Late Paid' because there was a paid product that was published "
				// .$order->n_dateTimeUs2DateTimeHr($lPrvi).", before payment date (".$order->n_dateUs2DateHr($pPaymentDate).")");
        
		} else {
			$order->n_status = 5;
			$order->status = 2;
			$order->n_payment_date = $pPaymentDate;
			$order->update();
			
	        //56 - Kad fakturi promijeniš status u "plaćeno", svi ad-ovi koji su na njoj a dodijeljen im je plaćeni proizvod postaju aktivni
			$lPaidAds = Nava::sqlToArray("select ad_id from orders_items where order_id = $pOrdersId and total > 0 and ad_id > 0 group by ad_id");
			foreach($lPaidAds as $ad) {
// 				Nava::activateAd( $ad['ad_id'] );
				Nava::runSql("update ads set online_product_id = 1 where id =".$ad['ad_id']); 
				Nava::runSql("update ads set online_product = 'O:47:' where id =".$ad['ad_id']); 
			}

			// $this->poruka("Status of Order No. $pOrdersId has been changed to 'Paid'.  "
				// ." First publication date is ".$order->n_dateTimeUs2DateTimeHr($lPrvi)
				// .". Payment date is ".$order->n_dateUs2DateHr($pPaymentDate));
			// return $this->redirect_back();
		}
            
		return false; // nema greske
    } 


}
