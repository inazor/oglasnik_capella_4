<?php
namespace Baseapp\Suva\Library;

 

class libAdsSearchWords {
	
	public static function updateAdsSearchWordsForPublication( $pPublicationId = null ){
		//zbog pucanja na ads2
		return;
		if( $pPublicationId > 0)	$publication = \Baseapp\Suva\Models\Publications::findFirst($pPublicationId);
		else return;
		if (!$publication) return;
		
		$txtSql = "	DELETE from n_ads_search_words WHERE publication_id = $pPublicationId ";
		//error_log("PORUKA".$txtSql );
		\Baseapp\Suva\Library\Nava::runSql($txtSql);
		
		$txtSql = " select ad_id as id from orders_items where n_publications_id = $pPublicationId group by ad_id ";
		$rsAds = \Baseapp\Suva\Library\Nava::sqlToArray($txtSql);
		foreach ($rsAds as $row){
			$adId = $row['id'];
			array_push($_SESSION['_context']['_messages_info'],"Updating ASW for ad $adId");
			libAdsSearchWords::updateAdsSearchWordsForAdAndPublication ( $adId, $pPublicationId );
		}
	}
	
	public static function updateAdsSearchWordsForAd ($pAdId = null){
		//zbog pucanja na ads2
		return;		
		if( $pAdId > 0)	$ad = \Baseapp\Suva\Models\Ads::findFirst($pAdId);
		else return;
		if (!$ad) return;
		
		
		$txtSql = "	DELETE from n_ads_search_words WHERE ad_id = $pAdId ";
		//error_log("PORUKA".$txtSql );
		\Baseapp\Suva\Library\Nava::runSql($txtSql);
		
		//za svaku publikaciju
		$pubs = \Baseapp\Suva\Models\Publications::find(" is_online = 1 and is_active = 1 ");
		foreach ( $pubs as $pub  ){
			//za svaku mapiranu kategoriju od ovog ad-a
			$category = $ad->Category();
			
			$pcms = $category->PublicationsCategoriesMappings(" publication_id = $pub->id ");
			//error_log("Ads:n_after_save ASW 1.1".count($pcms));
			foreach ($pcms as $pcm) {
				//za svako mapiranje kategorije po publikaciji
				
				//error_log("Ads:n_after_save ASW 1");
				/*prije kreiranja asw provjerit je li taj ad pripada tom pcm-u */
				if ( $pcm->mapped_category_id && !$pcm->mapped_dictionary_id && !$pcm->mapped_location_id )
					\Baseapp\Suva\Library\libAdsSearchWords::makeAdSearchWord( $ad->id, $pcm->id );
				elseif ( $pcm->mapped_category_id && $pcm->mapped_dictionary_id && !$pcm->mapped_location_id ){
					/* potražit u ads_parameters postoji li negdi taj dictionary_id 
						ako postoji onda kreirat, inače ne*/
					$adsParameters = \Baseapp\Suva\Models\AdsParameters::find( "ad_id = $ad->id" );
					foreach ( $adsParameters as $ap ){
						$ynDict = $ap->Parameter()->ParameterType()->accept_dictionary;
						if ( $ynDict ){
							if ( $ap->value == $pcm->mapped_dictionary_id ){
								//ako vrijednost u AdsParemeter označava ID od dictionary-ja i ako je 
								// ta vrijednost jednaka onoj iz PublicationsCategoriesMappings-a onda napravi redak u ASW
								\Baseapp\Suva\Library\libAdsSearchWords::makeAdSearchWord( $ad->id, $pcm->id );
							}
						}
					}
				}
				elseif ( $pcm->mapped_category_id && !$pcm->mapped_dictionary_id && $pcm->mapped_location_id ){
					if (
						$ad->country_id == $pcm->mapped_location_id
						|| $ad->county_id == $pcm->mapped_location_id
						|| $ad->municipality_id == $pcm->mapped_location_id
						|| $ad->city_id == $pcm->mapped_location_id
					)
						\Baseapp\Suva\Library\libAdsSearchWords::makeAdSearchWord( $ad->id, $pcm->id );
				}
			}
			
			//dodavanje drugih polja na asw, ako su kreirani
			\Baseapp\Suva\Library\libAdsSearchWords::updateAdsSearchWordsStatus( $ad->id, $pub->id );
		}	
	}

	public static function updateAdsSearchWordsForAdAndPublication ($pAdId = null, $pPublicationId = null){
		//zbog pucanja na ads2
		return;
		
		if( $pAdId > 0)	$ad = \Baseapp\Suva\Models\Ads::findFirst($pAdId);
		else return;
		if (!$ad) return;

		if( $pPublicationId > 0)	$pub = \Baseapp\Suva\Models\Publications::findFirst($pPublicationId);
		else return;
		if (!$pub) return;

		
		
		$txtSql = "	DELETE from n_ads_search_words WHERE ad_id = $pAdId and publication_id = $pPublicationId ";
		//error_log("PORUKA".$txtSql );
		\Baseapp\Suva\Library\Nava::runSql($txtSql);
		
		//za svaku publikaciju
			//za svaku mapiranu kategoriju od ovog ad-a
			$category = $ad->Category();
			
			$pcms = $category->PublicationsCategoriesMappings(" publication_id = $pub->id ");
			//error_log("Ads:n_after_save ASW 1.1".count($pcms));
			foreach ($pcms as $pcm) {
				//za svako mapiranje kategorije po publikaciji
				
				//error_log("Ads:n_after_save ASW 1");
				/*prije kreiranja asw provjerit je li taj ad pripada tom pcm-u */
				if ( $pcm->mapped_category_id && !$pcm->mapped_dictionary_id && !$pcm->mapped_location_id )
					\Baseapp\Suva\Library\libAdsSearchWords::makeAdSearchWord( $ad->id, $pcm->id );
				elseif ( $pcm->mapped_category_id && $pcm->mapped_dictionary_id && !$pcm->mapped_location_id ){
					/* potražit u ads_parameters postoji li negdi taj dictionary_id 
						ako postoji onda kreirat, inače ne*/
					$adsParameters = \Baseapp\Suva\Models\AdsParameters::find( "ad_id = $ad->id" );
					foreach ( $adsParameters as $ap ){
						$ynDict = $ap->Parameter()->ParameterType()->accept_dictionary;
						if ( $ynDict ){
							if ( $ap->value == $pcm->mapped_dictionary_id ){
								//ako vrijednost u AdsParemeter označava ID od dictionary-ja i ako je 
								// ta vrijednost jednaka onoj iz PublicationsCategoriesMappings-a onda napravi redak u ASW
								\Baseapp\Suva\Library\libAdsSearchWords::makeAdSearchWord( $ad->id, $pcm->id );
							}
						}
					}
				}
				elseif ( $pcm->mapped_category_id && !$pcm->mapped_dictionary_id && $pcm->mapped_location_id ){
					if (
						$ad->country_id == $pcm->mapped_location_id
						|| $ad->county_id == $pcm->mapped_location_id
						|| $ad->municipality_id == $pcm->mapped_location_id
						|| $ad->city_id == $pcm->mapped_location_id
					)
						\Baseapp\Suva\Library\libAdsSearchWords::makeAdSearchWord( $ad->id, $pcm->id );
				}
			}
			
			//dodavanje drugih polja na asw, ako su kreirani
			\Baseapp\Suva\Library\libAdsSearchWords::updateAdsSearchWordsStatus( $ad->id, $pub->id );
	
	}



	
	public static function makeAdSearchWord( $pAd_id = null, $pPcmId = null ) {
		
		
		//zbog pucanja na ads2
		return;
		if($pAd_id > 0){
			$ad = \Baseapp\Suva\Models\Ads::findFirst($pAd_id);
		}
		else
			return;
		
		if($pPcmId > 0) {
			$pcm = \Baseapp\Suva\Models\PublicationsCategoriesMappings::findFirst($pPcmId);
			$cat = $pcm->CategoryMapping();
		}
		else return;
			
		
		
		//Utils::remove_accents($merged_search_terms)
		//migrationRawDb : importAd()
		//NIKOLA: sređivanje ads search words
		
		//error_log("libAdsSearchWords.php 1");
		$par1 = null;
		$par2 = null;
		$par3 = null;
		$par4 = null;
		$par5 = null;
		
		if ($cat->parent_id != null) 
			$par1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($cat->parent_id);
		if ($par1) 
			if ($par1->parent_id != null)
				$par2 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($par1->parent_id); 
		if ($par2)
			if ($par2->parent_id != null)
				$par3 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($par2->parent_id);
		if ($par3)
			if ($par3->parent_id != null)
				$par4 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($par3->parent_id); 
		if ($par4)
			if ($par4->parent_id != null)
				$par5 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($par4->parent_id); 
		//error_log("libAdsSearchWords.php 2");						
				// //za svaki cat i parent vdit koji imaju level i dodat ih u odgovarajuće polje u asw
				$asw = new \Baseapp\Suva\Models\AdsSearchWords();
				// treba jos dodat hijerahiju kategorija
				$asw->publication_id = $pcm->publication_id;
				$asw->ad_id = $ad->id;
				$asw->publications_categories_mappings_id = $pcm->id;
				$apt = libAdsSearchWords::makeAdParametersText($ad->id);
					
				$apt = \Baseapp\Suva\Library\Utils::remove_accents( $apt );
				$asw->ad_parameters_text = $apt;
				
				$asw->ad_full_text = \Baseapp\Suva\Library\Utils::remove_accents( $ad->title." ".$ad->description." ".$apt);
				$asw->category_text = \Baseapp\Suva\Library\Utils::remove_accents( $pcm->CategoryMapping()->n_path );
				$asw->ad_title_text = \Baseapp\Suva\Library\Utils::remove_accents( $ad->title);
				
		//error_log("libAdsSearchWords.php 3");		
				if ($cat->level == 1) $asw->category_id_level_1 = $cat->id;
				if ($cat->level == 2) $asw->category_id_level_2 = $cat->id;
				if ($cat->level == 3) $asw->category_id_level_3 = $cat->id;
				if ($cat->level == 4) $asw->category_id_level_4 = $cat->id;
				if ($cat->level == 5) $asw->category_id_level_5 = $cat->id;
				
				if ($par1){
					if ($par1->level == 1) $asw->category_id_level_1 = $par1->id;
					if ($par1->level == 2) $asw->category_id_level_2 = $par1->id;
					if ($par1->level == 3) $asw->category_id_level_3 = $par1->id;
					if ($par1->level == 4) $asw->category_id_level_4 = $par1->id;
					if ($par1->level == 5) $asw->category_id_level_5 = $par1->id;
				}
				if ($par2){
					if ($par2->level == 1) $asw->category_id_level_1 = $par2->id;
					if ($par2->level == 2) $asw->category_id_level_2 = $par2->id;
					if ($par2->level == 3) $asw->category_id_level_3 = $par2->id;
					if ($par2->level == 4) $asw->category_id_level_4 = $par2->id;
					if ($par2->level == 5) $asw->category_id_level_5 = $par2->id;
				}
				
				if ($par3){
					if ($par3->level == 1) $asw->category_id_level_1 = $par3->id;
					if ($par3->level == 2) $asw->category_id_level_2 = $par3->id;
					if ($par3->level == 3) $asw->category_id_level_3 = $par3->id;
					if ($par3->level == 4) $asw->category_id_level_4 = $par3->id;
					if ($par3->level == 5) $asw->category_id_level_5 = $par3->id;
				}
				
				if ($par4){
					if ($par4->level == 1) $asw->category_id_level_1 = $par4->id;
					if ($par4->level == 2) $asw->category_id_level_2 = $par4->id;
					if ($par4->level == 3) $asw->category_id_level_3 = $par4->id;
					if ($par4->level == 4) $asw->category_id_level_4 = $par4->id;
					if ($par4->level == 5) $asw->category_id_level_5 = $par4->id;
				}
				
				if ($par5){
					if ($par5->level == 1) $asw->category_id_level_1 = $par5->id;
					if ($par5->level == 2) $asw->category_id_level_2 = $par5->id;
					if ($par5->level == 3) $asw->category_id_level_3 = $par5->id;
					if ($par5->level == 4) $asw->category_id_level_4 = $par5->id;
					if ($par5->level == 5) $asw->category_id_level_5 = $par5->id;
				}

				
				
				$asw->create();
			//error_log("libAdsSearchWords.php 4");			
				
				
				
				// //full_text, category_text
				// $asw->create();
			// }
		// }	
		
		
		
	}
	
	public static function updateAdsSearchWordsStatus( $pAdId = null, $pPublicationId = null){
		/*
		  Za sve orders items koji su povezani s tim ad-om i tom publikacijom 
		  pronaći one koji su first_published prije danas i expire poslije danas
			za svaki pronaći koji ima maksimalni prioritet proizvoda
			za sve s maksimalnim prioritetom naći koji onaj koji ima najraniji expires date. Taj je aktivan
		*/
		//error_log("updateAdsSearchWordsStatus 1");
		
		//zbog pucanja na ads2
		return;
		
		
		
		$ad = $pAdId > 0 ? \Baseapp\Suva\Models\Ads::findFirst($pAdId) : null;
		$publication = $pPublicationId > 0 ? \Baseapp\Suva\Models\Publications::findFirst($pPublicationId) : null;
		
		$asws = $ad && $publication ? \Baseapp\Suva\Models\AdsSearchWords::find ( " ad_id = $ad->id and publication_id = $publication->id " ) : array();
		
		if (!$ad || !$publication || count($asws) == 0 ){
			//error_log ("updateAdsSearchWordsStatus 2: GREŠKA!!");
		} 
		foreach ($asws as $asw){
			$txt = "ad_id = $ad->id and n_publications_id = $publication->id and n_expires_at > '".date("Y-m-d H:i:s")."' and n_first_published_at <= '".date("Y-m-d H:i:s")."'";
			//error_log("updateAdsSearchWordsStatus 2: $txt ");
			$orders_items = \Baseapp\Suva\Models\OrdersItems::find( $txt );
			
			//traži se max od prioriteta
			$max_priority = 0;
			foreach ($orders_items as $oi ) {
				//error_log("updateAdsSearchWordsStatus 2.1 ");
				if( $oi->Product()->priority_online > $max_priority)
					$max_priority = $oi->Product()->priority_online;
				
				//error_log("updateAdsSearchWordsStatus 2: $max_priority ");
				$asw->product_priority = $max_priority ;
			
			}
			//trazi se min expires_at za max_prioritet
			$min_expires_at = date("Y-m-d H:i:s");
			$oi_min = null;
			foreach ($orders_items as $oi ) {
				
				if( $oi->n_expires_at < $min_expires_at && $oi->Product()->priority_online == $max_priority ){
					$min_expires_at = $oi->n_expires_at;
					$oi_min = $oi;
				}
				//error_log("updateAdsSearchWordsStatus 3: $min_expires_at");
				$asw->expires_at = $min_expires_at; 
			}
			
			if ($oi_min){
				$asw->order_item_id = $oi_min->id;
				$asw->expires_at = $oi->n_expires_at;
			}
			else
				return;
			//error_log ("updateAdsSearchWordsStatus 1 asw:".print_r( $asw->toArray(), true ) );
			$asw->update();
		}
		
	} 
	
	public static function makeAdParametersText($pAdId = null){
		
		if ($pAdId > 0 ) $ad = \Baseapp\Suva\Models\Ads::findFirst($pAdId);
		else return "";
		if (!$ad) {
			//error_log("libASW za ad ID $pAdId  nije nađen AD ");
			return "";
		}

		//error_log("libASW AD:".print_r($ad->toArray(), true));
		$strText = "";
		foreach ( $ad->AdsParameters() as $ap ){
			//error_log("libASW  ADS PARAMETERS".print_r($ap->toArray(), true));
			$parameter = $ap->Parameter();
			//error_log("libASW parameter :".print_r($parameter->toArray(), true));
			if ( $parameter->ParameterType()->accept_dictionary == 1 ) {
				$dictionary = \Baseapp\Suva\Models\Dictionaries::findFirst( (int) $ap->value );
				$value = $dictionary->name;
			}
			else 
				$value = $ap->value;
			
			
			//Iskorišten je hasOne u Parameter
			$strText .= "  ".( $parameter->Dictionary() ? $parameter->Dictionary()->name : '' )." ".$value;
		}
		
		/*
		za slijedeća polja u ad: country_id, county. city, municipality:
			- ako je vrijednost veća od 0, dodati opis na slijeeći naču+in:  City:sibenik, Country:Hrvatska
		*/
		if ($ad->country_id > 0 ) $strText.= " ".$ad->Country()->name;
		if ($ad->county_id > 0 ) $strText.= " ".$ad->County()->name;
		if ($ad->municipality_id > 0 ) $strText.= " ".$ad->Municipality()->name;
		
		
		return $strText;
	}
	
}