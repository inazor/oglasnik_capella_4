<?php

namespace Baseapp\Suva\Library;

use Baseapp\Suva\Models\Orders;
use Phalcon\Mvc\Model\Resultset;

class SimpleXMLProcessorRba extends SimpleXMLReader
{
    protected $called = false;
    protected $warnings = array();
    protected $completed = array();
    protected $warnings_for_export = array();

    protected $data = array();

    protected $stats = array(
        'elements'       => 0,
        'pbo_matches'    => 0,
        'pbo_duplicates' => 0,
        'completed'      => 0
    );

    public function __construct(){
        // By node name
        // $this->addCallback('promet', array($this, 'callbackPromet'));

        // By xpath
        // $this->addCallback('/root/izvod/dan/promet', array($this, 'callbackPromet'));

        // Xpath for the new ISO 20022-based xml bank statement file
        $this->addCallback('/Document/BkToCstmrStmt/Stmt/Ntry', array($this, 'callbackNtry'));
    }

    public function callbackPromet( SimpleXMLReader $reader)
    {
        $this->called = true;
        $this->stats['elements']++;

        $xml = $reader->expandSimpleXml();
        $pbo = (string) $xml->pbo;

        if (Utils::str_has_pbo_prefix($pbo)) {
            $this->stats['pbo_matches']++;

            $total    = (string) $xml->iznos;
            $total_lp = Utils::kn2lp($total);

            // Store what appears to be a prefix-matched pbo we might be interested in
            // while skipping duplicates (those already encountered in the same file)
            if (!isset($this->data[$pbo])) {
                $this->data[$pbo] = array(
                    'total'    => $total,
                    'total_lp' => $total_lp,
                    'pbo'      => $pbo,
                );
            } else {
                $this->stats['pbo_duplicates']++;
            }
        }

        // A callback has to return true in order for others to continue
        return true;
    }

    public function callbackNtry(SimpleXMLReader $reader)
    {
        $this->called = true;
        $this->stats['elements']++;

        $xml = $reader->expandSimpleXml();

        $pbo_full = null;

        // zyt: 26.07.2016.
        // RBA appears to have moved things around a bit (or something).
        // Either way, the PBO we care about is now in <Ntry> that has a child
        // element of <CdtDbtInd> whose text value is CRDT. Once we find those,
        // we look for <NtryDtls><TxDtls><Refs><RmtInf><Strd><CdtrRefInf><Ref> el,
        // or in xpath terms: NtryDtls/TxDtls/RmtInf/Strd/CdtrRefInf/Ref
        // Note:
        // Bank had kept just 'HR99' for example in the path above during June 2016 (
        // and our PBO was in <EndToEndId>), but at some point in July they seem to have
        // changed it.
        $is_crdt = (isset($xml->CdtDbtInd) && ('CRDT' === (string) $xml->CdtDbtInd));
        if ($is_crdt) {
            $pbo_full = (string) $xml->NtryDtls->TxDtls->RmtInf->Strd->CdtrRefInf->Ref;

            // TODO/FIXME: We could test if whatever we got above is shorter than 4 chars
            // and then try the older path, but it might not be needed at all for now, and
            // they'll probably change shit again soon anyway...
        } else {
            // Bail early if not the right type of <Ntry>
            // All callbacks must return true for others to proceed
            return true;
        }

        // Cleanup our PBO so we can match it (if we're the right type of <Ntry>)
        $pbo = null;
        if (!empty($pbo_full)) {
            // Strips commonly spotted prefixes which appeared in the new xml format
            $pbo = trim(str_replace(array('HR00', 'HR ', 'HR'), '', $pbo_full));
            // error_log($pbo_full . ' -> ' . $pbo);
        }

        // Proceed if we have something that matches our prefix
        if (!empty($pbo) && Utils::str_has_pbo_prefix($pbo)) {
            $this->stats['pbo_matches']++;

			//10.01.2017 - traženje datuma transakcije
			$payment_date = $xml->ValDt->Dt;
			
			
            $total    = (string) $xml->Amt;
            $total_lp = Utils::kn2lp($total);

            // Store what appears to be a prefix-matched pbo we might be interested in
            // while skipping duplicates (those already encountered in the same file)
            if (!isset($this->data[$pbo])) {
                $this->data[$pbo] = array(
                    'total'    => $total,
                    'total_lp' => $total_lp,
                    'pbo'      => $pbo,
					'payment_date' => $payment_date,
                );
            } else {
                $this->stats['pbo_duplicates']++;
            }
        }

        // A callback has to return true in order for others to continue
        return true;
    }

    /**
     * Process payment data collected during XML parsing stage (via callback) using
     * a single query to fetch all the matched orders and then iterating over the
     * resultset (as opposed to querying each single Order as we encounter it)
     */
    public function processFoundPbos()
    {
        // Prepare an array mapping our actual order ids with matching xml payment data
       
		//radi listu order id-jeva tako da ide kroz data, kupi PBO i iz njega traži broj ordera
		$xml_orders = array();
        foreach ($this->getData() as $pbo => $xml_order_data) {
            $order_id = (int) Utils::str_remove_pbo_prefix($pbo);
            $xml_orders[$order_id] = $xml_order_data;
			\Baseapp\Suva\Library\Nava::poruka("obradjuje se uplata:$pbo , nađen order_id $order_id");
        }

        // Bail if we have nothing to work with
        if (empty($xml_orders)) {
            return;
        }

        // Query and process each Order separately
        
		//kroz sve order id-jeve koje je našao u XMLU
		$order_ids = array_keys($xml_orders);
        foreach ($order_ids as $order_id) {
			
            $order = Orders::findFirst($order_id);
			
            if (!$order) {
				//poruka 1
                // No such Order here for whatever reason
                $xml_pbo = $xml_orders[$order_id]['pbo'];
                // Matching output of $order->ident() for logging/audit purposes
                $error_msg                   = 'Order #' . $order_id . ' (PBO: ' . $xml_pbo . ') does not exist in our DB';
                $this->warnings[]            = $error_msg;
                $this->warnings_for_export[] = array(
                    'ID'  => $order_id,
                    'PBO' => $xml_pbo,
                    'MSG' => $error_msg
                );
            } 
			
			else {
                // We've found something, process it
                $this->processFoundOrder($order, $xml_orders[$order_id], $order_id);
            }
        }
    }

    /**
     * @param Orders $order
     * @param array $xml_data
     *
     * @return bool
     */
    protected function processFoundOrder(Orders $order, $xml_data = array(), $pOrderId = null){
        if ($pOrderId) $lOrder = \Baseapp\Suva\Models\Orders::findFirst($pOrderId);
		else $lOrder = null;
		
		$datumIzvoda = "".$xml_data['payment_date'];
		error_log("SimpleXMLProcessorRba::processFoundOrder 1 datumIzvoda".$xml_data['payment_date']);
		
		$ident = $order->ident();
		//error_log("processFoundOrder xml_data ".print_r($xml_data,true));
        // Should we finalize or not (modified as certain checks pass/fail)
        $finalize = true;

        // Check if status allows changes at all
		// NIKOLA : Zakomentirano jer ne dopusta promjenu status ako je ona ista drugo osim 1
	   // if (!($order->isStatusChangeable())) {
            // $warning_msg                 = $ident . ' is not changeable (status=' . $order->getStatusText() . ')';
            // $this->warnings[]            = $warning_msg;
            // $this->warnings_for_export[] = array(
                // 'ID'  => $order->id,
                // 'PBO' => $order->getPbo(),
                // 'MSG' => $warning_msg
            // );
            // // Don't even bother attempting to finalize such entities
            // $finalize = false;
        // }

        // Check that payment amount matches the required amount
        $xml_total    = $xml_data['total'];
        $xml_total_lp = $xml_data['total_lp'];
        // if ($order->getTotalRaw() !== $xml_total_lp) {
            // $warning_msg                 = $ident . ' - payment amount mismatch (payment: ' . $xml_total . ', required: ' . $order->getTotal() . ')';
            // $this->warnings[]            = $warning_msg;
            // $this->warnings_for_export[] = array(
                // 'ID'  => $order->id,
                // 'PBO' => $order->getPbo(),
                // 'MSG' => $warning_msg
            // );
            // // No need to even attempt finalizing these
            // $finalize = false;
        // }

		// error_log("order id je = ".$order->id);
		
        if ( (float)$order->n_total_with_tax != (float)$xml_total ) {
			
			// error_log("NIKOLA ".gettype((float)$order->n_total_with_tax).gettype((float)$xml_total));
			// error_log("NIKOLA1 ".$order->n_total_with_tax.'  RAZMAK   '.$xml_total);
			//greška 3
            $warning_msg                 = $ident . ' - payment amount mismatch (payment: ' . $xml_total . ', required: ' . $order->n_total_with_tax . ')';
            $this->warnings[]            = $warning_msg;
            $this->warnings_for_export[] = array(
                'ID'  => $order->id,
                'PBO' => $order->getPbo(),
                'MSG' => $warning_msg
            );
            // No need to even attempt finalizing these
            $finalize = false;
        }
		// // NIKOLA - PROVJERA n_pbo na orderima i PBO na XML-u
		
		// if( $order->n_pbo != $xml_data['pbo']) {
			// $warninng_msg = $ident . ' - PBO mismatch ' . $xml_data['PBO']
			
		// }
		
		if ( $order && $order->n_status == 5){
			//greška 2
			$warning_msg  = 'Pronađena je uplata (PBO: ' . $xml_data['PBO'] . ') za order  #' . $order->id . '  koji je u statusu Paid Invoice';
			$this->warnings[]            = $warning_msg;
            $this->warnings_for_export[] = array(
                'ID'  => $order->id,
                'PBO' => $order->getPbo(),
                'MSG' => $warning_msg
            );
			 $finalize = false;
		}
		

		
		
		//20.1.2017 - 2124, greška od 20.1.2017 - ne smije plaćati offline koji su trebali biti objavljeni
		foreach($order->OrdersItems() as $oi ) {
			//greska 4
			//if( strtotime($oi->n_first_published_at) < strtotime($order->n_payment_date) ) {
			if ( $oi->Product() && $oi->Product()->is_online_product == 0 ){
				if( strtotime($oi->n_first_published_at) < strtotime( $datumIzvoda ) ) {
					$warning_msg                 = "Uplate za Order $oi->order_id  se ne može zatvoriti jer je datum prve objave $oi->n_first_published_at manji od datuma uplate $datumIzvoda  STATUS: LATE";
					$this->warnings[]            = $warning_msg;
					$this->warnings_for_export[] = array(
						'ID'  => $order->id,
						'PBO' => $order->getPbo(),
						'MSG' => $warning_msg
					);
					
					$finalize = false;
				}
			}
		}
		
		
		if ( array_key_exists('logged_in_user_id',$_SESSION['_context'])) 
			$loggedUserId = $_SESSION['_context']['logged_in_user_id'] > 0 ? $_SESSION['_context']['logged_in_user_id'] : null ;
		else 
			$loggedUserId = null; 
		
		
		if ( $order->n_source == 'frontend' ){
			$operatorId = \Baseapp\Suva\Library\libAppSetting::get('frontend_default_operator_id');
			if ($operatorId > 0){
				$operator = \Baseapp\Suva\Models\Users::findFirst($operatorId);
				if (!$operator) {
					Nava::greska("User $operatorId, supplied via frontend_default_operator_id, not found.");
				}				
			}
			else{
				Nava::greska("Parameter frontend_default_operator_id not found");
			}
		}else{
			$operatorId = $loggedUserId;
		}
		
				
		
		//\Baseapp\Suva\Library\Nava::poruka("ORDER $order->id");

        // Finalize the order/purchase if various conditions above were met
        $success = false;
        if ($finalize) {
            //$finalized = $order->finalizePurchase();
			

			// //10.1.2017 -dodan je payment_date koji se puni u liniji 112
			$greska = \Baseapp\Suva\Library\libUserActions::actOrderPayWithMethod(
				'VIRMAN'
				, $order->id
				, $order->user_id
				, $order->user_id
				, false
				, $datumIzvoda
				
				);
			
			if (!$greska ){
				Nava::poruka ("Order $order->OrderStatus()->name $order->id changed to Paid Invoice");
				$finalized = true;
			}
			else{
				$finalized = false;
			}
            // Collect data about success/failure for log/audit purposes
            if ($finalized) {
				
				//borna - mora povuc operator_id i imat nav status unsynced da bi se mogao poslati i proknjizit u navisionu
				Nava::runsql("update orders set n_nav_sync_status = 'unsynced',n_operator_id = $operatorId where id = ".$order->id);
				
                $this->stats['completed']++;
                $completed_msg     = $ident . ' successfully finalized';
                $this->completed[] = $completed_msg;
            } else {
                $error_msg  = $ident . ' failed to finalize/complete';
                $error_data = $order->getMessages();
                if (!empty($error_data)) {
                    $error_data = var_export($error_data, true);
                }
                if (!empty($error_data)) {
                    $error_msg .= ' (error data: ' . $error_data . ')';
                }
                $this->warnings[]            = $error_msg;
                $this->warnings_for_export[] = array(
                    'ID'  => $order->id,
                    'PBO' => $order->getPbo(),
                    'MSG' => $error_msg
                );
            }
            $success = $finalized;
        }

        return $success;

	}


	public function called()
    {
        return $this->called;
    }

    public function getWarnings($for_export = false)
    {
        return $for_export ? $this->warnings_for_export : $this->warnings;
    }

    public function getCompletedMessages()
    {
        return $this->completed;
    }

    public function getStats()
    {
        return $this->stats;
    }

    public function getData()
    {
        return $this->data;
    }
 
}