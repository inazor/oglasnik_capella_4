<?php

namespace Baseapp\Cli\Tasks;

/**
 * Initialize imports
 */
class ImportTask extends MainTask
{
    public function adsByCategoryAction()
    {
        $in_params = $this->router->getParams();
        $options = array();
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        $inet_kat = null;
        if (isset($options['inet_kat'])) {
            $inet_kat = $options['inet_kat'];
        }

        if (!$inet_kat) {
            echo 'No category give!' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }

        if (empty($options['remaining_ads'])) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--             STARTING WITH ADS MIGRATION            --' . PHP_EOL;
            echo '--                  importByCategory                  --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }
        $migration = new \Baseapp\Library\Mapping\MigrationRawDb($options);
        if ($ads = $migration->importAdsByCategory($inet_kat)) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Imported ads      : ' . $ads['imported'] . PHP_EOL;
            echo '-- Duplicate ads     : ' . $ads['duplicate'] . PHP_EOL;
            echo '-- Error with import : ' . $ads['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

    public function adsAction()
    {
        $in_params = $this->router->getParams();
        $options = array();
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        if (empty($options['remaining_ads'])) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--             STARTING WITH ADS MIGRATION            --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }
        $migration = new \Baseapp\Library\Mapping\MigrationRawDb($options);
        if ($ads = $migration->importAds()) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Imported ads      : ' . $ads['imported'] . PHP_EOL;
            echo '-- Duplicate ads     : ' . $ads['duplicate'] . PHP_EOL;
            echo '-- Error with import : ' . $ads['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

    public function fixAdsLocationByCategoryAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'fixAdsLocationByCategory'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        if (empty($options['remaining_ads'])) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--             STARTING WITH ADS MIGRATION            --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }
        $migration = new \Baseapp\Library\Mapping\MigrationRawDb($options);
        if ($ads = $migration->fixAdsLocationByCategory()) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Fixed ads         : ' . $ads['fixed'] . PHP_EOL;
            echo '-- Error with import : ' . $ads['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

    public function fixSearchTermsAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'fixSearchTerms'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        if (empty($options['remaining_ads'])) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--                 FIX SEARCH TERMS                   --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }
        $migration = new \Baseapp\Library\Mapping\MigrationRawDb($options);
        if ($ads = $migration->fixSearchTermsAndTitleDescriptions()) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Fixed ads         : ' . $ads['fixed'] . PHP_EOL;
            echo '-- Error with fix    : ' . $ads['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

    public function fixSpecificSearchTermsAction()
    {
        $in_params = $this->router->getParams();
        $options = array();
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        echo PHP_EOL;
        echo '========================================================' . PHP_EOL;
        echo '--            FIX SPECIFIC SEARCH TERMS               --' . PHP_EOL;
        echo '--------------------------------------------------------' . PHP_EOL;
        $migration = new \Baseapp\Library\Mapping\MigrationRawDb($options);
        if ($ads = $migration->fixSpecificSearchTermsAndTitleDescriptions()) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Fixed ads         : ' . $ads['fixed'] . PHP_EOL;
            echo '-- Error with fix    : ' . $ads['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

    public function fixAdBodyAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'fixAdBody'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        if (empty($options['remaining_ads'])) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--                    FIX AD BODY                     --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }
        $migration = new \Baseapp\Library\Mapping\MigrationRawDb($options);
        if ($ads = $migration->fixAdBody()) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Fixed ads         : ' . $ads['fixed'] . PHP_EOL;
            echo '-- Error with fix    : ' . $ads['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

    public function avusMappingFixAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'avusMappingFix'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        echo PHP_EOL;
        echo '========================================================' . PHP_EOL;
        echo '--                    FIX AD BODY                     --' . PHP_EOL;
        echo '--------------------------------------------------------' . PHP_EOL;
        $migration = new \Baseapp\Library\Mapping\MigrateCategorySettings($options);
        if ($cs = $migration->avusMappingFix()) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Fixed category settings : ' . $cs['fixed'] . PHP_EOL;
            echo '-- Error with fix          : ' . $cs['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

    public function importUsersAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'importUsers'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        $options['log2email'] = true;

        echo PHP_EOL;
        echo '========================================================' . PHP_EOL;
        echo '--                     IMPORT USERS                   --' . PHP_EOL;
        echo '--------------------------------------------------------' . PHP_EOL;
        $migration = new \Baseapp\Library\Mapping\MigrationRawDb($options);
        if ($users = $migration->importUsers()) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Imported users    : ' . $users['imported'] . PHP_EOL;
            echo '-- Duplicate users   : ' . $users['duplicate'] . PHP_EOL;
            echo '-- Error with import : ' . $users['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

    public function migratePhoneNumbersAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'migratePhoneNumbers'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        $options['log2email'] = true;

        echo PHP_EOL;
        echo '========================================================' . PHP_EOL;
        echo '--               MIGRATE PHONE NUMBERS                --' . PHP_EOL;
        echo '--------------------------------------------------------' . PHP_EOL;
        $migration = new \Baseapp\Library\Mapping\MigrateAvusPhoneNumbers($options);
        if ($status = $migration->migratePhoneNumbers()) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- OK                : ' . $status['ok'] . PHP_EOL;
            echo '-- NOK               : ' . $status['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        }
    }

}
