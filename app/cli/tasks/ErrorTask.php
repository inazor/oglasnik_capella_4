<?php

namespace Baseapp\Cli\Tasks;

use Baseapp\Console;
use Baseapp\Traits\AuditLog;

class ErrorTask extends \Phalcon\CLI\Task
{

    //use AuditLog;

    public function errorAction()
    {

    }

    public function show503Action()
    {
        Console::error('Service unavailable (is maintenance active?)');
    }
}
