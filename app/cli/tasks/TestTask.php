<?php

namespace Baseapp\Cli\Tasks;

use Baseapp\Library\Utils;

/**
 * Run some CLI tests
 */
class TestTask extends MainTask
{
    public function securityHashAction()
    {
        $params = $this->router->getParams();

        $pass_plain = isset($params[0]) ? $params[0] : 'pasvord';
        $pass_hash = $this->security->hash($pass_plain);

        echo "\nSecurity service: " . get_class($this->security);
        echo "\nPassword plain: " . $pass_plain;
        echo "\nPassword hashed: " . $pass_hash;

        $valid = $this->security->checkHash($pass_plain, $pass_hash);
        echo "\ncheckHash result: " . var_export($valid, true);
        echo "\n";
    }

    public function fixLocationsAction()
    {
        $migration = new \Baseapp\Library\Mapping\Migration();
        $migration->fixLocations();
    }

    public function carsDictAction()
    {
        $migration = new \Baseapp\Library\Mapping\Migration();
        $migration->carsDict();
    }

    public function fillAdsWithSearchTermsAction()
    {
        $db = new \Baseapp\Library\RawDB();
        $ads = $db->find("SELECT id, title, description, json_data FROM ads", array());

        $stats = array(
            'OK'  => 0,
            'NOK' => 0
        );

        if ($ads) {
            foreach ($ads as $ad) {
                $ad_json_data = !empty($ad['json_data']) ? json_decode($ad['json_data'], true) : null;
                //if ($ad_json_data) {
                    $search_data = \Baseapp\Library\Parameters\Parametrizator::get_search_terms($ad, $ad_json_data);
                    if ($search_data) {
                        $updated = $db->update(
                            "INSERT INTO ads_search_terms (ad_id, search_data) VALUES (:ad_id, :search_data)",
                            array(
                                'search_data' => $search_data,
                                'ad_id'       => $ad['id']
                            )
                        );
                        if ($updated) {
                            $stats['OK']++;
                        } else {
                            $stats['NOK1']++;
                        }
                    }
                //}
            }
        }
        var_dump($stats);

    }

    public function populateUnaccentedSearchTermsAction()
    {
        $db = new \Baseapp\Library\RawDB();

        $tbl = 'ads_search_terms';

        $where = ' WHERE (search_data != "" AND search_data IS NOT NULL) AND search_data_unaccented IS NULL';
        $total = $db->findFirst('SELECT COUNT(*) FROM ' . $tbl . $where, array(), true);

        $per_batch = 500;
        $batches = ceil($total / $per_batch);

        if ($total <= 0) {
            return;
        }

        echo "\nUnaccenting search data!!\n";
        echo "\nTotal: " . $total;
        echo "\nPer batch: " . $per_batch;
        echo "\nBatches: " . $batches;

        $last_id = 0;
        $batch_cnt = 0;

        do {
            $sql   = 'SELECT id, ad_id, search_data FROM ' . $tbl . $where . ' AND id > '. $last_id . ' ORDER BY id LIMIT ' . $per_batch;

            $batch_cnt++;
            echo "\n-- Starting Batch #" . $batch_cnt. "/" . $batches . " (estimated) --";
            echo "\n-- " . $sql . " --";
            $accented = $db->find($sql, array());
            if ($accented) {
                foreach ($accented as $result) {
                    $unaccented = Utils::remove_accents($result['search_data']);
                    if (!empty($unaccented)) {
                        $updated = $db->update('
                        UPDATE
                          ads_search_terms
                        SET
                          search_data_unaccented = :unaccented
                        WHERE id = :id',
                            array(
                                'unaccented' => $unaccented,
                                'id'         => $result['id']
                            )
                        );
                        if ($updated) {
                            echo "\n\tUpdated record #" . $result['id'] . " (ad id: " . $result['ad_id'] . ")";
                        }
                    }
                    // Keep this
                    $last_id = $result['id'];
                }
            }

            unset($accented_data, $result, $updated, $unaccented);
            echo "\n-- Finished Batch #" . $batch_cnt . "/" . $batches . " --";

        } while (!empty($accented));
    }

}
