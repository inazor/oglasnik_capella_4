<?php

namespace Baseapp\Cli\Tasks;

/**
 * Prepare CLI Tasks
 * Used to run prep/deploy/maintenance tasks
 */
class PrepareTask extends MainTask
{

    /**
     * Minify css and js collection
     */
    public function assetAction()
    {
        foreach (array('css', 'js') as $asset) {
            foreach ($iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(ROOT_PATH . '/public/' . $asset, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST) as $item) {
                if (!$item->isDir() && ($item->getExtension() == 'css' || $item->getExtension() == 'js')) {
                    $subPath = $iterator->getSubPathName();
                    $dir = strstr($subPath, $item->getFilename(), true);
                    $add = 'add' . ucfirst($asset);
                    $this->assets->$add($asset . '/' . $dir . $item->getFilename());
                }
            }
        }

        // Minify css and js collection
        \Baseapp\Library\Tool::assetsMinification();
    }

    /**
     * Chmod our special folders/files that need to be group writable
     */
    public function chmodAction()
    {
        $dirs = array(
            '/cache',
            '/logs',
            '/public/min',
            '/public/repository'
        );

        foreach ($dirs as $dir) {
            chmod(ROOT_PATH . $dir, 0775);
        }
    }

    /**
     * Remove cache files, clear minified assets...
     */
    public function rmAction()
    {
        if ($this->config->app->env === 'development' || $this->config->app->env === 'testing') {
            exec('rm -R ' . ROOT_PATH . '/cache/*');
            // TODO: this deletes .dotfiles (on Windows at least) so that'll need fixing
            exec('rm -R ' . ROOT_PATH . '/public/min/*');
        }
    }

    /**
     * Grab the current version from git and replace the current one in config
     */
    public function versionAction()
    {
        $is_windows = ('\\' === DIRECTORY_SEPARATOR);
        if (!$is_windows) {
            $cmd = 'projver=$(git describe --tags --long 2> /dev/null || echo `git rev-parse --short HEAD`); sed -i -e "/^\[versions\]/,/^\[.*\]/ s|^\(app[ \t]*=[ \t]*\).*$|\1$projver|" "' . ROOT_PATH . 'config/config.ini"';
            exec($cmd);
        } else {
            // what here? kind of hard to pull this off in a simple way really...
            // There is no sed, and there is no "easy" equivalent: http://stackoverflow.com/q/127318
            // And capturing program output as a string is a big pain in the ass too: http://stackoverflow.com/q/2323292
            echo 'Cannot run on Windows yet, please set the version in config.ini manually.';
            exit(1);
        }
    }

    /**
     * Render views from volt files
     */
    public function voltAction()
    {
        ob_start();
        $e = '';
        foreach (array('frontend', 'backend') as $module) {
            foreach ($iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(ROOT_PATH . '/app/' . $module . '/views/', \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST) as $item) {
                if (!$item->isDir() && $item->getExtension() == 'volt') {
                    $this->view->setViewsDir(ROOT_PATH . '/app/' . $module . '/views/');

                    $sub_path = $iterator->getSubPathName();
                    $file = strstr($item->getFilename(), '.volt', true);
                    $dir = strstr($sub_path, $item->getFilename(), true);

                    $e .= $this->view->partial($dir . $file);
                }
            }
        }
        ob_get_clean();
        //\Baseapp\Console::log($e);
    }

    /**
     * Initialize category settings with data currently located in categories table
     */
    public function initCategorySettingsAction()
    {
        $categories = \Baseapp\Models\Categories::find();

        if ($categories) {
            foreach ($categories as $category) {
                $category_settings_id = null;
                if ($category->getSettings()) {
                    $category_settings_id = $category->getSettings()->id;
                }

                if ($category_settings_id) {
                    $category_settings = \Baseapp\Models\CategoriesSettings::findFirst($category_settings_id);
                } else {
                    $category_settings = new \Baseapp\Models\CategoriesSettings;
                    $category_settings->category_id = $category->id;
                }
                if ($category->getTransactionType()) {
                    $category_settings->default_ad_expire_time = (isset($category->default_ad_expire_time) ? intval($category->default_ad_expire_time) : 30);
                    $category_settings->meta_title = (isset($category->meta_title) ? $category->meta_title : null);
                    $category_settings->meta_description = (isset($category->meta_description) ? $category->meta_description : null);
                    $category_settings->meta_keywords = (isset($category->meta_keywords) ? $category->meta_keywords : null);
                }
                $category_settings->save();
            }
        }
    }
}
