<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', dirname(__DIR__));
}

if (!defined('APP_DIR')) {
    define('APP_DIR', ROOT_PATH . '/app');
}

// require composer autoloader
require ROOT_PATH . '/vendor/autoload.php';

require_once 'ModelsTestCase.php';
require_once APP_DIR . '/Bootstrap.php';

$app = new \Baseapp\Bootstrap(new \Phalcon\Di\FactoryDefault());

// clear out any error handlers we've set in the Bootstrap in order to allow
// PHPUnit's handlers to function as needed...
$app->restore_default_error_handling();
