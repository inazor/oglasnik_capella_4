<?php

class UniquenessExtraConditionsTest extends PHPUnit_Framework_TestCase
{

    public function testLocationsExtraConditionsUniqueValidation()
    {
        $validation = new \Phalcon\Validation();
        $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
            'model' => '\Baseapp\Models\Locations',
            'extra_conditions' => array(
                'parent_id IS NULL AND level = :level:',
                array(
                    'level' => 1
                )
            ),
            'message' => 'Location not unique under specified conditions',
        )));

        // this should already exist
        $data = array('name' => 'Hrvatska');
        $messages = $validation->validate($data);
        $this->assertCount(1, $messages);
        $this->assertEquals('Location not unique under specified conditions', $messages[0]);

        // this should not exist yet
        $data = array('name' => 'TestCategoryThatDoesntExistAtAllUnderSameConditions');
        $messages = $validation->validate($data);
        $this->assertCount(0, $messages);

        // test with different conditions, Hrvatska doesn't exist within level 2
        $validation = new \Phalcon\Validation();
        $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
            'model' => '\Baseapp\Models\Locations',
            'extra_conditions' => array(
                'parent_id IS NULL AND level = :level:',
                array(
                    'level' => 2,
                )
            ),
            'message' => 'Location not unique under specified conditions',
        )));
        $data = array('name' => 'Hrvatska');
        $messages = $validation->validate($data);
        $this->assertCount(0, $messages);
    }

}
