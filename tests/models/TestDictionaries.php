<?php

use Baseapp\Library\Behavior\NestedSet as NestedSetBehavior;

class TestDictionaries extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        $this->addBehavior(new NestedSetBehavior(array(
            'hasManyRoots' => true,
            'rootAttribute' => 'root_id',
            'rightAttribute' => 'rght',
            'parentAttribute' => 'parent_id',
            'useTransactions' => true,
        )));

        $this->hasMany('id', 'TestParameters', 'dictionary_id', array(
            'alias' => 'Parameters'
        ));

        // disable not null validations for testing
        $this->setup(array('notNullValidations' => false));
    }

    public function beforeSave()
    {
        if (!isset($this->active)) {
            $this->active = rand(0, 1) ? 1 : 0;
        }
    }

    public function getSource()
    {
        return 'dictionaries';
    }

}
