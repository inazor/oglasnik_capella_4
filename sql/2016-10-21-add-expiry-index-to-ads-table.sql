ALTER TABLE `ads`
	ADD INDEX `I_expiry` (`active`, `manual_deactivation`, `is_spam`, `expires_at`);
