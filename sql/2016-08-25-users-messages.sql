ALTER TABLE `users_messages` ADD `parent_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `id`, ADD INDEX (`parent_id`);
ALTER TABLE `users_messages` ADD `thread_id` BIGINT UNSIGNED NULL DEFAULT NULL AFTER `parent_id`, ADD INDEX (`thread_id`);
ALTER TABLE `users_messages` ADD CONSTRAINT `users_messages_parent_fk` FOREIGN KEY (`parent_id`) REFERENCES `users_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `users_messages` ADD CONSTRAINT `users_messages_thread_fk` FOREIGN KEY (`thread_id`) REFERENCES `users_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `users_messages_states` ADD `deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `read_at`;
ALTER TABLE `users_messages` ADD INDEX `users_messages_created_by_fk_idx` (`created_by_user_id` ASC);
ALTER TABLE `users_messages` ADD CONSTRAINT `users_messages_created_by_fk` FOREIGN KEY (`created_by_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
